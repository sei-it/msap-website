﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Technical Assistance Webinar</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 14px;
    font-weight: bold;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}

#download_list li{
	padding-bottom:10px;
}
-->
</style></head>
<body class="oneColElsCtrHdr">

<div id="container">
  <div id="header">
    <img src="Webinar_docs/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  <div id="mainContent">
    <h1>Program Sustainability Resources</h1>
   
   <table width="85%" border="0">
   		<tr>
 			<td style="text-align:right;vertical-align:top;">
            	<img src="Webinar_docs/Program_sustainability_webinar_4_4_12/PS_webinar_img2.jpg" style="border:2px #2274A0 solid;" />
            </td>
   			<td style="vertical-align:top;padding-left:15px;">
             <div><span id="subtitle">Planning for Sustainability: Aligning Project Objectives with Goals and Results</span><br/>
  Thank you for registering for the second webinar in the MSAP Center's Planning for Sustainability series. This will be held on April 4, 2012, from 1:00 p.m. to 2:00 p.m. Eastern time.
   
   <p>Sustaining a magnet school program depends on developing a clear, sensible, and convincing plan for organizing the key resources you need to continue—and to expand—your program.</p>
   
   <p>This webinar and its related tools will help you develop a logic model within the unique context of your project, and articulate clear implementation plans for a 3- to 5-year period.</p> 
   Here are the tools for this webinar: 
   </div>
            <ol id="download_list">
                <li><strong>Putting a Logic Model Together for Your MSAP Project</strong>: This tool describes the steps needed to complete your logic model. It includes three sample logic models for magnet schools: one for a STEM theme, one for IB, and one for the arts. <a href="Webinar_docs/Program_sustainability_webinar_4_4_12/MSAP_Logic_Model_with_Instructions_and_Samples.doc" target="_blank">Download</a>
                </li>
                <li><strong>Worksheet: Clarifying What MSAP Grantees Want to Sustain</strong>: To help you define the scope and scale of project activities, this worksheet shows how to break down each activity and think about how it may be need to be sustained differently over time. 
                <a href="Webinar_docs/Program_sustainability_webinar_4_4_12/What_do_you_want_to_sustain_worksheet.doc" target="_blank">Download</a>
                </li>
            </ol>
            </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
     </table>
	<!-- end #mainContent --></div>
  <div id="footer">
    <img src="Webinar_docs/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>
</body>
</html>
