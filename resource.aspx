﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="resource.aspx.cs" Inherits="resource" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Resources
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Library</h1>
        <div style="width: 620px; float: left; height: auto; display: table;">
            <p>
                The MSAP Center is dedicated to providing high-quality resources to help develop, implement, and evaluate innovative magnet programs. 
            </p>
            <div>
                <a href="publications.aspx">
                    <img src="images/pubs.gif" alt="publications" border="0" style="float: left; margin-right: 4px;" /></a>
                <a href="multimedia.aspx">
                    <img src="images/multimedia.gif" alt="multimedia" border="0" style="float: left;
                        margin-right: 4px;" /></a> <a href="toolkits.aspx">
                            <img src="images/toolkit.gif" alt="tolkits and guides" border="0" style="float: left;" /></a></div>
            <p>&nbsp;
                
            </p>

    
            <div class="ressearch" style="margin-top: 20px;border-color:White;">

             <p style="width:99%;">
             To browse resources by type, click on one of the buttons above, or use the Resource Type dropdown menu below. To search by keyword, enter the keywords in the Search Terms box and click Search. Or, to search by topic, select a topic area in the list box menu below and click Search. You can also combine any of the search options below to further filter your results.
             </p>
               
                <div style="background-color:#C0C0C0; text-align:center; width:97%">
                <asp:Literal ID="ltlPagingSummary" runat="server" Visible="true" />
                </div>
                <br />
                        <asp:HiddenField ID="hfSelection" runat="server" />
                        <asp:HiddenField ID="hfKeyword" runat="server" />
                        <asp:HiddenField ID="hfTitle" runat="server" />
                        <asp:HiddenField ID="hfOrg" runat="server" />

                        <div>
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="kwdButton">
                           
                            <table width="98%" style="background-image:url('images/searchbox.jpg'); background-repeat:no-repeat; border-width:0px; border-color:White;">

                           <tr>
                           <td>Search Terms</td>
                           <td>Resource Type</td>
                            <td>Topic Areas</td>
                           </tr>
                           <tr>
                           <td style="vertical-align:top;"><asp:TextBox ID="txtKeywords"  runat="server" CssClass="msapTxt" Width="200px" ></asp:TextBox></td>
                           <td style="vertical-align:top;">
                                <asp:DropDownList ID="ddlTypeResource" runat="server" CssClass="msapTxt msapDrop">
                                    <asp:ListItem Value="" Selected="True">Select All</asp:ListItem>
                                    <asp:ListItem Value="print">Publications</asp:ListItem>
                                    <asp:ListItem Value="video">Multimedia</asp:ListItem>
                                    <asp:ListItem Value="toolkit">Toolkits &amp; Guides</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                           <td><asp:ListBox CssClass="msapTxt msapDrop" DataTextField="topicname" DataValueField="id" DataSourceID="dsPulicationTopic" SelectionMode="Multiple" Height="180px" ID="LboxTopics" runat="server" AutoPostBack="false"
                                    >
                                </asp:ListBox></td>
                           
                           </tr>
                           <tr>
                           <td colspan="3" style="text-align:center">
                           <asp:Button ID="kwdButton" runat="server" Text="Search" CssClass="msapBtn"
                                    OnClick="OnSearch" />
                            <asp:Button ID="LinkButton1" runat="server" CssClass="msapBtn" Text="Clear Search Filters" OnClick="OnClearSearch" />
                           </td>
                           </tr>
                           </table>
                   
</asp:Panel>
                        </div>
                       
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
                            ShowHeader="false" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
                            GridLines="None" OnPageIndexChanging="OnPageIndexChanging" PageSize="10"
                             EmptyDataText="Your search did not match any documents" 
                            onrowdatabound="GridView1_RowDataBound">
                            <RowStyle BorderStyle="None" BorderColor="White" />
                            <HeaderStyle BorderStyle="None" BorderColor="White" />
                            <Columns>
                                <asp:BoundField DataField="DisplayData" HtmlEncode="false" SortExpression="" HeaderText="" />
                            </Columns>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>

            </div>
        </div>
        <div style="float: left; width: 190px; height: auto; display: table;">
             <div class="expBox1R" style="margin-bottom: 4px; margin-top: 10px;">
                <div class="LibTop">
                    &nbsp;
                </div>
                
                <div class="LibContent" style="padding: 6px;">
                    <div class="LibTxt">
                        <a href="login.aspx?ReturnUrl=modules/intro.aspx" >
                        <img src="images/Puzzle_icon.png" decoration: "none" 
                        alt="puzzle icon" width="55" style="margin-right: 10px;margin-bottom: 10px; 
                        float: left;" border="0" />
                        </a>
                        <span style="color: #1d5ba9; font-size: 1.4em; line-height: 20px;">
                            Theme Integration Course
                        </span>
                        <p style="margin-top: 15px; margin-bottom: 0;">
                             MSAP grantees can use this online course to support schoolwide work... <br/><a href="login.aspx?ReturnUrl=modules/intro.aspx">Learn More</a>
                        </p>
                    </div>
                    <!-- /LibTxt -->
                </div>
                <!-- /LibContent -->
                <div class="LibBot">
                    &nbsp;
                </div>
		  </div>
            <!--Box 2 -->
          <div class="expBoxR" style="background: #ffb900; margin-bottom: 4px; margin-top: 10px;">
                <div class="topCall">
                    &nbsp;</div>
                <div class="midCall">
                    <div class="expTxt">
                        <img src="images/bus_icon.gif" alt="school bus icon" style="float: left; margin-right: 8px;" /><span
                            class="expHdr" style="color: #F60;">Important Links</span><br />
                        <br />
                        <p class="linkP">
                            <a href="links.aspx">Equity Assistance Centers</a></p>
                        <p class="linkP">
                            <a href="http://www.magnet.edu" target="_blank">Magnet Schools of America</a>
                        </p>
                        <p class="linkP">
                            <a href="http://www.nationaltitleiassociation.org/" target="_blank">National Title I
                                Association</a>
                        </p>
                        <p class="linkP">
                            <a href="http://www2.ed.gov/about/offices/list/ocr/index.html" target="_blank">Office for Civil Rights</a>
                        </p>
                        <p class="linkP">
                            <a href="importantlinks.aspx">See more important links</a>
                        </p>
                    </div>
                </div>
                <div class="botCall">
                    &nbsp;</div>
          </div>
      </div>
    </div>
    <asp:SqlDataSource ID="dsPulicationTopic" runat="server" 
    ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
    SelectCommand="SELECT * FROM [MagnetPublicationSubTopic] order by topicname"></asp:SqlDataSource>
</asp:Content>
