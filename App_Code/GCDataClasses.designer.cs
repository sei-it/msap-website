﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;



[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Magnet_dev")]
public partial class GCDataClassesDataContext : System.Data.Linq.DataContext
{
	
	private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
	
  #region Extensibility Method Definitions
  partial void OnCreated();
  partial void InsertMagnetGCImage(MagnetGCImage instance);
  partial void UpdateMagnetGCImage(MagnetGCImage instance);
  partial void DeleteMagnetGCImage(MagnetGCImage instance);
  partial void InsertMagnetGC(MagnetGC instance);
  partial void UpdateMagnetGC(MagnetGC instance);
  partial void DeleteMagnetGC(MagnetGC instance);
  #endregion
	
	public GCDataClassesDataContext() : 
			base(global::System.Configuration.ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString, mappingSource)
	{
		OnCreated();
	}
	
	public GCDataClassesDataContext(string connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public GCDataClassesDataContext(System.Data.IDbConnection connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public GCDataClassesDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public GCDataClassesDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public System.Data.Linq.Table<MagnetGCImage> MagnetGCImages
	{
		get
		{
			return this.GetTable<MagnetGCImage>();
		}
	}
	
	public System.Data.Linq.Table<MagnetGC> MagnetGCs
	{
		get
		{
			return this.GetTable<MagnetGC>();
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.MagnetGCImages")]
public partial class MagnetGCImage : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _id;
	
	private System.Nullable<int> _gc_id;
	
	private System.Data.Linq.Binary _image;
	
	private string _caption;
	
	private string _alt_text;
	
	private bool _isShowOnHomePage;
	
	private bool _isArchiveImage;
	
	private System.Nullable<System.DateTime> _CreateOn;
	
	private string _CreatedBy;
	
	private int _displayOrder;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void Ongc_idChanging(System.Nullable<int> value);
    partial void Ongc_idChanged();
    partial void OnimageChanging(System.Data.Linq.Binary value);
    partial void OnimageChanged();
    partial void OncaptionChanging(string value);
    partial void OncaptionChanged();
    partial void Onalt_textChanging(string value);
    partial void Onalt_textChanged();
    partial void OnisShowOnHomePageChanging(bool value);
    partial void OnisShowOnHomePageChanged();
    partial void OnisArchiveImageChanging(bool value);
    partial void OnisArchiveImageChanged();
    partial void OnCreateOnChanging(System.Nullable<System.DateTime> value);
    partial void OnCreateOnChanged();
    partial void OnCreatedByChanging(string value);
    partial void OnCreatedByChanged();
    partial void OndisplayOrderChanging(int value);
    partial void OndisplayOrderChanged();
    #endregion
	
	public MagnetGCImage()
	{
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int id
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((this._id != value))
			{
				this.OnidChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("id");
				this.OnidChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_gc_id", DbType="Int")]
	public System.Nullable<int> gc_id
	{
		get
		{
			return this._gc_id;
		}
		set
		{
			if ((this._gc_id != value))
			{
				this.Ongc_idChanging(value);
				this.SendPropertyChanging();
				this._gc_id = value;
				this.SendPropertyChanged("gc_id");
				this.Ongc_idChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_image", DbType="Image", CanBeNull=true, UpdateCheck=UpdateCheck.Never)]
	public System.Data.Linq.Binary image
	{
		get
		{
			return this._image;
		}
		set
		{
			if ((this._image != value))
			{
				this.OnimageChanging(value);
				this.SendPropertyChanging();
				this._image = value;
				this.SendPropertyChanged("image");
				this.OnimageChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_caption", DbType="NVarChar(3000)")]
	public string caption
	{
		get
		{
			return this._caption;
		}
		set
		{
			if ((this._caption != value))
			{
				this.OncaptionChanging(value);
				this.SendPropertyChanging();
				this._caption = value;
				this.SendPropertyChanged("caption");
				this.OncaptionChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_alt_text", DbType="NVarChar(500)")]
	public string alt_text
	{
		get
		{
			return this._alt_text;
		}
		set
		{
			if ((this._alt_text != value))
			{
				this.Onalt_textChanging(value);
				this.SendPropertyChanging();
				this._alt_text = value;
				this.SendPropertyChanged("alt_text");
				this.Onalt_textChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isShowOnHomePage", DbType="Bit NOT NULL")]
	public bool isShowOnHomePage
	{
		get
		{
			return this._isShowOnHomePage;
		}
		set
		{
			if ((this._isShowOnHomePage != value))
			{
				this.OnisShowOnHomePageChanging(value);
				this.SendPropertyChanging();
				this._isShowOnHomePage = value;
				this.SendPropertyChanged("isShowOnHomePage");
				this.OnisShowOnHomePageChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isArchiveImage", DbType="Bit NOT NULL")]
	public bool isArchiveImage
	{
		get
		{
			return this._isArchiveImage;
		}
		set
		{
			if ((this._isArchiveImage != value))
			{
				this.OnisArchiveImageChanging(value);
				this.SendPropertyChanging();
				this._isArchiveImage = value;
				this.SendPropertyChanged("isArchiveImage");
				this.OnisArchiveImageChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreateOn", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreateOn
	{
		get
		{
			return this._CreateOn;
		}
		set
		{
			if ((this._CreateOn != value))
			{
				this.OnCreateOnChanging(value);
				this.SendPropertyChanging();
				this._CreateOn = value;
				this.SendPropertyChanged("CreateOn");
				this.OnCreateOnChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="NVarChar(50)")]
	public string CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_displayOrder", DbType="Int NOT NULL")]
	public int displayOrder
	{
		get
		{
			return this._displayOrder;
		}
		set
		{
			if ((this._displayOrder != value))
			{
				this.OndisplayOrderChanging(value);
				this.SendPropertyChanging();
				this._displayOrder = value;
				this.SendPropertyChanged("displayOrder");
				this.OndisplayOrderChanged();
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.MagnetGC")]
public partial class MagnetGC : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _id;
	
	private string _Title;
	
	private string _Contents;
	
	private string _abs_contents;
	
	private System.DateTime _CreatedOn;
	
	private string _Createdby;
	
	private System.Nullable<System.DateTime> _PublishedDate;
	
	private bool _isActive;
	
	private string _Cohort;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnTitleChanging(string value);
    partial void OnTitleChanged();
    partial void OnContentsChanging(string value);
    partial void OnContentsChanged();
    partial void Onabs_contentsChanging(string value);
    partial void Onabs_contentsChanged();
    partial void OnCreatedOnChanging(System.DateTime value);
    partial void OnCreatedOnChanged();
    partial void OnCreatedbyChanging(string value);
    partial void OnCreatedbyChanged();
    partial void OnPublishedDateChanging(System.Nullable<System.DateTime> value);
    partial void OnPublishedDateChanged();
    partial void OnisActiveChanging(bool value);
    partial void OnisActiveChanged();
    partial void OnCohortChanging(string value);
    partial void OnCohortChanged();
    #endregion
	
	public MagnetGC()
	{
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int id
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((this._id != value))
			{
				this.OnidChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("id");
				this.OnidChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Title", DbType="NVarChar(1000)")]
	public string Title
	{
		get
		{
			return this._Title;
		}
		set
		{
			if ((this._Title != value))
			{
				this.OnTitleChanging(value);
				this.SendPropertyChanging();
				this._Title = value;
				this.SendPropertyChanged("Title");
				this.OnTitleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Contents", DbType="NVarChar(MAX)")]
	public string Contents
	{
		get
		{
			return this._Contents;
		}
		set
		{
			if ((this._Contents != value))
			{
				this.OnContentsChanging(value);
				this.SendPropertyChanging();
				this._Contents = value;
				this.SendPropertyChanged("Contents");
				this.OnContentsChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_abs_contents", DbType="NVarChar(MAX)")]
	public string abs_contents
	{
		get
		{
			return this._abs_contents;
		}
		set
		{
			if ((this._abs_contents != value))
			{
				this.Onabs_contentsChanging(value);
				this.SendPropertyChanging();
				this._abs_contents = value;
				this.SendPropertyChanged("abs_contents");
				this.Onabs_contentsChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedOn", DbType="DateTime NOT NULL")]
	public System.DateTime CreatedOn
	{
		get
		{
			return this._CreatedOn;
		}
		set
		{
			if ((this._CreatedOn != value))
			{
				this.OnCreatedOnChanging(value);
				this.SendPropertyChanging();
				this._CreatedOn = value;
				this.SendPropertyChanged("CreatedOn");
				this.OnCreatedOnChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Createdby", DbType="NVarChar(50)")]
	public string Createdby
	{
		get
		{
			return this._Createdby;
		}
		set
		{
			if ((this._Createdby != value))
			{
				this.OnCreatedbyChanging(value);
				this.SendPropertyChanging();
				this._Createdby = value;
				this.SendPropertyChanged("Createdby");
				this.OnCreatedbyChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PublishedDate", DbType="DateTime")]
	public System.Nullable<System.DateTime> PublishedDate
	{
		get
		{
			return this._PublishedDate;
		}
		set
		{
			if ((this._PublishedDate != value))
			{
				this.OnPublishedDateChanging(value);
				this.SendPropertyChanging();
				this._PublishedDate = value;
				this.SendPropertyChanged("PublishedDate");
				this.OnPublishedDateChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isActive", DbType="Bit NOT NULL")]
	public bool isActive
	{
		get
		{
			return this._isActive;
		}
		set
		{
			if ((this._isActive != value))
			{
				this.OnisActiveChanging(value);
				this.SendPropertyChanging();
				this._isActive = value;
				this.SendPropertyChanged("isActive");
				this.OnisActiveChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Cohort", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
	public string Cohort
	{
		get
		{
			return this._Cohort;
		}
		set
		{
			if ((this._Cohort != value))
			{
				this.OnCohortChanging(value);
				this.SendPropertyChanging();
				this._Cohort = value;
				this.SendPropertyChanged("Cohort");
				this.OnCohortChanged();
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
#pragma warning restore 1591
