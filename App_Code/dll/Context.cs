


using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using SubSonic.DataProviders;
using SubSonic.Extensions;
using SubSonic.Linq.Structure;
using SubSonic.Query;
using SubSonic.Schema;
using System.Data.Common;
using System.Collections.Generic;

namespace Synergy.Magnet
{
    public partial class MagnetDBDB : IQuerySurface
    {

        public IDataProvider DataProvider;
        public DbQueryProvider provider;
        
        public static IDataProvider DefaultDataProvider { get; set; }

        public bool TestMode
		{
            get
			{
                return DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public MagnetDBDB() 
        {
            if (DefaultDataProvider == null) {
                DataProvider = ProviderFactory.GetProvider("MagnetServer");
            }
            else {
                DataProvider = DefaultDataProvider;
            }
            Init();
        }

        public MagnetDBDB(string connectionStringName)
        {
            DataProvider = ProviderFactory.GetProvider(connectionStringName);
            Init();
        }

		public MagnetDBDB(string connectionString, string providerName)
        {
            DataProvider = ProviderFactory.GetProvider(connectionString,providerName);
            Init();
        }

		public ITable FindByPrimaryKey(string pkName)
        {
            return DataProvider.Schema.Tables.SingleOrDefault(x => x.PrimaryKey.Name.Equals(pkName, StringComparison.InvariantCultureIgnoreCase));
        }

        public Query<T> GetQuery<T>()
        {
            return new Query<T>(provider);
        }
        
        public ITable FindTable(string tableName)
        {
            return DataProvider.FindTable(tableName);
        }
               
        public IDataProvider Provider
        {
            get { return DataProvider; }
            set {DataProvider=value;}
        }
        
        public DbQueryProvider QueryProvider
        {
            get { return provider; }
        }
        
        BatchQuery _batch = null;
        public void Queue<T>(IQueryable<T> qry)
        {
            if (_batch == null)
                _batch = new BatchQuery(Provider, QueryProvider);
            _batch.Queue(qry);
        }

        public void Queue(ISqlQuery qry)
        {
            if (_batch == null)
                _batch = new BatchQuery(Provider, QueryProvider);
            _batch.Queue(qry);
        }

        public void ExecuteTransaction(IList<DbCommand> commands)
		{
            if(!TestMode)
			{
                using(var connection = commands[0].Connection)
				{
                   if (connection.State == ConnectionState.Closed)
                        connection.Open();
                   
                   using (var trans = connection.BeginTransaction()) 
				   {
                        foreach (var cmd in commands) 
						{
                            cmd.Transaction = trans;
                            cmd.Connection = connection;
                            cmd.ExecuteNonQuery();
                        }
                        trans.Commit();
                    }
                    connection.Close();
                }
            }
        }

        public IDataReader ExecuteBatch()
        {
            if (_batch == null)
                throw new InvalidOperationException("There's nothing in the queue");
            if(!TestMode)
                return _batch.ExecuteReader();
            return null;
        }
			
        public Query<yaf_UserGroup> yaf_UserGroups { get; set; }
        public Query<yaf_Rank> yaf_Ranks { get; set; }
        public Query<TALogDropdownList> TALogDropdownLists { get; set; }
        public Query<STEMBlog> STEMBlogs { get; set; }
        public Query<aspnet_User> aspnet_Users { get; set; }
        public Query<STEMBlogComment> STEMBlogComments { get; set; }
        public Query<yaf_AccessMask> yaf_AccessMasks { get; set; }
        public Query<MagnetTWGMember> MagnetTWGMembers { get; set; }
        public Query<TALogEDStaff> TALogEDStaffs { get; set; }
        public Query<STEMNews> STEMNews { get; set; }
        public Query<MagnetState> MagnetStates { get; set; }
        public Query<STEMResource> STEMResources { get; set; }
        public Query<yaf_UserForum> yaf_UserForums { get; set; }
        public Query<MagnetContext> MagnetContexts { get; set; }
        public Query<MagnetPublicationSubTopic> MagnetPublicationSubTopics { get; set; }
        public Query<yaf_Board> yaf_Boards { get; set; }
        public Query<STEMTopic> STEMTopics { get; set; }
        public Query<yaf_NntpServer> yaf_NntpServers { get; set; }
        public Query<MagnetRGIObjective> MagnetRGIObjectives { get; set; }
        public Query<yaf_NntpForum> yaf_NntpForums { get; set; }
        public Query<aspnet_SchemaVersion> aspnet_SchemaVersions { get; set; }
        public Query<MagnetSchoolEnrollment> MagnetSchoolEnrollments { get; set; }
        public Query<yaf_NntpTopic> yaf_NntpTopics { get; set; }
        public Query<MagnetRGIDefinition> MagnetRGIDefinitions { get; set; }
        public Query<MagnetTARequest> MagnetTARequests { get; set; }
        public Query<yaf_UserPMessage> yaf_UserPMessages { get; set; }
        public Query<MagnetMapDataold> MagnetMapDataolds { get; set; }
        public Query<MagnetHQHSSStrategy> MagnetHQHSSStrategies { get; set; }
        public Query<MagnetGPRA> MagnetGPRAs { get; set; }
        public Query<MagnetSchoolFeederEnrollment> MagnetSchoolFeederEnrollments { get; set; }
        public Query<yaf_Replace_Word> yaf_Replace_Words { get; set; }
        public Query<TALogMSAPStaff> TALogMSAPStaffs { get; set; }
        public Query<TAParentEngagementSubCat> TAParentEngagementSubCats { get; set; }
        public Query<yaf_Registry> yaf_Registries { get; set; }
        public Query<MagnetApplicationPool> MagnetApplicationPools { get; set; }
        public Query<yaf_EventLog> yaf_EventLogs { get; set; }
        public Query<TAParentEngagementCat> TAParentEngagementCats { get; set; }
        public Query<MagnetAdHocReview> MagnetAdHocReviews { get; set; }
        public Query<yaf_Extension> yaf_Extensions { get; set; }
        public Query<aspnet_Membership> aspnet_Memberships { get; set; }
        public Query<yaf_BBCode> yaf_BBCodes { get; set; }
        public Query<MagnetBudgetInformation> MagnetBudgetInformations { get; set; }
        public Query<yaf_Medal> yaf_Medals { get; set; }
        public Query<TAThemeCat> TAThemeCats { get; set; }
        public Query<MagnetPublication> MagnetPublications { get; set; }
        public Query<TAPartnership> TAPartnerships { get; set; }
        public Query<TAThemeSubCat> TAThemeSubCats { get; set; }
        public Query<MagnetUserGuide> MagnetUserGuides { get; set; }
        public Query<MagnetReportCompletion> MagnetReportCompletions { get; set; }
        public Query<yaf_GroupMedal> yaf_GroupMedals { get; set; }
        public Query<MagnetCarousel> MagnetCarousels { get; set; }
        public Query<MagnetUpload> MagnetUploads { get; set; }
        public Query<yaf_UserMedal> yaf_UserMedals { get; set; }
        public Query<TALogFollowUp> TALogFollowUps { get; set; }
        public Query<MagnetAdminUpload> MagnetAdminUploads { get; set; }
        public Query<yaf_IgnoreUser> yaf_IgnoreUsers { get; set; }
        public Query<yaf_ShoutboxMessage> yaf_ShoutboxMessages { get; set; }
        public Query<APRGuidance> APRGuidances { get; set; }
        public Query<MagnetGPRAImprovementStatus> MagnetGPRAImprovementStatuses { get; set; }
        public Query<MagnetGPRAPerformanceMeasure> MagnetGPRAPerformanceMeasures { get; set; }
        public Query<aspnet_Profile> aspnet_Profiles { get; set; }
        public Query<MagnetUpdateHistory> MagnetUpdateHistories { get; set; }
        public Query<APRGuidanceCategory> APRGuidanceCategories { get; set; }
        public Query<MagnetResourceCategory> MagnetResourceCategories { get; set; }
        public Query<MagnetGPRA6> MagnetGPRA6s { get; set; }
        public Query<TAGranteeStatus> TAGranteeStatuses { get; set; }
        public Query<APRGuidanceUploadFile> APRGuidanceUploadFiles { get; set; }
        public Query<TALogActParticipation> TALogActParticipations { get; set; }
        public Query<magnetCompass> magnetCompasses { get; set; }
        public Query<MagnetSchool> MagnetSchools { get; set; }
        public Query<aspnet_Role> aspnet_Roles { get; set; }
        public Query<TALogCommunication> TALogCommunications { get; set; }
        public Query<aspnet_UsersInRole> aspnet_UsersInRoles { get; set; }
        public Query<TALogContact> TALogContacts { get; set; }
        public Query<Lk_WebinarRating> Lk_WebinarRatings { get; set; }
        public Query<TALogEventActivity> TALogEventActivities { get; set; }
        public Query<MagnetMailList> MagnetMailLists { get; set; }
        public Query<WebinarEvaluation> WebinarEvaluations { get; set; }
        public Query<MagnetPrivateEventDetail> MagnetPrivateEventDetails { get; set; }
        public Query<PastedWebinar> PastedWebinars { get; set; }
        public Query<TATypeComm> TATypeComms { get; set; }
        public Query<MagnetPrivateEvent> MagnetPrivateEvents { get; set; }
        public Query<MagnetFringeBudget> MagnetFringeBudgets { get; set; }
        public Query<MagnetGrantPerformance> MagnetGrantPerformances { get; set; }
        public Query<TATypeCommMEthed> TATypeCommMEtheds { get; set; }
        public Query<MagnetPrivateEventDetailUpload> MagnetPrivateEventDetailUploads { get; set; }
        public Query<PastedWebinarsCategory> PastedWebinarsCategories { get; set; }
        public Query<MagnetTravelBudget> MagnetTravelBudgets { get; set; }
        public Query<TATypeDCC> TATypeDCCs { get; set; }
        public Query<PastedWebinarUploadFile> PastedWebinarUploadFiles { get; set; }
        public Query<TALogMassComm> TALogMassComms { get; set; }
        public Query<MagnetEquipmentBudget> MagnetEquipmentBudgets { get; set; }
        public Query<WebinarEvaluation_NavigatingMAP> WebinarEvaluation_NavigatingMAPs { get; set; }
        public Query<TATypeGenComm> TATypeGenComms { get; set; }
        public Query<aspnet_Path> aspnet_Paths { get; set; }
        public Query<TATypeReportComm> TATypeReportComms { get; set; }
        public Query<MagnetHomepageEvent> MagnetHomepageEvents { get; set; }
        public Query<tempobjectiveid> tempobjectiveids { get; set; }
        public Query<TALogNGFollowUp> TALogNGFollowUps { get; set; }
        public Query<MagnetRenovationBudget> MagnetRenovationBudgets { get; set; }
        public Query<MagnetConsultantBudget> MagnetConsultantBudgets { get; set; }
        public Query<WebinarEvaluation_AligningProjectActivity> WebinarEvaluation_AligningProjectActivities { get; set; }
        public Query<TALogNoneGrantee> TALogNoneGrantees { get; set; }
        public Query<MagnetContractsBudget> MagnetContractsBudgets { get; set; }
        public Query<aspnet_PersonalizationAllUser> aspnet_PersonalizationAllUsers { get; set; }
        public Query<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUsers { get; set; }
        public Query<MagnetConsultant> MagnetConsultants { get; set; }
        public Query<TALogProgramOffer> TALogProgramOffers { get; set; }
        public Query<MagnetBudgetSummary> MagnetBudgetSummaries { get; set; }
        public Query<MagnetSuppliesBudget> MagnetSuppliesBudgets { get; set; }
        public Query<Webinar_Evaluation_Sustainability3> Webinar_Evaluation_Sustainability3s { get; set; }
        public Query<TALogRole> TALogRoles { get; set; }
        public Query<MagnetOtherBudget> MagnetOtherBudgets { get; set; }
        public Query<MagnetGranteeDatum> MagnetGranteeData { get; set; }
        public Query<TALogUploadFile> TALogUploadFiles { get; set; }
        public Query<GranteePerformanceCoverSheet> GranteePerformanceCoverSheets { get; set; }
        public Query<Calendar> Calendars { get; set; }
        public Query<Webinar_Evaluation_PuttingItAllTogether> Webinar_Evaluation_PuttingItAllTogethers { get; set; }
        public Query<TCalladtParticipant> TCalladtParticipants { get; set; }
        public Query<MagnetGranteeUserDistrict> MagnetGranteeUserDistricts { get; set; }
        public Query<MagnetGroupMember> MagnetGroupMembers { get; set; }
        public Query<MagnetSIP> MagnetSIPs { get; set; }
        public Query<TCallamendment> TCallamendments { get; set; }
        public Query<MagnetWebinar> MagnetWebinars { get; set; }
        public Query<aspnet_WebEvent_Event> aspnet_WebEvent_Events { get; set; }
        public Query<TCalladtPersonnel> TCalladtPersonnels { get; set; }
        public Query<MagnetGranteeContact> MagnetGranteeContacts { get; set; }
        public Query<yaf_Active> yaf_Actives { get; set; }
        public Query<MagnetAYP> MagnetAYPs { get; set; }
        public Query<yaf_BannedIP> yaf_BannedIPs { get; set; }
        public Query<yaf_Category> yaf_Categories { get; set; }
        public Query<MagnetGC> MagnetGCs { get; set; }
        public Query<TCallDoc> TCallDocs { get; set; }
        public Query<MagnetEnrollment> MagnetEnrollments { get; set; }
        public Query<yaf_CheckEmail> yaf_CheckEmails { get; set; }
        public Query<yaf_Choice> yaf_Choices { get; set; }
        public Query<MagnetGCImage> MagnetGCImages { get; set; }
        public Query<MagnetDataReportInfo> MagnetDataReportInfos { get; set; }
        public Query<yaf_PollVote> yaf_PollVotes { get; set; }
        public Query<yaf_Forum> yaf_Forums { get; set; }
        public Query<TCallKeyPersonnel> TCallKeyPersonnels { get; set; }
        public Query<MagnetSchoolStatus> MagnetSchoolStatuses { get; set; }
        public Query<EDConfirmation> EDConfirmations { get; set; }
        public Query<yaf_ForumAccess> yaf_ForumAccesses { get; set; }
        public Query<yaf_Group> yaf_Groups { get; set; }
        public Query<MagnetDLL> MagnetDLLs { get; set; }
        public Query<TCallMain> TCallMains { get; set; }
        public Query<yaf_Mail> yaf_Mails { get; set; }
        public Query<MagnetGranteeUser> MagnetGranteeUsers { get; set; }
        public Query<yaf_Message> yaf_Messages { get; set; }
        public Query<MagnetPublicEvent> MagnetPublicEvents { get; set; }
        public Query<MagnetReportPeriod> MagnetReportPeriods { get; set; }
        public Query<TCallParticipant> TCallParticipants { get; set; }
        public Query<yaf_MessageReported> yaf_MessageReporteds { get; set; }
        public Query<yaf_MessageReportedAudit> yaf_MessageReportedAudits { get; set; }
        public Query<GranteeReport> GranteeReports { get; set; }
        public Query<yaf_PMessage> yaf_PMessages { get; set; }
        public Query<MagnetGrantee> MagnetGrantees { get; set; }
        public Query<MagnetmapDatum> MagnetmapData { get; set; }
        public Query<UserTracker> UserTrackers { get; set; }
        public Query<TCallSchool> TCallSchools { get; set; }
        public Query<MagnetProjectObjective> MagnetProjectObjectives { get; set; }
        public Query<yaf_Poll> yaf_Polls { get; set; }
        public Query<yaf_Smiley> yaf_Smileys { get; set; }
        public Query<MagnetSchoolYear> MagnetSchoolYears { get; set; }
        public Query<yaf_prov_Membership> yaf_prov_Memberships { get; set; }
        public Query<yaf_Topic> yaf_Topics { get; set; }
        public Query<MagnetDesegregationPlan> MagnetDesegregationPlans { get; set; }
        public Query<MagnetFeederSchool> MagnetFeederSchools { get; set; }
        public Query<yaf_prov_Application> yaf_prov_Applications { get; set; }
        public Query<yaf_User> yaf_Users { get; set; }
        public Query<MagnetPersonnelBudget> MagnetPersonnelBudgets { get; set; }
        public Query<yaf_prov_Profile> yaf_prov_Profiles { get; set; }
        public Query<MagnetSurvey> MagnetSurveys { get; set; }
        public Query<yaf_prov_Role> yaf_prov_Roles { get; set; }
        public Query<yaf_WatchForum> yaf_WatchForums { get; set; }
        public Query<aspnet_Application> aspnet_Applications { get; set; }
        public Query<yaf_prov_RoleMembership> yaf_prov_RoleMemberships { get; set; }
        public Query<yaf_WatchTopic> yaf_WatchTopics { get; set; }
        public Query<MagnetSurveyUser> MagnetSurveyUsers { get; set; }
        public Query<yaf_Attachment> yaf_Attachments { get; set; }
        public Query<MagnetNews> MagnetNews { get; set; }
        public Query<State> States { get; set; }

			

        #region ' Aggregates and SubSonic Queries '
        public Select SelectColumns(params string[] columns)
        {
            return new Select(DataProvider, columns);
        }

        public Select Select
        {
            get { return new Select(this.Provider); }
        }

        public Insert Insert
		{
            get { return new Insert(this.Provider); }
        }

        public Update<T> Update<T>() where T:new()
		{
            return new Update<T>(this.Provider);
        }

        public SqlQuery Delete<T>(Expression<Func<T,bool>> column) where T:new()
        {
            LambdaExpression lamda = column;
            SqlQuery result = new Delete<T>(this.Provider);
            result = result.From<T>();
            result.Constraints=lamda.ParseConstraints().ToList();
            return result;
        }

        public SqlQuery Max<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = DataProvider.FindTable(objectName).Name;
            return new Select(DataProvider, new Aggregate(colName, AggregateFunction.Max)).From(tableName);
        }

        public SqlQuery Min<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Min)).From(tableName);
        }

        public SqlQuery Sum<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Sum)).From(tableName);
        }

        public SqlQuery Avg<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Avg)).From(tableName);
        }

        public SqlQuery Count<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Count)).From(tableName);
        }

        public SqlQuery Variance<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Var)).From(tableName);
        }

        public SqlQuery StandardDeviation<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.StDev)).From(tableName);
        }

        #endregion

        void Init()
        {
            provider = new DbQueryProvider(this.Provider);

            #region ' Query Defs '
            yaf_UserGroups = new Query<yaf_UserGroup>(provider);
            yaf_Ranks = new Query<yaf_Rank>(provider);
            TALogDropdownLists = new Query<TALogDropdownList>(provider);
            STEMBlogs = new Query<STEMBlog>(provider);
            aspnet_Users = new Query<aspnet_User>(provider);
            STEMBlogComments = new Query<STEMBlogComment>(provider);
            yaf_AccessMasks = new Query<yaf_AccessMask>(provider);
            MagnetTWGMembers = new Query<MagnetTWGMember>(provider);
            TALogEDStaffs = new Query<TALogEDStaff>(provider);
            STEMNews = new Query<STEMNews>(provider);
            MagnetStates = new Query<MagnetState>(provider);
            STEMResources = new Query<STEMResource>(provider);
            yaf_UserForums = new Query<yaf_UserForum>(provider);
            MagnetContexts = new Query<MagnetContext>(provider);
            MagnetPublicationSubTopics = new Query<MagnetPublicationSubTopic>(provider);
            yaf_Boards = new Query<yaf_Board>(provider);
            STEMTopics = new Query<STEMTopic>(provider);
            yaf_NntpServers = new Query<yaf_NntpServer>(provider);
            MagnetRGIObjectives = new Query<MagnetRGIObjective>(provider);
            yaf_NntpForums = new Query<yaf_NntpForum>(provider);
            aspnet_SchemaVersions = new Query<aspnet_SchemaVersion>(provider);
            MagnetSchoolEnrollments = new Query<MagnetSchoolEnrollment>(provider);
            yaf_NntpTopics = new Query<yaf_NntpTopic>(provider);
            MagnetRGIDefinitions = new Query<MagnetRGIDefinition>(provider);
            MagnetTARequests = new Query<MagnetTARequest>(provider);
            yaf_UserPMessages = new Query<yaf_UserPMessage>(provider);
            MagnetMapDataolds = new Query<MagnetMapDataold>(provider);
            MagnetHQHSSStrategies = new Query<MagnetHQHSSStrategy>(provider);
            MagnetGPRAs = new Query<MagnetGPRA>(provider);
            MagnetSchoolFeederEnrollments = new Query<MagnetSchoolFeederEnrollment>(provider);
            yaf_Replace_Words = new Query<yaf_Replace_Word>(provider);
            TALogMSAPStaffs = new Query<TALogMSAPStaff>(provider);
            TAParentEngagementSubCats = new Query<TAParentEngagementSubCat>(provider);
            yaf_Registries = new Query<yaf_Registry>(provider);
            MagnetApplicationPools = new Query<MagnetApplicationPool>(provider);
            yaf_EventLogs = new Query<yaf_EventLog>(provider);
            TAParentEngagementCats = new Query<TAParentEngagementCat>(provider);
            MagnetAdHocReviews = new Query<MagnetAdHocReview>(provider);
            yaf_Extensions = new Query<yaf_Extension>(provider);
            aspnet_Memberships = new Query<aspnet_Membership>(provider);
            yaf_BBCodes = new Query<yaf_BBCode>(provider);
            MagnetBudgetInformations = new Query<MagnetBudgetInformation>(provider);
            yaf_Medals = new Query<yaf_Medal>(provider);
            TAThemeCats = new Query<TAThemeCat>(provider);
            MagnetPublications = new Query<MagnetPublication>(provider);
            TAPartnerships = new Query<TAPartnership>(provider);
            TAThemeSubCats = new Query<TAThemeSubCat>(provider);
            MagnetUserGuides = new Query<MagnetUserGuide>(provider);
            MagnetReportCompletions = new Query<MagnetReportCompletion>(provider);
            yaf_GroupMedals = new Query<yaf_GroupMedal>(provider);
            MagnetCarousels = new Query<MagnetCarousel>(provider);
            MagnetUploads = new Query<MagnetUpload>(provider);
            yaf_UserMedals = new Query<yaf_UserMedal>(provider);
            TALogFollowUps = new Query<TALogFollowUp>(provider);
            MagnetAdminUploads = new Query<MagnetAdminUpload>(provider);
            yaf_IgnoreUsers = new Query<yaf_IgnoreUser>(provider);
            yaf_ShoutboxMessages = new Query<yaf_ShoutboxMessage>(provider);
            APRGuidances = new Query<APRGuidance>(provider);
            MagnetGPRAImprovementStatuses = new Query<MagnetGPRAImprovementStatus>(provider);
            MagnetGPRAPerformanceMeasures = new Query<MagnetGPRAPerformanceMeasure>(provider);
            aspnet_Profiles = new Query<aspnet_Profile>(provider);
            MagnetUpdateHistories = new Query<MagnetUpdateHistory>(provider);
            APRGuidanceCategories = new Query<APRGuidanceCategory>(provider);
            MagnetResourceCategories = new Query<MagnetResourceCategory>(provider);
            MagnetGPRA6s = new Query<MagnetGPRA6>(provider);
            TAGranteeStatuses = new Query<TAGranteeStatus>(provider);
            APRGuidanceUploadFiles = new Query<APRGuidanceUploadFile>(provider);
            TALogActParticipations = new Query<TALogActParticipation>(provider);
            magnetCompasses = new Query<magnetCompass>(provider);
            MagnetSchools = new Query<MagnetSchool>(provider);
            aspnet_Roles = new Query<aspnet_Role>(provider);
            TALogCommunications = new Query<TALogCommunication>(provider);
            aspnet_UsersInRoles = new Query<aspnet_UsersInRole>(provider);
            TALogContacts = new Query<TALogContact>(provider);
            Lk_WebinarRatings = new Query<Lk_WebinarRating>(provider);
            TALogEventActivities = new Query<TALogEventActivity>(provider);
            MagnetMailLists = new Query<MagnetMailList>(provider);
            WebinarEvaluations = new Query<WebinarEvaluation>(provider);
            MagnetPrivateEventDetails = new Query<MagnetPrivateEventDetail>(provider);
            PastedWebinars = new Query<PastedWebinar>(provider);
            TATypeComms = new Query<TATypeComm>(provider);
            MagnetPrivateEvents = new Query<MagnetPrivateEvent>(provider);
            MagnetFringeBudgets = new Query<MagnetFringeBudget>(provider);
            MagnetGrantPerformances = new Query<MagnetGrantPerformance>(provider);
            TATypeCommMEtheds = new Query<TATypeCommMEthed>(provider);
            MagnetPrivateEventDetailUploads = new Query<MagnetPrivateEventDetailUpload>(provider);
            PastedWebinarsCategories = new Query<PastedWebinarsCategory>(provider);
            MagnetTravelBudgets = new Query<MagnetTravelBudget>(provider);
            TATypeDCCs = new Query<TATypeDCC>(provider);
            PastedWebinarUploadFiles = new Query<PastedWebinarUploadFile>(provider);
            TALogMassComms = new Query<TALogMassComm>(provider);
            MagnetEquipmentBudgets = new Query<MagnetEquipmentBudget>(provider);
            WebinarEvaluation_NavigatingMAPs = new Query<WebinarEvaluation_NavigatingMAP>(provider);
            TATypeGenComms = new Query<TATypeGenComm>(provider);
            aspnet_Paths = new Query<aspnet_Path>(provider);
            TATypeReportComms = new Query<TATypeReportComm>(provider);
            MagnetHomepageEvents = new Query<MagnetHomepageEvent>(provider);
            tempobjectiveids = new Query<tempobjectiveid>(provider);
            TALogNGFollowUps = new Query<TALogNGFollowUp>(provider);
            MagnetRenovationBudgets = new Query<MagnetRenovationBudget>(provider);
            MagnetConsultantBudgets = new Query<MagnetConsultantBudget>(provider);
            WebinarEvaluation_AligningProjectActivities = new Query<WebinarEvaluation_AligningProjectActivity>(provider);
            TALogNoneGrantees = new Query<TALogNoneGrantee>(provider);
            MagnetContractsBudgets = new Query<MagnetContractsBudget>(provider);
            aspnet_PersonalizationAllUsers = new Query<aspnet_PersonalizationAllUser>(provider);
            aspnet_PersonalizationPerUsers = new Query<aspnet_PersonalizationPerUser>(provider);
            MagnetConsultants = new Query<MagnetConsultant>(provider);
            TALogProgramOffers = new Query<TALogProgramOffer>(provider);
            MagnetBudgetSummaries = new Query<MagnetBudgetSummary>(provider);
            MagnetSuppliesBudgets = new Query<MagnetSuppliesBudget>(provider);
            Webinar_Evaluation_Sustainability3s = new Query<Webinar_Evaluation_Sustainability3>(provider);
            TALogRoles = new Query<TALogRole>(provider);
            MagnetOtherBudgets = new Query<MagnetOtherBudget>(provider);
            MagnetGranteeData = new Query<MagnetGranteeDatum>(provider);
            TALogUploadFiles = new Query<TALogUploadFile>(provider);
            GranteePerformanceCoverSheets = new Query<GranteePerformanceCoverSheet>(provider);
            Calendars = new Query<Calendar>(provider);
            Webinar_Evaluation_PuttingItAllTogethers = new Query<Webinar_Evaluation_PuttingItAllTogether>(provider);
            TCalladtParticipants = new Query<TCalladtParticipant>(provider);
            MagnetGranteeUserDistricts = new Query<MagnetGranteeUserDistrict>(provider);
            MagnetGroupMembers = new Query<MagnetGroupMember>(provider);
            MagnetSIPs = new Query<MagnetSIP>(provider);
            TCallamendments = new Query<TCallamendment>(provider);
            MagnetWebinars = new Query<MagnetWebinar>(provider);
            aspnet_WebEvent_Events = new Query<aspnet_WebEvent_Event>(provider);
            TCalladtPersonnels = new Query<TCalladtPersonnel>(provider);
            MagnetGranteeContacts = new Query<MagnetGranteeContact>(provider);
            yaf_Actives = new Query<yaf_Active>(provider);
            MagnetAYPs = new Query<MagnetAYP>(provider);
            yaf_BannedIPs = new Query<yaf_BannedIP>(provider);
            yaf_Categories = new Query<yaf_Category>(provider);
            MagnetGCs = new Query<MagnetGC>(provider);
            TCallDocs = new Query<TCallDoc>(provider);
            MagnetEnrollments = new Query<MagnetEnrollment>(provider);
            yaf_CheckEmails = new Query<yaf_CheckEmail>(provider);
            yaf_Choices = new Query<yaf_Choice>(provider);
            MagnetGCImages = new Query<MagnetGCImage>(provider);
            MagnetDataReportInfos = new Query<MagnetDataReportInfo>(provider);
            yaf_PollVotes = new Query<yaf_PollVote>(provider);
            yaf_Forums = new Query<yaf_Forum>(provider);
            TCallKeyPersonnels = new Query<TCallKeyPersonnel>(provider);
            MagnetSchoolStatuses = new Query<MagnetSchoolStatus>(provider);
            EDConfirmations = new Query<EDConfirmation>(provider);
            yaf_ForumAccesses = new Query<yaf_ForumAccess>(provider);
            yaf_Groups = new Query<yaf_Group>(provider);
            MagnetDLLs = new Query<MagnetDLL>(provider);
            TCallMains = new Query<TCallMain>(provider);
            yaf_Mails = new Query<yaf_Mail>(provider);
            MagnetGranteeUsers = new Query<MagnetGranteeUser>(provider);
            yaf_Messages = new Query<yaf_Message>(provider);
            MagnetPublicEvents = new Query<MagnetPublicEvent>(provider);
            MagnetReportPeriods = new Query<MagnetReportPeriod>(provider);
            TCallParticipants = new Query<TCallParticipant>(provider);
            yaf_MessageReporteds = new Query<yaf_MessageReported>(provider);
            yaf_MessageReportedAudits = new Query<yaf_MessageReportedAudit>(provider);
            GranteeReports = new Query<GranteeReport>(provider);
            yaf_PMessages = new Query<yaf_PMessage>(provider);
            MagnetGrantees = new Query<MagnetGrantee>(provider);
            MagnetmapData = new Query<MagnetmapDatum>(provider);
            UserTrackers = new Query<UserTracker>(provider);
            TCallSchools = new Query<TCallSchool>(provider);
            MagnetProjectObjectives = new Query<MagnetProjectObjective>(provider);
            yaf_Polls = new Query<yaf_Poll>(provider);
            yaf_Smileys = new Query<yaf_Smiley>(provider);
            MagnetSchoolYears = new Query<MagnetSchoolYear>(provider);
            yaf_prov_Memberships = new Query<yaf_prov_Membership>(provider);
            yaf_Topics = new Query<yaf_Topic>(provider);
            MagnetDesegregationPlans = new Query<MagnetDesegregationPlan>(provider);
            MagnetFeederSchools = new Query<MagnetFeederSchool>(provider);
            yaf_prov_Applications = new Query<yaf_prov_Application>(provider);
            yaf_Users = new Query<yaf_User>(provider);
            MagnetPersonnelBudgets = new Query<MagnetPersonnelBudget>(provider);
            yaf_prov_Profiles = new Query<yaf_prov_Profile>(provider);
            MagnetSurveys = new Query<MagnetSurvey>(provider);
            yaf_prov_Roles = new Query<yaf_prov_Role>(provider);
            yaf_WatchForums = new Query<yaf_WatchForum>(provider);
            aspnet_Applications = new Query<aspnet_Application>(provider);
            yaf_prov_RoleMemberships = new Query<yaf_prov_RoleMembership>(provider);
            yaf_WatchTopics = new Query<yaf_WatchTopic>(provider);
            MagnetSurveyUsers = new Query<MagnetSurveyUser>(provider);
            yaf_Attachments = new Query<yaf_Attachment>(provider);
            MagnetNews = new Query<MagnetNews>(provider);
            States = new Query<State>(provider);
            #endregion


            #region ' Schemas '
        	if(DataProvider.Schema.Tables.Count == 0)
			{
            	DataProvider.Schema.Tables.Add(new yaf_UserGroupTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_RankTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogDropdownListsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new STEMBlogTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_UsersTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new STEMBlogCommentsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_AccessMaskTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetTWGMembersTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogEDStaffTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new STEMNewsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetStatesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new STEMResourcesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_UserForumTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetContextsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetPublicationSubTopicTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_BoardTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new STEMTopicsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_NntpServerTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetRGIObjectivesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_NntpForumTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_SchemaVersionsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSchoolEnrollmentsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_NntpTopicTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetRGIDefinitionsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetTARequestsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_UserPMessageTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetMapDataoldTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetHQHSSStrategyTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGPRATable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSchoolFeederEnrollmentTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_Replace_WordsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogMSAPStaffTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TAParentEngagementSubCatTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_RegistryTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetApplicationPoolsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_EventLogTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TAParentEngagementCatTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetAdHocReviewTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_ExtensionTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_MembershipTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_BBCodeTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetBudgetInformationTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_MedalTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TAThemeCatTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetPublicationsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TAPartnershipTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TAThemeSubCatTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetUserGuidesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetReportCompletionsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_GroupMedalTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetCarouselTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetUploadsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_UserMedalTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogFollowUpTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetAdminUploadsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_IgnoreUserTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_ShoutboxMessageTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new APRGuidanceTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGPRAImprovementStatusTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGPRAPerformanceMeasuresTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_ProfileTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetUpdateHistoryTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new APRGuidanceCategoryTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetResourceCategoryTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGPRA6Table(DataProvider));
            	DataProvider.Schema.Tables.Add(new TAGranteeStatusTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new APRGuidanceUploadFilesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogActParticipationTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new magnetCompassTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSchoolsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_RolesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogCommunicationsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_UsersInRolesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogContactsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new Lk_WebinarRatingTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogEventActivityTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetMailListTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new WebinarEvaluationTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetPrivateEventDetailTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new PastedWebinarsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TATypeCommTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetPrivateEventsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetFringeBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGrantPerformancesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TATypeCommMEthedTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetPrivateEventDetailUploadsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new PastedWebinarsCategoryTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetTravelBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TATypeDCCTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new PastedWebinarUploadFilesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogMassCommTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetEquipmentBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new WebinarEvaluation_NavigatingMAPSTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TATypeGenCommTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_PathsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TATypeReportCommTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetHomepageEventsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new tempobjectiveidsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogNGFollowUpTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetRenovationBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetConsultantBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new WebinarEvaluation_AligningProjectActivitiesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogNoneGranteesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetContractsBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_PersonalizationAllUsersTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_PersonalizationPerUserTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetConsultantsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogProgramOfferTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetBudgetSummaryTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSuppliesBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new Webinar_Evaluation_Sustainability3Table(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogRolesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetOtherBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGranteeDataTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TALogUploadFilesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new GranteePerformanceCoverSheetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new CalendarsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new Webinar_Evaluation_PuttingItAllTogetherTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCalladtParticipantTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGranteeUserDistrictsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGroupMembersTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSIPsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCallamendmentsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetWebinarsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_WebEvent_EventsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCalladtPersonnelTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGranteeContactsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_ActiveTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetAYPsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_BannedIPTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_CategoryTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGCTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCallDocsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetEnrollmentsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_CheckEmailTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_ChoiceTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGCImagesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetDataReportInfoTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_PollVoteTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_ForumTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCallKeyPersonnelTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSchoolStatusTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new EDConfirmationTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_ForumAccessTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_GroupTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetDLLTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCallMainTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_MailTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGranteeUsersTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_MessageTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetPublicEventsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetReportPeriodsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCallParticipantsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_MessageReportedTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_MessageReportedAuditTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new GranteeReportsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_PMessageTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetGranteesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetmapDataTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new UserTrackerTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TCallSchoolsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetProjectObjectivesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_PollTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_SmileyTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSchoolYearsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_prov_MembershipTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_TopicTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetDesegregationPlanTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetFeederSchoolsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_prov_ApplicationTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_UserTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetPersonnelBudgetTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_prov_ProfileTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSurveysTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_prov_RoleTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_WatchForumTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new aspnet_ApplicationsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_prov_RoleMembershipTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_WatchTopicTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetSurveyUsersTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new yaf_AttachmentTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MagnetNewsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new StatesTable(DataProvider));
            }
            #endregion
        }
        

        #region ' Helpers '
            
        internal static DateTime DateTimeNowTruncatedDownToSecond() {
            var now = DateTime.Now;
            return now.AddTicks(-now.Ticks % TimeSpan.TicksPerSecond);
        }

        #endregion

    }
}