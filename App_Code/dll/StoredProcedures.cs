﻿


  
using System;
using SubSonic;
using SubSonic.Schema;
using SubSonic.DataProviders;
using System.Data;

namespace Synergy.Magnet{
	public partial class MagnetDBDB{

        public StoredProcedure aspnet_AnyDataInTables(int TablesToCheck){
            StoredProcedure sp=new StoredProcedure("aspnet_AnyDataInTables",this.Provider);
            sp.Command.AddParameter("TablesToCheck",TablesToCheck,DbType.Int32);
            return sp;
        }
        public StoredProcedure aspnet_Applications_CreateApplication(string ApplicationName,Guid ApplicationId){
            StoredProcedure sp=new StoredProcedure("aspnet_Applications_CreateApplication",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("ApplicationId",ApplicationId,DbType.Guid);
            return sp;
        }
        public StoredProcedure aspnet_CheckSchemaVersion(string Feature,string CompatibleSchemaVersion){
            StoredProcedure sp=new StoredProcedure("aspnet_CheckSchemaVersion",this.Provider);
            sp.Command.AddParameter("Feature",Feature,DbType.String);
            sp.Command.AddParameter("CompatibleSchemaVersion",CompatibleSchemaVersion,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Membership_ChangePasswordQuestionAndAnswer(string ApplicationName,string UserName,string NewPasswordQuestion,string NewPasswordAnswer){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_ChangePasswordQuestionAndAnswer",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("NewPasswordQuestion",NewPasswordQuestion,DbType.String);
            sp.Command.AddParameter("NewPasswordAnswer",NewPasswordAnswer,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Membership_CreateUser(string ApplicationName,string UserName,string Password,string PasswordSalt,string Email,string PasswordQuestion,string PasswordAnswer,bool IsApproved,DateTime CurrentTimeUtc,DateTime CreateDate,int UniqueEmail,int PasswordFormat,Guid UserId){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_CreateUser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Password",Password,DbType.String);
            sp.Command.AddParameter("PasswordSalt",PasswordSalt,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            sp.Command.AddParameter("PasswordQuestion",PasswordQuestion,DbType.String);
            sp.Command.AddParameter("PasswordAnswer",PasswordAnswer,DbType.String);
            sp.Command.AddParameter("IsApproved",IsApproved,DbType.Boolean);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("CreateDate",CreateDate,DbType.DateTime);
            sp.Command.AddParameter("UniqueEmail",UniqueEmail,DbType.Int32);
            sp.Command.AddParameter("PasswordFormat",PasswordFormat,DbType.Int32);
            sp.Command.AddParameter("UserId",UserId,DbType.Guid);
            return sp;
        }
        public StoredProcedure aspnet_Membership_FindUsersByEmail(string ApplicationName,string EmailToMatch,int PageIndex,int PageSize){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_FindUsersByEmail",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("EmailToMatch",EmailToMatch,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            return sp;
        }
        public StoredProcedure aspnet_Membership_FindUsersByName(string ApplicationName,string UserNameToMatch,int PageIndex,int PageSize){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_FindUsersByName",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserNameToMatch",UserNameToMatch,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            return sp;
        }
        public StoredProcedure aspnet_Membership_GetAllUsers(string ApplicationName,int PageIndex,int PageSize){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_GetAllUsers",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            return sp;
        }
        public StoredProcedure aspnet_Membership_GetNumberOfUsersOnline(string ApplicationName,int MinutesSinceLastInActive,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_GetNumberOfUsersOnline",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("MinutesSinceLastInActive",MinutesSinceLastInActive,DbType.Int32);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Membership_GetPassword(string ApplicationName,string UserName,int MaxInvalidPasswordAttempts,int PasswordAttemptWindow,DateTime CurrentTimeUtc,string PasswordAnswer){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_GetPassword",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("MaxInvalidPasswordAttempts",MaxInvalidPasswordAttempts,DbType.Int32);
            sp.Command.AddParameter("PasswordAttemptWindow",PasswordAttemptWindow,DbType.Int32);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("PasswordAnswer",PasswordAnswer,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Membership_GetPasswordWithFormat(string ApplicationName,string UserName,bool UpdateLastLoginActivityDate,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_GetPasswordWithFormat",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("UpdateLastLoginActivityDate",UpdateLastLoginActivityDate,DbType.Boolean);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Membership_GetUserByEmail(string ApplicationName,string Email){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_GetUserByEmail",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Membership_GetUserByName(string ApplicationName,string UserName,DateTime CurrentTimeUtc,bool UpdateLastActivity){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_GetUserByName",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("UpdateLastActivity",UpdateLastActivity,DbType.Boolean);
            return sp;
        }
        public StoredProcedure aspnet_Membership_GetUserByUserId(Guid UserId,DateTime CurrentTimeUtc,bool UpdateLastActivity){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_GetUserByUserId",this.Provider);
            sp.Command.AddParameter("UserId",UserId,DbType.Guid);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("UpdateLastActivity",UpdateLastActivity,DbType.Boolean);
            return sp;
        }
        public StoredProcedure aspnet_Membership_ResetPassword(string ApplicationName,string UserName,string NewPassword,int MaxInvalidPasswordAttempts,int PasswordAttemptWindow,string PasswordSalt,DateTime CurrentTimeUtc,int PasswordFormat,string PasswordAnswer){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_ResetPassword",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("NewPassword",NewPassword,DbType.String);
            sp.Command.AddParameter("MaxInvalidPasswordAttempts",MaxInvalidPasswordAttempts,DbType.Int32);
            sp.Command.AddParameter("PasswordAttemptWindow",PasswordAttemptWindow,DbType.Int32);
            sp.Command.AddParameter("PasswordSalt",PasswordSalt,DbType.String);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("PasswordFormat",PasswordFormat,DbType.Int32);
            sp.Command.AddParameter("PasswordAnswer",PasswordAnswer,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Membership_SetPassword(string ApplicationName,string UserName,string NewPassword,string PasswordSalt,DateTime CurrentTimeUtc,int PasswordFormat){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_SetPassword",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("NewPassword",NewPassword,DbType.String);
            sp.Command.AddParameter("PasswordSalt",PasswordSalt,DbType.String);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("PasswordFormat",PasswordFormat,DbType.Int32);
            return sp;
        }
        public StoredProcedure aspnet_Membership_UnlockUser(string ApplicationName,string UserName){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_UnlockUser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Membership_UpdateUser(string ApplicationName,string UserName,string Email,string Comment,bool IsApproved,DateTime LastLoginDate,DateTime LastActivityDate,int UniqueEmail,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_UpdateUser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            sp.Command.AddParameter("Comment",Comment,DbType.String);
            sp.Command.AddParameter("IsApproved",IsApproved,DbType.Boolean);
            sp.Command.AddParameter("LastLoginDate",LastLoginDate,DbType.DateTime);
            sp.Command.AddParameter("LastActivityDate",LastActivityDate,DbType.DateTime);
            sp.Command.AddParameter("UniqueEmail",UniqueEmail,DbType.Int32);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Membership_UpdateUserInfo(string ApplicationName,string UserName,bool IsPasswordCorrect,bool UpdateLastLoginActivityDate,int MaxInvalidPasswordAttempts,int PasswordAttemptWindow,DateTime CurrentTimeUtc,DateTime LastLoginDate,DateTime LastActivityDate){
            StoredProcedure sp=new StoredProcedure("aspnet_Membership_UpdateUserInfo",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("IsPasswordCorrect",IsPasswordCorrect,DbType.Boolean);
            sp.Command.AddParameter("UpdateLastLoginActivityDate",UpdateLastLoginActivityDate,DbType.Boolean);
            sp.Command.AddParameter("MaxInvalidPasswordAttempts",MaxInvalidPasswordAttempts,DbType.Int32);
            sp.Command.AddParameter("PasswordAttemptWindow",PasswordAttemptWindow,DbType.Int32);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("LastLoginDate",LastLoginDate,DbType.DateTime);
            sp.Command.AddParameter("LastActivityDate",LastActivityDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Paths_CreatePath(Guid ApplicationId,string Path,Guid PathId){
            StoredProcedure sp=new StoredProcedure("aspnet_Paths_CreatePath",this.Provider);
            sp.Command.AddParameter("ApplicationId",ApplicationId,DbType.Guid);
            sp.Command.AddParameter("Path",Path,DbType.String);
            sp.Command.AddParameter("PathId",PathId,DbType.Guid);
            return sp;
        }
        public StoredProcedure aspnet_Personalization_GetApplicationId(string ApplicationName,Guid ApplicationId){
            StoredProcedure sp=new StoredProcedure("aspnet_Personalization_GetApplicationId",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("ApplicationId",ApplicationId,DbType.Guid);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAdministration_DeleteAllState(bool AllUsersScope,string ApplicationName,int Count){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAdministration_DeleteAllState",this.Provider);
            sp.Command.AddParameter("AllUsersScope",AllUsersScope,DbType.Boolean);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Count",Count,DbType.Int32);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAdministration_FindState(bool AllUsersScope,string ApplicationName,int PageIndex,int PageSize,string Path,string UserName,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAdministration_FindState",this.Provider);
            sp.Command.AddParameter("AllUsersScope",AllUsersScope,DbType.Boolean);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            sp.Command.AddParameter("Path",Path,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAdministration_GetCountOfState(int Count,bool AllUsersScope,string ApplicationName,string Path,string UserName,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAdministration_GetCountOfState",this.Provider);
            sp.Command.AddParameter("Count",Count,DbType.Int32);
            sp.Command.AddParameter("AllUsersScope",AllUsersScope,DbType.Boolean);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAdministration_ResetSharedState(int Count,string ApplicationName,string Path){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAdministration_ResetSharedState",this.Provider);
            sp.Command.AddParameter("Count",Count,DbType.Int32);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAdministration_ResetUserState(int Count,string ApplicationName,DateTime InactiveSinceDate,string UserName,string Path){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAdministration_ResetUserState",this.Provider);
            sp.Command.AddParameter("Count",Count,DbType.Int32);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAllUsers_GetPageSettings(string ApplicationName,string Path){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAllUsers_GetPageSettings",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAllUsers_ResetPageSettings(string ApplicationName,string Path){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAllUsers_ResetPageSettings",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationAllUsers_SetPageSettings(string ApplicationName,string Path,byte[] PageSettings,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationAllUsers_SetPageSettings",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            sp.Command.AddParameter("PageSettings",PageSettings,DbType.Binary);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationPerUser_GetPageSettings(string ApplicationName,string UserName,string Path,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationPerUser_GetPageSettings",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationPerUser_ResetPageSettings(string ApplicationName,string UserName,string Path,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationPerUser_ResetPageSettings",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_PersonalizationPerUser_SetPageSettings(string ApplicationName,string UserName,string Path,byte[] PageSettings,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_PersonalizationPerUser_SetPageSettings",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Path",Path,DbType.String);
            sp.Command.AddParameter("PageSettings",PageSettings,DbType.Binary);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Profile_DeleteInactiveProfiles(string ApplicationName,int ProfileAuthOptions,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("aspnet_Profile_DeleteInactiveProfiles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("ProfileAuthOptions",ProfileAuthOptions,DbType.Int32);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Profile_DeleteProfiles(string ApplicationName,string UserNames){
            StoredProcedure sp=new StoredProcedure("aspnet_Profile_DeleteProfiles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserNames",UserNames,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Profile_GetNumberOfInactiveProfiles(string ApplicationName,int ProfileAuthOptions,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("aspnet_Profile_GetNumberOfInactiveProfiles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("ProfileAuthOptions",ProfileAuthOptions,DbType.Int32);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Profile_GetProfiles(string ApplicationName,int ProfileAuthOptions,int PageIndex,int PageSize,string UserNameToMatch,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("aspnet_Profile_GetProfiles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("ProfileAuthOptions",ProfileAuthOptions,DbType.Int32);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            sp.Command.AddParameter("UserNameToMatch",UserNameToMatch,DbType.String);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Profile_GetProperties(string ApplicationName,string UserName,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_Profile_GetProperties",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_Profile_SetProperties(string ApplicationName,string PropertyNames,string PropertyValuesString,byte[] PropertyValuesBinary,string UserName,bool IsUserAnonymous,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_Profile_SetProperties",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("PropertyNames",PropertyNames,DbType.String);
            sp.Command.AddParameter("PropertyValuesString",PropertyValuesString,DbType.String);
            sp.Command.AddParameter("PropertyValuesBinary",PropertyValuesBinary,DbType.Binary);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("IsUserAnonymous",IsUserAnonymous,DbType.Boolean);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_RegisterSchemaVersion(string Feature,string CompatibleSchemaVersion,bool IsCurrentVersion,bool RemoveIncompatibleSchema){
            StoredProcedure sp=new StoredProcedure("aspnet_RegisterSchemaVersion",this.Provider);
            sp.Command.AddParameter("Feature",Feature,DbType.String);
            sp.Command.AddParameter("CompatibleSchemaVersion",CompatibleSchemaVersion,DbType.String);
            sp.Command.AddParameter("IsCurrentVersion",IsCurrentVersion,DbType.Boolean);
            sp.Command.AddParameter("RemoveIncompatibleSchema",RemoveIncompatibleSchema,DbType.Boolean);
            return sp;
        }
        public StoredProcedure aspnet_Roles_CreateRole(string ApplicationName,string RoleName){
            StoredProcedure sp=new StoredProcedure("aspnet_Roles_CreateRole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("RoleName",RoleName,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Roles_DeleteRole(string ApplicationName,string RoleName,bool DeleteOnlyIfRoleIsEmpty){
            StoredProcedure sp=new StoredProcedure("aspnet_Roles_DeleteRole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("RoleName",RoleName,DbType.String);
            sp.Command.AddParameter("DeleteOnlyIfRoleIsEmpty",DeleteOnlyIfRoleIsEmpty,DbType.Boolean);
            return sp;
        }
        public StoredProcedure aspnet_Roles_GetAllRoles(string ApplicationName){
            StoredProcedure sp=new StoredProcedure("aspnet_Roles_GetAllRoles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Roles_RoleExists(string ApplicationName,string RoleName){
            StoredProcedure sp=new StoredProcedure("aspnet_Roles_RoleExists",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("RoleName",RoleName,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Setup_RemoveAllRoleMembers(string name){
            StoredProcedure sp=new StoredProcedure("aspnet_Setup_RemoveAllRoleMembers",this.Provider);
            sp.Command.AddParameter("name",name,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Setup_RestorePermissions(string name){
            StoredProcedure sp=new StoredProcedure("aspnet_Setup_RestorePermissions",this.Provider);
            sp.Command.AddParameter("name",name,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_UnRegisterSchemaVersion(string Feature,string CompatibleSchemaVersion){
            StoredProcedure sp=new StoredProcedure("aspnet_UnRegisterSchemaVersion",this.Provider);
            sp.Command.AddParameter("Feature",Feature,DbType.String);
            sp.Command.AddParameter("CompatibleSchemaVersion",CompatibleSchemaVersion,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_Users_CreateUser(Guid ApplicationId,string UserName,bool IsUserAnonymous,DateTime LastActivityDate,Guid UserId){
            StoredProcedure sp=new StoredProcedure("aspnet_Users_CreateUser",this.Provider);
            sp.Command.AddParameter("ApplicationId",ApplicationId,DbType.Guid);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("IsUserAnonymous",IsUserAnonymous,DbType.Boolean);
            sp.Command.AddParameter("LastActivityDate",LastActivityDate,DbType.DateTime);
            sp.Command.AddParameter("UserId",UserId,DbType.Guid);
            return sp;
        }
        public StoredProcedure aspnet_Users_DeleteUser(string ApplicationName,string UserName,int TablesToDeleteFrom,int NumTablesDeletedFrom){
            StoredProcedure sp=new StoredProcedure("aspnet_Users_DeleteUser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("TablesToDeleteFrom",TablesToDeleteFrom,DbType.Int32);
            sp.Command.AddParameter("NumTablesDeletedFrom",NumTablesDeletedFrom,DbType.Int32);
            return sp;
        }
        public StoredProcedure aspnet_UsersInRoles_AddUsersToRoles(string ApplicationName,string UserNames,string RoleNames,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("aspnet_UsersInRoles_AddUsersToRoles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserNames",UserNames,DbType.String);
            sp.Command.AddParameter("RoleNames",RoleNames,DbType.String);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure aspnet_UsersInRoles_FindUsersInRole(string ApplicationName,string RoleName,string UserNameToMatch){
            StoredProcedure sp=new StoredProcedure("aspnet_UsersInRoles_FindUsersInRole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("RoleName",RoleName,DbType.String);
            sp.Command.AddParameter("UserNameToMatch",UserNameToMatch,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_UsersInRoles_GetRolesForUser(string ApplicationName,string UserName){
            StoredProcedure sp=new StoredProcedure("aspnet_UsersInRoles_GetRolesForUser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_UsersInRoles_GetUsersInRoles(string ApplicationName,string RoleName){
            StoredProcedure sp=new StoredProcedure("aspnet_UsersInRoles_GetUsersInRoles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("RoleName",RoleName,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_UsersInRoles_IsUserInRole(string ApplicationName,string UserName,string RoleName){
            StoredProcedure sp=new StoredProcedure("aspnet_UsersInRoles_IsUserInRole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("RoleName",RoleName,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_UsersInRoles_RemoveUsersFromRoles(string ApplicationName,string UserNames,string RoleNames){
            StoredProcedure sp=new StoredProcedure("aspnet_UsersInRoles_RemoveUsersFromRoles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserNames",UserNames,DbType.String);
            sp.Command.AddParameter("RoleNames",RoleNames,DbType.String);
            return sp;
        }
        public StoredProcedure aspnet_WebEvent_LogEvent(string EventId,DateTime EventTimeUtc,DateTime EventTime,string EventType,decimal EventSequence,decimal EventOccurrence,int EventCode,int EventDetailCode,string Message,string ApplicationPath,string ApplicationVirtualPath,string MachineName,string RequestUrl,string ExceptionType,string Details){
            StoredProcedure sp=new StoredProcedure("aspnet_WebEvent_LogEvent",this.Provider);
            sp.Command.AddParameter("EventId",EventId,DbType.AnsiStringFixedLength);
            sp.Command.AddParameter("EventTimeUtc",EventTimeUtc,DbType.DateTime);
            sp.Command.AddParameter("EventTime",EventTime,DbType.DateTime);
            sp.Command.AddParameter("EventType",EventType,DbType.String);
            sp.Command.AddParameter("EventSequence",EventSequence,DbType.Decimal);
            sp.Command.AddParameter("EventOccurrence",EventOccurrence,DbType.Decimal);
            sp.Command.AddParameter("EventCode",EventCode,DbType.Int32);
            sp.Command.AddParameter("EventDetailCode",EventDetailCode,DbType.Int32);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("ApplicationPath",ApplicationPath,DbType.String);
            sp.Command.AddParameter("ApplicationVirtualPath",ApplicationVirtualPath,DbType.String);
            sp.Command.AddParameter("MachineName",MachineName,DbType.String);
            sp.Command.AddParameter("RequestUrl",RequestUrl,DbType.String);
            sp.Command.AddParameter("ExceptionType",ExceptionType,DbType.String);
            sp.Command.AddParameter("Details",Details,DbType.String);
            return sp;
        }
        public StoredProcedure spp_copydatabase(int reportperiod){
            StoredProcedure sp=new StoredProcedure("spp_copydatabase",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_copydatabase_all(int reportperiod){
            StoredProcedure sp=new StoredProcedure("spp_copydatabase_all",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_copydatabase_APR_ADHOC(int reportperiod,int reportYear,bool isAPR){
            StoredProcedure sp=new StoredProcedure("spp_copydatabase_APR_ADHOC",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            sp.Command.AddParameter("reportYear",reportYear,DbType.Int32);
            sp.Command.AddParameter("isAPR",isAPR,DbType.Boolean);
            return sp;
        }
        public StoredProcedure spp_copydatabase_cohort13_ADHOC(int reportperiod,int reportYear,bool isAPR){
            StoredProcedure sp=new StoredProcedure("spp_copydatabase_cohort13_ADHOC",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            sp.Command.AddParameter("reportYear",reportYear,DbType.Int32);
            sp.Command.AddParameter("isAPR",isAPR,DbType.Boolean);
            return sp;
        }
        public StoredProcedure spp_copydatabase_y2APR(int reportperiod){
            StoredProcedure sp=new StoredProcedure("spp_copydatabase_y2APR",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_copydatabase_y3(int reportperiod){
            StoredProcedure sp=new StoredProcedure("spp_copydatabase_y3",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_copymagnetschools(int reportYear){
            StoredProcedure sp=new StoredProcedure("spp_copymagnetschools",this.Provider);
            sp.Command.AddParameter("reportYear",reportYear,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_copymagnetschools_y3(int reportYear){
            StoredProcedure sp=new StoredProcedure("spp_copymagnetschools_y3",this.Provider);
            sp.Command.AddParameter("reportYear",reportYear,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_deletedatabase(int reportperiod){
            StoredProcedure sp=new StoredProcedure("spp_deletedatabase",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_deleteDatestamp(int reportperiod){
            StoredProcedure sp=new StoredProcedure("spp_deleteDatestamp",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_feederenrollment(int GranteeReportID,int stageid,string SchoolIDs){
            StoredProcedure sp=new StoredProcedure("spp_feederenrollment",this.Provider);
            sp.Command.AddParameter("GranteeReportID",GranteeReportID,DbType.Int32);
            sp.Command.AddParameter("stageid",stageid,DbType.Int32);
            sp.Command.AddParameter("SchoolIDs",SchoolIDs,DbType.AnsiString);
            return sp;
        }
        public StoredProcedure spp_feederenrollment_order(int GranteeReportID,int stageid){
            StoredProcedure sp=new StoredProcedure("spp_feederenrollment_order",this.Provider);
            sp.Command.AddParameter("GranteeReportID",GranteeReportID,DbType.Int32);
            sp.Command.AddParameter("stageid",stageid,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_getGranteeInfo(int reportperiodid){
            StoredProcedure sp=new StoredProcedure("spp_getGranteeInfo",this.Provider);
            sp.Command.AddParameter("reportperiodid",reportperiodid,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_getnews(int option){
            StoredProcedure sp=new StoredProcedure("spp_getnews",this.Provider);
            sp.Command.AddParameter("option",option,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_getResource(string strQuery){
            StoredProcedure sp=new StoredProcedure("spp_getResource",this.Provider);
            sp.Command.AddParameter("strQuery",strQuery,DbType.AnsiString);
            return sp;
        }
        public StoredProcedure spp_getSchoolGradeInfo(int reportperiodid){
            StoredProcedure sp=new StoredProcedure("spp_getSchoolGradeInfo",this.Provider);
            sp.Command.AddParameter("reportperiodid",reportperiodid,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_leaenrollment(int GranteeReportID,int stageid){
            StoredProcedure sp=new StoredProcedure("spp_leaenrollment",this.Provider);
            sp.Command.AddParameter("GranteeReportID",GranteeReportID,DbType.Int32);
            sp.Command.AddParameter("stageid",stageid,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_resetdatabase(int reportperiod){
            StoredProcedure sp=new StoredProcedure("spp_resetdatabase",this.Provider);
            sp.Command.AddParameter("reportperiod",reportperiod,DbType.Int32);
            return sp;
        }
        public StoredProcedure spp_schoolenrollment(int GranteeReportID,int stageid,int schoolid){
            StoredProcedure sp=new StoredProcedure("spp_schoolenrollment",this.Provider);
            sp.Command.AddParameter("GranteeReportID",GranteeReportID,DbType.Int32);
            sp.Command.AddParameter("stageid",stageid,DbType.Int32);
            sp.Command.AddParameter("schoolid",schoolid,DbType.Int32);
            return sp;
        }
        public StoredProcedure Webinar_Evaluation_PuttingItAllTogether_ExcelDataset(){
            StoredProcedure sp=new StoredProcedure("Webinar_Evaluation_PuttingItAllTogether_ExcelDataset",this.Provider);
            return sp;
        }
        public StoredProcedure Webinar_Evaluation_PuttingItAllTogether_Insert(int RegisteringEasy,int AccessingEasy,int SpeakersKnowledgeable,int WebinarWellOrganized,int Relevantexamples,int SpeakersAdequatelyResponded,int CompleteProvidedTools,int EnagageInliveChats,int TakeAdvantageOfConsultativeServices,int ContinueWorkingWithTheFinanceProject,bool IsFurtherTraining,string FurtherTrainingNo,string IfFurtherSustainabilityTraining,bool IsWebBasedCommunity,string WebBasedCommunityNo,bool IsEngagedInOnlineCommunities,string EngagedInOnlineCommunitiesYes,string MostHelpful,string AdditionalComments,DateTime DateSubmitted){
            StoredProcedure sp=new StoredProcedure("Webinar_Evaluation_PuttingItAllTogether_Insert",this.Provider);
            sp.Command.AddParameter("RegisteringEasy",RegisteringEasy,DbType.Int32);
            sp.Command.AddParameter("AccessingEasy",AccessingEasy,DbType.Int32);
            sp.Command.AddParameter("SpeakersKnowledgeable",SpeakersKnowledgeable,DbType.Int32);
            sp.Command.AddParameter("WebinarWellOrganized",WebinarWellOrganized,DbType.Int32);
            sp.Command.AddParameter("Relevantexamples",Relevantexamples,DbType.Int32);
            sp.Command.AddParameter("SpeakersAdequatelyResponded",SpeakersAdequatelyResponded,DbType.Int32);
            sp.Command.AddParameter("CompleteProvidedTools",CompleteProvidedTools,DbType.Int32);
            sp.Command.AddParameter("EnagageInliveChats",EnagageInliveChats,DbType.Int32);
            sp.Command.AddParameter("TakeAdvantageOfConsultativeServices",TakeAdvantageOfConsultativeServices,DbType.Int32);
            sp.Command.AddParameter("ContinueWorkingWithTheFinanceProject",ContinueWorkingWithTheFinanceProject,DbType.Int32);
            sp.Command.AddParameter("IsFurtherTraining",IsFurtherTraining,DbType.Boolean);
            sp.Command.AddParameter("FurtherTrainingNo",FurtherTrainingNo,DbType.String);
            sp.Command.AddParameter("IfFurtherSustainabilityTraining",IfFurtherSustainabilityTraining,DbType.String);
            sp.Command.AddParameter("IsWebBasedCommunity",IsWebBasedCommunity,DbType.Boolean);
            sp.Command.AddParameter("WebBasedCommunityNo",WebBasedCommunityNo,DbType.String);
            sp.Command.AddParameter("IsEngagedInOnlineCommunities",IsEngagedInOnlineCommunities,DbType.Boolean);
            sp.Command.AddParameter("EngagedInOnlineCommunitiesYes",EngagedInOnlineCommunitiesYes,DbType.String);
            sp.Command.AddParameter("MostHelpful",MostHelpful,DbType.String);
            sp.Command.AddParameter("AdditionalComments",AdditionalComments,DbType.String);
            sp.Command.AddParameter("DateSubmitted",DateSubmitted,DbType.DateTime);
            return sp;
        }
        public StoredProcedure Webinar_Evaluation_Sustainability3_ExcelDataset(){
            StoredProcedure sp=new StoredProcedure("Webinar_Evaluation_Sustainability3_ExcelDataset",this.Provider);
            return sp;
        }
        public StoredProcedure Webinar_Evaluation_Sustainability3_Insert(int RegisteringEasy,int AccessingEasy,int SpeakersKnowledgeable,int WebinarWellOrganized,int Relevantexamples,int SpeakersAdequatelyResponded,int CompleteProvidedTools,int AttendNextWebinar,int EnagageInliveChats,bool IsUndestandStepsForDevelopingSustainabilityPlan,string AdditionalSupport,bool IsTeamInPlace,bool IsProperBuyIn,string AdditionalComments,DateTime DateSubmitted){
            StoredProcedure sp=new StoredProcedure("Webinar_Evaluation_Sustainability3_Insert",this.Provider);
            sp.Command.AddParameter("RegisteringEasy",RegisteringEasy,DbType.Int32);
            sp.Command.AddParameter("AccessingEasy",AccessingEasy,DbType.Int32);
            sp.Command.AddParameter("SpeakersKnowledgeable",SpeakersKnowledgeable,DbType.Int32);
            sp.Command.AddParameter("WebinarWellOrganized",WebinarWellOrganized,DbType.Int32);
            sp.Command.AddParameter("Relevantexamples",Relevantexamples,DbType.Int32);
            sp.Command.AddParameter("SpeakersAdequatelyResponded",SpeakersAdequatelyResponded,DbType.Int32);
            sp.Command.AddParameter("CompleteProvidedTools",CompleteProvidedTools,DbType.Int32);
            sp.Command.AddParameter("AttendNextWebinar",AttendNextWebinar,DbType.Int32);
            sp.Command.AddParameter("EnagageInliveChats",EnagageInliveChats,DbType.Int32);
            sp.Command.AddParameter("IsUndestandStepsForDevelopingSustainabilityPlan",IsUndestandStepsForDevelopingSustainabilityPlan,DbType.Boolean);
            sp.Command.AddParameter("AdditionalSupport",AdditionalSupport,DbType.String);
            sp.Command.AddParameter("IsTeamInPlace",IsTeamInPlace,DbType.Boolean);
            sp.Command.AddParameter("IsProperBuyIn",IsProperBuyIn,DbType.Boolean);
            sp.Command.AddParameter("AdditionalComments",AdditionalComments,DbType.String);
            sp.Command.AddParameter("DateSubmitted",DateSubmitted,DbType.DateTime);
            return sp;
        }
        public StoredProcedure WebinarEvaluation_AligningProjectActivities_ExcelDataset(){
            StoredProcedure sp=new StoredProcedure("WebinarEvaluation_AligningProjectActivities_ExcelDataset",this.Provider);
            return sp;
        }
        public StoredProcedure WebinarEvaluation_AligningProjectActivities_Insert(int RegisteringEasy,int AccessingEasy,int SpeakersKnowledgeable,int WellOrganized,int RelevantExamplesUsed,int SpeakersAdequatelyResponded,int CompleteProvidedTools,int AttendNextWebinar,int LiveChatOrConsultativeServices,int FacebookOrTwitter,bool IsFundingLevelRequiredToSustain,bool IsMSLeadershipUsingAdditionalFinancing,string FinancingOptions,bool IsPlanInPlace,bool IsWorkingToEstablishRelationships,string RelationshipsExamples,string AdditionalComments,DateTime CreatedDate){
            StoredProcedure sp=new StoredProcedure("WebinarEvaluation_AligningProjectActivities_Insert",this.Provider);
            sp.Command.AddParameter("RegisteringEasy",RegisteringEasy,DbType.Int32);
            sp.Command.AddParameter("AccessingEasy",AccessingEasy,DbType.Int32);
            sp.Command.AddParameter("SpeakersKnowledgeable",SpeakersKnowledgeable,DbType.Int32);
            sp.Command.AddParameter("WellOrganized",WellOrganized,DbType.Int32);
            sp.Command.AddParameter("RelevantExamplesUsed",RelevantExamplesUsed,DbType.Int32);
            sp.Command.AddParameter("SpeakersAdequatelyResponded",SpeakersAdequatelyResponded,DbType.Int32);
            sp.Command.AddParameter("CompleteProvidedTools",CompleteProvidedTools,DbType.Int32);
            sp.Command.AddParameter("AttendNextWebinar",AttendNextWebinar,DbType.Int32);
            sp.Command.AddParameter("LiveChatOrConsultativeServices",LiveChatOrConsultativeServices,DbType.Int32);
            sp.Command.AddParameter("FacebookOrTwitter",FacebookOrTwitter,DbType.Int32);
            sp.Command.AddParameter("IsFundingLevelRequiredToSustain",IsFundingLevelRequiredToSustain,DbType.Boolean);
            sp.Command.AddParameter("IsMSLeadershipUsingAdditionalFinancing",IsMSLeadershipUsingAdditionalFinancing,DbType.Boolean);
            sp.Command.AddParameter("FinancingOptions",FinancingOptions,DbType.String);
            sp.Command.AddParameter("IsPlanInPlace",IsPlanInPlace,DbType.Boolean);
            sp.Command.AddParameter("IsWorkingToEstablishRelationships",IsWorkingToEstablishRelationships,DbType.Boolean);
            sp.Command.AddParameter("RelationshipsExamples",RelationshipsExamples,DbType.String);
            sp.Command.AddParameter("AdditionalComments",AdditionalComments,DbType.String);
            sp.Command.AddParameter("CreatedDate",CreatedDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure WebinarEvaluation_ExcelDataset(){
            StoredProcedure sp=new StoredProcedure("WebinarEvaluation_ExcelDataset",this.Provider);
            return sp;
        }
        public StoredProcedure WebinarEvaluation_Insert(int RatingRegisteringForTheWebinarWasEasy,int RatingAccessingTheWebinarWasEasy,int RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter,int RatingTheWebinarWasWellOrganized,int RatingRelevantExamplesWereUsedToIllustrateConcepts,int RatingTheSpeakersAdequatelyRespondedToQuestions,int RatingIWillCompleteTheProvidedTools,int RatingIWillAttendTheNextWebinar,int RatingIWillEngageInLiveChats,bool ProvideBetterInformation,bool ClarifyWObjectives,bool ReduceWContent,bool IncreaseWContent,bool MakeWInteractive,bool ImproveWOrganization,bool MakeWLessDifficult,bool MakeWMoreDifficult,string Comments,bool StructureInPlace,string BiggestChallenge,bool DevelopedLogicModel,DateTime DateSubmitted){
            StoredProcedure sp=new StoredProcedure("WebinarEvaluation_Insert",this.Provider);
            sp.Command.AddParameter("RatingRegisteringForTheWebinarWasEasy",RatingRegisteringForTheWebinarWasEasy,DbType.Int32);
            sp.Command.AddParameter("RatingAccessingTheWebinarWasEasy",RatingAccessingTheWebinarWasEasy,DbType.Int32);
            sp.Command.AddParameter("RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter",RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter,DbType.Int32);
            sp.Command.AddParameter("RatingTheWebinarWasWellOrganized",RatingTheWebinarWasWellOrganized,DbType.Int32);
            sp.Command.AddParameter("RatingRelevantExamplesWereUsedToIllustrateConcepts",RatingRelevantExamplesWereUsedToIllustrateConcepts,DbType.Int32);
            sp.Command.AddParameter("RatingTheSpeakersAdequatelyRespondedToQuestions",RatingTheSpeakersAdequatelyRespondedToQuestions,DbType.Int32);
            sp.Command.AddParameter("RatingIWillCompleteTheProvidedTools",RatingIWillCompleteTheProvidedTools,DbType.Int32);
            sp.Command.AddParameter("RatingIWillAttendTheNextWebinar",RatingIWillAttendTheNextWebinar,DbType.Int32);
            sp.Command.AddParameter("RatingIWillEngageInLiveChats",RatingIWillEngageInLiveChats,DbType.Int32);
            sp.Command.AddParameter("ProvideBetterInformation",ProvideBetterInformation,DbType.Boolean);
            sp.Command.AddParameter("ClarifyWObjectives",ClarifyWObjectives,DbType.Boolean);
            sp.Command.AddParameter("ReduceWContent",ReduceWContent,DbType.Boolean);
            sp.Command.AddParameter("IncreaseWContent",IncreaseWContent,DbType.Boolean);
            sp.Command.AddParameter("MakeWInteractive",MakeWInteractive,DbType.Boolean);
            sp.Command.AddParameter("ImproveWOrganization",ImproveWOrganization,DbType.Boolean);
            sp.Command.AddParameter("MakeWLessDifficult",MakeWLessDifficult,DbType.Boolean);
            sp.Command.AddParameter("MakeWMoreDifficult",MakeWMoreDifficult,DbType.Boolean);
            sp.Command.AddParameter("Comments",Comments,DbType.String);
            sp.Command.AddParameter("StructureInPlace",StructureInPlace,DbType.Boolean);
            sp.Command.AddParameter("BiggestChallenge",BiggestChallenge,DbType.String);
            sp.Command.AddParameter("DevelopedLogicModel",DevelopedLogicModel,DbType.Boolean);
            sp.Command.AddParameter("DateSubmitted",DateSubmitted,DbType.DateTime);
            return sp;
        }
        public StoredProcedure WebinarEvaluation_NavigatingMAPS_ExcelDataset(){
            StoredProcedure sp=new StoredProcedure("WebinarEvaluation_NavigatingMAPS_ExcelDataset",this.Provider);
            return sp;
        }
        public StoredProcedure WebinarEvaluation_NavigatingMAPS_Insert(int RegisteringForWebinarWasEasy,int AccessingWebinarWasEasy,int ContentWasOrganized,int PresenterWasKnowledgeable,int RelevantExamplesWereUsed,int AnswersPresenterGaveClear,int QualityOfInstructionWasGood,int IwasEngagedInTraining,int MAPSTrainingLearningObjectsWereMet,int AdequateTrainingTimeProvided,int AbleToApplyKnowledgeLearned,string AnythingYouDidNotUnderstand,string HowCanWeImproveTraining,string MostusefulThingYouLearned,string AdditionalComments,DateTime DateEntered){
            StoredProcedure sp=new StoredProcedure("WebinarEvaluation_NavigatingMAPS_Insert",this.Provider);
            sp.Command.AddParameter("RegisteringForWebinarWasEasy",RegisteringForWebinarWasEasy,DbType.Int32);
            sp.Command.AddParameter("AccessingWebinarWasEasy",AccessingWebinarWasEasy,DbType.Int32);
            sp.Command.AddParameter("ContentWasOrganized",ContentWasOrganized,DbType.Int32);
            sp.Command.AddParameter("PresenterWasKnowledgeable",PresenterWasKnowledgeable,DbType.Int32);
            sp.Command.AddParameter("RelevantExamplesWereUsed",RelevantExamplesWereUsed,DbType.Int32);
            sp.Command.AddParameter("AnswersPresenterGaveClear",AnswersPresenterGaveClear,DbType.Int32);
            sp.Command.AddParameter("QualityOfInstructionWasGood",QualityOfInstructionWasGood,DbType.Int32);
            sp.Command.AddParameter("IwasEngagedInTraining",IwasEngagedInTraining,DbType.Int32);
            sp.Command.AddParameter("MAPSTrainingLearningObjectsWereMet",MAPSTrainingLearningObjectsWereMet,DbType.Int32);
            sp.Command.AddParameter("AdequateTrainingTimeProvided",AdequateTrainingTimeProvided,DbType.Int32);
            sp.Command.AddParameter("AbleToApplyKnowledgeLearned",AbleToApplyKnowledgeLearned,DbType.Int32);
            sp.Command.AddParameter("AnythingYouDidNotUnderstand",AnythingYouDidNotUnderstand,DbType.String);
            sp.Command.AddParameter("HowCanWeImproveTraining",HowCanWeImproveTraining,DbType.String);
            sp.Command.AddParameter("MostusefulThingYouLearned",MostusefulThingYouLearned,DbType.String);
            sp.Command.AddParameter("AdditionalComments",AdditionalComments,DbType.String);
            sp.Command.AddParameter("DateEntered",DateEntered,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_accessmask_delete(int AccessMaskID){
            StoredProcedure sp=new StoredProcedure("yaf_accessmask_delete",this.Provider);
            sp.Command.AddParameter("AccessMaskID",AccessMaskID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_accessmask_list(int BoardID,int AccessMaskID,int ExcludeFlags){
            StoredProcedure sp=new StoredProcedure("yaf_accessmask_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("AccessMaskID",AccessMaskID,DbType.Int32);
            sp.Command.AddParameter("ExcludeFlags",ExcludeFlags,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_accessmask_save(int AccessMaskID,int BoardID,string Name,bool ReadAccess,bool PostAccess,bool ReplyAccess,bool PriorityAccess,bool PollAccess,bool VoteAccess,bool ModeratorAccess,bool EditAccess,bool DeleteAccess,bool UploadAccess,bool DownloadAccess){
            StoredProcedure sp=new StoredProcedure("yaf_accessmask_save",this.Provider);
            sp.Command.AddParameter("AccessMaskID",AccessMaskID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("ReadAccess",ReadAccess,DbType.Boolean);
            sp.Command.AddParameter("PostAccess",PostAccess,DbType.Boolean);
            sp.Command.AddParameter("ReplyAccess",ReplyAccess,DbType.Boolean);
            sp.Command.AddParameter("PriorityAccess",PriorityAccess,DbType.Boolean);
            sp.Command.AddParameter("PollAccess",PollAccess,DbType.Boolean);
            sp.Command.AddParameter("VoteAccess",VoteAccess,DbType.Boolean);
            sp.Command.AddParameter("ModeratorAccess",ModeratorAccess,DbType.Boolean);
            sp.Command.AddParameter("EditAccess",EditAccess,DbType.Boolean);
            sp.Command.AddParameter("DeleteAccess",DeleteAccess,DbType.Boolean);
            sp.Command.AddParameter("UploadAccess",UploadAccess,DbType.Boolean);
            sp.Command.AddParameter("DownloadAccess",DownloadAccess,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_active_list(int BoardID,bool Guests,int ActiveTime,bool StyledNicks){
            StoredProcedure sp=new StoredProcedure("yaf_active_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Guests",Guests,DbType.Boolean);
            sp.Command.AddParameter("ActiveTime",ActiveTime,DbType.Int32);
            sp.Command.AddParameter("StyledNicks",StyledNicks,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_active_listforum(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_active_listforum",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_active_listtopic(int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_active_listtopic",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_active_stats(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_active_stats",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_active_updatemaxstats(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_active_updatemaxstats",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_attachment_delete(int AttachmentID){
            StoredProcedure sp=new StoredProcedure("yaf_attachment_delete",this.Provider);
            sp.Command.AddParameter("AttachmentID",AttachmentID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_attachment_download(int AttachmentID){
            StoredProcedure sp=new StoredProcedure("yaf_attachment_download",this.Provider);
            sp.Command.AddParameter("AttachmentID",AttachmentID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_attachment_list(int MessageID,int AttachmentID,int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_attachment_list",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("AttachmentID",AttachmentID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_attachment_save(int MessageID,string FileName,int Bytes,string ContentType,byte[] FileData){
            StoredProcedure sp=new StoredProcedure("yaf_attachment_save",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("FileName",FileName,DbType.String);
            sp.Command.AddParameter("Bytes",Bytes,DbType.Int32);
            sp.Command.AddParameter("ContentType",ContentType,DbType.String);
            sp.Command.AddParameter("FileData",FileData,DbType.Binary);
            return sp;
        }
        public StoredProcedure yaf_bannedip_delete(int ID){
            StoredProcedure sp=new StoredProcedure("yaf_bannedip_delete",this.Provider);
            sp.Command.AddParameter("ID",ID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_bannedip_list(int BoardID,int ID){
            StoredProcedure sp=new StoredProcedure("yaf_bannedip_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("ID",ID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_bannedip_save(int ID,int BoardID,string Mask){
            StoredProcedure sp=new StoredProcedure("yaf_bannedip_save",this.Provider);
            sp.Command.AddParameter("ID",ID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Mask",Mask,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_bbcode_delete(int BBCodeID){
            StoredProcedure sp=new StoredProcedure("yaf_bbcode_delete",this.Provider);
            sp.Command.AddParameter("BBCodeID",BBCodeID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_bbcode_list(int BoardID,int BBCodeID){
            StoredProcedure sp=new StoredProcedure("yaf_bbcode_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("BBCodeID",BBCodeID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_bbcode_save(int BBCodeID,int BoardID,string Name,string Description,string OnClickJS,string DisplayJS,string EditJS,string DisplayCSS,string SearchRegEx,string ReplaceRegEx,string Variables,bool UseModule,string ModuleClass,int ExecOrder){
            StoredProcedure sp=new StoredProcedure("yaf_bbcode_save",this.Provider);
            sp.Command.AddParameter("BBCodeID",BBCodeID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("Description",Description,DbType.String);
            sp.Command.AddParameter("OnClickJS",OnClickJS,DbType.String);
            sp.Command.AddParameter("DisplayJS",DisplayJS,DbType.String);
            sp.Command.AddParameter("EditJS",EditJS,DbType.String);
            sp.Command.AddParameter("DisplayCSS",DisplayCSS,DbType.String);
            sp.Command.AddParameter("SearchRegEx",SearchRegEx,DbType.String);
            sp.Command.AddParameter("ReplaceRegEx",ReplaceRegEx,DbType.String);
            sp.Command.AddParameter("Variables",Variables,DbType.String);
            sp.Command.AddParameter("UseModule",UseModule,DbType.Boolean);
            sp.Command.AddParameter("ModuleClass",ModuleClass,DbType.String);
            sp.Command.AddParameter("ExecOrder",ExecOrder,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_board_create(string BoardName,string MembershipAppName,string RolesAppName,string UserName,string UserKey,bool IsHostAdmin){
            StoredProcedure sp=new StoredProcedure("yaf_board_create",this.Provider);
            sp.Command.AddParameter("BoardName",BoardName,DbType.String);
            sp.Command.AddParameter("MembershipAppName",MembershipAppName,DbType.String);
            sp.Command.AddParameter("RolesAppName",RolesAppName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("UserKey",UserKey,DbType.String);
            sp.Command.AddParameter("IsHostAdmin",IsHostAdmin,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_board_delete(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_board_delete",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_board_list(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_board_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_board_poststats(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_board_poststats",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_board_resync(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_board_resync",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_board_save(int BoardID,string Name,bool AllowThreaded){
            StoredProcedure sp=new StoredProcedure("yaf_board_save",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("AllowThreaded",AllowThreaded,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_board_stats(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_board_stats",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_category_delete(int CategoryID){
            StoredProcedure sp=new StoredProcedure("yaf_category_delete",this.Provider);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_category_list(int BoardID,int CategoryID){
            StoredProcedure sp=new StoredProcedure("yaf_category_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_category_listread(int BoardID,int UserID,int CategoryID){
            StoredProcedure sp=new StoredProcedure("yaf_category_listread",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_category_save(int BoardID,int CategoryID,string Name,short SortOrder,string CategoryImage){
            StoredProcedure sp=new StoredProcedure("yaf_category_save",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Int16);
            sp.Command.AddParameter("CategoryImage",CategoryImage,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_category_simplelist(int StartID,int Limit){
            StoredProcedure sp=new StoredProcedure("yaf_category_simplelist",this.Provider);
            sp.Command.AddParameter("StartID",StartID,DbType.Int32);
            sp.Command.AddParameter("Limit",Limit,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_checkemail_list(string Email){
            StoredProcedure sp=new StoredProcedure("yaf_checkemail_list",this.Provider);
            sp.Command.AddParameter("Email",Email,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_checkemail_save(int UserID,string Hash,string Email){
            StoredProcedure sp=new StoredProcedure("yaf_checkemail_save",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Hash",Hash,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_checkemail_update(string Hash){
            StoredProcedure sp=new StoredProcedure("yaf_checkemail_update",this.Provider);
            sp.Command.AddParameter("Hash",Hash,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_choice_add(int PollID,string Choice){
            StoredProcedure sp=new StoredProcedure("yaf_choice_add",this.Provider);
            sp.Command.AddParameter("PollID",PollID,DbType.Int32);
            sp.Command.AddParameter("Choice",Choice,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_choice_delete(int ChoiceID){
            StoredProcedure sp=new StoredProcedure("yaf_choice_delete",this.Provider);
            sp.Command.AddParameter("ChoiceID",ChoiceID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_choice_update(int ChoiceID,string Choice){
            StoredProcedure sp=new StoredProcedure("yaf_choice_update",this.Provider);
            sp.Command.AddParameter("ChoiceID",ChoiceID,DbType.Int32);
            sp.Command.AddParameter("Choice",Choice,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_choice_vote(int ChoiceID,int UserID,string RemoteIP){
            StoredProcedure sp=new StoredProcedure("yaf_choice_vote",this.Provider);
            sp.Command.AddParameter("ChoiceID",ChoiceID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("RemoteIP",RemoteIP,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_eventlog_create(int UserID,string Source,string Description,int Type){
            StoredProcedure sp=new StoredProcedure("yaf_eventlog_create",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Source",Source,DbType.String);
            sp.Command.AddParameter("Description",Description,DbType.String);
            sp.Command.AddParameter("Type",Type,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_eventlog_delete(int EventLogID,int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_eventlog_delete",this.Provider);
            sp.Command.AddParameter("EventLogID",EventLogID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_eventlog_list(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_eventlog_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_extension_delete(int ExtensionID){
            StoredProcedure sp=new StoredProcedure("yaf_extension_delete",this.Provider);
            sp.Command.AddParameter("ExtensionID",ExtensionID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_extension_edit(int ExtensionID){
            StoredProcedure sp=new StoredProcedure("yaf_extension_edit",this.Provider);
            sp.Command.AddParameter("ExtensionID",ExtensionID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_extension_list(int BoardID,string Extension){
            StoredProcedure sp=new StoredProcedure("yaf_extension_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Extension",Extension,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_extension_save(int ExtensionID,int BoardID,string Extension){
            StoredProcedure sp=new StoredProcedure("yaf_extension_save",this.Provider);
            sp.Command.AddParameter("ExtensionID",ExtensionID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Extension",Extension,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_forum_delete(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_delete",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_list(int BoardID,int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_listall(int BoardID,int UserID,int root){
            StoredProcedure sp=new StoredProcedure("yaf_forum_listall",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("root",root,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_listall_fromcat(int BoardID,int CategoryID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_listall_fromcat",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_listallmymoderated(int BoardID,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_listallmymoderated",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_listpath(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_listpath",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_listread(int BoardID,int UserID,int CategoryID,int ParentID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_listread",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            sp.Command.AddParameter("ParentID",ParentID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_listSubForums(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_listSubForums",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_listtopics(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_listtopics",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_moderatelist(int BoardID,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_moderatelist",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_moderators(){
            StoredProcedure sp=new StoredProcedure("yaf_forum_moderators",this.Provider);
            return sp;
        }
        public StoredProcedure yaf_forum_resync(int BoardID,int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_resync",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_save(int ForumID,int CategoryID,int ParentID,string Name,string Description,short SortOrder,bool Locked,bool Hidden,bool IsTest,bool Moderated,string RemoteURL,string ThemeURL,int AccessMaskID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_save",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            sp.Command.AddParameter("ParentID",ParentID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("Description",Description,DbType.String);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Int16);
            sp.Command.AddParameter("Locked",Locked,DbType.Boolean);
            sp.Command.AddParameter("Hidden",Hidden,DbType.Boolean);
            sp.Command.AddParameter("IsTest",IsTest,DbType.Boolean);
            sp.Command.AddParameter("Moderated",Moderated,DbType.Boolean);
            sp.Command.AddParameter("RemoteURL",RemoteURL,DbType.String);
            sp.Command.AddParameter("ThemeURL",ThemeURL,DbType.String);
            sp.Command.AddParameter("AccessMaskID",AccessMaskID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_simplelist(int StartID,int Limit){
            StoredProcedure sp=new StoredProcedure("yaf_forum_simplelist",this.Provider);
            sp.Command.AddParameter("StartID",StartID,DbType.Int32);
            sp.Command.AddParameter("Limit",Limit,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_updatelastpost(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_updatelastpost",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forum_updatestats(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forum_updatestats",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forumaccess_group(int GroupID){
            StoredProcedure sp=new StoredProcedure("yaf_forumaccess_group",this.Provider);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forumaccess_list(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_forumaccess_list",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_forumaccess_save(int ForumID,int GroupID,int AccessMaskID){
            StoredProcedure sp=new StoredProcedure("yaf_forumaccess_save",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            sp.Command.AddParameter("AccessMaskID",AccessMaskID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_group_delete(int GroupID){
            StoredProcedure sp=new StoredProcedure("yaf_group_delete",this.Provider);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_group_list(int BoardID,int GroupID){
            StoredProcedure sp=new StoredProcedure("yaf_group_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_group_medal_delete(int GroupID,int MedalID){
            StoredProcedure sp=new StoredProcedure("yaf_group_medal_delete",this.Provider);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_group_medal_list(int GroupID,int MedalID){
            StoredProcedure sp=new StoredProcedure("yaf_group_medal_list",this.Provider);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_group_medal_save(int GroupID,int MedalID,string Message,bool Hide,bool OnlyRibbon,byte SortOrder){
            StoredProcedure sp=new StoredProcedure("yaf_group_medal_save",this.Provider);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("Hide",Hide,DbType.Boolean);
            sp.Command.AddParameter("OnlyRibbon",OnlyRibbon,DbType.Boolean);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Byte);
            return sp;
        }
        public StoredProcedure yaf_group_member(int BoardID,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_group_member",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_group_save(int GroupID,int BoardID,string Name,bool IsAdmin,bool IsStart,bool IsModerator,bool IsGuest,int AccessMaskID,int PMLimit,string Style,short SortOrder){
            StoredProcedure sp=new StoredProcedure("yaf_group_save",this.Provider);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("IsAdmin",IsAdmin,DbType.Boolean);
            sp.Command.AddParameter("IsStart",IsStart,DbType.Boolean);
            sp.Command.AddParameter("IsModerator",IsModerator,DbType.Boolean);
            sp.Command.AddParameter("IsGuest",IsGuest,DbType.Boolean);
            sp.Command.AddParameter("AccessMaskID",AccessMaskID,DbType.Int32);
            sp.Command.AddParameter("PMLimit",PMLimit,DbType.Int32);
            sp.Command.AddParameter("Style",Style,DbType.String);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Int16);
            return sp;
        }
        public StoredProcedure yaf_mail_create(string From,string FromName,string To,string ToName,string Subject,string Body,string BodyHtml){
            StoredProcedure sp=new StoredProcedure("yaf_mail_create",this.Provider);
            sp.Command.AddParameter("From",From,DbType.String);
            sp.Command.AddParameter("FromName",FromName,DbType.String);
            sp.Command.AddParameter("To",To,DbType.String);
            sp.Command.AddParameter("ToName",ToName,DbType.String);
            sp.Command.AddParameter("Subject",Subject,DbType.String);
            sp.Command.AddParameter("Body",Body,DbType.String);
            sp.Command.AddParameter("BodyHtml",BodyHtml,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_mail_createwatch(int TopicID,string From,string FromName,string Subject,string Body,string BodyHtml,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_mail_createwatch",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("From",From,DbType.String);
            sp.Command.AddParameter("FromName",FromName,DbType.String);
            sp.Command.AddParameter("Subject",Subject,DbType.String);
            sp.Command.AddParameter("Body",Body,DbType.String);
            sp.Command.AddParameter("BodyHtml",BodyHtml,DbType.String);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_mail_delete(int MailID){
            StoredProcedure sp=new StoredProcedure("yaf_mail_delete",this.Provider);
            sp.Command.AddParameter("MailID",MailID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_mail_list(int ProcessID){
            StoredProcedure sp=new StoredProcedure("yaf_mail_list",this.Provider);
            sp.Command.AddParameter("ProcessID",ProcessID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_medal_delete(int BoardID,int MedalID,string Category){
            StoredProcedure sp=new StoredProcedure("yaf_medal_delete",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            sp.Command.AddParameter("Category",Category,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_medal_list(int BoardID,int MedalID,string Category){
            StoredProcedure sp=new StoredProcedure("yaf_medal_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            sp.Command.AddParameter("Category",Category,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_medal_listusers(int MedalID){
            StoredProcedure sp=new StoredProcedure("yaf_medal_listusers",this.Provider);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_medal_resort(int BoardID,int MedalID,int Move){
            StoredProcedure sp=new StoredProcedure("yaf_medal_resort",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            sp.Command.AddParameter("Move",Move,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_medal_save(int BoardID,int MedalID,string Name,string Description,string Message,string Category,string MedalURL,string RibbonURL,string SmallMedalURL,string SmallRibbonURL,short SmallMedalWidth,short SmallMedalHeight,short SmallRibbonWidth,short SmallRibbonHeight,byte SortOrder,int Flags){
            StoredProcedure sp=new StoredProcedure("yaf_medal_save",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("Description",Description,DbType.String);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("Category",Category,DbType.String);
            sp.Command.AddParameter("MedalURL",MedalURL,DbType.String);
            sp.Command.AddParameter("RibbonURL",RibbonURL,DbType.String);
            sp.Command.AddParameter("SmallMedalURL",SmallMedalURL,DbType.String);
            sp.Command.AddParameter("SmallRibbonURL",SmallRibbonURL,DbType.String);
            sp.Command.AddParameter("SmallMedalWidth",SmallMedalWidth,DbType.Int16);
            sp.Command.AddParameter("SmallMedalHeight",SmallMedalHeight,DbType.Int16);
            sp.Command.AddParameter("SmallRibbonWidth",SmallRibbonWidth,DbType.Int16);
            sp.Command.AddParameter("SmallRibbonHeight",SmallRibbonHeight,DbType.Int16);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Byte);
            sp.Command.AddParameter("Flags",Flags,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_approve(int MessageID){
            StoredProcedure sp=new StoredProcedure("yaf_message_approve",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_delete(int MessageID,bool EraseMessage){
            StoredProcedure sp=new StoredProcedure("yaf_message_delete",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("EraseMessage",EraseMessage,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_message_deleteundelete(int MessageID,bool isModeratorChanged,string DeleteReason,int isDeleteAction){
            StoredProcedure sp=new StoredProcedure("yaf_message_deleteundelete",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("isModeratorChanged",isModeratorChanged,DbType.Boolean);
            sp.Command.AddParameter("DeleteReason",DeleteReason,DbType.String);
            sp.Command.AddParameter("isDeleteAction",isDeleteAction,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_findunread(int TopicID,DateTime LastRead){
            StoredProcedure sp=new StoredProcedure("yaf_message_findunread",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("LastRead",LastRead,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_message_getReplies(int MessageID){
            StoredProcedure sp=new StoredProcedure("yaf_message_getReplies",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_list(int MessageID){
            StoredProcedure sp=new StoredProcedure("yaf_message_list",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_listreported(int MessageFlag,int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_message_listreported",this.Provider);
            sp.Command.AddParameter("MessageFlag",MessageFlag,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_move(int MessageID,int MoveToTopic){
            StoredProcedure sp=new StoredProcedure("yaf_message_move",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("MoveToTopic",MoveToTopic,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_reply_list(int MessageID){
            StoredProcedure sp=new StoredProcedure("yaf_message_reply_list",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_report(int ReportFlag,int MessageID,int ReporterID,DateTime ReportedDate){
            StoredProcedure sp=new StoredProcedure("yaf_message_report",this.Provider);
            sp.Command.AddParameter("ReportFlag",ReportFlag,DbType.Int32);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("ReporterID",ReporterID,DbType.Int32);
            sp.Command.AddParameter("ReportedDate",ReportedDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_message_reportcopyover(int MessageID){
            StoredProcedure sp=new StoredProcedure("yaf_message_reportcopyover",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_reportresolve(int MessageFlag,int MessageID,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_message_reportresolve",this.Provider);
            sp.Command.AddParameter("MessageFlag",MessageFlag,DbType.Int32);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_save(int TopicID,int UserID,string Message,string UserName,string IP,DateTime Posted,int ReplyTo,string BlogPostID,int Flags,int MessageID){
            StoredProcedure sp=new StoredProcedure("yaf_message_save",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("IP",IP,DbType.String);
            sp.Command.AddParameter("Posted",Posted,DbType.DateTime);
            sp.Command.AddParameter("ReplyTo",ReplyTo,DbType.Int32);
            sp.Command.AddParameter("BlogPostID",BlogPostID,DbType.String);
            sp.Command.AddParameter("Flags",Flags,DbType.Int32);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_simplelist(int StartID,int Limit){
            StoredProcedure sp=new StoredProcedure("yaf_message_simplelist",this.Provider);
            sp.Command.AddParameter("StartID",StartID,DbType.Int32);
            sp.Command.AddParameter("Limit",Limit,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_unapproved(int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_message_unapproved",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_message_update(int MessageID,int Priority,string Subject,int Flags,string Message,string Reason,bool IsModeratorChanged,bool OverrideApproval){
            StoredProcedure sp=new StoredProcedure("yaf_message_update",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("Priority",Priority,DbType.Int32);
            sp.Command.AddParameter("Subject",Subject,DbType.String);
            sp.Command.AddParameter("Flags",Flags,DbType.Int32);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("Reason",Reason,DbType.String);
            sp.Command.AddParameter("IsModeratorChanged",IsModeratorChanged,DbType.Boolean);
            sp.Command.AddParameter("OverrideApproval",OverrideApproval,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_nntpforum_delete(int NntpForumID){
            StoredProcedure sp=new StoredProcedure("yaf_nntpforum_delete",this.Provider);
            sp.Command.AddParameter("NntpForumID",NntpForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_nntpforum_list(int BoardID,int Minutes,int NntpForumID,bool Active){
            StoredProcedure sp=new StoredProcedure("yaf_nntpforum_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Minutes",Minutes,DbType.Int32);
            sp.Command.AddParameter("NntpForumID",NntpForumID,DbType.Int32);
            sp.Command.AddParameter("Active",Active,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_nntpforum_save(int NntpForumID,int NntpServerID,string GroupName,int ForumID,bool Active){
            StoredProcedure sp=new StoredProcedure("yaf_nntpforum_save",this.Provider);
            sp.Command.AddParameter("NntpForumID",NntpForumID,DbType.Int32);
            sp.Command.AddParameter("NntpServerID",NntpServerID,DbType.Int32);
            sp.Command.AddParameter("GroupName",GroupName,DbType.String);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("Active",Active,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_nntpforum_update(int NntpForumID,int LastMessageNo,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_nntpforum_update",this.Provider);
            sp.Command.AddParameter("NntpForumID",NntpForumID,DbType.Int32);
            sp.Command.AddParameter("LastMessageNo",LastMessageNo,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_nntpserver_delete(int NntpServerID){
            StoredProcedure sp=new StoredProcedure("yaf_nntpserver_delete",this.Provider);
            sp.Command.AddParameter("NntpServerID",NntpServerID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_nntpserver_list(int BoardID,int NntpServerID){
            StoredProcedure sp=new StoredProcedure("yaf_nntpserver_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("NntpServerID",NntpServerID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_nntpserver_save(int NntpServerID,int BoardID,string Name,string Address,int Port,string UserName,string UserPass){
            StoredProcedure sp=new StoredProcedure("yaf_nntpserver_save",this.Provider);
            sp.Command.AddParameter("NntpServerID",NntpServerID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("Address",Address,DbType.String);
            sp.Command.AddParameter("Port",Port,DbType.Int32);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("UserPass",UserPass,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_nntptopic_list(string Thread){
            StoredProcedure sp=new StoredProcedure("yaf_nntptopic_list",this.Provider);
            sp.Command.AddParameter("Thread",Thread,DbType.AnsiStringFixedLength);
            return sp;
        }
        public StoredProcedure yaf_nntptopic_savemessage(int NntpForumID,string Topic,string Body,int UserID,string UserName,string IP,DateTime Posted,string Thread){
            StoredProcedure sp=new StoredProcedure("yaf_nntptopic_savemessage",this.Provider);
            sp.Command.AddParameter("NntpForumID",NntpForumID,DbType.Int32);
            sp.Command.AddParameter("Topic",Topic,DbType.String);
            sp.Command.AddParameter("Body",Body,DbType.String);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("IP",IP,DbType.String);
            sp.Command.AddParameter("Posted",Posted,DbType.DateTime);
            sp.Command.AddParameter("Thread",Thread,DbType.AnsiStringFixedLength);
            return sp;
        }
        public StoredProcedure yaf_pageload(string SessionID,int BoardID,string UserKey,string IP,string Location,string Browser,string Platform,int CategoryID,int ForumID,int TopicID,int MessageID,bool DontTrack){
            StoredProcedure sp=new StoredProcedure("yaf_pageload",this.Provider);
            sp.Command.AddParameter("SessionID",SessionID,DbType.String);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserKey",UserKey,DbType.String);
            sp.Command.AddParameter("IP",IP,DbType.String);
            sp.Command.AddParameter("Location",Location,DbType.String);
            sp.Command.AddParameter("Browser",Browser,DbType.String);
            sp.Command.AddParameter("Platform",Platform,DbType.String);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("DontTrack",DontTrack,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_pmessage_archive(int UserPMessageID){
            StoredProcedure sp=new StoredProcedure("yaf_pmessage_archive",this.Provider);
            sp.Command.AddParameter("UserPMessageID",UserPMessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_pmessage_delete(int UserPMessageID,bool FromOutbox){
            StoredProcedure sp=new StoredProcedure("yaf_pmessage_delete",this.Provider);
            sp.Command.AddParameter("UserPMessageID",UserPMessageID,DbType.Int32);
            sp.Command.AddParameter("FromOutbox",FromOutbox,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_pmessage_info(){
            StoredProcedure sp=new StoredProcedure("yaf_pmessage_info",this.Provider);
            return sp;
        }
        public StoredProcedure yaf_pmessage_list(int FromUserID,int ToUserID,int UserPMessageID){
            StoredProcedure sp=new StoredProcedure("yaf_pmessage_list",this.Provider);
            sp.Command.AddParameter("FromUserID",FromUserID,DbType.Int32);
            sp.Command.AddParameter("ToUserID",ToUserID,DbType.Int32);
            sp.Command.AddParameter("UserPMessageID",UserPMessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_pmessage_markread(int UserPMessageID){
            StoredProcedure sp=new StoredProcedure("yaf_pmessage_markread",this.Provider);
            sp.Command.AddParameter("UserPMessageID",UserPMessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_pmessage_prune(int DaysRead,int DaysUnread){
            StoredProcedure sp=new StoredProcedure("yaf_pmessage_prune",this.Provider);
            sp.Command.AddParameter("DaysRead",DaysRead,DbType.Int32);
            sp.Command.AddParameter("DaysUnread",DaysUnread,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_pmessage_save(int FromUserID,int ToUserID,string Subject,string Body,int Flags){
            StoredProcedure sp=new StoredProcedure("yaf_pmessage_save",this.Provider);
            sp.Command.AddParameter("FromUserID",FromUserID,DbType.Int32);
            sp.Command.AddParameter("ToUserID",ToUserID,DbType.Int32);
            sp.Command.AddParameter("Subject",Subject,DbType.String);
            sp.Command.AddParameter("Body",Body,DbType.String);
            sp.Command.AddParameter("Flags",Flags,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_poll_remove(int PollID){
            StoredProcedure sp=new StoredProcedure("yaf_poll_remove",this.Provider);
            sp.Command.AddParameter("PollID",PollID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_poll_save(string Question,string Choice1,string Choice2,string Choice3,string Choice4,string Choice5,string Choice6,string Choice7,string Choice8,string Choice9,DateTime Closes){
            StoredProcedure sp=new StoredProcedure("yaf_poll_save",this.Provider);
            sp.Command.AddParameter("Question",Question,DbType.String);
            sp.Command.AddParameter("Choice1",Choice1,DbType.String);
            sp.Command.AddParameter("Choice2",Choice2,DbType.String);
            sp.Command.AddParameter("Choice3",Choice3,DbType.String);
            sp.Command.AddParameter("Choice4",Choice4,DbType.String);
            sp.Command.AddParameter("Choice5",Choice5,DbType.String);
            sp.Command.AddParameter("Choice6",Choice6,DbType.String);
            sp.Command.AddParameter("Choice7",Choice7,DbType.String);
            sp.Command.AddParameter("Choice8",Choice8,DbType.String);
            sp.Command.AddParameter("Choice9",Choice9,DbType.String);
            sp.Command.AddParameter("Closes",Closes,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_poll_stats(int PollID){
            StoredProcedure sp=new StoredProcedure("yaf_poll_stats",this.Provider);
            sp.Command.AddParameter("PollID",PollID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_poll_update(int PollID,string Question,DateTime Closes){
            StoredProcedure sp=new StoredProcedure("yaf_poll_update",this.Provider);
            sp.Command.AddParameter("PollID",PollID,DbType.Int32);
            sp.Command.AddParameter("Question",Question,DbType.String);
            sp.Command.AddParameter("Closes",Closes,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_pollvote_check(int PollID,int UserID,string RemoteIP){
            StoredProcedure sp=new StoredProcedure("yaf_pollvote_check",this.Provider);
            sp.Command.AddParameter("PollID",PollID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("RemoteIP",RemoteIP,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_post_last10user(int BoardID,int UserID,int PageUserID){
            StoredProcedure sp=new StoredProcedure("yaf_post_last10user",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("PageUserID",PageUserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_post_list(int TopicID,short UpdateViewCount,bool ShowDeleted){
            StoredProcedure sp=new StoredProcedure("yaf_post_list",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("UpdateViewCount",UpdateViewCount,DbType.Int16);
            sp.Command.AddParameter("ShowDeleted",ShowDeleted,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_post_list_reverse10(int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_post_list_reverse10",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_prov_changepassword(string ApplicationName,string Username,string Password,string PasswordSalt,string PasswordFormat,string PasswordAnswer){
            StoredProcedure sp=new StoredProcedure("yaf_prov_changepassword",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("Password",Password,DbType.String);
            sp.Command.AddParameter("PasswordSalt",PasswordSalt,DbType.String);
            sp.Command.AddParameter("PasswordFormat",PasswordFormat,DbType.String);
            sp.Command.AddParameter("PasswordAnswer",PasswordAnswer,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_changepasswordquestionandanswer(string ApplicationName,string Username,string PasswordQuestion,string PasswordAnswer){
            StoredProcedure sp=new StoredProcedure("yaf_prov_changepasswordquestionandanswer",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("PasswordQuestion",PasswordQuestion,DbType.String);
            sp.Command.AddParameter("PasswordAnswer",PasswordAnswer,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_createapplication(string ApplicationName,Guid ApplicationID){
            StoredProcedure sp=new StoredProcedure("yaf_prov_createapplication",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("ApplicationID",ApplicationID,DbType.Guid);
            return sp;
        }
        public StoredProcedure yaf_prov_createuser(string ApplicationName,string Username,string Password,string PasswordSalt,string PasswordFormat,string Email,string PasswordQuestion,string PasswordAnswer,bool IsApproved,string UserKey){
            StoredProcedure sp=new StoredProcedure("yaf_prov_createuser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("Password",Password,DbType.String);
            sp.Command.AddParameter("PasswordSalt",PasswordSalt,DbType.String);
            sp.Command.AddParameter("PasswordFormat",PasswordFormat,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            sp.Command.AddParameter("PasswordQuestion",PasswordQuestion,DbType.String);
            sp.Command.AddParameter("PasswordAnswer",PasswordAnswer,DbType.String);
            sp.Command.AddParameter("IsApproved",IsApproved,DbType.Boolean);
            sp.Command.AddParameter("UserKey",UserKey,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_deleteuser(string ApplicationName,string Username,bool DeleteAllRelated){
            StoredProcedure sp=new StoredProcedure("yaf_prov_deleteuser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("DeleteAllRelated",DeleteAllRelated,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_prov_findusersbyemail(string ApplicationName,string EmailAddress,int PageIndex,int PageSize){
            StoredProcedure sp=new StoredProcedure("yaf_prov_findusersbyemail",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("EmailAddress",EmailAddress,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_prov_findusersbyname(string ApplicationName,string Username,int PageIndex,int PageSize){
            StoredProcedure sp=new StoredProcedure("yaf_prov_findusersbyname",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_prov_getallusers(string ApplicationName,int PageIndex,int PageSize){
            StoredProcedure sp=new StoredProcedure("yaf_prov_getallusers",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_prov_getnumberofusersonline(string ApplicationName,int TimeWindow,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("yaf_prov_getnumberofusersonline",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("TimeWindow",TimeWindow,DbType.Int32);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_prov_getuser(string ApplicationName,string Username,string UserKey,bool UserIsOnline){
            StoredProcedure sp=new StoredProcedure("yaf_prov_getuser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("UserKey",UserKey,DbType.String);
            sp.Command.AddParameter("UserIsOnline",UserIsOnline,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_prov_getusernamebyemail(string ApplicationName,string Email){
            StoredProcedure sp=new StoredProcedure("yaf_prov_getusernamebyemail",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_profile_deleteinactive(string ApplicationName,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("yaf_prov_profile_deleteinactive",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_prov_profile_deleteprofiles(string ApplicationName,string UserNames){
            StoredProcedure sp=new StoredProcedure("yaf_prov_profile_deleteprofiles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserNames",UserNames,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_profile_getnumberinactiveprofiles(string ApplicationName,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("yaf_prov_profile_getnumberinactiveprofiles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_prov_profile_getprofiles(string ApplicationName,int PageIndex,int PageSize,string UserNameToMatch,DateTime InactiveSinceDate){
            StoredProcedure sp=new StoredProcedure("yaf_prov_profile_getprofiles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("PageIndex",PageIndex,DbType.Int32);
            sp.Command.AddParameter("PageSize",PageSize,DbType.Int32);
            sp.Command.AddParameter("UserNameToMatch",UserNameToMatch,DbType.String);
            sp.Command.AddParameter("InactiveSinceDate",InactiveSinceDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_prov_resetpassword(string ApplicationName,string UserName,string Password,string PasswordSalt,string PasswordFormat,int MaxInvalidAttempts,int PasswordAttemptWindow,DateTime CurrentTimeUtc){
            StoredProcedure sp=new StoredProcedure("yaf_prov_resetpassword",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Password",Password,DbType.String);
            sp.Command.AddParameter("PasswordSalt",PasswordSalt,DbType.String);
            sp.Command.AddParameter("PasswordFormat",PasswordFormat,DbType.String);
            sp.Command.AddParameter("MaxInvalidAttempts",MaxInvalidAttempts,DbType.Int32);
            sp.Command.AddParameter("PasswordAttemptWindow",PasswordAttemptWindow,DbType.Int32);
            sp.Command.AddParameter("CurrentTimeUtc",CurrentTimeUtc,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_prov_role_addusertorole(string ApplicationName,string Username,string Rolename){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_addusertorole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("Rolename",Rolename,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_role_createrole(string ApplicationName,string Rolename){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_createrole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Rolename",Rolename,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_role_deleterole(string ApplicationName,string Rolename,bool DeleteOnlyIfRoleIsEmpty){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_deleterole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Rolename",Rolename,DbType.String);
            sp.Command.AddParameter("DeleteOnlyIfRoleIsEmpty",DeleteOnlyIfRoleIsEmpty,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_prov_role_exists(string ApplicationName,string Rolename){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_exists",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Rolename",Rolename,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_role_findusersinrole(string ApplicationName,string Rolename){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_findusersinrole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Rolename",Rolename,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_role_getroles(string ApplicationName,string Username){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_getroles",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_role_isuserinrole(string ApplicationName,string Username,string Rolename){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_isuserinrole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("Rolename",Rolename,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_role_removeuserfromrole(string ApplicationName,string Username,string Rolename){
            StoredProcedure sp=new StoredProcedure("yaf_prov_role_removeuserfromrole",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("Username",Username,DbType.String);
            sp.Command.AddParameter("Rolename",Rolename,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_unlockuser(string ApplicationName,string UserName){
            StoredProcedure sp=new StoredProcedure("yaf_prov_unlockuser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_prov_updateuser(string ApplicationName,string UserKey,string UserName,string Email,string Comment,bool IsApproved,DateTime LastLogin,DateTime LastActivity,bool UniqueEmail){
            StoredProcedure sp=new StoredProcedure("yaf_prov_updateuser",this.Provider);
            sp.Command.AddParameter("ApplicationName",ApplicationName,DbType.String);
            sp.Command.AddParameter("UserKey",UserKey,DbType.String);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            sp.Command.AddParameter("Comment",Comment,DbType.AnsiString);
            sp.Command.AddParameter("IsApproved",IsApproved,DbType.Boolean);
            sp.Command.AddParameter("LastLogin",LastLogin,DbType.DateTime);
            sp.Command.AddParameter("LastActivity",LastActivity,DbType.DateTime);
            sp.Command.AddParameter("UniqueEmail",UniqueEmail,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_prov_upgrade(int PreviousVersion,int NewVersion){
            StoredProcedure sp=new StoredProcedure("yaf_prov_upgrade",this.Provider);
            sp.Command.AddParameter("PreviousVersion",PreviousVersion,DbType.Int32);
            sp.Command.AddParameter("NewVersion",NewVersion,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_rank_delete(int RankID){
            StoredProcedure sp=new StoredProcedure("yaf_rank_delete",this.Provider);
            sp.Command.AddParameter("RankID",RankID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_rank_list(int BoardID,int RankID){
            StoredProcedure sp=new StoredProcedure("yaf_rank_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("RankID",RankID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_rank_save(int RankID,int BoardID,string Name,bool IsStart,bool IsLadder,int MinPosts,string RankImage,int PMLimit,string Style,short SortOrder){
            StoredProcedure sp=new StoredProcedure("yaf_rank_save",this.Provider);
            sp.Command.AddParameter("RankID",RankID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("IsStart",IsStart,DbType.Boolean);
            sp.Command.AddParameter("IsLadder",IsLadder,DbType.Boolean);
            sp.Command.AddParameter("MinPosts",MinPosts,DbType.Int32);
            sp.Command.AddParameter("RankImage",RankImage,DbType.String);
            sp.Command.AddParameter("PMLimit",PMLimit,DbType.Int32);
            sp.Command.AddParameter("Style",Style,DbType.String);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Int16);
            return sp;
        }
        public StoredProcedure yaf_registry_list(string Name,int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_registry_list",this.Provider);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_registry_save(string Name,string Value,int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_registry_save",this.Provider);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("Value",Value,DbType.String);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_replace_words_delete(int ID){
            StoredProcedure sp=new StoredProcedure("yaf_replace_words_delete",this.Provider);
            sp.Command.AddParameter("ID",ID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_replace_words_list(int BoardID,int ID){
            StoredProcedure sp=new StoredProcedure("yaf_replace_words_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("ID",ID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_replace_words_save(int BoardID,int ID,string BadWord,string GoodWord){
            StoredProcedure sp=new StoredProcedure("yaf_replace_words_save",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("ID",ID,DbType.Int32);
            sp.Command.AddParameter("BadWord",BadWord,DbType.String);
            sp.Command.AddParameter("GoodWord",GoodWord,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_shoutbox_clearmessages(){
            StoredProcedure sp=new StoredProcedure("yaf_shoutbox_clearmessages",this.Provider);
            return sp;
        }
        public StoredProcedure yaf_shoutbox_getmessages(int NumberOfMessages){
            StoredProcedure sp=new StoredProcedure("yaf_shoutbox_getmessages",this.Provider);
            sp.Command.AddParameter("NumberOfMessages",NumberOfMessages,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_shoutbox_savemessage(string UserName,int UserID,string Message,DateTime Date,string IP){
            StoredProcedure sp=new StoredProcedure("yaf_shoutbox_savemessage",this.Provider);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("Date",Date,DbType.DateTime);
            sp.Command.AddParameter("IP",IP,DbType.AnsiString);
            return sp;
        }
        public StoredProcedure yaf_smiley_delete(int SmileyID){
            StoredProcedure sp=new StoredProcedure("yaf_smiley_delete",this.Provider);
            sp.Command.AddParameter("SmileyID",SmileyID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_smiley_list(int BoardID,int SmileyID){
            StoredProcedure sp=new StoredProcedure("yaf_smiley_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("SmileyID",SmileyID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_smiley_listunique(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_smiley_listunique",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_smiley_resort(int BoardID,int SmileyID,int Move){
            StoredProcedure sp=new StoredProcedure("yaf_smiley_resort",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("SmileyID",SmileyID,DbType.Int32);
            sp.Command.AddParameter("Move",Move,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_smiley_save(int SmileyID,int BoardID,string Code,string Icon,string Emoticon,byte SortOrder,short Replace){
            StoredProcedure sp=new StoredProcedure("yaf_smiley_save",this.Provider);
            sp.Command.AddParameter("SmileyID",SmileyID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Code",Code,DbType.String);
            sp.Command.AddParameter("Icon",Icon,DbType.String);
            sp.Command.AddParameter("Emoticon",Emoticon,DbType.String);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Byte);
            sp.Command.AddParameter("Replace",Replace,DbType.Int16);
            return sp;
        }
        public StoredProcedure yaf_system_initialize(string Name,int TimeZone,string ForumEmail,string SmtpServer,string User,string Userkey){
            StoredProcedure sp=new StoredProcedure("yaf_system_initialize",this.Provider);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("TimeZone",TimeZone,DbType.Int32);
            sp.Command.AddParameter("ForumEmail",ForumEmail,DbType.String);
            sp.Command.AddParameter("SmtpServer",SmtpServer,DbType.String);
            sp.Command.AddParameter("User",User,DbType.String);
            sp.Command.AddParameter("Userkey",Userkey,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_system_updateversion(int Version,string VersionName){
            StoredProcedure sp=new StoredProcedure("yaf_system_updateversion",this.Provider);
            sp.Command.AddParameter("Version",Version,DbType.Int32);
            sp.Command.AddParameter("VersionName",VersionName,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_topic_active(int BoardID,int UserID,DateTime Since,int CategoryID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_active",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Since",Since,DbType.DateTime);
            sp.Command.AddParameter("CategoryID",CategoryID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_announcements(int BoardID,int NumPosts,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_announcements",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("NumPosts",NumPosts,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_create_by_message(int MessageID,int ForumID,string Subject){
            StoredProcedure sp=new StoredProcedure("yaf_topic_create_by_message",this.Provider);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("Subject",Subject,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_topic_delete(int TopicID,bool UpdateLastPost,bool EraseTopic){
            StoredProcedure sp=new StoredProcedure("yaf_topic_delete",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("UpdateLastPost",UpdateLastPost,DbType.Boolean);
            sp.Command.AddParameter("EraseTopic",EraseTopic,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_topic_findnext(int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_findnext",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_findprev(int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_findprev",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_info(int TopicID,bool ShowDeleted){
            StoredProcedure sp=new StoredProcedure("yaf_topic_info",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("ShowDeleted",ShowDeleted,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_topic_latest(int BoardID,int NumPosts,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_latest",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("NumPosts",NumPosts,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_list(int ForumID,int UserID,short Announcement,DateTime Date,int Offset,int Count){
            StoredProcedure sp=new StoredProcedure("yaf_topic_list",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Announcement",Announcement,DbType.Int16);
            sp.Command.AddParameter("Date",Date,DbType.DateTime);
            sp.Command.AddParameter("Offset",Offset,DbType.Int32);
            sp.Command.AddParameter("Count",Count,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_listmessages(int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_listmessages",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_lock(int TopicID,bool Locked){
            StoredProcedure sp=new StoredProcedure("yaf_topic_lock",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("Locked",Locked,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_topic_move(int TopicID,int ForumID,bool ShowMoved){
            StoredProcedure sp=new StoredProcedure("yaf_topic_move",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("ShowMoved",ShowMoved,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_topic_poll_update(int TopicID,int MessageID,int PollID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_poll_update",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("MessageID",MessageID,DbType.Int32);
            sp.Command.AddParameter("PollID",PollID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_prune(int BoardID,int ForumID,int Days,bool PermDelete){
            StoredProcedure sp=new StoredProcedure("yaf_topic_prune",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("Days",Days,DbType.Int32);
            sp.Command.AddParameter("PermDelete",PermDelete,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_topic_save(int ForumID,string Subject,int UserID,string Message,short Priority,string UserName,string IP,int PollID,DateTime Posted,string BlogPostID,int Flags){
            StoredProcedure sp=new StoredProcedure("yaf_topic_save",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("Subject",Subject,DbType.String);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("Priority",Priority,DbType.Int16);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("IP",IP,DbType.String);
            sp.Command.AddParameter("PollID",PollID,DbType.Int32);
            sp.Command.AddParameter("Posted",Posted,DbType.DateTime);
            sp.Command.AddParameter("BlogPostID",BlogPostID,DbType.String);
            sp.Command.AddParameter("Flags",Flags,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_simplelist(int StartID,int Limit){
            StoredProcedure sp=new StoredProcedure("yaf_topic_simplelist",this.Provider);
            sp.Command.AddParameter("StartID",StartID,DbType.Int32);
            sp.Command.AddParameter("Limit",Limit,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_topic_updatelastpost(int ForumID,int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_topic_updatelastpost",this.Provider);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_accessmasks(int BoardID,int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_accessmasks",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_activity_rank(int BoardID,int DisplayNumber,DateTime StartDate){
            StoredProcedure sp=new StoredProcedure("yaf_user_activity_rank",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("DisplayNumber",DisplayNumber,DbType.Int32);
            sp.Command.AddParameter("StartDate",StartDate,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_user_addignoreduser(int UserId,int IgnoredUserId){
            StoredProcedure sp=new StoredProcedure("yaf_user_addignoreduser",this.Provider);
            sp.Command.AddParameter("UserId",UserId,DbType.Int32);
            sp.Command.AddParameter("IgnoredUserId",IgnoredUserId,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_addpoints(int UserID,int Points){
            StoredProcedure sp=new StoredProcedure("yaf_user_addpoints",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Points",Points,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_adminsave(int BoardID,int UserID,string Name,string Email,int Flags,int RankID){
            StoredProcedure sp=new StoredProcedure("yaf_user_adminsave",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            sp.Command.AddParameter("Flags",Flags,DbType.Int32);
            sp.Command.AddParameter("RankID",RankID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_approve(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_approve",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_approveall(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_user_approveall",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_aspnet(int BoardID,string UserName,string Email,string ProviderUserKey,bool IsApproved){
            StoredProcedure sp=new StoredProcedure("yaf_user_aspnet",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            sp.Command.AddParameter("ProviderUserKey",ProviderUserKey,DbType.String);
            sp.Command.AddParameter("IsApproved",IsApproved,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_user_avatarimage(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_avatarimage",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_changepassword(int UserID,string OldPassword,string NewPassword){
            StoredProcedure sp=new StoredProcedure("yaf_user_changepassword",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("OldPassword",OldPassword,DbType.String);
            sp.Command.AddParameter("NewPassword",NewPassword,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_delete(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_delete",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_deleteavatar(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_deleteavatar",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_deleteold(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_user_deleteold",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_emails(int BoardID,int GroupID){
            StoredProcedure sp=new StoredProcedure("yaf_user_emails",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_find(int BoardID,bool Filter,string UserName,string Email){
            StoredProcedure sp=new StoredProcedure("yaf_user_find",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Filter",Filter,DbType.Boolean);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_getpoints(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_getpoints",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_getsignature(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_getsignature",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_guest(int BoardID){
            StoredProcedure sp=new StoredProcedure("yaf_user_guest",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_ignoredlist(int UserId){
            StoredProcedure sp=new StoredProcedure("yaf_user_ignoredlist",this.Provider);
            sp.Command.AddParameter("UserId",UserId,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_isuserignored(int UserId,int IgnoredUserId){
            StoredProcedure sp=new StoredProcedure("yaf_user_isuserignored",this.Provider);
            sp.Command.AddParameter("UserId",UserId,DbType.Int32);
            sp.Command.AddParameter("IgnoredUserId",IgnoredUserId,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_list(int BoardID,int UserID,bool Approved,int GroupID,int RankID){
            StoredProcedure sp=new StoredProcedure("yaf_user_list",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Approved",Approved,DbType.Boolean);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            sp.Command.AddParameter("RankID",RankID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_listmedals(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_listmedals",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_login(int BoardID,string Name,string Password){
            StoredProcedure sp=new StoredProcedure("yaf_user_login",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("Name",Name,DbType.String);
            sp.Command.AddParameter("Password",Password,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_medal_delete(int UserID,int MedalID){
            StoredProcedure sp=new StoredProcedure("yaf_user_medal_delete",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_medal_list(int UserID,int MedalID){
            StoredProcedure sp=new StoredProcedure("yaf_user_medal_list",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_medal_save(int UserID,int MedalID,string Message,bool Hide,bool OnlyRibbon,byte SortOrder,DateTime DateAwarded){
            StoredProcedure sp=new StoredProcedure("yaf_user_medal_save",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("MedalID",MedalID,DbType.Int32);
            sp.Command.AddParameter("Message",Message,DbType.String);
            sp.Command.AddParameter("Hide",Hide,DbType.Boolean);
            sp.Command.AddParameter("OnlyRibbon",OnlyRibbon,DbType.Boolean);
            sp.Command.AddParameter("SortOrder",SortOrder,DbType.Byte);
            sp.Command.AddParameter("DateAwarded",DateAwarded,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_user_migrate(int UserID,string ProviderUserKey,bool UpdateProvider){
            StoredProcedure sp=new StoredProcedure("yaf_user_migrate",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("ProviderUserKey",ProviderUserKey,DbType.String);
            sp.Command.AddParameter("UpdateProvider",UpdateProvider,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_user_nntp(int BoardID,string UserName,string Email){
            StoredProcedure sp=new StoredProcedure("yaf_user_nntp",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_pmcount(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_pmcount",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_recoverpassword(int BoardID,string UserName,string Email){
            StoredProcedure sp=new StoredProcedure("yaf_user_recoverpassword",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_removeignoreduser(int UserId,int IgnoredUserId){
            StoredProcedure sp=new StoredProcedure("yaf_user_removeignoreduser",this.Provider);
            sp.Command.AddParameter("UserId",UserId,DbType.Int32);
            sp.Command.AddParameter("IgnoredUserId",IgnoredUserId,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_removepoints(int UserID,int Points){
            StoredProcedure sp=new StoredProcedure("yaf_user_removepoints",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Points",Points,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_removepointsbytopicid(int TopicID,int Points){
            StoredProcedure sp=new StoredProcedure("yaf_user_removepointsbytopicid",this.Provider);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            sp.Command.AddParameter("Points",Points,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_resetpoints(){
            StoredProcedure sp=new StoredProcedure("yaf_user_resetpoints",this.Provider);
            return sp;
        }
        public StoredProcedure yaf_user_save(int UserID,int BoardID,string UserName,string Email,int TimeZone,string LanguageFile,string ThemeFile,bool OverrideDefaultTheme,bool Approved,bool PMNotification,string ProviderUserKey){
            StoredProcedure sp=new StoredProcedure("yaf_user_save",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("UserName",UserName,DbType.String);
            sp.Command.AddParameter("Email",Email,DbType.String);
            sp.Command.AddParameter("TimeZone",TimeZone,DbType.Int32);
            sp.Command.AddParameter("LanguageFile",LanguageFile,DbType.String);
            sp.Command.AddParameter("ThemeFile",ThemeFile,DbType.String);
            sp.Command.AddParameter("OverrideDefaultTheme",OverrideDefaultTheme,DbType.Boolean);
            sp.Command.AddParameter("Approved",Approved,DbType.Boolean);
            sp.Command.AddParameter("PMNotification",PMNotification,DbType.Boolean);
            sp.Command.AddParameter("ProviderUserKey",ProviderUserKey,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_saveavatar(int UserID,string Avatar,byte[] AvatarImage,string AvatarImageType){
            StoredProcedure sp=new StoredProcedure("yaf_user_saveavatar",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Avatar",Avatar,DbType.String);
            sp.Command.AddParameter("AvatarImage",AvatarImage,DbType.Binary);
            sp.Command.AddParameter("AvatarImageType",AvatarImageType,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_savepassword(int UserID,string Password){
            StoredProcedure sp=new StoredProcedure("yaf_user_savepassword",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Password",Password,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_savesignature(int UserID,string Signature){
            StoredProcedure sp=new StoredProcedure("yaf_user_savesignature",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Signature",Signature,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_setpoints(int UserID,int Points){
            StoredProcedure sp=new StoredProcedure("yaf_user_setpoints",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Points",Points,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_setrole(int BoardID,string ProviderUserKey,string Role){
            StoredProcedure sp=new StoredProcedure("yaf_user_setrole",this.Provider);
            sp.Command.AddParameter("BoardID",BoardID,DbType.Int32);
            sp.Command.AddParameter("ProviderUserKey",ProviderUserKey,DbType.String);
            sp.Command.AddParameter("Role",Role,DbType.String);
            return sp;
        }
        public StoredProcedure yaf_user_simplelist(int StartID,int Limit){
            StoredProcedure sp=new StoredProcedure("yaf_user_simplelist",this.Provider);
            sp.Command.AddParameter("StartID",StartID,DbType.Int32);
            sp.Command.AddParameter("Limit",Limit,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_user_suspend(int UserID,DateTime Suspend){
            StoredProcedure sp=new StoredProcedure("yaf_user_suspend",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("Suspend",Suspend,DbType.DateTime);
            return sp;
        }
        public StoredProcedure yaf_user_upgrade(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_user_upgrade",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_userforum_delete(int UserID,int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_userforum_delete",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_userforum_list(int UserID,int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_userforum_list",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_userforum_save(int UserID,int ForumID,int AccessMaskID){
            StoredProcedure sp=new StoredProcedure("yaf_userforum_save",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            sp.Command.AddParameter("AccessMaskID",AccessMaskID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_usergroup_list(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_usergroup_list",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_usergroup_save(int UserID,int GroupID,bool Member){
            StoredProcedure sp=new StoredProcedure("yaf_usergroup_save",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("GroupID",GroupID,DbType.Int32);
            sp.Command.AddParameter("Member",Member,DbType.Boolean);
            return sp;
        }
        public StoredProcedure yaf_userpmessage_delete(int UserPMessageID){
            StoredProcedure sp=new StoredProcedure("yaf_userpmessage_delete",this.Provider);
            sp.Command.AddParameter("UserPMessageID",UserPMessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_userpmessage_list(int UserPMessageID){
            StoredProcedure sp=new StoredProcedure("yaf_userpmessage_list",this.Provider);
            sp.Command.AddParameter("UserPMessageID",UserPMessageID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchforum_add(int UserID,int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_watchforum_add",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchforum_check(int UserID,int ForumID){
            StoredProcedure sp=new StoredProcedure("yaf_watchforum_check",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("ForumID",ForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchforum_delete(int WatchForumID){
            StoredProcedure sp=new StoredProcedure("yaf_watchforum_delete",this.Provider);
            sp.Command.AddParameter("WatchForumID",WatchForumID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchforum_list(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_watchforum_list",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchtopic_add(int UserID,int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_watchtopic_add",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchtopic_check(int UserID,int TopicID){
            StoredProcedure sp=new StoredProcedure("yaf_watchtopic_check",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            sp.Command.AddParameter("TopicID",TopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchtopic_delete(int WatchTopicID){
            StoredProcedure sp=new StoredProcedure("yaf_watchtopic_delete",this.Provider);
            sp.Command.AddParameter("WatchTopicID",WatchTopicID,DbType.Int32);
            return sp;
        }
        public StoredProcedure yaf_watchtopic_list(int UserID){
            StoredProcedure sp=new StoredProcedure("yaf_watchtopic_list",this.Provider);
            sp.Command.AddParameter("UserID",UserID,DbType.Int32);
            return sp;
        }
	
	}
	
}
 