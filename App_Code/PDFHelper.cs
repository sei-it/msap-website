﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text.pdf;
using iTextSharp.text;

/// <summary>
/// Summary description for PDFHelper
/// </summary>
public class PDFFooter : PdfPageEventHelper
{
    public PDFFooter()
    {
    }

    // This is the contentbyte object of the writer
    PdfContentByte cb;

    // this is the BaseFont we are going to use for the header / footer
    BaseFont bf = null;

    // we will put the final number of pages in a template
    PdfTemplate template;

    private Font _FooterFont;
    public Font FooterFont
    {
        get { return _FooterFont; }
        set { _FooterFont = value; }
    }

    private string _PRAward;
    public string PRAward
    {
        get { return _PRAward; }
        set { _PRAward = value; }
    }

    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
        try
        {
            bf = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb = writer.DirectContent;
            //template = cb.CreateTemplate(50, 50);
        }
        catch (DocumentException de)
        {
        }
        catch (System.IO.IOException ioe)
        {
        }
    }

    public override void OnStartPage(PdfWriter writer, Document document)
    {
        base.OnStartPage(writer, document);
    }

    public override void OnEndPage(PdfWriter writer, Document document)
    {
        base.OnEndPage(writer, document);

        Rectangle pageSize = document.PageSize;

        int pageN = writer.PageNumber;
        if (!string.IsNullOrEmpty(PRAward) && pageN != 1)
        {
            String text = "Page " + pageN;
            float len = bf.GetWidthPoint(text, 9);

            cb.BeginText();
            cb.SetFontAndSize(bf, 9);
            cb.SetTextMatrix(pageSize.Width - Convert.ToInt32(len) - 10, pageSize.GetBottom(5));
            cb.ShowText(text);

            text = "PR/Award #" + PRAward;
            len = bf.GetWidthPoint(text, 9);
            cb.SetTextMatrix(pageSize.Width - Convert.ToInt32(len) - 10, pageSize.GetTop(10));
            cb.ShowText(text);

            cb.EndText();
        }
    }

    public override void OnCloseDocument(PdfWriter writer, Document document)
    {
        base.OnCloseDocument(writer, document);
    }
}