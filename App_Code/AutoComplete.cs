﻿// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License.
// See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
// All other rights reserved.
using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Synergy.Magnet;

///<summary>
/// Summary description for AutoComplete
///</summary>

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, 
// uncomment the following line.
[System.Web.Script.Services.ScriptService]
public class AutoComplete : System.Web.Services.WebService
{

    public AutoComplete()
    {

        //Uncomment the following line if using designed components
        //InitializeComponent();
    }

    [WebMethod]
    public string[] GetCompletionList(string prefixText, int count)
    {
        UserProfileDataContext db = new UserProfileDataContext();

        return db.yaf_prov_Profiles.Where(n => n.LastName.StartsWith(prefixText)).OrderBy(n => n.LastName).Select(n => n.LastName + ", " + n.FirstName).Take(count).Distinct().ToArray<string>();

    }

    [WebMethod]
    public string[] GetMagnetUserList(string prefixText, int count)
    {
        return MagnetGranteeUser.All().Where(n => n.UserName.StartsWith(prefixText)).OrderBy(n => n.UserName).Select(n => n.UserName).Take(count).Distinct().ToArray<string>();
    }
}