﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Sockets;
using Synergy.Magnet;
using System.Linq.Dynamic;
using System.Collections;
using System.Data;
using System.Net.Mail;

namespace Synergy.Magnet
{
    public class IWGridItem : object
    {
        public IWGridItem(string name, int column, int columnspan, string text,
            bool visible, HorizontalAlign horizontalalign, string classoverride, bool? sortable,
            string sortexpression)
        {
            Name = name;
            Column = column;
            ColumnSpan = columnspan;
            Text = text;
            Visible = visible;
            HorizontalAlign = horizontalalign;
            ClassOverride = classoverride;
            Sortable = sortable;
            SortExpression = sortexpression;
        }
        public string Name { get; set; }
        public int Column { get; set; }
        public int ColumnSpan { get; set; }
        public string Text { get; set; }
        public bool Visible { get; set; }
        public HorizontalAlign HorizontalAlign { get; set; }
        public string ClassOverride { get; set; }
        public bool? Sortable { get; set; }
        public string SortExpression { get; set; }
    }
    public class ManageUtility
    {
        public static string FormatInteger(int seed)
        {
            string ret = "";
            if( seed != null)
            {
                ret = String.Format("{0:n0}", seed);
            }
            return ret;
        }
        public static string RandomNumbers(int length)
        {
            string result = string.Empty;
            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                char ch = Convert.ToChar(random.Next(97, 122));
                result += ch;
            }
            return result;
        }
        public static string FormatIntegerString(string str)
        {
            if (string.IsNullOrEmpty(str)) return str;
            string[] strs = str.Split(',');
            string ret = "";
            foreach (string tmp in strs)
                ret += tmp;
            return ret;
        }
        public static string PaddingString(string str)
        {
            string ret = str.Length == 1 ? "0" + str : str;
            return ret;
        }
        public static string FormatPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return "";

            string ret = "(";
            if (phoneNumber.Length >= 3)
            {
                ret += phoneNumber.Substring(0, 3) + ")&nbsp;";

                if (phoneNumber.Length >= 6)
                {
                    ret += phoneNumber.Substring(3, 3) + "-";
                    ret += phoneNumber.Substring(6, phoneNumber.Length - 6);
                }
                else
                {
                    ret += phoneNumber.Substring(3, phoneNumber.Length - 3);
                }
            }
            else
                ret += phoneNumber + ")&nbsp;";

            return ret;
        }
        public static string FormatDatetime(string datetime)
        {
            if (string.IsNullOrEmpty(datetime)) return "";

            string ret = "";
            if (datetime.Length >= 2)
            {
                ret = datetime.Substring(0, 2) + "/";
                if (datetime.Length >= 4)
                {
                    ret += datetime.Substring(2, 2) + "/" + datetime.Substring(4, datetime.Length - 4);
                }
                else
                {
                    ret += datetime.Substring(2, datetime.Length - 2);
                }
            }
            else
                ret = datetime;

            return ret;
        }
        public class PublicationData
        {
            public int ID { get; set; }
            public string DisplayData { get; set; }
        }
        public static int ComparePublicationByDate(MagnetPublication x, MagnetPublication y)
        {
            int year1 = 0;
            int year2 = 0;
            if (int.TryParse(x.PublicationDate, out year1) && int.TryParse(y.PublicationDate, out year2))
            {
                if (year1 == year2)
                    return x.PublicationTitle.CompareTo(y.PublicationTitle);
                else if (year1 > year2)
                    return -1;
                else
                    return 1;
            }
            else
                return 0;
        }
        public static List<PublicationData> SearchAllKeyword(string searchwords)
        {
            List<PublicationData> retData = new List<PublicationData>();
            //string[] strs = searchwords.Split(' ');
            //if (strs.Count() > 0)
            //{
            string strQuery = "";
            //foreach (string keyword in strs)
            //{
            if (!string.IsNullOrEmpty(searchwords))
            {
                if (string.IsNullOrEmpty(strQuery))
                {
                    strQuery += "  PublicationKeyword.Contains(\"" + searchwords + "\")  ";

                }
                //else
                //{
                //    strQuery += " && PublicationKeyword.Contains(\"" + searchwords + "\")  ";
                //}
            }
            //}

            strQuery = string.IsNullOrEmpty(strQuery) ? "" : strQuery + " || ";
            if (!string.IsNullOrEmpty(searchwords))
            {
                strQuery += " PublicationTitle.StartsWith( \"" + searchwords + " \") || ";
                strQuery += " PublicationTitle.Contains( \" " + searchwords + " \") || ";
                strQuery += " PublicationTitle.EndsWith( \" " + searchwords + "\") ";
            }

            strQuery = string.IsNullOrEmpty(strQuery) ? "" : strQuery + " || ";

            if (!string.IsNullOrEmpty(searchwords))
            {
                strQuery += " Organization.StartsWith( \"" + searchwords + " \") || ";
                strQuery += " Organization.Contains( \" " + searchwords + " \") || ";
                strQuery += " Organization.EndsWith( \" " + searchwords + "\") ";
            }

            strQuery = string.IsNullOrEmpty(strQuery) ? "" : strQuery + " || ";

            if (!string.IsNullOrEmpty(searchwords))
            {
                strQuery += " Description.StartsWith( \"" + searchwords + " \") || ";
                strQuery += " Description.Contains( \" " + searchwords + " \") || ";
                strQuery += " Description.EndsWith( \" " + searchwords + "\") ";
            }

            MagnetDBDB db = new MagnetDBDB();
            var data = db.MagnetPublications.Where(strQuery).ToList();
            data.Sort(ComparePublicationByDate);
            foreach (var publication in data)
            {
                string str = string.Format("<b>{0}</b><br /><b>Organization</b>: {1}<br /><b>Date</b>: {2}<br /><b>Description</b>: {3}",
                    publication.PublicationTitle,
                    publication.Organization,
                    publication.PublicationDate,
                    publication.Description);

                if (!string.IsNullOrEmpty(publication.OriginalFileName))
                {
                    str += "<br /><a href='applicationdoc/" + publication.PhysicalFileName + "' target='_blank'><img border=0 src='images/PDFlogo.jpg'> <b>Download PDF</b></a>";
                }

                retData.Add(new PublicationData()
                {
                    ID = publication.ID,
                    DisplayData = str
                });
            }
            //}
            return retData;
        }
        public static List<PublicationData> SearchKeyword(string sentance, string resourcetype, string topics)
        {
            List<PublicationData> retData = new List<PublicationData>();
            string strs = sentance.ToLower().Replace("'", "''").Trim();

            string strQuery = "";

            if (!string.IsNullOrEmpty(resourcetype))
                strQuery += " PublicationType = '" + resourcetype + "' ";

            if (!string.IsNullOrEmpty(topics))
            {
                if (!string.IsNullOrEmpty(strQuery))
                    strQuery += " AND ";

                strQuery += "  PublicationTopic IN (" + topics + ") ";
            }

            DataTable wholewordtbl = getMatchIndex(strQuery, strs);
            string excludeIDs = "";
            foreach (DataRow dr in wholewordtbl.Rows)
                excludeIDs += dr["ID"] + ",";

            if (!string.IsNullOrEmpty(excludeIDs))
            {
                excludeIDs = excludeIDs.Substring(0, excludeIDs.Length - 1);
                if (!string.IsNullOrEmpty(strQuery))
                    strQuery += " AND ";

                strQuery += "  ID NOT IN (" + excludeIDs + ") ";
            }

            string newPhrase = getExcludePhrase(strs);

            if (!string.IsNullOrEmpty(newPhrase))
            {
                string[] keywords = strs.Split(' ');
                if (!string.IsNullOrEmpty(strQuery))
                    strQuery += " AND ( ";
                else
                    strQuery += " ( ";
                foreach (string word in keywords)
                {
                    string tmp = word.Replace("'", "''").Trim();
                    strQuery += " ( PublicationTitle like '%" + tmp + "%' " +
                                " OR  Organization  like '%" + tmp + "%' " +
                                " OR  PublicationKeyword like '%" + tmp + "%' " +
                                " OR  Description like '%" + tmp + "%' " +
                                ") OR ";
                }

                strQuery = strQuery.Substring(0, strQuery.LastIndexOf("OR")) + ")";
            }
            

            ResourceDataClassesDataContext db = new ResourceDataClassesDataContext();
            DataTable data=  db.spp_getResource(strQuery).ToDataTable();

            wholewordtbl.Merge(data);


            foreach (DataRow publication in wholewordtbl.Rows)
            {
                string str = string.Format("<b>{0}</b><br /><b>Organization</b>: {1}<br /><b>Date</b>: {2}<br /><b>Description</b>: {3}",
                    publication["PublicationTitle"],
                    publication["Organization"],
                    publication["PublicationDate"],
                    publication["Description"]);

                if (!string.IsNullOrEmpty(publication["OriginalFileName"].ToString()))
                {
                    str += "<br /><a href='applicationdoc/" + publication["PhysicalFileName"] + "' target='_blank'><img border=0 src='images/PDFlogo.jpg'> <b>Download PDF</b></a>";
                }

                retData.Add(new PublicationData()
                {
                    ID = Convert.ToInt32(publication["ID"]),
                    DisplayData = str
                });
            }


            return retData;
        }

        private static string getExcludePhrase(string strs)
        {
            string[] excludeKeywords = { "is", "a", "has", "in", "for", "have", "not", ":" }; //, ",", "."
            string[] anywords = strs.Trim().Split(' ');
            string returnstr="";

            foreach(string value in anywords)
            {
                if (!excludeKeywords.Contains(value))
                    returnstr += value + " ";
            }

            return returnstr.Trim();
        }

     

        private static DataTable getMatchIndex(string strQuery, string sentance)
        {
            DataTable datatbl = new DataTable();
            if (!string.IsNullOrEmpty(strQuery))
                strQuery += " AND ( ";
            else
                strQuery += " ( ";

            if (!string.IsNullOrEmpty(sentance))
            {
                strQuery += " ( PublicationTitle like '%" + sentance + "%' " +
                                    " OR  Organization  like '%" + sentance + "%' " +
                                    " OR  PublicationKeyword like '%" + sentance + "%' " +
                                    " OR  Description like '%" + sentance + "%' " +
                                    ") OR ";

                strQuery = strQuery.Substring(0, strQuery.LastIndexOf("OR")) + ")";

                ResourceDataClassesDataContext db = new ResourceDataClassesDataContext();

                datatbl = db.spp_getResource(strQuery).ToDataTable();

            }
            return datatbl;
        }

     
        public static List<PublicationData> SearchAnyKeyword(string searchwords)
        {
            List<PublicationData> retData = new List<PublicationData>();
            string[] strs = searchwords.Split(' ');
            if (strs.Count() > 0)
            {
                string strQuery = "";
                foreach (string keyword in strs)
                {
                    if (!string.IsNullOrEmpty(keyword))
                    {
                        if (string.IsNullOrEmpty(strQuery))
                        {
                            //strQuery = " PublicationKeyword.StartsWith(\"" + keyword + ";\") || ";
                            strQuery += " PublicationKeyword.Contains(\"" + keyword + "\") || ";
                            //strQuery += " PublicationKeyword.EndsWith(\";" + keyword + "\") || ";

                            strQuery += " PublicationTitle.StartsWith(\"" + keyword + " \") || ";
                            strQuery += " PublicationTitle.Contains( \" " + keyword + " \") || ";
                            strQuery += " PublicationTitle.EndsWith(\" " + keyword + "\") || ";

                            strQuery += "  Organization.StartsWith(\"" + keyword + " \") || ";
                            strQuery += "  Organization.Contains( \" " + keyword + " \") || ";
                            strQuery += "  Organization.EndsWith(\" " + keyword + "\") || ";

                            strQuery += "  Description.StartsWith(\"" + keyword + " \") || ";
                            strQuery += "  Description.Contains( \" " + keyword + " \") || ";
                            strQuery += "  Description.EndsWith(\" " + keyword + "\")  ";

                        }
                        else
                        {
                            strQuery += " || PublicationKeyword.StartsWith(\"" + keyword + " \") ";
                            strQuery += " || PublicationKeyword.Contains(\" " + keyword + " \")  ";
                            strQuery += " || PublicationKeyword.EndsWith(\" " + keyword + "\")  ";


                            strQuery += " || PublicationTitle.StartsWith(\"" + keyword + " \") ";
                            strQuery += " || PublicationTitle.Contains( \" " + keyword + " \") ";
                            strQuery += " || PublicationTitle.EndsWith(\" " + keyword + "\")  ";


                            strQuery += " || Organization.StartsWith(\"" + keyword + " \") ";
                            strQuery += " || Organization.Contains( \" " + keyword + " \") ";
                            strQuery += " || Organization.EndsWith(\" " + keyword + "\")  ";

                            strQuery += " || Description.StartsWith(\"" + keyword + " \") ";
                            strQuery += " || Description.Contains( \" " + keyword + " \") ";
                            strQuery += " || Description.EndsWith(\" " + keyword + "\")  ";



                        }
                    }
                }


                if (string.IsNullOrEmpty(strQuery))
                    return retData;

                MagnetDBDB db = new MagnetDBDB();
                var data = db.MagnetPublications.Where(strQuery).ToList();
                data.Sort(ComparePublicationByDate);
                foreach (var publication in data)
                {
                    string str = string.Format("<b>{0}</b><br /><b>Organization</b>: {1}<br /><b>Date</b>: {2}<br /><b>Description</b>: {3}",
                        publication.PublicationTitle,
                        publication.Organization,
                        publication.PublicationDate,
                        publication.Description);

                    if (!string.IsNullOrEmpty(publication.OriginalFileName))
                    {
                        str += "<br /><a href='applicationdoc/" + publication.PhysicalFileName + "' target='_blank'><img border=0 src='images/PDFlogo.jpg'> <b>Download PDF</b></a>";
                    }

                    retData.Add(new PublicationData()
                    {
                        ID = publication.ID,
                        DisplayData = str
                    });
                }
            }
            return retData;
        }
        public static List<PublicationData> SearchPublicationSubType(int PublicationSubType)
        {
            List<PublicationData> retData = new List<PublicationData>();
            var data = MagnetPublication.Find(x => x.PublicationSubType == PublicationSubType).ToList();
            data.Sort(ComparePublicationByDate);
            foreach (var publication in data)
            {
                string str = string.Format("<b>{0}</b><br /><b>Organization</b>: {1}<br /><b>Date</b>: {2}<br /><b>Description</b>: {3}",
                    publication.PublicationTitle,
                    publication.Organization,
                    publication.PublicationDate,
                    publication.Description);

                if (!string.IsNullOrEmpty(publication.OriginalFileName))
                {
                    str += "<br /><a href='applicationdoc/" + publication.PhysicalFileName + "' target='_blank'><img border=0 src='images/PDFlogo.jpg'> <b>Download PDF</b></a>";
                }

                retData.Add(new PublicationData()
                {
                    ID = publication.ID,
                    DisplayData = str
                });
            }
            return retData;
        }
        public static List<PublicationData> SearchPublicationType(string publicationType)
        {
            List<PublicationData> retData = new List<PublicationData>();
            var data = MagnetPublication.Find(x => x.PublicationType.ToLower().Equals(publicationType)).ToList();
            data.Sort(ComparePublicationByDate);
            foreach (var publication in data)
            {
                string str = string.Format("<b>{0}</b><br /><b>Organization</b>: {1}<br /><b>Date</b>: {2}<br /><b>Description</b>: {3}",
                    publication.PublicationTitle,
                    publication.Organization,
                    publication.PublicationDate,
                    publication.Description);

                if (!string.IsNullOrEmpty(publication.OriginalFileName))
                {
                    str += "<br /><a href='applicationdoc/" + publication.PhysicalFileName + "' target='_blank'><img border=0 src='images/PDFlogo.jpg'> <b>Download PDF</b></a>";
                }

                retData.Add(new PublicationData()
                {
                    ID = publication.ID,
                    DisplayData = str
                });
            }
            return retData;
        }
        static private string tableHeader()
        {
            return ("<table class='msapTbl'><tr><th>Title</th><th>Organization</th><th>Publication Date</th><th>Authors</th></tr>");
        }

       
    }

    public class TAlog
    {
        public void TALogDatahasbeenChangedbyED(string user, string tblname, string datestamp, string action)
        {
            //send email
            string strTo = "msapcenter@leedmci.com";
            string strFrom = "msapcenter@leedmci.com";
            MailMessage objMailMsg = new MailMessage(strFrom, strTo);

            objMailMsg.CC.Add("bbrown@leedmci.com");
            

            objMailMsg.BodyEncoding = Encoding.UTF8;
            objMailMsg.Subject = user + " has " + action + " a record at a data source " + tblname + " on " + datestamp;
            objMailMsg.Body = "<p>contexts will be determined......</p>";
            objMailMsg.Priority = MailPriority.High;
            objMailMsg.IsBodyHtml = true;

            //prepare to send mail via SMTP transport
            SmtpClient objSMTPClient = new SmtpClient();
            objSMTPClient.Host = "mail2.seiservices.com";
            NetworkCredential userCredential = new NetworkCredential("SEInfo@leedmci.com", "");
            objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
            objSMTPClient.Send(objMailMsg);
        }
    }
}
