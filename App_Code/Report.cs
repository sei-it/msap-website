﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using ExcelLibrary.SpreadSheet;
using Synergy.Magnet;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Text;
using System.Drawing;

/// <summary>
/// Summary description for Report
/// </summary>
public class Report
{
    static int item18_Indian = 0, item19_Asian = 0,
            item20_Black = 0, item21_Hispanic = 0,
            item22_Hawaiian = 0, item23_White = 0,

            item26a_newIndian = 0, item26b_newAsian = 0,
            item26c_newBlack = 0, item26d_newHispanic = 0,
            item26e_newHawaiian = 0, item26f_newWhite = 0,

            item27a_ContIndian = 0, item27b_ContAsian = 0,
            item27c_ContBlack = 0, item27d_ContHispanic = 0,
            item27e_ContHawaiian = 0, item27f_ContWhite = 0,
            item28_total = 0;

    public Report()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void GetGrantees(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;
        
        worksheet.Name = "Grantees";
        worksheet.Cells[row, 1].Value = "GranteeID";
        worksheet.Cells[row, 2].Value = "GranteeName";
        worksheet.Cells[row, 3].Value = "PRAward";
        worksheet.Cells[row, 4].Value = "NCESID";
        worksheet.Cells[row, 5].Value = "ProjectTitle";
        worksheet.Cells[row, 6].Value = "Address Changed";  //Yes or No


        using (ExcelRange rng = worksheet.Cells["A1:F1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
           
        }

        //ReportType: true --- first time a report created
        foreach(GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71) ))
                continue;  //out of grantees are not avlid

                var director = MagnetGranteeDatum.SingleOrDefault(x => x.GranteeReportID == report.ID);
                MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
                row++;


                worksheet.Cells[row, 1].Value = new Cell(grantee.ID);
                worksheet.Cells[row, 2].Value = new Cell(grantee.GranteeName);
                worksheet.Cells[row, 3].Value = new Cell(grantee.PRAward);
                worksheet.Cells[row, 4].Value = new Cell(grantee.NCESID);
                worksheet.Cells[row, 5].Value = new Cell(grantee.ProjectTitle);
                if (director != null)
                {
                    worksheet.Cells[row, 6].Value = Convert.ToBoolean(director.AddressChanged) ? "[Yes]" : "[No]";
                }
                else
                    worksheet.Cells[row, 6].Value = "[No]";

        }
        
    }

    public static void GetReports(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;

        worksheet.Name = "Reports";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "PRAward";

        using (ExcelRange rng = worksheet.Cells["A1:C1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
         
        }


        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;

            worksheet.Cells[row, 1].Value = report.ID;
            worksheet.Cells[row, 2].Value = report.GranteeID;
            worksheet.Cells[row, 3].Value = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID).PRAward;

        }
        
    }

    public static void getCoverSheet1(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;  //1 - 18

        worksheet.Name = "Cover sheet1";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "PRAward";
        worksheet.Cells[row, 4].Value = "NCES";
        worksheet.Cells[row, 5].Value = "ProjectTitle";
        worksheet.Cells[row, 6].Value = "Grantee";
        worksheet.Cells[row, 7].Value = "ProjectDirector";
        worksheet.Cells[row, 8].Value = "Title";
        worksheet.Cells[row, 9].Value = "Phone";
        worksheet.Cells[row, 10].Value = "Ext";
        worksheet.Cells[row, 11].Value = "Fax";
        worksheet.Cells[row, 12].Value = "Email";
        worksheet.Cells[row, 13].Value = "Address";
        worksheet.Cells[row, 14].Value = "City";
        worksheet.Cells[row, 15].Value = "State";
        worksheet.Cells[row, 16].Value = "Zipcode";
        worksheet.Cells[row, 17].Value = "ReportPeriodFrom";
        worksheet.Cells[row, 18].Value = "ReportPeriodTo";

        using (ExcelRange rng = worksheet.Cells["A1:R1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;


            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            worksheet.Cells[row, 1].Value = report.ID;
            worksheet.Cells[row, 2].Value = grantee.ID;
            worksheet.Cells[row, 3].Value = grantee.PRAward;
            worksheet.Cells[row, 4].Value = grantee.NCESID;
            worksheet.Cells[row, 5].Value = grantee.ProjectTitle;
            worksheet.Cells[row, 6].Value = grantee.GranteeName;
            var directors = MagnetGranteeDatum.Find(x => x.GranteeReportID == report.ID);
           // var directors = MagnetGranteeContact.Find(x => x.LinkID == grantee.ID && x.ContactType == 1);
            if (directors.Count > 0)
            {
                //worksheet.Cells[row, 7].Value = directors[0].ContactFirstName + " " + directors[0].ContactLastName;
                //worksheet.Cells[row, 8].Value = directors[0].ContactTitle;
                worksheet.Cells[row, 7].Value = directors[0].ProjectDirector;
                worksheet.Cells[row, 8].Value = directors[0].Title;
                worksheet.Cells[row, 9].Value = directors[0].Phone;
                worksheet.Cells[row, 10].Value = directors[0].Ext;
                worksheet.Cells[row, 11].Value = directors[0].Fax;
                worksheet.Cells[row, 12].Value = directors[0].Email;
                worksheet.Cells[row, 13].Value = directors[0].Address;
                worksheet.Cells[row, 14].Value = directors[0].City;
                worksheet.Cells[row, 15].Value = directors[0].State;
                worksheet.Cells[row, 16].Value = directors[0].Zipcode;
            }
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
            worksheet.Cells[row, 17].Value = period.PeriodFrom;
            worksheet.Cells[row, 18].Value = period.PeriodTo;

        }
    }

    public static void getCoverSheet2(ExcelWorksheet worksheet, int ReportPeriodID)
    {

        int row = 1;  //19 - 33
        worksheet.Name = "Cover sheet2";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        //worksheet.Cells[row, 3].Value = "GranteeName";
        worksheet.Cells[row, 3].Value = "PreviousBudget";
        worksheet.Cells[row, 4].Value = "CurrentBudget";
        worksheet.Cells[row, 5].Value = "EntireBudget";
        worksheet.Cells[row, 6].Value = "PreviousNonFederal";
        worksheet.Cells[row, 7].Value = "CurrentNonFederal";
        worksheet.Cells[row, 8].Value = "EntireNonFederal";
        worksheet.Cells[row, 9].Value = "IndirectCost";
        worksheet.Cells[row, 10].Value = "IndirectCostRateAgreement";
        worksheet.Cells[row, 11].Value = "AgreementPeriodFrom";
        worksheet.Cells[row, 12].Value = "AgreementPeriodTo";
        worksheet.Cells[row, 13].Value = "ApprovalAgency";
        worksheet.Cells[row, 14].Value = "RateType";
        worksheet.Cells[row, 15].Value = "IncludedRestrictedRateProgram";
        worksheet.Cells[row, 16].Value = "CompliesWith34CFR";
        worksheet.Cells[row, 17].Value = "N/A";

        using (ExcelRange rng = worksheet.Cells["A1:Q1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;


            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            worksheet.Cells[row, 1].Value = report.ID;
            worksheet.Cells[row, 2].Value = grantee.ID;

            var CoverSheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == report.ID);
            if (CoverSheets.Count > 0)
            {
                GranteePerformanceCoverSheet sheet = CoverSheets[0];
                worksheet.Cells[row, 3].Value = Convert.ToDouble(sheet.PreviousFederalFunds);
                worksheet.Cells[row, 3].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 4].Value = Convert.ToDouble(sheet.CurrentFederalFunds);
                worksheet.Cells[row, 4].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 5].Value = sheet.EntireFederalFunds;

                worksheet.Cells[row, 6].Value = sheet.PreviousNonFederalFunds;
                worksheet.Cells[row, 7].Value = sheet.CurrentNonFederalFunds;
                worksheet.Cells[row, 8].Value = sheet.EntireNonFederalFunds;
                if (sheet.IndirectCost != null)
                {
                    worksheet.Cells[row, 9].Value = sheet.IndirectCost == true ? "Yes" : "No";
                    if (sheet.IndirectCost == true)
                    {
                        if (sheet.IndirectCostApproved != null)
                        {
                            worksheet.Cells[row, 10].Value = sheet.IndirectCostApproved == true ? "Yes" : "No";
                            if (sheet.IndirectCostApproved == true)
                            {
                                worksheet.Cells[row, 11].Value = sheet.IndirectCostStart;
                                worksheet.Cells[row, 12].Value = sheet.IndirectCostEnd;
                                if (sheet.EDApproved != null)
                                {
                                    worksheet.Cells[row, 13].Value = sheet.EDApproved == true ? "Ed" : sheet.OtherAgency;
                                }
                                if (sheet.RateType != null)
                                {
                                    worksheet.Cells[row, 14].Value = sheet.RateType == 1 ? "Provisional" : sheet.RateType == 2 ? "Final" : sheet.OtherRateType;
                                }
                            }
                        }
                    }
                }
                if (sheet.RestrictedRateProgram != null)
                    worksheet.Cells[row, 15].Value = sheet.RestrictedRateProgram == true ? "Yes" : "No";
                if (sheet.Comply34CFR != null)
                    worksheet.Cells[row, 16].Value = sheet.Comply34CFR == true ? "Yes" : "No";
                if (sheet.na != null)
                    worksheet.Cells[row, 17].Value = sheet.na == true ? "Yes" : "No";
                else
                    worksheet.Cells[row, 17].Value = "No";
            }
        }
    }

    public static void getCoverSheet3(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;  //34 - 36
        worksheet.Name = "Cover sheet3";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        //worksheet.Cells[row, 3].Value = "GranteeName";
        worksheet.Cells[row, 3].Value = "IRBAttached"; //Yes or No

        worksheet.Cells[row, 4].Value = "PerformanceMeasureData";//Yes or No

        worksheet.Cells[row, 5].Value = "PerformanceMeasureDataAvailable";

        using (ExcelRange rng = worksheet.Cells["A1:E1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            worksheet.Cells[row, 1].Value = report.ID;
            worksheet.Cells[row, 2].Value = grantee.ID;

            
            var CoverSheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == report.ID);
            if (CoverSheets.Count > 0)
            {
                GranteePerformanceCoverSheet sheet = CoverSheets[0];

                if (sheet.AnnualCertificationApproval != null)
                    worksheet.Cells[row, 3].Value = sheet.AnnualCertificationApproval == 1 ? "Yes" : sheet.AnnualCertificationApproval == 2 ? "No" : "N/A";
                if (sheet.CompletionDate != null)
                {
                    worksheet.Cells[row, 4].Value = sheet.CompletionDate == true ? "Yes" : "No";
                    if (sheet.CompletionDate == false)
                        worksheet.Cells[row, 5].Value = sheet.DataAvailableDate;
                }
            }
        }
    }

    public static void getCoverSheet4(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;
        worksheet.Name = "Cover sheet4";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        //worksheet.Cells[row, 3].Value = "GranteeName";
        worksheet.Cells[row, 3].Value = "Coversheet (ED524B)Upload"; //INDICATE "YES or NO"

        using (ExcelRange rng = worksheet.Cells["A1:E1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            worksheet.Cells[row, 1].Value = report.ID;
            worksheet.Cells[row, 2].Value = grantee.ID;


            var data = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(report.ID) && x.FormID == 0);
            if (data.Count > 0)
            {
                worksheet.Cells[row, 3].Value = "YES";
            }
            else
                worksheet.Cells[row, 3].Value = "NO";
        }
    }

    public static void getExecutiveSummary(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;
        worksheet.Name = "Executive Summary";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        //worksheet.Cells[row, 3].Value = "GranteeName";
        worksheet.Cells[row, 3].Value = "Executive Summary Upload/Text";

        using (ExcelRange rng = worksheet.Cells["A1:E1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            worksheet.Cells[row, 1].Value = report.ID;
            worksheet.Cells[row, 2].Value = grantee.ID;

            var CoverSheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == report.ID);
            if (CoverSheets.Count > 0 && !string.IsNullOrEmpty(CoverSheets[0].ExecutiveSummary))
            {
                worksheet.Cells[row, 3].Value =  CoverSheets[0].ExecutiveSummary ;
            }

            //Uploaded file
            var data = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(report.ID) && x.FormID == -1);
            if (data.Count > 0 && !string.IsNullOrEmpty(data[0].FileName))
            {
                worksheet.Cells[row, 3].Value += "\r\n Uploaded Document: " + data[0].FileName.ToString().Split('.')[0];
            }
        }
    }

    //old code--not used
    public static void GetCoverSheets(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;

        worksheet.Name = "Cover sheet";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "PRAward";
        worksheet.Cells[row, 4].Value = "NCES";
        worksheet.Cells[row, 5].Value = "ProjectTitle";
        worksheet.Cells[row, 6].Value = "Grantee";
        worksheet.Cells[row, 7].Value = "ProjectDirector";
        worksheet.Cells[row, 8].Value = "Title";
        worksheet.Cells[row, 9].Value = "Phone";
        worksheet.Cells[row, 10].Value = "Ext";
        worksheet.Cells[row, 11].Value = "Fax";
        worksheet.Cells[row, 12].Value = "Email";
        worksheet.Cells[row, 13].Value = "Address";
        worksheet.Cells[row, 14].Value = "City";
        worksheet.Cells[row, 15].Value = "State";
        worksheet.Cells[row, 16].Value = "Zipcode";
        worksheet.Cells[row, 17].Value = "ReportPeriodFrom";
        worksheet.Cells[row, 18].Value = "ReportPeriodTo";
        worksheet.Cells[row, 19].Value = "PreviousBudget";
        worksheet.Cells[row, 20].Value = "CurrentBudget";
        worksheet.Cells[row, 21].Value = "EntireBudget";
        worksheet.Cells[row, 22].Value = "PreviousNonFederal";
        worksheet.Cells[row, 23].Value = "CurrentNonFederal";
        worksheet.Cells[row, 24].Value = "EntireNonFederal";
        worksheet.Cells[row, 25].Value = "IndirectCost";
        worksheet.Cells[row, 26].Value = "IndirectCostRateAgreement";
        worksheet.Cells[row, 27].Value = "AgreementPeriodFrom";
        worksheet.Cells[row, 28].Value = "AgreementPeriodTo";
        worksheet.Cells[row, 29].Value = "ApprovalAgency";
        worksheet.Cells[row, 30].Value = "RateType";
        worksheet.Cells[row, 31].Value = "IncludedRestrictedRateProgram";
        worksheet.Cells[row, 32].Value = "CompliesWith34CFR";
        worksheet.Cells[row, 33].Value = "N/A";
        worksheet.Cells[row, 34].Value = "IRBAttached";
        worksheet.Cells[row, 35].Value = "PerformanceMeasureData";
        worksheet.Cells[row, 36].Value = "PerformanceMeasureDataAvailable";


        using (ExcelRange rng = worksheet.Cells["A1:AJ1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;


            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            worksheet.Cells[row, 1].Value = report.ID;
            worksheet.Cells[row, 2].Value = grantee.ID;
            worksheet.Cells[row, 3].Value = grantee.PRAward;
            worksheet.Cells[row, 4].Value = grantee.NCESID;
            worksheet.Cells[row, 5].Value = grantee.ProjectTitle;
            worksheet.Cells[row, 6].Value = grantee.GranteeName;
            var directors = MagnetGranteeContact.Find(x => x.LinkID == grantee.ID && x.ContactType == 1);
            if (directors.Count > 0)
            {
                worksheet.Cells[row, 7].Value = directors[0].ContactFirstName + " " + directors[0].ContactLastName;
                worksheet.Cells[row, 8].Value = directors[0].ContactTitle;
                worksheet.Cells[row, 9].Value = directors[0].Phone;
                worksheet.Cells[row, 10].Value = directors[0].Ext;
                worksheet.Cells[row, 11].Value = directors[0].Fax;
                worksheet.Cells[row, 12].Value = directors[0].Email;
                worksheet.Cells[row, 13].Value = directors[0].Address;
                worksheet.Cells[row, 14].Value = directors[0].City;
                worksheet.Cells[row, 15].Value = directors[0].State;
                worksheet.Cells[row, 16].Value = directors[0].Zipcode;
            }
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
            worksheet.Cells[row, 17].Value = period.PeriodFrom;
            worksheet.Cells[row, 18].Value = period.PeriodTo;

            var CoverSheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == report.ID);
            if (CoverSheets.Count > 0)
            {
                GranteePerformanceCoverSheet sheet = CoverSheets[0];
                worksheet.Cells[row, 19].Value = Convert.ToDouble(sheet.PreviousFederalFunds);
                worksheet.Cells[row, 19].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 20].Value = Convert.ToDouble(sheet.CurrentFederalFunds);
                worksheet.Cells[row, 20].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 21].Value = sheet.EntireFederalFunds;

                worksheet.Cells[row, 22].Value = sheet.PreviousNonFederalFunds;
                worksheet.Cells[row, 23].Value = sheet.CurrentNonFederalFunds;
                worksheet.Cells[row, 24].Value = sheet.EntireNonFederalFunds;
                if (sheet.IndirectCost != null)
                {
                    worksheet.Cells[row, 25].Value = sheet.IndirectCost == true ? "Yes" : "No";
                    if (sheet.IndirectCost == true)
                    {
                        if (sheet.IndirectCostApproved != null)
                        {
                            worksheet.Cells[row, 26].Value = sheet.IndirectCostApproved == true ? "Yes" : "No";
                            if (sheet.IndirectCostApproved == true)
                            {
                                worksheet.Cells[row, 27].Value = sheet.IndirectCostStart;
                                worksheet.Cells[row, 28].Value = sheet.IndirectCostEnd;
                                if (sheet.EDApproved != null)
                                {
                                    worksheet.Cells[row, 29].Value = sheet.EDApproved == true ? "Ed" : sheet.OtherAgency;
                                }
                                if (sheet.RateType != null)
                                {
                                    worksheet.Cells[row, 30].Value = sheet.RateType == 1 ? "Provisional" : sheet.RateType == 2 ? "Final" : sheet.OtherRateType;
                                }
                            }
                        }
                    }
                }
                if (sheet.RestrictedRateProgram != null)
                    worksheet.Cells[row, 31].Value = sheet.RestrictedRateProgram == true ? "Yes" : "No";
                if (sheet.Comply34CFR != null)
                    worksheet.Cells[row, 32].Value = sheet.Comply34CFR == true ? "Yes" : "No";
                if (sheet.na != null)
                    worksheet.Cells[row, 33].Value = sheet.na == true ? "Yes" : "No";
                else
                    worksheet.Cells[row, 33].Value = "No";
                if (sheet.AnnualCertificationApproval != null)
                    worksheet.Cells[row, 34].Value = sheet.AnnualCertificationApproval == 1 ? "Yes" : sheet.AnnualCertificationApproval == 2 ? "No" : "N/A";
                if (sheet.CompletionDate != null)
                {
                    worksheet.Cells[row, 35].Value = sheet.CompletionDate == true ? "Yes" : "No";
                    if (sheet.CompletionDate == false)
                        worksheet.Cells[row, 36].Value = sheet.DataAvailableDate;
                }
            }
        }
    }
    //old code--not used
    public static void GetStatusChart(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;

        worksheet.Name = "Status Chart";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "PRAward";
        worksheet.Cells[row, 4].Value = "Objective";
        worksheet.Cells[row, 5].Value = "StatusUpdate";
        worksheet.Cells[row, 6].Value = "Performance Measure";
        worksheet.Cells[row, 7].Value = "Measure Type";
        worksheet.Cells[row, 8].Value = "Target Raw Number";
        worksheet.Cells[row, 9].Value = "Target Ratio";
        worksheet.Cells[row, 10].Value = "Target %";
        worksheet.Cells[row, 11].Value = "Actual Raw Number";
        worksheet.Cells[row, 12].Value = "Actual Ratio";
        worksheet.Cells[row, 13].Value = "Actual %";
        worksheet.Cells[row, 14].Value = "ExplanationProgress";

        using (ExcelRange rng = worksheet.Cells["A1:N1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            //row++;
            foreach (MagnetProjectObjective objective in MagnetProjectObjective.Find(x => x.ReportID == report.ID))
            {
                if (MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == objective.ID).Count == 0)
                {
                    row++;
                    if (row % 2 == 0)
                    {
                        using (ExcelRange rng = worksheet.Cells["A" + row + ":N" + row])
                        {
                            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(159, 207, 255));
                        }
                    }

                    worksheet.Cells[row, 1].Value = report.ID;
                    worksheet.Cells[row, 2].Value = grantee.ID;
                    worksheet.Cells[row, 3].Value = grantee.PRAward;
                    worksheet.Cells[row, 4].Value = objective.ProjectObjective;
                    if (objective.StatusUpdate != null)
                        worksheet.Cells[row, 5].Value = (bool)objective.StatusUpdate ? "Yes" : "No";
                    else
                        worksheet.Cells[row, 5].Value = "";

                }
                else
                {
                    foreach (MagnetGrantPerformance performance in MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == objective.ID))
                    {
                        row++;
                        if (row % 2 == 0)
                        {
                            using (ExcelRange rng = worksheet.Cells["A" + row + ":N" + row])
                            {
                                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(159, 207, 255));
                            }
                        }

                        worksheet.Cells[row, 1].Value = report.ID.ToString();
                        worksheet.Cells[row, 2].Value = grantee.ID.ToString();
                        worksheet.Cells[row, 3].Value = grantee.PRAward;
                        worksheet.Cells[row, 4].Value = objective.ProjectObjective;
                        if (objective.StatusUpdate != null)
                            worksheet.Cells[row, 5].Value = (bool)objective.StatusUpdate ? "Yes" : "No";
                        else
                            worksheet.Cells[row, 5].Value = "";

                        worksheet.Cells[row, 6].Value = performance.PerformanceMeasure;
                        worksheet.Cells[row, 7].Value = performance.MeasureType;
                        worksheet.Cells[row, 8].Value = Convert.ToDouble(performance.TargetNumber);
                        worksheet.Cells[row, 8].Style.Numberformat.Format = "#0.00";
                        worksheet.Cells[row, 9].Value = Convert.ToString(performance.TargetRatio1) + "/" + Convert.ToString(performance.TargetRatio2);
                        worksheet.Cells[row, 10].Value = Convert.ToDouble(performance.TargetPercentage);
                        worksheet.Cells[row, 10].Style.Numberformat.Format = "#0.00";
                        worksheet.Cells[row, 11].Value = Convert.ToDouble(performance.ActualNumber);
                        worksheet.Cells[row, 11].Style.Numberformat.Format = "#0.00";
                        worksheet.Cells[row, 12].Value = Convert.ToString(performance.AcutalRatio1) + "/" + Convert.ToString(performance.AcutalRatio2);
                        worksheet.Cells[row, 13].Value = Convert.ToDouble(performance.ActualPercentage);
                        worksheet.Cells[row, 13].Style.Numberformat.Format = "#0.00";
                        worksheet.Cells[row, 14].Value = performance.ExplanationProgress;
                    }
                }
            }
        }
    }

    public static void GetStatusChart2(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;

        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "PRAward";
        worksheet.Cells[row, 4].Value = "Objective";
        worksheet.Cells[row, 5].Value = "Active";
        worksheet.Cells[row, 6].Value = "StatusUpdate";
        worksheet.Cells[row, 7].Value = "Performance Measure";
        worksheet.Cells[row, 8].Value = "Active";
        worksheet.Cells[row, 9].Value = "Measure Type";
        worksheet.Cells[row, 10].Value = "Target Raw Number";
        worksheet.Cells[row, 11].Value = "Target Ratio";
        worksheet.Cells[row, 12].Value = "Target %";
        worksheet.Cells[row, 13].Value = "Actual Raw Number";
        worksheet.Cells[row, 14].Value = "Actual Ratio";
        worksheet.Cells[row, 15].Value = "Actual %";
        worksheet.Cells[row, 16].Value = "ExplanationProgress";

        using (ExcelRange rng = worksheet.Cells["A1:P1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;

        }

        int granteeID = -1;
        bool isAlternative = false;
        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x=>x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);


            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;

            foreach (MagnetProjectObjective objective in MagnetProjectObjective.Find(x => x.ReportID == report.ID))
            {
                if (MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == objective.ID && x.IsActive == true).Count == 0)
                {
                    row++;

                    if (isAlternative)
                    {
                        using (ExcelRange rng = worksheet.Cells["A" + row + ":P" + row])
                        {
                            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                        }

                    }

                    worksheet.Cells[row, 1].Value = report.ID;
                    worksheet.Cells[row, 2].Value = grantee.ID;
                    worksheet.Cells[row, 3].Value = grantee.PRAward;
                    worksheet.Cells[row, 4].Value = objective.ProjectObjective;
                    if (objective.IsActive != null)
                        worksheet.Cells[row, 5].Value = (bool)objective.IsActive ? "Yes" : "No";
                    if (objective.StatusUpdate != null)
                        worksheet.Cells[row, 6].Value = (bool)objective.StatusUpdate ? "Yes" : "No";
                    else
                        worksheet.Cells[row, 6].Value = "";
                }
                else
                {
                    foreach (MagnetGrantPerformance performance in MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == objective.ID && x.IsActive == true))
                    {
                        row++;

                        if (isAlternative)
                        {
                            using (ExcelRange rng = worksheet.Cells["A" + row + ":P" + row])
                            {
                                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                            }

                        }


                        granteeID = grantee.ID;

                        worksheet.Cells[row, 1].Value = report.ID;
                        worksheet.Cells[row, 2].Value = grantee.ID;
                        worksheet.Cells[row, 3].Value = grantee.PRAward;
                        worksheet.Cells[row, 4].Value = objective.ProjectObjective;
                        if (objective.IsActive != null)
                            worksheet.Cells[row, 5].Value = (bool)objective.IsActive ? "Yes" : "No";
                        if (objective.StatusUpdate != null)
                            worksheet.Cells[row, 6].Value = (bool)objective.StatusUpdate ? "Yes" : "No";
                        else
                            worksheet.Cells[row, 6].Value = "";


                        worksheet.Cells[row, 7].Value = performance.PerformanceMeasure;
                        if (performance.IsActive != null)
                            worksheet.Cells[row, 8].Value = (bool)performance.IsActive ? "Yes" : "No";
                        worksheet.Cells[row, 9].Value = performance.MeasureType;
                        worksheet.Cells[row, 10].Value = Convert.ToDouble(performance.TargetNumber).ToString("0.00");

                        worksheet.Cells[row, 11].Value = Convert.ToString(performance.TargetRatio1) + "/" + Convert.ToString(performance.TargetRatio2);
                        worksheet.Cells[row, 12].Value = Convert.ToDouble(performance.TargetPercentage).ToString("0.00");
                        worksheet.Cells[row, 13].Value = Convert.ToDouble(performance.ActualNumber).ToString("0.00");
                        worksheet.Cells[row, 14].Value = Convert.ToString(performance.AcutalRatio1) + "/" + Convert.ToString(performance.AcutalRatio2);
                        worksheet.Cells[row, 15].Value = Convert.ToDouble(performance.ActualPercentage).ToString("0.00");
                        worksheet.Cells[row, 16].Value = performance.ExplanationProgress;
                    }
                }
            }
        }
    }
    //old code--not used
    public static void GetGPRA(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int reportyear = (int)MagnetReportPeriod.SingleOrDefault(x=>x.ID==ReportPeriodID).reportYear;


        if (ReportPeriodID < 6) //cohort 2010
            GetGPRA2010(worksheet, ReportPeriodID, reportyear);
        else
            GetGPRA2013(worksheet, ReportPeriodID, reportyear);
    }
    //old code--not used
    private static void GetGPRA2010(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
        int TotalSchools = 0;
        int granteeID = -1;
        bool isAlternative = false;

        Hashtable gpralist = new Hashtable();

        gpralist.Add(20, 0);
        gpralist.Add(21, 0);
        gpralist.Add(22, 0);
        gpralist.Add(23, 0);
        gpralist.Add(24, 0);
        gpralist.Add(25, 0);
        gpralist.Add(26, 0);
        gpralist.Add(27, 0);
        gpralist.Add(28, 0);
        gpralist.Add(29, 0);
        gpralist.Add(30, 0);
        gpralist.Add(31, 0);
        gpralist.Add(32, 0);
        gpralist.Add(33, 0);
        gpralist.Add(34, 0);
        gpralist.Add(35, 0);
        gpralist.Add(36, 0);
        gpralist.Add(37, 0);
        gpralist.Add(38, 0);
        gpralist.Add(39, 0);
        gpralist.Add(40, 0);
        gpralist.Add(41, 0);
        gpralist.Add(42, 0);
        gpralist.Add(43, 0);
        gpralist.Add(44, 0);
        gpralist.Add(45, 0);
        gpralist.Add(46, 0);
        gpralist.Add(47, 0);
        gpralist.Add(48, 0);
        gpralist.Add(49, 0);
        gpralist.Add(50, 0);
        gpralist.Add(51, 0);
        gpralist.Add(52, 0);
        gpralist.Add(53, 0);
        gpralist.Add(54, 0);
        gpralist.Add(55, 0);
        gpralist.Add(56, 0);
        gpralist.Add(57, 0);
        gpralist.Add(58, 0);
        gpralist.Add(59, 0);
        gpralist.Add(60, 0);
        gpralist.Add(61, 0);
        gpralist.Add(62, 0);
        gpralist.Add(63, 0);
        gpralist.Add(64, 0);
        gpralist.Add(65, 0);
        gpralist.Add(66, 0);
        gpralist.Add(67, 0);
        gpralist.Add(68, 0);
        gpralist.Add(69, 0);
        gpralist.Add(70, 0);
        gpralist.Add(71, 0);
        gpralist.Add(72, 0);
        gpralist.Add(73, 0);
        gpralist.Add(74, 0);
        gpralist.Add(75, 0);
        gpralist.Add(76, 0);
        gpralist.Add(77, 0);
        gpralist.Add(78, 0);
        gpralist.Add(79, 0);
        gpralist.Add(80, 0);
        gpralist.Add(81, 0);
        gpralist.Add(82, 0);
        gpralist.Add(83, 0);
        gpralist.Add(84, 0);
        gpralist.Add(85, 0);
        gpralist.Add(86, 0);
        gpralist.Add(87, 0);
        gpralist.Add(88, 0);
        gpralist.Add(89, 0);
        gpralist.Add(90, 0);
        gpralist.Add(91, 0);
        gpralist.Add(92, 0);
        gpralist.Add(93, 0);
        gpralist.Add(94, 0);
        gpralist.Add(95, 0);
        gpralist.Add(96, 0);
        gpralist.Add(97, 0);
        gpralist.Add(98, 0);
        gpralist.Add(99, 0);
        gpralist.Add(100, 0);
        gpralist.Add(101, 0);

        Hashtable ImpvStatus = new Hashtable();
        ImpvStatus.Add(8, "Approaching target");
        ImpvStatus.Add(4, "Celebration");
        ImpvStatus.Add(23, "Continuous improvement");
        ImpvStatus.Add(2, "Corrective action");
        ImpvStatus.Add(5, "Excelling school");
        ImpvStatus.Add(6, "Focus");
        ImpvStatus.Add(7, "Focus targeted");
        ImpvStatus.Add(1, "Improvement");
        ImpvStatus.Add(9, "Improvement required");
        ImpvStatus.Add(10, "In improvement");
        ImpvStatus.Add(11, "Level 1");
        ImpvStatus.Add(12, "Level 2");
        ImpvStatus.Add(13, "Level 3 (focus)");
        ImpvStatus.Add(14, "Level 4 (priority)");
        ImpvStatus.Add(15, "Level 5 (priority)");
        ImpvStatus.Add(16, "Met alternative standard");
        ImpvStatus.Add(17, "Met standard");
        ImpvStatus.Add(18, "On target");
        ImpvStatus.Add(19, "Performance");
        ImpvStatus.Add(20, "Priority");
        ImpvStatus.Add(21, "Priority improvement");
        ImpvStatus.Add(22, "Progressing");
        ImpvStatus.Add(3, "Restructuring");
        ImpvStatus.Add(24, "Review (including focus)");
        ImpvStatus.Add(25, "Reward");
        ImpvStatus.Add(26, "Transitioning");
        ImpvStatus.Add(27, "Turnaround");
        ImpvStatus.Add(28, "Other");

        worksheet.Name = "GPRA";

        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "SchoolID";
        worksheet.Cells[row, 4].Value = "PRAward";
        worksheet.Cells[row, 5].Value = "School_Name";
        worksheet.Cells[row, 6].Value = "Grantee_Name";
        worksheet.Cells[row, 7].Value = "Grades";
        worksheet.Cells[row, 8].Value = "Program_Type";
        worksheet.Cells[row, 9].Value = "Coded_Program_Type";
        worksheet.Cells[row, 10].Value = "Title_I";
        worksheet.Cells[row, 11].Value = "Coded_Title_I";
        worksheet.Cells[row, 12].Value = "School_Improvement";
        worksheet.Cells[row, 13].Value = "Coded_School_Improvement";
        worksheet.Cells[row, 14].Value = "School_Improvement_Status";
        worksheet.Cells[row, 15].Value = "Coded_SIS";
        worksheet.Cells[row, 16].Value = "Lowest_Achieving";
        worksheet.Cells[row, 17].Value = "Coded_Lowest_Achieving";
        worksheet.Cells[row, 18].Value = "SIG";
        worksheet.Cells[row, 19].Value = "Coded_SIG";
        worksheet.Cells[row, 20].Value = "App_Amer_Indian";
        worksheet.Cells[row, 21].Value = "App_Asian";
        worksheet.Cells[row, 22].Value = "App_Black";
        worksheet.Cells[row, 23].Value = "App_Hispanic";
        worksheet.Cells[row, 24].Value = "App_Native_Hawaiian";
        worksheet.Cells[row, 25].Value = "App_White";
        worksheet.Cells[row, 26].Value = "App_Two_more";
        worksheet.Cells[row, 27].Value = "Undeclared";
        worksheet.Cells[row, 28].Value = "App_Total";
        worksheet.Cells[row, 29].Value = "Enr_Amer_Indian";
        worksheet.Cells[row, 30].Value = "Enr_Asian";
        worksheet.Cells[row, 31].Value = "Enr_Black";
        worksheet.Cells[row, 32].Value = "Enr_Hispanic";
        worksheet.Cells[row, 33].Value = "Enr_Native_Hawaiian";
        worksheet.Cells[row, 34].Value = "Enr_White";
        worksheet.Cells[row, 35].Value = "Enr_Two_more";
        worksheet.Cells[row, 36].Value = "Enr_Total";
        worksheet.Cells[row, 37].Value = "New_Students";
        worksheet.Cells[row, 38].Value = "Continuing_Students";
        worksheet.Cells[row, 39].Value = "Total_Sch_Enroll";

        worksheet.Cells[row, 40].Value = "Part_AYP_R_All";
        worksheet.Cells[row, 41].Value = "Met_AYP_R_All";
        worksheet.Cells[row, 42].Value = "%_AYP_R_All";

        worksheet.Cells[row, 43].Value = "Part_AYP_R_Amer_Ind";
        worksheet.Cells[row, 44].Value = "Met_AYP_R_Amer_Ind";
        worksheet.Cells[row, 45].Value = "%_AYP_R_Amer_Ind";

        worksheet.Cells[row, 46].Value = "Part_AYP_R_Asian";
        worksheet.Cells[row, 47].Value = "Met_AYP_R_Asian";
        worksheet.Cells[row, 48].Value = "%_AYP_R_Asian";

        worksheet.Cells[row, 49].Value = "Part_AYP_R_Black";
        worksheet.Cells[row, 50].Value = "Met_AYP_R_Black";
        worksheet.Cells[row, 51].Value = "%_AYP_R_Black";

        worksheet.Cells[row, 52].Value = "Part_AYP_R_Hispanic";
        worksheet.Cells[row, 53].Value = "Met_AYP_R_Hispanic";
        worksheet.Cells[row, 54].Value = "%_AYP_R_Hispanic";

        worksheet.Cells[row, 55].Value = "Part_AYP_R_Native_Hawaiian";
        worksheet.Cells[row, 56].Value = "Met_AYP_R_Native_Hawaiian";
        worksheet.Cells[row, 57].Value = "%_AYP_R_Native_Hawaiian";

        worksheet.Cells[row, 58].Value = "Part_AYP_R_White";
        worksheet.Cells[row, 59].Value = "Met_AYP_R_White";
        worksheet.Cells[row, 60].Value = "%_AYP_R_White";

        worksheet.Cells[row, 61].Value = "Part_AYP_R_TwoOrMore";
        worksheet.Cells[row, 62].Value = "Met_AYP_R_TwoOrMore";
        worksheet.Cells[row, 63].Value = "%_AYP_R_TwoOrMore";

        worksheet.Cells[row, 64].Value = "Part_AYP_R_EconDis";
        worksheet.Cells[row, 65].Value = "Met_AYP_R_EconDis";
        worksheet.Cells[row, 66].Value = "%_AYP_R_EconDis";

        worksheet.Cells[row, 67].Value = "Part_AYP_R_EL";
        worksheet.Cells[row, 68].Value = "Met_AYP_R_EL";
        worksheet.Cells[row, 69].Value = "%_AYP_R_EL";

        worksheet.Cells[row, 70].Value = "Part_AYP_M_All";
        worksheet.Cells[row, 71].Value = "Met_AYP_M_All";
        worksheet.Cells[row, 72].Value = "%_AYP_M_All";

        worksheet.Cells[row, 73].Value = "Part_AYP_M_Amer_Ind";
        worksheet.Cells[row, 74].Value = "Met_AYP_M_Amer_Ind";
        worksheet.Cells[row, 75].Value = "%_AYP_M_Amer_Ind";

        worksheet.Cells[row, 76].Value = "Part_AYP_M_Asian";
        worksheet.Cells[row, 77].Value = "Met_AYP_M_Asian";
        worksheet.Cells[row, 78].Value = "%_AYP_M_Asian";

        worksheet.Cells[row, 79].Value = "Part_AYP_M_Black";
        worksheet.Cells[row, 80].Value = "Met_AYP_M_Black";
        worksheet.Cells[row, 81].Value = "%_AYP_M_Black";

        worksheet.Cells[row, 82].Value = "Part_AYP_M_Hispanic";
        worksheet.Cells[row, 83].Value = "Met_AYP_M_Hispanic";
        worksheet.Cells[row, 84].Value = "%_AYP_M_Hispanic";

        worksheet.Cells[row, 85].Value = "Part_AYP_M_Native_Hawaii";
        worksheet.Cells[row, 86].Value = "Met_AYP_M_Native_Hawaii";
        worksheet.Cells[row, 87].Value = "%_AYP_M_Native_Hawaii";

        worksheet.Cells[row, 88].Value = "Part_AYP_M_White";
        worksheet.Cells[row, 89].Value = "Met_AYP_M_White";
        worksheet.Cells[row, 90].Value = "%_AYP_M_White";

        worksheet.Cells[row, 91].Value = "Part_AYP_M_TwoOrMore";
        worksheet.Cells[row, 92].Value = "Met_AYP_M_TwoOrMore";
        worksheet.Cells[row, 93].Value = "%_AYP_M_TwoOrMore";

        worksheet.Cells[row, 94].Value = "Part_AYP_M_EconDis";
        worksheet.Cells[row, 95].Value = "Met_AYP_M_EconDis";
        worksheet.Cells[row, 96].Value = "%_AYP_M_EconDis";

        worksheet.Cells[row, 97].Value = "Part_AYP_M_EL";
        worksheet.Cells[row, 98].Value = "Met_AYP_M_EL";
        worksheet.Cells[row, 99].Value = "%_AYP_M_EL";

        worksheet.Cells[row, 100].Value = "MSAP_Funds";
        worksheet.Cells[row, 101].Value = "Studs_Served";

        worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:CW1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x => x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                    row++;

                    if (isAlternative)
                    {
                        using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                        {
                            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                        }

                    }


                    MagnetGPRA item = gpraData[0];
                    worksheet.Cells[row, 1].Value = report.ID;

                    worksheet.Cells[row, 2].Value = report.GranteeID;
                    worksheet.Cells[row, 3].Value = school.SchoolCode;
                    worksheet.Cells[row, 4].Value = grantee.PRAward;
                    worksheet.Cells[row, 5].Value = school.SchoolName;

                    worksheet.Cells[row, 6].Value = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID).GranteeName;
                    string GradeString = "";
                    if (!string.IsNullOrEmpty(item.SchoolGrade))
                    {
                        foreach (string str in item.SchoolGrade.Split(';'))
                        {
                            switch (str)
                            {
                                case "1":
                                    GradeString += "Pre-K;";
                                    break;
                                case "2":
                                    GradeString += "K;"; break;
                                case "3":
                                    GradeString += "First;"; break;
                                case "4":
                                    GradeString += "Second;"; break;
                                case "5":
                                    GradeString += "Third;"; break;
                                case "6":
                                    GradeString += "Fourth;"; break;
                                case "7":
                                    GradeString += "Fifth;"; break;
                                case "8":
                                    GradeString += "Sixth;"; break;
                                case "9":
                                    GradeString += "Seventh;"; break;
                                case "10":
                                    GradeString += "Eighth;"; break;
                                case "11":
                                    GradeString += "Ninth;"; break;
                                case "12":
                                    GradeString += "Tenth;"; break;
                                case "13":
                                    GradeString += "Eleventh;"; break;
                                case "14":
                                    GradeString += "Twelfth;"; break;
                            }
                        }
                    }
                    worksheet.Cells[row, 7].Value = GradeString;
                    if (item.ProgramType != null)
                    {
                        worksheet.Cells[row, 8].Value = (bool)item.ProgramType ? "Partial" : "Whole-school";
                        worksheet.Cells[row, 9].Value = (bool)item.ProgramType ? 2 : 1;
                    }
                    else
                    {
                        worksheet.Cells[row, 8].Value = "Missing";
                        worksheet.Cells[row, 9].Value = 99;
                    }


                    //Title I
                    if (item.TitleISchoolFunding != null)
                    {
                        worksheet.Cells[row, 10].Value = (bool)item.TitleISchoolFunding ? "Yes" : "No";
                        worksheet.Cells[row, 11].Value = (bool)item.TitleISchoolFunding ? 1 : 0;

                        if (item.TitleISchoolFunding == true) //Title I = Yes
                        {
                            if (item.TitleISchoolFundingImprovement != null)
                            {
                                if ((item.TitleISchoolFundingImprovement == true))
                                {
                                    worksheet.Cells[row, 12].Value = "Yes";
                                    worksheet.Cells[row, 13].Value = 1;
                                }
                                else //school imp = no
                                {
                                    worksheet.Cells[row, 12].Value = "No";
                                    worksheet.Cells[row, 13].Value = 0;

                                    worksheet.Cells[row, 14].Value = "Not Applicable";
                                    worksheet.Cells[row, 15].Value = 98;
                                }
                            }
                            else
                            {
                                worksheet.Cells[row, 12].Value = "Missing";
                                worksheet.Cells[row, 13].Value = 99;

                                worksheet.Cells[row, 14].Value = "Missing";
                                worksheet.Cells[row, 15].Value = 99;
                            }

                        }
                        else //Title I = No
                        {
                            worksheet.Cells[row, 12].Value = "Not Applicable";
                            worksheet.Cells[row, 13].Value = 98;

                            worksheet.Cells[row, 14].Value = "Not Applicable";
                            worksheet.Cells[row, 15].Value = 98;
                        }

                    }
                    else
                    {
                        worksheet.Cells[row, 10].Value = "Missing";
                        worksheet.Cells[row, 11].Value = 99;

                        worksheet.Cells[row, 12].Value = "Missing";
                        worksheet.Cells[row, 13].Value = 99;

                        worksheet.Cells[row, 14].Value = "Missing";
                        worksheet.Cells[row, 15].Value = 99;
                    }

                    if (item.TitleISchoolFundingImprovementStatus != null)
                    {
                        int key = (int)item.TitleISchoolFundingImprovementStatus;

                        worksheet.Cells[row, 14].Value = ImpvStatus[key];
                        worksheet.Cells[row, 15].Value = item.TitleISchoolFundingImprovementStatus;
                    }

                    //lowest achieving
                    if (item.PersistentlyLlowestAchievingSchool != null)
                    {
                        worksheet.Cells[row, 16].Value = (bool)item.PersistentlyLlowestAchievingSchool ? "Yes" : "No";
                        worksheet.Cells[row, 17].Value = (bool)item.PersistentlyLlowestAchievingSchool ? 1 : 0;
                        if (item.PersistentlyLlowestAchievingSchool == true)
                        {
                            if (item.SchoolImprovementGrant != null)
                            {
                                worksheet.Cells[row, 18].Value = (bool)item.SchoolImprovementGrant ? "Yes" : "No";
                                worksheet.Cells[row, 19].Value = (bool)item.SchoolImprovementGrant ? 1 : 0;
                            }
                            else
                            {
                                worksheet.Cells[row, 18].Value = "Missing";
                                worksheet.Cells[row, 19].Value = 99;
                            }
                        }
                        else
                        {
                            worksheet.Cells[row, 18].Value = "Not Applicable";
                            worksheet.Cells[row, 19].Value = 98;

                        }
                    }
                    else
                    {
                        worksheet.Cells[row, 16].Value = "Missing";
                        worksheet.Cells[row, 17].Value = 99;

                        worksheet.Cells[row, 18].Value = "Missing";
                        worksheet.Cells[row, 19].Value = 99;
                    }

                    ///end coding////

                    var applicantPoolData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 1);
                    if (applicantPoolData.Count > 0) //App_
                    {
                        worksheet.Cells[row, 20].Value = applicantPoolData[0].Indian;
                        gpralist[20] = Convert.ToDouble(gpralist[20]) + Convert.ToDouble(worksheet.Cells[row, 20].Value);
                        worksheet.Cells[row, 21].Value = applicantPoolData[0].Asian;
                        gpralist[21] = Convert.ToDouble(gpralist[21]) + Convert.ToDouble(worksheet.Cells[row, 21].Value);
                        worksheet.Cells[row, 22].Value = applicantPoolData[0].Black;
                        gpralist[22] = Convert.ToDouble(gpralist[22]) + Convert.ToDouble(worksheet.Cells[row, 22].Value);
                        worksheet.Cells[row, 23].Value = applicantPoolData[0].Hispanic;
                        gpralist[23] = Convert.ToDouble(gpralist[23]) + Convert.ToDouble(worksheet.Cells[row, 23].Value);
                        worksheet.Cells[row, 24].Value = applicantPoolData[0].Hawaiian;
                        gpralist[24] = Convert.ToDouble(gpralist[24]) + Convert.ToDouble(worksheet.Cells[row, 24].Value);
                        worksheet.Cells[row, 25].Value = applicantPoolData[0].White;
                        gpralist[25] = Convert.ToDouble(gpralist[25]) + Convert.ToDouble(worksheet.Cells[row, 25].Value);
                        worksheet.Cells[row, 26].Value = applicantPoolData[0].MultiRaces;
                        gpralist[26] = Convert.ToDouble(gpralist[26]) + Convert.ToDouble(worksheet.Cells[row, 26].Value);
                        worksheet.Cells[row, 27].Value = applicantPoolData[0].Undeclared;
                        gpralist[27] = Convert.ToDouble(gpralist[27]) + Convert.ToDouble(worksheet.Cells[row, 27].Value);
                        worksheet.Cells[row, 28].Value = applicantPoolData[0].Total;
                        gpralist[28] = Convert.ToDouble(gpralist[28]) + Convert.ToDouble(worksheet.Cells[row, 28].Value);
                    }
                    var enrollmentData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 2);
                    if (enrollmentData.Count > 0) //Enr_
                    {
                        worksheet.Cells[row, 29].Value = enrollmentData[0].Indian;
                        gpralist[29] = Convert.ToDouble(gpralist[29]) + Convert.ToDouble(worksheet.Cells[row, 29].Value);
                        worksheet.Cells[row, 30].Value = enrollmentData[0].Asian;
                        gpralist[30] = Convert.ToDouble(gpralist[30]) + Convert.ToDouble(worksheet.Cells[row, 30].Value);
                        worksheet.Cells[row, 31].Value = enrollmentData[0].Black;
                        gpralist[31] = Convert.ToDouble(gpralist[31]) + Convert.ToDouble(worksheet.Cells[row, 31].Value);
                        worksheet.Cells[row, 32].Value = enrollmentData[0].Hispanic;
                        gpralist[32] = Convert.ToDouble(gpralist[32]) + Convert.ToDouble(worksheet.Cells[row, 32].Value);
                        worksheet.Cells[row, 33].Value = enrollmentData[0].Hawaiian;
                        gpralist[33] = Convert.ToDouble(gpralist[33]) + Convert.ToDouble(worksheet.Cells[row, 33].Value);
                        worksheet.Cells[row, 34].Value = enrollmentData[0].White;
                        gpralist[34] = Convert.ToDouble(gpralist[34]) + Convert.ToDouble(worksheet.Cells[row, 34].Value);
                        worksheet.Cells[row, 35].Value = enrollmentData[0].MultiRaces;
                        gpralist[35] = Convert.ToDouble(gpralist[35]) + Convert.ToDouble(worksheet.Cells[row, 35].Value);
                        worksheet.Cells[row, 36].Value = enrollmentData[0].Total;
                        gpralist[36] = Convert.ToDouble(gpralist[36]) + Convert.ToDouble(worksheet.Cells[row, 36].Value);
                        worksheet.Cells[row, 37].Value = enrollmentData[0].ExtraFiled1; //new_student
                        gpralist[37] = Convert.ToDouble(gpralist[37]) + Convert.ToDouble(worksheet.Cells[row, 37].Value);
                        worksheet.Cells[row, 38].Value = enrollmentData[0].ExtraFiled2; //Continuing Students
                        gpralist[38] = Convert.ToDouble(gpralist[38]) + Convert.ToDouble(worksheet.Cells[row, 38].Value);
                        worksheet.Cells[row, 39].Value = enrollmentData[0].ExtraFiled3; //Total_sch_Enroll
                        gpralist[39] = Convert.ToDouble(gpralist[39]) + Convert.ToDouble(worksheet.Cells[row, 39].Value);
                    }
                    var participationReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 3);
                    if (participationReading.Count > 0) //Part_AYP_R_
                    {
                        if (participationReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 40].Value = participationReading[0].Total;
                            gpralist[40] = Convert.ToDouble(gpralist[40]) + Convert.ToDouble(worksheet.Cells[row, 40].Value);
                        }
                        if (participationReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 43].Value = participationReading[0].Indian;
                            gpralist[43] = Convert.ToDouble(gpralist[43]) + Convert.ToDouble(worksheet.Cells[row, 43].Value);
                        }
                        if (participationReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 46].Value = participationReading[0].Asian;
                            gpralist[46] = Convert.ToDouble(gpralist[46]) + Convert.ToDouble(worksheet.Cells[row, 46].Value);
                        }
                        if (participationReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 49].Value = participationReading[0].Black;
                            gpralist[49] = Convert.ToDouble(gpralist[49]) + Convert.ToDouble(worksheet.Cells[row, 49].Value);
                        }

                        if (participationReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 52].Value = participationReading[0].Hispanic;
                            gpralist[52] = Convert.ToDouble(gpralist[52]) + Convert.ToDouble(worksheet.Cells[row, 52].Value);
                        }
                        if (participationReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 55].Value = participationReading[0].Hawaiian;
                            gpralist[55] = Convert.ToDouble(gpralist[55]) + Convert.ToDouble(worksheet.Cells[row, 55].Value);
                        }
                        if (participationReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 58].Value = participationReading[0].White;
                            gpralist[58] = Convert.ToDouble(gpralist[58]) + Convert.ToDouble(worksheet.Cells[row, 58].Value);
                        }
                        if (participationReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 61].Value = participationReading[0].MultiRaces;
                            gpralist[61] = Convert.ToDouble(gpralist[61]) + Convert.ToDouble(worksheet.Cells[row, 61].Value);
                        }
                        if (participationReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 64].Value = participationReading[0].ExtraFiled2;//_econnDis
                            gpralist[64] = Convert.ToDouble(gpralist[64]) + Convert.ToDouble(worksheet.Cells[row, 64].Value);
                        }

                        if (participationReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 67].Value = participationReading[0].ExtraFiled1;   //Par_AYP_R_EL
                            gpralist[67] = Convert.ToDouble(gpralist[67]) + Convert.ToDouble(worksheet.Cells[row, 67].Value);
                        }
                    }
                    var achievementReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 5);
                    if (achievementReading.Count > 0) //met_AYP_R
                    {
                        if (achievementReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 41].Value = achievementReading[0].Total;
                            gpralist[41] = Convert.ToDouble(gpralist[41]) + Convert.ToDouble(worksheet.Cells[row, 41].Value);
                        }
                        if (achievementReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 44].Value = achievementReading[0].Indian;
                            gpralist[44] = Convert.ToDouble(gpralist[44]) + Convert.ToDouble(worksheet.Cells[row, 44].Value);
                        }
                        if (achievementReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 47].Value = achievementReading[0].Asian;
                            gpralist[47] = Convert.ToDouble(gpralist[47]) + Convert.ToDouble(worksheet.Cells[row, 47].Value);
                        }
                        if (achievementReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 50].Value = achievementReading[0].Black;
                            gpralist[50] = Convert.ToDouble(gpralist[50]) + Convert.ToDouble(worksheet.Cells[row, 50].Value);
                        }
                        if (achievementReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 53].Value = achievementReading[0].Hispanic;
                            gpralist[53] = Convert.ToDouble(gpralist[53]) + Convert.ToDouble(worksheet.Cells[row, 53].Value);
                        }
                        if (achievementReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 56].Value = achievementReading[0].Hawaiian;
                            gpralist[56] = Convert.ToDouble(gpralist[56]) + Convert.ToDouble(worksheet.Cells[row, 56].Value);
                        }
                        if (achievementReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 59].Value = achievementReading[0].White;
                            gpralist[59] = Convert.ToDouble(gpralist[59]) + Convert.ToDouble(worksheet.Cells[row, 59].Value);
                        }
                        if (achievementReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 62].Value = achievementReading[0].MultiRaces;
                            gpralist[62] = Convert.ToDouble(gpralist[62]) + Convert.ToDouble(worksheet.Cells[row, 62].Value);
                        }
                        if (achievementReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 65].Value = achievementReading[0].ExtraFiled2; //met_AYP_R_EconDis
                            gpralist[65] = Convert.ToDouble(gpralist[65]) + Convert.ToDouble(worksheet.Cells[row, 65].Value);
                        }
                        if (achievementReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 68].Value = achievementReading[0].ExtraFiled1; //met_AYP_R_EL
                            gpralist[68] = Convert.ToDouble(gpralist[68]) + Convert.ToDouble(worksheet.Cells[row, 68].Value);
                        }
                    }
                    if (participationReading.Count > 0 && achievementReading.Count > 0)
                    {
                        if (participationReading[0].Total > 0 && achievementReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 42].Value = Convert.ToDouble(achievementReading[0].Total) / participationReading[0].Total;
                            worksheet.Cells[row, 42].Style.Numberformat.Format = "#0.00%";
                            gpralist[42] = Convert.ToDouble(gpralist[42]) + Convert.ToDouble(worksheet.Cells[row, 42].Value);
                        }

                        if (participationReading[0].Indian > 0 && achievementReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 45].Value = Convert.ToDouble(achievementReading[0].Indian) / participationReading[0].Indian;
                            worksheet.Cells[row, 45].Style.Numberformat.Format = "#0.00%";
                            gpralist[45] = Convert.ToDouble(gpralist[45]) + Convert.ToDouble(worksheet.Cells[row, 45].Value);
                        }
                        if (participationReading[0].Asian > 0 && achievementReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 48].Value = Convert.ToDouble(achievementReading[0].Asian) / participationReading[0].Asian;
                            worksheet.Cells[row, 48].Style.Numberformat.Format = "#0.00%";
                            gpralist[48] = Convert.ToDouble(gpralist[48]) + Convert.ToDouble(worksheet.Cells[row, 48].Value);
                        }
                        if (participationReading[0].Black > 0 && achievementReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 51].Value = Convert.ToDouble(achievementReading[0].Black) / participationReading[0].Black;
                            worksheet.Cells[row, 51].Style.Numberformat.Format = "#0.00%";
                            gpralist[51] = Convert.ToDouble(gpralist[51]) + Convert.ToDouble(worksheet.Cells[row, 51].Value);
                        }
                        if (participationReading[0].Hispanic > 0 && achievementReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 54].Value = Convert.ToDouble(achievementReading[0].Hispanic) / participationReading[0].Hispanic;
                            worksheet.Cells[row, 54].Style.Numberformat.Format = "#0.00%";
                            gpralist[54] = Convert.ToDouble(gpralist[54]) + Convert.ToDouble(worksheet.Cells[row, 54].Value);

                        }
                        if (participationReading[0].Hawaiian > 0 && achievementReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 57].Value = Convert.ToDouble(achievementReading[0].Hawaiian) / participationReading[0].Hawaiian;
                            worksheet.Cells[row, 57].Style.Numberformat.Format = "#0.00%";
                            gpralist[57] = Convert.ToDouble(gpralist[57]) + Convert.ToDouble(worksheet.Cells[row, 57].Value);
                        }
                        if (participationReading[0].White > 0 && achievementReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 60].Value = Convert.ToDouble(achievementReading[0].White) / participationReading[0].White;
                            worksheet.Cells[row, 60].Style.Numberformat.Format = "#0.00%";
                            gpralist[60] = Convert.ToDouble(gpralist[60]) + Convert.ToDouble(worksheet.Cells[row, 60].Value);
                        }
                        if (participationReading[0].MultiRaces > 0 && achievementReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 63].Value = Convert.ToDouble(achievementReading[0].MultiRaces) / participationReading[0].MultiRaces;
                            worksheet.Cells[row, 63].Style.Numberformat.Format = "#0.00%";
                            gpralist[63] = Convert.ToDouble(gpralist[63]) + Convert.ToDouble(worksheet.Cells[row, 63].Value);
                        }
                        if (participationReading[0].ExtraFiled2 > 0 && achievementReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 66].Value = Convert.ToDouble(achievementReading[0].ExtraFiled2) / participationReading[0].ExtraFiled2;
                            worksheet.Cells[row, 66].Style.Numberformat.Format = "#0.00%";
                            gpralist[66] = Convert.ToDouble(gpralist[66]) + Convert.ToDouble(worksheet.Cells[row, 66].Value);
                        }
                        if (participationReading[0].ExtraFiled1 > 0 && achievementReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 69].Value = Convert.ToDouble(achievementReading[0].ExtraFiled1) / participationReading[0].ExtraFiled1;
                            worksheet.Cells[row, 69].Style.Numberformat.Format = "#0.00%";
                            gpralist[69] = Convert.ToDouble(gpralist[69]) + Convert.ToDouble(worksheet.Cells[row, 69].Value);
                        }
                    }

                    ////////////////////////////////////////////////////////////////////
                    var participationMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 4);
                    if (participationMath.Count > 0) //Part_AYP_M
                    {
                        if (participationMath[0].Total >= 0)
                        {
                            worksheet.Cells[row, 70].Value = participationMath[0].Total;
                            gpralist[70] = Convert.ToDouble(gpralist[70]) + Convert.ToDouble(worksheet.Cells[row, 70].Value);
                        }
                        if (participationMath[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 73].Value = participationMath[0].Indian;
                            gpralist[73] = Convert.ToDouble(gpralist[73]) + Convert.ToDouble(worksheet.Cells[row, 73].Value);
                        }
                        if (participationMath[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 76].Value = participationMath[0].Asian;
                            gpralist[76] = Convert.ToDouble(gpralist[76]) + Convert.ToDouble(worksheet.Cells[row, 76].Value);
                        }
                        if (participationMath[0].Black >= 0)
                        {
                            worksheet.Cells[row, 79].Value = participationMath[0].Black;
                            gpralist[79] = Convert.ToDouble(gpralist[79]) + Convert.ToDouble(worksheet.Cells[row, 79].Value);
                        }
                        if (participationMath[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 82].Value = participationMath[0].Hispanic;
                            gpralist[82] = Convert.ToDouble(gpralist[82]) + Convert.ToDouble(worksheet.Cells[row, 82].Value);
                        }
                        if (participationMath[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 85].Value = participationMath[0].Hawaiian;
                            gpralist[85] = Convert.ToDouble(gpralist[85]) + Convert.ToDouble(worksheet.Cells[row, 85].Value);
                        }
                        if (participationMath[0].White >= 0)
                        {
                            worksheet.Cells[row, 88].Value = participationMath[0].White;
                            gpralist[88] = Convert.ToDouble(gpralist[88]) + Convert.ToDouble(worksheet.Cells[row, 88].Value);
                        }
                        if (participationMath[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 91].Value = participationMath[0].MultiRaces;
                            gpralist[91] = Convert.ToDouble(gpralist[91]) + Convert.ToDouble(worksheet.Cells[row, 91].Value);
                        }
                        if (participationMath[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 94].Value = participationMath[0].ExtraFiled2;
                            gpralist[94] = Convert.ToDouble(gpralist[94]) + Convert.ToDouble(worksheet.Cells[row, 94].Value);
                        }
                        if (participationMath[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 97].Value = participationMath[0].ExtraFiled1;
                            gpralist[97] = Convert.ToDouble(gpralist[97]) + Convert.ToDouble(worksheet.Cells[row, 97].Value);
                        }
                    }

                    var achievementMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 6);
                    if (achievementMath.Count > 0) //Met_AYP_M_
                    {
                        if (achievementMath[0].Total >= 0)
                        {
                            worksheet.Cells[row, 71].Value = achievementMath[0].Total;
                            gpralist[71] = Convert.ToDouble(gpralist[71]) + Convert.ToDouble(worksheet.Cells[row, 71].Value);
                        }
                        if (achievementMath[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 74].Value = achievementMath[0].Indian;
                            gpralist[74] = Convert.ToDouble(gpralist[74]) + Convert.ToDouble(worksheet.Cells[row, 74].Value);
                        }
                        if (achievementMath[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 77].Value = achievementMath[0].Asian;
                            gpralist[77] = Convert.ToDouble(gpralist[77]) + Convert.ToDouble(worksheet.Cells[row, 77].Value);
                        }
                        if (achievementMath[0].Black >= 0)
                        {
                            worksheet.Cells[row, 80].Value = achievementMath[0].Black;
                            gpralist[80] = Convert.ToDouble(gpralist[80]) + Convert.ToDouble(worksheet.Cells[row, 80].Value);
                        }
                        if (achievementMath[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 83].Value = achievementMath[0].Hispanic;
                            gpralist[83] = Convert.ToDouble(gpralist[83]) + Convert.ToDouble(worksheet.Cells[row, 83].Value);
                        }
                        if (achievementMath[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 86].Value = achievementMath[0].Hawaiian;
                            gpralist[86] = Convert.ToDouble(gpralist[86]) + Convert.ToDouble(worksheet.Cells[row, 86].Value);
                        }
                        if (achievementMath[0].White >= 0)
                        {
                            worksheet.Cells[row, 89].Value = achievementMath[0].White;
                            gpralist[89] = Convert.ToDouble(gpralist[89]) + Convert.ToDouble(worksheet.Cells[row, 89].Value);
                        }
                        if (achievementMath[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 92].Value = achievementMath[0].MultiRaces;
                            gpralist[92] = Convert.ToDouble(gpralist[92]) + Convert.ToDouble(worksheet.Cells[row, 92].Value);
                        }
                        if (achievementMath[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 95].Value = achievementMath[0].ExtraFiled2;
                            gpralist[95] = Convert.ToDouble(gpralist[95]) + Convert.ToDouble(worksheet.Cells[row, 95].Value);
                        }
                        if (achievementMath[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 98].Value = achievementMath[0].ExtraFiled1;
                            gpralist[98] = Convert.ToDouble(gpralist[98]) + Convert.ToDouble(worksheet.Cells[row, 98].Value);
                        }
                    }
                    if (achievementMath.Count > 0 && participationMath.Count > 0)
                    {
                        if (achievementMath[0].Total >= 0 && participationMath[0].Total > 0)
                        {
                            worksheet.Cells[row, 72].Value = Convert.ToDouble(achievementMath[0].Total) / participationMath[0].Total;
                            worksheet.Cells[row, 72].Style.Numberformat.Format = "#0.00%";
                            gpralist[72] = Convert.ToDouble(gpralist[72]) + Convert.ToDouble(worksheet.Cells[row, 72].Value);

                        }
                        if (achievementMath[0].Indian >= 0 && participationMath[0].Indian > 0)
                        {
                            worksheet.Cells[row, 75].Value = Convert.ToDouble(achievementMath[0].Indian) / participationMath[0].Indian;
                            worksheet.Cells[row, 75].Style.Numberformat.Format = "#0.00%";
                            gpralist[75] = Convert.ToDouble(gpralist[75]) + Convert.ToDouble(worksheet.Cells[row, 75].Value);
                        }
                        if (achievementMath[0].Asian >= 0 && participationMath[0].Asian > 0)
                        {
                            worksheet.Cells[row, 78].Value = Convert.ToDouble(achievementMath[0].Asian) / participationMath[0].Asian;
                            worksheet.Cells[row, 78].Style.Numberformat.Format = "#0.00%";
                            gpralist[78] = Convert.ToDouble(gpralist[78]) + Convert.ToDouble(worksheet.Cells[row, 78].Value);
                        }
                        if (achievementMath[0].Black >= 0 && participationMath[0].Black > 0)
                        {
                            worksheet.Cells[row, 81].Value = Convert.ToDouble(achievementMath[0].Black) / participationMath[0].Black;
                            worksheet.Cells[row, 81].Style.Numberformat.Format = "#0.00%";
                            gpralist[81] = Convert.ToDouble(gpralist[81]) + Convert.ToDouble(worksheet.Cells[row, 81].Value);
                        }

                        if (achievementMath[0].Hispanic >= 0 && participationMath[0].Hispanic > 0)
                        {
                            worksheet.Cells[row, 84].Value = Convert.ToDouble(achievementMath[0].Hispanic) / participationMath[0].Hispanic;
                            worksheet.Cells[row, 84].Style.Numberformat.Format = "#0.00%";
                            gpralist[84] = Convert.ToDouble(gpralist[84]) + Convert.ToDouble(worksheet.Cells[row, 84].Value);
                        }
                        if (achievementMath[0].Hawaiian >= 0 && participationMath[0].Hawaiian > 0)
                        {
                            worksheet.Cells[row, 87].Value = Convert.ToDouble(achievementMath[0].Hawaiian) / participationMath[0].Hawaiian;
                            worksheet.Cells[row, 87].Style.Numberformat.Format = "#0.00%";
                            gpralist[87] = Convert.ToDouble(gpralist[87]) + Convert.ToDouble(worksheet.Cells[row, 87].Value);
                        }
                        if (achievementMath[0].White >= 0 && participationMath[0].White > 0)
                        {
                            worksheet.Cells[row, 90].Value = Convert.ToDouble(achievementMath[0].White) / participationMath[0].White;
                            worksheet.Cells[row, 90].Style.Numberformat.Format = "#0.00%";
                            gpralist[90] = Convert.ToDouble(gpralist[90]) + Convert.ToDouble(worksheet.Cells[row, 90].Value);
                        }
                        if (achievementMath[0].MultiRaces >= 0 && participationMath[0].MultiRaces > 0)
                        {
                            worksheet.Cells[row, 93].Value = Convert.ToDouble(achievementMath[0].MultiRaces) / participationMath[0].MultiRaces;
                            worksheet.Cells[row, 93].Style.Numberformat.Format = "#0.00%";
                            gpralist[93] = Convert.ToDouble(gpralist[93]) + Convert.ToDouble(worksheet.Cells[row, 93].Value);
                        }

                        if (achievementMath[0].ExtraFiled2 >= 0 && participationMath[0].ExtraFiled2 > 0)
                        {
                            worksheet.Cells[row, 96].Value = Convert.ToDouble(achievementMath[0].ExtraFiled2) / participationMath[0].ExtraFiled2;
                            worksheet.Cells[row, 96].Style.Numberformat.Format = "#0.00%";
                            gpralist[96] = Convert.ToDouble(gpralist[96]) + Convert.ToDouble(worksheet.Cells[row, 96].Value);
                        }
                        if (achievementMath[0].ExtraFiled1 >= 0 && participationMath[0].ExtraFiled1 > 0)
                        {
                            worksheet.Cells[row, 99].Value = Convert.ToDouble(achievementMath[0].ExtraFiled1) / participationMath[0].ExtraFiled1;
                            worksheet.Cells[row, 99].Style.Numberformat.Format = "#0.00%";
                            gpralist[99] = Convert.ToDouble(gpralist[99]) + Convert.ToDouble(worksheet.Cells[row, 99].Value);
                        }
                    }

                    var performanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 7);
                    if (performanceMeasure.Count > 0)
                    {
                        if (performanceMeasure[0].Budget != null)
                        {
                            TotalSchools++;
                            worksheet.Cells[row, 100].Value = Convert.ToDouble(performanceMeasure[0].Budget);
                            worksheet.Cells[row, 100].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";
                        }
                        worksheet.Cells[row, 101].Value = performanceMeasure[0].Total; //Studs_Served
                        gpralist[100] = Convert.ToDouble(gpralist[100]) + Convert.ToDouble(worksheet.Cells[row, 100].Value);
                        gpralist[101] = Convert.ToDouble(gpralist[101]) + Convert.ToDouble(worksheet.Cells[row, 101].Value);

                    }
                } //end for gpraData
            } //end for school
        } //end for report
        //add total acount and % rows

        row++;
        worksheet.Cells[row, 19].Value = "Total:";
        using (ExcelRange rng = worksheet.Cells["T" + row + ":CT" + row])
        {
            rng.Style.Numberformat.Format = "#,##0";
        }
        using (ExcelRange rng = worksheet.Cells["CW" + row + ":CW" + row])
        {
            rng.Style.Numberformat.Format = "#,##0";
        }

        worksheet.Cells[row, 20].Value = gpralist[20];
        worksheet.Cells[row + 3, 20].Value = Convert.ToDouble(gpralist[20]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 20].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 21].Value = gpralist[21];
        worksheet.Cells[row + 3, 21].Value = Convert.ToDouble(gpralist[21]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 21].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 22].Value = gpralist[22];
        worksheet.Cells[row + 3, 22].Value = Convert.ToDouble(gpralist[22]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 22].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 23].Value = gpralist[23];
        worksheet.Cells[row + 3, 23].Value = Convert.ToDouble(gpralist[23]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 23].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 24].Value = gpralist[24];
        worksheet.Cells[row + 3, 24].Value = Convert.ToDouble(gpralist[24]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 24].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 25].Value = gpralist[25];
        worksheet.Cells[row + 3, 25].Value = Convert.ToDouble(gpralist[25]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 25].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 26].Value = gpralist[26];
        worksheet.Cells[row + 3, 26].Value = Convert.ToDouble(gpralist[26]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 26].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 27].Value = gpralist[27];
        worksheet.Cells[row + 3, 27].Value = Convert.ToDouble(gpralist[27]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 27].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 28].Value = gpralist[28];
        worksheet.Cells[row + 3, 28].Value = Convert.ToDouble(gpralist[28]) / Convert.ToDouble(gpralist[28]);
        worksheet.Cells[row + 3, 28].Style.Numberformat.Format = "#0.00%";
        worksheet.Cells[row, 29].Value = gpralist[29];
        worksheet.Cells[row, 30].Value = gpralist[30];

        worksheet.Cells[row, 31].Value = gpralist[31];
        worksheet.Cells[row, 32].Value = gpralist[32];
        worksheet.Cells[row, 33].Value = gpralist[33];
        worksheet.Cells[row, 34].Value = gpralist[34];
        worksheet.Cells[row, 35].Value = gpralist[35];
        worksheet.Cells[row, 36].Value = gpralist[36];
        worksheet.Cells[row, 37].Value = gpralist[37];
        worksheet.Cells[row, 38].Value = gpralist[38];
        worksheet.Cells[row, 39].Value = gpralist[39];
        worksheet.Cells[row, 40].Value = gpralist[40];
        worksheet.Cells[row, 41].Value = gpralist[41];
        //worksheet.Cells[row, 42].Value = Convert.ToDouble(gpralist[42]) / (row - 1);
        //worksheet.Cells[row, 42].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 43].Value = gpralist[43];
        worksheet.Cells[row, 44].Value = gpralist[44];
        //worksheet.Cells[row, 45].Value = Convert.ToDouble(gpralist[45]) / (row - 1);
        //worksheet.Cells[row, 45].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 46].Value = gpralist[46];
        worksheet.Cells[row, 47].Value = gpralist[47];
        //worksheet.Cells[row, 48].Value = Convert.ToDouble(gpralist[48]) / (row - 1); 
        //worksheet.Cells[row, 48].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 49].Value = gpralist[49];
        worksheet.Cells[row, 50].Value = gpralist[50];
        //worksheet.Cells[row, 51].Value = Convert.ToDouble(gpralist[51]) / (row - 1); 

        worksheet.Cells[row, 52].Value = gpralist[52];
        worksheet.Cells[row, 53].Value = gpralist[53];
        //worksheet.Cells[row, 54].Value = Convert.ToDouble(gpralist[54]) / (row - 1); 
        //worksheet.Cells[row, 54].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 55].Value = gpralist[55];
        worksheet.Cells[row, 56].Value = gpralist[56];
        //worksheet.Cells[row, 57].Value = Convert.ToDouble(gpralist[57]) / (row - 1); 
        //worksheet.Cells[row, 57].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 58].Value = gpralist[58];
        worksheet.Cells[row, 59].Value = gpralist[59];
        //worksheet.Cells[row, 60].Value = Convert.ToDouble(gpralist[60]) / (row - 1); 
        //worksheet.Cells[row, 60].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 61].Value = gpralist[61];
        worksheet.Cells[row, 62].Value = gpralist[62];
        //worksheet.Cells[row, 63].Value = Convert.ToDouble(gpralist[63]) / (row - 1);
        //worksheet.Cells[row, 63].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 64].Value = gpralist[64];
        worksheet.Cells[row, 65].Value = gpralist[65];
        //worksheet.Cells[row, 66].Value = Convert.ToDouble(gpralist[66]) / (row - 1); 
        //worksheet.Cells[row, 66].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 67].Value = gpralist[67];
        worksheet.Cells[row, 68].Value = gpralist[68];
        //worksheet.Cells[row, 69].Value = Convert.ToDouble(gpralist[69]) / (row - 1); 
        //worksheet.Cells[row, 69].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 70].Value = gpralist[70];
        worksheet.Cells[row, 71].Value = gpralist[71];
        //worksheet.Cells[row, 72].Value = Convert.ToDouble(gpralist[72]) / (row - 1); 
        //worksheet.Cells[row, 72].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 73].Value = gpralist[73];
        worksheet.Cells[row, 74].Value = gpralist[74];
        //worksheet.Cells[row, 75].Value = Convert.ToDouble(gpralist[75]) / (row - 1); 
        //worksheet.Cells[row, 75].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 76].Value = gpralist[76];
        worksheet.Cells[row, 77].Value = gpralist[77];
        //worksheet.Cells[row, 78].Value = Convert.ToDouble(gpralist[78]) / (row - 1); 
        //worksheet.Cells[row, 78].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 79].Value = gpralist[79];
        worksheet.Cells[row, 80].Value = gpralist[80];
        //worksheet.Cells[row, 81].Value = Convert.ToDouble(gpralist[81]) / (row - 1); 
        //worksheet.Cells[row, 81].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 82].Value = gpralist[82];
        worksheet.Cells[row, 83].Value = gpralist[83];
        //worksheet.Cells[row, 84].Value = Convert.ToDouble(gpralist[84]) / (row - 1); 
        //worksheet.Cells[row, 84].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 85].Value = gpralist[85];
        worksheet.Cells[row, 86].Value = gpralist[86];
        //worksheet.Cells[row, 87].Value = Convert.ToDouble(gpralist[87]) / (row - 1); 
        //worksheet.Cells[row, 87].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 88].Value = gpralist[88];
        worksheet.Cells[row, 89].Value = gpralist[89];
        //worksheet.Cells[row, 90].Value = Convert.ToDouble(gpralist[90]) / (row - 1); 
        //worksheet.Cells[row, 90].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 91].Value = gpralist[91];
        worksheet.Cells[row, 92].Value = gpralist[92];
        //worksheet.Cells[row, 93].Value = Convert.ToDouble(gpralist[93]) / (row - 1); 
        //worksheet.Cells[row, 93].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 94].Value = gpralist[94];
        worksheet.Cells[row, 95].Value = gpralist[95];
        //worksheet.Cells[row, 96].Value = Convert.ToDouble(gpralist[96]) / (row - 1); 
        //worksheet.Cells[row, 96].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 97].Value = gpralist[97];
        worksheet.Cells[row, 98].Value = gpralist[98];
        //worksheet.Cells[row, 99].Value = Convert.ToDouble(gpralist[99]) / (row - 1); 
        //worksheet.Cells[row, 99].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 100].Value = Convert.ToDouble(gpralist[100]);
        worksheet.Cells[row, 100].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

        worksheet.Cells[row + 1, 100].Value = Convert.ToDouble(gpralist[100]) / TotalSchools;
        worksheet.Cells[row + 1, 100].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";
        worksheet.Cells[row + 1, 100].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        worksheet.Cells[row + 1, 100].Style.Font.Bold = true;

        worksheet.Cells[row, 101].Value = gpralist[101];
        worksheet.Cells[row + 1, 101].Value = Convert.ToDouble(gpralist[101]) / TotalSchools;
        worksheet.Cells[row + 1, 101].Style.Numberformat.Format = "#,##0.0";
        worksheet.Cells[row + 1, 101].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        worksheet.Cells[row + 1, 101].Style.Font.Bold = true;

        worksheet.Cells[row + 2, 100].Value = "Average annual MSAP funds expended at a school";
        worksheet.Cells[row + 2, 101].Value = "Average number of students served by a school's magnet program";

        using (ExcelRange rng = worksheet.Cells["CV" + (row + 3) + ":CW" + (row + 3)])
        {
            rng.Merge = true;
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.Value = Convert.ToDouble(worksheet.Cells[row + 1, 100].Value) / Convert.ToDouble(worksheet.Cells[row + 1, 101].Value);
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";


        }

        using (ExcelRange rng = worksheet.Cells["S" + (row + 1) + ":CW" + (row + 1)])
        {
            rng.Style.Border.Top.Style = ExcelBorderStyle.Thick;
        }

        worksheet.Column(100).Width = 20;

        worksheet.Column(20).Width = 25;
        worksheet.Column(21).Width = 25;
        worksheet.Column(22).Width = 25;
        worksheet.Column(23).Width = 25;
        worksheet.Column(24).Width = 25;
        worksheet.Column(25).Width = 25;
        worksheet.Column(26).Width = 25;
        worksheet.Column(27).Width = 25;
        worksheet.Column(28).Width = 25;
        worksheet.Column(39).Width = 30;
        worksheet.Column(39).Style.WrapText = true;


        using (ExcelRange rng = worksheet.Cells["T" + row + ":AB" + (row + 4)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
        }

        using (ExcelRange rng = worksheet.Cells["T" + row + ":AB" + (row) + ",T" + (row + 3) + ":AB" + (row + 3)])
        {
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.Font.Bold = true;
        }
        using (ExcelRange rng = worksheet.Cells["T" + (row + 6) + ":AB" + (row + 6)])
        {
            rng.Merge = true;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.IsRichText = true;
            ExcelRichText ert0 = rng.RichText.Add("GPRA Measure 1:");
            ert0.Bold = true;
            ert0 = rng.RichText.Add("  The percentage of magnet schools whose student applicant pool reduces, prevents, or eliminates minority group isolation.");
            ert0.Bold = false;

            rng.Style.Border.Top.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            rng.Style.Font.Size = 10;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

        }

        worksheet.Cells[(row + 1), 20].Value = "Total number of American Indian or Alaska Native applicants";
        worksheet.Cells[(row + 4), 20].Value = "Percentage of American Indian or Alaska Native applicants";

        worksheet.Cells[(row + 1), 21].Value = "Total number of Asian Applicants";
        worksheet.Cells[(row + 4), 21].Value = "Percentage of Asian Applicants";

        worksheet.Cells[(row + 1), 22].Value = "Total number of Black or African-American applicants";
        worksheet.Cells[(row + 4), 22].Value = "Percentage of Black or African-American applicants";

        worksheet.Cells[(row + 1), 23].Value = "Total number of Hispanic or Latino applicants";
        worksheet.Cells[(row + 4), 23].Value = "Percentage of Hispanic or Latino applicants";

        worksheet.Cells[(row + 1), 24].Value = "Total number of Native Hawaiian or Other Pacific Islander applicants";
        worksheet.Cells[(row + 4), 24].Value = "Percentage of Native Hawaiian or Other Pacific Islander applicants";

        worksheet.Cells[(row + 1), 25].Value = "Total number of White applicants";
        worksheet.Cells[(row + 4), 25].Value = "Percentage of White applicants";

        worksheet.Cells[(row + 1), 26].Value = "Total number of Two or more races";
        worksheet.Cells[(row + 4), 26].Value = "Percentage of Two or more races";

        worksheet.Cells[(row + 1), 27].Value = "Total number of students who did not declare race/ethnicity";
        worksheet.Cells[(row + 4), 27].Value = "Percentage of students who did not declare race/ethnicity";

        worksheet.Cells[(row + 1), 28].Value = "Total number of applicants";
        worksheet.Cells[(row + 4), 28].Value = "";


        using (ExcelRange rng = worksheet.Cells["A" + row + ":S" + row])
        {
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }

        using (ExcelRange rng = worksheet.Cells["T" + row + ":CW" + row])
        {
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        using (ExcelRange rng = worksheet.Cells["T2:CW" + row])
        {
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }
        row++;  //row number under "total:" line

        using (ExcelRange rng = worksheet.Cells["AN" + (row + 1) + ":AO" + (row + 1)])
        {
            rng.Merge = true;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rng.Style.WrapText = true;
            rng.IsRichText = true;
            ExcelRichText ert0 = rng.RichText.Add("GPRA Measure 2: ");
            ert0.Bold = true;
            ert0 = rng.RichText.Add("The percentage of magnet schools whose students from major racial and ethnic groups meet or exceed State annual progress standards in reading/language arts.");
            ert0.Bold = false;

            rng.Style.Border.Top.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            rng.Style.Font.Size = 10;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

        }

        using (ExcelRange rng = worksheet.Cells["AN" + (row + 2) + ":AO" + (row + 2)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;

        }
        worksheet.Cells[row + 2, 40].IsRichText = true;
        ExcelRichText ert = worksheet.Cells[row + 2, 40].RichText.Add("GPRA Measure 2a: ");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 40].RichText.Add("Total number of students from major racial and ethnic groups that participated in the State annual progress standards in the State annual progress standards in reading/language arts.");
        ert.Bold = false;

        //worksheet.Cells[row + 1, 40].Value = "Total number of students from major racial and ethnic groups that participated in the State annual progress standards in the State annual progress standards in reading/language arts.";

        worksheet.Cells[row + 2, 41].IsRichText = true;
        ert = worksheet.Cells[row + 2, 41].RichText.Add("GPRA Measure 2b: ");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 41].RichText.Add("Total number of students from major racial and ethnic groups that ");
        ert.Bold = false;
        ert = worksheet.Cells[row + 2, 41].RichText.Add("met or exceeded");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 41].RichText.Add(" in the State annual progress standards in the State annual progress standards in mathematics.");
        ert.Bold = false;

        worksheet.Column(40).Width = 40;
        worksheet.Column(41).Width = 40;

        using (ExcelRange rng = worksheet.Cells["AN" + row + ":AO" + (row + 1)])
        {
            rng.Style.Font.Size = 10;
            rng.Style.WrapText = true;
        }

        using (ExcelRange rng = worksheet.Cells["AM" + (row + 3) + ":AO" + (row + 3)])
        {
            rng.Style.Font.Bold = true;
        }

        using (ExcelRange rng = worksheet.Cells["AM" + (row + 4) + ":AO" + (row + 4)])
        {
            rng.Style.Font.Bold = true;
        }

        worksheet.Cells[(row + 3), 39].Value = "With White Students:";
        int sum = 0;
        //with white student col1 SUM(AQ155, AT155, AW155, AZ155, BC155,BF155,  BI155)
        for (int i = 43; i < 62; i = (i + 3))
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 40].Value = sum;
        worksheet.Cells[(row + 3), 40].Style.Numberformat.Format = "#,##0";
        sum = 0;
        //with white student col2 SUM(AR155, AU155, AX155, BA155, BD155, BG155, BJ155)
        for (int i = 44; i < 63; i = (i + 3))
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 41].Value = sum;
        worksheet.Cells[(row + 3), 41].Style.Numberformat.Format = "#,##0";


        worksheet.Cells[(row + 4), 39].Value = "Without White Students:";
        sum = 0;
        //without white student col1 SUM(AQ155, AT155, AW155, AZ155, BC155, BI155)
        for (int i = 43; i < 56; i = (i + 3))
            sum += Convert.ToInt32(gpralist[i]);

        sum += Convert.ToInt32(gpralist[61]);

        worksheet.Cells[(row + 4), 40].Value = sum;
        worksheet.Cells[(row + 4), 40].Style.Numberformat.Format = "#,##0";


        //without white student col2
        sum = 0;
        for (int i = 44; i < 57; i = (i + 3)) //SUM(AR155, AU155, AX155, BA155, BD155, BJ155)
            sum += Convert.ToInt32(gpralist[i]);

        sum += Convert.ToInt32(gpralist[62]);
        worksheet.Cells[(row + 4), 41].Value = sum;
        worksheet.Cells[(row + 4), 41].Style.Numberformat.Format = "#,##0";
        ///////end g2 
        ///gpra3
        using (ExcelRange rng = worksheet.Cells["BR" + (row + 1) + ":BS" + (row + 1)])
        {
            rng.Merge = true;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.IsRichText = true;
            ExcelRichText ert0 = rng.RichText.Add("GPRA Measure 3: ");
            ert0.Bold = true;
            ert0 = rng.RichText.Add("The percentage of magnet schools whose students from major racial and ethnic groups meet or exceed State annual progress standards in mathematics.");
            ert0.Bold = false;

            rng.Style.Border.Top.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            rng.Style.Font.Size = 10;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

        }

        using (ExcelRange rng = worksheet.Cells["BR" + (row + 2) + ":BS" + (row + 2)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;
        }

        worksheet.Column(70).Width = 40;
        worksheet.Column(71).Width = 40;

        using (ExcelRange rng = worksheet.Cells["BR" + row + ":BS" + (row + 1)])
        {
            rng.Style.Font.Size = 10;
            rng.Style.WrapText = true;
        }

        using (ExcelRange rng = worksheet.Cells["BQ" + (row + 3) + ":BS" + (row + 3)])
        {
            rng.Style.Font.Bold = true;
        }

        using (ExcelRange rng = worksheet.Cells["BQ" + (row + 4) + ":BS" + (row + 4)])
        {
            rng.Style.Font.Bold = true;
        }

        worksheet.Column(69).Width = 35;
        worksheet.Cells[(row + 3), 69].Value = "With White Students:";
        worksheet.Cells[(row + 3), 69].Style.WrapText = true;
        sum = 0;
        //with white student col1
        for (int i = 73; i < 92; i = (i + 3)) //(BU155, BX155, CA155, CD155, CG155, CJ155, CM155)
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 70].Value = sum;
        worksheet.Cells[(row + 3), 70].Style.Numberformat.Format = "#,##0";
        sum = 0;
        //with white student col2
        for (int i = 74; i < 93; i = (i + 3))  //SUM(BV155, BY155, CB155, CE155, CH155, CK155, CN155)
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 71].Value = sum;
        worksheet.Cells[(row + 3), 71].Style.Numberformat.Format = "#,##0";


        worksheet.Cells[(row + 4), 69].Value = "Without White Students:";
        worksheet.Cells[(row + 4), 69].Style.WrapText = true;
        sum = 0;
        //without white student col1
        for (int i = 73; i < 86; i = (i + 3)) //=SUM(BU155, BX155, CA155, CD155, CG155, CM155)
            sum += Convert.ToInt32(gpralist[i]);

        sum += Convert.ToInt32(gpralist[91]);

        worksheet.Cells[(row + 4), 70].Value = sum;
        worksheet.Cells[(row + 4), 70].Style.Numberformat.Format = "#,##0";

        //without white student col2
        sum = 0;
        for (int i = 74; i < 87; i = (i + 3))  //SUM(BV155, BY155, CB155, CE155, CH155, CN155)
            sum += Convert.ToInt32(gpralist[i]);
        sum += Convert.ToInt32(gpralist[92]);

        worksheet.Cells[(row + 4), 71].Value = sum;
        worksheet.Cells[(row + 4), 71].Style.Numberformat.Format = "#,##0";
        ///////
        ///end gpra3
        worksheet.Cells[row, 70].IsRichText = true;
        ert = worksheet.Cells[row + 2, 70].RichText.Add("GPRA Measure 3a:");
        ert.Bold = true;

        ert = worksheet.Cells[row + 2, 70].RichText.Add("  Total number of students from major racial and ethnic groups that participated in the State annual progress standards in the State annual progress standards in mathematics.");
        ert.Bold = false;

        worksheet.Cells[row + 2, 71].IsRichText = true;
        ert = worksheet.Cells[row + 2, 71].RichText.Add("GPRA Measure 3b:");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 71].RichText.Add("  Total number of students from major racial and ethnic groups that");
        ert.Bold = false;
        ert = worksheet.Cells[row + 2, 71].RichText.Add(" met or exceeded ");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 71].RichText.Add(" in the State annual progress standards in the State annual progress standards in mathematics.");
        ert.Bold = false;

        worksheet.Column(70).Width = 40;
        worksheet.Column(71).Width = 40;

        using (ExcelRange rng = worksheet.Cells["BR" + row + ":BS" + (row + 1)])
        {
            rng.Style.Font.Size = 10;
            rng.Style.WrapText = true;
        }

        using (ExcelRange rng = worksheet.Cells["CV" + (row + 1) + ":CW" + (row + 1)])
        {
            rng.Style.WrapText = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
        }
        using (ExcelRange rng = worksheet.Cells["CV" + (row + 2) + ":CW" + (row + 2)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        using (ExcelRange rng = worksheet.Cells["CV" + (row + 3) + ":CW" + (row + 3)])
        {
            rng.Value = "MSAP_Funds/Studs_Served";
            rng.Merge = true;
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }



        using (ExcelRange rng = worksheet.Cells["CV" + (row + 4) + ":CW" + (row + 4)])
        {
            rng.Merge = true;
            rng.Style.Font.Size = 10;
            rng.Style.WrapText = true;

            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.IsRichText = true;
            ExcelRichTextCollection rtfCollection = rng.RichText;
            ert = rtfCollection.Add("GPRA Measure 6:");
            ert.Bold = true;

            ert = rtfCollection.Add(" The cost per student in a magnet school. ");
            ert.Bold = false;

        }

        worksheet.Row(row + 4).Height = 30;

    }

    //old code--not used
    private static void GetGPRA2013(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
        int TotalSchools = 0;
        int granteeID = -1;
        bool isAlternative = false;

        Hashtable SIScode = new Hashtable();
        SIScode.Add(8,1);
        SIScode.Add(4,2);
        SIScode.Add(23,3);
        SIScode.Add(2,4);
        SIScode.Add(5,5);
        SIScode.Add(6,6);
        SIScode.Add(7,7);
        SIScode.Add(1,8);
        SIScode.Add(9,9);
        SIScode.Add(10,10);
        SIScode.Add(11,11);
        SIScode.Add(12,12);
        SIScode.Add(13,13);
        SIScode.Add(14,14);
        SIScode.Add(15,15);
        SIScode.Add(16,16);
        SIScode.Add(17,17);
        SIScode.Add(18,18);
        SIScode.Add(19,19);
        SIScode.Add(20,20);
        SIScode.Add(21,21);
        SIScode.Add(22,22);
        SIScode.Add(3,23);
        SIScode.Add(24,24);
        SIScode.Add(26,26);
        SIScode.Add(27,27);
        SIScode.Add(28,28);
        SIScode.Add(29,98);

        Hashtable gpralist = new Hashtable();

        gpralist.Add(20, 0);
        gpralist.Add(21, 0);
        gpralist.Add(22, 0);
        gpralist.Add(23, 0);
        gpralist.Add(24, 0);
        gpralist.Add(25, 0);
        gpralist.Add(26, 0);
        gpralist.Add(27, 0);
        gpralist.Add(28, 0);
        gpralist.Add(29, 0);
        gpralist.Add(30, 0);
        gpralist.Add(31, 0);
        gpralist.Add(32, 0);
        gpralist.Add(33, 0);
        gpralist.Add(34, 0);
        gpralist.Add(35, 0);
        gpralist.Add(36, 0);
        gpralist.Add(37, 0);
        gpralist.Add(38, 0);
        gpralist.Add(39, 0);
        gpralist.Add(40, 0);
        gpralist.Add(41, 0);
        gpralist.Add(42, 0);
        gpralist.Add(43, 0);
        gpralist.Add(44, 0);
        gpralist.Add(45, 0);
        gpralist.Add(46, 0);
        gpralist.Add(47, 0);
        gpralist.Add(48, 0);
        gpralist.Add(49, 0);
        gpralist.Add(50, 0);
        gpralist.Add(51, 0);
        gpralist.Add(52, 0);
        gpralist.Add(53, 0);
        gpralist.Add(54, 0);
        gpralist.Add(55, 0);
        gpralist.Add(56, 0);
        gpralist.Add(57, 0);
        gpralist.Add(58, 0);
        gpralist.Add(59, 0);
        gpralist.Add(60, 0);
        gpralist.Add(61, 0);
        gpralist.Add(62, 0);
        gpralist.Add(63, 0);
        gpralist.Add(64, 0);
        gpralist.Add(65, 0);
        gpralist.Add(66, 0);
        gpralist.Add(67, 0);
        gpralist.Add(68, 0);
        gpralist.Add(69, 0);
        gpralist.Add(70, 0);
        gpralist.Add(71, 0);
        gpralist.Add(72, 0);
        gpralist.Add(73, 0);
        gpralist.Add(74, 0);
        gpralist.Add(75, 0);
        gpralist.Add(76, 0);
        gpralist.Add(77, 0);
        gpralist.Add(78, 0);
        gpralist.Add(79, 0);
        gpralist.Add(80, 0);
        gpralist.Add(81, 0);
        gpralist.Add(82, 0);
        gpralist.Add(83, 0);
        gpralist.Add(84, 0);
        gpralist.Add(85, 0);
        gpralist.Add(86, 0);
        gpralist.Add(87, 0);
        gpralist.Add(88, 0);
        gpralist.Add(89, 0);
        gpralist.Add(90, 0);
        gpralist.Add(91, 0);
        gpralist.Add(92, 0);
        gpralist.Add(93, 0);
        gpralist.Add(94, 0);
        gpralist.Add(95, 0);
        gpralist.Add(96, 0);
        gpralist.Add(97, 0);
        gpralist.Add(98, 0);
        gpralist.Add(99, 0);
        gpralist.Add(100, 0);
        gpralist.Add(101, 0);
        gpralist.Add(102, 0);
        gpralist.Add(103, 0);
        gpralist.Add(104, 0);
        gpralist.Add(105, 0);
        gpralist.Add(106, 0);
        gpralist.Add(107, 0);
        gpralist.Add(108, 0);
        gpralist.Add(109, 0);
        gpralist.Add(110, 0);
        gpralist.Add(111, 0);
        gpralist.Add(112, 0);
        gpralist.Add(113, 0);
        gpralist.Add(114, 0);
        gpralist.Add(115, 0);
        gpralist.Add(116, 0);
        gpralist.Add(117, 0);
        gpralist.Add(118, 0);
        gpralist.Add(119, 0);
        gpralist.Add(120, 0);
        gpralist.Add(121, 0);
        gpralist.Add(122, 0);
        gpralist.Add(123, 0);
        gpralist.Add(124, 0);
        gpralist.Add(125, 0);
        gpralist.Add(126, 0);
        gpralist.Add(127, 0);
        gpralist.Add(128, 0);
        gpralist.Add(129, 0);
        gpralist.Add(130, 0);
        gpralist.Add(131, 0);
        gpralist.Add(132, 0);
        gpralist.Add(133, 0);
        gpralist.Add(134, 0);
        gpralist.Add(135, 0);
        gpralist.Add(136, 0);
        gpralist.Add(137, 0);
        gpralist.Add(138, 0);
        gpralist.Add(139, 0);
        gpralist.Add(140, 0);
        gpralist.Add(141, 0);
        gpralist.Add(142, 0);
        gpralist.Add(143, 0);
        gpralist.Add(144, 0);
        gpralist.Add(145, 0);

        Hashtable ImpvStatus = new Hashtable();
        ImpvStatus.Add(8, "Approaching target");
        ImpvStatus.Add(4, "Celebration");
        ImpvStatus.Add(23, "Continuous improvement");
        ImpvStatus.Add(2, "Corrective action");
        ImpvStatus.Add(5, "Excelling school");
        ImpvStatus.Add(6, "Focus");
        ImpvStatus.Add(7, "Focus targeted");
        ImpvStatus.Add(1, "Improvement");
        ImpvStatus.Add(9, "Improvement required");
        ImpvStatus.Add(10, "In improvement");
        ImpvStatus.Add(11, "Level 1");
        ImpvStatus.Add(12, "Level 2");
        ImpvStatus.Add(13, "Level 3 (focus)");
        ImpvStatus.Add(14, "Level 4 (priority)");
        ImpvStatus.Add(15, "Level 5 (priority)");
        ImpvStatus.Add(16, "Met alternative standard");
        ImpvStatus.Add(17, "Met standard");
        ImpvStatus.Add(18, "On target");
        ImpvStatus.Add(19, "Performance");
        ImpvStatus.Add(20, "Priority");
        ImpvStatus.Add(21, "Priority improvement");
        ImpvStatus.Add(22, "Progressing");
        ImpvStatus.Add(3, "Restructuring");
        ImpvStatus.Add(24, "Review (including focus)");
        ImpvStatus.Add(25, "Reward");
        ImpvStatus.Add(26, "Transitioning");
        ImpvStatus.Add(27, "Turnaround");
        ImpvStatus.Add(28, "Other");
        ImpvStatus.Add(29, "N/A");
        ImpvStatus.Add(99, "missing");

        worksheet.Name = "GPRA";

        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "SchoolID";
        worksheet.Cells[row, 4].Value = "PRAward";
        worksheet.Cells[row, 5].Value = "School_Name";
        worksheet.Cells[row, 6].Value = "Grantee_Name";
        worksheet.Cells[row, 7].Value = "Grades";
        worksheet.Cells[row, 8].Value = "Program_Type";
        worksheet.Cells[row, 9].Value = "Coded_Program_Type";
        worksheet.Cells[row, 10].Value = "Title_I";
        worksheet.Cells[row, 11].Value = "Coded_Title_I";
        worksheet.Cells[row, 12].Value = "School_Improvement";
        worksheet.Cells[row, 13].Value = "Coded_School_Improvement";
        worksheet.Cells[row, 14].Value = "School_Improvement_Status";
        worksheet.Cells[row, 15].Value = "Coded_SIS";
        worksheet.Cells[row, 16].Value = "Lowest_Achieving";
        worksheet.Cells[row, 17].Value = "Coded_Lowest_Achieving";
        worksheet.Cells[row, 18].Value = "SIG";
        worksheet.Cells[row, 19].Value = "Coded_SIG";
        worksheet.Cells[row, 20].Value = "App_Amer_Indian";
        worksheet.Cells[row, 21].Value = "App_Asian";
        worksheet.Cells[row, 22].Value = "App_Black";
        worksheet.Cells[row, 23].Value = "App_Hispanic";
        worksheet.Cells[row, 24].Value = "App_Native_Hawaiian";
        worksheet.Cells[row, 25].Value = "App_White";
        worksheet.Cells[row, 26].Value = "App_Two_more";
        worksheet.Cells[row, 27].Value = "Undeclared";
        worksheet.Cells[row, 28].Value = "App_Total";
        worksheet.Cells[row, 29].Value = "Enr_Amer_Indian";
        worksheet.Cells[row, 30].Value = "Enr_Asian";
        worksheet.Cells[row, 31].Value = "Enr_Black";
        worksheet.Cells[row, 32].Value = "Enr_Hispanic";
        worksheet.Cells[row, 33].Value = "Enr_Native_Hawaiian";
        worksheet.Cells[row, 34].Value = "Enr_White";
        worksheet.Cells[row, 35].Value = "Enr_Two_more";
        worksheet.Cells[row, 36].Value = "Enr_Total";
        worksheet.Cells[row, 37].Value = "New_Amer_Indian";
        worksheet.Cells[row, 38].Value = "New_Asian";
        worksheet.Cells[row, 39].Value = "New_Black";
        worksheet.Cells[row, 40].Value = "New_Hispanic";
        worksheet.Cells[row, 41].Value = "New_Native_Hawaiian";
        worksheet.Cells[row, 42].Value = "New_White";
        worksheet.Cells[row, 43].Value = "New_Two_more";
        //New_students
        worksheet.Cells[row, 44].Value = "Total_New";
        worksheet.Cells[row, 45].Value = "Cont_Amer_Indian";
        worksheet.Cells[row, 46].Value = "Cont_Asian";
        worksheet.Cells[row, 47].Value = "Cont_Black";
        worksheet.Cells[row, 48].Value = "Cont_Hispanic";
        worksheet.Cells[row, 49].Value = "Cont_Native_Hawaiian";
        worksheet.Cells[row, 50].Value = "Cont_White";
        worksheet.Cells[row, 51].Value = "Cont_Two_more";
        //Continuing_Students
        worksheet.Cells[row, 52].Value = "Total_Cont";

        // offset 53-39=14 to 101
        worksheet.Cells[row, 53].Value = "Total_Sch_Enroll";

        worksheet.Cells[row, 54].Value = "Part_AYP_R_All";
        worksheet.Cells[row, 55].Value = "Met_AYP_R_All";
        worksheet.Cells[row, 56].Value = "%_AYP_R_All";

        worksheet.Cells[row, 57].Value = "Part_AYP_R_Amer_Ind";
        worksheet.Cells[row, 58].Value = "Met_AYP_R_Amer_Ind";
        worksheet.Cells[row, 59].Value = "%_AYP_R_Amer_Ind";

        worksheet.Cells[row, 60].Value = "Part_AYP_R_Asian";
        worksheet.Cells[row, 61].Value = "Met_AYP_R_Asian";
        worksheet.Cells[row, 62].Value = "%_AYP_R_Asian";

        worksheet.Cells[row, 63].Value = "Part_AYP_R_Black";
        worksheet.Cells[row, 64].Value = "Met_AYP_R_Black";
        worksheet.Cells[row, 65].Value = "%_AYP_R_Black";

        worksheet.Cells[row, 66].Value = "Part_AYP_R_Hispanic";
        worksheet.Cells[row, 67].Value = "Met_AYP_R_Hispanic";
        worksheet.Cells[row, 68].Value = "%_AYP_R_Hispanic";

        worksheet.Cells[row, 69].Value = "Part_AYP_R_Native_Hawaiian";
        worksheet.Cells[row, 70].Value = "Met_AYP_R_Native_Hawaiian";
        worksheet.Cells[row, 71].Value = "%_AYP_R_Native_Hawaiian";

        worksheet.Cells[row, 72].Value = "Part_AYP_R_White";
        worksheet.Cells[row, 73].Value = "Met_AYP_R_White";
        worksheet.Cells[row, 74].Value = "%_AYP_R_White";

        worksheet.Cells[row, 75].Value = "Part_AYP_R_TwoOrMore";
        worksheet.Cells[row, 76].Value = "Met_AYP_R_TwoOrMore";
        worksheet.Cells[row, 77].Value = "%_AYP_R_TwoOrMore";

        worksheet.Cells[row, 78].Value = "Part_AYP_R_EconDis";
        worksheet.Cells[row, 79].Value = "Met_AYP_R_EconDis";
        worksheet.Cells[row, 80].Value = "%_AYP_R_EconDis";

        worksheet.Cells[row, 81].Value = "Part_AYP_R_EL";
        worksheet.Cells[row, 82].Value = "Met_AYP_R_EL";
        worksheet.Cells[row, 83].Value = "%_AYP_R_EL";

        worksheet.Cells[row, 84].Value = "Part_AYP_M_All";
        worksheet.Cells[row, 85].Value = "Met_AYP_M_All";
        worksheet.Cells[row, 86].Value = "%_AYP_M_All";

        worksheet.Cells[row, 87].Value = "Part_AYP_M_Amer_Ind";
        worksheet.Cells[row, 88].Value = "Met_AYP_M_Amer_Ind";
        worksheet.Cells[row, 89].Value = "%_AYP_M_Amer_Ind";

        worksheet.Cells[row, 90].Value = "Part_AYP_M_Asian";
        worksheet.Cells[row, 91].Value = "Met_AYP_M_Asian";
        worksheet.Cells[row, 92].Value = "%_AYP_M_Asian";

        worksheet.Cells[row, 93].Value = "Part_AYP_M_Black";
        worksheet.Cells[row, 94].Value = "Met_AYP_M_Black";
        worksheet.Cells[row, 95].Value = "%_AYP_M_Black";

        worksheet.Cells[row, 96].Value = "Part_AYP_M_Hispanic";
        worksheet.Cells[row, 97].Value = "Met_AYP_M_Hispanic";
        worksheet.Cells[row, 98].Value = "%_AYP_M_Hispanic";

        worksheet.Cells[row, 99].Value = "Part_AYP_M_Native_Hawaii";
        worksheet.Cells[row, 100].Value = "Met_AYP_M_Native_Hawaii";
        worksheet.Cells[row, 101].Value = "%_AYP_M_Native_Hawaii";

        worksheet.Cells[row, 102].Value = "Part_AYP_M_White";
        worksheet.Cells[row, 103].Value = "Met_AYP_M_White";
        worksheet.Cells[row, 104].Value = "%_AYP_M_White";

        worksheet.Cells[row, 105].Value = "Part_AYP_M_TwoOrMore";
        worksheet.Cells[row, 106].Value = "Met_AYP_M_TwoOrMore";
        worksheet.Cells[row, 107].Value = "%_AYP_M_TwoOrMore";

        worksheet.Cells[row, 108].Value = "Part_AYP_M_EconDis";
        worksheet.Cells[row, 109].Value = "Met_AYP_M_EconDis";
        worksheet.Cells[row, 110].Value = "%_AYP_M_EconDis";

        worksheet.Cells[row, 111].Value = "Part_AYP_M_EL";
        worksheet.Cells[row, 112].Value = "Met_AYP_M_EL";
        worksheet.Cells[row, 113].Value = "%_AYP_M_EL";

        worksheet.Cells[row, 114].Value = "MSAP_Funds";
        worksheet.Cells[row, 115].Value = "Studs_Served";

        //gpra v data
        worksheet.Cells[row, 116].Value = "4yr_Amer_Indian";
        worksheet.Cells[row, 117].Value = "4yr_Asian";
        worksheet.Cells[row, 118].Value = "4yr_Black";
        worksheet.Cells[row, 119].Value = "4yr_Hispanic";
        worksheet.Cells[row, 120].Value = "4yr_Native_Hawaiian";
        worksheet.Cells[row, 121].Value = "4yr_White";
        worksheet.Cells[row, 122].Value = "4yr_Two_more";
        worksheet.Cells[row, 123].Value = "4yr_Cohort_Tot";
        worksheet.Cells[row, 124].Value = "Grad_Amer_Indian";
        worksheet.Cells[row, 125].Value = "Grad_Asian";
        worksheet.Cells[row, 126].Value = "Grad_Black";
        worksheet.Cells[row, 127].Value = "Grad_Hispanic";
        worksheet.Cells[row, 128].Value = "Grad_Native_Hawaiian";
        worksheet.Cells[row, 129].Value = "Grad_White";
        worksheet.Cells[row, 130].Value = "Grad_Two_more";
        worksheet.Cells[row, 131].Value = "Grad_Tot";
        worksheet.Cells[row, 132].Value = "%Grad_Amer_Indian";
        worksheet.Cells[row, 133].Value = "%Grad_Asian";
        worksheet.Cells[row, 134].Value = "%Grad_Black";
        worksheet.Cells[row, 135].Value = "%Grad_Hispanic";
        worksheet.Cells[row, 136].Value = "%Grad_Native_Hawaiian";
        worksheet.Cells[row, 137].Value = "%Grad_White";
        worksheet.Cells[row, 138].Value = "%Grad_Two_more";
        worksheet.Cells[row, 139].Value = "%Grad_Tot";
        worksheet.Cells[row, 140].Value = "4Yr_EconDis";
        worksheet.Cells[row, 141].Value = "Grad_EconDis";
        worksheet.Cells[row, 142].Value = "%Grad_EconDis";
        worksheet.Cells[row, 143].Value = "4yr_EL";
        worksheet.Cells[row, 144].Value = "Grad_EL";
        worksheet.Cells[row, 145].Value = "%Grad_EL";

        worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:EO1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x=>x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                    row++;

                    if (isAlternative)
                    {
                        using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                        {
                            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                        }

                    }
                    

                    MagnetGPRA item = gpraData[0];
                    worksheet.Cells[row, 1].Value = report.ID;

                    worksheet.Cells[row, 2].Value = report.GranteeID;
                    worksheet.Cells[row, 3].Value = school.SchoolCode;
                    worksheet.Cells[row, 4].Value = grantee.PRAward;
                    worksheet.Cells[row, 5].Value = school.SchoolName;

                    worksheet.Cells[row, 6].Value = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID).GranteeName;
                    string GradeString = "";
                    if (!string.IsNullOrEmpty(item.SchoolGrade))
                    {
                        foreach (string str in item.SchoolGrade.Split(';'))
                        {
                            switch (str)
                            {
                                case "1":
                                    GradeString += "Pre-K;";
                                    break;
                                case "2":
                                    GradeString += "K;"; break;
                                case "3":
                                    GradeString += "First;"; break;
                                case "4":
                                    GradeString += "Second;"; break;
                                case "5":
                                    GradeString += "Third;"; break;
                                case "6":
                                    GradeString += "Fourth;"; break;
                                case "7":
                                    GradeString += "Fifth;"; break;
                                case "8":
                                    GradeString += "Sixth;"; break;
                                case "9":
                                    GradeString += "Seventh;"; break;
                                case "10":
                                    GradeString += "Eighth;"; break;
                                case "11":
                                    GradeString += "Ninth;"; break;
                                case "12":
                                    GradeString += "Tenth;"; break;
                                case "13":
                                    GradeString += "Eleventh;"; break;
                                case "14":
                                    GradeString += "Twelfth;"; break;
                            }
                        }
                    }
                    worksheet.Cells[row, 7].Value = GradeString;
                    if (item.ProgramType != null)
                    {
                        worksheet.Cells[row, 8].Value = (bool)item.ProgramType ? "Partial" : "Whole-school";
                        worksheet.Cells[row, 9].Value = (bool)item.ProgramType ? 2 : 1;
                    }
                    else
                    {
                        worksheet.Cells[row, 8].Value = "Missing";
                        worksheet.Cells[row, 9].Value = 99;
                    }


                    //Title I
                    if (item.TitleISchoolFunding != null)
                    {
                        worksheet.Cells[row, 10].Value = (bool)item.TitleISchoolFunding ? "Yes" : "No";
                        worksheet.Cells[row, 11].Value = (bool)item.TitleISchoolFunding ? 1 : 0;

                        if (item.TitleISchoolFunding == true) //Title I = Yes
                        {
                            if (item.TitleISchoolFundingImprovement != null)
                            {
                                if ((item.TitleISchoolFundingImprovement == true))
                                {
                                    worksheet.Cells[row, 12].Value =  "Yes";
                                    worksheet.Cells[row, 13].Value = 1 ;
                                }
                                else //school imp = no
                                {
                                    worksheet.Cells[row, 12].Value = "No";
                                    worksheet.Cells[row, 13].Value = 0;

                                    worksheet.Cells[row, 14].Value = "N/A";
                                    //worksheet.Cells[row, 15].Value = 98;
                                }

                            }
                            else
                            {
                                worksheet.Cells[row, 12].Value = "Missing";
                                worksheet.Cells[row, 13].Value = 99;

                                worksheet.Cells[row, 14].Value = "Missing";
                                //worksheet.Cells[row, 15].Value = 99;
                            }

                        }
                        else //Title I = No
                        {
                            worksheet.Cells[row, 12].Value = "Not Applicable";
                            worksheet.Cells[row, 13].Value = 98;

                            worksheet.Cells[row, 14].Value = "N/A";
                            //worksheet.Cells[row, 15].Value = 98;
                        }

                    }
                    else
                    {
                        worksheet.Cells[row, 10].Value = "Missing";
                        worksheet.Cells[row, 11].Value = 99;

                        worksheet.Cells[row, 12].Value = "Missing";
                        worksheet.Cells[row, 13].Value = 99;

                        worksheet.Cells[row, 14].Value = "Missing";
                        //worksheet.Cells[row, 15].Value = 99;
                    }

                    if (item.TitleISchoolFundingImprovementStatus != null)
                    {
                        int key = (int)item.TitleISchoolFundingImprovementStatus;

                        worksheet.Cells[row, 14].Value = ImpvStatus[key];
                        worksheet.Cells[row, 15].Value = SIScode[key];
                    }
                    else
                    {
                        worksheet.Cells[row, 14].Value = "missing";
                        worksheet.Cells[row, 15].Value = "99";
                    }

                    //lowest achieving
                    if (item.PersistentlyLlowestAchievingSchool != null)
                    {
                        worksheet.Cells[row, 16].Value = (bool)item.PersistentlyLlowestAchievingSchool ? "Yes" : "No";
                        worksheet.Cells[row, 17].Value = (bool)item.PersistentlyLlowestAchievingSchool ? 1 : 0;
                        if (item.PersistentlyLlowestAchievingSchool == true)
                        {
                            if (item.SchoolImprovementGrant != null)
                            {
                                worksheet.Cells[row, 18].Value = (bool)item.SchoolImprovementGrant ? "Yes" : "No";
                                worksheet.Cells[row, 19].Value = (bool)item.SchoolImprovementGrant ? 1 : 0;
                            }
                            else
                            {
                                worksheet.Cells[row, 18].Value = "Missing";
                                worksheet.Cells[row, 19].Value = 99;
                            }
                        }
                        else
                        {
                            worksheet.Cells[row, 18].Value = "Not Applicable";
                            worksheet.Cells[row, 19].Value = 98;

                        }
                    }
                    else
                    {
                        worksheet.Cells[row, 16].Value = "Missing";
                        worksheet.Cells[row, 17].Value = 99;

                         worksheet.Cells[row, 18].Value = "Missing";
                         worksheet.Cells[row, 19].Value = 99;
                    }

                    ///end coding////

                    var applicantPoolData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 1);
                    if (applicantPoolData.Count > 0) //App_
                    {
                        worksheet.Cells[row, 20].Value = applicantPoolData[0].Indian;
                        gpralist[20] = Convert.ToDouble(gpralist[20]) + Convert.ToDouble(worksheet.Cells[row, 20].Value);
                        worksheet.Cells[row, 21].Value = applicantPoolData[0].Asian;
                        gpralist[21] = Convert.ToDouble(gpralist[21]) + Convert.ToDouble(worksheet.Cells[row, 21].Value);
                        worksheet.Cells[row, 22].Value = applicantPoolData[0].Black;
                        gpralist[22] = Convert.ToDouble(gpralist[22]) + Convert.ToDouble(worksheet.Cells[row, 22].Value);
                        worksheet.Cells[row, 23].Value = applicantPoolData[0].Hispanic;
                        gpralist[23] = Convert.ToDouble(gpralist[23]) + Convert.ToDouble(worksheet.Cells[row, 23].Value);
                        worksheet.Cells[row, 24].Value = applicantPoolData[0].Hawaiian;
                        gpralist[24] = Convert.ToDouble(gpralist[24]) + Convert.ToDouble(worksheet.Cells[row, 24].Value);
                        worksheet.Cells[row, 25].Value = applicantPoolData[0].White;
                        gpralist[25] = Convert.ToDouble(gpralist[25]) + Convert.ToDouble(worksheet.Cells[row, 25].Value);
                        worksheet.Cells[row, 26].Value = applicantPoolData[0].MultiRaces;
                        gpralist[26] = Convert.ToDouble(gpralist[26]) + Convert.ToDouble(worksheet.Cells[row, 26].Value);
                        worksheet.Cells[row, 27].Value = applicantPoolData[0].Undeclared;
                        gpralist[27] = Convert.ToDouble(gpralist[27]) + Convert.ToDouble(worksheet.Cells[row, 27].Value);
                        worksheet.Cells[row, 28].Value = applicantPoolData[0].Total;
                        gpralist[28] = Convert.ToDouble(gpralist[28]) + Convert.ToDouble(worksheet.Cells[row, 28].Value);
                    }
                    var enrollmentData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 2);
                    if (enrollmentData.Count > 0) //Enr_
                    {
                        worksheet.Cells[row, 29].Value = enrollmentData[0].Indian;
                        gpralist[29] = Convert.ToDouble(gpralist[29]) + Convert.ToDouble(worksheet.Cells[row, 29].Value);
                        worksheet.Cells[row, 30].Value = enrollmentData[0].Asian;
                        gpralist[30] = Convert.ToDouble(gpralist[30]) + Convert.ToDouble(worksheet.Cells[row, 30].Value);
                        worksheet.Cells[row, 31].Value = enrollmentData[0].Black;
                        gpralist[31] = Convert.ToDouble(gpralist[31]) + Convert.ToDouble(worksheet.Cells[row, 31].Value);
                        worksheet.Cells[row, 32].Value = enrollmentData[0].Hispanic;
                        gpralist[32] = Convert.ToDouble(gpralist[32]) + Convert.ToDouble(worksheet.Cells[row, 32].Value);
                        worksheet.Cells[row, 33].Value = enrollmentData[0].Hawaiian;
                        gpralist[33] = Convert.ToDouble(gpralist[33]) + Convert.ToDouble(worksheet.Cells[row, 33].Value);
                        worksheet.Cells[row, 34].Value = enrollmentData[0].White;
                        gpralist[34] = Convert.ToDouble(gpralist[34]) + Convert.ToDouble(worksheet.Cells[row, 34].Value);
                        worksheet.Cells[row, 35].Value = enrollmentData[0].MultiRaces;
                        gpralist[35] = Convert.ToDouble(gpralist[35]) + Convert.ToDouble(worksheet.Cells[row, 35].Value);
                        worksheet.Cells[row, 36].Value = enrollmentData[0].Total;
                        gpralist[36] = Convert.ToDouble(gpralist[36]) + Convert.ToDouble(worksheet.Cells[row, 36].Value);

                        if (enrollmentData[0].NewIndian != null)
                        {
                            worksheet.Cells[row, 37].Value = enrollmentData[0].NewIndian.ToString();
                            gpralist[37] = Convert.ToDouble(gpralist[37]) + Convert.ToDouble(worksheet.Cells[row, 37].Value);
                        }
                        else
                            worksheet.Cells[row, 37].Value = "missing";

                        if (enrollmentData[0].NewAsian != null)
                        {
                            worksheet.Cells[row, 38].Value = enrollmentData[0].NewAsian.ToString();
                            gpralist[38] = Convert.ToDouble(gpralist[38]) + Convert.ToDouble(worksheet.Cells[row, 38].Value);
                        }
                        else
                            worksheet.Cells[row, 38].Value = "missing";

                        if (enrollmentData[0].NewBlack != null)
                        {
                            worksheet.Cells[row, 39].Value = enrollmentData[0].NewBlack.ToString();
                            gpralist[39] = Convert.ToDouble(gpralist[39]) + Convert.ToDouble(worksheet.Cells[row, 39].Value);
                        }
                        else
                            worksheet.Cells[row, 39].Value = "missing";

                        if (enrollmentData[0].NewHispanic != null)
                        {
                            worksheet.Cells[row, 40].Value = enrollmentData[0].NewHispanic.ToString();
                            gpralist[40] = Convert.ToDouble(gpralist[40]) + Convert.ToDouble(worksheet.Cells[row, 40].Value);
                        }
                        else
                            worksheet.Cells[row, 40].Value = "missing";
                        
                        if(enrollmentData[0].NewHawaiian != null)
                        {
                        worksheet.Cells[row, 41].Value = enrollmentData[0].NewHawaiian.ToString();
                        gpralist[41] = Convert.ToDouble(gpralist[41]) + Convert.ToDouble(worksheet.Cells[row, 41].Value);
                        }
                        else
                             worksheet.Cells[row, 41].Value = "missing";
                        
                        if(enrollmentData[0].NewWhite != null)
                        {
                        worksheet.Cells[row, 42].Value = enrollmentData[0].NewWhite.ToString();
                        gpralist[42] = Convert.ToDouble(gpralist[42]) + Convert.ToDouble(worksheet.Cells[row, 42].Value);
                        }
                        else
                             worksheet.Cells[row, 42].Value = "missing";
                        
                        if(enrollmentData[0].NewMultiRaces != null)
                        {
                        worksheet.Cells[row, 43].Value = enrollmentData[0].NewMultiRaces.ToString();
                        gpralist[43] = Convert.ToDouble(gpralist[43]) + Convert.ToDouble(worksheet.Cells[row, 43].Value);
                        }
                        else
                             worksheet.Cells[row, 43].Value = "missing";

                        if(enrollmentData[0].ExtraFiled1 != null)
                        {
                        worksheet.Cells[row, 44].Value = enrollmentData[0].ExtraFiled1.ToString(); //new_student
                        gpralist[44] = Convert.ToDouble(gpralist[44]) + Convert.ToDouble(worksheet.Cells[row, 44].Value);
                        }
                        else
                             worksheet.Cells[row, 44].Value = "missing";

                        if(enrollmentData[0].ContIndian != null)
                        {
                        worksheet.Cells[row, 45].Value = enrollmentData[0].ContIndian.ToString();
                        gpralist[45] = Convert.ToDouble(gpralist[45]) + Convert.ToDouble(worksheet.Cells[row, 45].Value);
                        }
                        else
                             worksheet.Cells[row, 45].Value = "missing";
                        
                        if(enrollmentData[0].ContAsian != null)
                        {
                        worksheet.Cells[row, 46].Value = enrollmentData[0].ContAsian.ToString();
                        gpralist[46] = Convert.ToDouble(gpralist[46]) + Convert.ToDouble(worksheet.Cells[row, 46].Value);
                        }
                        else
                             worksheet.Cells[row, 46].Value = "missing";
                        
                        if(enrollmentData[0].ContBlack != null)
                        {
                        worksheet.Cells[row, 47].Value = enrollmentData[0].ContBlack.ToString();
                        gpralist[47] = Convert.ToDouble(gpralist[47]) + Convert.ToDouble(worksheet.Cells[row, 47].Value);
                        }
                        else
                             worksheet.Cells[row, 47].Value = "missing";
                        
                        if(enrollmentData[0].ContHispanic != null)
                        {
                        worksheet.Cells[row, 48].Value = enrollmentData[0].ContHispanic.ToString();
                        gpralist[48] = Convert.ToDouble(gpralist[48]) + Convert.ToDouble(worksheet.Cells[row, 48].Value);
                        }
                        else
                             worksheet.Cells[row, 48].Value = "missing";
                        
                        if(enrollmentData[0].ContHawaiian != null)
                        {
                        worksheet.Cells[row, 49].Value = enrollmentData[0].ContHawaiian.ToString();
                        gpralist[49] = Convert.ToDouble(gpralist[49]) + Convert.ToDouble(worksheet.Cells[row, 49].Value);
                        }
                        else
                             worksheet.Cells[row, 49].Value = "missing";
                        
                        if(enrollmentData[0].ContWhite != null)
                        {
                        worksheet.Cells[row, 50].Value = enrollmentData[0].ContWhite.ToString();
                        gpralist[50] = Convert.ToDouble(gpralist[50]) + Convert.ToDouble(worksheet.Cells[row, 50].Value);
                        }
                        else
                             worksheet.Cells[row, 50].Value = "missing";
                        
                        if(enrollmentData[0].ContMultiRaces != null)
                        {
                        worksheet.Cells[row, 51].Value = enrollmentData[0].ContMultiRaces.ToString();
                        gpralist[51] = Convert.ToDouble(gpralist[51]) + Convert.ToDouble(worksheet.Cells[row, 51].Value);
                        }
                        else
                             worksheet.Cells[row, 51].Value = "missing";

                        if(enrollmentData[0].ExtraFiled2 != null)
                        {
                        worksheet.Cells[row, 52].Value = enrollmentData[0].ExtraFiled2.ToString(); //Continuing Students
                        gpralist[52] = Convert.ToDouble(gpralist[52]) + Convert.ToDouble(worksheet.Cells[row, 52].Value);
                        }
                        else
                             worksheet.Cells[row, 52].Value = "missing";
                        
                        worksheet.Cells[row, 53].Value = enrollmentData[0].ExtraFiled3; //Total_sch_Enroll
                        gpralist[53] = Convert.ToDouble(gpralist[53]) + Convert.ToDouble(worksheet.Cells[row, 53].Value);
                    }
                    var participationReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 3);
                    if (participationReading.Count > 0) //Part_AYP_R_
                    {
                        if (participationReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 54].Value = participationReading[0].Total;
                            gpralist[54] = Convert.ToDouble(gpralist[54]) + Convert.ToDouble(worksheet.Cells[row, 54].Value);
                        }
                        if (participationReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 57].Value = participationReading[0].Indian;
                            gpralist[57] = Convert.ToDouble(gpralist[57]) + Convert.ToDouble(worksheet.Cells[row, 57].Value);
                        }
                        if (participationReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 60].Value = participationReading[0].Asian;
                            gpralist[60] = Convert.ToDouble(gpralist[60]) + Convert.ToDouble(worksheet.Cells[row, 60].Value);
                        }
                        if (participationReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 63].Value = participationReading[0].Black;
                            gpralist[63] = Convert.ToDouble(gpralist[63]) + Convert.ToDouble(worksheet.Cells[row, 63].Value);
                        }

                        if (participationReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 66].Value = participationReading[0].Hispanic;
                            gpralist[66] = Convert.ToDouble(gpralist[66]) + Convert.ToDouble(worksheet.Cells[row, 66].Value);
                        }
                        if (participationReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 69].Value = participationReading[0].Hawaiian;
                            gpralist[69] = Convert.ToDouble(gpralist[69]) + Convert.ToDouble(worksheet.Cells[row, 69].Value);
                        }
                        if (participationReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 72].Value = participationReading[0].White;
                            gpralist[72] = Convert.ToDouble(gpralist[72]) + Convert.ToDouble(worksheet.Cells[row, 72].Value);
                        }
                        if (participationReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 75].Value = participationReading[0].MultiRaces;
                            gpralist[75] = Convert.ToDouble(gpralist[75]) + Convert.ToDouble(worksheet.Cells[row, 75].Value);
                        }
                        if (participationReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 78].Value = participationReading[0].ExtraFiled2;//_econnDis
                            gpralist[78] = Convert.ToDouble(gpralist[78]) + Convert.ToDouble(worksheet.Cells[row, 78].Value);
                        }

                        if (participationReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 81].Value = participationReading[0].ExtraFiled1;   //Par_AYP_R_EL
                            gpralist[81] = Convert.ToDouble(gpralist[81]) + Convert.ToDouble(worksheet.Cells[row, 81].Value);
                        }
                    }
                    var achievementReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 5);
                    if (achievementReading.Count > 0) //met_AYP_R
                    {
                        if (achievementReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 55].Value = achievementReading[0].Total;
                            gpralist[55] = Convert.ToDouble(gpralist[55]) + Convert.ToDouble(worksheet.Cells[row, 55].Value);
                        }
                        if (achievementReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 58].Value = achievementReading[0].Indian;
                            gpralist[58] = Convert.ToDouble(gpralist[58]) + Convert.ToDouble(worksheet.Cells[row, 58].Value);
                        }
                        if (achievementReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 61].Value = achievementReading[0].Asian;
                            gpralist[61] = Convert.ToDouble(gpralist[61]) + Convert.ToDouble(worksheet.Cells[row, 61].Value);
                        }
                        if (achievementReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 64].Value = achievementReading[0].Black;
                            gpralist[64] = Convert.ToDouble(gpralist[64]) + Convert.ToDouble(worksheet.Cells[row, 64].Value);
                        }
                        if (achievementReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 67].Value = achievementReading[0].Hispanic;
                            gpralist[67] = Convert.ToDouble(gpralist[67]) + Convert.ToDouble(worksheet.Cells[row, 67].Value);
                        }
                        if (achievementReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 70].Value = achievementReading[0].Hawaiian;
                            gpralist[70] = Convert.ToDouble(gpralist[70]) + Convert.ToDouble(worksheet.Cells[row, 70].Value);
                        }
                        if (achievementReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 73].Value = achievementReading[0].White;
                            gpralist[73] = Convert.ToDouble(gpralist[73]) + Convert.ToDouble(worksheet.Cells[row, 73].Value);
                        }
                        if (achievementReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 76].Value = achievementReading[0].MultiRaces;
                            gpralist[76] = Convert.ToDouble(gpralist[76]) + Convert.ToDouble(worksheet.Cells[row, 76].Value);
                        }
                        if (achievementReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 79].Value = achievementReading[0].ExtraFiled2; //met_AYP_R_EconDis
                            gpralist[79] = Convert.ToDouble(gpralist[79]) + Convert.ToDouble(worksheet.Cells[row, 79].Value);
                        }
                        if (achievementReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 82].Value = achievementReading[0].ExtraFiled1; //met_AYP_R_EL
                            gpralist[82] = Convert.ToDouble(gpralist[82]) + Convert.ToDouble(worksheet.Cells[row, 82].Value);
                        }
                    }
                    if (participationReading.Count > 0 && achievementReading.Count > 0)
                    {
                        if (participationReading[0].Total > 0 && achievementReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 56].Value = Convert.ToDouble(achievementReading[0].Total) / participationReading[0].Total; 
                            worksheet.Cells[row, 56].Style.Numberformat.Format = "#0.00%";
                            gpralist[56] = Convert.ToDouble(gpralist[56]) + Convert.ToDouble(worksheet.Cells[row, 56].Value);
                        }

                        if (participationReading[0].Indian > 0 && achievementReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 59].Value = Convert.ToDouble(achievementReading[0].Indian) / participationReading[0].Indian; 
                            worksheet.Cells[row, 59].Style.Numberformat.Format = "#0.00%";
                            gpralist[59] = Convert.ToDouble(gpralist[59]) + Convert.ToDouble(worksheet.Cells[row, 59].Value);
                        }
                        if (participationReading[0].Asian > 0 && achievementReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 62].Value = Convert.ToDouble(achievementReading[0].Asian) / participationReading[0].Asian;
                            worksheet.Cells[row, 62].Style.Numberformat.Format = "#0.00%";
                            gpralist[62] = Convert.ToDouble(gpralist[62]) + Convert.ToDouble(worksheet.Cells[row, 62].Value);
                        }
                        if (participationReading[0].Black > 0 && achievementReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 65].Value = Convert.ToDouble(achievementReading[0].Black) / participationReading[0].Black; 
                            worksheet.Cells[row, 65].Style.Numberformat.Format = "#0.00%";
                            gpralist[65] = Convert.ToDouble(gpralist[65]) + Convert.ToDouble(worksheet.Cells[row, 65].Value);
                        }
                        if (participationReading[0].Hispanic > 0 && achievementReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 68].Value = Convert.ToDouble(achievementReading[0].Hispanic) / participationReading[0].Hispanic;
                            worksheet.Cells[row, 68].Style.Numberformat.Format = "#0.00%";
                            gpralist[68] = Convert.ToDouble(gpralist[68]) + Convert.ToDouble(worksheet.Cells[row, 68].Value);

                        }
                        if (participationReading[0].Hawaiian > 0 && achievementReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 71].Value = Convert.ToDouble(achievementReading[0].Hawaiian) / participationReading[0].Hawaiian; 
                            worksheet.Cells[row, 71].Style.Numberformat.Format = "#0.00%";
                            gpralist[71] = Convert.ToDouble(gpralist[71]) + Convert.ToDouble(worksheet.Cells[row, 71].Value);
                        }
                        if (participationReading[0].White > 0 && achievementReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 74].Value = Convert.ToDouble(achievementReading[0].White) / participationReading[0].White;
                            worksheet.Cells[row, 74].Style.Numberformat.Format = "#0.00%";
                            gpralist[74] = Convert.ToDouble(gpralist[74]) + Convert.ToDouble(worksheet.Cells[row, 74].Value);
                        }
                        if (participationReading[0].MultiRaces > 0 && achievementReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 77].Value = Convert.ToDouble(achievementReading[0].MultiRaces) / participationReading[0].MultiRaces;
                            worksheet.Cells[row, 77].Style.Numberformat.Format = "#0.00%";
                            gpralist[77] = Convert.ToDouble(gpralist[77]) + Convert.ToDouble(worksheet.Cells[row, 77].Value);
                        }
                        if (participationReading[0].ExtraFiled2 > 0 && achievementReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 80].Value = Convert.ToDouble(achievementReading[0].ExtraFiled2) / participationReading[0].ExtraFiled2; 
                            worksheet.Cells[row, 80].Style.Numberformat.Format = "#0.00%";
                            gpralist[80] = Convert.ToDouble(gpralist[80]) + Convert.ToDouble(worksheet.Cells[row, 80].Value);
                        }
                        if (participationReading[0].ExtraFiled1 > 0 && achievementReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 83].Value = Convert.ToDouble(achievementReading[0].ExtraFiled1) / participationReading[0].ExtraFiled1; 
                            worksheet.Cells[row, 83].Style.Numberformat.Format = "#0.00%";
                            gpralist[83] = Convert.ToDouble(gpralist[83]) + Convert.ToDouble(worksheet.Cells[row, 83].Value);
                        }
                    }

                    ////////////////////////////////////////////////////////////////////
                    var participationMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 4);
                    if (participationMath.Count > 0) //Part_AYP_M
                    {
                        if (participationMath[0].Total >= 0)
                        {
                            worksheet.Cells[row, 84].Value = participationMath[0].Total;
                            gpralist[84] = Convert.ToDouble(gpralist[84]) + Convert.ToDouble(worksheet.Cells[row, 84].Value);
                        }
                        if (participationMath[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 87].Value = participationMath[0].Indian;
                            gpralist[87] = Convert.ToDouble(gpralist[87]) + Convert.ToDouble(worksheet.Cells[row, 87].Value);
                        }
                        if (participationMath[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 90].Value = participationMath[0].Asian;
                            gpralist[90] = Convert.ToDouble(gpralist[90]) + Convert.ToDouble(worksheet.Cells[row, 90].Value);
                        }
                        if (participationMath[0].Black >= 0)
                        {
                            worksheet.Cells[row, 93].Value = participationMath[0].Black;
                            gpralist[93] = Convert.ToDouble(gpralist[93]) + Convert.ToDouble(worksheet.Cells[row, 93].Value);
                        }
                        if (participationMath[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 96].Value = participationMath[0].Hispanic;
                            gpralist[96] = Convert.ToDouble(gpralist[96]) + Convert.ToDouble(worksheet.Cells[row, 96].Value);
                        }
                        if (participationMath[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 99].Value = participationMath[0].Hawaiian;
                            gpralist[99] = Convert.ToDouble(gpralist[99]) + Convert.ToDouble(worksheet.Cells[row, 99].Value);
                        }
                        if (participationMath[0].White >= 0)
                        {
                            worksheet.Cells[row, 102].Value = participationMath[0].White;
                            gpralist[102] = Convert.ToDouble(gpralist[102]) + Convert.ToDouble(worksheet.Cells[row, 102].Value);
                        }
                        if (participationMath[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 105].Value = participationMath[0].MultiRaces;
                            gpralist[105] = Convert.ToDouble(gpralist[105]) + Convert.ToDouble(worksheet.Cells[row, 105].Value);
                        }
                        if (participationMath[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 108].Value = participationMath[0].ExtraFiled2;
                            gpralist[108] = Convert.ToDouble(gpralist[108]) + Convert.ToDouble(worksheet.Cells[row, 108].Value);
                        }
                        if (participationMath[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 111].Value = participationMath[0].ExtraFiled1;
                            gpralist[111] = Convert.ToDouble(gpralist[111]) + Convert.ToDouble(worksheet.Cells[row, 111].Value);
                        }
                    }

                    var achievementMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 6);
                    if (achievementMath.Count > 0) //Met_AYP_M_
                    {
                        if (achievementMath[0].Total >= 0)
                        {
                            worksheet.Cells[row, 85].Value = achievementMath[0].Total;
                            gpralist[85] = Convert.ToDouble(gpralist[85]) + Convert.ToDouble(worksheet.Cells[row, 85].Value);
                        }
                        if (achievementMath[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 88].Value = achievementMath[0].Indian;
                            gpralist[88] = Convert.ToDouble(gpralist[88]) + Convert.ToDouble(worksheet.Cells[row, 88].Value);
                        }
                        if (achievementMath[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 91].Value = achievementMath[0].Asian;
                            gpralist[91] = Convert.ToDouble(gpralist[91]) + Convert.ToDouble(worksheet.Cells[row, 91].Value);
                        }
                        if (achievementMath[0].Black >= 0)
                        {
                            worksheet.Cells[row, 94].Value = achievementMath[0].Black;
                            gpralist[94] = Convert.ToDouble(gpralist[94]) + Convert.ToDouble(worksheet.Cells[row, 94].Value);
                        }
                        if (achievementMath[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 97].Value = achievementMath[0].Hispanic;
                            gpralist[97] = Convert.ToDouble(gpralist[97]) + Convert.ToDouble(worksheet.Cells[row, 97].Value);
                        }
                        if (achievementMath[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 100].Value = achievementMath[0].Hawaiian;
                            gpralist[100] = Convert.ToDouble(gpralist[100]) + Convert.ToDouble(worksheet.Cells[row, 100].Value);
                        }
                        if (achievementMath[0].White >= 0)
                        {
                            worksheet.Cells[row, 103].Value = achievementMath[0].White;
                            gpralist[103] = Convert.ToDouble(gpralist[103]) + Convert.ToDouble(worksheet.Cells[row, 103].Value);
                        }
                        if (achievementMath[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 106].Value = achievementMath[0].MultiRaces;
                            gpralist[106] = Convert.ToDouble(gpralist[106]) + Convert.ToDouble(worksheet.Cells[row, 106].Value);
                        }
                        if (achievementMath[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 109].Value = achievementMath[0].ExtraFiled2;
                            gpralist[109] = Convert.ToDouble(gpralist[109]) + Convert.ToDouble(worksheet.Cells[row, 109].Value);
                        }
                        if (achievementMath[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 112].Value = achievementMath[0].ExtraFiled1;
                            gpralist[112] = Convert.ToDouble(gpralist[112]) + Convert.ToDouble(worksheet.Cells[row, 112].Value);
                        }
                    }
                    if (achievementMath.Count > 0 && participationMath.Count > 0)
                    {
                        if (achievementMath[0].Total >= 0 && participationMath[0].Total > 0)
                        {
                            worksheet.Cells[row, 86].Value = Convert.ToDouble(achievementMath[0].Total) / participationMath[0].Total;
                            worksheet.Cells[row, 86].Style.Numberformat.Format = "#0.00%";
                            gpralist[86] = Convert.ToDouble(gpralist[86]) + Convert.ToDouble(worksheet.Cells[row, 86].Value);

                        }
                        if (achievementMath[0].Indian >= 0 && participationMath[0].Indian > 0)
                        {
                            worksheet.Cells[row, 89].Value = Convert.ToDouble(achievementMath[0].Indian) / participationMath[0].Indian; 
                            worksheet.Cells[row, 89].Style.Numberformat.Format = "#0.00%";
                            gpralist[89] = Convert.ToDouble(gpralist[89]) + Convert.ToDouble(worksheet.Cells[row, 89].Value);
                        }
                        if (achievementMath[0].Asian >= 0 && participationMath[0].Asian > 0)
                        {
                            worksheet.Cells[row, 92].Value = Convert.ToDouble(achievementMath[0].Asian) / participationMath[0].Asian; 
                            worksheet.Cells[row, 92].Style.Numberformat.Format = "#0.00%";
                            gpralist[92] = Convert.ToDouble(gpralist[92]) + Convert.ToDouble(worksheet.Cells[row, 92].Value);
                        }
                        if (achievementMath[0].Black >= 0 && participationMath[0].Black > 0)
                        {
                            worksheet.Cells[row, 95].Value = Convert.ToDouble(achievementMath[0].Black) / participationMath[0].Black; 
                            worksheet.Cells[row, 95].Style.Numberformat.Format = "#0.00%";
                            gpralist[95] = Convert.ToDouble(gpralist[95]) + Convert.ToDouble(worksheet.Cells[row, 95].Value);
                        }

                        if (achievementMath[0].Hispanic >= 0 && participationMath[0].Hispanic > 0)
                        {
                            worksheet.Cells[row, 98].Value = Convert.ToDouble(achievementMath[0].Hispanic) / participationMath[0].Hispanic; 
                            worksheet.Cells[row, 98].Style.Numberformat.Format = "#0.00%";
                            gpralist[98] = Convert.ToDouble(gpralist[98]) + Convert.ToDouble(worksheet.Cells[row, 98].Value);
                        }
                        if (achievementMath[0].Hawaiian >= 0 && participationMath[0].Hawaiian > 0)
                        {
                            worksheet.Cells[row, 101].Value = Convert.ToDouble(achievementMath[0].Hawaiian) / participationMath[0].Hawaiian;
                            worksheet.Cells[row, 101].Style.Numberformat.Format = "#0.00%";
                            gpralist[101] = Convert.ToDouble(gpralist[101]) + Convert.ToDouble(worksheet.Cells[row, 101].Value);
                        }
                        if (achievementMath[0].White >= 0 && participationMath[0].White > 0)
                        {
                            worksheet.Cells[row, 104].Value = Convert.ToDouble(achievementMath[0].White) / participationMath[0].White;
                            worksheet.Cells[row, 104].Style.Numberformat.Format = "#0.00%";
                            gpralist[104] = Convert.ToDouble(gpralist[104]) + Convert.ToDouble(worksheet.Cells[row, 104].Value);
                        }
                        if (achievementMath[0].MultiRaces >= 0 && participationMath[0].MultiRaces > 0)
                        {
                            worksheet.Cells[row, 107].Value = Convert.ToDouble(achievementMath[0].MultiRaces) / participationMath[0].MultiRaces;
                            worksheet.Cells[row, 107].Style.Numberformat.Format = "#0.00%";
                            gpralist[107] = Convert.ToDouble(gpralist[107]) + Convert.ToDouble(worksheet.Cells[row, 107].Value);
                        }

                        if (achievementMath[0].ExtraFiled2 >= 0 && participationMath[0].ExtraFiled2 > 0)
                        {
                            worksheet.Cells[row, 110].Value = Convert.ToDouble(achievementMath[0].ExtraFiled2) / participationMath[0].ExtraFiled2;
                            worksheet.Cells[row, 110].Style.Numberformat.Format = "#0.00%";
                            gpralist[110] = Convert.ToDouble(gpralist[110]) + Convert.ToDouble(worksheet.Cells[row, 110].Value);
                        }
                        if (achievementMath[0].ExtraFiled1 >= 0 && participationMath[0].ExtraFiled1 > 0)
                        {
                            worksheet.Cells[row, 113].Value = Convert.ToDouble(achievementMath[0].ExtraFiled1) / participationMath[0].ExtraFiled1;
                            worksheet.Cells[row, 113].Style.Numberformat.Format = "#0.00%";
                            gpralist[113] = Convert.ToDouble(gpralist[113]) + Convert.ToDouble(worksheet.Cells[row, 113].Value);
                        }
                    }

                    var performanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 7);
                    if (performanceMeasure.Count > 0)
                    {
                        if (performanceMeasure[0].Budget != null)
                        {
                            TotalSchools++;
                            worksheet.Cells[row, 114].Value = Convert.ToDouble(performanceMeasure[0].Budget);
                            worksheet.Cells[row, 114].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";
                        }
                        worksheet.Cells[row, 115].Value = performanceMeasure[0].Total; //Studs_Served
                        gpralist[114] = Convert.ToDouble(gpralist[114]) + Convert.ToDouble(worksheet.Cells[row, 114].Value);
                        gpralist[115] = Convert.ToDouble(gpralist[115]) + Convert.ToDouble(worksheet.Cells[row, 115].Value);

                    }

                    var gprav = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 8);
                    if (gprav.Count > 0)
                    {
                        if (gprav[0].AmericanIndiancohort != null)
                        {
                            worksheet.Cells[row, 116].Value = gprav[0].AmericanIndiancohort.ToString();
                            gpralist[116] = Convert.ToDouble(gpralist[116]) + Convert.ToDouble(worksheet.Cells[row, 116].Value);
                        }
                        else
                            worksheet.Cells[row, 116].Value = "missing";

                        if (gprav[0].Asiancohort != null)
                        {
                            worksheet.Cells[row, 117].Value = gprav[0].Asiancohort.ToString();
                            gpralist[117] = Convert.ToDouble(gpralist[117]) + Convert.ToDouble(worksheet.Cells[row, 117].Value);
                        }
                        else
                            worksheet.Cells[row, 117].Value = "missing";

                        if (gprav[0].AfricanAmericancohort != null)
                        {
                            worksheet.Cells[row, 118].Value = gprav[0].AfricanAmericancohort.ToString();
                            gpralist[118] = Convert.ToDouble(gpralist[118]) + Convert.ToDouble(worksheet.Cells[row, 118].Value);
                        }
                        else
                            worksheet.Cells[row, 118].Value = "missing";

                        if (gprav[0].HispanicLatinocohort != null)
                        {
                            worksheet.Cells[row, 119].Value = gprav[0].HispanicLatinocohort.ToString();
                            gpralist[119] = Convert.ToDouble(gpralist[119]) + Convert.ToDouble(worksheet.Cells[row, 119].Value);
                        }
                        else
                            worksheet.Cells[row, 119].Value = "missing";

                        if (gprav[0].NativeHawaiiancohort != null)
                        {
                            worksheet.Cells[row, 120].Value = gprav[0].NativeHawaiiancohort.ToString();
                            gpralist[120] = Convert.ToDouble(gpralist[120]) + Convert.ToDouble(worksheet.Cells[row, 120].Value);
                        }
                        else
                            worksheet.Cells[row, 120].Value = "missing";

                        if (gprav[0].Whitecohort != null)
                        {
                            worksheet.Cells[row, 121].Value = gprav[0].Whitecohort.ToString();
                            gpralist[121] = Convert.ToDouble(gpralist[121]) + Convert.ToDouble(worksheet.Cells[row, 121].Value);
                        }
                        else
                            worksheet.Cells[row, 121].Value = "missing";


                        if (gprav[0].MoreRacescohort != null)
                        {
                            worksheet.Cells[row, 122].Value = gprav[0].MoreRacescohort.ToString();
                            gpralist[122] = Convert.ToDouble(gpralist[122]) + Convert.ToDouble(worksheet.Cells[row, 122].Value);

                        }
                        else
                            worksheet.Cells[row, 122].Value = "missing";

                        if (gprav[0].Allcohort != null)
                        {
                            worksheet.Cells[row, 123].Value = gprav[0].Allcohort.ToString();
                            gpralist[123] = Convert.ToDouble(gpralist[123]) + Convert.ToDouble(worksheet.Cells[row, 123].Value);
                        }
                        else
                            worksheet.Cells[row, 123].Value = "missing";

                        if (gprav[0].AmericanIndiangraduated != null)
                        {
                            worksheet.Cells[row, 124].Value = gprav[0].AmericanIndiangraduated.ToString();
                            gpralist[124] = Convert.ToDouble(gpralist[124]) + Convert.ToDouble(worksheet.Cells[row, 124].Value);
                        }
                        else
                            worksheet.Cells[row, 124].Value = "missing";

                        if (gprav[0].Asiangraduated != null)
                        {
                            worksheet.Cells[row, 125].Value = gprav[0].Asiangraduated.ToString();
                            gpralist[125] = Convert.ToDouble(gpralist[125]) + Convert.ToDouble(worksheet.Cells[row, 125].Value);
                        }
                        else
                            worksheet.Cells[row, 125].Value = "missing";

                        if (gprav[0].AfricanAmericangraduated != null)
                        {
                            worksheet.Cells[row, 126].Value = gprav[0].AfricanAmericangraduated.ToString();
                            gpralist[126] = Convert.ToDouble(gpralist[126]) + Convert.ToDouble(worksheet.Cells[row, 126].Value);
                        }
                        else
                            worksheet.Cells[row, 126].Value = "missing";

                        if (gprav[0].HispanicLatinograduated != null)
                        {
                            worksheet.Cells[row, 127].Value = gprav[0].HispanicLatinograduated.ToString();
                            gpralist[127] = Convert.ToDouble(gpralist[127]) + Convert.ToDouble(worksheet.Cells[row, 127].Value);
                        }
                        else
                            worksheet.Cells[row, 127].Value = "missing";

                        if (gprav[0].NativeHawaiiangraduated != null)
                        {
                            worksheet.Cells[row, 128].Value = gprav[0].NativeHawaiiangraduated.ToString();
                            gpralist[128] = Convert.ToDouble(gpralist[128]) + Convert.ToDouble(worksheet.Cells[row, 128].Value);
                        }
                        else
                            worksheet.Cells[row, 128].Value = "missing";

                        if (gprav[0].Whitegraduated != null)
                        {
                            worksheet.Cells[row, 129].Value = gprav[0].Whitegraduated.ToString();
                            gpralist[129] = Convert.ToDouble(gpralist[129]) + Convert.ToDouble(worksheet.Cells[row, 129].Value);
                        }
                        else
                            worksheet.Cells[row, 129].Value = "missing";

                        if (gprav[0].MoreRacesgraduated != null)
                        {
                            worksheet.Cells[row, 130].Value = gprav[0].MoreRacesgraduated.ToString();
                            gpralist[130] = Convert.ToDouble(gpralist[130]) + Convert.ToDouble(worksheet.Cells[row, 130].Value);
                        }
                        else
                            worksheet.Cells[row, 130].Value = "missing";


                        if (gprav[0].Allgraduated != null)
                        {
                            worksheet.Cells[row, 131].Value = gprav[0].Allgraduated.ToString();
                            gpralist[131] = Convert.ToDouble(gpralist[131]) + Convert.ToDouble(worksheet.Cells[row, 131].Value);
                        }
                        else
                             worksheet.Cells[row, 131].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].AmericanIndianpercentage))
                        {
                            worksheet.Cells[row, 132].Value = (Convert.ToDouble(gprav[0].AmericanIndianpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[132] = Convert.ToDouble(gpralist[132]) + Convert.ToDouble(worksheet.Cells[row, 132].Value);
                        }
                        else
                            worksheet.Cells[row, 132].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Asianpercentage))
                        {
                            worksheet.Cells[row, 133].Value = (Convert.ToDouble(gprav[0].Asianpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[133] = Convert.ToDouble(gpralist[133]) + Convert.ToDouble(worksheet.Cells[row, 133].Value);
                        }
                        else
                            worksheet.Cells[row, 133].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].AfricanAmericanpercentage))
                        {
                            worksheet.Cells[row, 134].Value = (Convert.ToDouble(gprav[0].AfricanAmericanpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[134] = Convert.ToDouble(gpralist[134]) + Convert.ToDouble(worksheet.Cells[row, 134].Value);
                        }
                        else
                            worksheet.Cells[row, 134].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].HispanicLatinopercentage))
                        {
                            worksheet.Cells[row, 135].Value = (Convert.ToDouble(gprav[0].HispanicLatinopercentage.Replace("%", "")) / 100).ToString();
                            gpralist[135] = Convert.ToDouble(gpralist[135]) + Convert.ToDouble(worksheet.Cells[row, 135].Value);
                        }
                        else
                            worksheet.Cells[row, 135].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].NativeHawaiianpercentage))
                        {
                            worksheet.Cells[row, 136].Value = (Convert.ToDouble(gprav[0].NativeHawaiianpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[136] = Convert.ToDouble(gpralist[136]) + Convert.ToDouble(worksheet.Cells[row, 136].Value);
                        }
                        else
                            worksheet.Cells[row, 136].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Whitepercentage))
                        {
                            worksheet.Cells[row, 137].Value = (Convert.ToDouble(gprav[0].Whitepercentage.Replace("%", "")) / 100).ToString();
                            gpralist[137] = Convert.ToDouble(gpralist[137]) + Convert.ToDouble(worksheet.Cells[row, 137].Value);
                        }
                        else
                            worksheet.Cells[row, 137].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].MoreRacespercentage))
                        {
                            worksheet.Cells[row, 138].Value = (Convert.ToDouble(gprav[0].MoreRacespercentage.Replace("%", "")) / 100).ToString();
                            gpralist[138] = Convert.ToDouble(gpralist[138]) + Convert.ToDouble(worksheet.Cells[row, 138].Value);
                        }
                        else
                            worksheet.Cells[row, 138].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].allpercentage))
                        {
                            worksheet.Cells[row, 139].Value = (Convert.ToDouble(gprav[0].allpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[139] = Convert.ToDouble(gpralist[139]) + Convert.ToDouble(worksheet.Cells[row, 139].Value);
                        }
                        else
                            worksheet.Cells[row, 139].Value = "missing";

                        if (gprav[0].Economicallycohort != null)
                        {
                            worksheet.Cells[row, 140].Value = gprav[0].Economicallycohort.ToString();
                            gpralist[140] = Convert.ToDouble(gpralist[140]) + Convert.ToDouble(worksheet.Cells[row, 140].Value);
                        }
                        else
                            worksheet.Cells[row, 140].Value = "missing";

                        if (gprav[0].Economicallygraduated != null)
                        {
                            worksheet.Cells[row, 141].Value = gprav[0].Economicallygraduated.ToString();
                            gpralist[141] = Convert.ToDouble(gpralist[141]) + Convert.ToDouble(worksheet.Cells[row, 141].Value);
                        }
                        else
                            worksheet.Cells[row, 141].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Economicallypercentage))
                        {
                            worksheet.Cells[row, 142].Value = (Convert.ToDouble(gprav[0].Economicallypercentage.Replace("%", "")) / 100).ToString();
                            gpralist[142] = Convert.ToDouble(gpralist[142]) + Convert.ToDouble(worksheet.Cells[row, 142].Value);
                        }
                        else
                            worksheet.Cells[row, 142].Value = "missing";

                        if (gprav[0].Englishlearnerscohort != null)
                        {
                            worksheet.Cells[row, 143].Value = gprav[0].Englishlearnerscohort.ToString();
                            gpralist[143] = Convert.ToDouble(gpralist[143]) + Convert.ToDouble(worksheet.Cells[row, 143].Value);
                        }
                        else
                            worksheet.Cells[row, 143].Value = "missing";

                        if (gprav[0].Englishlearnersgraduated != null)
                        {
                            worksheet.Cells[row, 144].Value = gprav[0].Englishlearnersgraduated.ToString();
                            gpralist[144] = Convert.ToDouble(gpralist[144]) + Convert.ToDouble(worksheet.Cells[row, 144].Value);
                        }
                        else
                            worksheet.Cells[row, 144].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Englishlearnerspercentage))
                        {
                            worksheet.Cells[row, 145].Value = (Convert.ToDouble(gprav[0].Englishlearnerspercentage.Replace("%", "")) / 100).ToString();
                            gpralist[145] = Convert.ToDouble(gpralist[145]) + Convert.ToDouble(worksheet.Cells[row, 145].Value);
                        }
                        else
                            worksheet.Cells[row, 145].Value = "missing";
                    }
                } //end for gpraData
            } //end for school
        } //end foreach report
        //add total acount and % rows

        row++;
        worksheet.Cells[row, 19].Value = "Total:";
        using (ExcelRange rng = worksheet.Cells["T" + row + ":CT" + row])
        {
            rng.Style.Numberformat.Format = "#,##0";
        }
        using (ExcelRange rng = worksheet.Cells["CW" + row + ":CW" + row])
        {
            rng.Style.Numberformat.Format = "#,##0";
        }

        worksheet.Cells[row, 20].Value = gpralist[20];
        worksheet.Cells[row + 3, 20].Value = Convert.ToDouble(gpralist[20]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 20].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 21].Value = gpralist[21];
        worksheet.Cells[row + 3, 21].Value = Convert.ToDouble(gpralist[21]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 21].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 22].Value = gpralist[22];
        worksheet.Cells[row + 3, 22].Value = Convert.ToDouble(gpralist[22]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 22].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 23].Value = gpralist[23];
        worksheet.Cells[row + 3, 23].Value = Convert.ToDouble(gpralist[23]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 23].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 24].Value = gpralist[24];
        worksheet.Cells[row + 3, 24].Value = Convert.ToDouble(gpralist[24]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 24].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 25].Value = gpralist[25];
        worksheet.Cells[row + 3, 25].Value = Convert.ToDouble(gpralist[25]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 25].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 26].Value = gpralist[26];
        worksheet.Cells[row + 3, 26].Value = Convert.ToDouble(gpralist[26]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 26].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 27].Value = gpralist[27];
        worksheet.Cells[row + 3, 27].Value = Convert.ToDouble(gpralist[27]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 27].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 28].Value = gpralist[28];
        worksheet.Cells[row + 3, 28].Value = Convert.ToDouble(gpralist[28]) / Convert.ToDouble(gpralist[28]); 
        worksheet.Cells[row + 3, 28].Style.Numberformat.Format = "#0.00%";
        worksheet.Cells[row, 29].Value = gpralist[29];
        worksheet.Cells[row, 30].Value = gpralist[30];

        worksheet.Cells[row, 31].Value = gpralist[31];
        worksheet.Cells[row, 32].Value = gpralist[32];
        worksheet.Cells[row, 33].Value = gpralist[33];
        worksheet.Cells[row, 34].Value = gpralist[34];
        worksheet.Cells[row, 35].Value = gpralist[35];
        worksheet.Cells[row, 36].Value = gpralist[36];

        worksheet.Cells[row, 37].Value = gpralist[37];
        worksheet.Cells[row, 38].Value = gpralist[38];
         worksheet.Cells[row, 39].Value = gpralist[39];
        worksheet.Cells[row, 40].Value = gpralist[40];
         worksheet.Cells[row, 41].Value = gpralist[41];
        worksheet.Cells[row, 42].Value = gpralist[42];
         worksheet.Cells[row, 43].Value = gpralist[43];

        worksheet.Cells[row, 44].Value = gpralist[44]; //new_students total h26

         worksheet.Cells[row, 45].Value = gpralist[45];
        worksheet.Cells[row, 46].Value = gpralist[46];
         worksheet.Cells[row, 47].Value = gpralist[47];
        worksheet.Cells[row, 48].Value = gpralist[58];
         worksheet.Cells[row, 49].Value = gpralist[49];
        worksheet.Cells[row, 50].Value = gpralist[50];
         worksheet.Cells[row, 51].Value = gpralist[51];

        worksheet.Cells[row, 52].Value = gpralist[52]; //continuing_students total h27

        worksheet.Cells[row, 53].Value = gpralist[53];
        worksheet.Cells[row, 53].Value = gpralist[53];
        worksheet.Cells[row, 55].Value = gpralist[55];
        //worksheet.Cells[row, 42].Value = Convert.ToDouble(gpralist[42]) / (row - 1);
        //worksheet.Cells[row, 42].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 57].Value = gpralist[57];
        worksheet.Cells[row, 58].Value = gpralist[58];
        //worksheet.Cells[row, 45].Value = Convert.ToDouble(gpralist[45]) / (row - 1);
        //worksheet.Cells[row, 45].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 60].Value = gpralist[60];
        worksheet.Cells[row, 61].Value = gpralist[61];
        //worksheet.Cells[row, 48].Value = Convert.ToDouble(gpralist[48]) / (row - 1); 
        //worksheet.Cells[row, 48].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 63].Value = gpralist[63];
        worksheet.Cells[row, 64].Value = gpralist[64];
        //worksheet.Cells[row, 51].Value = Convert.ToDouble(gpralist[51]) / (row - 1); 

        worksheet.Cells[row, 66].Value = gpralist[66];
        worksheet.Cells[row, 67].Value = gpralist[67];
        //worksheet.Cells[row, 54].Value = Convert.ToDouble(gpralist[54]) / (row - 1); 
        //worksheet.Cells[row, 54].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 69].Value = gpralist[69];
        worksheet.Cells[row, 70].Value = gpralist[70];
        //worksheet.Cells[row, 57].Value = Convert.ToDouble(gpralist[57]) / (row - 1); 
        //worksheet.Cells[row, 57].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 72].Value = gpralist[72];
        worksheet.Cells[row, 73].Value = gpralist[73];
        //worksheet.Cells[row, 60].Value = Convert.ToDouble(gpralist[60]) / (row - 1); 
        //worksheet.Cells[row, 60].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 75].Value = gpralist[75];
        worksheet.Cells[row, 76].Value = gpralist[76];
        //worksheet.Cells[row, 63].Value = Convert.ToDouble(gpralist[63]) / (row - 1);
        //worksheet.Cells[row, 63].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 78].Value = gpralist[78];
        worksheet.Cells[row, 79].Value = gpralist[79];
        //worksheet.Cells[row, 66].Value = Convert.ToDouble(gpralist[66]) / (row - 1); 
        //worksheet.Cells[row, 66].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 81].Value = gpralist[81];
        worksheet.Cells[row, 82].Value = gpralist[82];
        //worksheet.Cells[row, 69].Value = Convert.ToDouble(gpralist[69]) / (row - 1); 
        //worksheet.Cells[row, 69].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 84].Value = gpralist[84];
        worksheet.Cells[row, 85].Value = gpralist[85];
        //worksheet.Cells[row, 72].Value = Convert.ToDouble(gpralist[72]) / (row - 1); 
        //worksheet.Cells[row, 72].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 87].Value = gpralist[87];
        worksheet.Cells[row, 88].Value = gpralist[88];
        //worksheet.Cells[row, 75].Value = Convert.ToDouble(gpralist[75]) / (row - 1); 
        //worksheet.Cells[row, 75].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 90].Value = gpralist[90];
        worksheet.Cells[row, 91].Value = gpralist[91];
        //worksheet.Cells[row, 78].Value = Convert.ToDouble(gpralist[78]) / (row - 1); 
        //worksheet.Cells[row, 78].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 93].Value = gpralist[93];
        worksheet.Cells[row, 94].Value = gpralist[94];
        //worksheet.Cells[row, 81].Value = Convert.ToDouble(gpralist[81]) / (row - 1); 
        //worksheet.Cells[row, 81].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 96].Value = gpralist[96];
        worksheet.Cells[row, 97].Value = gpralist[97];
        //worksheet.Cells[row, 84].Value = Convert.ToDouble(gpralist[84]) / (row - 1); 
        //worksheet.Cells[row, 84].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 99].Value = gpralist[99];
        worksheet.Cells[row, 100].Value = gpralist[100];
        //worksheet.Cells[row, 87].Value = Convert.ToDouble(gpralist[87]) / (row - 1); 
        //worksheet.Cells[row, 87].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 102].Value = gpralist[102];
        worksheet.Cells[row, 103].Value = gpralist[103];
        //worksheet.Cells[row, 90].Value = Convert.ToDouble(gpralist[90]) / (row - 1); 
        //worksheet.Cells[row, 90].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 105].Value = gpralist[105];
        worksheet.Cells[row, 106].Value = gpralist[106];
        //worksheet.Cells[row, 93].Value = Convert.ToDouble(gpralist[93]) / (row - 1); 
        //worksheet.Cells[row, 93].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 108].Value = gpralist[108];
        worksheet.Cells[row, 109].Value = gpralist[109];
        //worksheet.Cells[row, 96].Value = Convert.ToDouble(gpralist[96]) / (row - 1); 
        //worksheet.Cells[row, 96].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 111].Value = gpralist[111];
        worksheet.Cells[row, 112].Value = gpralist[112];
        //worksheet.Cells[row, 99].Value = Convert.ToDouble(gpralist[99]) / (row - 1); 
        //worksheet.Cells[row, 99].Style.Numberformat.Format = "#0.00%";

        worksheet.Cells[row, 114].Value = Convert.ToDouble(gpralist[114]); 
        worksheet.Cells[row, 114].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

        worksheet.Cells[row + 1, 114].Value = Convert.ToDouble(gpralist[114])/TotalSchools;
        worksheet.Cells[row + 1, 114].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";
        worksheet.Cells[row + 1, 114].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        worksheet.Cells[row + 1, 114].Style.Font.Bold = true;

        worksheet.Cells[row, 115].Value = gpralist[115];
        worksheet.Cells[row + 1, 115].Value = Convert.ToDouble(gpralist[115]) / TotalSchools;
        worksheet.Cells[row + 1, 115].Style.Numberformat.Format = "#,##0.0";
        worksheet.Cells[row + 1, 115].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        worksheet.Cells[row + 1, 115].Style.Font.Bold = true;

        worksheet.Cells[row + 2, 114].Value = "Average annual MSAP funds expended at a school";
        worksheet.Cells[row + 2, 115].Value = "Average number of students served by a school's magnet program";

        //gpra v
         worksheet.Cells[row, 116].Value = gpralist[116];
        worksheet.Cells[row, 117].Value = gpralist[117];
         worksheet.Cells[row, 118].Value = gpralist[118];
        worksheet.Cells[row, 119].Value = gpralist[119];
        worksheet.Cells[row, 120].Value = gpralist[120];
         worksheet.Cells[row, 121].Value = gpralist[121];
        worksheet.Cells[row, 122].Value = gpralist[122];
         worksheet.Cells[row, 123].Value = gpralist[123];
        worksheet.Cells[row, 124].Value = gpralist[124];
        worksheet.Cells[row, 125].Value = gpralist[125];
         worksheet.Cells[row, 126].Value = gpralist[126];
        worksheet.Cells[row, 127].Value = gpralist[127];
         worksheet.Cells[row, 128].Value = gpralist[128];
        worksheet.Cells[row, 129].Value = gpralist[129];
        worksheet.Cells[row, 130].Value = gpralist[130];
         worksheet.Cells[row, 131].Value = gpralist[131];
        worksheet.Cells[row, 132].Value = gpralist[132];
        worksheet.Cells[row, 132].Style.Numberformat.Format = "#0.00%";
         worksheet.Cells[row, 133].Value = gpralist[133];
         worksheet.Cells[row, 133].Style.Numberformat.Format = "#0.00%";
        worksheet.Cells[row, 134].Value = gpralist[134];
        worksheet.Cells[row, 134].Style.Numberformat.Format = "#0.00%";
        worksheet.Cells[row, 135].Value = gpralist[135];
        worksheet.Cells[row, 135].Style.Numberformat.Format = "#0.00%";
         worksheet.Cells[row, 136].Value = gpralist[136];
         worksheet.Cells[row, 136].Style.Numberformat.Format = "#0.00%";
        worksheet.Cells[row, 137].Value = gpralist[137];
        worksheet.Cells[row, 137].Style.Numberformat.Format = "#0.00%";
         worksheet.Cells[row, 138].Value = gpralist[138];
         worksheet.Cells[row, 138].Style.Numberformat.Format = "#0.00%";
        worksheet.Cells[row, 139].Value = gpralist[139];
        worksheet.Cells[row, 139].Style.Numberformat.Format = "#0.00%";
        worksheet.Cells[row, 140].Value = gpralist[140];
         worksheet.Cells[row, 141].Value = gpralist[141];
        worksheet.Cells[row, 142].Value = gpralist[142];
        worksheet.Cells[row, 142].Style.Numberformat.Format = "#0.00%";
         worksheet.Cells[row, 143].Value = gpralist[143];
        worksheet.Cells[row, 144].Value = gpralist[144];
        worksheet.Cells[row, 145].Value = gpralist[145];
        worksheet.Cells[row, 145].Style.Numberformat.Format = "#0.00%";


        using (ExcelRange rng = worksheet.Cells["DJ" + (row+3)+ ":DK" + (row + 3)])
        {
            rng.Merge = true;
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.Value = Convert.ToDouble(worksheet.Cells[row + 1, 114].Value) / Convert.ToDouble(worksheet.Cells[row + 1, 115].Value);
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";


        }

        using (ExcelRange rng = worksheet.Cells["S" + (row + 1) + ":DK" + (row + 1)])
        {
            rng.Style.Border.Top.Style = ExcelBorderStyle.Thick;
        }

        worksheet.Column(100).Width = 20;

        worksheet.Column(20).Width = 25;
        worksheet.Column(21).Width = 25;
        worksheet.Column(22).Width = 25;
        worksheet.Column(23).Width = 25;
        worksheet.Column(24).Width = 25;
        worksheet.Column(25).Width = 25;
        worksheet.Column(26).Width = 25;
        worksheet.Column(27).Width = 25;
        worksheet.Column(28).Width = 25;
        worksheet.Column(39).Width = 30;
        worksheet.Column(39).Style.WrapText = true;


        using (ExcelRange rng = worksheet.Cells["T" + row + ":AB" + (row+4)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
        }

        using (ExcelRange rng = worksheet.Cells["T" + row + ":AB" + (row)+ ",T" + (row+3) + ":AB" + (row+3)])
        {
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.Font.Bold = true;
        }
        using (ExcelRange rng = worksheet.Cells["T" + (row + 6) + ":AB" + (row + 6)])
        {
            rng.Merge = true;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.IsRichText = true;
            ExcelRichText ert0 = rng.RichText.Add("GPRA Measure 1:");
            ert0.Bold = true;
            ert0 = rng.RichText.Add("  The percentage of magnet schools whose student applicant pool reduces, prevents, or eliminates minority group isolation.");
            ert0.Bold = false;

            rng.Style.Border.Top.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            rng.Style.Font.Size = 10;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
           
        }

        worksheet.Cells[(row + 1), 20].Value = "Total number of American Indian or Alaska Native applicants";
        worksheet.Cells[(row + 4), 20].Value = "Percentage of American Indian or Alaska Native applicants";

        worksheet.Cells[(row + 1), 21].Value = "Total number of Asian Applicants";
        worksheet.Cells[(row + 4), 21].Value = "Percentage of Asian Applicants";

        worksheet.Cells[(row + 1), 22].Value = "Total number of Black or African-American applicants";
        worksheet.Cells[(row + 4), 22].Value = "Percentage of Black or African-American applicants";

        worksheet.Cells[(row + 1), 23].Value = "Total number of Hispanic or Latino applicants";
        worksheet.Cells[(row + 4), 23].Value = "Percentage of Hispanic or Latino applicants";

        worksheet.Cells[(row + 1), 24].Value = "Total number of Native Hawaiian or Other Pacific Islander applicants";
        worksheet.Cells[(row + 4), 24].Value = "Percentage of Native Hawaiian or Other Pacific Islander applicants";

        worksheet.Cells[(row + 1), 25].Value = "Total number of White applicants";
        worksheet.Cells[(row + 4), 25].Value = "Percentage of White applicants";

        worksheet.Cells[(row + 1), 26].Value = "Total number of Two or more races";
        worksheet.Cells[(row + 4), 26].Value = "Percentage of Two or more races";

        worksheet.Cells[(row + 1), 27].Value = "Total number of students who did not declare race/ethnicity";
        worksheet.Cells[(row + 4), 27].Value = "Percentage of students who did not declare race/ethnicity";

        worksheet.Cells[(row + 1), 28].Value = "Total number of applicants";
        worksheet.Cells[(row + 4), 28].Value = "";

       
        using (ExcelRange rng = worksheet.Cells["A" + row + ":S" + row])
        {
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }

        using (ExcelRange rng = worksheet.Cells["T" + row + ":DK" + row])
        {
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        using (ExcelRange rng = worksheet.Cells["T2:DK" + row])
        {
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }
        row++;  //row number under "total:" line

        using (ExcelRange rng = worksheet.Cells["BB" + (row + 1) + ":BC" + (row + 1)])
        {
            rng.Merge = true;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rng.Style.WrapText = true;
            rng.IsRichText = true;
            ExcelRichText ert0 = rng.RichText.Add("GPRA Measure 2: ");
            ert0.Bold = true;
            ert0 = rng.RichText.Add("The percentage of magnet schools whose students from major racial and ethnic groups meet or exceed State annual progress standards in reading/language arts.");
            ert0.Bold = false;

            rng.Style.Border.Top.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            rng.Style.Font.Size = 10;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

        }

        using (ExcelRange rng = worksheet.Cells["BB" + (row + 2) + ":BC" + (row + 2)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;

        }
        worksheet.Cells[row + 2, 54].IsRichText = true;
        ExcelRichText ert = worksheet.Cells[row + 2, 54].RichText.Add("GPRA Measure 2a: ");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 54].RichText.Add("Total number of students from major racial and ethnic groups that participated in the State annual progress standards in the State annual progress standards in reading/language arts.");
        ert.Bold = false;

        //worksheet.Cells[row + 1, 40].Value = "Total number of students from major racial and ethnic groups that participated in the State annual progress standards in the State annual progress standards in reading/language arts.";

        worksheet.Cells[row + 2, 55].IsRichText = true;
        ert = worksheet.Cells[row + 2, 55].RichText.Add("GPRA Measure 2b: ");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 55].RichText.Add("Total number of students from major racial and ethnic groups that ");
        ert.Bold = false; 
        ert = worksheet.Cells[row + 2, 55].RichText.Add("met or exceeded");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 55].RichText.Add(" in the State annual progress standards in the State annual progress standards in mathematics.");
        ert.Bold = false;        

        worksheet.Column(54).Width = 40;
        worksheet.Column(55).Width = 40;

        using (ExcelRange rng = worksheet.Cells["BB" + row + ":BC" + (row + 1)])
        {
            rng.Style.Font.Size = 10;
            rng.Style.WrapText = true;
        }

        using (ExcelRange rng = worksheet.Cells["BA" + (row + 3) + ":BC" + (row + 3)])
        {
            rng.Style.Font.Bold = true;
        }

        using (ExcelRange rng = worksheet.Cells["BA" + (row + 4) + ":BC" + (row + 4)])
        {
            rng.Style.Font.Bold = true;
        }

        worksheet.Cells[(row + 3), 53].Value = "With White Students:";
        int sum = 0;
        //with white student col1 SUM(AQ155, AT155, AW155, AZ155, BC155,BF155,  BI155)
        for(int i = 57; i <76; i=(i+3))
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 54].Value = sum;
        worksheet.Cells[(row + 3), 54].Style.Numberformat.Format = "#,##0";
        sum = 0;
        //with white student col2 SUM(AR155, AU155, AX155, BA155, BD155, BG155, BJ155)
        for (int i = 58; i < 77; i = (i + 3))
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 55].Value = sum;
        worksheet.Cells[(row + 3), 55].Style.Numberformat.Format = "#,##0";

        
        worksheet.Cells[(row + 4), 53].Value = "Without White Students:";
        sum = 0;
        //without white student col1 SUM(AQ155, AT155, AW155, AZ155, BC155, BI155)
        for (int i = 57; i < 70; i = (i + 3))
            sum += Convert.ToInt32(gpralist[i]);

        sum += Convert.ToInt32(gpralist[75]);

        worksheet.Cells[(row + 4), 54].Value = sum;
        worksheet.Cells[(row + 4), 54].Style.Numberformat.Format = "#,##0";


        //without white student col2
        sum = 0;
        for (int i = 58; i < 71; i = (i + 3)) //SUM(AR155, AU155, AX155, BA155, BD155, BJ155)
            sum += Convert.ToInt32(gpralist[i]);

        sum += Convert.ToInt32(gpralist[76]);
        worksheet.Cells[(row + 4), 55].Value = sum;
        worksheet.Cells[(row + 4), 55].Style.Numberformat.Format = "#,##0";
        ///////end g2 
        ///gpra3
        using (ExcelRange rng = worksheet.Cells["CF" + (row + 1) + ":CG" + (row + 1)])
        {
            rng.Merge = true;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.IsRichText = true;
            ExcelRichText ert0 = rng.RichText.Add("GPRA Measure 3: ");
            ert0.Bold = true;
            ert0 = rng.RichText.Add("The percentage of magnet schools whose students from major racial and ethnic groups meet or exceed State annual progress standards in mathematics.");
            ert0.Bold = false;

            rng.Style.Border.Top.Style = rng.Style.Border.Left.Style = rng.Style.Border.Right.Style = rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            rng.Style.Font.Size = 10;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

        }

        using (ExcelRange rng = worksheet.Cells["CF" + (row + 2) + ":CG" + (row + 2)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;
        }
      
        worksheet.Column(84).Width = 40;
        worksheet.Column(84).Width = 40;

        using (ExcelRange rng = worksheet.Cells["CF" + row + ":CG" + (row + 1)])
        {
            rng.Style.Font.Size = 10;
            rng.Style.WrapText = true;
        }

        using (ExcelRange rng = worksheet.Cells["CE" + (row + 3) + ":CG" + (row + 3)])
        {
            rng.Style.Font.Bold = true;
        }

        using (ExcelRange rng = worksheet.Cells["CE" + (row + 4) + ":CG" + (row + 4)])
        {
            rng.Style.Font.Bold = true;
        }

        worksheet.Column(83).Width = 35;
        worksheet.Cells[(row + 3), 83].Value = "With White Students:";
        worksheet.Cells[(row + 3), 83].Style.WrapText = true;
        sum = 0;
        //with white student col1
        for (int i = 87; i < 106; i = (i + 3)) //(BU155, BX155, CA155, CD155, CG155, CJ155, CM155)
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 84].Value = sum;
        worksheet.Cells[(row + 3), 84].Style.Numberformat.Format = "#,##0";
        sum = 0;
        //with white student col2
        for (int i = 88; i < 107; i = (i + 3))  //SUM(BV155, BY155, CB155, CE155, CH155, CK155, CN155)
            sum += Convert.ToInt32(gpralist[i]);

        worksheet.Cells[(row + 3), 85].Value = sum;
        worksheet.Cells[(row + 3), 85].Style.Numberformat.Format = "#,##0";


        worksheet.Cells[(row + 4), 83].Value = "Without White Students:";
        worksheet.Cells[(row + 4), 83].Style.WrapText = true;
        sum = 0;
        //without white student col1
        for (int i = 87; i < 100; i = (i + 3)) //=SUM(BU155, BX155, CA155, CD155, CG155, CM155)
            sum += Convert.ToInt32(gpralist[i]);

        sum += Convert.ToInt32(gpralist[105]);

        worksheet.Cells[(row + 4), 84].Value = sum;
        worksheet.Cells[(row + 4), 84].Style.Numberformat.Format = "#,##0";

        //without white student col2
        sum = 0;
        for (int i = 88; i < 101; i = (i + 3))  //SUM(BV155, BY155, CB155, CE155, CH155, CN155)
            sum += Convert.ToInt32(gpralist[i]);
        sum += Convert.ToInt32(gpralist[92]);

        worksheet.Cells[(row + 4), 85].Value = sum;
        worksheet.Cells[(row + 4), 85].Style.Numberformat.Format = "#,##0";
        ///////
        ///end gpra3
        worksheet.Cells[row, 84].IsRichText = true;
        ert = worksheet.Cells[row + 2, 84].RichText.Add("GPRA Measure 3a:");
        ert.Bold = true;
        
        ert = worksheet.Cells[row + 2, 84].RichText.Add("  Total number of students from major racial and ethnic groups that participated in the State annual progress standards in the State annual progress standards in mathematics.");
        ert.Bold = false;

        worksheet.Cells[row + 2, 85].IsRichText = true;
        ert = worksheet.Cells[row + 2, 85].RichText.Add("GPRA Measure 3b:");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 85].RichText.Add("  Total number of students from major racial and ethnic groups that");
        ert.Bold = false;
        ert = worksheet.Cells[row + 2, 85].RichText.Add(" met or exceeded ");
        ert.Bold = true;
        ert = worksheet.Cells[row + 2, 85].RichText.Add(" in the State annual progress standards in the State annual progress standards in mathematics.");
        ert.Bold = false;

        worksheet.Column(84).Width = 40;
        worksheet.Column(85).Width = 40;

        using (ExcelRange rng = worksheet.Cells["CF" + row + ":CG" + (row + 1)])
        {
            rng.Style.Font.Size =10;
            rng.Style.WrapText = true;
        }

        using (ExcelRange rng = worksheet.Cells["CV" + (row + 1) + ":CW" + (row + 1)])
        {
            rng.Style.WrapText = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
        }
        using (ExcelRange rng = worksheet.Cells["DJ" + (row + 2) + ":DK" + (row + 2)])
        {
            rng.Style.WrapText = true;
            rng.Style.Font.Size = 10;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        using (ExcelRange rng = worksheet.Cells["DJ" + (row + 3) + ":DK" + (row + 3)])
        {
            rng.Value = "MSAP_Funds/Studs_Served";
            rng.Merge = true;
            rng.Style.Font.Bold = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;  
        }

       

        using (ExcelRange rng = worksheet.Cells["DJ" + (row + 4) + ":DK" + (row + 4)])
        {
            rng.Merge = true;
            rng.Style.Font.Size = 10;
            rng.Style.WrapText = true;

            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            rng.IsRichText = true;
            ExcelRichTextCollection rtfCollection = rng.RichText;
            ert = rtfCollection.Add("GPRA Measure 6:");
            ert.Bold = true;

            ert = rtfCollection.Add(" The cost per student in a magnet school. ");
            ert.Bold = false;

        }

        worksheet.Row(row + 4).Height = 30;

    }

    public static void GetGPRA1(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;  //1 - 19
        int granteeID = -1;
        bool isAlternative = false;

        worksheet.Name = "GPRA I";

        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "Grantee_Name";
        worksheet.Cells[row, 4].Value = "SchoolID";
        worksheet.Cells[row, 5].Value = "School_Name";
        //worksheet.Cells[row, 6].Value = "PRAward";
        worksheet.Cells[row, 6].Value = "Grades";
        worksheet.Cells[row, 7].Value = "Program_Type";
        worksheet.Cells[row, 8].Value = "Coded_Program_Type";
        worksheet.Cells[row, 9].Value = "Title_I";
        worksheet.Cells[row, 10].Value = "Coded_Title_I";
        worksheet.Cells[row, 11].Value = "School_Improvement";
        worksheet.Cells[row, 12].Value = "Coded_School_Improvement";
        worksheet.Cells[row, 13].Value = "School_Improvement_Status";
        worksheet.Cells[row, 14].Value = "Coded_SIS";
        worksheet.Cells[row, 15].Value = "Lowest_Achieving";
        worksheet.Cells[row, 16].Value = "Coded_Lowest_Achieving";
        worksheet.Cells[row, 17].Value = "SIG";
        worksheet.Cells[row, 18].Value = "Coded_SIG";
        worksheet.Cells[row, 19].Value = "FRPMPercent";

        Hashtable SIScode = new Hashtable(); //Coded_SIS: row15
        SIScode.Add(8, 1);
        SIScode.Add(4, 2);
        SIScode.Add(23, 3);
        SIScode.Add(2, 4);
        SIScode.Add(5, 5);
        SIScode.Add(6, 6);
        SIScode.Add(7, 7);
        SIScode.Add(1, 8);
        SIScode.Add(9, 9);
        SIScode.Add(10, 10);
        SIScode.Add(11, 11);
        SIScode.Add(12, 12);
        SIScode.Add(13, 13);
        SIScode.Add(14, 14);
        SIScode.Add(15, 15);
        SIScode.Add(16, 16);
        SIScode.Add(17, 17);
        SIScode.Add(18, 18);
        SIScode.Add(19, 19);
        SIScode.Add(20, 20);
        SIScode.Add(21, 21);
        SIScode.Add(22, 22);
        SIScode.Add(3, 23);
        SIScode.Add(24, 24);
        SIScode.Add(26, 26);
        SIScode.Add(27, 27);
        SIScode.Add(28, 28);
        SIScode.Add(29, 98);
        SIScode.Add(30, 30);
        SIScode.Add(31, 31);

        Hashtable ImpvStatus = new Hashtable(); //School_Improvement_Status: row14
        ImpvStatus.Add(8, "Approaching target");
        ImpvStatus.Add(4, "Celebration");
        ImpvStatus.Add(23, "Continuous improvement");
        ImpvStatus.Add(2, "Corrective action");
        ImpvStatus.Add(5, "Excelling school");
        ImpvStatus.Add(6, "Focus");
        ImpvStatus.Add(7, "Focus targeted");
        ImpvStatus.Add(1, "Improvement");
        ImpvStatus.Add(9, "Improvement required");
        ImpvStatus.Add(10, "In improvement");
        ImpvStatus.Add(11, "Level 1");
        ImpvStatus.Add(12, "Level 2");
        ImpvStatus.Add(13, "Level 3 (focus)");
        ImpvStatus.Add(14, "Level 4 (priority)");
        ImpvStatus.Add(15, "Level 5 (priority)");
        ImpvStatus.Add(16, "Met alternative standard");
        ImpvStatus.Add(17, "Met standard");
        ImpvStatus.Add(18, "On target");
        ImpvStatus.Add(19, "Performance");
        ImpvStatus.Add(20, "Priority");
        ImpvStatus.Add(21, "Priority improvement");
        ImpvStatus.Add(22, "Progressing");
        ImpvStatus.Add(3, "Restructuring");
        ImpvStatus.Add(24, "Review (including focus)");
        ImpvStatus.Add(25, "Reward");
        ImpvStatus.Add(26, "Transitioning");
        ImpvStatus.Add(27, "Turnaround");
        ImpvStatus.Add(28, "Other");
        ImpvStatus.Add(29, "N/A");
        ImpvStatus.Add(99, "missing");
        ImpvStatus.Add(30, "In good standing");
        ImpvStatus.Add(31, "Prevent");

        worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:S1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x => x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                row++;

                if (isAlternative)
                {
                    using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                    {
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                    }

                }

                worksheet.Cells[row, 1].Value = report.ID;

                worksheet.Cells[row, 2].Value = report.GranteeID;
                worksheet.Cells[row, 3].Value = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID).GranteeName;
                worksheet.Cells[row, 4].Value = school.SchoolCode;
                worksheet.Cells[row, 5].Value = school.SchoolName;

                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                    MagnetGPRA item = gpraData[0];
                    string GradeString = "";
                    if (!string.IsNullOrEmpty(item.SchoolGrade))
                    {
                        foreach (string str in item.SchoolGrade.Split(';'))
                        {
                            switch (str)
                            {
                                case "1":
                                    GradeString += "Pre-K;";
                                    break;
                                case "2":
                                    GradeString += "K;"; break;
                                case "3":
                                    GradeString += "First;"; break;
                                case "4":
                                    GradeString += "Second;"; break;
                                case "5":
                                    GradeString += "Third;"; break;
                                case "6":
                                    GradeString += "Fourth;"; break;
                                case "7":
                                    GradeString += "Fifth;"; break;
                                case "8":
                                    GradeString += "Sixth;"; break;
                                case "9":
                                    GradeString += "Seventh;"; break;
                                case "10":
                                    GradeString += "Eighth;"; break;
                                case "11":
                                    GradeString += "Ninth;"; break;
                                case "12":
                                    GradeString += "Tenth;"; break;
                                case "13":
                                    GradeString += "Eleventh;"; break;
                                case "14":
                                    GradeString += "Twelfth;"; break;
                            }
                        }
                    }
                    worksheet.Cells[row, 6].Value = GradeString;
                    if (item.ProgramType != null)
                    {
                        worksheet.Cells[row, 7].Value = (bool)item.ProgramType ? "Partial" : "Whole-school";
                        worksheet.Cells[row, 8].Value = (bool)item.ProgramType ? 2 : 1;
                    }
                    else
                    {
                        worksheet.Cells[row, 7].Value = "Missing";
                        worksheet.Cells[row, 8].Value = 99;
                    }


                    //Title I
                    if (item.TitleISchoolFunding != null)
                    {
                        worksheet.Cells[row, 9].Value = (bool)item.TitleISchoolFunding ? "Yes" : "No";
                        worksheet.Cells[row, 10].Value = (bool)item.TitleISchoolFunding ? 1 : 0;

                        //if (item.TitleISchoolFunding == true) //Title I = Yes
                        //{
                        if (item.TitleISchoolFundingImprovement != null)
                        {
                            if ((item.TitleISchoolFundingImprovement == true))
                            {
                                worksheet.Cells[row, 11].Value = "Yes";
                                worksheet.Cells[row, 12].Value = 1;
                            }
                            else //school imp = no
                            {
                                worksheet.Cells[row, 11].Value = "No";
                                worksheet.Cells[row, 12].Value = 0;

                                worksheet.Cells[row, 13].Value = "Not Applicable";
                                worksheet.Cells[row, 14].Value = 98;
                            }
                            //}
                            //else
                            //{
                            //    worksheet.Cells[row, 11].Value = "Missing";
                            //    worksheet.Cells[row, 12].Value = 99;

                            //    worksheet.Cells[row, 13].Value = "Missing";
                            //    worksheet.Cells[row, 14].Value = 99;
                            //}

                        }
                        else //Title I = No
                        {
                            worksheet.Cells[row, 11].Value = "Not Applicable";
                            worksheet.Cells[row, 12].Value = 98;

                            worksheet.Cells[row, 13].Value = "Not Applicable";
                            worksheet.Cells[row, 14].Value = 98;
                        }

                    }
                    else
                    {
                        worksheet.Cells[row, 9].Value = "Missing";
                        worksheet.Cells[row, 10].Value = 99;

                        worksheet.Cells[row, 11].Value = "Missing";
                        worksheet.Cells[row, 12].Value = 99;

                        worksheet.Cells[row, 13].Value = "Missing";
                        worksheet.Cells[row, 14].Value = 99;
                    }

                    if (item.TitleISchoolFundingImprovementStatus != null)
                    {
                        int key = (int)item.TitleISchoolFundingImprovementStatus;
                        //key = key == 29 ? 98 : key;

                        worksheet.Cells[row, 13].Value = ImpvStatus[key];
                        worksheet.Cells[row, 14].Value = SIScode[key]; //item.TitleISchoolFundingImprovementStatus;
                    }

                    //lowest achieving
                    if (item.PersistentlyLlowestAchievingSchool != null)
                    {
                        worksheet.Cells[row, 15].Value = (bool)item.PersistentlyLlowestAchievingSchool ? "Yes" : "No";
                        worksheet.Cells[row, 16].Value = (bool)item.PersistentlyLlowestAchievingSchool ? 1 : 0;
                    }

                    if (item.SchoolImprovementGrant != null)
                    {
                        worksheet.Cells[row, 17].Value = (bool)item.SchoolImprovementGrant ? "Yes" : "No";
                        worksheet.Cells[row, 18].Value = (bool)item.SchoolImprovementGrant ? 1 : 0;
                    }
                    else
                    {
                        worksheet.Cells[row, 17].Value = "Missing";
                        worksheet.Cells[row, 18].Value = 99;
                    }

                    if (item.FRPMPpercentage != null)
                        worksheet.Cells[row, 19].Value = (Convert.ToDouble(item.FRPMPpercentage) / 100).ToString("F");
                    else
                        worksheet.Cells[row, 19].Value = "-99";

                }
                else
                {
                    worksheet.Cells[row, 6].Value = "";
                    worksheet.Cells[row, 7].Value = "Missing";
                    worksheet.Cells[row, 8].Value = 99;
                    worksheet.Cells[row, 9].Value = "Missing";
                    worksheet.Cells[row, 10].Value = 99;
                    worksheet.Cells[row, 11].Value = "Missing";
                    worksheet.Cells[row, 12].Value = 99;
                    worksheet.Cells[row, 13].Value = "Missing";
                    worksheet.Cells[row, 14].Value = 99;
                    worksheet.Cells[row, 15].Value = "";
                    worksheet.Cells[row, 16].Value = "";
                    worksheet.Cells[row, 17].Value = "Missing";
                    worksheet.Cells[row, 18].Value = 99;
                    worksheet.Cells[row, 19].Value = "-99";
                }
            }
        }
    }

    public static void GetGPRA2(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
        int TotalSchools = 0;
        int granteeID = -1;
        bool isAlternative = false;

        worksheet.Name = "GPRA II";  //20 -- 53

        //worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 1].Value = "GranteeID";
        worksheet.Cells[row, 2].Value = "Grantee_Name";
        worksheet.Cells[row, 3].Value = "SchoolID";
        worksheet.Cells[row, 4].Value = "School_Name";
        worksheet.Cells[row, 5].Value = "App_Amer_Indian";
        worksheet.Cells[row, 6].Value = "App_Asian";
        worksheet.Cells[row, 7].Value = "App_Black";
        worksheet.Cells[row, 8].Value = "App_Hispanic";
        worksheet.Cells[row, 9].Value = "App_Native_Hawaiian";
        worksheet.Cells[row, 10].Value = "App_White";
        worksheet.Cells[row, 11].Value = "App_Two_more";
        worksheet.Cells[row, 12].Value = "Undeclared";
        worksheet.Cells[row, 13].Value = "App_Total";
        worksheet.Cells[row, 14].Value = "Enr_Amer_Indian";
        worksheet.Cells[row, 15].Value = "Enr_Asian";
        worksheet.Cells[row, 16].Value = "Enr_Black";
        worksheet.Cells[row, 17].Value = "Enr_Hispanic";
        worksheet.Cells[row, 18].Value = "Enr_Native_Hawaiian";
        worksheet.Cells[row, 19].Value = "Enr_White";
        worksheet.Cells[row, 20].Value = "Enr_Two_more";
        worksheet.Cells[row, 21].Value = "Enr_Total";
        worksheet.Cells[row, 22].Value = "New_Amer_Indian";
        worksheet.Cells[row, 23].Value = "New_Asian";
        worksheet.Cells[row, 24].Value = "New_Black";
        worksheet.Cells[row, 25].Value = "New_Hispanic";
        worksheet.Cells[row, 26].Value = "New_Native_Hawaiian";
        worksheet.Cells[row, 27].Value = "New_White";
        worksheet.Cells[row, 28].Value = "New_Two_more";
        //New_students
        worksheet.Cells[row, 29].Value = "Total_New";
        worksheet.Cells[row, 30].Value = "Cont_Amer_Indian";
        worksheet.Cells[row, 31].Value = "Cont_Asian";
        worksheet.Cells[row, 32].Value = "Cont_Black";
        worksheet.Cells[row, 33].Value = "Cont_Hispanic";
        worksheet.Cells[row, 34].Value = "Cont_Native_Hawaiian";
        worksheet.Cells[row, 35].Value = "Cont_White";
        worksheet.Cells[row, 36].Value = "Cont_Two_more";
        //Continuing_Students
        worksheet.Cells[row, 37].Value = "Total_Cont";

        // offset 53-39=14 to 101
        worksheet.Cells[row, 38].Value = "Total_Sch_Enroll";

        Hashtable gpralist = new Hashtable();
        // 20 -- 53
        gpralist.Add(5, 0);
        gpralist.Add(6, 0);
        gpralist.Add(7, 0);
        gpralist.Add(8, 0);
        gpralist.Add(9, 0);
        gpralist.Add(10, 0);
        gpralist.Add(11, 0);
        gpralist.Add(12, 0);
        gpralist.Add(13, 0);
        gpralist.Add(14, 0);
        gpralist.Add(15, 0);
        gpralist.Add(16, 0);
        gpralist.Add(17, 0);
        gpralist.Add(18, 0);
        gpralist.Add(19, 0);
        gpralist.Add(20, 0);
        gpralist.Add(21, 0);
        gpralist.Add(22, 0);
        gpralist.Add(23, 0);
        gpralist.Add(24, 0);
        gpralist.Add(25, 0);
        gpralist.Add(26, 0);
        gpralist.Add(27, 0);
        gpralist.Add(28, 0);
        gpralist.Add(29, 0);
        gpralist.Add(30, 0);
        gpralist.Add(31, 0);
        gpralist.Add(32, 0);
        gpralist.Add(33, 0);
        gpralist.Add(34, 0);
        gpralist.Add(35, 0);
        gpralist.Add(36, 0);
        gpralist.Add(37, 0);
        gpralist.Add(38, 0);

        worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:AL1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x => x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                row++;

                if (isAlternative)
                {
                    using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                    {
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                    }

                }

                //worksheet.Cells[row, 1].Value = report.ID;
                worksheet.Cells[row, 1].Value = report.GranteeID;
                worksheet.Cells[row, 2].Value = grantee.GranteeName;
                worksheet.Cells[row, 3].Value = school.SchoolCode;
                worksheet.Cells[row, 4].Value = school.SchoolName;
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                    MagnetGPRA item = gpraData[0];
                    ////////////////////////////////////////////////////////////////

                    var applicantPoolData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 1);
                    if (applicantPoolData.Count > 0) //App_
                    {
                        worksheet.Cells[row, 5].Value = applicantPoolData[0].Indian;
                        gpralist[5] = Convert.ToDouble(gpralist[5]) + Convert.ToDouble(worksheet.Cells[row, 5].Value);
                        worksheet.Cells[row, 6].Value = applicantPoolData[0].Asian;
                        gpralist[6] = Convert.ToDouble(gpralist[6]) + Convert.ToDouble(worksheet.Cells[row, 6].Value);
                        worksheet.Cells[row, 7].Value = applicantPoolData[0].Black;
                        gpralist[7] = Convert.ToDouble(gpralist[7]) + Convert.ToDouble(worksheet.Cells[row, 7].Value);
                        worksheet.Cells[row, 8].Value = applicantPoolData[0].Hispanic;
                        gpralist[8] = Convert.ToDouble(gpralist[8]) + Convert.ToDouble(worksheet.Cells[row, 8].Value);
                        worksheet.Cells[row, 9].Value = applicantPoolData[0].Hawaiian;
                        gpralist[9] = Convert.ToDouble(gpralist[9]) + Convert.ToDouble(worksheet.Cells[row, 9].Value);
                        worksheet.Cells[row, 10].Value = applicantPoolData[0].White;
                        gpralist[10] = Convert.ToDouble(gpralist[10]) + Convert.ToDouble(worksheet.Cells[row, 10].Value);
                        worksheet.Cells[row, 11].Value = applicantPoolData[0].MultiRaces;
                        gpralist[11] = Convert.ToDouble(gpralist[11]) + Convert.ToDouble(worksheet.Cells[row, 11].Value);
                        worksheet.Cells[row, 12].Value = applicantPoolData[0].Undeclared;
                        gpralist[12] = Convert.ToDouble(gpralist[12]) + Convert.ToDouble(worksheet.Cells[row, 12].Value);
                        worksheet.Cells[row, 13].Value = applicantPoolData[0].Total;
                        gpralist[13] = Convert.ToDouble(gpralist[13]) + Convert.ToDouble(worksheet.Cells[row, 13].Value);
                    }
                    var enrollmentData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 2);
                    if (enrollmentData.Count > 0) //Enr_
                    {
                        item18_Indian = enrollmentData[0].Indian == null ? 0 : (int)enrollmentData[0].Indian;
                        item19_Asian = enrollmentData[0].Asian == null ? 0 : (int)enrollmentData[0].Asian;
                        item20_Black = enrollmentData[0].Black == null ? 0 : (int)enrollmentData[0].Black;
                        item21_Hispanic = enrollmentData[0].Hispanic == null ? 0 : (int)enrollmentData[0].Hispanic;
                        item22_Hawaiian = enrollmentData[0].Hawaiian == null ? 0 : (int)enrollmentData[0].Hawaiian;
                        item23_White = enrollmentData[0].White == null ? 0 : (int)enrollmentData[0].White;

                        item26a_newIndian = enrollmentData[0].NewIndian == null ? 0 : (int)enrollmentData[0].NewIndian;
                        item26b_newAsian = enrollmentData[0].NewAsian == null ? 0 : (int)enrollmentData[0].NewAsian;
                        item26c_newBlack = enrollmentData[0].NewBlack == null ? 0 : (int)enrollmentData[0].NewBlack;
                        item26d_newHispanic = enrollmentData[0].NewHispanic == null ? 0 : (int)enrollmentData[0].NewHispanic;
                        item26e_newHawaiian = enrollmentData[0].NewHawaiian == null ? 0 : (int)enrollmentData[0].NewHawaiian;
                        item26f_newWhite = enrollmentData[0].NewWhite == null ? 0 : (int)enrollmentData[0].NewWhite;

                        item27a_ContIndian = enrollmentData[0].ContIndian == null ? 0 : (int)enrollmentData[0].ContIndian;
                        item27b_ContAsian = enrollmentData[0].ContAsian == null ? 0 : (int)enrollmentData[0].ContAsian;
                        item27c_ContBlack = enrollmentData[0].ContBlack == null ? 0 : (int)enrollmentData[0].ContBlack;
                        item27d_ContHispanic = enrollmentData[0].ContHispanic == null ? 0 : (int)enrollmentData[0].ContHispanic;
                        item27e_ContHawaiian = enrollmentData[0].ContHawaiian == null ? 0 : (int)enrollmentData[0].ContHawaiian;
                        item27f_ContWhite = enrollmentData[0].ContWhite == null ? 0 : (int)enrollmentData[0].ContWhite;

                        item28_total = enrollmentData[0].ExtraFiled3 == null ? 0 : (int)enrollmentData[0].ExtraFiled3;

                        worksheet.Cells[row, 14].Value = enrollmentData[0].Indian;
                        gpralist[14] = Convert.ToDouble(gpralist[14]) + Convert.ToDouble(worksheet.Cells[row, 14].Value);
                        worksheet.Cells[row, 15].Value = enrollmentData[0].Asian;
                        gpralist[15] = Convert.ToDouble(gpralist[15]) + Convert.ToDouble(worksheet.Cells[row, 15].Value);
                        worksheet.Cells[row, 16].Value = enrollmentData[0].Black;
                        gpralist[16] = Convert.ToDouble(gpralist[16]) + Convert.ToDouble(worksheet.Cells[row, 16].Value);
                        worksheet.Cells[row, 17].Value = enrollmentData[0].Hispanic;
                        gpralist[17] = Convert.ToDouble(gpralist[17]) + Convert.ToDouble(worksheet.Cells[row, 17].Value);
                        worksheet.Cells[row, 18].Value = enrollmentData[0].Hawaiian;
                        gpralist[18] = Convert.ToDouble(gpralist[18]) + Convert.ToDouble(worksheet.Cells[row, 18].Value);
                        worksheet.Cells[row, 19].Value = enrollmentData[0].White;
                        gpralist[19] = Convert.ToDouble(gpralist[19]) + Convert.ToDouble(worksheet.Cells[row, 19].Value);
                        worksheet.Cells[row, 20].Value = enrollmentData[0].MultiRaces;
                        gpralist[20] = Convert.ToDouble(gpralist[20]) + Convert.ToDouble(worksheet.Cells[row, 20].Value);
                        worksheet.Cells[row, 21].Value = enrollmentData[0].Total;
                        gpralist[21] = Convert.ToDouble(gpralist[21]) + Convert.ToDouble(worksheet.Cells[row, 21].Value);

                        if (enrollmentData[0].NewIndian != null)
                        {
                            worksheet.Cells[row, 22].Value = enrollmentData[0].NewIndian.ToString();
                            gpralist[22] = Convert.ToDouble(gpralist[22]) + Convert.ToDouble(worksheet.Cells[row, 22].Value);
                        }
                        else
                            worksheet.Cells[row, 22].Value = "missing";

                        if (enrollmentData[0].NewAsian != null)
                        {
                            worksheet.Cells[row, 23].Value = enrollmentData[0].NewAsian.ToString();
                            gpralist[23] = Convert.ToDouble(gpralist[23]) + Convert.ToDouble(worksheet.Cells[row, 23].Value);
                        }
                        else
                            worksheet.Cells[row, 23].Value = "missing";

                        if (enrollmentData[0].NewBlack != null)
                        {
                            worksheet.Cells[row, 24].Value = enrollmentData[0].NewBlack.ToString();
                            gpralist[24] = Convert.ToDouble(gpralist[24]) + Convert.ToDouble(worksheet.Cells[row, 24].Value);
                        }
                        else
                            worksheet.Cells[row, 24].Value = "missing";

                        if (enrollmentData[0].NewHispanic != null)
                        {
                            worksheet.Cells[row, 25].Value = enrollmentData[0].NewHispanic.ToString();
                            gpralist[25] = Convert.ToDouble(gpralist[25]) + Convert.ToDouble(worksheet.Cells[row, 25].Value);
                        }
                        else
                            worksheet.Cells[row, 25].Value = "missing";

                        if (enrollmentData[0].NewHawaiian != null)
                        {
                            worksheet.Cells[row, 26].Value = enrollmentData[0].NewHawaiian.ToString();
                            gpralist[26] = Convert.ToDouble(gpralist[26]) + Convert.ToDouble(worksheet.Cells[row, 26].Value);
                        }
                        else
                            worksheet.Cells[row, 26].Value = "missing";

                        if (enrollmentData[0].NewWhite != null)
                        {
                            worksheet.Cells[row, 27].Value = enrollmentData[0].NewWhite.ToString();
                            gpralist[27] = Convert.ToDouble(gpralist[27]) + Convert.ToDouble(worksheet.Cells[row, 27].Value);
                        }
                        else
                            worksheet.Cells[row, 27].Value = "missing";

                        if (enrollmentData[0].NewMultiRaces != null)
                        {
                            worksheet.Cells[row, 28].Value = enrollmentData[0].NewMultiRaces.ToString();
                            gpralist[28] = Convert.ToDouble(gpralist[28]) + Convert.ToDouble(worksheet.Cells[row, 28].Value);
                        }
                        else
                            worksheet.Cells[row, 28].Value = "missing";

                        if (enrollmentData[0].ExtraFiled1 != null)
                        {
                            worksheet.Cells[row, 29].Value = enrollmentData[0].ExtraFiled1.ToString(); //new_student
                            gpralist[29] = Convert.ToDouble(gpralist[29]) + Convert.ToDouble(worksheet.Cells[row, 29].Value);
                        }
                        else
                            worksheet.Cells[row, 29].Value = "missing";

                        if (enrollmentData[0].ContIndian != null)
                        {
                            worksheet.Cells[row, 30].Value = enrollmentData[0].ContIndian.ToString();
                            gpralist[30] = Convert.ToDouble(gpralist[30]) + Convert.ToDouble(worksheet.Cells[row, 30].Value);
                        }
                        else
                            worksheet.Cells[row, 30].Value = "missing";

                        if (enrollmentData[0].ContAsian != null)
                        {
                            worksheet.Cells[row, 31].Value = enrollmentData[0].ContAsian.ToString();
                            gpralist[31] = Convert.ToDouble(gpralist[31]) + Convert.ToDouble(worksheet.Cells[row, 31].Value);
                        }
                        else
                            worksheet.Cells[row, 31].Value = "missing";

                        if (enrollmentData[0].ContBlack != null)
                        {
                            worksheet.Cells[row, 32].Value = enrollmentData[0].ContBlack.ToString();
                            gpralist[32] = Convert.ToDouble(gpralist[32]) + Convert.ToDouble(worksheet.Cells[row, 32].Value);
                        }
                        else
                            worksheet.Cells[row, 32].Value = "missing";

                        if (enrollmentData[0].ContHispanic != null)
                        {
                            worksheet.Cells[row, 33].Value = enrollmentData[0].ContHispanic.ToString();
                            gpralist[33] = Convert.ToDouble(gpralist[33]) + Convert.ToDouble(worksheet.Cells[row, 33].Value);
                        }
                        else
                            worksheet.Cells[row, 33].Value = "missing";

                        if (enrollmentData[0].ContHawaiian != null)
                        {
                            worksheet.Cells[row, 34].Value = enrollmentData[0].ContHawaiian.ToString();
                            gpralist[34] = Convert.ToDouble(gpralist[34]) + Convert.ToDouble(worksheet.Cells[row, 34].Value);
                        }
                        else
                            worksheet.Cells[row, 34].Value = "missing";

                        if (enrollmentData[0].ContWhite != null)
                        {
                            worksheet.Cells[row, 35].Value = enrollmentData[0].ContWhite.ToString();
                            gpralist[35] = Convert.ToDouble(gpralist[35]) + Convert.ToDouble(worksheet.Cells[row, 35].Value);
                        }
                        else
                            worksheet.Cells[row, 35].Value = "missing";

                        if (enrollmentData[0].ContMultiRaces != null)
                        {
                            worksheet.Cells[row, 36].Value = enrollmentData[0].ContMultiRaces.ToString();
                            gpralist[36] = Convert.ToDouble(gpralist[36]) + Convert.ToDouble(worksheet.Cells[row, 36].Value);
                        }
                        else
                            worksheet.Cells[row, 36].Value = "missing";

                        if (enrollmentData[0].ExtraFiled2 != null)
                        {
                            worksheet.Cells[row, 37].Value = enrollmentData[0].ExtraFiled2.ToString(); //Continuing Students
                            gpralist[37] = Convert.ToDouble(gpralist[37]) + Convert.ToDouble(worksheet.Cells[row, 37].Value);
                        }
                        else
                            worksheet.Cells[row, 37].Value = "missing";

                        worksheet.Cells[row, 38].Value = enrollmentData[0].ExtraFiled3; //Total_sch_Enroll
                        gpralist[38] = Convert.ToDouble(gpralist[38]) + Convert.ToDouble(worksheet.Cells[row, 38].Value);
                    }
                }
                else
                {
                    worksheet.Cells[row, 5].Value = "missing";
                    worksheet.Cells[row, 6].Value = "missing";
                    worksheet.Cells[row, 7].Value = "missing";
                    worksheet.Cells[row, 8].Value = "missing";
                    worksheet.Cells[row, 9].Value = "missing";
                    worksheet.Cells[row, 10].Value = "missing";
                    worksheet.Cells[row, 11].Value = "missing";
                    worksheet.Cells[row, 12].Value = "missing";
                    worksheet.Cells[row, 13].Value = "missing";
                    worksheet.Cells[row, 14].Value = "missing";
                    worksheet.Cells[row, 15].Value = "missing";
                    worksheet.Cells[row, 16].Value = "missing";
                    worksheet.Cells[row, 17].Value = "missing";
                    worksheet.Cells[row, 18].Value = "missing";
                    worksheet.Cells[row, 19].Value = "missing";
                    worksheet.Cells[row, 20].Value = "missing";
                    worksheet.Cells[row, 21].Value = "missing";
                    worksheet.Cells[row, 22].Value = "missing";
                    worksheet.Cells[row, 23].Value = "missing";
                    worksheet.Cells[row, 24].Value = "missing";
                    worksheet.Cells[row, 25].Value = "missing";
                    worksheet.Cells[row, 26].Value = "missing";
                    worksheet.Cells[row, 27].Value = "missing";
                    worksheet.Cells[row, 28].Value = "missing";
                    worksheet.Cells[row, 29].Value = "missing";
                    worksheet.Cells[row, 30].Value = "missing";
                    worksheet.Cells[row, 31].Value = "missing";
                    worksheet.Cells[row, 32].Value = "missing";
                    worksheet.Cells[row, 33].Value = "missing";
                    worksheet.Cells[row, 34].Value = "missing";
                    worksheet.Cells[row, 35].Value = "missing";
                    worksheet.Cells[row, 36].Value = "missing";
                    worksheet.Cells[row, 37].Value = "missing";
                    worksheet.Cells[row, 38].Value = "missing";
                }
            }
        }
    }

    public static void GetGPRA3(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
        int TotalSchools = 0;
        int granteeID = -1;
        bool isAlternative = false;

        Hashtable gpralist = new Hashtable();
        gpralist.Add(5, 0);
        gpralist.Add(6, 0);
        gpralist.Add(7, 0);
        gpralist.Add(8, 0);
        gpralist.Add(9, 0);
        gpralist.Add(10, 0);
        gpralist.Add(11, 0);
        gpralist.Add(12, 0);
        gpralist.Add(13, 0);
        gpralist.Add(14, 0);
        gpralist.Add(15, 0);
        gpralist.Add(16, 0);
        gpralist.Add(17, 0);
        gpralist.Add(18, 0);
        gpralist.Add(19, 0);
        gpralist.Add(20, 0);
        gpralist.Add(21, 0);
        gpralist.Add(22, 0);
        gpralist.Add(23, 0);
        gpralist.Add(24, 0);
        gpralist.Add(25, 0);
        gpralist.Add(26, 0);
        gpralist.Add(27, 0);
        gpralist.Add(28, 0);
        gpralist.Add(29, 0);
        gpralist.Add(30, 0);
        gpralist.Add(31, 0);
        gpralist.Add(32, 0);
        gpralist.Add(33, 0);
        gpralist.Add(34, 0);
        gpralist.Add(35, 0);
        gpralist.Add(36, 0);
        gpralist.Add(37, 0);
        gpralist.Add(38, 0);
        gpralist.Add(39, 0);
        gpralist.Add(40, 0);
        gpralist.Add(41, 0);
        gpralist.Add(42, 0);
        gpralist.Add(43, 0);
        gpralist.Add(44, 0);
        gpralist.Add(45, 0);
        gpralist.Add(46, 0);
        gpralist.Add(47, 0);
        gpralist.Add(48, 0);
        gpralist.Add(49, 0);
        gpralist.Add(50, 0);
        gpralist.Add(51, 0);
        gpralist.Add(52, 0);
        gpralist.Add(53, 0);
        gpralist.Add(54, 0);
        gpralist.Add(55, 0);
        gpralist.Add(56, 0);
        gpralist.Add(57, 0);
        gpralist.Add(58, 0);
        gpralist.Add(59, 0);
        gpralist.Add(60, 0);
        gpralist.Add(61, 0);
        gpralist.Add(62, 0);
        gpralist.Add(63, 0);
        gpralist.Add(64, 0);

        worksheet.Name = "GPRA III";          //54 --113

        worksheet.Cells[row, 1].Value = "GranteeID";
        worksheet.Cells[row, 2].Value = "Grantee_Name";
        worksheet.Cells[row, 3].Value = "SchoolID";
        worksheet.Cells[row, 4].Value = "School_Name";

        worksheet.Cells[row, 5].Value = "Part_AYP_R_All";
        worksheet.Cells[row, 6].Value = "Met_AYP_R_All";
        worksheet.Cells[row, 7].Value = "%_AYP_R_All";

        worksheet.Cells[row, 8].Value = "Part_AYP_R_Amer_Ind";
        worksheet.Cells[row, 9].Value = "Met_AYP_R_Amer_Ind";
        worksheet.Cells[row, 10].Value = "%_AYP_R_Amer_Ind";

        worksheet.Cells[row, 11].Value = "Part_AYP_R_Asian";
        worksheet.Cells[row, 12].Value = "Met_AYP_R_Asian";
        worksheet.Cells[row, 13].Value = "%_AYP_R_Asian";

        worksheet.Cells[row, 14].Value = "Part_AYP_R_Black";
        worksheet.Cells[row, 15].Value = "Met_AYP_R_Black";
        worksheet.Cells[row, 16].Value = "%_AYP_R_Black";

        worksheet.Cells[row, 17].Value = "Part_AYP_R_Hispanic";
        worksheet.Cells[row, 18].Value = "Met_AYP_R_Hispanic";
        worksheet.Cells[row, 19].Value = "%_AYP_R_Hispanic";

        worksheet.Cells[row, 20].Value = "Part_AYP_R_Native_Hawaiian";
        worksheet.Cells[row, 21].Value = "Met_AYP_R_Native_Hawaiian";
        worksheet.Cells[row, 22].Value = "%_AYP_R_Native_Hawaiian";

        worksheet.Cells[row, 23].Value = "Part_AYP_R_White";
        worksheet.Cells[row, 24].Value = "Met_AYP_R_White";
        worksheet.Cells[row, 25].Value = "%_AYP_R_White";

        worksheet.Cells[row, 26].Value = "Part_AYP_R_TwoOrMore";
        worksheet.Cells[row, 27].Value = "Met_AYP_R_TwoOrMore";
        worksheet.Cells[row, 28].Value = "%_AYP_R_TwoOrMore";

        worksheet.Cells[row, 29].Value = "Part_AYP_R_EconDis";
        worksheet.Cells[row, 30].Value = "Met_AYP_R_EconDis";
        worksheet.Cells[row, 31].Value = "%_AYP_R_EconDis";

        worksheet.Cells[row, 32].Value = "Part_AYP_R_EL";
        worksheet.Cells[row, 33].Value = "Met_AYP_R_EL";
        worksheet.Cells[row, 34].Value = "%_AYP_R_EL";

        worksheet.Cells[row, 35].Value = "Part_AYP_M_All";
        worksheet.Cells[row, 36].Value = "Met_AYP_M_All";
        worksheet.Cells[row, 37].Value = "%_AYP_M_All";

        worksheet.Cells[row, 38].Value = "Part_AYP_M_Amer_Ind";
        worksheet.Cells[row, 39].Value = "Met_AYP_M_Amer_Ind";
        worksheet.Cells[row, 40].Value = "%_AYP_M_Amer_Ind";

        worksheet.Cells[row, 41].Value = "Part_AYP_M_Asian";
        worksheet.Cells[row, 42].Value = "Met_AYP_M_Asian";
        worksheet.Cells[row, 43].Value = "%_AYP_M_Asian";

        worksheet.Cells[row, 44].Value = "Part_AYP_M_Black";
        worksheet.Cells[row, 45].Value = "Met_AYP_M_Black";
        worksheet.Cells[row, 46].Value = "%_AYP_M_Black";

        worksheet.Cells[row, 47].Value = "Part_AYP_M_Hispanic";
        worksheet.Cells[row, 48].Value = "Met_AYP_M_Hispanic";
        worksheet.Cells[row, 49].Value = "%_AYP_M_Hispanic";

        worksheet.Cells[row, 50].Value = "Part_AYP_M_Native_Hawaii";
        worksheet.Cells[row, 51].Value = "Met_AYP_M_Native_Hawaii";
        worksheet.Cells[row, 52].Value = "%_AYP_M_Native_Hawaii";

        worksheet.Cells[row, 53].Value = "Part_AYP_M_White";
        worksheet.Cells[row, 54].Value = "Met_AYP_M_White";
        worksheet.Cells[row, 55].Value = "%_AYP_M_White";

        worksheet.Cells[row, 56].Value = "Part_AYP_M_TwoOrMore";
        worksheet.Cells[row, 57].Value = "Met_AYP_M_TwoOrMore";
        worksheet.Cells[row, 58].Value = "%_AYP_M_TwoOrMore";

        worksheet.Cells[row, 59].Value = "Part_AYP_M_EconDis";
        worksheet.Cells[row, 60].Value = "Met_AYP_M_EconDis";
        worksheet.Cells[row, 61].Value = "%_AYP_M_EconDis";

        worksheet.Cells[row, 62].Value = "Part_AYP_M_EL";
        worksheet.Cells[row, 63].Value = "Met_AYP_M_EL";
        worksheet.Cells[row, 64].Value = "%_AYP_M_EL";

            worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:BL1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x => x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                row++;

                if (isAlternative)
                {
                    using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                    {
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                    }

                }

                //worksheet.Cells[row, 1].Value = report.ID;
                worksheet.Cells[row, 1].Value = report.GranteeID;
                worksheet.Cells[row, 2].Value = grantee.GranteeName;
                worksheet.Cells[row, 3].Value = school.SchoolCode;
                worksheet.Cells[row, 4].Value = school.SchoolName;

                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                    MagnetGPRA item = gpraData[0];
                    var participationReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 3);
                    if (participationReading.Count > 0) //Part_AYP_R_
                    {
                        if (participationReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 5].Value = participationReading[0].Total;
                            gpralist[5] = Convert.ToDouble(gpralist[5]) + Convert.ToDouble(worksheet.Cells[row, 5].Value);
                        }
                        if (participationReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 8].Value = participationReading[0].Indian;
                            gpralist[8] = Convert.ToDouble(gpralist[8]) + Convert.ToDouble(worksheet.Cells[row, 8].Value);
                        }
                        if (participationReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 11].Value = participationReading[0].Asian;
                            gpralist[11] = Convert.ToDouble(gpralist[11]) + Convert.ToDouble(worksheet.Cells[row, 11].Value);
                        }
                        if (participationReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 14].Value = participationReading[0].Black;
                            gpralist[14] = Convert.ToDouble(gpralist[14]) + Convert.ToDouble(worksheet.Cells[row, 14].Value);
                        }

                        if (participationReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 17].Value = participationReading[0].Hispanic;
                            gpralist[17] = Convert.ToDouble(gpralist[17]) + Convert.ToDouble(worksheet.Cells[row, 17].Value);
                        }
                        if (participationReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 20].Value = participationReading[0].Hawaiian;
                            gpralist[20] = Convert.ToDouble(gpralist[20]) + Convert.ToDouble(worksheet.Cells[row, 20].Value);
                        }
                        if (participationReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 23].Value = participationReading[0].White;
                            gpralist[23] = Convert.ToDouble(gpralist[23]) + Convert.ToDouble(worksheet.Cells[row, 23].Value);
                        }
                        if (participationReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 26].Value = participationReading[0].MultiRaces;
                            gpralist[26] = Convert.ToDouble(gpralist[26]) + Convert.ToDouble(worksheet.Cells[row, 26].Value);
                        }
                        if (participationReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 29].Value = participationReading[0].ExtraFiled2;//_econnDis
                            gpralist[29] = Convert.ToDouble(gpralist[29]) + Convert.ToDouble(worksheet.Cells[row, 29].Value);
                        }

                        if (participationReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 32].Value = participationReading[0].ExtraFiled1;   //Par_AYP_R_EL
                            gpralist[32] = Convert.ToDouble(gpralist[32]) + Convert.ToDouble(worksheet.Cells[row, 32].Value);
                        }
                    }
                    var achievementReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 5);
                    if (achievementReading.Count > 0) //met_AYP_R
                    {
                        if (achievementReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 6].Value = achievementReading[0].Total;
                            gpralist[6] = Convert.ToDouble(gpralist[6]) + Convert.ToDouble(worksheet.Cells[row, 6].Value);
                        }
                        if (achievementReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 9].Value = achievementReading[0].Indian;
                            gpralist[9] = Convert.ToDouble(gpralist[9]) + Convert.ToDouble(worksheet.Cells[row, 9].Value);
                        }
                        if (achievementReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 12].Value = achievementReading[0].Asian;
                            gpralist[12] = Convert.ToDouble(gpralist[12]) + Convert.ToDouble(worksheet.Cells[row, 12].Value);
                        }
                        if (achievementReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 15].Value = achievementReading[0].Black;
                            gpralist[15] = Convert.ToDouble(gpralist[15]) + Convert.ToDouble(worksheet.Cells[row, 15].Value);
                        }
                        if (achievementReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 18].Value = achievementReading[0].Hispanic;
                            gpralist[18] = Convert.ToDouble(gpralist[18]) + Convert.ToDouble(worksheet.Cells[row, 18].Value);
                        }
                        if (achievementReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 21].Value = achievementReading[0].Hawaiian;
                            gpralist[21] = Convert.ToDouble(gpralist[21]) + Convert.ToDouble(worksheet.Cells[row, 21].Value);
                        }
                        if (achievementReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 24].Value = achievementReading[0].White;
                            gpralist[24] = Convert.ToDouble(gpralist[24]) + Convert.ToDouble(worksheet.Cells[row, 24].Value);
                        }
                        if (achievementReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 27].Value = achievementReading[0].MultiRaces;
                            gpralist[27] = Convert.ToDouble(gpralist[27]) + Convert.ToDouble(worksheet.Cells[row, 27].Value);
                        }
                        if (achievementReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 30].Value = achievementReading[0].ExtraFiled2; //met_AYP_R_EconDis
                            gpralist[30] = Convert.ToDouble(gpralist[30]) + Convert.ToDouble(worksheet.Cells[row, 30].Value);
                        }
                        if (achievementReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 33].Value = achievementReading[0].ExtraFiled1; //met_AYP_R_EL
                            gpralist[33] = Convert.ToDouble(gpralist[33]) + Convert.ToDouble(worksheet.Cells[row, 33].Value);
                        }
                    }
                    if (participationReading.Count > 0 && achievementReading.Count > 0)
                    {
                        if (participationReading[0].Total > 0 && achievementReading[0].Total >= 0)
                        {
                            worksheet.Cells[row, 7].Value = Convert.ToDouble(achievementReading[0].Total) / participationReading[0].Total;
                            worksheet.Cells[row, 7].Style.Numberformat.Format = "#0.00%";
                            gpralist[7] = Convert.ToDouble(gpralist[7]) + Convert.ToDouble(worksheet.Cells[row, 7].Value);
                        }

                        if (participationReading[0].Indian > 0 && achievementReading[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 10].Value = Convert.ToDouble(achievementReading[0].Indian) / participationReading[0].Indian;
                            worksheet.Cells[row, 10].Style.Numberformat.Format = "#0.00%";
                            gpralist[10] = Convert.ToDouble(gpralist[10]) + Convert.ToDouble(worksheet.Cells[row, 10].Value);
                        }
                        if (participationReading[0].Asian > 0 && achievementReading[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 13].Value = Convert.ToDouble(achievementReading[0].Asian) / participationReading[0].Asian;
                            worksheet.Cells[row, 13].Style.Numberformat.Format = "#0.00%";
                            gpralist[13] = Convert.ToDouble(gpralist[13]) + Convert.ToDouble(worksheet.Cells[row, 13].Value);
                        }
                        if (participationReading[0].Black > 0 && achievementReading[0].Black >= 0)
                        {
                            worksheet.Cells[row, 16].Value = Convert.ToDouble(achievementReading[0].Black) / participationReading[0].Black;
                            worksheet.Cells[row, 16].Style.Numberformat.Format = "#0.00%";
                            gpralist[16] = Convert.ToDouble(gpralist[16]) + Convert.ToDouble(worksheet.Cells[row, 16].Value);
                        }
                        if (participationReading[0].Hispanic > 0 && achievementReading[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 19].Value = Convert.ToDouble(achievementReading[0].Hispanic) / participationReading[0].Hispanic;
                            worksheet.Cells[row, 19].Style.Numberformat.Format = "#0.00%";
                            gpralist[19] = Convert.ToDouble(gpralist[19]) + Convert.ToDouble(worksheet.Cells[row, 19].Value);

                        }
                        if (participationReading[0].Hawaiian > 0 && achievementReading[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 22].Value = Convert.ToDouble(achievementReading[0].Hawaiian) / participationReading[0].Hawaiian;
                            worksheet.Cells[row, 22].Style.Numberformat.Format = "#0.00%";
                            gpralist[22] = Convert.ToDouble(gpralist[22]) + Convert.ToDouble(worksheet.Cells[row, 22].Value);
                        }
                        if (participationReading[0].White > 0 && achievementReading[0].White >= 0)
                        {
                            worksheet.Cells[row, 25].Value = Convert.ToDouble(achievementReading[0].White) / participationReading[0].White;
                            worksheet.Cells[row, 25].Style.Numberformat.Format = "#0.00%";
                            gpralist[25] = Convert.ToDouble(gpralist[25]) + Convert.ToDouble(worksheet.Cells[row, 25].Value);
                        }
                        if (participationReading[0].MultiRaces > 0 && achievementReading[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 28].Value = Convert.ToDouble(achievementReading[0].MultiRaces) / participationReading[0].MultiRaces;
                            worksheet.Cells[row, 28].Style.Numberformat.Format = "#0.00%";
                            gpralist[28] = Convert.ToDouble(gpralist[28]) + Convert.ToDouble(worksheet.Cells[row, 28].Value);
                        }
                        if (participationReading[0].ExtraFiled2 > 0 && achievementReading[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 31].Value = Convert.ToDouble(achievementReading[0].ExtraFiled2) / participationReading[0].ExtraFiled2;
                            worksheet.Cells[row, 31].Style.Numberformat.Format = "#0.00%";
                            gpralist[31] = Convert.ToDouble(gpralist[31]) + Convert.ToDouble(worksheet.Cells[row, 31].Value);
                        }
                        if (participationReading[0].ExtraFiled1 > 0 && achievementReading[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 34].Value = Convert.ToDouble(achievementReading[0].ExtraFiled1) / participationReading[0].ExtraFiled1;
                            worksheet.Cells[row, 34].Style.Numberformat.Format = "#0.00%";
                            gpralist[34] = Convert.ToDouble(gpralist[34]) + Convert.ToDouble(worksheet.Cells[row, 34].Value);
                        }
                    }

                    ////////////////////////////////////////////////////////////////////
                    var participationMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 4);
                    if (participationMath.Count > 0) //Part_AYP_M
                    {
                        if (participationMath[0].Total >= 0)
                        {
                            worksheet.Cells[row, 35].Value = participationMath[0].Total;
                            gpralist[35] = Convert.ToDouble(gpralist[35]) + Convert.ToDouble(worksheet.Cells[row, 35].Value);
                        }
                        if (participationMath[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 38].Value = participationMath[0].Indian;
                            gpralist[38] = Convert.ToDouble(gpralist[38]) + Convert.ToDouble(worksheet.Cells[row, 38].Value);
                        }
                        if (participationMath[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 41].Value = participationMath[0].Asian;
                            gpralist[41] = Convert.ToDouble(gpralist[41]) + Convert.ToDouble(worksheet.Cells[row, 41].Value);
                        }
                        if (participationMath[0].Black >= 0)
                        {
                            worksheet.Cells[row, 44].Value = participationMath[0].Black;
                            gpralist[44] = Convert.ToDouble(gpralist[44]) + Convert.ToDouble(worksheet.Cells[row, 44].Value);
                        }
                        if (participationMath[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 47].Value = participationMath[0].Hispanic;
                            gpralist[47] = Convert.ToDouble(gpralist[47]) + Convert.ToDouble(worksheet.Cells[row, 47].Value);
                        }
                        if (participationMath[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 50].Value = participationMath[0].Hawaiian;
                            gpralist[50] = Convert.ToDouble(gpralist[50]) + Convert.ToDouble(worksheet.Cells[row, 50].Value);
                        }
                        if (participationMath[0].White >= 0)
                        {
                            worksheet.Cells[row, 53].Value = participationMath[0].White;
                            gpralist[53] = Convert.ToDouble(gpralist[53]) + Convert.ToDouble(worksheet.Cells[row, 53].Value);
                        }
                        if (participationMath[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 56].Value = participationMath[0].MultiRaces;
                            gpralist[56] = Convert.ToDouble(gpralist[56]) + Convert.ToDouble(worksheet.Cells[row, 56].Value);
                        }
                        if (participationMath[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 59].Value = participationMath[0].ExtraFiled2;
                            gpralist[59] = Convert.ToDouble(gpralist[59]) + Convert.ToDouble(worksheet.Cells[row, 59].Value);
                        }
                        if (participationMath[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 62].Value = participationMath[0].ExtraFiled1;
                            gpralist[62] = Convert.ToDouble(gpralist[62]) + Convert.ToDouble(worksheet.Cells[row, 62].Value);
                        }
                    }

                    var achievementMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 6);
                    if (achievementMath.Count > 0) //Met_AYP_M_
                    {
                        if (achievementMath[0].Total >= 0)
                        {
                            worksheet.Cells[row, 36].Value = achievementMath[0].Total;
                            gpralist[36] = Convert.ToDouble(gpralist[36]) + Convert.ToDouble(worksheet.Cells[row, 36].Value);
                        }
                        if (achievementMath[0].Indian >= 0)
                        {
                            worksheet.Cells[row, 39].Value = achievementMath[0].Indian;
                            gpralist[39] = Convert.ToDouble(gpralist[39]) + Convert.ToDouble(worksheet.Cells[row, 39].Value);
                        }
                        if (achievementMath[0].Asian >= 0)
                        {
                            worksheet.Cells[row, 42].Value = achievementMath[0].Asian;
                            gpralist[42] = Convert.ToDouble(gpralist[42]) + Convert.ToDouble(worksheet.Cells[row, 42].Value);
                        }
                        if (achievementMath[0].Black >= 0)
                        {
                            worksheet.Cells[row, 45].Value = achievementMath[0].Black;
                            gpralist[45] = Convert.ToDouble(gpralist[45]) + Convert.ToDouble(worksheet.Cells[row, 45].Value);
                        }
                        if (achievementMath[0].Hispanic >= 0)
                        {
                            worksheet.Cells[row, 48].Value = achievementMath[0].Hispanic;
                            gpralist[48] = Convert.ToDouble(gpralist[48]) + Convert.ToDouble(worksheet.Cells[row, 48].Value);
                        }
                        if (achievementMath[0].Hawaiian >= 0)
                        {
                            worksheet.Cells[row, 51].Value = achievementMath[0].Hawaiian;
                            gpralist[51] = Convert.ToDouble(gpralist[51]) + Convert.ToDouble(worksheet.Cells[row, 51].Value);
                        }
                        if (achievementMath[0].White >= 0)
                        {
                            worksheet.Cells[row, 54].Value = achievementMath[0].White;
                            gpralist[54] = Convert.ToDouble(gpralist[54]) + Convert.ToDouble(worksheet.Cells[row, 54].Value);
                        }
                        if (achievementMath[0].MultiRaces >= 0)
                        {
                            worksheet.Cells[row, 57].Value = achievementMath[0].MultiRaces;
                            gpralist[57] = Convert.ToDouble(gpralist[57]) + Convert.ToDouble(worksheet.Cells[row, 57].Value);
                        }
                        if (achievementMath[0].ExtraFiled2 >= 0)
                        {
                            worksheet.Cells[row, 60].Value = achievementMath[0].ExtraFiled2;
                            gpralist[60] = Convert.ToDouble(gpralist[60]) + Convert.ToDouble(worksheet.Cells[row, 60].Value);
                        }
                        if (achievementMath[0].ExtraFiled1 >= 0)
                        {
                            worksheet.Cells[row, 63].Value = achievementMath[0].ExtraFiled1;
                            gpralist[63] = Convert.ToDouble(gpralist[63]) + Convert.ToDouble(worksheet.Cells[row, 63].Value);
                        }
                    }
                    if (achievementMath.Count > 0 && participationMath.Count > 0)
                    {
                        if (achievementMath[0].Total >= 0 && participationMath[0].Total > 0)
                        {
                            worksheet.Cells[row, 37].Value = Convert.ToDouble(achievementMath[0].Total) / participationMath[0].Total;
                            worksheet.Cells[row, 37].Style.Numberformat.Format = "#0.00%";
                            gpralist[37] = Convert.ToDouble(gpralist[37]) + Convert.ToDouble(worksheet.Cells[row, 37].Value);

                        }
                        if (achievementMath[0].Indian >= 0 && participationMath[0].Indian > 0)
                        {
                            worksheet.Cells[row, 40].Value = Convert.ToDouble(achievementMath[0].Indian) / participationMath[0].Indian;
                            worksheet.Cells[row, 40].Style.Numberformat.Format = "#0.00%";
                            gpralist[40] = Convert.ToDouble(gpralist[40]) + Convert.ToDouble(worksheet.Cells[row, 40].Value);
                        }
                        if (achievementMath[0].Asian >= 0 && participationMath[0].Asian > 0)
                        {
                            worksheet.Cells[row, 43].Value = Convert.ToDouble(achievementMath[0].Asian) / participationMath[0].Asian;
                            worksheet.Cells[row, 43].Style.Numberformat.Format = "#0.00%";
                            gpralist[43] = Convert.ToDouble(gpralist[43]) + Convert.ToDouble(worksheet.Cells[row, 43].Value);
                        }
                        if (achievementMath[0].Black >= 0 && participationMath[0].Black > 0)
                        {
                            worksheet.Cells[row, 46].Value = Convert.ToDouble(achievementMath[0].Black) / participationMath[0].Black;
                            worksheet.Cells[row, 46].Style.Numberformat.Format = "#0.00%";
                            gpralist[46] = Convert.ToDouble(gpralist[46]) + Convert.ToDouble(worksheet.Cells[row, 46].Value);
                        }

                        if (achievementMath[0].Hispanic >= 0 && participationMath[0].Hispanic > 0)
                        {
                            worksheet.Cells[row, 49].Value = Convert.ToDouble(achievementMath[0].Hispanic) / participationMath[0].Hispanic;
                            worksheet.Cells[row, 49].Style.Numberformat.Format = "#0.00%";
                            gpralist[49] = Convert.ToDouble(gpralist[49]) + Convert.ToDouble(worksheet.Cells[row, 49].Value);
                        }
                        if (achievementMath[0].Hawaiian >= 0 && participationMath[0].Hawaiian > 0)
                        {
                            worksheet.Cells[row, 52].Value = Convert.ToDouble(achievementMath[0].Hawaiian) / participationMath[0].Hawaiian;
                            worksheet.Cells[row, 52].Style.Numberformat.Format = "#0.00%";
                            gpralist[52] = Convert.ToDouble(gpralist[52]) + Convert.ToDouble(worksheet.Cells[row, 52].Value);
                        }
                        if (achievementMath[0].White >= 0 && participationMath[0].White > 0)
                        {
                            worksheet.Cells[row, 55].Value = Convert.ToDouble(achievementMath[0].White) / participationMath[0].White;
                            worksheet.Cells[row, 55].Style.Numberformat.Format = "#0.00%";
                            gpralist[55] = Convert.ToDouble(gpralist[55]) + Convert.ToDouble(worksheet.Cells[row, 55].Value);
                        }
                        if (achievementMath[0].MultiRaces >= 0 && participationMath[0].MultiRaces > 0)
                        {
                            worksheet.Cells[row, 58].Value = Convert.ToDouble(achievementMath[0].MultiRaces) / participationMath[0].MultiRaces;
                            worksheet.Cells[row, 58].Style.Numberformat.Format = "#0.00%";
                            gpralist[58] = Convert.ToDouble(gpralist[58]) + Convert.ToDouble(worksheet.Cells[row, 58].Value);
                        }

                        if (achievementMath[0].ExtraFiled2 >= 0 && participationMath[0].ExtraFiled2 > 0)
                        {
                            worksheet.Cells[row, 61].Value = Convert.ToDouble(achievementMath[0].ExtraFiled2) / participationMath[0].ExtraFiled2;
                            worksheet.Cells[row, 61].Style.Numberformat.Format = "#0.00%";
                            gpralist[61] = Convert.ToDouble(gpralist[61]) + Convert.ToDouble(worksheet.Cells[row, 61].Value);
                        }
                        if (achievementMath[0].ExtraFiled1 >= 0 && participationMath[0].ExtraFiled1 > 0)
                        {
                            worksheet.Cells[row, 64].Value = Convert.ToDouble(achievementMath[0].ExtraFiled1) / participationMath[0].ExtraFiled1;
                            worksheet.Cells[row, 64].Style.Numberformat.Format = "#0.00%";
                            gpralist[64] = Convert.ToDouble(gpralist[64]) + Convert.ToDouble(worksheet.Cells[row, 64].Value);
                        }
                    }

                }  //end foreach
                else
                {
                    worksheet.Cells[row, 5].Value = "missing";
                    worksheet.Cells[row, 6].Value = "missing";
                    worksheet.Cells[row, 7].Value = "missing";
                    worksheet.Cells[row, 8].Value = "missing";
                    worksheet.Cells[row, 9].Value = "missing";
                    worksheet.Cells[row, 10].Value = "missing";
                    worksheet.Cells[row, 11].Value = "missing";
                    worksheet.Cells[row, 12].Value = "missing";
                    worksheet.Cells[row, 13].Value = "missing";
                    worksheet.Cells[row, 14].Value = "missing";
                    worksheet.Cells[row, 15].Value = "missing";
                    worksheet.Cells[row, 16].Value = "missing";
                    worksheet.Cells[row, 17].Value = "missing";
                    worksheet.Cells[row, 18].Value = "missing";
                    worksheet.Cells[row, 19].Value = "missing";
                    worksheet.Cells[row, 20].Value = "missing";
                    worksheet.Cells[row, 21].Value = "missing";
                    worksheet.Cells[row, 22].Value = "missing";
                    worksheet.Cells[row, 23].Value = "missing";
                    worksheet.Cells[row, 24].Value = "missing";
                    worksheet.Cells[row, 25].Value = "missing";
                    worksheet.Cells[row, 26].Value = "missing";
                    worksheet.Cells[row, 27].Value = "missing";
                    worksheet.Cells[row, 28].Value = "missing";
                    worksheet.Cells[row, 29].Value = "missing";
                    worksheet.Cells[row, 30].Value = "missing";
                    worksheet.Cells[row, 31].Value = "missing";
                    worksheet.Cells[row, 32].Value = "missing";
                    worksheet.Cells[row, 33].Value = "missing";
                    worksheet.Cells[row, 34].Value = "missing";
                    worksheet.Cells[row, 35].Value = "missing";
                    worksheet.Cells[row, 36].Value = "missing";
                    worksheet.Cells[row, 37].Value = "missing";
                    worksheet.Cells[row, 38].Value = "missing";

                    worksheet.Cells[row, 39].Value = "missing";
                    worksheet.Cells[row, 40].Value = "missing";
                    worksheet.Cells[row, 41].Value = "missing";
                    worksheet.Cells[row, 42].Value = "missing";
                    worksheet.Cells[row, 43].Value = "missing";
                    worksheet.Cells[row, 44].Value = "missing";
                    worksheet.Cells[row, 45].Value = "missing";
                    worksheet.Cells[row, 46].Value = "missing";
                    worksheet.Cells[row, 47].Value = "missing";
                    worksheet.Cells[row, 48].Value = "missing";
                    worksheet.Cells[row, 49].Value = "missing";
                    worksheet.Cells[row, 50].Value = "missing";
                    worksheet.Cells[row, 51].Value = "missing";
                    worksheet.Cells[row, 52].Value = "missing";
                    worksheet.Cells[row, 53].Value = "missing";
                    worksheet.Cells[row, 54].Value = "missing";
                    worksheet.Cells[row, 55].Value = "missing";
                    worksheet.Cells[row, 56].Value = "missing";
                    worksheet.Cells[row, 57].Value = "missing";
                    worksheet.Cells[row, 58].Value = "missing";
                    worksheet.Cells[row, 59].Value = "missing";
                    worksheet.Cells[row, 60].Value = "missing";
                    worksheet.Cells[row, 61].Value = "missing";
                    worksheet.Cells[row, 62].Value = "missing";
                    worksheet.Cells[row, 63].Value = "missing";
                    worksheet.Cells[row, 64].Value = "missing";
                }

            }
        }
    }

    public static void GetGPRA4(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
         int TotalSchools = 0;
        int granteeID = -1;
        bool isAlternative = false;

        Hashtable gpralist = new Hashtable();
        gpralist.Add(5, 0);
        gpralist.Add(6, 0);

        worksheet.Name = "GPRA IV";          //114 -- 115

        worksheet.Cells[row, 1].Value = "GranteeID";
        worksheet.Cells[row, 2].Value = "Grantee_Name";
        worksheet.Cells[row, 3].Value = "SchoolID";
        worksheet.Cells[row, 4].Value = "School_Name";

        worksheet.Cells[row, 5].Value = "MSAP_Funds";
        worksheet.Cells[row, 6].Value = "Studs_Served";

        worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:F1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x => x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                row++;

                if (isAlternative)
                {
                    using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                    {
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                    }

                }

                //worksheet.Cells[row, 1].Value = report.ID;
                worksheet.Cells[row, 1].Value = report.GranteeID;
                worksheet.Cells[row, 2].Value = grantee.GranteeName;
                worksheet.Cells[row, 3].Value = school.SchoolCode;
                worksheet.Cells[row, 4].Value = school.SchoolName;

                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                    MagnetGPRA item = gpraData[0];
                    var performanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 7);
                    if (performanceMeasure.Count > 0)
                    {
                        if (performanceMeasure[0].Budget != null)
                        {
                            TotalSchools++;
                            worksheet.Cells[row, 5].Value = Convert.ToDouble(performanceMeasure[0].Budget);
                            worksheet.Cells[row, 5].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";
                        }
                        worksheet.Cells[row, 6].Value = performanceMeasure[0].Total; //Studs_Served
                        gpralist[5] = Convert.ToDouble(gpralist[5]) + Convert.ToDouble(worksheet.Cells[row, 5].Value);
                        gpralist[6] = Convert.ToDouble(gpralist[6]) + Convert.ToDouble(worksheet.Cells[row, 6].Value);

                    }
                }
                else
                {
                    worksheet.Cells[row, 5].Value = "missing";
                    worksheet.Cells[row, 6].Value = "missing";
                }
            }
        }
    }

    public static void GetGPRA5(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
          int TotalSchools = 0;
        int granteeID = -1;
        bool isAlternative = false;

         Hashtable gpralist = new Hashtable();

        gpralist.Add(5, 0);
        gpralist.Add(6, 0);
        gpralist.Add(7, 0);
        gpralist.Add(8, 0);
        gpralist.Add(9, 0);
        gpralist.Add(10, 0);
        gpralist.Add(11, 0);
        gpralist.Add(12, 0);
        gpralist.Add(13, 0);
        gpralist.Add(14, 0);
        gpralist.Add(15, 0);
        gpralist.Add(16, 0);
        gpralist.Add(17, 0);
        gpralist.Add(18, 0);
        gpralist.Add(19, 0);
        gpralist.Add(20, 0);
        gpralist.Add(21, 0);
        gpralist.Add(22, 0);
        gpralist.Add(23, 0);
        gpralist.Add(24, 0);
        gpralist.Add(25, 0);
        gpralist.Add(26, 0);
        gpralist.Add(27, 0);
        gpralist.Add(28, 0);
        gpralist.Add(29, 0);
        gpralist.Add(30, 0);
        gpralist.Add(31, 0);
        gpralist.Add(32, 0);
        gpralist.Add(33, 0);
        gpralist.Add(34, 0);

        worksheet.Name = "GPRA V";          //116 --145

        worksheet.Cells[row, 1].Value = "GranteeID";
        worksheet.Cells[row, 2].Value = "Grantee_Name";
        worksheet.Cells[row, 3].Value = "SchoolID";
        worksheet.Cells[row, 4].Value = "School_Name";

        worksheet.Cells[row, 5].Value = "4yr_Amer_Indian";
        worksheet.Cells[row, 6].Value = "4yr_Asian";
        worksheet.Cells[row, 7].Value = "4yr_Black";
        worksheet.Cells[row, 8].Value = "4yr_Hispanic";
        worksheet.Cells[row, 9].Value = "4yr_Native_Hawaiian";
        worksheet.Cells[row, 10].Value = "4yr_White";
        worksheet.Cells[row, 11].Value = "4yr_Two_more";
        worksheet.Cells[row, 12].Value = "4yr_Cohort_Tot";
        worksheet.Cells[row, 13].Value = "Grad_Amer_Indian";
        worksheet.Cells[row, 14].Value = "Grad_Asian";
        worksheet.Cells[row, 15].Value = "Grad_Black";
        worksheet.Cells[row, 16].Value = "Grad_Hispanic";
        worksheet.Cells[row, 17].Value = "Grad_Native_Hawaiian";
        worksheet.Cells[row, 18].Value = "Grad_White";
        worksheet.Cells[row, 19].Value = "Grad_Two_more";
        worksheet.Cells[row, 20].Value = "Grad_Tot";
        worksheet.Cells[row, 21].Value = "%Grad_Amer_Indian";
        worksheet.Cells[row, 22].Value = "%Grad_Asian";
        worksheet.Cells[row, 23].Value = "%Grad_Black";
        worksheet.Cells[row, 24].Value = "%Grad_Hispanic";
        worksheet.Cells[row, 25].Value = "%Grad_Native_Hawaiian";
        worksheet.Cells[row, 26].Value = "%Grad_White";
        worksheet.Cells[row, 27].Value = "%Grad_Two_more";
        worksheet.Cells[row, 28].Value = "%Grad_Tot";
        worksheet.Cells[row, 29].Value = "4Yr_EconDis";
        worksheet.Cells[row, 30].Value = "Grad_EconDis";
        worksheet.Cells[row, 31].Value = "%Grad_EconDis";
        worksheet.Cells[row, 32].Value = "4yr_EL";
        worksheet.Cells[row, 33].Value = "Grad_EL";
        worksheet.Cells[row, 34].Value = "%Grad_EL";

         worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:AH1"])   //format title
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x => x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                row++;

                if (isAlternative)
                {
                    using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                    {
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                    }

                }
                //worksheet.Cells[row, 1].Value = report.ID;
                worksheet.Cells[row, 1].Value = report.GranteeID;
                worksheet.Cells[row, 2].Value = grantee.GranteeName;
                worksheet.Cells[row, 3].Value = school.SchoolCode;
                worksheet.Cells[row, 4].Value = school.SchoolName;
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                    MagnetGPRA item = gpraData[0];
                    var gprav = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == item.ID && x.ReportType == 8);
                    if (gprav.Count > 0)
                    {
                        if (gprav[0].AmericanIndiancohort != null)
                        {
                            worksheet.Cells[row, 5].Value = gprav[0].AmericanIndiancohort.ToString();
                            gpralist[5] = Convert.ToDouble(gpralist[5]) + Convert.ToDouble(worksheet.Cells[row, 5].Value);
                        }
                        else
                            worksheet.Cells[row, 5].Value = "missing";

                        if (gprav[0].Asiancohort != null)
                        {
                            worksheet.Cells[row, 6].Value = gprav[0].Asiancohort.ToString();
                            gpralist[6] = Convert.ToDouble(gpralist[6]) + Convert.ToDouble(worksheet.Cells[row, 6].Value);
                        }
                        else
                            worksheet.Cells[row, 6].Value = "missing";

                        if (gprav[0].AfricanAmericancohort != null)
                        {
                            worksheet.Cells[row, 7].Value = gprav[0].AfricanAmericancohort.ToString();
                            gpralist[7] = Convert.ToDouble(gpralist[7]) + Convert.ToDouble(worksheet.Cells[row, 7].Value);
                        }
                        else
                            worksheet.Cells[row, 7].Value = "missing";

                        if (gprav[0].HispanicLatinocohort != null)
                        {
                            worksheet.Cells[row, 8].Value = gprav[0].HispanicLatinocohort.ToString();
                            gpralist[8] = Convert.ToDouble(gpralist[8]) + Convert.ToDouble(worksheet.Cells[row, 8].Value);
                        }
                        else
                            worksheet.Cells[row, 8].Value = "missing";

                        if (gprav[0].NativeHawaiiancohort != null)
                        {
                            worksheet.Cells[row, 9].Value = gprav[0].NativeHawaiiancohort.ToString();
                            gpralist[9] = Convert.ToDouble(gpralist[9]) + Convert.ToDouble(worksheet.Cells[row, 9].Value);
                        }
                        else
                            worksheet.Cells[row, 9].Value = "missing";

                        if (gprav[0].Whitecohort != null)
                        {
                            worksheet.Cells[row, 10].Value = gprav[0].Whitecohort.ToString();
                            gpralist[10] = Convert.ToDouble(gpralist[10]) + Convert.ToDouble(worksheet.Cells[row, 10].Value);
                        }
                        else
                            worksheet.Cells[row, 10].Value = "missing";


                        if (gprav[0].MoreRacescohort != null)
                        {
                            worksheet.Cells[row, 11].Value = gprav[0].MoreRacescohort.ToString();
                            gpralist[11] = Convert.ToDouble(gpralist[11]) + Convert.ToDouble(worksheet.Cells[row, 11].Value);

                        }
                        else
                            worksheet.Cells[row, 11].Value = "missing";

                        if (gprav[0].Allcohort != null)
                        {
                            worksheet.Cells[row, 12].Value = gprav[0].Allcohort.ToString();
                            gpralist[12] = Convert.ToDouble(gpralist[12]) + Convert.ToDouble(worksheet.Cells[row, 12].Value);
                        }
                        else
                            worksheet.Cells[row, 12].Value = "missing";

                        if (gprav[0].AmericanIndiangraduated != null)
                        {
                            worksheet.Cells[row, 13].Value = gprav[0].AmericanIndiangraduated.ToString();
                            gpralist[13] = Convert.ToDouble(gpralist[13]) + Convert.ToDouble(worksheet.Cells[row, 13].Value);
                        }
                        else
                            worksheet.Cells[row, 13].Value = "missing";

                        if (gprav[0].Asiangraduated != null)
                        {
                            worksheet.Cells[row, 14].Value = gprav[0].Asiangraduated.ToString();
                            gpralist[14] = Convert.ToDouble(gpralist[14]) + Convert.ToDouble(worksheet.Cells[row, 14].Value);
                        }
                        else
                            worksheet.Cells[row, 14].Value = "missing";

                        if (gprav[0].AfricanAmericangraduated != null)
                        {
                            worksheet.Cells[row, 15].Value = gprav[0].AfricanAmericangraduated.ToString();
                            gpralist[15] = Convert.ToDouble(gpralist[15]) + Convert.ToDouble(worksheet.Cells[row, 15].Value);
                        }
                        else
                            worksheet.Cells[row, 15].Value = "missing";

                        if (gprav[0].HispanicLatinograduated != null)
                        {
                            worksheet.Cells[row, 16].Value = gprav[0].HispanicLatinograduated.ToString();
                            gpralist[16] = Convert.ToDouble(gpralist[16]) + Convert.ToDouble(worksheet.Cells[row, 16].Value);
                        }
                        else
                            worksheet.Cells[row, 16].Value = "missing";

                        if (gprav[0].NativeHawaiiangraduated != null)
                        {
                            worksheet.Cells[row, 17].Value = gprav[0].NativeHawaiiangraduated.ToString();
                            gpralist[17] = Convert.ToDouble(gpralist[17]) + Convert.ToDouble(worksheet.Cells[row, 17].Value);
                        }
                        else
                            worksheet.Cells[row, 17].Value = "missing";

                        if (gprav[0].Whitegraduated != null)
                        {
                            worksheet.Cells[row, 18].Value = gprav[0].Whitegraduated.ToString();
                            gpralist[18] = Convert.ToDouble(gpralist[18]) + Convert.ToDouble(worksheet.Cells[row, 18].Value);
                        }
                        else
                            worksheet.Cells[row, 18].Value = "missing";

                        if (gprav[0].MoreRacesgraduated != null)
                        {
                            worksheet.Cells[row, 19].Value = gprav[0].MoreRacesgraduated.ToString();
                            gpralist[19] = Convert.ToDouble(gpralist[19]) + Convert.ToDouble(worksheet.Cells[row, 19].Value);
                        }
                        else
                            worksheet.Cells[row, 19].Value = "missing";


                        if (gprav[0].Allgraduated != null)
                        {
                            worksheet.Cells[row, 20].Value = gprav[0].Allgraduated.ToString();
                            gpralist[20] = Convert.ToDouble(gpralist[20]) + Convert.ToDouble(worksheet.Cells[row, 20].Value);
                        }
                        else
                            worksheet.Cells[row, 20].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].AmericanIndianpercentage))
                        {
                            worksheet.Cells[row, 21].Value = (Convert.ToDouble(gprav[0].AmericanIndianpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[21] = Convert.ToDouble(gpralist[21]) + Convert.ToDouble(worksheet.Cells[row, 21].Value);
                        }
                        else
                            worksheet.Cells[row, 21].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Asianpercentage))
                        {
                            worksheet.Cells[row, 22].Value = (Convert.ToDouble(gprav[0].Asianpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[22] = Convert.ToDouble(gpralist[22]) + Convert.ToDouble(worksheet.Cells[row, 22].Value);
                        }
                        else
                            worksheet.Cells[row, 22].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].AfricanAmericanpercentage))
                        {
                            worksheet.Cells[row, 23].Value = (Convert.ToDouble(gprav[0].AfricanAmericanpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[23] = Convert.ToDouble(gpralist[23]) + Convert.ToDouble(worksheet.Cells[row, 23].Value);
                        }
                        else
                            worksheet.Cells[row, 23].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].HispanicLatinopercentage))
                        {
                            worksheet.Cells[row, 24].Value = (Convert.ToDouble(gprav[0].HispanicLatinopercentage.Replace("%", "")) / 100).ToString();
                            gpralist[24] = Convert.ToDouble(gpralist[24]) + Convert.ToDouble(worksheet.Cells[row, 24].Value);
                        }
                        else
                            worksheet.Cells[row, 24].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].NativeHawaiianpercentage))
                        {
                            worksheet.Cells[row, 25].Value = (Convert.ToDouble(gprav[0].NativeHawaiianpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[25] = Convert.ToDouble(gpralist[25]) + Convert.ToDouble(worksheet.Cells[row, 25].Value);
                        }
                        else
                            worksheet.Cells[row, 25].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Whitepercentage))
                        {
                            worksheet.Cells[row, 26].Value = (Convert.ToDouble(gprav[0].Whitepercentage.Replace("%", "")) / 100).ToString();
                            gpralist[26] = Convert.ToDouble(gpralist[26]) + Convert.ToDouble(worksheet.Cells[row, 26].Value);
                        }
                        else
                            worksheet.Cells[row, 26].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].MoreRacespercentage))
                        {
                            worksheet.Cells[row, 27].Value = (Convert.ToDouble(gprav[0].MoreRacespercentage.Replace("%", "")) / 100).ToString();
                            gpralist[27] = Convert.ToDouble(gpralist[27]) + Convert.ToDouble(worksheet.Cells[row, 27].Value);
                        }
                        else
                            worksheet.Cells[row, 27].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].allpercentage))
                        {
                            worksheet.Cells[row, 28].Value = (Convert.ToDouble(gprav[0].allpercentage.Replace("%", "")) / 100).ToString();
                            gpralist[28] = Convert.ToDouble(gpralist[28]) + Convert.ToDouble(worksheet.Cells[row, 28].Value);
                        }
                        else
                            worksheet.Cells[row, 28].Value = "missing";

                        if (gprav[0].Economicallycohort != null)
                        {
                            worksheet.Cells[row, 29].Value = gprav[0].Economicallycohort.ToString();
                            gpralist[29] = Convert.ToDouble(gpralist[29]) + Convert.ToDouble(worksheet.Cells[row, 29].Value);
                        }
                        else
                            worksheet.Cells[row, 29].Value = "missing";

                        if (gprav[0].Economicallygraduated != null)
                        {
                            worksheet.Cells[row, 30].Value = gprav[0].Economicallygraduated.ToString();
                            gpralist[30] = Convert.ToDouble(gpralist[30]) + Convert.ToDouble(worksheet.Cells[row, 30].Value);
                        }
                        else
                            worksheet.Cells[row, 30].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Economicallypercentage))
                        {
                            worksheet.Cells[row, 31].Value = (Convert.ToDouble(gprav[0].Economicallypercentage.Replace("%", "")) / 100).ToString();
                            gpralist[31] = Convert.ToDouble(gpralist[31]) + Convert.ToDouble(worksheet.Cells[row, 31].Value);
                        }
                        else
                            worksheet.Cells[row, 31].Value = "missing";

                        if (gprav[0].Englishlearnerscohort != null)
                        {
                            worksheet.Cells[row, 32].Value = gprav[0].Englishlearnerscohort.ToString();
                            gpralist[32] = Convert.ToDouble(gpralist[32]) + Convert.ToDouble(worksheet.Cells[row, 32].Value);
                        }
                        else
                            worksheet.Cells[row, 32].Value = "missing";

                        if (gprav[0].Englishlearnersgraduated != null)
                        {
                            worksheet.Cells[row, 33].Value = gprav[0].Englishlearnersgraduated.ToString();
                            gpralist[33] = Convert.ToDouble(gpralist[33]) + Convert.ToDouble(worksheet.Cells[row, 33].Value);
                        }
                        else
                            worksheet.Cells[row, 33].Value = "missing";

                        if (!string.IsNullOrEmpty(gprav[0].Englishlearnerspercentage))
                        {
                            worksheet.Cells[row, 34].Value = (Convert.ToDouble(gprav[0].Englishlearnerspercentage.Replace("%", "")) / 100).ToString();
                            gpralist[34] = Convert.ToDouble(gpralist[34]) + Convert.ToDouble(worksheet.Cells[row, 34].Value);
                        }
                        else
                            worksheet.Cells[row, 34].Value = "missing";
                    }
                    else
                    {
                        worksheet.Cells[row, 5].Value = "missing";
                        worksheet.Cells[row, 6].Value = "missing";
                        worksheet.Cells[row, 7].Value = "missing";
                        worksheet.Cells[row, 8].Value = "missing";
                        worksheet.Cells[row, 9].Value = "missing";
                        worksheet.Cells[row, 10].Value = "missing";
                        worksheet.Cells[row, 11].Value = "missing";
                        worksheet.Cells[row, 12].Value = "missing";
                        worksheet.Cells[row, 13].Value = "missing";
                        worksheet.Cells[row, 14].Value = "missing";
                        worksheet.Cells[row, 15].Value = "missing";
                        worksheet.Cells[row, 16].Value = "missing";
                        worksheet.Cells[row, 17].Value = "missing";
                        worksheet.Cells[row, 18].Value = "missing";
                        worksheet.Cells[row, 19].Value = "missing";
                        worksheet.Cells[row, 20].Value = "missing";
                        worksheet.Cells[row, 21].Value = "missing";
                        worksheet.Cells[row, 22].Value = "missing";
                        worksheet.Cells[row, 23].Value = "missing";
                        worksheet.Cells[row, 24].Value = "missing";
                        worksheet.Cells[row, 25].Value = "missing";
                        worksheet.Cells[row, 26].Value = "missing";
                        worksheet.Cells[row, 27].Value = "missing";
                        worksheet.Cells[row, 28].Value = "missing";
                        worksheet.Cells[row, 29].Value = "missing";
                        worksheet.Cells[row, 30].Value = "missing";
                        worksheet.Cells[row, 31].Value = "missing";
                        worksheet.Cells[row, 32].Value = "missing";
                        worksheet.Cells[row, 33].Value = "missing";
                        worksheet.Cells[row, 34].Value = "missing";
                    }
                } //end for gpraData
                else
                {
                    worksheet.Cells[row, 5].Value = "missing";
                    worksheet.Cells[row, 6].Value = "missing";
                    worksheet.Cells[row, 7].Value = "missing";
                    worksheet.Cells[row, 8].Value = "missing";
                    worksheet.Cells[row, 9].Value = "missing";
                    worksheet.Cells[row, 10].Value = "missing";
                    worksheet.Cells[row, 11].Value = "missing";
                    worksheet.Cells[row, 12].Value = "missing";
                    worksheet.Cells[row, 13].Value = "missing";
                    worksheet.Cells[row, 14].Value = "missing";
                    worksheet.Cells[row, 15].Value = "missing";
                    worksheet.Cells[row, 16].Value = "missing";
                    worksheet.Cells[row, 17].Value = "missing";
                    worksheet.Cells[row, 18].Value = "missing";
                    worksheet.Cells[row, 19].Value = "missing";
                    worksheet.Cells[row, 20].Value = "missing";
                    worksheet.Cells[row, 21].Value = "missing";
                    worksheet.Cells[row, 22].Value = "missing";
                    worksheet.Cells[row, 23].Value = "missing";
                    worksheet.Cells[row, 24].Value = "missing";
                    worksheet.Cells[row, 25].Value = "missing";
                    worksheet.Cells[row, 26].Value = "missing";
                    worksheet.Cells[row, 27].Value = "missing";
                    worksheet.Cells[row, 28].Value = "missing";
                    worksheet.Cells[row, 29].Value = "missing";
                    worksheet.Cells[row, 30].Value = "missing";
                    worksheet.Cells[row, 31].Value = "missing";
                    worksheet.Cells[row, 32].Value = "missing";
                    worksheet.Cells[row, 33].Value = "missing";
                    worksheet.Cells[row, 34].Value = "missing";
                }
            } //end for school
        }
    }

    public static void GetGPRA6(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
        int TotalSchools = 0;
        int granteeID = -1;
        bool isAlternative = false;

        //items 65
        string IndianActualPercentage65 = "", AsianActualPercentage65 = "",
               BlackActualPercentage65 = "", HispanicActualPercentage65 = "",
               NativeActualPercentage65 = "", WhiteActualPercentage65 = "";

        IndianActualPercentage65 = item28_total == 0 ? "" : ((double)(item18_Indian + item26a_newIndian + item27a_ContIndian) /
                                    (item28_total)).ToString("F");
        AsianActualPercentage65 = item28_total == 0 ? "" : ((double)(item19_Asian + item26b_newAsian + item27b_ContAsian) /
                                    (item28_total)).ToString("F");
        BlackActualPercentage65 = item28_total == 0 ? "" : ((double)(item20_Black + item26c_newBlack + item27c_ContBlack) / (item28_total)).ToString("F");
        HispanicActualPercentage65 = item28_total == 0 ? "" : ((double)(item21_Hispanic + item26d_newHispanic + item27d_ContHispanic) / (item28_total)).ToString("F");
        NativeActualPercentage65 = item28_total == 0 ? "" : ((double)(item22_Hawaiian + item26e_newHawaiian + item27e_ContHawaiian) / (item28_total)).ToString("F");
        WhiteActualPercentage65 = item28_total == 0 ? "" : ((double)(item23_White + item26f_newWhite + item27f_ContWhite) / (item28_total)).ToString("F");

        worksheet.Name = "GPRA VI";

        worksheet.Cells[row, 1].Value = "GranteeID";  
        worksheet.Cells[row, 2].Value = "Grantee_Name"; 
        worksheet.Cells[row, 3].Value = "SchoolID";  
        worksheet.Cells[row, 4].Value = "School_Name"; 

        worksheet.Cells[row, 5].Value = "Group_min_isolated_American_Indian"; 
        worksheet.Cells[row, 6].Value = "Group_min_isolated_Asian";  

        worksheet.Cells[row, 7].Value = "Group_min_isolated_Black";  
        worksheet.Cells[row, 8].Value = "Group_min_isolated_Hispanic";  
        worksheet.Cells[row, 9].Value = "Group_min_isolated_Native_Hawaiian";  
        worksheet.Cells[row, 10].Value = "Group_min_isolated_White";  
        worksheet.Cells[row, 11].Value = "Sch_min_isolated_American_Indian";     
        worksheet.Cells[row, 12].Value = "Sch_min_isolated_Asian";  
        worksheet.Cells[row, 13].Value = "Sch_min_isolated_Black";   
        worksheet.Cells[row, 14].Value = "Sch_min_isolated_Hispanic";  
        worksheet.Cells[row, 15].Value = "Sch_min_isolated_Native_Hawaiian";  
        worksheet.Cells[row, 16].Value = "Sch_min_isolated_White";  
        worksheet.Cells[row, 17].Value = "Min_change_enroll_American_Indian";  
        worksheet.Cells[row, 18].Value = "Min_change_enroll_Asian";  

        worksheet.Cells[row, 19].Value = "Min_change_enroll_Black";  
        worksheet.Cells[row, 20].Value = "Min_change_enroll_Hispanic";  
        worksheet.Cells[row, 21].Value = "Min_change_enroll_Native_Hawaiian";  
        worksheet.Cells[row, 22].Value = "Min_change_enroll_White";  

        worksheet.Cells[row, 23].Value = "Rac_grp_sch_targ_enroll_American_Indian";  
        worksheet.Cells[row, 24].Value = "Rac_grp_sch_targ_enroll_Asian";  
        worksheet.Cells[row, 25].Value = "Rac_grp_sch_targ_enroll_Black";  
        worksheet.Cells[row, 26].Value = "Rac_grp_sch_targ_enroll_Hispanic";  
        worksheet.Cells[row, 27].Value = "Rac_grp_sch_targ_enroll_Native_Hawaiian";  
        worksheet.Cells[row, 28].Value = "Rac_grp_sch_targ_enroll_White"; 

        worksheet.Cells[row, 29].Value = "MGI_targ_enroll_American_Indian";  
        worksheet.Cells[row, 30].Value = "MGI_targ_enroll_Asian";  
        worksheet.Cells[row, 31].Value = "MGI_targ_enroll_Black";  
        worksheet.Cells[row, 32].Value = "MGI_targ_enroll_Hispanic";  
        worksheet.Cells[row, 33].Value = "MGI_targ_enroll_Native_Hawaiian";  
        worksheet.Cells[row, 34].Value = "MGI_targ_enroll_White";  
        worksheet.Cells[row, 35].Value = "Sch_meet_rac_enroll_targ_American_Indian";  
        worksheet.Cells[row, 36].Value = "Sch_meet_rac_enroll_targ_Asian";  

        worksheet.Cells[row, 37].Value = "Sch_meet_rac_enroll_targ_Black";  
        worksheet.Cells[row, 38].Value = "Sch_meet_rac_enroll_targ_Hispanic";  
        worksheet.Cells[row, 39].Value = "Sch_meet_rac_enroll_targ_Native_Hawaiian";  
        worksheet.Cells[row, 40].Value = "Sch_meet_rac_enroll_targ_White";  
        worksheet.Cells[row, 41].Value = "Sch_make_prog_targ_American_Indian";  

        worksheet.Cells[row, 42].Value = "Sch_make_prog_targ_Asian";  
        worksheet.Cells[row, 43].Value = "Sch_make_prog_targ_Black";  
        worksheet.Cells[row, 44].Value = "Sch_make_prog_targ_Hispanic";  
        worksheet.Cells[row, 45].Value = "Sch_make_prog_targ_Native_Hawaiian";  
        worksheet.Cells[row, 46].Value = "Sch_make_prog_targ_White";  


         worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:AT1"])   //format title
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false).OrderBy(x => x.GranteeID))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            if (granteeID != grantee.ID)
                isAlternative = !isAlternative;


            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                row++;

                if (isAlternative)
                {
                    using (ExcelRange rng = worksheet.Cells["A" + row + ":" + row])
                    {
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(219, 229, 241));
                    }

                }
                worksheet.Cells[row, 1].Value = grantee.ID;
                worksheet.Cells[row, 2].Value = grantee.GranteeName;
                worksheet.Cells[row, 3].Value = school.SchoolCode;
                worksheet.Cells[row, 4].Value = school.SchoolName;
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == school.ID && x.ReportID == report.ID);
                if (gpraData.Count > 0)
                {
                   
                    //to do gpra 6
                    MagnetGPRA item = gpraData[0];
                    MagnetGPRA6 gpra6Record = MagnetGPRA6.SingleOrDefault(x => x.MagnetGPRAID == item.ID);
                   

                    if (gpra6Record != null)
                    {
                        if (!string.IsNullOrEmpty(gpra6Record.MinorityIsolatedGroups.Trim()))
                        {
                            //items 61
                            worksheet.Cells[row, 5].Value = "0";
                            worksheet.Cells[row, 6].Value = "0";
                            worksheet.Cells[row, 7].Value = "0";
                            worksheet.Cells[row, 8].Value = "0";
                            worksheet.Cells[row, 9].Value = "0";
                            worksheet.Cells[row, 10].Value = "0";

                            foreach (string str in gpra6Record.MinorityIsolatedGroups.Split(','))
                            {
                                if (str == "1")
                                    worksheet.Cells[row, 5].Value = "1";
                                else if (str == "2")
                                    worksheet.Cells[row, 6].Value = "1";
                                if (str == "3")
                                    worksheet.Cells[row, 7].Value = "1";
                                else if (str == "4")
                                    worksheet.Cells[row, 8].Value = "1";
                                else if (str == "5")
                                    worksheet.Cells[row, 9].Value = "1";
                                else if (str == "6")
                                    worksheet.Cells[row, 10].Value = "1";
                            }
                        }
                        else
                        {
                            worksheet.Cells[row, 5].Value = "-98";
                            worksheet.Cells[row, 6].Value = "-98";
                            worksheet.Cells[row, 7].Value = "-98";
                            worksheet.Cells[row, 8].Value = "-98";
                            worksheet.Cells[row, 9].Value = "-98";
                            worksheet.Cells[row, 10].Value = "-98";
                        }

                        //items 62
                        if (!gpra6Record.IndianMSAP62 && !gpra6Record.IndianFeeder62)
                            worksheet.Cells[row, 11].Value = "-98";
                        else if (gpra6Record.IndianMSAP62)
                            worksheet.Cells[row, 11].Value = "1";
                        else if (gpra6Record.IndianFeeder62)
                            worksheet.Cells[row, 11].Value = "2";

                        if (!gpra6Record.AsianMSAP62 && !gpra6Record.AsianFeeder62)
                            worksheet.Cells[row, 12].Value = "-98";
                        else if (gpra6Record.AsianMSAP62)
                            worksheet.Cells[row, 12].Value = "1";
                        else if (gpra6Record.AsianFeeder62)
                            worksheet.Cells[row, 12].Value = "2";

                        if (!gpra6Record.BlackMSAP62 && !gpra6Record.BlackFeeder62)
                            worksheet.Cells[row, 13].Value = "-98";
                        else if (gpra6Record.BlackMSAP62)
                            worksheet.Cells[row, 13].Value = "1";
                        else if (gpra6Record.BlackFeeder62)
                            worksheet.Cells[row, 13].Value = "2";

                        if (!gpra6Record.HispanicMSAP62 && !gpra6Record.HispanicFeeder62)
                            worksheet.Cells[row, 14].Value = "-98";
                        else if (gpra6Record.HispanicMSAP62)
                            worksheet.Cells[row, 14].Value = "1";
                        else if (gpra6Record.HispanicFeeder62)
                            worksheet.Cells[row, 14].Value = "2";

                        if (!gpra6Record.NativeMSAP62 && !gpra6Record.NativeFeeder62)
                            worksheet.Cells[row, 15].Value = "-98";
                        else if (gpra6Record.NativeMSAP62)
                            worksheet.Cells[row, 15].Value = "1";
                        else if (gpra6Record.NativeFeeder62)
                            worksheet.Cells[row, 15].Value = "2";

                        if (!gpra6Record.WhiteMSAP62 && !gpra6Record.WhiteFeeder62)
                            worksheet.Cells[row, 16].Value = "-98";
                        else if (gpra6Record.WhiteMSAP62)
                            worksheet.Cells[row, 16].Value = "1";
                        else if (gpra6Record.WhiteFeeder62)
                            worksheet.Cells[row, 16].Value = "2";

                        //items 63
                        if (!gpra6Record.IndianDec63 && !gpra6Record.IndianInc63 && !gpra6Record.IndianMtn63)
                            worksheet.Cells[row, 17].Value = "-98";
                        else if (gpra6Record.IndianDec63)
                            worksheet.Cells[row, 17].Value = "1";
                        else if (gpra6Record.IndianInc63)
                            worksheet.Cells[row, 17].Value = "2";
                        else if (gpra6Record.IndianMtn63)
                            worksheet.Cells[row, 17].Value = "3";

                        if (!gpra6Record.AsianDec63 && !gpra6Record.AsianInc63 && !gpra6Record.AsianMtn63)
                            worksheet.Cells[row, 18].Value = "-98";
                        else if (gpra6Record.AsianDec63)
                            worksheet.Cells[row, 18].Value = "1";
                        else if (gpra6Record.AsianInc63)
                            worksheet.Cells[row, 18].Value = "2";
                        else if (gpra6Record.AsianMtn63)
                            worksheet.Cells[row, 18].Value = "3";

                        if (!gpra6Record.BlackDec63 && !gpra6Record.BlackInc63 && !gpra6Record.BlackMtn63)
                            worksheet.Cells[row, 19].Value = "-98";
                        else if (gpra6Record.BlackDec63)
                            worksheet.Cells[row, 19].Value = "1";
                        else if (gpra6Record.BlackInc63)
                            worksheet.Cells[row, 19].Value = "2";
                        else if (gpra6Record.BlackMtn63)
                            worksheet.Cells[row, 19].Value = "3";

                        if (!gpra6Record.HispanicDec63 && !gpra6Record.HispanicInc63 && !gpra6Record.HispanicMtn63)
                            worksheet.Cells[row, 20].Value = "-98";
                        else if (gpra6Record.HispanicDec63)
                            worksheet.Cells[row, 20].Value = "1";
                        else if (gpra6Record.HispanicInc63)
                            worksheet.Cells[row, 20].Value = "2";
                        else if (gpra6Record.HispanicMtn63)
                            worksheet.Cells[row, 20].Value = "3";

                        if (!gpra6Record.NativeDec63 && !gpra6Record.NativeInc63 && !gpra6Record.NativeMtn63)
                            worksheet.Cells[row, 21].Value = "-98";
                        else if (gpra6Record.NativeDec63)
                            worksheet.Cells[row, 21].Value = "1";
                        else if (gpra6Record.NativeInc63)
                            worksheet.Cells[row, 21].Value = "2";
                        else if (gpra6Record.NativeMtn63)
                            worksheet.Cells[row, 21].Value = "3";

                        if (!gpra6Record.WhiteDec63 && !gpra6Record.WhiteInc63 && !gpra6Record.WhiteMtn63)
                            worksheet.Cells[row, 22].Value = "-98";
                        else if (gpra6Record.WhiteDec63)
                            worksheet.Cells[row, 22].Value = "1";
                        else if (gpra6Record.WhiteInc63)
                            worksheet.Cells[row, 22].Value = "2";
                        else if (gpra6Record.WhiteMtn63)
                            worksheet.Cells[row, 22].Value = "3";

                        if (!string.IsNullOrEmpty(gpra6Record.TargetRacialGroup64.Trim()))
                        {
                            //items 64
                            worksheet.Cells[row, 23].Value = "0";
                            worksheet.Cells[row, 24].Value = "0";
                            worksheet.Cells[row, 25].Value = "0";
                            worksheet.Cells[row, 26].Value = "0";
                            worksheet.Cells[row, 27].Value = "0";
                            worksheet.Cells[row, 28].Value = "0";

                            foreach (string str in gpra6Record.TargetRacialGroup64.Split(','))
                            {
                                if (str == "1")
                                    worksheet.Cells[row, 23].Value = "1";
                                else if (str == "2")
                                    worksheet.Cells[row, 24].Value = "1";
                                if (str == "3")
                                    worksheet.Cells[row, 25].Value = "1";
                                else if (str == "4")
                                    worksheet.Cells[row, 26].Value = "1";
                                else if (str == "5")
                                    worksheet.Cells[row, 27].Value = "1";
                                else if (str == "6")
                                    worksheet.Cells[row, 28].Value = "1";
                            }
                        }
                        else
                        {
                            worksheet.Cells[row, 23].Value = "-98";
                            worksheet.Cells[row, 24].Value = "-98";
                            worksheet.Cells[row, 25].Value = "-98";
                            worksheet.Cells[row, 26].Value = "-98";
                            worksheet.Cells[row, 27].Value = "-98";
                            worksheet.Cells[row, 28].Value = "-98";
                        }


                        //items 65
                        worksheet.Cells[row, 29].Value = gpra6Record.IndianTargetPercentage65 == "" ? "-98" : (Convert.ToDouble(gpra6Record.IndianTargetPercentage65) / 100).ToString("F");
                        // worksheet.Cells[row, 44].Value = IndianActualPercentage65;
                        worksheet.Cells[row, 30].Value = gpra6Record.AsianTargetPercentage65 == "" ? "-98" : (Convert.ToDouble(gpra6Record.AsianTargetPercentage65) / 100).ToString("F");
                        // worksheet.Cells[row, 46].Value = AsianActualPercentage65;
                        worksheet.Cells[row, 31].Value = gpra6Record.BlackTargetPercentage65 == "" ? "-98" : (Convert.ToDouble(gpra6Record.BlackTargetPercentage65) / 100).ToString("F");
                        // worksheet.Cells[row, 48].Value = BlackActualPercentage65;
                        worksheet.Cells[row, 32].Value = gpra6Record.HispanicTargetPercentage65 == "" ? "-98" : (Convert.ToDouble(gpra6Record.HispanicTargetPercentage65) / 100).ToString("F");
                        //  worksheet.Cells[row, 50].Value = HispanicActualPercentage65;
                        worksheet.Cells[row, 33].Value = gpra6Record.NativeTargetPercentage65 == "" ? "-98" : (Convert.ToDouble(gpra6Record.NativeTargetPercentage65) / 100).ToString("F");
                        //  worksheet.Cells[row, 52].Value = NativeActualPercentage65;
                        worksheet.Cells[row, 34].Value = gpra6Record.WhiteTargetPercentage65 == "" ? "-98" : (Convert.ToDouble(gpra6Record.WhiteTargetPercentage65) / 100).ToString("F");
                        //  worksheet.Cells[row, 54].Value = WhiteActualPercentage65;

                        //items 66
                        if (!gpra6Record.IndianMet66 && !gpra6Record.IndianNotMet66)
                            worksheet.Cells[row, 35].Value = "-98";
                        else if (gpra6Record.IndianMet66)
                            worksheet.Cells[row, 35].Value = "1";
                        else if (gpra6Record.IndianNotMet66)
                            worksheet.Cells[row, 35].Value = "0";

                        if (!gpra6Record.AsianMet66 && !gpra6Record.AsianNotMet66)
                            worksheet.Cells[row, 36].Value = "-98";
                        else if (gpra6Record.AsianMet66)
                            worksheet.Cells[row, 36].Value = "1";
                        else if (gpra6Record.AsianNotMet66)
                            worksheet.Cells[row, 36].Value = "0";

                        if (!gpra6Record.BlackMet66 && !gpra6Record.BlackNotMet66)
                            worksheet.Cells[row, 37].Value = "-98";
                        else if (gpra6Record.BlackMet66)
                            worksheet.Cells[row, 37].Value = "1";
                        else if (gpra6Record.BlackNotMet66)
                            worksheet.Cells[row, 37].Value = "0";


                        if (!gpra6Record.HispanicMet66 && !gpra6Record.HispanicNotMet66)
                            worksheet.Cells[row, 38].Value = "-98";
                        else if (gpra6Record.HispanicMet66)
                            worksheet.Cells[row, 38].Value = "1";
                        else if (gpra6Record.HispanicNotMet66)
                            worksheet.Cells[row, 38].Value = "0";

                        if (!gpra6Record.NativeMet66 && !gpra6Record.NativeNotMet66)
                            worksheet.Cells[row, 39].Value = "-98";
                        else if (gpra6Record.NativeMet66)
                            worksheet.Cells[row, 39].Value = "1";
                        else if (gpra6Record.NativeNotMet66)
                            worksheet.Cells[row, 39].Value = "0";

                        if (!gpra6Record.WhiteMet66 && !gpra6Record.WhiteNotMet66)
                            worksheet.Cells[row, 40].Value = "-98";
                        else if (gpra6Record.WhiteMet66)
                            worksheet.Cells[row, 40].Value = "1";
                        else if (gpra6Record.WhiteNotMet66)
                            worksheet.Cells[row, 40].Value = "0";

                        //items 67
                        if (!gpra6Record.IndianMade67 && !gpra6Record.IndianNotMade67 && !gpra6Record.IndianNA67)
                            worksheet.Cells[row, 41].Value = "-98";
                        else if (gpra6Record.IndianMade67)
                            worksheet.Cells[row, 41].Value = "1";
                        else if (gpra6Record.IndianNotMade67)
                            worksheet.Cells[row, 41].Value = "0";
                        else if (gpra6Record.IndianNA67)
                            worksheet.Cells[row, 41].Value = "-98";

                        if (!gpra6Record.AsianMade67 && !gpra6Record.AsianNotMade67 && !gpra6Record.AsianNA67)
                            worksheet.Cells[row, 42].Value = "-98";
                        else if (gpra6Record.AsianMade67)
                            worksheet.Cells[row, 42].Value = "1";
                        else if (gpra6Record.AsianNotMade67)
                            worksheet.Cells[row, 42].Value = "0";
                        else if (gpra6Record.AsianNA67)
                            worksheet.Cells[row, 42].Value = "-98";

                        if (!gpra6Record.BlackMade67 && !gpra6Record.BlackNotMade67 && !gpra6Record.BlackNA67)
                            worksheet.Cells[row, 43].Value = "-98";
                        else if (gpra6Record.BlackMade67)
                            worksheet.Cells[row, 43].Value = "1";
                        else if (gpra6Record.BlackNotMade67)
                            worksheet.Cells[row, 43].Value = "0";
                        else if (gpra6Record.BlackNA67)
                            worksheet.Cells[row, 43].Value = "-98";

                        if (!gpra6Record.HispanicMade67 && !gpra6Record.HispanicNotMade67 && !gpra6Record.HispanicNA67)
                            worksheet.Cells[row, 44].Value = "-98";
                        else if (gpra6Record.HispanicMade67)
                            worksheet.Cells[row, 44].Value = "1";
                        else if (gpra6Record.HispanicNotMade67)
                            worksheet.Cells[row, 44].Value = "0";
                        else if (gpra6Record.HispanicNA67)
                            worksheet.Cells[row, 44].Value = "-98";

                        if (!gpra6Record.NativeMade67 && !gpra6Record.NativeNotMade67 && !gpra6Record.NativeNA67)
                            worksheet.Cells[row, 45].Value = "-98";
                        else if (gpra6Record.NativeMade67)
                            worksheet.Cells[row, 45].Value = "1";
                        else if (gpra6Record.NativeNotMade67)
                            worksheet.Cells[row, 45].Value = "0";
                        else if (gpra6Record.NativeNA67)
                            worksheet.Cells[row, 45].Value = "-98";

                        if (!gpra6Record.WhiteMade67 && !gpra6Record.WhiteNotMade67 && !gpra6Record.WhiteNA67)
                            worksheet.Cells[row, 46].Value = "-98";
                        else if (gpra6Record.WhiteMade67)
                            worksheet.Cells[row, 46].Value = "1";
                        else if (gpra6Record.WhiteNotMade67)
                            worksheet.Cells[row, 46].Value = "0";
                        else if (gpra6Record.WhiteNA67)
                            worksheet.Cells[row, 46].Value = "-98";

                    }
                    else
                    {

                        worksheet.Cells[row, 5].Value = "-99";
                        worksheet.Cells[row, 6].Value = "-99";
                        worksheet.Cells[row, 7].Value = "-99";
                        worksheet.Cells[row, 8].Value = "-99";
                        worksheet.Cells[row, 9].Value = "-99";
                        worksheet.Cells[row, 10].Value = "-99";
                        //items 62
                        worksheet.Cells[row, 11].Value = "-99";
                        worksheet.Cells[row, 12].Value = "-99";
                        worksheet.Cells[row, 13].Value = "-99";
                        worksheet.Cells[row, 10].Value = "-99";
                        worksheet.Cells[row, 14].Value = "-99";
                        worksheet.Cells[row, 15].Value = "-99";
                        worksheet.Cells[row, 16].Value = "-99";

                        //items 63
                        worksheet.Cells[row, 17].Value = "-99";
                        worksheet.Cells[row, 18].Value = "-99";
                        worksheet.Cells[row, 19].Value = "-99";
                        worksheet.Cells[row, 20].Value = "-99";
                        worksheet.Cells[row, 21].Value = "-99";
                        worksheet.Cells[row, 22].Value = "-99";

                        //items 64
                        worksheet.Cells[row, 23].Value = "-99";
                        worksheet.Cells[row, 24].Value = "-99";
                        worksheet.Cells[row, 25].Value = "-99";
                        worksheet.Cells[row, 26].Value = "-99";
                        worksheet.Cells[row, 27].Value = "-99";
                        worksheet.Cells[row, 28].Value = "-99";

                        worksheet.Cells[row, 29].Value = "-99";
                        worksheet.Cells[row, 30].Value = "-99";
                        worksheet.Cells[row, 31].Value = "-99";
                        worksheet.Cells[row, 32].Value = "-99";
                        worksheet.Cells[row, 33].Value = "-99";
                        worksheet.Cells[row, 34].Value = "-99";
                        worksheet.Cells[row, 35].Value = "-99";
                        worksheet.Cells[row, 36].Value = "-99";
                        worksheet.Cells[row, 37].Value = "-99";
                        worksheet.Cells[row, 38].Value = "-99";
                        worksheet.Cells[row, 39].Value = "-99";
                        worksheet.Cells[row, 40].Value = "-99";
                        worksheet.Cells[row, 41].Value = "-99";
                        worksheet.Cells[row, 42].Value = "-99";
                        worksheet.Cells[row, 43].Value = "-99";
                        worksheet.Cells[row, 44].Value = "-99";
                        worksheet.Cells[row, 45].Value = "-99";
                        worksheet.Cells[row, 46].Value = "-99";
                    }
                   
                }
                else   //no records in db  missing
                {

                    worksheet.Cells[row, 5].Value = "-99";
                    worksheet.Cells[row, 6].Value = "-99";
                    worksheet.Cells[row, 7].Value = "-99";
                    worksheet.Cells[row, 8].Value = "-99";
                    worksheet.Cells[row, 9].Value = "-99";
                    worksheet.Cells[row, 10].Value = "-99";
                    //items 62
                    worksheet.Cells[row, 11].Value = "-99";
                    worksheet.Cells[row, 12].Value = "-99";
                    worksheet.Cells[row, 13].Value = "-99";
                    worksheet.Cells[row, 10].Value = "-99";
                    worksheet.Cells[row, 14].Value = "-99";
                    worksheet.Cells[row, 15].Value = "-99";
                    worksheet.Cells[row, 16].Value = "-99";

                    //items 63
                    worksheet.Cells[row, 17].Value = "-99";
                    worksheet.Cells[row, 18].Value = "-99";
                    worksheet.Cells[row, 19].Value = "-99";
                    worksheet.Cells[row, 20].Value = "-99";
                    worksheet.Cells[row, 21].Value = "-99";
                    worksheet.Cells[row, 22].Value = "-99";

                    //items 64
                    worksheet.Cells[row, 23].Value = "-99";
                    worksheet.Cells[row, 24].Value = "-99";
                    worksheet.Cells[row, 25].Value = "-99";
                    worksheet.Cells[row, 26].Value = "-99";
                    worksheet.Cells[row, 27].Value = "-99";
                    worksheet.Cells[row, 28].Value = "-99";

                    worksheet.Cells[row, 29].Value = "-99";
                    worksheet.Cells[row, 30].Value = "-99";
                    worksheet.Cells[row, 31].Value = "-99";
                    worksheet.Cells[row, 32].Value = "-99";
                    worksheet.Cells[row, 33].Value = "-99";
                    worksheet.Cells[row, 34].Value = "-99";
                    worksheet.Cells[row, 35].Value = "-99";
                    worksheet.Cells[row, 36].Value = "-99";
                    worksheet.Cells[row, 37].Value = "-99";
                    worksheet.Cells[row, 38].Value = "-99";
                    worksheet.Cells[row, 39].Value = "-99";
                    worksheet.Cells[row, 40].Value = "-99";
                    worksheet.Cells[row, 41].Value = "-99";
                    worksheet.Cells[row, 42].Value = "-99";
                    worksheet.Cells[row, 43].Value = "-99";
                    worksheet.Cells[row, 44].Value = "-99";
                    worksheet.Cells[row, 45].Value = "-99";
                    worksheet.Cells[row, 46].Value = "-99";
                }
            }
        }

    }

    public static void GetBudgetSummary(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;
        worksheet.Name = "Budget 1 of 2";

        worksheet.Cells[row, 1].Value ="ReportID";
        worksheet.Cells[row, 2].Value ="GranteeID";
        worksheet.Cells[row, 3].Value ="PRAward";
        worksheet.Cells[row, 4].Value ="Personnel";
        worksheet.Cells[row, 5].Value ="Fringe Benefits";
        worksheet.Cells[row, 6].Value ="Travel";
        worksheet.Cells[row, 7].Value ="Equipment";
        worksheet.Cells[row, 8].Value ="Supplies";
        worksheet.Cells[row, 9].Value ="Contractual";
        worksheet.Cells[row, 10].Value ="Construction";
        worksheet.Cells[row, 11].Value ="Other";
        worksheet.Cells[row, 12].Value ="Total Direct Costs";
        worksheet.Cells[row, 13].Value ="Indirect Costs";
        worksheet.Cells[row, 14].Value ="Training Stipends";
        worksheet.Cells[row, 15].Value ="Total Costs";
        worksheet.Cells[row, 16].Value ="Personnel Non Federal";
        worksheet.Cells[row, 17].Value ="Fringe Benefits Non Federal";
        worksheet.Cells[row, 18].Value ="Travel Non Federal";
        worksheet.Cells[row, 19].Value ="Equipment Non Federal";
        worksheet.Cells[row, 20].Value ="Supplies Non Federal";
        worksheet.Cells[row, 21].Value ="Contractual Non Federal";
        worksheet.Cells[row, 22].Value ="Construction Non Federal";
        worksheet.Cells[row, 23].Value ="Other Non Federal";
        worksheet.Cells[row, 24].Value ="Total Direct Costs Non Federal";
        worksheet.Cells[row, 25].Value ="Indirect Costs Non Federal";
        worksheet.Cells[row, 26].Value ="Training Stipends Non Federal";
        worksheet.Cells[row, 27].Value ="Total Costs Non Federal";
        worksheet.Cells[row, 28].Value ="Indirect Cost Agreement (yes/no)";
        worksheet.Cells[row, 29].Value ="AgreementPeriod";
        worksheet.Cells[row, 30].Value ="Approving Federal agency";
        worksheet.Cells[row, 31].Value ="Indirect Cost Rate";
        worksheet.Cells[row, 32].Value ="Included in your approved Indirect Cost Rate Agreement";
        worksheet.Cells[row, 33].Value ="CompliesWith34CFR";
        worksheet.Cells[row, 34].Value ="N/A";
        worksheet.Cells[row, 35].Value ="Indirect Cost Rate";
        worksheet.Cells[row, 36].Value ="Budget Change?";
        worksheet.Cells[row, 37].Value ="Reason";

        using (ExcelRange rng = worksheet.Cells["A1:AK1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }
       
        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            
            var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == report.ID);
            if (data.Count > 0)
            {
                row++;
               
               

                worksheet.Cells[row, 1].Value =report.ID;
                worksheet.Cells[row, 2].Value =report.GranteeID;
                worksheet.Cells[row, 3].Value =grantee.PRAward;
                worksheet.Cells[row, 4].Value = Convert.ToDouble(data[0].Personnel);
                worksheet.Cells[row, 4].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 5].Value =Convert.ToDouble(data[0].Fringe);
                worksheet.Cells[row, 5].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 6].Value =Convert.ToDouble(data[0].Travel);
                worksheet.Cells[row, 6].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 7].Value =Convert.ToDouble(data[0].Equipment);
                worksheet.Cells[row, 7].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 8].Value =Convert.ToDouble(data[0].Supplies);
                worksheet.Cells[row, 8].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 9].Value =Convert.ToDouble(data[0].Contractual);
                worksheet.Cells[row, 9].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 10].Value =Convert.ToDouble(data[0].Construction);
                worksheet.Cells[row, 10].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 11].Value =Convert.ToDouble(data[0].Other);
                worksheet.Cells[row, 11].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                decimal Total = 0;
                Total = (decimal)((data[0].Personnel == null ? 0 : data[0].Personnel) +
                    (data[0].Fringe == null ? 0 : data[0].Fringe) +
                    (data[0].Travel == null ? 0 : data[0].Travel) +
                    (data[0].Equipment == null ? 0 : data[0].Equipment) +
                    (data[0].Supplies == null ? 0 : data[0].Supplies) +
                    (data[0].Contractual == null ? 0 : data[0].Contractual) +
                    (data[0].Construction == null ? 0 : data[0].Construction) +
                    (data[0].Other == null ? 0 : data[0].Other));
                worksheet.Cells[row, 12].Value =Convert.ToDouble(Total);
                worksheet.Cells[row, 12].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 13].Value =Convert.ToDouble(data[0].IndirectCost);
                worksheet.Cells[row, 13].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 14].Value =Convert.ToDouble(data[0].TrainingStipends);
                worksheet.Cells[row, 14].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                Total += (data[0].IndirectCost == null ? 0 : (decimal)data[0].IndirectCost) +
                    (data[0].TrainingStipends == null ? 0 : (decimal)data[0].TrainingStipends);
                worksheet.Cells[row, 15].Value =Convert.ToDouble(Total);
                worksheet.Cells[row, 15].Style.Numberformat.Format = "\"$\"#,##0.00_);(\"$\"#,##0.00)";

                worksheet.Cells[row, 16].Value =data[0].PersonnelNF;
                worksheet.Cells[row, 17].Value =data[0].FringeNF;
                worksheet.Cells[row, 18].Value =data[0].TravelNF;
                worksheet.Cells[row, 19].Value =data[0].EquipmentNF;
                worksheet.Cells[row, 20].Value =data[0].SuppliesNF;
                worksheet.Cells[row, 21].Value =data[0].ContractualNF;
                worksheet.Cells[row, 22].Value =data[0].ConstructionNF;
                worksheet.Cells[row, 23].Value =data[0].OtherNF;
                Total = (decimal)((data[0].PersonnelNF == null ? 0 : data[0].PersonnelNF) +
                    (data[0].FringeNF == null ? 0 : data[0].FringeNF) +
                    (data[0].TravelNF == null ? 0 : data[0].TravelNF) +
                    (data[0].EquipmentNF == null ? 0 : data[0].EquipmentNF) +
                    (data[0].SuppliesNF == null ? 0 : data[0].SuppliesNF) +
                    (data[0].ContractualNF == null ? 0 : data[0].ContractualNF) +
                    (data[0].ConstructionNF == null ? 0 : data[0].ConstructionNF) +
                    (data[0].OtherNF == null ? 0 : data[0].OtherNF));
                worksheet.Cells[row, 24].Value =Total;
                worksheet.Cells[row, 25].Value =data[0].IndirectCostNF;
                worksheet.Cells[row, 26].Value =data[0].TrainingStipendsNF;
                Total += (data[0].IndirectCostNF == null ? 0 : (decimal)data[0].IndirectCostNF) +
                    (data[0].TrainingStipendsNF == null ? 0 : (decimal)data[0].TrainingStipendsNF);
                worksheet.Cells[row, 27].Value =Total;

                if (data[0].IndirectCostAgreement != null)
                {
                    worksheet.Cells[row, 28].Value =(bool)data[0].IndirectCostAgreement ? "True" : "False";
                    if ((bool)data[0].IndirectCostAgreement)
                    {
                        worksheet.Cells[row, 29].Value =data[0].AgreementFrom + "-" + data[0].AgreementTo;
                        if (data[0].ApprovalAgency != null)
                        {
                            worksheet.Cells[row, 30].Value =(bool)data[0].ApprovalAgency ? "ED" : data[0].OtherAgency;
                        }
                        worksheet.Cells[row, 31].Value =Convert.ToString(data[0].IndirectCostRate);
                    }
                }
                if (data[0].RestrictedRateProgram != null)
                {
                    if ((bool)data[0].RestrictedRateProgram)
                        worksheet.Cells[row, 32].Value ="Yes";
                    else
                        worksheet.Cells[row, 33].Value ="Yes";
                }
                if (data[0].na != null)
                    worksheet.Cells[row, 34].Value =(bool)data[0].na ? "Yes" : "No";
                else
                    worksheet.Cells[row, 34].Value ="No";

                worksheet.Cells[row, 35].Value =Convert.ToString(data[0].RestrictedRate);
                if (data[0].BudgetChange != null)
                {
                    worksheet.Cells[row, 36].Value =(bool)data[0].BudgetChange ? "Yes" : "No";
                    if (data[0].BudgetChange == true)
                    {
                        worksheet.Cells[row, 37].Value =data[0].BudgetReason;
                    }
                }
            }
        }

    }
    public static void GetBudget2(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;
        worksheet.Name = "Budget 2 of 2";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "Budget Summary";
        worksheet.Cells[row, 4].Value = "Budget Narrative";

        worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:D1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);

            var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == report.ID);
            if (data.Count > 0)
            {
                row++;

                worksheet.Cells[row, 1].Value = report.ID;
                worksheet.Cells[row, 2].Value = report.GranteeID;
                worksheet.Cells[row, 3].Value = data[0].BudgetSummary != null ? data[0].BudgetSummary.Trim() : "";
                var files = MagnetUpload.Find(x => x.ProjectID == report.ID && x.FormID == 13);
                if (files.Count > 0 && !string.IsNullOrEmpty(files[0].FileName))
                {
                    worksheet.Cells[row, 3].Value += "\r\n Uploaded Document: " + files[0].FileName.ToString().Split('.')[0];
                }

                worksheet.Cells[row, 4].Value = data[0].BudgetItemized != null ? data[0].BudgetItemized : "";
                files = MagnetUpload.Find(x => x.ProjectID == report.ID && x.FormID == 14);
                if (files.Count > 0 && !string.IsNullOrEmpty(files[0].FileName))
                {
                    worksheet.Cells[row, 4].Value += "\r\n Uploaded Document: " + files[0].FileName.ToString().Split('.')[0];
                }
            }
        }
    }
    public static void GetDesegregation(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;

        worksheet.Name = "Desegregation";
        worksheet.Cells[row, 1].Value ="ReportID";
        worksheet.Cells[row, 2].Value ="GranteeID";
        worksheet.Cells[row, 3].Value ="PRAward";
        worksheet.Cells[row, 4].Value ="PlanType";
        worksheet.Cells[row, 5].Value ="Coded_Plan_Type";

         using (ExcelRange rng = worksheet.Cells["A1:E1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            row++;
           

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            worksheet.Cells[row, 1].Value =report.ID;
            worksheet.Cells[row, 2].Value =report.GranteeID;
            worksheet.Cells[row, 3].Value =grantee.PRAward;

            var data = MagnetDesegregationPlan.Find(x => x.ReportID == report.ID);
            if (data.Count > 0)
            {
                if (data[0].PlanType != null)
                {
                    worksheet.Cells[row, 4].Value =(bool)data[0].PlanType == false ? "Required" : "Voluntary";
                    worksheet.Cells[row, 5].Value =(bool)data[0].PlanType == false ? 2 : 1;
                }
                else
                    worksheet.Cells[row, 5].Value =99;
            }
            else
            {
                worksheet.Cells[row, 4].Value ="";
                worksheet.Cells[row, 5].Value =99;
            }
        }
    }
    public static void GetTable7(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;
        int AmericanTotal = 0, AsianTotal = 0, BlackTotal = 0, HispanicTotal = 0, NativeTotal = 0,
            WhiteTotal = 0, moretah2Total = 0, GrantTotal=0;

        Hashtable ht = new Hashtable();
        foreach (MagnetDLL dll in MagnetDLL.Find(x => x.TypeID == 1))
        {
            ht.Add(dll.TypeIndex, dll.TypeName);
        }

        worksheet.Name = "Table 1";
        worksheet.Cells[row, 1].Value ="ReportID";
        worksheet.Cells[row, 2].Value ="GranteeID";
        worksheet.Cells[row, 3].Value ="PRAward";
        worksheet.Cells[row, 4].Value ="Grade";
        worksheet.Cells[row, 5].Value ="American Indian or Alaska Native";
        worksheet.Cells[row, 6].Value ="Asian";
        worksheet.Cells[row, 7].Value ="Black or African-American";
        worksheet.Cells[row, 8].Value ="Hispanic or Latino";
        worksheet.Cells[row, 9].Value ="Native Hawaiian or Other Pacific Islander";
        worksheet.Cells[row, 10].Value ="White";
        worksheet.Cells[row, 11].Value ="Two or more races";
        worksheet.Cells[row, 12].Value ="Total";

         using (ExcelRange rng = worksheet.Cells["A1:L1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            bool hasdata = false;
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);

            MagnetDBDB db = new MagnetDBDB();
            var data = db.spp_leaenrollment(report.ID, 1).ExecuteReader();
            while(data.Read())
            {
                hasdata = true;
                row++;
               

                worksheet.Cells[row, 1].Value =report.ID;
                worksheet.Cells[row, 2].Value =report.GranteeID;
                worksheet.Cells[row, 3].Value =grantee.PRAward;
                if(!data.IsDBNull(1)) worksheet.Cells[row, 4].Value =ht[data.GetValue(1)];
                if (!data.IsDBNull(2))
                {
                    worksheet.Cells[row, 5].Value =data.GetValue(2);
                    AmericanTotal += Convert.ToInt32(data.GetValue(2));
                }
                if (!data.IsDBNull(5))
                {
                    worksheet.Cells[row, 6].Value =data.GetValue(5);
                    AsianTotal += Convert.ToInt32(data.GetValue(5));
                }

                if (!data.IsDBNull(7))
                {
                    worksheet.Cells[row, 7].Value =data.GetValue(7);
                    BlackTotal += Convert.ToInt32(data.GetValue(7));
                }
                else
                    worksheet.Cells[row, 7].Value =" ";
                if (!data.IsDBNull(9))
                {
                    worksheet.Cells[row, 8].Value =data.GetValue(9);
                    HispanicTotal += Convert.ToInt32(data.GetValue(9));
                }
                else
                    worksheet.Cells[row, 8].Value =" ";
                if (!data.IsDBNull(13))
                {
                    worksheet.Cells[row, 9].Value =data.GetValue(13);
                    NativeTotal += Convert.ToInt32(data.GetValue(13));
                }
                else
                    worksheet.Cells[row, 9].Value =" ";
                if (!data.IsDBNull(11))
                {
                    worksheet.Cells[row, 10].Value =data.GetValue(11);
                    WhiteTotal += Convert.ToInt32(data.GetValue(11));
                }
                else
                    worksheet.Cells[row, 10].Value =" ";
                if (!data.IsDBNull(15))
                {
                    worksheet.Cells[row, 11].Value =data.GetValue(15);
                    moretah2Total += Convert.ToInt32(data.GetValue(15));
                }
                else
                    worksheet.Cells[row, 11].Value =" ";
                if (!data.IsDBNull(3))
                {
                    worksheet.Cells[row, 12].Value =data.GetValue(3);
                    GrantTotal += Convert.ToInt32(data.GetValue(3));
                }
                else
                    worksheet.Cells[row, 12].Value =" ";
            }

            data.Close();

            if (hasdata)
            {
                row++;
                worksheet.Cells[row, 1].Value =report.ID;
                worksheet.Cells[row, 2].Value =report.GranteeID;
                worksheet.Cells[row, 3].Value =grantee.PRAward;

                worksheet.Cells[row, 4].Value ="Total";
                worksheet.Cells[row, 5].Value =AmericanTotal;
                worksheet.Cells[row, 6].Value =AsianTotal;
                worksheet.Cells[row, 7].Value =BlackTotal;
                worksheet.Cells[row, 8].Value =HispanicTotal;
                worksheet.Cells[row, 9].Value =NativeTotal;
                worksheet.Cells[row, 10].Value =WhiteTotal;
                worksheet.Cells[row, 11].Value =moretah2Total;
                worksheet.Cells[row, 12].Value =GrantTotal;
                hasdata = false;
                AmericanTotal = 0;
                AsianTotal = 0;
                BlackTotal = 0;
                HispanicTotal = 0;
                NativeTotal = 0;
                WhiteTotal = 0;
                moretah2Total = 0;
                GrantTotal = 0;

                //using (ExcelRange rng = worksheet.Cells["A" + row + ":L" + row])
                //{
                //    rng.Style.Font.Bold = true;

                //    rng.Style.Fill.PatternType = ExcelFillStyle.Solid; 
                //    rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                //}
            }
            
        }

    }
    public static void GetTable8(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
       

        worksheet.Name = "Table 2";
        worksheet.Cells[row, 1].Value ="ReportID";
        worksheet.Cells[row, 2].Value ="GranteeID";
        worksheet.Cells[row, 3].Value ="PRAward";
        worksheet.Cells[row, 4].Value ="SchoolID";
        worksheet.Cells[row, 5].Value ="SchoolName";
        worksheet.Cells[row, 6].Value ="SchoolYear";
         using (ExcelRange rng = worksheet.Cells["A1:F1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);

            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear==reportyear && x.isActive))
            {
                row++;

              

                worksheet.Cells[row, 1].Value =report.ID;
                worksheet.Cells[row, 2].Value =report.GranteeID;
                worksheet.Cells[row, 3].Value =grantee.PRAward;
                worksheet.Cells[row, 4].Value =school.SchoolCode;
                worksheet.Cells[row, 5].Value =school.SchoolName;
                var data = MagnetSchoolYear.Find(x => x.ReportID == report.ID && x.SchoolID == school.ID);
                if (data.Count > 0)
                {
                    worksheet.Cells[row, 6].Value =data[0].SchoolYear;
                }
            }
        }
    }
    public static void GetTable9(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
        int AmericanTotal = 0, AsianTotal = 0, BlackTotal = 0, HispanicTotal = 0, NativeTotal = 0,
            WhiteTotal = 0, moretah2Total = 0, GrantTotal = 0;
        

        Hashtable ht = new Hashtable();
        foreach (MagnetDLL dll in MagnetDLL.Find(x => x.TypeID == 1))
        {
            ht.Add(dll.TypeIndex, dll.TypeName);
        }

        worksheet.Name = "Table 3";
        worksheet.Cells[row, 1].Value ="ReportID";
        worksheet.Cells[row, 2].Value ="GranteeID";
        worksheet.Cells[row, 3].Value ="PRAward";
        worksheet.Cells[row, 4].Value ="SchoolID"; //13
        worksheet.Cells[row, 5].Value ="SchoolName"; //3
        worksheet.Cells[row, 6].Value ="Grade";//4
        worksheet.Cells[row, 7].Value ="American Indian or Alaska Native";//5
        worksheet.Cells[row, 8].Value ="Asian";//6
        worksheet.Cells[row, 9].Value ="Black or African-American";//7
        worksheet.Cells[row, 10].Value ="Hispanic or Latino";//8
        worksheet.Cells[row, 11].Value ="Native Hawaiian or Other Pacific Islander";//9
        worksheet.Cells[row, 12].Value ="White";//10
        worksheet.Cells[row, 13].Value ="Two or more races";//11
        worksheet.Cells[row, 14].Value ="Total";//12

        using (ExcelRange rng = worksheet.Cells["A1:N1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            bool hasdata = false;
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);

            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive))
            {
                MagnetDBDB db = new MagnetDBDB();
                var data = db.spp_schoolenrollment(report.ID, 2, school.ID).ExecuteReader();
                //var data = from m in db.MagnetSchoolEnrollments
                //                     where m.GranteeReportID == report.ID
                //                     && m.SchoolID == school.ID
                //                     && m.StageID == 2//School enrollment data
                //                     orderby m.GradeLevel
                //                     select new
                //                     {
                //                         m.GradeLevel,
                //                         AmericanIndian = m.AmericanIndian == null ? (int?)null : m.AmericanIndian,
                //                         Asian = m.Asian == null ? (int?)null : m.Asian,
                //                         AfricanAmerican = m.AfricanAmerican == null ? (int?)null : m.AfricanAmerican,
                //                         Hispanic = m.Hispanic == null ? (int?)null : m.Hispanic,
                //                         Hawaiian = m.Hawaiian == null ? (int?)null : m.Hawaiian,
                //                         White = m.White == null ? (int?)null : m.White,
                //                         MultiRacial = m.MultiRacial == null ? (int?)null : m.MultiRacial,
                //                         GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                //                         + (m.Asian == null ? 0 : m.Asian)
                //                         + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                //                         + (m.Hispanic == null ? 0 : m.Hispanic)
                //                         + (m.Hawaiian == null ? 0 : m.Hawaiian)
                //                         + (m.White == null ? 0 : m.White)
                //                         + (m.MultiRacial == null ? 0 : m.MultiRacial))
                //                     };
                while (data.Read())
                {
                    hasdata = true;
                    row++;
                   

                    worksheet.Cells[row, 1].Value =report.ID;
                    worksheet.Cells[row, 2].Value =report.GranteeID;
                    worksheet.Cells[row, 3].Value =grantee.PRAward;
                    worksheet.Cells[row, 4].Value =school.SchoolCode;
                    worksheet.Cells[row, 5].Value =school.SchoolName;

                    if (!data.IsDBNull(1)) worksheet.Cells[row, 6].Value =ht[data.GetValue(1)];
                    if (!data.IsDBNull(2))
                    {
                        worksheet.Cells[row, 7].Value =data.GetValue(2);
                        AmericanTotal += Convert.ToInt32(data.GetValue(2));
                    }
                    else
                        worksheet.Cells[row, 7].Value =" ";
                    if (!data.IsDBNull(5))
                    {
                        worksheet.Cells[row, 8].Value =data.GetValue(5);
                        AsianTotal += Convert.ToInt32(data.GetValue(5));
                    }
                    else
                        worksheet.Cells[row, 8].Value =" ";
                    if (!data.IsDBNull(7))
                    {
                        worksheet.Cells[row, 9].Value =data.GetValue(7);
                        BlackTotal += Convert.ToInt32(data.GetValue(7));
                    }
                    else
                        worksheet.Cells[row, 9].Value =" ";
                    if (!data.IsDBNull(9))
                    {
                        worksheet.Cells[row, 10].Value =data.GetValue(9);
                        HispanicTotal += Convert.ToInt32(data.GetValue(9));
                    }
                    else
                        worksheet.Cells[row, 10].Value =" ";
                    if (!data.IsDBNull(13))
                    {
                        worksheet.Cells[row, 11].Value =data.GetValue(13);
                        NativeTotal += Convert.ToInt32(data.GetValue(13));
                    }
                    else
                        worksheet.Cells[row, 11].Value =" ";
                    if (!data.IsDBNull(11))
                    {
                        worksheet.Cells[row, 12].Value =data.GetValue(11);
                        WhiteTotal += Convert.ToInt32(data.GetValue(11));
                    }
                    else
                        worksheet.Cells[row, 12].Value =" ";
                    if (!data.IsDBNull(15))
                    {
                        worksheet.Cells[row, 13].Value =data.GetValue(15);
                        moretah2Total += Convert.ToInt32(data.GetValue(15));
                    }
                    else
                        worksheet.Cells[row, 13].Value =" ";
                    if (!data.IsDBNull(3))
                    {
                        worksheet.Cells[row, 14].Value =data.GetValue(3);
                        GrantTotal += Convert.ToInt32(data.GetValue(3));
                    }
                    else
                        worksheet.Cells[row, 14].Value =" ";
                }
                data.Close();

                if (hasdata)
                {
                    hasdata = false;
                    row++;
                    worksheet.Cells[row, 1].Value =report.ID;
                    worksheet.Cells[row, 2].Value =report.GranteeID;
                    worksheet.Cells[row, 3].Value =grantee.PRAward;
                    worksheet.Cells[row, 4].Value =school.SchoolCode;
                    worksheet.Cells[row, 5].Value =school.SchoolName;

                    worksheet.Cells[row, 6].Value ="Total";
                    worksheet.Cells[row, 7].Value =AmericanTotal;
                    worksheet.Cells[row, 8].Value =AsianTotal;
                    worksheet.Cells[row, 9].Value =BlackTotal;
                    worksheet.Cells[row, 10].Value =HispanicTotal;
                    worksheet.Cells[row, 11].Value =NativeTotal;
                    worksheet.Cells[row, 12].Value =WhiteTotal;
                    worksheet.Cells[row, 13].Value =moretah2Total;
                    worksheet.Cells[row, 14].Value =GrantTotal;
                    AmericanTotal = 0;
                    AsianTotal = 0;
                    BlackTotal = 0;
                    HispanicTotal = 0;
                    NativeTotal = 0;
                    WhiteTotal = 0;
                    moretah2Total = 0;
                    GrantTotal = 0;
                    //using (ExcelRange rng = worksheet.Cells["A" + row + ":N" + row])
                    //{
                    //    rng.Style.Font.Bold = true;

                    //    rng.Style.Fill.PatternType = ExcelFillStyle.Solid; 
                    //    rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                    //}

                }

            }
        }

    }
    public static void GetTable11(ExcelWorksheet worksheet, int ReportPeriodID, int reportyear)
    {
        int row = 1;
        int AmericanTotal = 0, AsianTotal = 0, BlackTotal = 0, HispanicTotal = 0, NativeTotal = 0,
            WhiteTotal = 0, moretah2Total = 0, GrantTotal = 0;

        worksheet.Name = "Table 4";
        worksheet.Cells[row, 1].Value ="ReportID";
        worksheet.Cells[row, 2].Value ="GranteeID";
        worksheet.Cells[row, 3].Value ="PRAward";
        worksheet.Cells[row, 4].Value ="FeederSchool";
        worksheet.Cells[row, 5].Value ="MagnetSchool";
        worksheet.Cells[row, 6].Value ="American Indian or Alaska Native";
        worksheet.Cells[row, 7].Value ="Asian";
        worksheet.Cells[row, 8].Value ="Black or African-American";
        worksheet.Cells[row, 9].Value ="Hispanic or Latino";
        worksheet.Cells[row, 10].Value ="Native Hawaiian or Other Pacific Islander";
        worksheet.Cells[row, 11].Value ="White";
        worksheet.Cells[row, 12].Value ="Two or more races";
        worksheet.Cells[row, 13].Value ="Total";

         using (ExcelRange rng = worksheet.Cells["A1:M1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;

        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            bool hasdata = false;

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            

            var mschools = MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive == false);
            var enrollments = MagnetSchoolFeederEnrollment.Find(m => m.GranteeReportID == report.ID && m.StageID == 3);

            string strIDs = "";
            string[] deactiveshools = mschools.Select(x => x.ID.ToString()).ToArray();

            int count = 0;

            foreach (MagnetSchool mgschool in mschools)
            {

                foreach (MagnetSchoolFeederEnrollment mfenroll in enrollments)
                {
                    if (mgschool.ID.ToString() == mfenroll.SchoolID)
                        strIDs += mfenroll.ID + ";";
                    else
                    {
                        foreach (string meId in mfenroll.SchoolID.Split(';'))
                        {
                            if (!deactiveshools.Contains(meId))
                                break;
                            count++;
                        }
                        if (mfenroll.SchoolID.Split(';').Count() == count)
                            strIDs += mfenroll.ID + ";";
                        count = 0;
                    }
                }

            }
            strIDs = strIDs.TrimEnd(';');

            if (string.IsNullOrEmpty(strIDs))
                strIDs = "9988999"; //keep sql happy


                MagnetDBDB db = new MagnetDBDB();
                var data = db.spp_feederenrollment(report.ID, 3, strIDs).ExecuteReader();
                while (data.Read())
                {
                    hasdata = true;
                    row++;

                    //if (row % 2 == 0)
                    //{
                    //    using (ExcelRange rng = worksheet.Cells["A" + row + ":M" + row])
                    //    {
                    //        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(159, 207, 255));
                    //    }
                    //}

                    worksheet.Cells[row, 1].Value =report.ID;
                    worksheet.Cells[row, 2].Value =report.GranteeID;
                    worksheet.Cells[row, 3].Value =grantee.PRAward;
                    if (!data.IsDBNull(4)) worksheet.Cells[row, 4].Value =data.GetString(4);
                    if (!data.IsDBNull(5))
                    {
                        string magnetSchools = Convert.ToString(data.GetString(5));

                        magnetSchools = magnetSchools.Replace("<p>", "").Replace("</p>", "\r\n\r\n");

                        worksheet.Column(5).Width = 50;
                        worksheet.Cells[row, 5].Style.WrapText = true;
                        worksheet.Cells[row, 5].Value =magnetSchools;
                    }
                    if (!data.IsDBNull(6))
                    {
                        worksheet.Cells[row, 6].Value =data.GetValue(6);
                        AmericanTotal += Convert.ToInt32(data.GetValue(6));
                    }
                    else
                        worksheet.Cells[row, 6].Value =" ";
                    if (!data.IsDBNull(9))
                    {
                        worksheet.Cells[row, 7].Value =data.GetValue(9);
                        AsianTotal += Convert.ToInt32(data.GetValue(9));
                    }
                    else
                        worksheet.Cells[row, 7].Value =" ";
                    if (!data.IsDBNull(11))
                    {
                        worksheet.Cells[row, 8].Value =data.GetValue(11);
                        BlackTotal += Convert.ToInt32(data.GetValue(11));
                    }
                    else
                        worksheet.Cells[row, 8].Value =" ";
                    if (!data.IsDBNull(13))
                    {
                        worksheet.Cells[row, 9].Value =data.GetValue(13);
                        HispanicTotal += Convert.ToInt32(data.GetValue(13));
                    }
                    else
                        worksheet.Cells[row, 9].Value =" ";
                    if (!data.IsDBNull(17))
                    {
                        worksheet.Cells[row, 10].Value =data.GetValue(17);
                        NativeTotal += Convert.ToInt32(data.GetValue(17));
                    }
                    else
                        worksheet.Cells[row, 10].Value =" ";
                    if (!data.IsDBNull(15))
                    {
                        worksheet.Cells[row, 11].Value =data.GetValue(15);
                        WhiteTotal += Convert.ToInt32(data.GetValue(15));
                    }
                    else
                        worksheet.Cells[row, 11].Value =" ";
                    if (!data.IsDBNull(19))
                    {
                        worksheet.Cells[row, 12].Value =data.GetValue(19);
                        moretah2Total += Convert.ToInt32(data.GetValue(19));
                    }
                    else
                        worksheet.Cells[row, 12].Value =" ";
                    if (!data.IsDBNull(7))
                    {
                        worksheet.Cells[row, 13].Value =data.GetValue(7);
                        GrantTotal += Convert.ToInt32(data.GetValue(7));
                    }
                    else
                        worksheet.Cells[row, 13].Value =" ";
                }

                if (hasdata)
                {
                    hasdata = false;
                    row++;
                    worksheet.Cells[row, 1].Value =report.ID;
                    worksheet.Cells[row, 2].Value =report.GranteeID;
                    worksheet.Cells[row, 3].Value =grantee.PRAward;
                    //if (!data.IsDBNull(4)) worksheet.Cells[row, 4].Value = data.GetString(4);

                    worksheet.Cells[row, 5].Value ="Total";
                    worksheet.Cells[row, 6].Value =AmericanTotal;
                    worksheet.Cells[row, 7].Value =AsianTotal;
                    worksheet.Cells[row, 8].Value =BlackTotal;
                    worksheet.Cells[row, 9].Value =HispanicTotal;
                    worksheet.Cells[row, 10].Value =NativeTotal;
                    worksheet.Cells[row, 11].Value =WhiteTotal;
                    worksheet.Cells[row, 12].Value =moretah2Total;
                    worksheet.Cells[row, 13].Value =GrantTotal;
                    AmericanTotal = 0;
                    AsianTotal = 0;
                    BlackTotal = 0;
                    HispanicTotal = 0;
                    NativeTotal = 0;
                    WhiteTotal = 0;
                    moretah2Total = 0;
                    GrantTotal = 0;

                  
                }

                data.Close();
        }

    }

    public static void GetAdditionalUpdate(ExcelWorksheet worksheet, int ReportPeriodID)
    {
        int row = 1;
        worksheet.Name = "Additional Uploads";
        worksheet.Cells[row, 1].Value = "ReportID";
        worksheet.Cells[row, 2].Value = "GranteeID";
        worksheet.Cells[row, 3].Value = "Additional Uploads";

        worksheet.Cells["A:XFD"].Style.Font.Name = "Calibri";  //Sets font to Calibri for all cells in a worksheet

        using (ExcelRange rng = worksheet.Cells["A1:C1"])
        {
            rng.Style.Font.Bold = true;
            rng.Style.Font.UnderLine = true;
            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }

        foreach (GranteeReport report in GranteeReport.Find(x => x.ReportPeriodID == ReportPeriodID && x.ReportType == false))
        {
            //skip test account:
            //39 : Marisa School District; 40 : Yoshi School District; 41 : Bonita School District;
            //1001 : Bonita School District2013; 1002 : Bonita School District2013; 1003 : Bonita School District2013;
            if (report.GranteeID == 39 || report.GranteeID == 40 || report.GranteeID == 41 || report.GranteeID == 0
                || report.GranteeID == 1001 || report.GranteeID == 1002 || report.GranteeID == 1003
                ) //test account, skip
                continue;
            else if ((report.ReportPeriodID > 8 && (report.GranteeID == 69 || report.GranteeID == 70)) || (report.ReportPeriodID <= 8 && (report.GranteeID == 71)))
                continue;  //out of grantees are not avlid

            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);

            var data = MagnetUpload.Find(x => x.ProjectID== report.ID);
            if (data.Count > 0)
            {
                row++;

                worksheet.Cells[row, 1].Value = report.ID;
                worksheet.Cells[row, 2].Value = report.GranteeID;
                foreach (var thefile in data)
                {
                    if (worksheet.Cells[row, 3].Value != null)
                    {
                        worksheet.Cells[row, 3].Value += "\r\n";
                    }
                    worksheet.Cells[row, 3].Value += thefile.FileName != null ? "Uploaded Document: " + thefile.FileName.Trim().ToString().Split('.')[0] : "";
                }
            }

        }
    }
}
