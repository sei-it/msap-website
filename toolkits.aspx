﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="toolkits.aspx.cs" Inherits="attendresource" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center Resources - Toolkits & Guides
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <a name="skip"></a>
    <div class="mainContent">
    <a href="resource.aspx">Library</a> &nbsp;&nbsp;> &nbsp;&nbsp;<a href="publications.aspx">Publications</a> &nbsp;&nbsp;<a href="multimedia.aspx">Multimedia</a> &nbsp;&nbsp;Toolkits &amp; Guides
    <h1>Toolkits &amp; Guides</h1>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false" ShowHeader="false"
            AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" GridLines="None"
            OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
            <RowStyle BorderStyle="None" BorderColor="White" />
            <HeaderStyle BorderStyle="None" BorderColor="White" />
            <Columns>
                <asp:BoundField DataField="DisplayData" HtmlEncode="false" SortExpression="" HeaderText="" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>

