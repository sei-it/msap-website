﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MSAPWebinarEvaluation
{
    public partial class WebinarEvaluation_NavigatingMAPS : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateResponses();
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            Response.Redirect("WebinarEvaluation_NavigationMAPSp2.aspx?a=1");
        }
        protected void SaveResponsesInSession()
        {
            Session["RegisteringForWebinarWasEasy"] = GetRblSelectedValue(rblRegistrationForWebinarWasEasy);
            Session["AccessingWebinarWasEasy"] = GetRblSelectedValue(rblAccessingWebinarEasy);
            Session["ContentWasOrganized"] = GetRblSelectedValue(rblContentOrganizedAndEasyToFollow);
            Session["PresenterWasKnowledgeable"] = GetRblSelectedValue(rblPresenterKnowledgeable);
            Session["RelevantExamplesWereUsed"] = GetRblSelectedValue(rblRelevantExamplesProvided);
            Session["AnswersPresenterGaveClear"] = GetRblSelectedValue(rblAnswersPresenterGaveClear);
            Session["QualityOfInstructionWasGood"] = GetRblSelectedValue(rblQualityOfInstruction);
            Session["IwasEngagedInTraining"] = GetRblSelectedValue(rblEngagedInTraining);
            Session["MAPSTrainingLearningObjectsWereMet"] = GetRblSelectedValue(rblMAPSTrainingLearningObjectsWereMet);
            Session["AdequateTrainingTimeProvided"] = GetRblSelectedValue(rblAdequateTrainingTimeProvided);
            Session["AbleToApplyKnowledgeLearned"] = GetRblSelectedValue(rblAbleToApplyKnowledgeLearned);
           
        }
        protected int GetRblSelectedValue(RadioButtonList rbl)
        {
            int x = 0;
            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                x = Convert.ToInt32(rbl.SelectedValue);
            }
            return x;
        }

        protected void PopulateResponses()
        {
            if (Convert.ToInt32(Session["RegisteringForWebinarWasEasy"]) > 0)
            {
               rblRegistrationForWebinarWasEasy.SelectedValue = Session["RegisteringForWebinarWasEasy"].ToString();
            }
            
            if (Convert.ToInt32(Session["AccessingWebinarWasEasy"]) > 0)
            {
                rblAccessingWebinarEasy.SelectedValue = Session["AccessingWebinarWasEasy"].ToString();
            }
            if (Convert.ToInt32(Session["ContentWasOrganized"]) > 0)
            {
                rblContentOrganizedAndEasyToFollow.SelectedValue = Session["ContentWasOrganized"].ToString();
            }
            if (Convert.ToInt32(Session["PresenterWasKnowledgeable"]) > 0)
            {
                rblPresenterKnowledgeable.SelectedValue = Session["PresenterWasKnowledgeable"].ToString();
            }
            if (Convert.ToInt32(Session["RelevantExamplesWereUsed"]) > 0)
            {
                rblRelevantExamplesProvided.SelectedValue = Session["RelevantExamplesWereUsed"].ToString();
            }
            if (Convert.ToInt32(Session["AnswersPresenterGaveClear"]) > 0)
            {
                rblAnswersPresenterGaveClear.SelectedValue = Session["AnswersPresenterGaveClear"].ToString();
            }
            if (Convert.ToInt32(Session["QualityOfInstructionWasGood"]) > 0)
            {
                rblQualityOfInstruction.SelectedValue = Session["QualityOfInstructionWasGood"].ToString();
            }
            if (Convert.ToInt32(Session["IwasEngagedInTraining"]) > 0)
            {
                rblEngagedInTraining.SelectedValue = Session["IwasEngagedInTraining"].ToString();
            }
            if (Convert.ToInt32(Session["MAPSTrainingLearningObjectsWereMet"]) > 0)
            {
                rblMAPSTrainingLearningObjectsWereMet.SelectedValue = Session["MAPSTrainingLearningObjectsWereMet"].ToString();
            }
            if (Convert.ToInt32(Session["AdequateTrainingTimeProvided"]) > 0)
            {
                rblAdequateTrainingTimeProvided.SelectedValue = Session["AdequateTrainingTimeProvided"].ToString();
            }
            if (Convert.ToInt32(Session["AbleToApplyKnowledgeLearned"]) > 0)
            {
                rblAbleToApplyKnowledgeLearned.SelectedValue = Session["AbleToApplyKnowledgeLearned"].ToString();
            }
        }


    }
}