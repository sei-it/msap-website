﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MSAPWebinarEvaluation
{
    public partial class WebinarEvaluation_NavigationMAPSp2 : System.Web.UI.Page
    {
        string connString = "Data Source=sei-db01; Initial Catalog=magnet; User ID=SynergyAppUser; Password=AppUser$";

        int a = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            
                if (!Page.IsPostBack)
                {
                    if (Convert.ToInt32(Request.QueryString["a"]) == 1)
                    {
                        PopulateResponses();
                    }
                    else
                    {
                        Response.Redirect("WebinarEvaluation_NavigatingMAPS.aspx");
                    }
                }
 

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if ((a = CheckIfAtleastOneResponseProvided()) > 0)
            {
                a = SaveResponsesToDatabase();
                if (a != 0)
                {
                    Session.Abandon();
                    Response.Redirect("ThankYouNavigationMAPS.aspx");
                }
                else
                {
                    lblMessage.Text = "The following error occured: " + Session["ErrorMessage"];
                }
            }
            else
            {
                lblMessage.Text = "Please select atleast one response to submit the survey.";
            }
        }
        protected int CheckIfAtleastOneResponseProvided()
        {
            int count = 0;
            if(Convert.ToInt32(Session["RegisteringForWebinarWasEasy"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["AccessingWebinarWasEasy"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["ContentWasOrganized"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["PresenterWasKnowledgeable"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["RelevantExamplesWereUsed"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["AnswersPresenterGaveClear"] )>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["QualityOfInstructionWasGood"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["IwasEngagedInTraining"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["MAPSTrainingLearningObjectsWereMet"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32(Session["AdequateTrainingTimeProvided"])>0)
            {
                count ++;
            }
            if(Convert.ToInt32( Session["AbleToApplyKnowledgeLearned"])>0)
            {
                count ++;
            }
           // if(!String.IsNullOrEmpty(txtAdditionalComments.Text.Trim()))
           if( Session != null && Session["AdditionalComments"] != null)
            {
                if (!String.IsNullOrEmpty(txtAdditionalComments.Text.Trim()))
                {
                    count++;
                    Session["AdditionalComments"] = txtAdditionalComments.Text.Trim();
                }
            }
           // if(!String.IsNullOrEmpty(txtAnythingYouDidNotUnderstand.Text.Trim()))
           if (Session != null && Session["AnythingYouDidNotUnderstand"] != null)
           {
               if (!String.IsNullOrEmpty(txtAnythingYouDidNotUnderstand.Text.Trim()))
               {
                   count++;
                   Session["AnythingYouDidNotUnderstand"] = txtAnythingYouDidNotUnderstand.Text.Trim();
               }
            }
          //  if (!String.IsNullOrEmpty(txtHowCanWeImproveTraining.Text.Trim()))
           if (Session != null && Session["HowCanWeImproveTraining"] != null)
            {
                if (!String.IsNullOrEmpty(txtHowCanWeImproveTraining.Text.Trim()))
                {
                    count++;
                    Session["HowCanWeImproveTraining"] = txtHowCanWeImproveTraining.Text.Trim();
                }
            }
         //   if (!String.IsNullOrEmpty(txtMostusefulThingYouLearned.Text.Trim()))
           if (Session != null && Session["MostusefulThingYouLearned"] != null)
            {
                if (!String.IsNullOrEmpty(txtMostusefulThingYouLearned.Text.Trim()))
                {
                    count++;
                    Session["MostusefulThingYouLearned"] = txtMostusefulThingYouLearned.Text.Trim();
                }
            }
            return count;
        }
       
        protected int SaveResponsesToDatabase()
        {
            int x = 0;

            int RegisteringForWebinarWasEasy = Convert.ToInt32(Session["RegisteringForWebinarWasEasy"].ToString());
            int AccessingWebinarWasEasy = Convert.ToInt32(Session["AccessingWebinarWasEasy"].ToString());
            int ContentWasOrganized = Convert.ToInt32(Session["ContentWasOrganized"].ToString());
            int PresenterWasKnowledgeable = Convert.ToInt32(Session["PresenterWasKnowledgeable"].ToString());
            int RelevantExamplesWereUsed = Convert.ToInt32(Session["RelevantExamplesWereUsed"].ToString());
            int AnswersPresenterGaveClear = Convert.ToInt32(Session["AnswersPresenterGaveClear"].ToString());
            int QualityOfInstructionWasGood = Convert.ToInt32(Session["QualityOfInstructionWasGood"].ToString());
            int IwasEngagedInTraining = Convert.ToInt32(Session["IwasEngagedInTraining"].ToString());
            int MAPSTrainingLearningObjectsWereMet = Convert.ToInt32(Session["MAPSTrainingLearningObjectsWereMet"].ToString());
            int AdequateTrainingTimeProvided = Convert.ToInt32(Session["AdequateTrainingTimeProvided"].ToString());
            int AbleToApplyKnowledgeLearned = Convert.ToInt32(Session["AbleToApplyKnowledgeLearned"].ToString());
            string AnythingYouDidNotUnderstand = Session["AnythingYouDidNotUnderstand"].ToString();
            string HowCanWeImproveTraining = Session["HowCanWeImproveTraining"].ToString();
            string MostusefulThingYouLearned = Session["MostusefulThingYouLearned"].ToString();
            string AdditionalComments = Session["AdditionalComments"].ToString();

            x= InsertIntoDatabse(RegisteringForWebinarWasEasy
                                , AccessingWebinarWasEasy
                                , ContentWasOrganized
                                , PresenterWasKnowledgeable
                                , RelevantExamplesWereUsed
                                , AnswersPresenterGaveClear
                                , QualityOfInstructionWasGood
                                , IwasEngagedInTraining
                                , MAPSTrainingLearningObjectsWereMet
                                , AdequateTrainingTimeProvided
                                , AbleToApplyKnowledgeLearned
                                , AnythingYouDidNotUnderstand
                                , HowCanWeImproveTraining
                                , MostusefulThingYouLearned
                                , AdditionalComments);

            return x;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtAdditionalComments.Text.Trim()))
            {
                Session["AdditionalComments"] = txtAdditionalComments.Text.Trim();
            }
            if (!String.IsNullOrEmpty(txtAnythingYouDidNotUnderstand.Text.Trim()))
            {
                Session["AnythingYouDidNotUnderstand"] = txtAnythingYouDidNotUnderstand.Text.Trim();
            }
            if (!String.IsNullOrEmpty(txtHowCanWeImproveTraining.Text.Trim()))
            {
                Session["HowCanWeImproveTraining"] = txtHowCanWeImproveTraining.Text.Trim();
            }
            if (!String.IsNullOrEmpty(txtMostusefulThingYouLearned.Text.Trim()))
            {
                Session["MostusefulThingYouLearned"] = txtMostusefulThingYouLearned.Text.Trim();
            }

            Response.Redirect("WebinarEvaluation_NavigatingMAPS.aspx");
        }

        protected void PopulateResponses()
        {
            if (Session != null && Session["AdditionalComments"] != null)
            {
                txtAdditionalComments.Text = Session["AdditionalComments"].ToString();
            }
            else
            {
                Session["AdditionalComments"] = "";
            }
            if (Session != null && Session["AnythingYouDidNotUnderstand"]!=null)
            {
                txtAnythingYouDidNotUnderstand.Text = Session["AnythingYouDidNotUnderstand"].ToString();
            }
            else
            {
                Session["AnythingYouDidNotUnderstand"] = "";
            }
            if (Session != null && Session["HowCanWeImproveTraining"]!=null)
            {
                txtHowCanWeImproveTraining.Text = Session["HowCanWeImproveTraining"].ToString();
            }
            else
            {
                Session["HowCanWeImproveTraining"] = "";
            }
            if (Session != null && Session["MostusefulThingYouLearned"]!=null)
            {
                txtMostusefulThingYouLearned.Text = Session["MostusefulThingYouLearned"].ToString();
            }
            else
            {
                Session["MostusefulThingYouLearned"] = "";
            }
        }

        protected int InsertIntoDatabse(int RegisteringForWebinarWasEasy
                                        ,int AccessingWebinarWasEasy
                                        ,int ContentWasOrganized
                                        ,int PresenterWasKnowledgeable
                                        ,int RelevantExamplesWereUsed
                                        ,int AnswersPresenterGaveClear
                                        ,int QualityOfInstructionWasGood
                                        ,int IwasEngagedInTraining
                                        ,int MAPSTrainingLearningObjectsWereMet
                                        ,int AdequateTrainingTimeProvided
                                        ,int AbleToApplyKnowledgeLearned
                                        ,string AnythingYouDidNotUnderstand
                                        ,string HowCanWeImproveTraining
                                        ,string MostusefulThingYouLearned
                                        ,string AdditionalComments)
        {
            a = 0;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("WebinarEvaluation_NavigatingMAPS_Insert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RegisteringForWebinarWasEasy", RegisteringForWebinarWasEasy);
            cmd.Parameters.Add("@AccessingWebinarWasEasy", AccessingWebinarWasEasy);
            cmd.Parameters.Add("@ContentWasOrganized", ContentWasOrganized);
            cmd.Parameters.Add("@PresenterWasKnowledgeable", PresenterWasKnowledgeable);
            cmd.Parameters.Add("@RelevantExamplesWereUsed", RelevantExamplesWereUsed);
            cmd.Parameters.Add("@AnswersPresenterGaveClear", AnswersPresenterGaveClear);
            cmd.Parameters.Add("@QualityOfInstructionWasGood", QualityOfInstructionWasGood);
            cmd.Parameters.Add("@IwasEngagedInTraining", IwasEngagedInTraining);
            cmd.Parameters.Add("@MAPSTrainingLearningObjectsWereMet", MAPSTrainingLearningObjectsWereMet);
            cmd.Parameters.Add("@AdequateTrainingTimeProvided", AdequateTrainingTimeProvided);
            cmd.Parameters.Add("@AbleToApplyKnowledgeLearned", AbleToApplyKnowledgeLearned);
            cmd.Parameters.Add("@AnythingYouDidNotUnderstand", AnythingYouDidNotUnderstand);
            cmd.Parameters.Add("@HowCanWeImproveTraining", HowCanWeImproveTraining);
            cmd.Parameters.Add("@MostusefulThingYouLearned", MostusefulThingYouLearned);
            cmd.Parameters.Add("@AdditionalComments", AdditionalComments);
            cmd.Parameters.Add("@DateEntered", DateTime.Now);
            

            try
            {
                conn.Open();
                a = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            return a;
        }
    }
}