﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Webinar_Evaluation_PuttingItAllTogetherP2.aspx.cs" Inherits="MSAPWebinarEvaluation.Webinar_Evaluation_PuttingItAllTogetherP2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Putting It All Together</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.txtArea 
{
	width:600px;
	height:100px;
}

.Rating
{
	width:20px;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 12px;
    /*font-weight: bold;*/
	font-style:italic;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}



#download_list li{
	padding-bottom:10px;
}

#main_tbl{
	width:80%;
	padding:0px;
	margin:0px;
	border-collapse:collapse !important;
}

#main_tbl td:first-child{
	padding-left:10px;
}

#main_tbl td{
	width:20%;
	vertical-align:top;
	border-bottom:1px solid #A9DEF2;
	padding:5px 0px;
}

#main_tbl th{
	font-size:11px;
}
#tbl2{
	width:80%;
	padding:0px;
	margin:0px;
	border-collapse:collapse !important;
}

#tbl2 td
{
	padding:5px 0px;
}

.sub_tbls{
	width:50%;
	text-align:center;
	float:right;
	border:0px;
	padding:0px;
	margin:0px;
}

.sub_tbls2{
	width:100%;
	text-align:center;
	float:right;
	border:0px;
	padding:0px;
	margin:0px;
}

.sub_tbls2 td{
	width:25% !important;
	border:0px !important;
	padding:5px 0px;
}

.sub_titles{
	font-weight:bold;
}

.sub_titles_top{
	font-weight:bold;
	padding-top:30px;
}

.surveyBtn1 {
    background: url("images/surveybutton2.png") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF !important;
    font-size: 10px;
    font-weight: bold;
	min-height:14px;
    padding-bottom: 3px;
	padding-top:2px;
    width: 115px !important;
	float:right;
	text-align:center;
}

.underlineTd {
	border-bottom-style:solid; border-bottom:1px; border-bottom-color:#A9DEF2; padding:5px;border-collapse:collapse;
}
.underlineTd2 {
	border-bottom-style:solid; border-bottom:1px; border-bottom-color:#A9DEF2; padding:5px;border-collapse:collapse; width:35%;
}

    .style1
    {
        width: 97px;
    }
    .style2
    {
        width: 47px;
    }

    .style3
    {
        width: 82px;
    }

-->
</style></head>
<body class="oneColElsCtrHdr">
    <form id="form1" runat="server">
   <div id="container">
  <div id="header">
    <img src="images/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  
  <div id="mainContent">
    <h1>Planning for Sustainability: Putting It All Together</h1>

        <p><asp:Label runat="server" ID="lblMessage" ForeColor="Red" Font-Bold="true"></asp:Label></p>


<p class="sub_titles">Rate the following statements. Select one response for each.</p>
    <table class="style1" id="main_tbl" border="0">
        <tr style="background-color:#A9DEF2;">
            <td colspan="2">
                <table class="sub_tbls">
                    <tr>
                        <th class="style3">Strongly<br />
                            Agree</th>
                        <th>Agree </th>
                        <th class="style2">Disagree</th>
                        <th>Strongly <br />
                        Disagree</th>
                    </tr>
                </table>
            </td> 
        </tr>
    </table>
    <table width="80%" id="tbl2" >

        <tr>
            <td class="underlineTd">Registering for the webinar was easy.</td>
            
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblRegistrationForWebinarWasEasy" runat="server" 
                    RepeatDirection="Horizontal" Width="328px" style="margin-left: 0px" >
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>
                </asp:RadioButtonList> </td>
            
        </tr>
        <tr>
            <td class="underlineTd">Accessing the webinar was easy.</td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblAccessingWebinarEasy" runat="server" RepeatDirection="Horizontal" Width="328px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>            
        </tr>
        <tr>
            <td class="underlineTd">The speakers were knowledgeable on the subject matter.</td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblPresenterKnowledgeable" runat="server" RepeatDirection="Horizontal" Width="328px" >
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>           
        </tr>
        <tr>
            <td class="underlineTd">The webinar was well organized.</td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblWebinarWellOrganized" runat="server" RepeatDirection="Horizontal" Width="328px" >
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>           
        </tr>



        <tr>
            <td class="underlineTd">Relevant examples were used to illustrate concepts.</td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblRelevantExamplesProvided" runat="server" RepeatDirection="Horizontal" Width="328px" >
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>             
        </tr>
        <tr>
            <td class="underlineTd">The speakers adequately responded to questions.</td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblSpeakersAdequatelyResponded" runat="server" RepeatDirection="Horizontal" Width="328px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>        </tr>
         <tr>
            <td class="underlineTd">I will complete the provided tools.</td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblCompleteProvidedTools" runat="server" RepeatDirection="Horizontal" Width="328px" >
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>        </tr>
        <tr>
            <td class="underlineTd">I will engage in the final Planning for Sustainability live chat.</td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblEnagageInliveChats" runat="server" RepeatDirection="Horizontal" Width="328px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>                
        </tr>
        <tr>
            <td class="underlineTd">I will take advantage of consultative services with The Finance Project before June 30, 2012.  </td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblTakeAdvantageOfConsultativeServices" runat="server" RepeatDirection="Horizontal" Width="328px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>

        </tr>
          <tr>
            <td class="underlineTd">The MSAP Center should continue working with The Finance <br /> Project to provide services in program sustainability and strategic financing.  </td>
            <td class="underlineTd" style=" float:right;">              
                <asp:RadioButtonList ID="rblContinueWorkingWithTheFinanceProject" runat="server" RepeatDirection="Horizontal" Width="328px" >
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>        </tr>

    </table>

<p style="text-align:right;width:80%;padding-top:30px;">
 <asp:Button runat="server" ID="btnSubmit" CssClass="surveyBtn1" Text="Submit" 
        onclick="btnSubmit_Click"/>
   <asp:Button runat="server" ID="btnBack" CssClass="surveyBtn1" Text="Back" 
        onclick="btnBack_Click"/>
   
    </p>


	<!-- end #mainContent --></div>
    <br />
 <div id="footer">

    <img src="images/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>
    </form>
</body>
</html>
