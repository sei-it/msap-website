﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MSAPWebinarEvaluation
{
    public partial class Webinar_Evaluation_PuttingItAllTogetherP2 : System.Web.UI.Page
    {
        string connString = "Data Source=sei-db01; Initial Catalog=magnet; User ID=SynergyAppUser; Password=AppUser$";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Convert.ToInt32(Request.QueryString["a"]) == 1)
                {
                    PopulateResponses();
                }
                else
                {
                    Response.Redirect("Webinar_Evaluation_PuttingItAllTogether.aspx");
                }
            }
        }
        protected void PopulateResponses()
        {
            if (Convert.ToInt32(Session["RegisteringEasy"]) > 0)
            {
                rblRegistrationForWebinarWasEasy.SelectedValue = Session["RegisteringEasy"].ToString();
            }

            if (Convert.ToInt32(Session["AccessingEasy"]) > 0)
            {
                rblAccessingWebinarEasy.SelectedValue = Session["AccessingEasy"].ToString();
            }
            if (Convert.ToInt32(Session["SpeakersKnowledgeable"]) > 0)
            {

                rblPresenterKnowledgeable.SelectedValue = Session["SpeakersKnowledgeable"].ToString();
            }
            if (Convert.ToInt32(Session["WebinarWellOrganized"]) > 0)
            {
                rblWebinarWellOrganized.SelectedValue = Session["WebinarWellOrganized"].ToString();
            }
            if (Convert.ToInt32(Session["Relevantexamples"]) > 0)
            {
                rblRelevantExamplesProvided.SelectedValue = Session["Relevantexamples"].ToString();
            }
            if (Convert.ToInt32(Session["SpeakersAdequatelyResponded"]) > 0)
            {
                rblSpeakersAdequatelyResponded.SelectedValue = Session["SpeakersAdequatelyResponded"].ToString();
            }
            if (Convert.ToInt32(Session["CompleteProvidedTools"]) > 0)
            {
                rblCompleteProvidedTools.SelectedValue = Session["CompleteProvidedTools"].ToString();
            }
            //////////
            if (Convert.ToInt32(Session["EnagageInliveChats"]) > 0)
            {
                rblEnagageInliveChats.SelectedValue = Session["EnagageInliveChats"].ToString();
            }
            if (Convert.ToInt32(Session["TakeAdvantageOfConsultativeServices"]) > 0)
            {
                rblTakeAdvantageOfConsultativeServices.SelectedValue = Session["TakeAdvantageOfConsultativeServices"].ToString();
            }
            if (Convert.ToInt32(Session["ContinueWorkingWithTheFinanceProject"]) > 0)
            {
                rblContinueWorkingWithTheFinanceProject.SelectedValue = Session["ContinueWorkingWithTheFinanceProject"].ToString();
            }

        }


        protected void btnBack_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            Response.Redirect("Webinar_Evaluation_PuttingItAllTogether.aspx");
        }
        protected void SaveResponsesInSession()
        {
            Session["RegisteringEasy"]=GetRblSelectedValue(rblRegistrationForWebinarWasEasy);
            Session["AccessingEasy"]=GetRblSelectedValue(rblAccessingWebinarEasy);
            Session["SpeakersKnowledgeable"] = GetRblSelectedValue(rblPresenterKnowledgeable);
            Session["WebinarWellOrganized"]=GetRblSelectedValue(rblWebinarWellOrganized);
            Session["Relevantexamples"]=GetRblSelectedValue(rblRelevantExamplesProvided);
            Session["SpeakersAdequatelyResponded"] = GetRblSelectedValue(rblSpeakersAdequatelyResponded);
            Session["CompleteProvidedTools"]=GetRblSelectedValue(rblCompleteProvidedTools);
            Session["EnagageInliveChats"]=GetRblSelectedValue(rblEnagageInliveChats);
            Session["TakeAdvantageOfConsultativeServices"] = GetRblSelectedValue(rblTakeAdvantageOfConsultativeServices);
            Session["ContinueWorkingWithTheFinanceProject"] = GetRblSelectedValue(rblContinueWorkingWithTheFinanceProject);
        }
        protected int GetRblSelectedValue(RadioButtonList rbl)
        {
            int x = 0;
            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                x = Convert.ToInt32(rbl.SelectedValue);
            }
            return x;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            if (CheckIfAtleastOneResponseProvided() > 0)
            {
              int a=  SaveResponsesInSessionToDB();
              if (a != 0)
              {
                  Session.Clear();
                  Response.Redirect("ThankYouPuttingItAllTogether.aspx");
              }
              else
              {
                  lblMessage.Text = "The following error occured: " + Session["ErrorMessage"];
              }
            }
            else
            {
                lblMessage.Text = "Please select atleast one response to submit the survey.";
            }
           
        }
        protected int SaveResponsesInSessionToDB()
        {
            int isAdded = 0;

           int RegisteringEasy=Convert.ToInt32(Session["RegisteringEasy"]);
           int AccessingEasy=Convert.ToInt32(Session["AccessingEasy"]);
           int SpeakersKnowledgeable=Convert.ToInt32(Session["SpeakersKnowledgeable"]);
           int WebinarWellOrganized=Convert.ToInt32(Session["WebinarWellOrganized"]);
           int Relevantexamples=Convert.ToInt32(Session["Relevantexamples"]);
           int SpeakersAdequatelyResponded=Convert.ToInt32(Session["SpeakersAdequatelyResponded"]);
           int CompleteProvidedTools=Convert.ToInt32(Session["CompleteProvidedTools"]);
           int EnagageInliveChats=Convert.ToInt32(Session["EnagageInliveChats"]);
           int TakeAdvantageOfConsultativeServices=Convert.ToInt32(Session["TakeAdvantageOfConsultativeServices"]);
           int ContinueWorkingWithTheFinanceProject = Convert.ToInt32(Session["ContinueWorkingWithTheFinanceProject"]);
           string IsFurtherTraining = GetSessionBitValue(Session["IsFurtherTraining"].ToString());
           string FurtherTrainingNo=Session["FurtherTrainingNo"].ToString();
           string IfFurtherSustainabilityTraining=Session["IfFurtherSustainabilityTraining"].ToString();
           string IsWebBasedCommunity = GetSessionBitValue(Session["IsWebBasedCommunity"].ToString());
           string WebBasedCommunityNo=Session["WebBasedCommunityNo"].ToString();
           string IsEngagedInOnlineCommunities = GetSessionBitValue(Session["IsEngagedInOnlineCommunities"].ToString());
           string EngagedInOnlineCommunitiesYes=Session["EngagedInOnlineCommunitiesYes"].ToString();
           string MostHelpful=Session["MostHelpful"].ToString();
           string AdditionalComments=Session["AdditionalComments"].ToString();
           
            isAdded = InsertIntoDB(RegisteringEasy, AccessingEasy, SpeakersKnowledgeable, WebinarWellOrganized, Relevantexamples, SpeakersAdequatelyResponded
            , CompleteProvidedTools, EnagageInliveChats, TakeAdvantageOfConsultativeServices, ContinueWorkingWithTheFinanceProject, IsFurtherTraining
            , FurtherTrainingNo, IfFurtherSustainabilityTraining, IsWebBasedCommunity, WebBasedCommunityNo, IsEngagedInOnlineCommunities
            , EngagedInOnlineCommunitiesYes, MostHelpful, AdditionalComments);

            return isAdded;
            
        }
        protected int CheckIfAtleastOneResponseProvided()
        {
            int count = 0;
            if (Session != null && Session["IsFurtherTraining"] != null )
            {
                if (!String.IsNullOrEmpty(Session["IsFurtherTraining"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["FurtherTrainingNo"] != null)
            {
                if (!String.IsNullOrEmpty(Session["FurtherTrainingNo"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["IfFurtherSustainabilityTraining"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IfFurtherSustainabilityTraining"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["IsWebBasedCommunity"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsWebBasedCommunity"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["WebBasedCommunityNo"] != null)
            {
                if (!String.IsNullOrEmpty(Session["WebBasedCommunityNo"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["IsEngagedInOnlineCommunities"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsEngagedInOnlineCommunities"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["EngagedInOnlineCommunitiesYes"] != null)
            {
                if (!String.IsNullOrEmpty(Session["EngagedInOnlineCommunitiesYes"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["MostHelpful"] != null)
            {
                if (!String.IsNullOrEmpty(Session["MostHelpful"].ToString()))
                {
                    count++;
                }
            }
            if (Session != null && Session["AdditionalComments"] != null)
            {
                if (!String.IsNullOrEmpty(Session["AdditionalComments"].ToString()))
                {
                    count++;
                }
            }
            if (Convert.ToInt32(Session["RegisteringEasy"]) > 0)
            {
                count++;
            }

            if (Convert.ToInt32(Session["AccessingEasy"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["SpeakersKnowledgeable"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["WebinarWellOrganized"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["Relevantexamples"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["SpeakersAdequatelyResponded"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["CompleteProvidedTools"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["EnagageInliveChats"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["TakeAdvantageOfConsultativeServices"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["ContinueWorkingWithTheFinanceProject"]) > 0)
            {
                count++;
            }
            return count;
        }

        protected int InsertIntoDB(int RegisteringEasy,int AccessingEasy,int SpeakersKnowledgeable,int WebinarWellOrganized,int Relevantexamples,int SpeakersAdequatelyResponded
            , int CompleteProvidedTools, int EnagageInliveChats, int TakeAdvantageOfConsultativeServices, int ContinueWorkingWithTheFinanceProject, string IsFurtherTraining
            ,string FurtherTrainingNo,string IfFurtherSustainabilityTraining,string IsWebBasedCommunity,string WebBasedCommunityNo,string IsEngagedInOnlineCommunities
            ,string EngagedInOnlineCommunitiesYes,string MostHelpful,string AdditionalComments)
        {
            int added=0;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("Webinar_Evaluation_PuttingItAllTogether_Insert", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RegisteringEasy", RegisteringEasy);
            cmd.Parameters.Add("@AccessingEasy", AccessingEasy);
            cmd.Parameters.Add("@SpeakersKnowledgeable", SpeakersKnowledgeable);
            cmd.Parameters.Add("@WebinarWellOrganized", WebinarWellOrganized);
            cmd.Parameters.Add("@Relevantexamples",Relevantexamples );
            cmd.Parameters.Add("@SpeakersAdequatelyResponded", SpeakersAdequatelyResponded);
            cmd.Parameters.Add("@CompleteProvidedTools", CompleteProvidedTools);
            cmd.Parameters.Add("@EnagageInliveChats", EnagageInliveChats);
            cmd.Parameters.Add("@TakeAdvantageOfConsultativeServices", TakeAdvantageOfConsultativeServices);
            cmd.Parameters.Add("@ContinueWorkingWithTheFinanceProject",ContinueWorkingWithTheFinanceProject );
            cmd.Parameters.Add("@IsFurtherTraining",IsFurtherTraining );
            cmd.Parameters.Add("@FurtherTrainingNo",FurtherTrainingNo );
            cmd.Parameters.Add("@IfFurtherSustainabilityTraining",IfFurtherSustainabilityTraining );
            cmd.Parameters.Add("@IsWebBasedCommunity",IsWebBasedCommunity );
            cmd.Parameters.Add("@WebBasedCommunityNo",WebBasedCommunityNo );
            cmd.Parameters.Add("@IsEngagedInOnlineCommunities", IsEngagedInOnlineCommunities);
            cmd.Parameters.Add("@EngagedInOnlineCommunitiesYes",EngagedInOnlineCommunitiesYes );
            cmd.Parameters.Add("@MostHelpful",MostHelpful );
            cmd.Parameters.Add("@AdditionalComments", AdditionalComments);
            cmd.Parameters.Add("@DateSubmitted", DateTime.Now);

            try
            {
                conn.Open();
                added = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

            return added;
        }


        protected string GetSessionBitValue(string checkValue)
        {
            string retVal = null;
            if (!String.IsNullOrEmpty(checkValue))
            {
                retVal = checkValue;
            }

            return retVal;
        }
    }
}