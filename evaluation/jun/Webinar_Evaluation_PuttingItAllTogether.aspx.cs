﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MSAPWebinarEvaluation
{
    public partial class Webinar_Evaluation_PuttingItAllTogether : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateResponses();
            }

            Page.MaintainScrollPositionOnPostBack = true;
        }

        protected void PopulateResponses()
        {
           if (Session != null && Session["IsFurtherTraining"] !=null)
           {
               rblFurtherTraining.SelectedValue = Session["IsFurtherTraining"].ToString();
           }
           if (Session != null &&  Session["FurtherTrainingNo"] !=null)
           {
               txtFurtherTrainingNo.Text = Session["FurtherTrainingNo"].ToString();
           }
           if (Session != null &&  Session["IfFurtherSustainabilityTraining"] !=null)
           {
               txtIfFurtherSustainabilityTraining.Text = Session["IfFurtherSustainabilityTraining"].ToString();
           }
           if (Session != null &&  Session["IsWebBasedCommunity"] !=null)
           {
               rblWebBasedCommunity.SelectedValue = Session["IsWebBasedCommunity"].ToString();
           }
           if (Session != null && Session["WebBasedCommunityNo"] != null)
           {
               txtWebBasedCommunityNo.Text = Session["WebBasedCommunityNo"].ToString();
           }
           if (Session != null &&  Session["IsEngagedInOnlineCommunities"] !=null)
           {
               rblEngagedInOnlineCommunities.SelectedValue = Session["IsEngagedInOnlineCommunities"].ToString();
           }
           if (Session != null &&  Session["EngagedInOnlineCommunitiesYes"] !=null)
           {
               txtEngagedInOnlineCommunitiesYes.Text = Session["EngagedInOnlineCommunitiesYes"].ToString();
           }
           if (Session != null &&  Session["MostHelpful"] !=null)
           {
               txtMostHelpful.Text = Session["MostHelpful"].ToString();
           }
           if (Session != null &&  Session["AdditionalComments"] !=null)
           {
               txtAdditionalComments.Text = Session["AdditionalComments"].ToString();
           }
        }

        protected void rblFurtherTraining_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblFurtherTraining.SelectedValue == "0")
            {
                lbltxtFurtherTrainingNo.Visible = true;
                txtFurtherTrainingNo.Visible = true;
            }
            else if (rblFurtherTraining.SelectedValue == "1")
            {
                lbltxtFurtherTrainingNo.Visible = false;
                txtFurtherTrainingNo.Visible = false;
            }
        }

        protected void rblWebBasedCommunity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblWebBasedCommunity.SelectedValue == "0")
            {
                lblWebBased.Visible = true;
                txtWebBasedCommunityNo.Visible = true;
            }
            else if (rblWebBasedCommunity.SelectedValue == "1")
            {
                lblWebBased.Visible = false;
                txtWebBasedCommunityNo.Visible = false;
            }
        }

        protected void rblEngagedInOnlineCommunities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblEngagedInOnlineCommunities.SelectedValue == "1")
            {
                lblEngagedInOnlineCommunitiesYes.Visible = true;
                txtEngagedInOnlineCommunitiesYes.Visible = true;
            }
            else if (rblEngagedInOnlineCommunities.SelectedValue == "0")
            {
                lblEngagedInOnlineCommunitiesYes.Visible = false;
                txtEngagedInOnlineCommunitiesYes.Visible = false;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            Response.Redirect("Webinar_Evaluation_PuttingItAllTogetherP2.aspx?a=1");
        }

        protected void SaveResponsesInSession()
        {
            Session["IsFurtherTraining"]=GetRblSelectedValue(rblFurtherTraining);
            Session["FurtherTrainingNo"]=txtFurtherTrainingNo.Text;
            Session["IfFurtherSustainabilityTraining"]=txtIfFurtherSustainabilityTraining.Text;
            Session["IsWebBasedCommunity"]=GetRblSelectedValue(rblWebBasedCommunity);
            Session["WebBasedCommunityNo"]=txtWebBasedCommunityNo.Text;
            Session["IsEngagedInOnlineCommunities"]=GetRblSelectedValue(rblEngagedInOnlineCommunities);
            Session["EngagedInOnlineCommunitiesYes"]=txtEngagedInOnlineCommunitiesYes.Text;
            Session["MostHelpful"]=txtMostHelpful.Text;
            Session["AdditionalComments"] = txtAdditionalComments.Text;
        }

        protected string GetRblSelectedValue(RadioButtonList rbl)
        {
            string s = "";

            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                s = rbl.SelectedValue;
            }
            return s;
        }

       
    }
}