﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MSAPWebinarEvaluation
{
    public partial class WebinarEvaluation_AligningProjectActivities : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateResponses();
            }
        }

        protected void PopulateResponses()
        {
            if (Convert.ToInt32(Session["RegisteringEasy"]) > 0)
            {
                rblRegistrationForWebinarWasEasy.SelectedValue = Session["RegisteringEasy"].ToString();
            }

            if (Convert.ToInt32(Session["AccessingEasy"]) > 0)
            {
                rblAccessingWebinarEasy.SelectedValue = Session["AccessingEasy"].ToString();
            }
            if (Convert.ToInt32(Session["SpeakersKnowledgeable"]) > 0)
            {
                
                rblPresenterKnowledgeable.SelectedValue = Session["SpeakersKnowledgeable"].ToString();
            }
            if (Convert.ToInt32(Session["WellOrganized"]) > 0)
            {
                rblContentOrganizedAndEasyToFollow.SelectedValue = Session["WellOrganized"].ToString();
            }
            if (Convert.ToInt32(Session["RelevantExamplesUsed"]) > 0)
            {
                rblRelevantExamplesProvided.SelectedValue = Session["RelevantExamplesUsed"].ToString();
            }
            if (Convert.ToInt32(Session["SpeakersAdequatelyResponded"]) > 0)
            {
                rblSpeakersAdequatelyResponded.SelectedValue = Session["SpeakersAdequatelyResponded"].ToString();
            }
            if (Convert.ToInt32(Session["CompleteProvidedTools"]) > 0)
            {
                rblCompleteProvidedTools.SelectedValue = Session["CompleteProvidedTools"].ToString();
            }
            if (Convert.ToInt32(Session["AttendNextWebinar"]) > 0)
            {
                rblAttendNextWebinar.SelectedValue = Session["AttendNextWebinar"].ToString();
            }
            if (Convert.ToInt32(Session["LiveChatOrConsultativeServices"]) > 0)
            {
                rblLiveChats.SelectedValue = Session["LiveChatOrConsultativeServices"].ToString();
            }
            if (Convert.ToInt32(Session["FacebookOrTwitter"]) > 0)
            {
                rblFacebookTwitter.SelectedValue = Session["FacebookOrTwitter"].ToString();
            }
           
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            Response.Redirect("WebinarEvaluation_AligningProjectActivitiesP2.aspx?a=1");
        }

        protected void SaveResponsesInSession()
        {
            Session["RegisteringEasy"] = GetRblSelectedValue(rblRegistrationForWebinarWasEasy);
            Session["AccessingEasy"] = GetRblSelectedValue(rblAccessingWebinarEasy);
            Session["SpeakersKnowledgeable"] = GetRblSelectedValue(rblPresenterKnowledgeable);
            Session["WellOrganized"] = GetRblSelectedValue(rblContentOrganizedAndEasyToFollow);
            Session["RelevantExamplesUsed"] = GetRblSelectedValue(rblRelevantExamplesProvided);
            Session["SpeakersAdequatelyResponded"] = GetRblSelectedValue(rblSpeakersAdequatelyResponded);
            Session["CompleteProvidedTools"] = GetRblSelectedValue(rblCompleteProvidedTools);
            Session["AttendNextWebinar"] = GetRblSelectedValue(rblAttendNextWebinar);
            Session["LiveChatOrConsultativeServices"] = GetRblSelectedValue(rblLiveChats);
            Session["FacebookOrTwitter"] = GetRblSelectedValue(rblFacebookTwitter);
        }

        protected int GetRblSelectedValue(RadioButtonList rbl)
        {
            int x = 0;
            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                x = Convert.ToInt32(rbl.SelectedValue);
            }
            return x;
        }

    }
}