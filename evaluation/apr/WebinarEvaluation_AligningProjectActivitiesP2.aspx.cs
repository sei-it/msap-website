﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MSAPWebinarEvaluation
{
    public partial class WebinarEvaluation_AligningProjectActivitiesP2 : System.Web.UI.Page
    {
        string connString = "Data Source=sei-db01; Initial Catalog=magnet; User ID=SynergyAppUser; Password=AppUser$";

        int a = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Convert.ToInt32(Request.QueryString["a"]) == 1)
                {
                    PopulateResponses();
                }
                else
                {
                    Response.Redirect("WebinarEvaluation_AligningProjectActivities.aspx");
                }
            }
        }

        protected void PopulateResponses()
        {
            if(Session!=null && Session["IsFundingLevelRequiredToSustain"]!=null)
            {
              rblFundingLevel.SelectedValue  =Session["IsFundingLevelRequiredToSustain"].ToString();
            }
            else
            {
                 Session["IsFundingLevelRequiredToSustain"]="";
            }

            if (Session != null && Session["IsMSLeadershipUsingAdditionalFinancing"] != null)
            {
               rblAdditionalFinancing.SelectedValue = Session["IsMSLeadershipUsingAdditionalFinancing"].ToString();
            }
            else
            {
                Session["IsMSLeadershipUsingAdditionalFinancing"] = "";
            }

            if (Session != null && Session["FinancingOptions"] != null)
            {
                txtFinancingOptions.Text = Session["FinancingOptions"].ToString();
            }
            else
            {
                Session["FinancingOptions"] = "";
            }

            if (Session != null && Session["IsPlanInPlace"] != null)
            {
                rblPlanInPlace.SelectedValue = Session["IsPlanInPlace"].ToString();
            }
            else
            {
                Session["IsPlanInPlace"] = "";
            }
            
            if(Session!=null && Session["IsWorkingToEstablishRelationships"]!=null)
            {
              rblEstablishRelationships.SelectedValue  =Session["IsWorkingToEstablishRelationships"].ToString();
            }
            else
            {
                 Session["IsWorkingToEstablishRelationships"]="";
            }

            if (Session != null && Session["RelationshipsExamples"] != null)
            {
                txtEstablishRelationships.Text = Session["RelationshipsExamples"].ToString();
            }
            else
            {
                Session["RelationshipsExamples"] = "";
            }



            if (Session != null && Session["AdditionalComments"] != null)
            {
                txtAdditionalComments.Text = Session["AdditionalComments"].ToString();
            }
            else
            {
                Session["AdditionalComments"] = "";
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            Response.Redirect("WebinarEvaluation_AligningProjectActivities.aspx");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            if ((a = CheckIfAtleastOneResponseProvided()) > 0)
            {
               // SaveResponsesInSession();
                a = SaveResponsesToDatabase();
                if (a != 0)
                {
                    Session.Abandon();
                    Response.Redirect("ThankYouAligningProjectActivities.aspx");
                }
                else
                {
                    lblMessage.Text = "The following error occured: " + Session["ErrorMessage"];
                }
            }
            else
            {
                lblMessage.Text = "Please select atleast one response to submit the survey.";
            }
        }

        protected int CheckIfAtleastOneResponseProvided()
        {
            int count = 0;
            if (Convert.ToInt32(Session["RegisteringEasy"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["AccessingEasy"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["SpeakersKnowledgeable"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["WellOrganized"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["RelevantExamplesUsed"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["SpeakersAdequatelyResponded"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["CompleteProvidedTools"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["AttendNextWebinar"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["LiveChatOrConsultativeServices"]) > 0)
            {
                count++;
            }
            if (Convert.ToInt32(Session["FacebookOrTwitter"]) > 0)
            {
                count++;
            }
            if (Session != null && Session["IsFundingLevelRequiredToSustain"]!=null)
            {
                if (!String.IsNullOrEmpty(Session["IsFundingLevelRequiredToSustain"].ToString()))
                {
                    count++;
                    Session["IsFundingLevelRequiredToSustain"] = GetRblSelectedValue(rblFundingLevel);
                }
            }

            if (Session != null && Session["IsMSLeadershipUsingAdditionalFinancing"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsMSLeadershipUsingAdditionalFinancing"].ToString()))
                {
                    count++;
                    Session["IsMSLeadershipUsingAdditionalFinancing"] = GetRblSelectedValue(rblAdditionalFinancing);
                }
            }

            if (Session != null && Session["FinancingOptions"] != null)
            {
                if (!String.IsNullOrEmpty(txtFinancingOptions.Text))
                {
                    count++;
                    Session["FinancingOptions"] = txtFinancingOptions.Text.Trim();
                }
            }

            if (Session != null && Session["IsPlanInPlace"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsPlanInPlace"].ToString()))
                {
                    count++;
                    Session["IsPlanInPlace"] = GetRblSelectedValue(rblPlanInPlace);
                }
            }
            if (Session != null && Session["IsWorkingToEstablishRelationships"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsWorkingToEstablishRelationships"].ToString()))
                {
                    count++;
                }
            }
            
            if (Session != null && Session["RelationshipsExamples"] != null)
            {
                if (!String.IsNullOrEmpty(txtEstablishRelationships.Text))
                {
                    count++;
                    Session["RelationshipsExamples"] = txtEstablishRelationships.Text.Trim();
                    Session["IsWorkingToEstablishRelationships"] = GetRblSelectedValue(rblEstablishRelationships);
                }
            }

            if (Session != null && Session["AdditionalComments"] != null)
            {
                if (!String.IsNullOrEmpty(txtAdditionalComments.Text.Trim()))
                {
                    count++;
                    Session["AdditionalComments"] = txtAdditionalComments.Text.Trim();
                }
            }

            return count;
        }

        protected void SaveResponsesInSession()
        {
            Session["IsFundingLevelRequiredToSustain"] = GetRblSelectedValue(rblFundingLevel);
            Session["IsMSLeadershipUsingAdditionalFinancing"] = GetRblSelectedValue(rblAdditionalFinancing);
            if (!String.IsNullOrEmpty(txtFinancingOptions.Text))
            {
                Session["FinancingOptions"] = txtFinancingOptions.Text.Trim();
            }
            Session["IsPlanInPlace"] = GetRblSelectedValue(rblPlanInPlace);
            Session["IsWorkingToEstablishRelationships"] = GetRblSelectedValue(rblEstablishRelationships);
            if (!String.IsNullOrEmpty(txtEstablishRelationships.Text))
            {
                Session["RelationshipsExamples"] = txtEstablishRelationships.Text.Trim();
            }
            if (!String.IsNullOrEmpty(txtAdditionalComments.Text))
            {
                Session["AdditionalComments"] = txtAdditionalComments.Text.Trim();
            }
     
        }
        protected string GetRblSelectedValue(RadioButtonList rbl)
        {
            string s = "";
           
            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                s = rbl.SelectedValue;
            }
            return s;
        }

        protected int SaveResponsesToDatabase()
        {
            int x = 0;

            int RegisteringEasy=Convert.ToInt32(Session["RegisteringEasy"]);
            int AccessingEasy=Convert.ToInt32(Session["AccessingEasy"]);
            int SpeakersKnowledgeable=Convert.ToInt32(Session["SpeakersKnowledgeable"]);
            int WellOrganized=Convert.ToInt32(Session["WellOrganized"]);
            int RelevantExamplesUsed=Convert.ToInt32(Session["RelevantExamplesUsed"]);
            int SpeakersAdequatelyResponded=Convert.ToInt32(Session["SpeakersAdequatelyResponded"]);
            int CompleteProvidedTools=Convert.ToInt32(Session["CompleteProvidedTools"]);
            int AttendNextWebinar=Convert.ToInt32(Session["AttendNextWebinar"]);
            int LiveChatOrConsultativeServices=Convert.ToInt32(Session["LiveChatOrConsultativeServices"]);
            int FacebookOrTwitter=Convert.ToInt32(Session["FacebookOrTwitter"]);

            string IsFundingLevelRequiredToSustain = GetSessionBitValue(Session["IsFundingLevelRequiredToSustain"].ToString());

            string IsMSLeadershipUsingAdditionalFinancing = GetSessionBitValue(Session["IsMSLeadershipUsingAdditionalFinancing"].ToString());
            string FinancingOptions = Session["FinancingOptions"].ToString();
            string IsPlanInPlace = GetSessionBitValue(Session["IsPlanInPlace"].ToString());
            string IsWorkingToEstablishRelationships = GetSessionBitValue(Session["IsWorkingToEstablishRelationships"].ToString());
            string RelationshipsExamples = Session["RelationshipsExamples"].ToString();
            string AdditionalComments = Session["AdditionalComments"].ToString();

            x = InsertIntoDatabase( RegisteringEasy, AccessingEasy, SpeakersKnowledgeable, WellOrganized, RelevantExamplesUsed
                                        , SpeakersAdequatelyResponded, CompleteProvidedTools, AttendNextWebinar, LiveChatOrConsultativeServices
                                        , FacebookOrTwitter, IsFundingLevelRequiredToSustain, IsMSLeadershipUsingAdditionalFinancing 
                                        , FinancingOptions, IsPlanInPlace , IsWorkingToEstablishRelationships, RelationshipsExamples, AdditionalComments);

            return x;
        }

        protected string GetSessionBitValue(string checkValue)
        {
            string retVal=null;
            if (!String.IsNullOrEmpty(checkValue))
            {
                retVal = checkValue;
            }

            return retVal;
        }

        protected int InsertIntoDatabase(int RegisteringEasy
                                        ,int AccessingEasy
                                        ,int SpeakersKnowledgeable
                                        ,int WellOrganized
                                        ,int RelevantExamplesUsed
                                        ,int SpeakersAdequatelyResponded
                                        ,int CompleteProvidedTools
                                        ,int AttendNextWebinar
                                        ,int LiveChatOrConsultativeServices
                                        ,int FacebookOrTwitter
                                        ,string IsFundingLevelRequiredToSustain 
                                        ,string IsMSLeadershipUsingAdditionalFinancing 
                                        ,string FinancingOptions
                                        ,string IsPlanInPlace 
                                        ,string IsWorkingToEstablishRelationships
                                        ,string RelationshipsExamples 
                                        ,string AdditionalComments)
        {
            int inserted = 0;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("WebinarEvaluation_AligningProjectActivities_Insert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RegisteringEasy",RegisteringEasy);
            cmd.Parameters.Add("@AccessingEasy",AccessingEasy);
            cmd.Parameters.Add("@SpeakersKnowledgeable",SpeakersKnowledgeable);
            cmd.Parameters.Add("@WellOrganized",WellOrganized);
            cmd.Parameters.Add("@RelevantExamplesUsed",RelevantExamplesUsed);
            cmd.Parameters.Add("@SpeakersAdequatelyResponded",SpeakersAdequatelyResponded);
            cmd.Parameters.Add("@CompleteProvidedTools",CompleteProvidedTools);
            cmd.Parameters.Add("@AttendNextWebinar",AttendNextWebinar);
            cmd.Parameters.Add("@LiveChatOrConsultativeServices",LiveChatOrConsultativeServices);
            cmd.Parameters.Add("@FacebookOrTwitter",FacebookOrTwitter);
            cmd.Parameters.Add("@IsFundingLevelRequiredToSustain",IsFundingLevelRequiredToSustain);
            cmd.Parameters.Add("@IsMSLeadershipUsingAdditionalFinancing",IsMSLeadershipUsingAdditionalFinancing);
            cmd.Parameters.Add("@FinancingOptions",FinancingOptions);
            cmd.Parameters.Add("@IsPlanInPlace",IsPlanInPlace);
            cmd.Parameters.Add("@IsWorkingToEstablishRelationships",IsWorkingToEstablishRelationships);
            cmd.Parameters.Add("@RelationshipsExamples",RelationshipsExamples);
            cmd.Parameters.Add("@AdditionalComments", AdditionalComments);
            cmd.Parameters.Add("@CreatedDate", DateTime.Now);


            try
            {
                conn.Open();
                inserted = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }


            return inserted;
        }

        protected void rblAdditionalFinancing_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblAdditionalFinancing.SelectedValue == "1")
            {
                txtFinancingOptions.Visible = true;
            }
        }

        protected void rblEstablishRelationships_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblEstablishRelationships.SelectedValue == "1")
            {
                txtEstablishRelationships.Visible = true;
            }
        }
    }
}