﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebinarEvaluation_AligningProjectActivitiesP2.aspx.cs" Inherits="MSAPWebinarEvaluation.WebinarEvaluation_AligningProjectActivitiesP2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <title>Technical Assistance Webinar</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.txtArea 
{
	width:600px;
	height:100px;
}

.Rating
{
	width:20px;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 20px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 12px;
    /*font-weight: bold;*/
	font-style:italic;
	line-height:18px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}



#download_list li{
	padding-bottom:10px;
}

#main_tbl{
	width:80%;
	padding:0px;
	margin:0px;
	border-collapse:collapse !important;
}

#main_tbl td:first-child{
	padding-left:10px;
}

#main_tbl td{
	width:50%;
	vertical-align:top;
	border-bottom:1px solid #A9DEF2;
	padding:5px 0px;
}

#main_tbl th{
	font-size:11px;
}

.sub_tbls{
	width:50%;
	text-align:center;
	float:right;
	border:0px;
	padding:0px;
	margin:0px;
}

.sub_tbls2{
	width:100%;
	text-align:center;
	float:right;
	border:0px;
	padding:0px;
	margin:0px;
}

.sub_tbls2 td{
	width:25% !important;
	border:0px !important;
	padding:5px 0px;
}

.sub_titles{
	font-weight:bold;
}


.surveyBtn1 {
    background: url("images/surveybutton2.png") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    font-size: 10px;
    font-weight: bold;
    height: 19px;
    padding-bottom: 3px;
    width: 115px;
}

.yes_no_q{
	padding-top:15px;
	vertical-align:top;
}

.yes_no_space{
	padding-top:5px;
}



-->
</style>
</head>
<body  class="oneColElsCtrHdr">
    <form id="form1" runat="server">
 <div id="container">
  <div id="header">
    <img src="images/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
   <div id="mainContent">
    <h1>Planning for Sustainability: Aligning Project Activities with Goals and Results </h1>

<span id="subtitle"><p style="width:80%;">Please complete the following targeted questions regarding your project’s sustainability planning needs.
</p></span>

    <p><asp:Label runat="server" ID="lblMessage" ForeColor="Red" Font-Bold="true"></asp:Label></p>



    <table border="0" width="80%">
		<tbody>
        <tr>
        	<td class="sub_titles" style="vertical-align:top;">1.</td>
        	<td class="sub_titles" colspan="2">Do you know the funding level (including costs and resources) required to sustain your MSAP projects once the grant ends?</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
			<td  class="yes_no_space"><asp:RadioButtonList runat="server" ID="rblFundingLevel" 
                    RepeatDirection="Horizontal" AutoPostBack="True">
            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
            <asp:ListItem Text="No" Value="0"></asp:ListItem>
            </asp:RadioButtonList></td>
		</tr>
        <tr>
        	<td class="sub_titles yes_no_q">2.</td>
        	<td class="sub_titles yes_no_q" colspan="2">Is your magnet school leadership currently using additional financing options independent of the MSAP grant to implement the project (e.g. grants, contracts, earned revenue, individual donors, etc.)?</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
			<td class="yes_no_space"><asp:RadioButtonList runat="server" 
                    ID="rblAdditionalFinancing" RepeatDirection="Horizontal" AutoPostBack="True" 
                    onselectedindexchanged="rblAdditionalFinancing_SelectedIndexChanged">
            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
            <asp:ListItem Text="No" Value="0"></asp:ListItem>
            </asp:RadioButtonList></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        	<td  class="yes_no_space"><asp:TextBox runat="server" ID="txtFinancingOptions" 
                    TextMode="MultiLine" CssClass="txtArea" style="width:100%;" Visible="False"></asp:TextBox></td>
        </tr>
                <tr>
        	<td class="sub_titles yes_no_q">3.</td>
        	<td class="sub_titles yes_no_q" colspan="2">Does your MSAP project have a plan in place to pursue a variety of financing options for sustainability after the grant ends?</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
			<td class="yes_no_space"><asp:RadioButtonList runat="server" ID="rblPlanInPlace" RepeatDirection="Horizontal">
            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
            <asp:ListItem Text="No" Value="0"></asp:ListItem>
            </asp:RadioButtonList></td>
        </tr>
        <tr>
        	<td class="sub_titles yes_no_q">4.</td>
        	<td class="sub_titles yes_no_q" colspan="2">Are you and other magnet school leaders working to establish relationships (or have established relationships) with the proper stakeholder(s) that can help your pursue your plan successfully (e.g. district-level grant administrators, relevant city officials, business leaders, etc.)?</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
			<td class="yes_no_space"><asp:RadioButtonList runat="server" 
                    ID="rblEstablishRelationships" RepeatDirection="Horizontal" AutoPostBack="True" 
                    onselectedindexchanged="rblEstablishRelationships_SelectedIndexChanged">
            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
            <asp:ListItem Text="No" Value="0"></asp:ListItem>
            </asp:RadioButtonList></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        	<td class="yes_no_space"> <asp:TextBox runat="server" 
                    ID="txtEstablishRelationships" TextMode="MultiLine" CssClass="txtArea" 
                    style="width:100%;" Visible="False"></asp:TextBox></td>
        </tr>
           <tr>
        	<td class="yes_no_space" colspan="2"><p  class="sub_titles">Additional Comments:</p></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        	<td  class="yes_no_space"><asp:TextBox runat="server" ID="txtAdditionalComments" TextMode="MultiLine" CssClass="txtArea" style="width:100%;"></asp:TextBox></td>

        </tr>
		</tbody>
    </table>


<p style="text-align:right;width:80%;padding-top:30px;"><asp:Button runat="server" 
        ID="btnBack" Text="Back"  CssClass="surveyBtn1" onclick="btnBack_Click" />
    <asp:Button runat="server" ID="btnSubmit" CssClass="surveyBtn1" Text="Submit" 
        onclick="btnSubmit_Click" /></p>

	<!-- end #mainContent --></div>
  <div id="footer">

    <img src="images/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>


    </form>
</body>
</html>
