﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MSAPWebinarEvaluation
{
    public partial class Report_Webinar_Evaluation_Sustainability3 : System.Web.UI.Page
    {
        string connString = "Data Source=sei-db01; Initial Catalog=magnet; User ID=SynergyAppUser; Password=AppUser$";

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected DataSet Get_DatabaseData()
        {
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("Webinar_Evaluation_Sustainability3_ExcelDataset", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            return ds;
        }

        public void ExportToExcel(DataSet dSet, int TableIndex, HttpResponse Response, string FileName)
        {
            Response.Clear();
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("content-disposition", "attachment; filename=" + FileName + ".xls");
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(sw);
            GridView gv = new GridView();
            gv.DataSource = dSet.Tables[TableIndex];
            gv.DataBind();
            gv.RenderControl(hw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void btnShowAligningDatabase_Click(object sender, EventArgs e)
        {
            DataSet dsData = Get_DatabaseData();
            if (dsData.Tables.Count != 0 && dsData.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(dsData, 0, Response, "Webinar_Evaluation_Sustainability3_Responses2012");
            }
            else
            {
                lblMessage.Text = "Sorry, there is nothing in the database.";
            }
        }
    }
}