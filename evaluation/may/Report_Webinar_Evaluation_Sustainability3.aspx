﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report_Webinar_Evaluation_Sustainability3.aspx.cs" Inherits="MSAPWebinarEvaluation.Report_Webinar_Evaluation_Sustainability3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Creating a Strategic Financing Approach</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 12px;
    font-weight: bold;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}

#download_list li{
	padding-bottom:10px;
}
.surveyBtn1 {
    background: url("images/surveybutton2.png") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF !important;
    font-size: 9.4px;
    font-weight: bold;
	min-height:14px;
    padding-bottom: 3px;
	padding-top:2px;
    width: 115px !important;
	float:left;
	text-align:center;
}
-->
</style></head>
<body class="oneColElsCtrHdr">
<form id="form1" runat="server"><div id="container">
  <div id="header">
    <img src="images/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  <div id="mainContent">
      <h1>Planning for Sustainability: Creating a Strategic Financing Approach </h1>
   
    <div><span id="subtitle">
    <asp:Label runat="server" ID="lblMessage"></asp:Label></span></div></br>
      <asp:Button ID="btnShowAligningDatabase" runat="server" class="surveyBtn1"
          Text="Show Databse in Excel" onclick="btnShowAligningDatabase_Click"  />
	<!-- end #mainContent -->
      <br />
      <br />
    </div>


  <div id="footer">
    <img src="images/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>
</form>

</body>
</html>
