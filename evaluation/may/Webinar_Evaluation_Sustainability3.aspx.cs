﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MSAPWebinarEvaluation
{
    public partial class Webinar_Evaluation_Sustainability3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateResponses();
            }
        }

        protected void PopulateResponses()
        {
            if (Convert.ToInt32(Session["RegisteringEasy"]) > 0)
            {
                rblRegistrationForWebinarWasEasy.SelectedValue = Session["RegisteringEasy"].ToString();
            }

            if (Convert.ToInt32(Session["AccessingEasy"]) > 0)
            {
                rblAccessingWebinarEasy.SelectedValue = Session["AccessingEasy"].ToString();
            }
            if (Convert.ToInt32(Session["SpeakersKnowledgeable"]) > 0)
            {

                rblPresenterKnowledgeable.SelectedValue = Session["SpeakersKnowledgeable"].ToString();
            }
            if (Convert.ToInt32(Session["WebinarWellOrganized"]) > 0)
            {
                rblWebinarWellOrganized.SelectedValue = Session["WebinarWellOrganized"].ToString();
            }
            if (Convert.ToInt32(Session["Relevantexamples"]) > 0)
            {
                rblRelevantExamplesProvided.SelectedValue = Session["Relevantexamples"].ToString();
            }
            if (Convert.ToInt32(Session["SpeakersAdequatelyResponded"]) > 0)
            {
                rblSpeakersAdequatelyResponded.SelectedValue = Session["SpeakersAdequatelyResponded"].ToString();
            }
            if (Convert.ToInt32(Session["CompleteProvidedTools"]) > 0)
            {
                rblCompleteProvidedTools.SelectedValue = Session["CompleteProvidedTools"].ToString();
            }
            if (Convert.ToInt32(Session["AttendNextWebinar"]) > 0)
            {
                rblAttendNextWebinar.SelectedValue = Session["AttendNextWebinar"].ToString();
            }
            if (Convert.ToInt32(Session["EnagageInliveChats"]) > 0)
            {
                rblLiveChats.SelectedValue = Session["EnagageInliveChats"].ToString();
            }
           

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            Response.Redirect("Webinar_Evaluation_Sustainability3P2.aspx?a=1");
        }

        protected void SaveResponsesInSession()
        {
            Session["RegisteringEasy"] = GetRblSelectedValue(rblRegistrationForWebinarWasEasy);
            Session["AccessingEasy"] = GetRblSelectedValue(rblAccessingWebinarEasy);
            Session["SpeakersKnowledgeable"] = GetRblSelectedValue(rblPresenterKnowledgeable);
            Session["WebinarWellOrganized"] = GetRblSelectedValue(rblWebinarWellOrganized);
            Session["Relevantexamples"] = GetRblSelectedValue(rblRelevantExamplesProvided);
            Session["SpeakersAdequatelyResponded"] = GetRblSelectedValue(rblSpeakersAdequatelyResponded);
            Session["CompleteProvidedTools"] = GetRblSelectedValue(rblCompleteProvidedTools);
            Session["AttendNextWebinar"] = GetRblSelectedValue(rblAttendNextWebinar);
            Session["EnagageInliveChats"] = GetRblSelectedValue(rblLiveChats);
        }

        protected int GetRblSelectedValue(RadioButtonList rbl)
        {
            int x = 0;
            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                x = Convert.ToInt32(rbl.SelectedValue);
            }
            return x;
        }

        protected string GetSessionBitValue(string checkValue)
        {
            string retVal = null;
            if (!String.IsNullOrEmpty(checkValue))
            {
                retVal = checkValue;
            }

            return retVal;
        }
    }
}