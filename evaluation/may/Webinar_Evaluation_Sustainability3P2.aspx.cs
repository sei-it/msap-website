﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MSAPWebinarEvaluation
{
    public partial class Webinar_Evaluation_Sustainability3P2 : System.Web.UI.Page
    {
        string connString = "Data Source=sei-db01; Initial Catalog=magnet; User ID=SynergyAppUser; Password=AppUser$";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Convert.ToInt32(Request.QueryString["a"]) == 1)
                {
                    PopulateResponses();
                }
                else
                {
                    Response.Redirect("Webinar_Evaluation_Sustainability3.aspx");
                }
            }
        }

        public void PopulateResponses()
        {
            if (Session != null && Session["IsUndestandStepsForDevelopingSustainabilityPlan"] != null)
            {
                rblUndestandStepsForDevelopingSustainabilityPlan.SelectedValue = Session["IsUndestandStepsForDevelopingSustainabilityPlan"].ToString();
            }
            else
            {
                Session["IsUndestandStepsForDevelopingSustainabilityPlan"] = "";
            }

            if (Session != null && Session["AdditionalSupport"] != null)
            {
                txtAdditionalSupport.Text = Session["AdditionalSupport"].ToString();
            }
            else
            {
                Session["AdditionalSupport"] = "";
            }

            if (Session != null && Session["IsTeamInPlace"] != null)
            {
                rblTeamInPlace.SelectedValue = Session["IsTeamInPlace"].ToString();
            }
            else
            {
                Session["IsTeamInPlace"] = "";
            }
            
            if (Session != null && Session["IsProperBuyIn"] != null)
            {
                rblProperBuyIn.SelectedValue = Session["IsProperBuyIn"].ToString();
            }
            else
            {
                Session["IsProperBuyIn"] = "";
            }

            if (Session != null && Session["AdditionalComments"] != null)
            {
                txtAdditionalComments.Text = Session["AdditionalComments"].ToString();
            }
            else
            {
                Session["AdditionalComments"] = "";
            }

        }


        protected void btnBack_Click(object sender, EventArgs e)
        {
            SaveResponsesInSession();
            Response.Redirect("Webinar_Evaluation_Sustainability3.aspx");
        }

        protected void SaveResponsesInSession()
        {
            Session["IsUndestandStepsForDevelopingSustainabilityPlan"]=GetRblSelectedValue(rblUndestandStepsForDevelopingSustainabilityPlan);
          
            if (!String.IsNullOrEmpty(txtAdditionalSupport.Text))
            {
                Session["AdditionalSupport"] = txtAdditionalSupport.Text.Trim();
            }

            Session["IsTeamInPlace"]=GetRblSelectedValue(rblTeamInPlace);
            Session["IsProperBuyIn"]=GetRblSelectedValue(rblProperBuyIn);
            if (!String.IsNullOrEmpty(txtAdditionalComments.Text))
            {
                Session["AdditionalComments"] = txtAdditionalComments.Text.Trim();
            }

        }
        protected string GetRblSelectedValue(RadioButtonList rbl)
        {
            string s = "";

            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                s = rbl.SelectedValue;
            }
            return s;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int a = 0;
            SaveResponsesInSession();
            if ((a = CheckIfAtleastOneResponseProvided()) > 0)
            {
                a = SaveResponsesToDatabase();
                if (a != 0)
                {
                    Session.Clear();
                    Response.Redirect("ThankYouSustainability3.aspx");
                }
                else
                {
                    lblMessage.Text = "The following error occured: " + Session["ErrorMessage"];
                }
            }
            else
            {
                lblMessage.Text = "Please select atleast one response to submit the survey.";
            }
        }


        protected int CheckIfAtleastOneResponseProvided()
        {
            int count = 0;
            if (Convert.ToInt32(Session["RegisteringEasy"]) > 0)
            {
                string easy = Session["RegisteringEasy"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["AccessingEasy"]) > 0)
            {
                string AccessingEasy = Session["AccessingEasy"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["SpeakersKnowledgeable"]) > 0)
            {
                string SpeakersKnowledgeable = Session["SpeakersKnowledgeable"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["WebinarWellOrganized"]) > 0)
            {
                string WebinarWellOrganized = Session["WebinarWellOrganized"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["Relevantexamples"]) > 0)
            {
                string Relevantexamples = Session["Relevantexamples"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["SpeakersAdequatelyResponded"]) > 0)
            {
                string SpeakersAdequatelyResponded = Session["SpeakersAdequatelyResponded"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["CompleteProvidedTools"]) > 0)
            {
                string CompleteProvidedTools = Session["CompleteProvidedTools"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["AttendNextWebinar"]) > 0)
            {
                string AttendNextWebinar = Session["AttendNextWebinar"].ToString();
                count++;
            }
            if (Convert.ToInt32(Session["EnagageInliveChats"]) > 0)
            {
                string EnagageInliveChats = Session["EnagageInliveChats"].ToString();
                count++;
            }

            if (Session != null && Session["IsUndestandStepsForDevelopingSustainabilityPlan"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsUndestandStepsForDevelopingSustainabilityPlan"].ToString()))
                {
                    count++;
                    Session["IsUndestandStepsForDevelopingSustainabilityPlan"] = GetRblSelectedValue(rblUndestandStepsForDevelopingSustainabilityPlan);
                }
            }
            
            if (Session != null && Session["AdditionalSupport"] != null)
            {
                if (!String.IsNullOrEmpty(txtAdditionalSupport.Text.Trim()))
                {
                    count++;
                    Session["AdditionalSupport"] = txtAdditionalSupport.Text.Trim();
                }
            }

            if (Session != null && Session["IsTeamInPlace"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsTeamInPlace"].ToString()))
                {
                    count++;
                    Session["IsTeamInPlace"] = GetRblSelectedValue(rblTeamInPlace);
                }
            }

            if (Session != null && Session["IsProperBuyIn"] != null)
            {
                if (!String.IsNullOrEmpty(Session["IsProperBuyIn"].ToString()))
                {
                    count++;
                    Session["IsProperBuyIn"] = GetRblSelectedValue(rblProperBuyIn);
                }
            }

            if (Session != null && Session["AdditionalComments"] != null)
            {
                if (!String.IsNullOrEmpty(txtAdditionalComments.Text.Trim()))
                {
                    count++;
                    Session["AdditionalComments"] = txtAdditionalComments.Text.Trim();
                }
            }

            return count;
        }

        protected int SaveResponsesToDatabase()
        {
            int x = 0;

            int RegisteringEasy = Convert.ToInt32(Session["RegisteringEasy"]);
            int AccessingEasy = Convert.ToInt32(Session["AccessingEasy"]);
            int SpeakersKnowledgeable = Convert.ToInt32(Session["SpeakersKnowledgeable"]);
            int WebinarWellOrganized = Convert.ToInt32(Session["WebinarWellOrganized"]);
            int Relevantexamples = Convert.ToInt32(Session["Relevantexamples"]);
            int SpeakersAdequatelyResponded = Convert.ToInt32(Session["SpeakersAdequatelyResponded"]);
            int CompleteProvidedTools = Convert.ToInt32(Session["CompleteProvidedTools"]);
            int AttendNextWebinar = Convert.ToInt32(Session["AttendNextWebinar"]);
            int EnagageInliveChats = Convert.ToInt32(Session["EnagageInliveChats"]);

            string IsUndestandStepsForDevelopingSustainabilityPlan = GetSessionBitValue(Session["IsUndestandStepsForDevelopingSustainabilityPlan"].ToString());
            string AdditionalSupport = Session["AdditionalSupport"].ToString();
            string IsTeamInPlace = GetSessionBitValue(Session["IsTeamInPlace"].ToString());
            string IsProperBuyIn = GetSessionBitValue(Session["IsProperBuyIn"].ToString());
            string AdditionalComments = Session["AdditionalComments"].ToString();

            x = InsertIntoDatabase(RegisteringEasy, AccessingEasy, SpeakersKnowledgeable, WebinarWellOrganized, Relevantexamples
                                        , SpeakersAdequatelyResponded, CompleteProvidedTools, AttendNextWebinar, EnagageInliveChats
                                        , IsUndestandStepsForDevelopingSustainabilityPlan, AdditionalSupport
                                        , IsTeamInPlace, IsProperBuyIn, AdditionalComments);

            return x;
        }


        protected string GetSessionBitValue(string checkValue)
        {
            string retVal = null;
            if (!String.IsNullOrEmpty(checkValue))
            {
                retVal = checkValue;
            }

            return retVal;
        }

        protected int InsertIntoDatabase(int RegisteringEasy
                                      , int AccessingEasy
                                      , int SpeakersKnowledgeable
                                      , int WebinarWellOrganized
                                      , int Relevantexamples
                                      , int SpeakersAdequatelyResponded
                                      , int CompleteProvidedTools
                                      , int AttendNextWebinar
                                      , int EnagageInliveChats
                                      , string IsUndestandStepsForDevelopingSustainabilityPlan
                                      , string AdditionalSupport
                                      , string IsTeamInPlace
                                      , string IsProperBuyIn
                                      , string AdditionalComments)
        {
            int inserted = 0;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("Webinar_Evaluation_Sustainability3_Insert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RegisteringEasy", RegisteringEasy);
            cmd.Parameters.Add("@AccessingEasy", AccessingEasy);
            cmd.Parameters.Add("@SpeakersKnowledgeable", SpeakersKnowledgeable);
            cmd.Parameters.Add("@WebinarWellOrganized", WebinarWellOrganized);
            cmd.Parameters.Add("@Relevantexamples", Relevantexamples);
            cmd.Parameters.Add("@SpeakersAdequatelyResponded", SpeakersAdequatelyResponded);
            cmd.Parameters.Add("@CompleteProvidedTools", CompleteProvidedTools);
            cmd.Parameters.Add("@AttendNextWebinar", AttendNextWebinar);
            cmd.Parameters.Add("@EnagageInliveChats", EnagageInliveChats);
            cmd.Parameters.Add("@IsUndestandStepsForDevelopingSustainabilityPlan", IsUndestandStepsForDevelopingSustainabilityPlan);
            cmd.Parameters.Add("@AdditionalSupport", AdditionalSupport);
            cmd.Parameters.Add("@IsTeamInPlace", IsTeamInPlace);
            cmd.Parameters.Add("@IsProperBuyIn", IsProperBuyIn);
            cmd.Parameters.Add("@AdditionalComments", AdditionalComments);
            cmd.Parameters.Add("@DateSubmitted", DateTime.Now);


            try
            {
                conn.Open();
                inserted = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }


            return inserted;
        }

        //protected void rblUndestandStepsForDevelopingSustainabilityPlan_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (rblUndestandStepsForDevelopingSustainabilityPlan.SelectedValue == "1")
        //    {
        //        //lblYNheader.Visible = true;
        //        txtAdditionalSupport.Visible = true;
        //    }
        //    else if (rblUndestandStepsForDevelopingSustainabilityPlan.SelectedValue == "0")
        //    {
        //        //lblYNheader.Visible = false;
        //        txtAdditionalSupport.Visible = false;
        //    }
        //}

    }
}