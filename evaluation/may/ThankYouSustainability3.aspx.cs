﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MSAPWebinarEvaluation
{
    public partial class ThankYouSustainability3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void lbtnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("http://msapcenter.com");
        }

        protected void lbtnAnotherSurvey_Click(object sender, EventArgs e)
        {
            Response.Redirect("Webinar_Evaluation_Sustainability3.aspx");
        }
    }
}