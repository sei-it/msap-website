﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ThankYouSustainability3.aspx.cs" Inherits="MSAPWebinarEvaluation.ThankYouSustainability3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Creating a Strategic Financing Approach</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.txtArea 
{
	width:600px;
	height:100px;
}

.Rating
{
	width:20px;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 12px;
    /*font-weight: bold;*/
	font-style:italic;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}



#download_list li{
	padding-bottom:10px;
}

#main_tbl{
	width:80%;
	padding:0px;
	margin:0px;
	border-collapse:collapse !important;
}

#main_tbl td:first-child{
	padding-left:10px;
}

#main_tbl td{
	width:50%;
	vertical-align:top;
	border-bottom:1px solid #A9DEF2;
	padding:5px 0px;
}

#main_tbl th{
	font-size:11px;
}
#tbl2{
	width:80%;
	padding:0px;
	margin:0px;
	border-collapse:collapse !important;
}

#tbl2 td
{
	padding:5px 0px;
}

.sub_tbls{
	width:50%;
	text-align:center;
	float:right;
	border:0px;
	padding:0px;
	margin:0px;
}

.sub_tbls2{
	width:100%;
	text-align:center;
	float:right;
	border:0px;
	padding:0px;
	margin:0px;
}

.sub_tbls2 td{
	width:25% !important;
	border:0px !important;
	padding:5px 0px;
}

.sub_titles{
	font-weight:bold;
}

.sub_titles_top{
	font-weight:bold;
	padding-top:30px;
}

.surveyBtn1 {
    background: url("images/surveybutton2.png") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF !important;
    font-size: 10px;
    font-weight: bold;
	min-height:14px;
    padding-bottom: 3px;
	padding-top:2px;
    width: 115px !important;
	float:right;
	text-align:center;
}
.surveyBtn2 {
    background: url("images/another_survey_btn.png") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF !important;
    font-size: 10px;
    font-weight: bold;
	min-height:14px;
    padding-bottom: 3px;
	padding-top:2px;
    width: 155px !important;
	float:left;
	text-align:center;
}
.surveyBtn3 {
    background: url("images/another_survey_btn.png") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF !important;
    font-size: 10px;
    font-weight: bold;
	min-height:14px;
    padding-bottom: 3px;
	padding-top:2px;
    width: 155px !important;
	float:left;
	text-align:center;
}

-->
</style></head>
<body class="oneColElsCtrHdr">
    <form id="form1" runat="server">
   <div id="container">
  <div id="header">
    <img src="images/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  
  <div id="mainContent">
    <h1>Planning for Sustainability: Creating a Strategic Financing Approach </h1>

<p style="font-weight:bold; color:#2274A0;">Thank you for completing the Planning for Sustainability webinar evaluation. For more information or to participate in follow-on activities, contact the MSAP Center.</p>
<%--<p style="color:#2274A0;">Be sure to contact the MSAP Center if you have questions about MAPS.</p>
--%>
   <table width="800px">
    <tr>
        <td  style="padding-top:30px;padding-bottom:30px; width:400px; margin-left:20px ">
            <asp:LinkButton runat="server" ID="lbtnHome" CssClass="surveyBtn3" Text="Home" 
                onclick="lbtnHome_Click" ></asp:LinkButton></td>
        <td style="padding-top:30px;padding-bottom:30px; width:400px">
            <asp:LinkButton runat="server" ID="lbtnAnotherSurvey" CssClass="surveyBtn2" 
                Text="Submit Another Survey" onclick="lbtnAnotherSurvey_Click" ></asp:LinkButton></td>
    </tr>
    </table>
	<!-- end #mainContent --></div>
  <div id="footer">

    <img src="images/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>
</form>
</body>
</html>
