﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MSAPWebinarEvaluation
{
    public partial class WebinarEvaluation2 : System.Web.UI.Page
    {
        string connString = "Data Source=sei-db01; Initial Catalog=magnet; User ID=SynergyAppUser; Password=AppUser$";
        int x = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            x = 0;
            if(CheckForAnyResponse()!=0)
            {
                x = SaveFormDetails();
                if (x > 0)
                {
                    Response.Redirect("ThankYou2.aspx?success=1");
                }
                else
                {
                    Response.Redirect("ThankYou2.aspx?success=0");
                }
            }
            else
            {
                lblMessage.Text="Please respond to atleast one question.";
            }
        }

        protected int SaveFormDetails()
        {
            x = 0;
            int RatingRegisteringForTheWebinarWasEasy = GetRblSelectedValue(rblRegistrationForWebinarWasEasy);
            int RatingAccessingTheWebinarWasEasy = GetRblSelectedValue(rblAccessingWebinarEasy);
            int RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter = GetRblSelectedValue(rblSpeakersKnowledgeable);
            int RatingTheWebinarWasWellOrganized=GetRblSelectedValue(rblWebinarWellOrganized);
            int RatingRelevantExamplesWereUsedToIllustrateConcepts=GetRblSelectedValue(rblRelevantExamplesProvided);
            int RatingTheSpeakersAdequatelyRespondedToQuestions = GetRblSelectedValue(rblSpeakersAdequatelyResponded);
            int RatingIWillCompleteTheProvidedTools=GetRblSelectedValue(rblCompleteProvidedTools);
            int RatingIWillAttendTheNextWebinar=GetRblSelectedValue(rblAttendNextWebinar);
            int RatingIWillEngageInLiveChats=GetRblSelectedValue(rblEngageInLiveChats);
            bool ProvideBetterInformation=cbProvideBetterInformation.Checked;
            bool ClarifyWObjectives=cbClarifyWObjectives.Checked;
            bool ReduceWContent=cbReduceWContent.Checked;
            bool IncreaseWContent=cbIncreaseWContent.Checked;
            bool MakeWInteractive= cbMakeWInteractive.Checked;
            bool ImproveWOrganization=cbImproveWOrganization.Checked;
            bool MakeWLessDifficult=cbMakeWLessDifficult.Checked;
            bool MakeWMoreDifficult=cbMakeWMoreDifficult.Checked;
            string Comments = txtComments.Text.Trim();
            bool StructureInPlace= Convert.ToBoolean(GetRblSelectedValue(rblStructureInPlace));
            string BiggestChallenge=txtChallenge.Text.Trim();
            bool DevelopedLogicModel=Convert.ToBoolean(GetRblSelectedValue(rblDevelopedLogicProg));

            x=InsertIntoDatabse(RatingRegisteringForTheWebinarWasEasy
                                ,RatingAccessingTheWebinarWasEasy
                                ,RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter
                                ,RatingTheWebinarWasWellOrganized
                                ,RatingRelevantExamplesWereUsedToIllustrateConcepts
                                ,RatingTheSpeakersAdequatelyRespondedToQuestions
                                ,RatingIWillCompleteTheProvidedTools
                                ,RatingIWillAttendTheNextWebinar
                                ,RatingIWillEngageInLiveChats
                                ,ProvideBetterInformation
                                ,ClarifyWObjectives
                                ,ReduceWContent
                                ,IncreaseWContent
                                ,MakeWInteractive
                                ,ImproveWOrganization
                                ,MakeWLessDifficult
                                ,MakeWMoreDifficult
                                ,Comments
                                ,StructureInPlace 
                                ,BiggestChallenge
                                ,DevelopedLogicModel);

            return x;
        }

        protected int GetRblSelectedValue(RadioButtonList rbl)
        {
            x = 0;
            if (!String.IsNullOrEmpty(rbl.SelectedValue))
            {
                x = Convert.ToInt32(rbl.SelectedValue);
            }
            return x;
        }

        protected int InsertIntoDatabse(int RatingRegisteringForTheWebinarWasEasy
                                        ,int RatingAccessingTheWebinarWasEasy
                                        ,int RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter
                                        ,int RatingTheWebinarWasWellOrganized
                                        ,int RatingRelevantExamplesWereUsedToIllustrateConcepts
                                        ,int RatingTheSpeakersAdequatelyRespondedToQuestions
                                        ,int RatingIWillCompleteTheProvidedTools
                                        ,int RatingIWillAttendTheNextWebinar
                                        ,int RatingIWillEngageInLiveChats
                                        ,bool ProvideBetterInformation
                                        ,bool ClarifyWObjectives
                                        ,bool ReduceWContent
                                        ,bool IncreaseWContent
                                        ,bool MakeWInteractive
                                        ,bool ImproveWOrganization
                                        ,bool MakeWLessDifficult
                                        ,bool MakeWMoreDifficult
                                        ,string Comments
                                        ,bool StructureInPlace
                                        ,string BiggestChallenge
                                        ,bool DevelopedLogicModel)
        {
            x = 0;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("WebinarEvaluation_Insert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RatingRegisteringForTheWebinarWasEasy",RatingRegisteringForTheWebinarWasEasy);
            cmd.Parameters.Add("@RatingAccessingTheWebinarWasEasy", RatingAccessingTheWebinarWasEasy);
            cmd.Parameters.Add("@RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter",RatingTheSpeakersWereKnowledgeableOnTheSubjectMatter);
            cmd.Parameters.Add("@RatingTheWebinarWasWellOrganized",RatingTheWebinarWasWellOrganized);
            cmd.Parameters.Add("@RatingRelevantExamplesWereUsedToIllustrateConcepts",RatingRelevantExamplesWereUsedToIllustrateConcepts);
            cmd.Parameters.Add("@RatingTheSpeakersAdequatelyRespondedToQuestions",RatingTheSpeakersAdequatelyRespondedToQuestions);
            cmd.Parameters.Add("@RatingIWillCompleteTheProvidedTools",RatingIWillCompleteTheProvidedTools);
            cmd.Parameters.Add("@RatingIWillAttendTheNextWebinar",RatingIWillAttendTheNextWebinar);
            cmd.Parameters.Add("@RatingIWillEngageInLiveChats",RatingIWillEngageInLiveChats);
            cmd.Parameters.Add("@ProvideBetterInformation",ProvideBetterInformation);
            cmd.Parameters.Add("@ClarifyWObjectives",ClarifyWObjectives);
            cmd.Parameters.Add("@ReduceWContent",ReduceWContent);
            cmd.Parameters.Add("@IncreaseWContent",IncreaseWContent);
            cmd.Parameters.Add("@MakeWInteractive",MakeWInteractive);
            cmd.Parameters.Add("@ImproveWOrganization",ImproveWOrganization);
            cmd.Parameters.Add("@MakeWLessDifficult",MakeWLessDifficult);
            cmd.Parameters.Add("@MakeWMoreDifficult",MakeWMoreDifficult);
            cmd.Parameters.Add("@Comments",Comments);
            cmd.Parameters.Add("@StructureInPlace",StructureInPlace);
            cmd.Parameters.Add("@BiggestChallenge",BiggestChallenge);
            cmd.Parameters.Add("@DevelopedLogicModel", DevelopedLogicModel);
            cmd.Parameters.Add("@DateSubmitted", DateTime.Now);

            try
            {
                conn.Open();
                x = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
            }
            catch(Exception ex)
            {
                Response.Write(ex);
            }
            return x;
        }

        protected int CheckForAnyResponse()
        {
            int count=0;
            if(GetRblSelectedValue(rblRegistrationForWebinarWasEasy)!=0)
            {
                count++;
            }
            if(GetRblSelectedValue(rblAccessingWebinarEasy)!=0)
            {
                count++;
            }
            if(GetRblSelectedValue(rblSpeakersKnowledgeable)!=0)
            {
                count++;
            }
            
            if(GetRblSelectedValue(rblWebinarWellOrganized)!=0)
            {
                count++;
            }
            if(GetRblSelectedValue(rblRelevantExamplesProvided)!=0)
            {
                count++;
            }
            if(GetRblSelectedValue(rblSpeakersAdequatelyResponded)!=0)
            {
                count++;
            }
            if(GetRblSelectedValue(rblCompleteProvidedTools)!=0)
            {
                count++;
            }
            
            if(GetRblSelectedValue(rblAttendNextWebinar)!=0)
            {
                count++;
            }
            if(GetRblSelectedValue(rblEngageInLiveChats)!=0)
            {
                count++;
            }
            if(cbProvideBetterInformation.Checked)
            {
                count++;
            }
            if(cbClarifyWObjectives.Checked)
            {
                count++;
            }
            
            if(cbReduceWContent.Checked)
            {
                count++;
            }
            if(cbIncreaseWContent.Checked)
            {
                count++;
            }
            if(cbMakeWInteractive.Checked)
            {
                count++;
            }
            if(cbImproveWOrganization.Checked)
            {
                count++;
            }
			if(cbMakeWLessDifficult.Checked)
            {
                count++;
            }
            if(cbMakeWMoreDifficult.Checked)
            {
                count++;
            }			
            if(!String.IsNullOrEmpty(txtComments.Text.Trim()))
            {
                count++;
            }
            if(GetRblSelectedValue(rblStructureInPlace)!=0)
            {
                count++;
            }
			if(!String.IsNullOrEmpty(txtChallenge.Text.Trim()))
            {
                count++;
            }

            for (var i = 0; i < rblDevelopedLogicProg.Items.Count; i++)
            {
                if (rblDevelopedLogicProg.Items[i].Selected) 
                {
                    count++;
                }
            }
            
            return count;
        }
    }

    
}