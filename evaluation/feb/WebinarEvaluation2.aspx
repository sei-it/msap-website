﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebinarEvaluation2.aspx.cs" Inherits="MSAPWebinarEvaluation.WebinarEvaluation2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Technical Assistance Webinar</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.txtArea 
{
	width:600px;
	height:100px;
}

.Rating
{
	width:20px;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 12px;
    font-weight: bold;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}



#download_list li{
	padding-bottom:10px;
}
-->
</style></head>
 
<body class="oneColElsCtrHdr">
    <form id="form1" runat="server">
  <div id="container">
  <div id="header">
    <img src="images/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  <div id="mainContent">
    <h1>Planning for Sustainability: Building the Framework</h1>
    <p><asp:Label runat="server" ID="lblMessage" ForeColor="Red" Font-Bold="true"></asp:Label></p>
<span id="subtitle"><p>As we plan future Planning for Sustainability webinars and follow-on activities, your feedback will be very helpful for us. Please complete the questions below. </p></span>

<p><strong>Rate the following statements. Select one response for each.</strong></p>
    <table class="style1">
        <tr>
            <td></td>
            <td style="width:540px;">
            <table>
            <tr style="font-weight:bold;">
           
                <td style="width:70px; text-align:left;">Strongly 
                    <br />
                    Agree</td>
                <td style="width:70px;">Agree </td>
                <td style="width:70px;">Disagree</td>
                <td style="width:70px;">Strongly 
                    <br />
                    Disagree</td>
            </tr>
            </table>
            </td>
            
            
        </tr>
        <tr>
            <td>Registering for the webinar was easy.</td>
            
            <td>              
                <asp:RadioButtonList ID="rblRegistrationForWebinarWasEasy" runat="server" 
                    RepeatDirection="Horizontal" Width="320px" TextAlign="Right" >
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>
                </asp:RadioButtonList> </td>
            
        </tr>
        <tr>
            <td>Accessing the webinar was easy.</td>
            <td>              
                <asp:RadioButtonList ID="rblAccessingWebinarEasy" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>            
        </tr>
        <tr>
            <td>The speakers were knowledgeable on the subject matter.</td>
            <td>              
                <asp:RadioButtonList ID="rblSpeakersKnowledgeable" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>           
        </tr>

        <tr>
            <td>The webinar was well organized.</td>
            <td>              
                <asp:RadioButtonList ID="rblWebinarWellOrganized" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>           
        </tr>

        <tr>
            <td>Relevant examples were used to illustrate concepts.</td>
            <td>              
                <asp:RadioButtonList ID="rblRelevantExamplesProvided" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>             
        </tr>
        <tr>
            <td>The speakers responded adequately to questions.</td>
            <td>              
                <asp:RadioButtonList ID="rblSpeakersAdequatelyResponded" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>        </tr>
         <tr>
            <td>I will complete the provided tools.</td>
            <td>              
                <asp:RadioButtonList ID="rblCompleteProvidedTools" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>        </tr>
        <tr>
            <td>I will attend the next webinar.</td>
            <td>              
                <asp:RadioButtonList ID="rblAttendNextWebinar" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>                
        </tr>
        <tr>
            <td>I will engage in live chats or consultative services.</td>
            <td>              
                <asp:RadioButtonList ID="rblEngageInLiveChats" runat="server" RepeatDirection="Horizontal" Width="320px">
                <asp:ListItem Value="4" Text=""></asp:ListItem>
                <asp:ListItem Value="3" Text=""></asp:ListItem>
                <asp:ListItem Value="2" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text=""></asp:ListItem>

                </asp:RadioButtonList>  &nbsp;</td>

        </tr>


    </table>


<strong><p>How would you improve this webinar? Select as many as apply.</strong>
</p>
<p><table>

<tr>
    <td><asp:CheckBox runat="server" ID="cbProvideBetterInformation" Text="Provide better information before the webinar" /></td>
    <td><asp:CheckBox runat="server" ID="cbClarifyWObjectives" Text="Clarify the webinar objectives" /></td>
</tr>
<tr>
    <td><asp:CheckBox runat="server" ID="cbReduceWContent" Text="Reduce the content covered in the webinar"/></td>
    <td><asp:CheckBox runat="server" ID="cbIncreaseWContent" Text="Increase the content covered in the webinar"/></td>

</tr>
<tr>
    <td><asp:CheckBox runat="server" ID="cbMakeWInteractive" Text="Make webinar activities more interactive" /></td>
    <td><asp:CheckBox runat="server" ID="cbImproveWOrganization" Text="Improve webinar organization"/></td>
</tr>
<tr>
    <td><asp:CheckBox runat="server" ID="cbMakeWLessDifficult" Text="Make the webinar less difficult" Visible="false"/></td>
    <td><asp:CheckBox runat="server" ID="cbMakeWMoreDifficult" Text="Make the webinar more difficult" Visible="false"/></td>
    <td></td>
</tr>
</table>
</p>
<p><strong>Comments:</strong></p>
<p>
  <div> <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" CssClass="txtArea"></asp:TextBox></div>
</p>

<span id="subtitle"><p>Please complete the following targeted questions regarding your project’s sustainability planning needs.</p></span>

<p><strong>Do you feel you have the structure in place (i.e. staff time, stakeholder buy-in, leadership, appropriate partners willing to participate) to begin sustainability planning?</strong>
    <asp:RadioButtonList ID="rblStructureInPlace" runat="server">
    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
    <asp:ListItem Text="No" Value="0"></asp:ListItem>
    </asp:RadioButtonList>
</p>
<p><strong> What do you see as your biggest challenge in launching sustainability planning?</strong>
<div><asp:TextBox ID="txtChallenge" runat="server" TextMode="MultiLine" CssClass="txtArea" ></asp:TextBox></div>
</p>
<p><strong> Have you developed a logic model for your program?</strong>
    <asp:RadioButtonList ID="rblDevelopedLogicProg" runat="server">
    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
    <asp:ListItem Text="No" Value="0"></asp:ListItem>
    </asp:RadioButtonList>
    </p>
    <p><asp:Button ID="btnSubmit" runat="server" Text="Submit" 
            onclick="btnSubmit_Click" /></p>

   
	<!-- end #mainContent --></div>
  <div id="footer">
    <img src="images/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>

    </form>
</body>
</html>
