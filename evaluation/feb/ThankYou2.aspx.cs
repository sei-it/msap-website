﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MSAPWebinarEvaluation
{
    public partial class ThankYou2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["success"] == "1")
                {
                  //  lblMessage.Text = "Thank you for the feedback. It is very valuable for us to plan future Planning for Sustainability webinars and follow-on activities.";
                    lblMessage.Text = "Thank you for providing feedback. Be sure to join us for the live chat on March 1, 2012 at 3:00 pm Eastern.";
                }
                if (Request.QueryString["success"] == "0")
                {
                    lblMessage.Text = "Sorry there was a problem and your feedback was not received.";
                }
            }
        }
    }
}