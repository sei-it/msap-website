﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="approach.aspx.cs" Inherits="approach" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Approach to Technical Assistance
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <link rel="stylesheet" href="../../../css/feature-carousel.css" charset="utf-8" />
    <script src="../../../js/jquery-1.7.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="../../../js/jquery.featureCarousel.min.js" type="text/javascript" charset="utf-8"></script>

 <script type="text/javascript">
     $(document).ready(function () {
         var carousel = $("#carousel").featureCarousel({
             // include options like this:
             // (use quotes only for string values, and no trailing comma after last option)
             // option: value,
             // option: value
             //         trackerIndividual: false,
             //  //        trackerSummation: false,
             //          smallFeatureWidth: 05,
             //          smallFeatureHeight: 0.5
         });

         $("#but_prev").click(function () {
             carousel.prev();
         });
         $("#but_pause").click(function () {
             carousel.pause();
         });
         $("#but_start").click(function () {
             carousel.start();
         });
         $("#but_next").click(function () {
             carousel.next();
         });
     });
    </script>
    <style>
	.carousel-caption{
		 width:100% !important;
		 left:0% !important;
		 height:43% !important;
	}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Approach to Technical Assistance</h1>
        <p>
            Every magnet project develops at its own pace and operates within the unique context of its local education agency. 
            At any time, magnet staffs and schools may have different informational and resource needs. To accommodate these differences, 
            the MSAP Center uses a four-tiered approach to designing and delivering technical assistance, described here.
        </p>
        <div>
        <table>
<tr>
<td>
<div class="carousel-container">
    
      <div id="carousel">
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/approachTA/individualservices.jpg"></a>
          <div class="carousel-caption">
           <h2>Individual Services</h2>
            <p style="padding-top:0px;">
              Through one-on-one assistance from expert consultants and MSAP Center staff, an individual MSAP grantee receives customized support to help implement and manage its magnet program.
            </p>
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/approachTA/intensiveservices.jpg"></a>
          <div class="carousel-caption">
           <h2>Intensive Services</h2>
            <p  style="padding-top:0px;">
            These services provide assistance with specific magnet content to groups of grantees.  Technical assistance may include sessions with expert consultants in combination with resources created by the MSAP Center, such as online courses and content-focused tools.
        </p>   
         <!-- <p class="cc_link">
            <a href="">link</a>
            </p>-->
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/approachTA/targetedservices.jpg"></a>
          <div class="carousel-caption">
            <h2>Targeted Services</h2>
            <p  style="padding-top:0px;">These services focus on specific groups of magnet staff, such as project directors, school administrators, and teachers, who each play different roles within their magnet programs. These services may be delivered at magnet conferences, webinars, and professional development seminars.</p>
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/approachTA/globalservices.jpg"></a>
           <div class="carousel-caption">
           <h2 style="line-height:20px;">Global Services</h2>
            <p  style="padding-top:0px;">These technical assistance services reach the whole magnet school community through the MSAP Center website, where magnet staff can find professional development events and resources, research-based information, and tools from a variety of sources.
            </p>
           </div>
        </div>
        <!--<div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide4.jpg"></a>
           <div class="carousel-caption">
           <h2>Links to Other sites</h2>
            <p style="padding-top:0px;">Links to approved sites...
            <img src="../../../images/Puzzle_icon.png" alt="" width="50" style="float:left;padding-right:7px;"> 
           <a href="/admin/doc/past_webinars/Compliance_Monitoring_crosswalk.pdf" target="_blank">Download now</a>
            </p>
            
             <p class="cc_link">
            <a href="">link</a>
            </p>
           </div>
        </div>-->
      </div>
    
      <div id="carousel-left"><img src="../../../img/carousel/arrow-left.png" /></div>
      <div id="carousel-right"><img src="../../../img/carousel/arrow-right.png" /></div>
    </div>
</td>
</tr>
</table>
           <!-- <img src="images/TA_pyramid.jpg" style="margin-left: 10px;" alt="Individual services.  Services are tailored to meet the specific needs of an individual grantee that requires additional support to implement and manage its magnet program., Intensive services.  Services are tailored to meet specific grantee needs, such as marketing and student recruitment, and are focused on specific issues related to those needs, such as developing and implementing effective recruitment strategies., Targeted services.  Services focus on specific magnet school staff (e.g., administrators and teachers) and are delivered at magnet conferences, webinars, and professional development seminars., Global services.  Services, information, and resources are broadly disseminated to the interested magnet schools' community and public." />--></div>
    </div>
</asp:Content>
