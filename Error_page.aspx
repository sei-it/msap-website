﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    MSAP Center - An error has occured!
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="mainContent">
        <div style="margin-top: 10px; margin-bottom: 10px;">
            <p style="color: #f58220;">
                <strong>An error has occured!</strong></p>
            <div style="padding-left: 20px; margin-left: 20px;">
                <p style="color: #f58220; font-size: 18px;">
                    <img src="images/warning.png" alt="warning" />
                    An error has occured! <strong>Please contact <a href="mailto:msapcenter@seiservices.com">
                        MSAP center technical support group</a> for suport.</strong></p>
                <p>
                    Click <a href="default.aspx">here</a> to return to the home page.</p>
            </div>
        </div>
    </div>
</asp:Content>

