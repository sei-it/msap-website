﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class unsubscribe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void OnSubscribe(object sender, EventArgs e)
    {
        foreach (MagnetMailList mail in MagnetMailList.Find(x => x.Email.ToLower().Equals(txtSubscription.Text.ToLower())))
        {
            mail.UnSubscribed = true;
            mail.TimeStamp = DateTime.Now;
            mail.Save();
        }
    }
}