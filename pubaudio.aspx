﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="pubaudio.aspx.cs" Inherits="pubaudio" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center Resources - Multimedia Audio
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <a name="skip"></a>
    <div class="mainContent">
        <a href="resource.aspx">Library</a> &nbsp;&nbsp;> &nbsp;&nbsp;<a href="publications.aspx">Publications</a>
        &nbsp;&nbsp;Multimedia &nbsp;&nbsp;<a href="toolkits.aspx">Toolkits &amp; Guides</a>&nbsp;&nbsp;> &nbsp;&nbsp;
        <a href="multimedia.aspx">All</a>&nbsp;&nbsp;Audio&nbsp;&nbsp;<a href="pubvideo.aspx">Video</a>&nbsp;&nbsp;<a href="pubwebinar.aspx">Webinar</a>
        <h1>
            Multimedia Audio</h1>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
            ShowHeader="false" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            GridLines="None" OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
            <RowStyle BorderStyle="None" BorderColor="White" />
            <HeaderStyle BorderStyle="None" BorderColor="White" />
            <Columns>
                <asp:BoundField DataField="DisplayData" HtmlEncode="false" SortExpression="" HeaderText="" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
