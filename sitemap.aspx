﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="sitemap.aspx.cs" Inherits="sitemap" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Sitemap
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" media="screen, print" href="css/sitemapstyler.css" />
    <script type="text/javascript" src="js/sitemapstyler.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            MSAP Center Sitemap</h1>
   <br/>
        <div id="content">
            <ul id="sitemap">
                <li id="home"><a href="default.aspx">Home</a>
                    <ul>
                    	<li><a href="conference.aspx">Conferences</a></li>
                    	<li><a href="granteecorner.aspx">Grantee Corner</a></li>
                        <li><a href="news.aspx">Magnet School News Archive</a> </li>
                        <li><a href="newsletters.aspx">The Magnet Compass</a></li>
                        <li><a href="webinar.aspx">Webinars</a> </li>
                    </ul>
                </li>
                <li><a href="about.aspx">About Us</a>
                    <ul>
                        <li><a href="faq.aspx">MSAP Center FAQs</a></li>
                    </ul>
                </li>
                <li><div class="nolink">Resources</div>
                    <ul>
                        <li><a href="resource.aspx">Library</a>
                            <ul>
                                <li><a href="importantlinks.aspx">Important Links</a></li>
                                <li><a href="multimedia.aspx">Multimedia</a>
                                    <ul>
                                        <li><a href="pubaudio.aspx">Audio</a> </li>
                                        <li><a href="pubvideo.aspx">Video</a> </li>
                                        <li><a href="pubwebinar.aspx">Webinar</a></li>
                                    </ul>
                                </li>
                                <li><a href="publications.aspx">Publications</a></li>
                                <li><a href="toolkits.aspx">Toolkits and Guides</a></li>  
                            </ul>
                        </li>
                        <li><a href="news.aspx">Magnet School News Archive</a></li>
                        <li><a href="newsletters.aspx">The Magnet Compass Archive</a></li>
                    </ul>
                </li>
                <li><div class="nolink">Events</div>
                    <ul>
                        <li><a href="conference.aspx">Conferences</a> </li>
                        <li><a href="webinar.aspx">Webinars</a> </li>
                    </ul>
                </li>
                <li><div class="nolink">MSAP Grantees</div> 
                    <ul>
                        <li><a href="grantees.aspx">Grantee Abstracts</a> </li>
                        <li><a href="cornerarchives.aspx">Grantee Corner Archive</a></li>
                    </ul>
		</li>
                <li><div class="nolink">Technical Assistance</div>
                    <ul>
                        <li><a href="approach.aspx">Approach to Technical Assistance</a> </li>
                        <li><a href="assistance.aspx">Technical Assistance Request Form</a> </li>
                    </ul>
                </li>
                <li><a href="contact.aspx">Contact Us</a> </li>
                <li><a href="login.aspx">Login</a></li>
                <li><a href="sitesearch.aspx">Site Search</a></li>
                <li><a href="privacypolicy.aspx">Privacy Policy</a></li>
                <li><a href="disclaimer.aspx">Disclaimer</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
