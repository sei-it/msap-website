﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="links.aspx.cs" Inherits="links" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Equity Assistance Center
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <style type="text/css">
            a, a:visited
            {
                color: #333;
            }
        </style>
        <h1>
            Equity Assistance Centers</h1>
        <div style="float: right; margin-left: 50px; width: 450px;">
            <a href="http://www.idra.org/South_Central_Collaborative_for_Equity/" target="_blank">
                <strong>South Central Collaborative for Equity</strong></a>
            <br />
            Region VI: AR, LA, NM, OK, TX
            <p>
                <a href="http://www.meac.org/" target="_blank"><strong>Midwest Equity Assistance Center</strong></a>
                <br />
                Region VII: IA, KS, MO, NE
            </p>
            <p>
                <a href="http://www.msudenver.edu/eac/" target="_blank"><strong>Metropolitan State College of Denver</strong></a>
                <br />
                Region VIII: CO, MT, ND, SD, UT, WY
            </p>
            <p>
                <a href="http://wested.org/eac" target="_blank"><strong>Region IX Equity Assistance Center at WestEd</strong></a>
                <br />
                Region IX: AZ, CA, NV</p>
            <p>
                <a href="http://educationnorthwest.org/" target="_blank"><strong>The Equity Program
                    at Northwest Regional Educational Laboratory</strong></a>
                <br />
                Region X: AK, HI, ID, OR, WA, American Samoa, Guam, N. Mariana Islands, Republic
                of Palau</p>
        </div>
        <a href="http://neeac.alliance.brown.edu/" target="_blank"><strong>New England Equity
            Assistance Center at Brown University</strong></a>
        <br />
        Region I: CT, ME, MA, NH, RI, VT </p>
        <p>
            <a href="http://www.touro.edu/edgrad/EAC/index.asp" target="_blank"><strong>Equity Assistance
                Center - Touro College</strong></a>
            <br />
            Region II: NJ, NY, Puerto Rico, Virgin Is.</p>
        <p>
            <a href="http://www.maec.org/mac.html" target="_blank"><strong>Mid-Atlantic Equity Consortium</strong></a>
            <br />
            Region III: DE, Wash. DC, MD, PA, VA, WV
        </p>
        <p>
            <a href="http://se-equity.org/SEC/Home.html" target="_blank">
                <strong>Southeastern Equity Assistance Center</strong></a>
            <br />
            Region IV: AL, FL, GA, KY, MS, NC, SC, TN
        </p>
        <p>
            <a href="http://glec.education.iupui.edu" target="_blank"><strong>Great Lakes Equity Center</strong></a>
            <br />
            Region V: IL, IN, MI, MN, OH, WI
        </p>
    </div>
</asp:Content>
