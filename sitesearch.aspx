﻿<%@ Page Title="Site search" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="sitesearch.aspx.cs" Inherits="sitesearch" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP - Web site search
</asp:Content>
<%@ Register TagPrefix="Search" Namespace="SiteSearchASP.NET.Interface" Assembly="SiteSearchASP.NET" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Search this site</h1>
        <table border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td><input type="text" id="etacTxt" runat="server" size="15" />
                </td>
                <td>
                    <select class="msapTxt" id="SearchType" runat="server">
                        <option value="exact">Exact Phrase</option>
                        <option value="all">All words</option>
                        <option value="any">Any words</option>
                    </select>
                </td>
                <td>
                    <input class="msapBtn" type="submit" value="Search">
                </td>
            </tr>
        </table>
        <br />
        <br />
        <Search:InformationWhenSearchNotRan ID="InformationWhenSearchNotRan1" runat="server">
            Please enter search term above.<br />
            <br />
        </Search:InformationWhenSearchNotRan>
        <Search:InformationWhenSearchRan ID="InformationWhenSearchRan1" runat="server">
            Searching for '<%#Search.Render(Page, "SearchTerm")%>' found
            <%#Search.Render(Container, "PageCount")%>
            pages.<br />
            <br />
        </Search:InformationWhenSearchRan>
        <asp:DataGrid ID="ResultsDataGrid" runat="Server" PagerStyle-Mode="NumericPages"
            PagerStyle-HorizontalAlign="Right" PageSize="10" CellPadding="0" BorderWidth="0"
            CellSpacing="0" ShowHeader="false" AllowPaging="true" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <a href='<%# Search.Render(Container,"PageUrl") %>'><b>
                            <%#Search.Render(Container, "PageTitle")%></b></a>
                        <%# Search.Render(Container,"PageFileType") %>
                        <%#Search.Render(Container, "PageRelevance")%>%<br>
                        <%#Search.Render(Container, "PagecontentSummary")%><br>
                        <a href='<%# Search.Render(Container,"PageUrl") %>'>
                            <%# Search.Render(Container,"PageUrlShort") %></a> &nbsp; Size:
                        <%# Search.Render(Container,"PageSize") %>kb.<br>
                        <br>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <br />
        <br />
        <Search:Settings ID="Settings1" runat="server" ResultsControl="ResultsDataGrid" SearchTermControl="etacTxt"
            SearchTypeControl="SearchType" LicenseCode="xgI9JKYCumNJQsiCLVrSH5sYf1LXRDq5TLyG2USoLlg="
            MatchWholeWordOnly="false" HighlightFoundTermPrefix="<span style='background-color:#FFCC33'>"
            HighlightFoundTermSuffix="</span>" Visible="false" />
    </div>
</asp:Content>
