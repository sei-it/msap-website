﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_managemembers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetTWGMember.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    private void LoadData(int PageNumber)
    {
        GridView1.DataSource = MagnetTWGMember.All();
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        //txtTitle.Text = "";
        txtMemberName.Text = "";
        //txtOrganization.Text = "";
        txtBio.Content = "";
        hfID.Value = "";
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeNewsWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetTWGMember data = MagnetTWGMember.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        //txtTitle.Text = data.Title;
        txtMemberName.Text = data.MemberName;
        //txtOrganization.Text = data.Organization;
        txtBio.Content = data.Bio;
        mpeNewsWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetTWGMember data = new MagnetTWGMember();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetTWGMember.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        //data.Title = txtTitle.Text;
        data.MemberName = txtMemberName.Text;
        //data.Organization = txtOrganization.Text;
        data.Bio = txtBio.Content;
        data.Save();
        LoadData(0);
    }
}