﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using ExcelLibrary.SpreadSheet;

public partial class admin_managegpra : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }

    protected void OnDownload(object sender, EventArgs e)
    {
        int ReportPeriod = 1;
        int[] ColumnSum1 = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int[] ColumnSum2 = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        using (System.IO.MemoryStream output = new System.IO.MemoryStream())
        {
            Workbook workbook = new Workbook();
            Worksheet worksheet = new Worksheet("GPRA Mesaure 2");
            int idx = 0;

            MagnetDBDB db = new MagnetDBDB();

            //Measure 1

            //Measure 2
            for (int i = 0; i < 10; i++)
            {
                ColumnSum1[i] = 0;
                ColumnSum2[i] = 0;
            }
            var Measure2ReadingParticipation = from m in db.MagnetGPRAs
                                               from n in db.GranteeReports
                                               from l in db.MagnetGPRAPerformanceMeasures
                                               where m.ReportID == n.ID
                                               && n.ReportPeriodID == ReportPeriod
                                               && m.ID == l.MagnetGPRAID
                                               && l.ReportType == 3
                                               select l;
            foreach (var Data in Measure2ReadingParticipation)
            {
                ColumnSum1[0] += (int)Data.Indian;
                ColumnSum1[1] += (int)Data.Asian;
                ColumnSum1[2] += (int)Data.Black;
                ColumnSum1[3] += (int)Data.Hispanic;
                ColumnSum1[4] += (int)Data.Hawaiian;
                ColumnSum1[5] += (int)Data.White;
                ColumnSum1[6] += (int)Data.MultiRaces;
                ColumnSum1[7] += (int)Data.Total;
                if (Data.ExtraFiled1 != null) ColumnSum1[8] += (int)Data.ExtraFiled1;
                if (Data.ExtraFiled2 != null) ColumnSum1[9] += (int)Data.ExtraFiled2;
            }
            var Measure2ReadingArchievement = from m in db.MagnetGPRAs
                                              from n in db.GranteeReports
                                              from l in db.MagnetGPRAPerformanceMeasures
                                              where m.ReportID == n.ID
                                              && n.ReportPeriodID == ReportPeriod
                                              && m.ID == l.MagnetGPRAID
                                              && l.ReportType == 5
                                              select l;
            foreach (var Data in Measure2ReadingArchievement)
            {
                ColumnSum2[0] += (int)Data.Indian;
                ColumnSum2[1] += (int)Data.Asian;
                ColumnSum2[2] += (int)Data.Black;
                ColumnSum2[3] += (int)Data.Hispanic;
                ColumnSum2[4] += (int)Data.Hawaiian;
                ColumnSum2[5] += (int)Data.White;
                ColumnSum2[6] += (int)Data.MultiRaces;
                ColumnSum2[7] += (int)Data.Total;
                if (Data.ExtraFiled1 != null) ColumnSum2[8] += (int)Data.ExtraFiled1;
                if (Data.ExtraFiled2 != null) ColumnSum2[9] += (int)Data.ExtraFiled2;
            }

            worksheet.Cells[idx++, 0] = new Cell("Reading All students");
            if (ColumnSum1[7] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[7] / ColumnSum1[7] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading American Indian or Alaska Native students");
            if (ColumnSum1[0] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[0] / ColumnSum1[0] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading Asian students");
            if (ColumnSum1[1] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[1] / ColumnSum1[1] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading Black or African-American students");
            if (ColumnSum1[2] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[2] / ColumnSum1[2] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading Hispanic or Latino students");
            if (ColumnSum1[3] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[3] / ColumnSum1[3] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading Native Hawaiian or Other Pacific Islander students");
            if (ColumnSum1[4] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[4] / ColumnSum1[4] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading White students students");
            if (ColumnSum1[5] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[5] / ColumnSum1[5] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading Two or more races students");
            if (ColumnSum1[6] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[6] / ColumnSum1[6] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Reading Economically disadvantaged students students");
            if (ColumnSum1[8] != 0) worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[8] / ColumnSum1[8] * 100));
            worksheet.Cells[idx++, 0] = new Cell("Reading English learners students");
            if (ColumnSum1[9] != 0) worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[9] / ColumnSum1[9] * 100));

            //Measure 3
            for (int i = 0; i < 10; i++)
            {
                ColumnSum1[i] = 0;
                ColumnSum2[i] = 0;
            }
            var Measure2MathParticipation = from m in db.MagnetGPRAs
                                            from n in db.GranteeReports
                                            from l in db.MagnetGPRAPerformanceMeasures
                                            where m.ReportID == n.ID
                                            && n.ReportPeriodID == ReportPeriod
                                            && m.ID == l.MagnetGPRAID
                                            && l.ReportType == 4
                                            select l;
            foreach (var Data in Measure2ReadingParticipation)
            {
                ColumnSum1[0] += (int)Data.Indian;
                ColumnSum1[1] += (int)Data.Asian;
                ColumnSum1[2] += (int)Data.Black;
                ColumnSum1[3] += (int)Data.Hispanic;
                ColumnSum1[4] += (int)Data.Hawaiian;
                ColumnSum1[5] += (int)Data.White;
                ColumnSum1[6] += (int)Data.MultiRaces;
                ColumnSum1[7] += (int)Data.Total;
                if (Data.ExtraFiled1 != null) ColumnSum1[8] += (int)Data.ExtraFiled1;
                if (Data.ExtraFiled2 != null) ColumnSum1[9] += (int)Data.ExtraFiled2;
            }
            var Measure2MathArchievement = from m in db.MagnetGPRAs
                                           from n in db.GranteeReports
                                           from l in db.MagnetGPRAPerformanceMeasures
                                           where m.ReportID == n.ID
                                           && n.ReportPeriodID == ReportPeriod
                                           && m.ID == l.MagnetGPRAID
                                           && l.ReportType == 6
                                           select l;
            foreach (var Data in Measure2ReadingArchievement)
            {
                ColumnSum2[0] += (int)Data.Indian;
                ColumnSum2[1] += (int)Data.Asian;
                ColumnSum2[2] += (int)Data.Black;
                ColumnSum2[3] += (int)Data.Hispanic;
                ColumnSum2[4] += (int)Data.Hawaiian;
                ColumnSum2[5] += (int)Data.White;
                ColumnSum2[6] += (int)Data.MultiRaces;
                ColumnSum2[7] += (int)Data.Total;
                if (Data.ExtraFiled1 != null) ColumnSum2[8] += (int)Data.ExtraFiled1;
                if (Data.ExtraFiled2 != null) ColumnSum2[9] += (int)Data.ExtraFiled2;
            }

            worksheet.Cells[idx++, 0] = new Cell("Math All students");
            if (ColumnSum1[7] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[7] / ColumnSum1[7] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math American Indian or Alaska Native students");
            if (ColumnSum1[0] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[0] / ColumnSum1[0] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math Asian students");
            if (ColumnSum1[1] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[1] / ColumnSum1[1] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math Black or African-American students");
            if (ColumnSum1[2] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[2] / ColumnSum1[2] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math Hispanic or Latino students");
            if (ColumnSum1[3] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[3] / ColumnSum1[3] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math Native Hawaiian or Other Pacific Islander students");
            if (ColumnSum1[4] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[4] / ColumnSum1[4] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math White students students");
            if (ColumnSum1[5] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[5] / ColumnSum1[5] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math Two or more races students");
            if (ColumnSum1[6] > 0)
                worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[6] / ColumnSum1[6] * 100));
            else
                worksheet.Cells[idx++, 0] = new Cell(String.Format("0"));
            worksheet.Cells[idx++, 0] = new Cell("Math Economically disadvantaged students students");
            if (ColumnSum1[8] != 0) worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[8] / ColumnSum1[8] * 100));
            worksheet.Cells[idx++, 0] = new Cell("Math English learners students");
            if (ColumnSum1[9] != 0) worksheet.Cells[idx++, 0] = new Cell(String.Format("{0:#,0.00}", ColumnSum2[9] / ColumnSum1[9] * 100));

            //Measure 6
            int TotalSutdents = 0;
            decimal TotalBudget = 0;

            var Measure6 = from m in db.MagnetGPRAs
                           from n in db.GranteeReports
                           from l in db.MagnetGPRAPerformanceMeasures
                           where m.ReportID == n.ID
                           && n.ReportPeriodID == ReportPeriod
                           && m.ID == l.MagnetGPRAID
                           && l.ReportType == 7
                           select l;
            foreach (var Data in Measure2ReadingParticipation)
            {
                if (Data.Total != null) TotalSutdents += (int)Data.Total;
                if (Data.Budget != null) TotalBudget += (decimal)Data.Budget;
            }

            worksheet.Cells[idx++, 0] = new Cell("GPRa Performance Measure 6");
            worksheet.Cells[idx++, 0] = new Cell("$" + String.Format("{0:#,0.00}", TotalBudget / TotalSutdents));

            workbook.Worksheets.Add(worksheet);
            workbook.Save(output);
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=gprareport.xls");
            Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            output.Close();
        }
    }
}