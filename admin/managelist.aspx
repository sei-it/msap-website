﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managelist.aspx.cs" Inherits="admin_managelist" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Download Email List
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Upload Emails</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p>
        <asp:FileUpload ID="FileUplaod1" runat="server" /><asp:Button ID="Button1" runat="server"
            Text="Upload" CssClass="msapBtn" OnClick="OnUpload" />
    </p><br /><br />
    <h1>
        Download Email List</h1>
    <p style="text-align: left">
        <asp:Button ID="Newbutton" runat="server" Text="Download List" CssClass="msapBtn"
            OnClick="OnDownload" />
    </p>
</asp:Content>
