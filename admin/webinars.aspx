﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="webinars.aspx.cs" Inherits="admin_webinars" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Annual Performance Reporting 2011</h1>
    <h2>
        Annual Performance Reporting Guidance
Webinar</h2>
    <div id="Div1" runat="server">
    </div>
    <!-- 
    <h2>
        Annual Performance Reporting Feedback Webinar</h2>
    <div id="Div3" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr>
                <td width='90%'>
                    <b>File name</b>
                </td>
                <td width='10%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                    Annual Performance Reporting Feedback Presentation
                </td>
                <td width='10%'>
                    <a href='../doc/Annual Performance Reporting Feedback Presentation.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    Annual Performance Reporting Feedback Transcript
                </td>
                <td width='10%'>
                    <a href='../doc/Annual Performance Reporting Feedback Transcript.pdf' target='_blank'>PDF</a>
                </td>
            </tr>
            <tr>
                <td>
                    Annual Performance Reporting Feedback Webinar
                </td>
                <td width='10%'>
                    <a href='../doc/Annual Performance Reporting Feedback Webinar.wmv' target='_blank'>Video</a>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    APR Feedback Debriefing Transcript
                </td>
                <td width='10%'>
                    <a href='../doc/APR Feedback Debriefing Transcript.doc' target='_blank'>DOC</a>
                </td>
            </tr>
        </table>
    </div>
    -->
    <h2>
        Ad Hoc Reporting Guidance Webinar</h2>
    <div id="Div2" runat="server">
    </div>
<div style="padding-top: 40px;"><a href="webinars_2012.aspx"><strong>&lt;&lt; Annual Performance Reporting 2012</strong></a></div>
</asp:Content>
