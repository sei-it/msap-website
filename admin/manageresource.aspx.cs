﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Configuration;

public partial class admin_manageresource : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetPublication.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    protected void OnGridViewPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = (from m in db.MagnetPublications
                    orderby m.CreateDate descending
                    select new { m.ID, m.PublicationTitle, m.PublicationType, m.Organization, m.PublicationDate, m.PublicationKeyword }).ToList();
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtPublicationName.Text = "";
        txtPublicationType.Text = "";
        txtKeywords.Text = "";
        txtDescription.Content = "";
        txtOrganization.Text = "";
        txtPublicationDate.Text = "";
        ddlSubType.SelectedIndex = 0;
        foreach (ListItem li in ddlTopics.Items)
        {
                li.Selected = false;
        }
        hfID.Value = "";
    }
    protected void OnAddPublication(object sender, EventArgs e)
    {
        ClearFields();
        FileUpload1.Enabled = true;
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetPublication publication = MagnetPublication.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtPublicationName.Text = publication.PublicationTitle;
        txtPublicationType.Text = publication.PublicationType;
        txtKeywords.Text = publication.PublicationKeyword;
        txtOrganization.Text = publication.Organization;
        txtPublicationDate.Text = publication.PublicationDate;
        txtDescription.Content = publication.Description;
        if (publication.PublicationSubType != null) ddlSubType.SelectedValue = publication.PublicationSubType.ToString();
        //if (publication.PublicationTopic != null) ddlTopics.SelectedValue = publication.PublicationTopic.ToString();
        FileUpload1.Enabled = false;

        foreach (MagnetResourceCategory category in MagnetResourceCategory.Find(x => x.ResourceID == Convert.ToInt32(hfID.Value)))
        {
            foreach (ListItem li in ddlTopics.Items)
            {
                if (category.CategoryID == Convert.ToInt32(li.Value))
                    li.Selected = true;
            }
        }

        mpeResourceWindow.Show();
    }
    protected void OnSavePublication(object sender, EventArgs e)
    {

        MagnetPublication publication = new MagnetPublication();
  
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            publication = MagnetPublication.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            publication = new MagnetPublication();
            publication.CreateDate = DateTime.Now;
        }
        publication.PublicationTitle = txtPublicationName.Text;
        publication.PublicationType = txtPublicationType.Text;
        publication.PublicationKeyword = txtKeywords.Text;
        publication.Organization = txtOrganization.Text;
        publication.PublicationDate = txtPublicationDate.Text;
        publication.Description = txtDescription.Content;
        if (ddlSubType.SelectedIndex > 0) publication.PublicationSubType = Convert.ToInt32(ddlSubType.SelectedValue);
        if (ddlTopics.SelectedIndex > 0) publication.PublicationTopic = Convert.ToInt32(ddlTopics.SelectedValue);
        if (FileUpload1.HasFile)
        {
            string baseFolder = Server.MapPath("../applicationdoc/");
            string filePrefix = Guid.NewGuid().ToString();
            FileUpload1.SaveAs(baseFolder + filePrefix + FileUpload1.FileName);
            publication.OriginalFileName = FileUpload1.FileName;
            publication.PhysicalFileName = filePrefix + FileUpload1.FileName;
        }
        publication.Save();

        //topics
        foreach (ListItem li in ddlTopics.Items)
        {
            if (li.Selected)
            {
                if (MagnetResourceCategory.Find(x => x.ResourceID == publication.ID && x.CategoryID == Convert.ToInt32(li.Value)).Count == 0)
                {
                    MagnetResourceCategory category = new MagnetResourceCategory();
                    category.ResourceID = publication.ID;
                    category.CategoryID = Convert.ToInt32(li.Value);
                    category.Save();
                }
            }
            else
            {
                var data = MagnetResourceCategory.Find(x => x.ResourceID == publication.ID && x.CategoryID == Convert.ToInt32(li.Value));
                if (data.Count>0)
                {
                    MagnetResourceCategory.Delete(x => x.ID == data[0].ID);
                }
            }
        }

        LoadData(GridView1.PageIndex);
    }
    protected void btnNewTopic_Click(object sender, EventArgs e)
    {
        mpeTopicWindow.Show();
    }

    protected void OnSaveTopic(object sender, EventArgs e)
    {
        MagnetPublicationSubTopic newTopic = new MagnetPublicationSubTopic();
        newTopic.topicname = txtNewTopic.Text.Trim();
        newTopic.des = txtNewTopicDes.Text.Trim();
        newTopic.Save();

        LoadData(GridView1.PageIndex); 
    }
}