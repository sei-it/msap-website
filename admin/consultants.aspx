﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="consultants.aspx.cs" Inherits="admin_consultants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Consultants
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery.colorbox.js"></script>
    <link media="screen" rel="stylesheet" href="../css/colorbox.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".thickbox").colorbox({ width: 600, opacity: 0.6 });
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        MSAP Center Consultants</h1>
    The MSAP Center is pleased to offer free, individualized consulting services to members of the 2010 MSAP cohort. Please review the list of consultants, click through to the bios to learn more, and then contact the MSAP Center to schedule an introductory meeting to make sure that the consultant’s expertise meets your needs.
    <div id="content" runat="server">
    </div>
</asp:Content>
