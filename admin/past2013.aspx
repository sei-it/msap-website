﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="past2013.aspx.cs" Inherits="admin_past2013" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">Past MSAP Conferences - 2013
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="border:none;">Past Conferences</h1>
 	<div class="titles">2013 MSAP Project Directors Meeting, December 5-6, 2013</div>
    <div class="tab_area" style="width:840px;">
    		<ul class="tabs">
                <div id="Div1" runat="server" style="width: 600px; float: right;">
                <li><a href="past.aspx">2010</a></li> 
                <li><a href="past2011.aspx">2011</a></li>
                <li><a href="past2012.aspx">2012</a></li>
                <li class="tab_active">2013</li>
                
                </div>
        	</ul>
    	</div>
    	<br/>
      
	<div style="float: left; margin-top: 0; width: 20%;;padding-bottom:15px;">
        <p>
        <strong>Meeting Agenda</strong>
        <br />
        <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/MSAPPDMeetingAgenda.pdf" target="_blank">Download
                    PDF</a> <small>(149 KB)</small>
        </p>
        <p>
        <strong>Meeting Participant List</strong>
        <br />
        <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
            vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/ParticipantList.pdf" target="_blank">Download
                PDF</a> <small>(220 KB)</small>
        </p>
    </div>
	<div style="float: left; margin-top: 0; width: 75%;;padding-bottom:15px;border-left:1px solid #A9DEF0;padding-left:20px;">
     <h3 class="pastconf_subsection" style="padding-top:0px;">Day 1: December 5, 2013</h3>

    <p>   
        <strong class="pastconf_subtitle">Keynote – School Choice and Magnets: An Evolving Tradition</strong><br />
            
            <span class="pastconf_author"><i>Terry Grier, Ed.D.</i><br />
            Houston Independent School District</span>
       
     </p>
    <p>
        This presentation discusses Dr. Grier’s successes in turning around low-performing schools in Houston Independent School District and in increasing graduation rates and student performance. It also shares the district’s history with magnet schools and his successes with promoting magnet school goals in the district. 
	</p>
	<div id="Div3" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Keynote – School Choice and Magnets Presentation
                </td>
                <td width='20%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/KeynoteSchoolChoiceandMagnets.pdf" target="_blank">Download
                    PDF</a> <small>(2.4 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Managing Change in Magnet Schools</strong><br />
            <span class="pastconf_author"><i>David Gregory</i><br />
            G&D Associates</span>
       
     </p>
    <p>
        This presentation gives an overview of change management and how it facilitates successful implementation of magnet schools. Areas covered include reasons for change, the value of a change management process, change management models and structures, and real-world examples of successful, education-based change management.
	</p>
	<div id="Div4" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Managing Change in Magnet Schools Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/ChangeManagement.pdf" target="_blank">Download
                    PDF</a> <small>(4.1 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Taking Stock of Your Performance Measures </strong>(Refining Your Performance Measures)<br />
            <span class="pastconf_author"><i>Manya Walton, Ph.D., </i>MSAP Center<br />
            <i>Stacey Merola, Ph.D., </i>Merola Research LLC</span>
       
     </p>
    <p>
        This session presents information on developing measureable performance measures and objectives and leads participants in a focused discussion to learn about grantees’ methods and processes for establishing performance measures and setting targets. 
	</p>
	<div id="Div11" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Taking Stock of Your Performance Measures Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/Performance_Measures.pdf" target="_blank">Download
                    PDF</a> <small>(342 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Beginning the Change Management Process</strong><br />
            <span class="pastconf_author"><i>David Gregory</i><br />
            G&D Associates</span>
       
     </p>
    <p>
        This presentation helps participants examine the processes needed to develop change components. Topics include vision development, current situation analysis, planning development, and checks and balances. Participants will consider which skills, resources, and incentives need to be in place for their projects to succeed.
	</p>
	<div id="Div12" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Beginning the Change Management Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/BeginningChangeManagementProcess.pdf" target="_blank">Download
                    PDF</a> <small>(771 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Opportunity Mapping</strong><br />
            <span class="pastconf_author"><i>Jason Reece; Matt Martin</i><br />
            Kirwan Institute</span>
       
     </p>
    <p>
        This presentation discusses how opportunity maps are created, and how participants can use opportunity mapping in their own communities to make better data-informed decisions and start conversations with stakeholders about the needs of their communities. The session explores the use of opportunity mapping to target marketing and student recruitment, build community partnerships, and better understand community needs. 
	</p>
	<div id="Div13" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Opportunity Mapping Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/OpportunityMapping.pdf" target="_blank">Download
                    PDF</a> <small>(2.9 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Going “All in” With Elementary STEM Integration</strong><br />
            <span class="pastconf_author"><i>Melvina Jones; Hope Harrod; and Jennifer Wehner</i><br />
            District of Columbia Public Schools</span>
       
     </p>
    <p>
        This presentation explains the process the presenter followed in implementing their school’s STEM program and how they were able to fully integrate all four components of the program by involving all staff. It also discusses challenges and successes they have faced and will provide participants with ideas and strategies for implementing STEM successfully in their own schools.
	</p>
	<div id="Div14" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Going “All in” With Elementary STEM Integration Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/AllinwithSTEM.pdf" target="_blank">Download
                    PDF</a> <small>(421 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Engaging Families Through Cultural Brokers</strong><br />
            <span class="pastconf_author"><i>Linda Tillman, Ph.D.,</i>University of North Carolina at Chapel Hill<br />
            <i>Cheryl Fields-Smith, Ph.D.,</i> University of Georgia</span>
       
     </p>
    <p>
        This presentation provides research-based information about using cultural brokers as facilitators to increase levels of family engagement. It covers the following topics: what is a cultural broker, how cultural brokers can be identified, some of the roles of cultural brokers, and ways magnet schools might use cultural brokers to promote schoolwide family engagement. 
	</p>
	<div id="Div15" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Engaging Families Through Cultural Brokers Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/EngagingFamilies-CulturalBrokers.pdf" target="_blank">Download
                    PDF</a> <small>(288 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Aligning Magnet With District Priorities</strong><br />
            <span class="pastconf_author"><i>Terry Grier, Ed.D.; Lupita Hinojosa, Ed.D.; Jennifer Todd</i><br />
            Houston Independent School District</span>
       
     </p>
    <p>
        In this presentation the panel presents their experiences on aligning district priorities with the goals of the MSAP project. The panelists share strategies and successes from each level of district and program management and how they worked together to implement successful magnet programs.
	</p>
	<div id="Div16" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Aligning Magnet With District Priorities Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/AligningMagnet-DistrictPriorities.pdf" target="_blank">Download
                    PDF</a> <small>(461 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">What Does the Supreme Court Decision in <i>Fisher v. University of Texas</i> Have to Do With Magnet Schools?</strong><br />
            <span class="pastconf_author"><i>Mary Hanna-Weir; Richard Foster</i><br />
            Office for Civil Rights</span>
       
     </p>
    <p>
        In this presentation the panel presents their experiences on aligning district priorities with the goals of the MSAP project. The panelists share strategies and successes from each level of district and program management and how they worked together to implement successful magnet programs.
	</p>
	<div id="Div2" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Fisher v. University of Texas and Magnet Schools Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/OCRFisher.pdf" target="_blank">Download
                    PDF</a> <small>(340 KB)</small>
                </td>
            </tr>
        </table>
    </div>
   	<h3 class="pastconf_subsection">Day 2: December 6, 2013</h3>
    <p>
    	<strong class="pastconf_subtitle">Diversity, Equity, and Excellence</strong><br /> 
       <span class="pastconf_author"><i>Richard Foster; Mary Hanna-Weir</i><br/>
        Office for Civil Rights</span>
       
     </p>
    <p>
        This presentation provides information on OCR’s role in MSAP, and gives guidance on desegregation plan documents.
    </p>
    <div id="Div5" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Diversity, Equity, and Excellence Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/DiversityEquityExcellence.pdf" target="_blank">Download
                    PDF</a> <small>(283 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
    	<strong class="pastconf_subtitle">A Study of Exemplar Inclusive STEM-Focused Schools and Their Critical Components</strong><br />
        	<span class="pastconf_author"><i>Sharon Lynch, Ph.D.</i><br />
            George Washington University</span>
    </p>
    <p>
    This presentation discusses Dr. Lynch’s research on schools that develop new sources of STEM talent among underrepresented minority students and provide them with the means to succeed academically and professionally in a STEM field. The research focuses on ten critical components, ranging from curriculum and instruction to outside-the-classroom learning experiences and supports for students. The presentation also highlights the schools’ unique, community-based STEM programs that incorporate partnerships with local businesses and universities.
    </p>
    <div id="Div8" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Exemplar STEM-Focused Schools Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/STEMSchools.pdf" target="_blank">Download
                    PDF</a> <small>(1.3 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Compliance Monitoring</strong><br />
            <span class="pastconf_author"><i>Sara Allender; Seewan Eng</i><br />
            WestEd</span>
       
     </p>
    <p>
        This presentation reviews the basic components, purpose, and process of MSAP compliance monitoring; highlights lessons learned from the monitoring of 2010 grantees; and clarifies common misconceptions and myths about compliance monitoring.
	</p>
	<div id="Div6" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Compliance Monitoring Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/Compliance_Monitoring.pdf" target="_blank">Download
                    PDF</a> <small>(1.2 MB)</small>
                </td>
            </tr>
            <tr >
                <td>
                    Compliance Monitoring Handbook </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/MSAP_201314_Compliance_Monitoring_Handbook_9-16-13.pdf" target="_blank">Download
                    PDF</a> <small>(989 KB)</small>
                </td>
            </tr>
        </table>
    </div>

    <p>
        <strong class="pastconf_subtitle">Grants Management</strong><br />
            <span class="pastconf_author"><i>Brittany Beth; Tyrone Harris; Justis Tuia</i><br />
            U.S. Department of Education</span>
       
     </p>
    <p>
        This presentation reviews the basic components, purpose, and process of MSAP compliance monitoring; highlights lessons learned from the monitoring of 2010 grantees; and clarifies common misconceptions and myths about compliance monitoring.
	</p>
	<div id="Div7" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Grants Management Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2013_Events/GrantsManagement.pdf" target="_blank">Download
                    PDF</a> <small>(344 KB)</small>
                </td>
            </tr>
        </table>
    </div>
  
   </div>
<!--<div style="text-align:right;clear:both;width:820px;padding-right:20px;" class="space_top">
	2011 Meeting&nbsp;&nbsp;
	<a href="past.aspx"><strong>2010 Meeting</strong></a>
</div>
-->
</asp:Content>


