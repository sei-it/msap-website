﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="past_webinars.aspx.cs" Inherits="admin_past_webinars" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">Technical Assistance Webinars
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
.rpText{
	font-size: 1em;
	font-weight: bold;
	color: #016C60;
	font-family: Helvetica,Arial,sans-serif;
}

.Panel_slide{
	padding:15px;	
	color: #333333;
    font-family: Helvetica,Arial,sans-serif;
    font-size: 0.95em;
    line-height: 14px;
}

</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
	  <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
		<h1>Technical Assistance Webinars</h1>
    
          <telerik:RadPanelBar runat="server" ID="radpnlbarCategory" 
          OnItemDataBound="radpnlbarCategory_ItemDataBound" style="width:820px;" >
               <Items>
                    <%--<telerik:RadPanelItem Text="Marketing and Recruitment">
                           <ContentTemplate>
                             <div id="Div8" class="Panel_slide" runat="server">
                             </div>
                           </ContentTemplate>
                    </telerik:RadPanelItem>--%>
               </Items>
               <ExpandAnimation Type="None" />
               <CollapseAnimation Type="None" />
          </telerik:RadPanelBar>
  
</asp:Content>

