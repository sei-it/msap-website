﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managegpra.aspx.cs" Inherits="admin_managegpra" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Download GPRA Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Download GPRA Report</h1>
    <p style="text-align: left">
        <asp:Button ID="Newbutton" runat="server" Text="Download GPRA Report" CssClass="msapBtn"
            OnClick="OnDownload" />
    </p>
</asp:Content>
