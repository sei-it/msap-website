﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageusers.aspx.cs" Inherits="manageusers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Grantee Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
    function resetSearchTextBox() {
        var txtSearch = document.getElementById('<%=txtSearch.ClientID%>');
        txtSearch.text = "";
    }

</script>
    <link href="../css/autocomplete.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 435px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Manage Grantee Users</h1>
    <div>
 
       <%--  <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal">
    <asp:ListItem Value="1" >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2" Selected="True">MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>--%>

    </div>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <table class="content" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td class="post" valign="top">
      
				<table cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td class="header2" colspan="2">
							<b>Grantee User Search</b>
						</td>
					</tr>
			
					<tr class="post">
                    <td class="style1">
                   User Name: <asp:TextBox ID="txtSearch" runat="server" Width="95%"></asp:TextBox>
                   
                   <ajaxToolkit:AutoCompleteExtender ID="autoComplete1" runat="server"
  EnableCaching="true"
  BehaviorID="AutoCompleteEx"
  MinimumPrefixLength="1"
  TargetControlID="txtSearch"
  ServicePath="~/AutoComplete.asmx"
  ServiceMethod="GetMagnetUserList" 
  CompletionInterval="500"  
  CompletionSetCount="12"
  CompletionListCssClass="autocomplete_completionListElement"
  CompletionListItemCssClass="autocomplete_listItem"
  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
  DelimiterCharacters=";, :"
  ShowOnlyCurrentWordInCompletionListItem="true">
  <Animations>
  <OnShow>
  <Sequence>
  <%-- Make the completion list transparent and then show it --%>
  <OpacityAction Opacity="0" />
  <HideAction Visible="true" />

  <%--Cache the original size of the completion list the first time
    the animation is played and then set it to zero --%>
  <ScriptAction Script="// Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
  <%-- Expand from 0px to the appropriate size while fading in --%>
  <Parallel Duration=".4">
  <FadeIn />
  <Length PropertyKey="height" StartValue="0" 
	EndValueScript="$find('AutoCompleteEx')._height" />
  </Parallel>
  </Sequence>
  </OnShow>
  <OnHide>
  <%-- Collapse down to 0px and fade out --%>
  <Parallel Duration=".4">
  <FadeOut />
  <Length PropertyKey="height" StartValueScript=
	"$find('AutoCompleteEx')._height" EndValue="0" />
  </Parallel>
  </OnHide>
  </Animations>
  </ajaxToolkit:AutoCompleteExtender>


						</td>
						<td >
                        
                            <asp:Button ID="search" runat="server"  Text="Search" onclick="search_Click" />
                            <asp:Button ID="Clear" runat="server"  Text="Clear" 
                                OnClientClick ="resetSearchTextBox();" onclick="Clear_Click" />
                        
                        </td>
					</tr>
            
				</table>

              
			</td>
		</tr>
	</table>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New User" CssClass="msapBtn" OnClick="OnAdd" />
        </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            OnPageIndexChanging="OnPageIndexChanging">
            <Columns>
                <asp:BoundField DataField="UserName" SortExpression="" HeaderText="User Name" />
                <asp:BoundField DataField="Email" SortExpression="" HeaderText="Email" />
                <asp:BoundField DataField="Roles" SortExpression="" HeaderText="Roles" />
                <asp:BoundField DataField="Grantees" SortExpression="" HeaderText="Grantees" HtmlEncode="false" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton4" runat="server" Text="Password" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEditPassword"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hfID" runat="server" />
        <%-- New/Edit User --%>
        <asp:Panel ID="Panel1" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Grantee Users</div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    User Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox1" runat="server" Enabled="false" MaxLength="250" CssClass="msapTxt"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox2" runat="server" MaxLength="250" CssClass="msapTxt"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegExp1" runat="server"    
                                        ErrorMessage="Password length must be between 6 to 20 characters"
                                        ControlToValidate="TextBox2"    
                                        ValidationExpression="^[a-zA-Z0-9'@&#!$^&*%.\s]{6,20}$" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="Button1" runat="server" CssClass="msapBtn" Text="Change Password"
                                        OnClick="OnResetPassword" />
                                </td>
                                <td>
                                    <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Close Window" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="Button1" />
                        <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton3" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpePassword" runat="server" TargetControlID="LinkButton3"
            PopupControlID="Panel1" DropShadow="true" OkControlID="Button2" CancelControlID="Button2"
            BackgroundCssClass="magnetMPE" Y="20" />
        <%-- New/Edit User --%>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Grantee Users</div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    User Name:
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                    <asp:TextBox ID="txtUserName"  runat="server" MaxLength="250" CssClass="msapTxt" 
                                        ontextchanged="txtUserName_TextChanged"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="lblUserValidation"  runat="server" Text=""></asp:Label>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="250" CssClass="msapTxt"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"    
                                        ErrorMessage="Password length must be between 6 to 20 characters"
                                        ControlToValidate="txtPassword"    
                                        ValidationExpression="^[a-zA-Z0-9'@&#.\s]{6,20}$" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" CssClass="msapTxt"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Islocked:
                                </td>
                                <td>
                                    <asp:CheckBox ID="ckLocked" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Role:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlRoles" runat="server">
                                        <asp:ListItem Value="">Please select</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="2">
                             <h3>2010 MSAP Cohort</h3>
                            </td>
                            </tr>

                            <tr>
                                <td style="vertical-align:top">
                                  grantee:
                                </td>
                                <td>
                                    <asp:CheckBoxList ID="cblGrantees" runat="server">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="2">
                             <h3>2013 MSAP Cohort</h3>
                            </td>
                            </tr>
                            <tr>
                                <td style="vertical-align:top;">
                                  grantee:
                                </td>
                                <td>
                                    <asp:CheckBoxList ID="cblGrantees2013" runat="server">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                                </td>
                                <td>
                                    <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="Button14" />
                        <asp:AsyncPostBackTrigger ControlID="Button15" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
            BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
