﻿<%@ Page Title="Manage News" Language="C#" MasterPageFile="~/admin/magnetadmin.master"
    AutoEventWireup="true" CodeFile="managehomepageWebinars.aspx.cs" Inherits="admin_managehomepageWebinars" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Home Page Webinars
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Manage Home Page Webinars</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p style="text-align: left">
        <asp:Button ID="Newbutton" runat="server" Text="New Webinar" CssClass="msapBtn" OnClick="OnAddNews" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        OnPageIndexChanging="OnGridViewPageIndexChanged">
        <Columns>
            <asp:BoundField DataField="Title" SortExpression="" HeaderText="Title" />
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date Time" />
            <asp:BoundField DataField="SortField" SortExpression="" HeaderText="Sort" />
            <asp:BoundField DataField="Description" SortExpression="" HeaderText="Description" />
            <asp:BoundField DataField="CreateDate" SortExpression="" HeaderText="Created Date" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hfID" runat="server" />
    <%-- News --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Webinar</div>
            <table>
                <tr>
                    <td>
                        Title:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Organization:
                    </td>
                    <td>
                        <asp:TextBox ID="txtOrganization" runat="server" MaxLength="500" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Date Time:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDate" runat="server" MaxLength="50" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Sort:
                    </td>
                    <td>
                        <asp:TextBox ID="txtSortField" runat="server" MaxLength="2"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99" TargetControlID="txtSortField">
                        </ajax:MaskedEditExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description:
                    </td>
                    <td>
                        <cc1:Editor ID="txtDescription" runat="server" Width="470" Height="200">
                        </cc1:Editor>
                    </td>
                </tr>
                <tr>
                    <td>
                        Display On Home Page:
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblDisplay" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveNews" />
                    </td>
                    <td>
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeNewsWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>
