﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="managecompass.aspx.cs" Inherits="admin_managecompass" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage Newsletter File
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Manage Newsletter File</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
  
    <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New newsletter File" CausesValidation="false" CssClass="msapBtn" OnClick="OnAdd" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
             <asp:TemplateField>
             <HeaderTemplate>Image</HeaderTemplate>
                <ItemTemplate>
                <asp:Image ID="imageid" runat="server" Width="100px" ImageUrl='<%#"../img/Handler.ashx?CompassID=" + Eval("id")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DocumentName" SortExpression="" HeaderText="Document Name" />
            <asp:BoundField DataField="CreatedBy" SortExpression="" HeaderText="Created By" />
            <asp:BoundField DataField="CreatedDate" SortExpression="" HeaderText="Created Date" />
             <asp:BoundField DataField="modifiedBy" SortExpression="" HeaderText="modified By" />
            <asp:BoundField DataField="ModifiedDate" SortExpression="" HeaderText="ModifiedDate" />
            <asp:BoundField DataField="isCurrent" SortExpression="" HeaderText="Display on Homepage" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hfID" runat="server" />
    <%-- News --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Newsletter</div>
            <table>
                <tr>
                    <td>
                        Document Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDocumentName" CssClass="msapTxt" runat="server" Enabled="false" MaxLength="500" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        File*:
                    </td>
                    <td>
                        <asp:FileUpload ID="fuploadDoc" runat="server" CssClass="msapTxt" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Newsletter Image*:
                    </td>
                    <td>
                       <asp:FileUpload ID="fUploadImage" runat="server" CssClass="msapTxt" />
                    </td>
                </tr>
                <tr>
                <td>Image Alt:</td>
                <td><asp:TextBox ID="txtAlt" runat="server" />
                </td>
                </tr>
                <tr>
                <td>Issue Date*:
                </td>
                <td>
                    <asp:DropDownList ID="ddlmonth" runat="server">
                    
                        <asp:ListItem Selected="True" Value="">Select Month</asp:ListItem>
                        <asp:ListItem Value="1">01</asp:ListItem>
                        <asp:ListItem Value="2">02</asp:ListItem>
                        <asp:ListItem Value="3">03</asp:ListItem>
                        <asp:ListItem Value="4">04</asp:ListItem>
                        <asp:ListItem Value="5">05</asp:ListItem>
                        <asp:ListItem Value="6">06</asp:ListItem>
                        <asp:ListItem Value="7">07</asp:ListItem>
                        <asp:ListItem Value="8">08</asp:ListItem>
                        <asp:ListItem Value="9">09</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlyear" runat="server">
                        <asp:ListItem Selected="True" Value="">Select Year</asp:ListItem>
                        <asp:ListItem>2011</asp:ListItem>
                        <asp:ListItem Value="2012"></asp:ListItem>
                        <asp:ListItem>2013</asp:ListItem>
                        <asp:ListItem>2014</asp:ListItem>
                        <asp:ListItem>2015</asp:ListItem>
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem>2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlmonth"
                        ErrorMessage="Month is required."></asp:RequiredFieldValidator>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlyear"
                        ErrorMessage="Year is required."></asp:RequiredFieldValidator>
                </td>
                </tr>
                  <tr>
    <td>Compass Text:</td>
    <td>
        <cc1:Editor ID="edtContent" runat="server" />
    </td>
    </tr>
    <tr>
    <td>Display On Home Page:</td>
    <td>
     <asp:RadioButtonList ID="rblDisplay" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>
                        </asp:RadioButtonList>
    </td>
    </tr>
                <tr>
                    <td colspan='2'>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                    </td>
                    <td>
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeNewsWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>
