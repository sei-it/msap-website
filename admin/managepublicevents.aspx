﻿<%@ Page Title="Manage Resource" Language="C#" MasterPageFile="~/admin/magnetadmin.master"
    AutoEventWireup="true" CodeFile="managepublicevents.aspx.cs" Inherits="admin_managepublicevents" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Public Events
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Manage Public Events</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p style="text-align: left">
        <asp:Button ID="Newbutton" runat="server" Text="New Events" CssClass="msapBtn" OnClick="OnAddEvent" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        OnPageIndexChanging="OnGridViewPageIndexChanged">
        <Columns>
            <asp:BoundField DataField="EventType" SortExpression="" HeaderText="Type" />
            <asp:BoundField DataField="EventTitle" SortExpression="" HeaderText="Title" />
            <asp:BoundField DataField="Organization" SortExpression="" HeaderText="Organization" />
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" />
            <asp:BoundField DataField="Time" SortExpression="" HeaderText="Time" />
            <asp:BoundField DataField="Registration" SortExpression="" HeaderText="Registration" />
            <asp:BoundField DataField="Location" SortExpression="" HeaderText="Location" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hfID" runat="server" />
    <%-- Event --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Public Events</div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                Event Type:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblEventType" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="true" OnSelectedIndexChanged="OnEventTypeChanged">
                                    <asp:ListItem>Webinars</asp:ListItem>
                                    <asp:ListItem Selected="true">Conferences</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Event Title:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEventTitle" runat="server" MaxLength="500" Width="470" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Organization:
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrganization" runat="server" MaxLength="5000" Width="470" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="rwStartDate">
                            <td>
                                Start Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" runat="server" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDate" runat="server" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="rwTime">
                            <td>
                                Time:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTime" runat="server" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="rwRegistration">
                            <td>
                                Registration:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRegistration" runat="server" MaxLength="300" Width="470" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="rwLocation">
                            <td>
                                Location:
                            </td>
                            <td>
                                <asp:TextBox ID="txtLocation" runat="server" MaxLength="500"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Display:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblDisplay" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="true">No</asp:ListItem>
                                    <asp:ListItem>Yes</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description:
                            </td>
                            <td>
                                <cc1:Editor ID="txtDescription" runat="server" Width="470" Height="200">
                                </cc1:Editor>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                            </td>
                            <td>
                                <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button14" /> 
                    <asp:AsyncPostBackTrigger ControlID="Button15" EventName="Click" /> 
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>
