﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="editAPRGuidance.aspx.cs" Inherits="admin_editAPRGuidance" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
  

    function keyPress(sender, args) {
        var text = sender.get_value() + args.get_keyCharacter();
        if (!text.match('^[0-9]+$'))
            args.set_cancel(true);
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
     <asp:Panel ID="Panel1" runat="server"   >
        <div class="pwebinarDiv">
            <div class="mpeDivHeader">
                Add/Edit APR Guide</div>
            <table>    
                <%--<tr>
                    <td >
                        Icon Image:
                    </td>
                    <td >
                    <asp:FileUpload ID="fuploadimg" runat="server" CssClass="msapDataTxt" Height="19px"
                            Width="474px" />
                    </td>
                   <td><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="fuploadimg" 
ErrorMessage="Invalid Image File" 
ValidationExpression= 
"^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.gif|.GIF|.png|.PNG)$"> 
</asp:RegularExpressionValidator> 
</td>
                </tr>
                <tr>
                    <td>
                        Icon alternate text:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAlt" runat="server" MaxLength="500" Width="470"></asp:TextBox>
                    </td>
                    
                </tr>--%>
                  <tr>
                    <td >
                        Title*:
                    </td>
                    <td >
                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" Width="470" ></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtTitle" runat="server" SetFocusOnError="true" ErrorMessage="A title is required."></asp:RequiredFieldValidator>--%>
                    </td>
                    <td ></td>
                    </tr>
       <tr>
                    <td>
                        Event Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEventDate" runat="server" MaxLength="500" Width="470" ></asp:TextBox>

                    </td>
                    <td>
                        <asp:Image ID="ibtnCalendar" runat="server" ImageUrl="~/images/calendar.png" />
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" ControlToValidate="txtEventDate" runat="server" ErrorMessage="Event date is required."/>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="txtEventDate" ErrorMessage="Datetime is not well formated." 
                            ForeColor="Red" 
                            ValidationExpression="^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$" />
                     
                        <ajax:CalendarExtender ID="CalendarExtender1" runat="server"  CssClass="AjaxCalendar" Format="MM/dd/yyyy" PopupButtonID="ibtnCalendar" 
                            PopupPosition="Right" TargetControlID="txtEventDate">
                        </ajax:CalendarExtender>

                     </td>
                </tr>
                <tr>
                    <td>
                        Description:
                    </td>
                    <td>
                        <cc:Editor ID="txtDescription" runat="server" Width="470px" Height="240px"/>
                    </td>
                     <td rowspan="6" style="vertical-align:top;">
                    <asp:Panel ID="pnlImage" runat="server" Width="280px" Height="220px"></asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Display On Page:
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblwPublish" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                  
                </tr>
                <tr>
                <td>Display Order:</td>
                <td colspan='2'>
                  <telerik:RadTextBox ID="txtDisOrder" runat="server" oncopy="return false" onpaste="return false" oncut="return false">
                    <ClientEvents OnKeyPress="keyPress" />
                  </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan='3'>
                        &nbsp;
                    </td>
                </tr>
                  <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="msapBtn" Text="Save Record"  OnClick="OnSaveWebinar" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" CssClass="msapBtn" Text="Cancel" 
                            onclick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

           <p style="text-align:left">
        <asp:Button ID="btnNewUploadFile" runat="server" Text="Upload New File" CssClass="msapBtn" OnClick="OnAddUploadFile" />
    </p>
    <asp:GridView ID="gvWebinar" runat="server"
        PageSize="20" AutoGenerateColumns="False" CssClass="msapTbl" 
        DataSourceID="ldsFileUpload" DataKeyNames="id"
        >
        <Columns>
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" Visible="false"
                ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="webinar_id" HeaderText="webinar_id" Visible="false"
                SortExpression="webinar_id" />
            <asp:BoundField DataField="FileName" HeaderText="FileName" 
                SortExpression="FileName" />
            <asp:BoundField DataField="FileType" HeaderText="FileType" 
                SortExpression="FileType" />
            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" 
                SortExpression="CreatedDate" />
            <asp:BoundField DataField="CreatedBy" HeaderText="Created By" 
                SortExpression="CreatedBy" />
             <asp:BoundField DataField="displayorder" HeaderText="Display Order" 
                SortExpression="FileURI" />
            <asp:CheckBoxField DataField="isActive" HeaderText="Active" Visible="false"
                SortExpression="isActive" />
            <asp:CheckBoxField DataField="isPublish" HeaderText="Publish" Visible="false"
                SortExpression="isPublish" />
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="btnFileEdit" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditFile"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
       <%-- <EmptyDataTemplate>
        <table class="msapTbl" cellspacing="0" rules="all" border="1" id="ctl00_ContentPlaceHolder1_GridView1" style="border-collapse:collapse;">
		<tr>
			<th scope="col">File Name</th>
            <th scope="col">File URI</th>
            <th scope="col">File Type</th>
            <th scope="col">Active Status</th>
            <th scope="col">Published Status</th>
            <th scope="col">&nbsp;</th>
		</tr>
            <tr>
			<td colspan="6">
            <table border="0" width="100%">
				<tr>
					<td ><span>No Record!</span></td>
				</tr>
			</table>
            </td>
		</tr>
	</table>
        </EmptyDataTemplate>--%>
    </asp:GridView>
    <asp:HiddenField ID="hfFileID" runat="server" />
    <%-- News --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Upload File</div>
            <table>
                <tr>
                    <td>
                        Upload File Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFileName" runat="server" MaxLength="250" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                      
                    </td>
                    <td>
                    <asp:FileUpload ID="fileuploadDoc" runat="server" CssClass="msapDataTxt" Height="19px" Width="474px" />
                    
                    </td>
                </tr>
                <tr>
                <td> File URI: </td>
                <td>
                    <asp:Literal ID="ltlfilepath" runat="server"></asp:Literal>
                </td>
                </tr>
                   <tr>
                    <td>
                        Display on Page:
                    </td>
                    <td>
                          <asp:RadioButtonList ID="rblfPublish" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                <td>Display Order:</td>
                <td >
                  <telerik:RadTextBox ID="txtDisplayOrder" runat="server" oncopy="return false" onpaste="return false" oncut="return false">
                    <ClientEvents OnKeyPress="keyPress" />
                  </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSaveFile" runat="server" CssClass="msapBtn" Text="Save File" OnClick="OnSaveUploadFile" />
                    </td>
                    <td>
                        <asp:Button ID="btnSaveFileCancel" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeAddUploadFile" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="btnSaveFileCancel" CancelControlID="btnSaveFileCancel"
        BackgroundCssClass="magnetMPE" Y="20" />
    <asp:LinqDataSource ID="ldsFileUpload" runat="server" 
         ContextTypeName="APRGuidanceDataClassesDataContext" EntityTypeName="" 
         TableName="APRGuidanceUploadFiles" OrderBy="displayorder" >
    </asp:LinqDataSource>

</asp:Content>


