<%@ Page Title="MSAP Past Conferences" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">Past MSAP Conferences - 2012
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="border:none;">Past Conferences</h1>
 	<div class="titles">2012 MSAP Project Directors Meeting, November 1-2, 2012</div>
    <div class="tab_area" style="width:840px;">
    		<ul class="tabs">
                <div runat="server" style="width: 600px; float: right;">
                <li><a href="past.aspx">2010</a></li> 
                <li><a href="past2011.aspx">2011</a></li>
                <li class="tab_active">2012</li>
                <li><a href="past2013.aspx">2013</a></li>
                </div>
        	</ul>
    	</div>
    	<br/>
      
	<div style="float: left; margin-top: 0; width: 20%;;padding-bottom:15px;">
            <p>
            <strong>Meeting Agenda</strong>
            <br />
            <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                    vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/2012_MSAP_PD_Meeting_Agenda.pdf" target="_blank">Download
                        PDF</a> <small>(365 KB)</small>
  		 </p>
    </div>
	<div style="float: left; margin-top: 0; width: 75%;;padding-bottom:15px;border-left:1px solid #A9DEF0;padding-left:20px;">
     <h3 class="pastconf_subsection" style="padding-top:0px;">Day 1: November 1, 2012</h3>

     <p>   
        <strong class="pastconf_subtitle">Performance Reporting Overview</strong><br />
            
            <span class="pastconf_author"><i>Anna Hinton, Ph.D.; Rosie Kelley; Brittany Beth; Tyrone Harris; Veronica Edwards</i><br />
            U.S. Department of Education</span>
       
     </p>
     <p>
        This presentation addresses common budget questions, such as which costs are and are not allowable. It also discusses the Annual Performance Report, and how to close out the grant project. 
        </p>
	<div id="Div3" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Performance Reporting Presentation
                </td>
                <td width='20%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Performance_Reporting_Overview_ED.pdf" target="_blank">Download
                    PDF</a> <small>(688 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Highlights from Year 1 GPRA Analysis</strong><br />
            <span class="pastconf_author"><i>Manya Walton, Ph.D.</i><br />
            MSAP Center</span>
       
     </p>
     <p>
        This presentation describes the analysis of the Year 1 GPRA data, and provides highlights on cohort characteristics; minority group isolation elimination, reduction, and prevention; and student academic achievement.
	</p>
	<div id="Div4" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    GPRA Analysis Highlights Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Highlights_from_Year_1_GPRA_Data.pdf" target="_blank">Download
                    PDF</a> <small>(324 KB)</small>
                </td>
            </tr>
        </table>
       </div>
        <p>
        <strong class="pastconf_subtitle">Compliance Monitoring 101</strong><br />
            <span class="pastconf_author"><i>Sara Allender; Seewan Eng</i><br />
            WestEd</span>
       
     </p>
     <p>
        This session includes information on the compliance monitoring process and clarifies misconceptions about the process. It includes strategies and tools for successfully navigating compliance monitoring.
	</p>
	<div id="Div8" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Compliance Monitoring 101 Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Compliance_Monitoring_101_WestEd.pdf" target="_blank">Download
                    PDF</a> <small>(1.8 MB)</small>
                </td>
            </tr>
            <tr>
                <td>
                    Grantee Responsibilities Overview
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Monitoring_Grantee_Overview.pdf" target="_blank">Download
                    PDF</a> <small>(553 KB)</small>
                </td>
            </tr>
              <tr  bgcolor='#E8E8E8'>
                <td>
                    Compliance Monitoring Documents
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Monitoring_documents.pdf" target="_blank">Download
                    PDF</a> <small>(397 kB)</small>
                </td>
            </tr>
              <tr>
                <td>
                    Sample Site Visit Agenda
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Monitoring_sampleagenda.pdf" target="_blank">Download
                    PDF</a> <small>(557 KB)</small>
                </td>
            </tr>
              <tr  bgcolor='#E8E8E8'>
                <td>
                    Compliance Monitoring Handbook
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Y2_Compliance_Monitoring_Handbook.pdf" target="_blank">Download
                    PDF</a> <small>(737 KB)</small>
                </td>
            </tr>
        </table>
    </div>
     <p>
        <strong class="pastconf_subtitle">Program Sustainability: Moving from Planning to Action</strong><br />
            <span class="pastconf_author"><i>Shawn Stelow Griffin</i>, The Finance Project<br/>
            <i>Freddie Martin, Ph.D.</i></span>
     </p>
     <p>
        This presentation includes information on identifying financing needs and strategies. It also discusses the development of a sustainability action plan.  
	</p>
	<div id="Div9" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Program Sustainability Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/MSAP_2012_Sustainability_action_presentation_final_TFP_and_Martin.pdf" target="_blank">Download
                    PDF</a> <small>(627 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Building a Recognizable Brand</strong><br />
            <span class="pastconf_author"><i>David Gregory</i>, G&amp;D Associates<br/>
            <i>Alison Vai</i>, Metropolitan Nashville Public Schools<br/>
            <i>Gary Bryan</i>, Duval County Public Schools</span>
     </p>
     <p>
        This presentation reviews the what, why, and how of marketing for magnet schools, and shares practical applications of the marketing concepts. 
	</p>
	<div id="Div10" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Building a Recognizable Brand Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/MSAP_2012_Building_a_Recognizable_Brand_Final_GandD_Associates.pdf" target="_blank">Download
                    PDF</a> <small>(2.6 MB)</small>
                </td>
            </tr>
        </table>
    </div>
      <p>
        <strong class="pastconf_subtitle">Magnet Theme Integration in Motion</strong><br />
            <span class="pastconf_author"><i>Bonnie Hansen-Grafton</i></span>
       
     </p>
     <p>This presentation introduces the MSAP Center�s online theme integration course, and walks users through the steps for creating a theme integration plan for magnet 
     schools.</p>
	<div id="Div11" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                   Theme Integration Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/Magnet_Theme_Integration_Hansen_Grafton.pdf" target="_blank">Download
                    PDF</a> <small>(2 MB)</small>
                </td>
            </tr>
            <tr>
                <td>
                    Gauging School Readiness Worksheet
                </td>
                <td>
                   <img src="../images/Microsoft_Word_Icon.png" width="17" height="17" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/gauging_school_readiness.doc" target="_blank">Download
                    DOC</a> <small>(496 KB)</small>
                </td>
            </tr>
              <tr  bgcolor='#E8E8E8'>
                <td>
                    Action Plan Worksheet
                </td>
                <td>
                   <img src="../images/Microsoft_Word_Icon.png" width="17" height="17" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/theme_integration_action_plan.doc" target="_blank">Download
                    DOC</a> <small>(238 KB)</small>
                </td>
            </tr>
        </table>
    </div>
   	<h3 class="pastconf_subsection">Day 2: November 2, 2012</h3>
    <p>
    	<strong class="pastconf_subtitle">Diversity, Equity, and Excellence</strong><br /> 
       <span class="pastconf_author"><i>Richard Foster; Mary Hanna-Weir</i><br/>
       Office for Civil Rights
       </span>
       
     </p>
     <p>
        This presentation provides information on OCR�s role in MSAP, and gives guidance on desegregation plan documents. 
    </p>
    <div id="Div5" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Diversity, Equity, and Excellence Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/OCR_Diversity_Equity_Excellence.pdf" target="_blank">Download
                    PDF</a> <small>(1 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
    	<strong class="pastconf_subtitle">Compliance Monitoring: Themes and Trends from Year 1</strong><br />
        	<span class="pastconf_author"><i>Sara Allender; Seewan Eng</i><br />
            WestEd</span>
        
    </p>
    <p>
    This presentation provides a description of year 1 activities and a summary of year 1 findings, and discusses lessons learned,  common challenges, and recommendations based off the year 1 findings.
    </p>

    <div id="Div6" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                  Themes and Trends Presentation
                </td>
                <td>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2012_Events/MSAP_2012_Compliance_Monitoring_Trends_and_Themes_Year_1_WestEd.pdf" target="_blank">Download
                    PDF</a> <small>(270 KB)</small>
                </td>
            </tr>
        </table>
    </div>
   </div>
<!--<div style="text-align:right;clear:both;width:820px;padding-right:20px;" class="space_top">
	2011 Meeting&nbsp;&nbsp;
	<a href="past.aspx"><strong>2010 Meeting</strong></a>
</div>
-->
</asp:Content>
