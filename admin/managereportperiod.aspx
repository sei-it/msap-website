﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managereportperiod.aspx.cs" Inherits="admin_managesurvey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Quareter Report Period
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Manage Quareter Report Period</h1>
    <ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        Report Period:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPeriod" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnPeriodChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Period Time:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFrom" runat="server" CssClass="msapTxt" >
                        </asp:TextBox>
                        To
                        <asp:TextBox ID="txtTo" runat="server" CssClass="msapTxt" >
                        </asp:TextBox>
                        <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"></ajax:CalendarExtender>
                        <ajax:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"></ajax:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="Newbutton" runat="server" Text="Save Data" CssClass="msapBtn" OnClick="OnSave" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
