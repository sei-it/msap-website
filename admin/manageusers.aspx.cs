﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Web.Security;
using YAF.Classes;
using YAF.Classes.Core;
using YAF.Classes.Utils;
using YAF;
using YAF.Modules;

public partial class manageusers : System.Web.UI.Page
{
    MagnetDBDB db = new MagnetDBDB();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var reportPeriods = MagnetReportPeriod.Find(x => x.isReportPeriod == true);
            if (reportPeriods.Count == 0)
            {
                reportPeriods = MagnetReportPeriod.Find(x => x.isActive == true);
            }

            // cohort 2010 grantee list
            foreach (MagnetGrantee grantee in MagnetGrantee.Find(x => x.isActive && x.CohortType == 1).OrderBy(x => x.GranteeName))
            {
                cblGrantees.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
            }

            if (reportPeriods.Last().ID > 8)  //starting from 2015 APR
            {
                // cohort 2013 grantee list
                foreach (MagnetGrantee grantee in MagnetGrantee.Find(x => x.isActive && x.CohortType == 2 && x.ID != 69 && x.ID != 70).OrderBy(x => x.GranteeName))
                {
                    cblGrantees2013.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
                }
            }
            else   //2014 APR and 2014 AH hoc
            {
                foreach (MagnetGrantee grantee in MagnetGrantee.Find(x => x.isActive && x.CohortType == 2).OrderBy(x => x.GranteeName))
                {
                    cblGrantees.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
                }
            }

            foreach (string role in Roles.GetAllRoles())
            {
                ddlRoles.Items.Add(new ListItem(role, role));
            }
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        Session["GranteeUsers"] = null;
        txtSearch.Text = "";
        MagnetGranteeUser user = MagnetGranteeUser.SingleOrDefault(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        Membership.DeleteUser(user.UserName);
        user.Delete();

        //delete from messageboard
        //foreach (yaf_User boardUser in yaf_User.Find(x => x.Name.ToLower().Equals(user.UserName.ToLower())))
        //{
        //    MagnetDBDB db = new MagnetDBDB();
        //    db.yaf_user_delete(boardUser.UserID).Execute();
        //}

        if (Session["PageNumber"] == null)
            LoadData(0);
        else
            LoadData(Convert.ToInt32(Session["PageNumber"]));

    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["PageNumber"] = e.NewPageIndex;
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        List<GranteeUser> users = new List<GranteeUser>();
        if (Session["GranteeUsers"] == null)
        {
            foreach (var user in MagnetGranteeUser.All().OrderBy(x => x.UserName))
            {
                GranteeUser granteeUser = new GranteeUser();
                granteeUser.ID = user.ID;
                granteeUser.UserName = user.UserName;
                MembershipUser membershipUser = Membership.GetUser(user.UserName, false);
                if (membershipUser != null)
                    granteeUser.Email = membershipUser.Email;
                string roles = "";
                foreach (string role in Roles.GetRolesForUser(user.UserName))
                {
                    roles += role + " ";
                }
                string grantees = "";
                foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == user.ID))
                {
                    grantees += MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID).GranteeName + "<br/>";
                }
                granteeUser.Grantees = grantees;
                granteeUser.Roles = roles;
                users.Add(granteeUser);
            }
        }
        else
            users = (List<GranteeUser>)Session["GranteeUsers"];

        GridView1.DataSource = users;
        Session["GranteeUsers"] = users;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();

    }

    //private void LoadData(int PageNumber)
    //{
    //    List<GranteeUser> users = new List<GranteeUser>();
    //    if (Session["GranteeUsers"] == null)
    //    {
    //        var GranteeUserlist = from drwg in db.MagnetGrantees
    //                              join drwu in db.MagnetGranteeUsers
    //                              on drwg.ID equals drwu.GranteeID
    //                              where 
    //                              //drwg.CohortType == Convert.ToInt32(rbtnlstCohort.SelectedValue) &&
    //                               drwg.isActive == true
    //                              orderby drwu.UserName
    //                              select drwu;

    //        foreach (var user in GranteeUserlist)
    //        {
    //            GranteeUser granteeUser = new GranteeUser();
    //            granteeUser.ID = user.ID; 
    //            granteeUser.UserName = user.UserName;
    //            MembershipUser membershipUser = Membership.GetUser(user.UserName, false);
    //            if (membershipUser != null)
    //                granteeUser.Email = membershipUser.Email;
    //            string roles = "";
    //            foreach (string role in Roles.GetRolesForUser(user.UserName))
    //            {
    //                roles += role + " ";
    //            }
    //            string grantees = "";
    //            foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == user.ID))
    //            {
    //                grantees += MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID).GranteeName + "<br/>";
    //            }
    //            granteeUser.Grantees = grantees;
    //            granteeUser.Roles = roles;
    //            users.Add(granteeUser);
    //        }
    //    }
    //    else
    //        users = (List<GranteeUser>)Session["GranteeUsers"];

    //    GridView1.DataSource = users;
    //    Session["GranteeUsers"] = users ;
    //    GridView1.PageIndex = PageNumber;
    //    GridView1.DataBind();

    //}
    private void ClearFields()
    {
        txtUserName.Enabled = true;
        txtUserName.Text = "";
        txtPassword.Enabled = true;
        txtPassword.Text = "";
        txtEmail.Text = "";
        ddlRoles.SelectedIndex = 0;
        foreach (ListItem item in cblGrantees.Items)
        {
            item.Selected = false;
        }
        foreach (ListItem item in cblGrantees2013.Items)
        {
            item.Selected = false;
        }
        hfID.Value = "";
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        Session["GranteeUsers"] = null;
        ClearFields();
        mpeResourceWindow.Show();
    }
    protected void OnResetPassword(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(TextBox2.Text))
        {
            txtSearch.Text = "";
            MagnetGranteeUser item = MagnetGranteeUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
            MembershipUser user = Membership.GetUser(item.UserName, false);

            string tmpPassword = "";
               
            try
            {
                if (!user.IsLockedOut)
                {
                    tmpPassword = user.ResetPassword();
                    if (TextBox2.Text.Length < 6)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "Exception", "<script>alert('Exception: The length of parameter newPassword needs to be greater or equal to 6.');</script>", false);
                        mpePassword.Show();
                    }
                    else
                        user.ChangePassword(tmpPassword, TextBox2.Text);
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('User account is locked out.');</script>", false);
            }
            catch (Exception x)
            {

                //YafContext.Current.AddLoadMessage("Exception: " + x.Message);

            }			
        }
    }
    protected void OnEditPassword(object sender, EventArgs e)
    {
        //ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetGranteeUser item = MagnetGranteeUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        TextBox1.Text = item.UserName;
        mpePassword.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        Session["GranteeUsers"] = null;
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetGranteeUser item = MagnetGranteeUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtUserName.Text = item.UserName;
        //txtUserName.Enabled = false;
        txtPassword.Enabled = false;
        txtPassword.Text = "";

        MembershipUser user = Membership.GetUser(item.UserName, false);
        if (user != null)
        {
            txtEmail.Text = user.Email;
            ckLocked.Checked = user.IsLockedOut;
            string[] rolesArray = Roles.GetRolesForUser(item.UserName);
            if (rolesArray.Count() > 0)
                ddlRoles.SelectedValue = rolesArray[0];
        }

        foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == Convert.ToInt32(hfID.Value)))
        {
            int granteeCohort = (int)MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(district.GranteeID)).CohortType;
            if (granteeCohort == 1)
            {
                if (cblGrantees.Items.FindByValue(district.GranteeID.ToString()) != null)
                    cblGrantees.Items.FindByValue(district.GranteeID.ToString()).Selected = true;
            }
            else if (granteeCohort == 2)
            {
                if (cblGrantees2013.Items.FindByValue(district.GranteeID.ToString()) != null)
                    cblGrantees2013.Items.FindByValue(district.GranteeID.ToString()).Selected = true;
            }
        }

        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        string grandeeUser = "";
        txtSearch.Text = "";
        MagnetGranteeUser item = new MagnetGranteeUser();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetGranteeUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        grandeeUser = item.UserName;
        item.UserName = txtUserName.Text;
        item.Save();

        if (!txtPassword.Enabled)
        {
            //Existing user
            MembershipUser user = Membership.GetUser(grandeeUser, false);
            if (user != null)
            {
                aspusersDataContext db = new aspusersDataContext();
                aspnet_User aspUser = db.aspnet_Users.SingleOrDefault(x => x.UserName == user.UserName);
                aspUser.UserName = txtUserName.Text;
                aspUser.LoweredUserName = txtUserName.Text.ToLower();
                db.SubmitChanges();
                
                db.Dispose();

                user = Membership.GetUser(item.UserName, false);
                user.Email = txtEmail.Text;
                Membership.UpdateUser(user);
                if (!ckLocked.Checked) user.UnlockUser();
                string[] roles = Roles.GetRolesForUser(item.UserName);
                if(roles.Count()>0)
                    Roles.RemoveUserFromRoles(item.UserName, Roles.GetRolesForUser(item.UserName));
                if (ddlRoles.SelectedIndex > 0)
                    Roles.AddUserToRole(txtUserName.Text, ddlRoles.SelectedValue);
            }
        }
        else
        {
            //New user
            MembershipUser checkUser = Membership.GetUser(item.UserName, false);
            if (checkUser == null)
            {
                MembershipUser user = Membership.CreateUser(txtUserName.Text, txtPassword.Text, txtEmail.Text);
                //user.Email = txtEmail.Text;
                string tempPassword = user.ResetPassword();
                user.ChangePassword(tempPassword, txtPassword.Text);
                Membership.UpdateUser(user);
                if (ddlRoles.SelectedIndex > 0)
                    Roles.AddUserToRole(txtUserName.Text, ddlRoles.SelectedValue);
                else
                    Roles.AddUserToRole(txtUserName.Text, "ProjectDirector");
            }
        }
        saveGranteelist(cblGrantees, item);
        saveGranteelist(cblGrantees2013, item);
       
        if (Session["PageNumber"] == null)
            LoadData(0);
        else
            LoadData(Convert.ToInt32(Session["PageNumber"]));
    }



    private void saveGranteelist(CheckBoxList cblgranteelist, MagnetGranteeUser item)
    {
        //Grantee list
        int granteeid = 0;
        foreach (ListItem data in cblgranteelist.Items)
        {
            if (data.Selected)
            {
                if (MagnetGranteeUserDistrict.Find(x => x.GranteeID == Convert.ToInt32(data.Value) && x.GranteeUserID == item.ID).Count == 0)
                {
                    MagnetGranteeUserDistrict district = new MagnetGranteeUserDistrict();
                    district.GranteeUserID = item.ID;
                    district.GranteeID = Convert.ToInt32(data.Value);
                    district.Save();
                    granteeid = Convert.ToInt32(data.Value);
                }
            }
            else
            {
                foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeID == Convert.ToInt32(data.Value) && x.GranteeUserID == item.ID))
                {
                    district.Delete();
                }
            }
        }
        if (granteeid > 0)
        {
            item.GranteeID = granteeid;
            item.Save();
        }
    }
    class GranteeUser
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Roles { get; set; }
        public string Grantees { get; set; }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        List<GranteeUser> users = new List<GranteeUser>();
        if (Session["GranteeUsers"] == null)
        {
            foreach (var user in MagnetGranteeUser.All().OrderBy(x => x.UserName))
            {
                GranteeUser granteeUser = new GranteeUser();
                granteeUser.ID = user.ID;
                granteeUser.UserName = user.UserName;
                MembershipUser membershipUser = Membership.GetUser(user.UserName, false);
                if (membershipUser != null)
                    granteeUser.Email = membershipUser.Email;
                string roles = "";
                foreach (string role in Roles.GetRolesForUser(user.UserName))
                {
                    roles += role + " ";
                }
                string grantees = "";
                foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == user.ID))
                {
                    grantees += MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID).GranteeName + "<br/>";
                }
                granteeUser.Grantees = grantees;
                granteeUser.Roles = roles;
                users.Add(granteeUser);
            }
        }
        else
        {
            users = null;
            users = (List<GranteeUser>)Session["GranteeUsers"];
        }

        List<GranteeUser> founduser = new List<GranteeUser>();
        if (users != null)
        {
            if (string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                GridView1.DataSource = users;
            }
            else
            {

                var tmp = users.Find(x => x.UserName == txtSearch.Text.Trim());
                if (tmp != null )
                    founduser.Add(tmp);
                GridView1.DataSource = founduser;
            }

        }
        else
            GridView1.DataSource = founduser;

        GridView1.DataBind();
    }

    //protected void search_Click(object sender, EventArgs e)
    //{
    //    List<GranteeUser> users = new List<GranteeUser>();
    //    if (Session["GranteeUsers"] == null)
    //    {
    //        var GranteeUserlist = from drwg in db.MagnetGrantees
    //                              join drwu in db.MagnetGranteeUsers
    //                              on drwg.ID equals drwu.GranteeID
    //                              where
    //                              //drwg.CohortType == Convert.ToInt32(rbtnlstCohort.SelectedValue) &&
    //                               drwg.isActive == true
    //                              orderby drwu.UserName
    //                              select drwu;

    //        foreach (var user in GranteeUserlist)
    //        {
    //            GranteeUser granteeUser = new GranteeUser();
    //            granteeUser.ID = user.ID;
    //            granteeUser.UserName = user.UserName;
    //            MembershipUser membershipUser = Membership.GetUser(user.UserName, false);
    //            if (membershipUser != null)
    //                granteeUser.Email = membershipUser.Email;
    //            string roles = "";
    //            foreach (string role in Roles.GetRolesForUser(user.UserName))
    //            {
    //                roles += role + " ";
    //            }
    //            string grantees = "";
    //            foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == user.ID))
    //            {
    //                grantees += MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID).GranteeName + "<br/>";
    //            }
    //            granteeUser.Grantees = grantees;
    //            granteeUser.Roles = roles;
    //            users.Add(granteeUser);
    //        }
    //    }
    //    else
    //    {
    //        users = null;
    //        users = (List<GranteeUser>)Session["GranteeUsers"];
    //    }

    //    List<GranteeUser> founduser = new List<GranteeUser>();
    //    if (users != null)
    //    {
    //        if (string.IsNullOrEmpty(txtSearch.Text.Trim()))
    //        {
    //            GridView1.DataSource = users;
    //        }
    //        else
    //        {

    //            var tmp = users.SingleOrDefault(x => x.UserName == txtSearch.Text.Trim());
    //            if (tmp != null)
    //                founduser.Add(tmp);
    //                GridView1.DataSource = founduser;
    //        }
            
    //    }
    //    else
    //        GridView1.DataSource = founduser;
        
    //        GridView1.DataBind();
    //}
    protected void Clear_Click(object sender, EventArgs e)
    {
        txtSearch.Text = "";
    }
    protected void txtUserName_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtUserName.Text))
        {
            var users = Membership.FindUsersByName(txtUserName.Text.Trim());
            if (users == null)
            {
                lblUserValidation.Text = "Username is available";
                lblUserValidation.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblUserValidation.Text = "Username is not available";
                lblUserValidation.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}