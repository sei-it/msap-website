﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_ManageGC : System.Web.UI.Page
{
    GCDataClassesDataContext lsqldb = new GCDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }

        if (ddlGranteeCorner.SelectedValue == "" || ddlGranteeCorner.SelectedValue == "1")
        {
           // pnlGCImages.Visible = false;
            hfGCID.Value = "";
        }
        else
        {
            hfGCID.Value = ddlGranteeCorner.SelectedValue.Trim();
            pnlGCImages.Visible = true;
            LoadImgData(0);
        }
    }

    private void LoadImgData(int PageNumber)
    {
        string cohort = rbtnlstCohort.SelectedValue;
        var data = from gc in lsqldb.MagnetGCImages
                   from cnr in lsqldb.MagnetGCs 
                   where cnr.id == gc.gc_id && cnr.Cohort==cohort && gc.gc_id == Convert.ToInt32(ddlGranteeCorner.SelectedValue)
                   orderby gc.displayOrder
                   select gc;
        gvImage.DataSource = data;
        gvImage.PageIndex = PageNumber;
        gvImage.DataBind();
    }

    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = new object();
        if (rbtnlstCohort.SelectedValue == "2010")
        {
           data = (from m in db.MagnetGCs
                    where  m.Cohort=="2010" && m.id != 1 
                    orderby m.id descending
                    select new { m.id, m.Title, m.Createdby, m.CreatedOn, m.isActive }).ToList();
          
        }
        else
        {
           data = (from m in db.MagnetGCs
                    where m.Cohort == "2013" &&  m.id != 1 
                    orderby m.id descending
                    select new { m.id, m.Title, m.Createdby, m.CreatedOn, m.isActive }).ToList();

        }

        gdvGC.DataSource = data;
        gdvGC.PageIndex = PageNumber;
        gdvGC.DataBind();
    }


    protected void OnAddGC(object sender, EventArgs e)
    {
        var gcitem = from rd in lsqldb.MagnetGCs
                     where rd.isActive
                     select rd;

        ClearGCFields();
        hfContentID.Value = "";
        if (gcitem != null && gcitem.Count() > 0)
        {
            rblActive.Enabled = false;
            rblActive.SelectedIndex = 1;
        }
        else
            rblActive.Enabled = true;
        mpeContentWindow.Show();
    }
    protected void OnEditGC(object sender, EventArgs e)
    {
        var gcitem = from rd in lsqldb.MagnetGCs
                     where rd.isActive
                     select rd;

        hfContentID.Value = (sender as LinkButton).CommandArgument;

        var gcCheck = from rw in lsqldb.MagnetGCs
                      where rw.isActive
                      select rw;

        var gc = lsqldb.MagnetGCs.SingleOrDefault(x => x.id == Convert.ToInt32(hfContentID.Value));
        txtTitle.Text = gc.Title;
        edtAbsContent.Content = gc.abs_contents;
        edtContents.Content = gc.Contents;
        rblActive.SelectedIndex = (bool)gc.isActive ? 0 : 1;
        if (gcCheck != null && gcCheck.Count() > 0 && gc.isActive ==false)
        {
            rblActive.Enabled = false;
            rblActive.SelectedIndex = 1;

        }
        else
            rblActive.Enabled = true;
        mpeContentWindow.Show();
    }
    protected void OnDeleteGC(object sender, EventArgs e)
    {
        //MagnetNews.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        //LoadData(GridView1.PageIndex);
        var rws = from gcr in lsqldb.MagnetGCs
                 where gcr.id == Convert.ToInt32((sender as LinkButton).CommandArgument)
                 select gcr;
        foreach(var rw in rws)
            lsqldb.MagnetGCs.DeleteOnSubmit(rw);
        lsqldb.SubmitChanges();
        LoadData(gdvGC.PageIndex);

    }
    protected void OnEditImage(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        int gcid = 0;

        var item = lsqldb.MagnetGCImages.SingleOrDefault(x => x.id == id);

        if (item != null)
        {
            gcid = Convert.ToInt32(item.gc_id);
        }
        var gchomepageimg = from rd in lsqldb.MagnetGCImages
                            where rd.isShowOnHomePage && rd.gc_id==gcid
                            select rd;
        var gcArchimg = from rd in lsqldb.MagnetGCImages
                        where rd.isArchiveImage && rd.gc_id == gcid
                        select rd;

    

        hfImgID.Value = (sender as LinkButton).CommandArgument;
        var data = lsqldb.MagnetGCImages.Single(x => x.id == Convert.ToInt32(hfImgID.Value.Trim()));

        if (gchomepageimg != null && gchomepageimg.Count() > 0 && data.isShowOnHomePage==false)
        {
            rbtnlDisplayonHomepage.Enabled = false;
            rbtnlDisplayonHomepage.SelectedIndex = 1;
        }
        else
            rbtnlDisplayonHomepage.Enabled = true;

        if (gcArchimg != null && gcArchimg.Count() > 0 && data.isArchiveImage==false)
        {
            rbtnlArchImg.Enabled = false;
            rbtnlArchImg.SelectedIndex = 1;
        }
        else
            rbtnlArchImg.Enabled = true;

        txtCaption.Text = data.caption;
        rbtnlArchImg.SelectedIndex = (bool)data.isArchiveImage ? 0 : 1;
        rbtnlDisplayonHomepage.SelectedIndex = (bool)data.isShowOnHomePage ? 0 : 1;
        txtAltText.Text = data.alt_text;
        txtOrder.Text = data.displayOrder.ToString();
        mpextImage.Show();
    }
    protected void OnDeleteImage(object sender, EventArgs e)
    {
        var rws = from gcr in lsqldb.MagnetGCImages
                  where gcr.id == Convert.ToInt32((sender as LinkButton).CommandArgument)
                  select gcr;
        foreach (var rw in rws)
        {
            lsqldb.MagnetGCImages.DeleteOnSubmit(rw);
            lsqldb.SubmitChanges();
        }
        LoadImgData(gvImage.PageIndex);
    }
    protected void OnSaveGC(object sender, EventArgs e)
    {
        if (hfContentID.Value != "") //edit mode
        {
            MagnetGC gc = lsqldb.MagnetGCs.SingleOrDefault(x => x.id == Convert.ToInt32(hfContentID.Value.Trim()));
            gc.Title = txtTitle.Text.Trim();
            gc.abs_contents = edtAbsContent.Content;
            gc.Contents = edtContents.Content;
            gc.Createdby = HttpContext.Current.User.Identity.Name;
            gc.CreatedOn = DateTime.Now;
            gc.isActive = rblActive.SelectedIndex == 1 ? false : true;
            gc.Cohort = rbtnlstCohort.SelectedValue;
            lsqldb.SubmitChanges();
        }
        else
        {
            MagnetGC gc = new MagnetGC();
            gc.Title = txtTitle.Text.Trim();
            gc.abs_contents = edtAbsContent.Content;
            gc.Contents = edtContents.Content;
            gc.Createdby = HttpContext.Current.User.Identity.Name;
            gc.CreatedOn = DateTime.Now;
            gc.isActive = rblActive.SelectedIndex == 1 ? false : true;
            lsqldb.MagnetGCs.InsertOnSubmit(gc);
            gc.Cohort = rbtnlstCohort.SelectedValue;
            lsqldb.SubmitChanges();
        }
        //LoadData(gdvGC.PageIndex);
        Response.Redirect("ManageGC.aspx");
    }

    protected void OnSaveImg(object sender, EventArgs e)
    { 
        int len = fuploadImg.PostedFile.ContentLength;
        if(string.IsNullOrEmpty(hfImgID.Value))
        {
           
            MagnetGCImage gcimg = new MagnetGCImage();

            if(len>0)
            {
                byte[] pic = new byte[len];
                fuploadImg.PostedFile.InputStream.Read(pic, 0, len);
                gcimg.image = pic;
            }
            gcimg.isArchiveImage = rbtnlArchImg.SelectedIndex == 1 ?  false : true;
            gcimg.isShowOnHomePage = rbtnlDisplayonHomepage.SelectedIndex == 1 ? false : true;
            gcimg.caption = txtCaption.Text.Trim();
            gcimg.alt_text = txtAltText.Text.Trim();
            gcimg.CreateOn = DateTime.Now;
            gcimg.CreatedBy = HttpContext.Current.User.Identity.Name;
            gcimg.gc_id = Convert.ToInt32(ddlGranteeCorner.SelectedValue.Trim());
            gcimg.displayOrder = Convert.ToInt32(txtOrder.Text.Trim());
            lsqldb.MagnetGCImages.InsertOnSubmit(gcimg);
            lsqldb.SubmitChanges();
        }
        else
        {
            var gcimg = lsqldb.MagnetGCImages.SingleOrDefault(x => x.id == Convert.ToInt32(hfImgID.Value.Trim()));
            if (len > 0)
            {
                byte[] pic = new byte[len];
                fuploadImg.PostedFile.InputStream.Read(pic, 0, len);
                gcimg.image = pic;
            }
            gcimg.isArchiveImage = rbtnlArchImg.SelectedIndex == 1 ? false : true;
            gcimg.isShowOnHomePage = rbtnlDisplayonHomepage.SelectedIndex == 1 ? false : true;
            gcimg.caption = txtCaption.Text.Trim();
            gcimg.alt_text = txtAltText.Text.Trim();
            gcimg.gc_id = Convert.ToInt32(ddlGranteeCorner.SelectedValue.Trim());
            gcimg.CreateOn = DateTime.Now;
            gcimg.CreatedBy = HttpContext.Current.User.Identity.Name;
            gcimg.displayOrder = Convert.ToInt32(txtOrder.Text.Trim());
            lsqldb.SubmitChanges();
        }
        LoadImgData(gvImage.PageIndex);
    }

    private void ClearGCFields()
    {
        txtTitle.Text = "";
        edtAbsContent.Content = "";
        edtContents.Content = "";
        //rblActive.SelectedIndex = 0;

    }

    private void ClearImgFields()
    {
        txtCaption.Text = "";
        txtAltText.Text = "";
    }
    protected void btnNewImage_Click(object sender, EventArgs e)
    {
        ClearImgFields();
        hfImgID.Value = "";
        var gchomepageimg = from rd in lsqldb.MagnetGCImages
                            where rd.isShowOnHomePage && rd.gc_id == Convert.ToInt32(hfGCID.Value)
                            select rd;
        var gcArchimg = from rd in lsqldb.MagnetGCImages
                        where rd.isArchiveImage && rd.gc_id == Convert.ToInt32(hfGCID.Value)
                        select rd;

        if (gchomepageimg != null && gchomepageimg.Count() > 0)
        {
            rbtnlDisplayonHomepage.Enabled = false;
            rbtnlDisplayonHomepage.SelectedIndex = 1;
        }
        else
            rbtnlDisplayonHomepage.Enabled = true;

        if (gcArchimg != null && gcArchimg.Count() > 0)
        {
            rbtnlArchImg.Enabled = false;
            rbtnlArchImg.SelectedIndex = 1;
        }
        else
            rbtnlArchImg.Enabled = true;
        mpextImage.Show();
    }
    protected void rbtnlstCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadData(0);
    }
}