﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using ExcelLibrary.SpreadSheet;

public partial class admin_managesurvey : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }
 
    protected void OnDownload(object sender, EventArgs e)
    {
        using (System.IO.MemoryStream output = new System.IO.MemoryStream())
        {
            Workbook workbook = new Workbook();
            Worksheet worksheet = new Worksheet("Email");
            int idx = 0;

            MagnetDBDB db = new MagnetDBDB();
            var data = from m in db.MagnetGrantees
                       from n in db.MagnetSchools
                       from l in db.MagnetGranteeContacts
                       from o in db.MagnetSurveyUsers
                       from p in db.MagnetSurveys
                       where o.GranteeID == m.ID
                       && o.SchoolID == n.ID
                       && o.PrincipalID == l.ID
                       && p.SurveyUserID == o.ID
                       && p.Submitted == true
                       select new { m.GranteeName, n.SchoolName, l.ContactFirstName, l.ContactLastName, l.Email, o.Passcode, p};
            foreach (var SurveyData in data)
            {
                worksheet.Cells[idx, 0] = new Cell(SurveyData.GranteeName);
                worksheet.Cells[idx, 1] = new Cell(SurveyData.SchoolName);
                worksheet.Cells[idx, 2] = new Cell(SurveyData.ContactFirstName);
                worksheet.Cells[idx, 3] = new Cell(SurveyData.ContactLastName);
                worksheet.Cells[idx, 4] = new Cell(SurveyData.Email);
                worksheet.Cells[idx, 5] = new Cell(SurveyData.Passcode);

                worksheet.Cells[idx, 6] = new Cell(SurveyData.p.Question1);
                worksheet.Cells[idx, 7] = new Cell(SurveyData.p.Question2);
                worksheet.Cells[idx, 8] = new Cell(SurveyData.p.Question3);
                worksheet.Cells[idx, 9] = new Cell(SurveyData.p.Question4a1);
                worksheet.Cells[idx, 10] = new Cell(SurveyData.p.Question4a2);
                worksheet.Cells[idx, 11] = new Cell(SurveyData.p.Question4b1);
                worksheet.Cells[idx, 12] = new Cell(SurveyData.p.Question4b2);
                worksheet.Cells[idx, 13] = new Cell(SurveyData.p.Question4c1);
                worksheet.Cells[idx, 14] = new Cell(SurveyData.p.Question4c2);
                worksheet.Cells[idx, 15] = new Cell(SurveyData.p.Question4d1);
                worksheet.Cells[idx, 16] = new Cell(SurveyData.p.Question4d2);
                worksheet.Cells[idx, 17] = new Cell(SurveyData.p.Question4e1);
                worksheet.Cells[idx, 18] = new Cell(SurveyData.p.Question4e2);
                worksheet.Cells[idx, 19] = new Cell(SurveyData.p.Question4f1);
                worksheet.Cells[idx, 20] = new Cell(SurveyData.p.Question4f2);
                worksheet.Cells[idx, 21] = new Cell(SurveyData.p.Question4g1);
                worksheet.Cells[idx, 22] = new Cell(SurveyData.p.Question4g2);
                worksheet.Cells[idx, 23] = new Cell(SurveyData.p.Question4h1);
                worksheet.Cells[idx, 24] = new Cell(SurveyData.p.Question4h2);
                worksheet.Cells[idx, 25] = new Cell(SurveyData.p.Question4i1);
                worksheet.Cells[idx, 26] = new Cell(SurveyData.p.Question4i2);
                worksheet.Cells[idx, 27] = new Cell(SurveyData.p.Question4j1);
                worksheet.Cells[idx, 28] = new Cell(SurveyData.p.Question4j2);
                worksheet.Cells[idx, 29] = new Cell(SurveyData.p.Question4k1);
                worksheet.Cells[idx, 30] = new Cell(SurveyData.p.Question4k2);
                worksheet.Cells[idx, 31] = new Cell(SurveyData.p.Question4l1);
                worksheet.Cells[idx, 32] = new Cell(SurveyData.p.Question4l2);
                worksheet.Cells[idx, 33] = new Cell(SurveyData.p.Question4m1);
                worksheet.Cells[idx, 34] = new Cell(SurveyData.p.Question4m2);
                worksheet.Cells[idx, 35] = new Cell(SurveyData.p.Question4n1);
                worksheet.Cells[idx, 36] = new Cell(SurveyData.p.Question4n2);
                worksheet.Cells[idx, 37] = new Cell(SurveyData.p.Question4o1);
                worksheet.Cells[idx, 38] = new Cell(SurveyData.p.Question4o2);
                worksheet.Cells[idx, 39] = new Cell(SurveyData.p.Question4p1);
                worksheet.Cells[idx, 40] = new Cell(SurveyData.p.Question4p2);
                worksheet.Cells[idx, 41] = new Cell(SurveyData.p.Question4q1);
                worksheet.Cells[idx, 42] = new Cell(SurveyData.p.Question4q2);
                worksheet.Cells[idx, 43] = new Cell(SurveyData.p.Question4r1);
                worksheet.Cells[idx, 44] = new Cell(SurveyData.p.Question4r2);
                worksheet.Cells[idx, 45] = new Cell(SurveyData.p.Question4s1);
                worksheet.Cells[idx, 46] = new Cell(SurveyData.p.Question4s2);
                worksheet.Cells[idx, 47] = new Cell(SurveyData.p.Question4t1);
                worksheet.Cells[idx, 48] = new Cell(SurveyData.p.Question4t2);
                worksheet.Cells[idx, 49] = new Cell(SurveyData.p.Question4u1);
                worksheet.Cells[idx, 50] = new Cell(SurveyData.p.Question4u2);
                worksheet.Cells[idx, 51] = new Cell(SurveyData.p.Question4v1);
                worksheet.Cells[idx, 52] = new Cell(SurveyData.p.Question4v2);
                worksheet.Cells[idx, 53] = new Cell(SurveyData.p.Question4w1);
                worksheet.Cells[idx, 54] = new Cell(SurveyData.p.Question4w2);
                worksheet.Cells[idx, 55] = new Cell(SurveyData.p.Question4x1);
                worksheet.Cells[idx, 56] = new Cell(SurveyData.p.Question4x2);
                worksheet.Cells[idx, 57] = new Cell(SurveyData.p.Question4y1);
                worksheet.Cells[idx, 58] = new Cell(SurveyData.p.Question4y2);
                worksheet.Cells[idx, 59] = new Cell(SurveyData.p.Question4z1);
                worksheet.Cells[idx, 60] = new Cell(SurveyData.p.Question4z2);
                worksheet.Cells[idx, 61] = new Cell(SurveyData.p.Question4aa1);
                worksheet.Cells[idx, 62] = new Cell(SurveyData.p.Question4aa2);
                worksheet.Cells[idx, 63] = new Cell(SurveyData.p.Question4bb1);
                worksheet.Cells[idx, 64] = new Cell(SurveyData.p.Question4bb2);
                worksheet.Cells[idx, 65] = new Cell(SurveyData.p.Question4cc1);
                worksheet.Cells[idx, 66] = new Cell(SurveyData.p.Question4cc2);
                worksheet.Cells[idx, 67] = new Cell(SurveyData.p.Question4dd1);
                worksheet.Cells[idx, 68] = new Cell(SurveyData.p.Question4dd2);
                worksheet.Cells[idx, 69] = new Cell(SurveyData.p.Question4ee1);
                worksheet.Cells[idx, 70] = new Cell(SurveyData.p.Question4ee2);
                worksheet.Cells[idx, 71] = new Cell(SurveyData.p.Question4ff1);
                worksheet.Cells[idx, 72] = new Cell(SurveyData.p.Question4ff2);
                worksheet.Cells[idx, 73] = new Cell(SurveyData.p.Question4gg1);
                worksheet.Cells[idx, 74] = new Cell(SurveyData.p.Question4gg2);
                worksheet.Cells[idx, 75] = new Cell(SurveyData.p.Question4hh1);
                worksheet.Cells[idx, 76] = new Cell(SurveyData.p.Question4hh2);
                worksheet.Cells[idx, 77] = new Cell(SurveyData.p.Question4ii1);
                worksheet.Cells[idx, 78] = new Cell(SurveyData.p.Question4ii2);
                worksheet.Cells[idx, 79] = new Cell(SurveyData.p.Question5a1);
                worksheet.Cells[idx, 80] = new Cell(SurveyData.p.Question5a2);
                worksheet.Cells[idx, 81] = new Cell(SurveyData.p.Question5b1);
                worksheet.Cells[idx, 82] = new Cell(SurveyData.p.Question5b2);
                worksheet.Cells[idx, 83] = new Cell(SurveyData.p.Question5c1);
                worksheet.Cells[idx, 84] = new Cell(SurveyData.p.Question5c2);
                worksheet.Cells[idx, 85] = new Cell(SurveyData.p.Question5d1);
                worksheet.Cells[idx, 86] = new Cell(SurveyData.p.Question5d2);
                worksheet.Cells[idx, 87] = new Cell(SurveyData.p.Question6a1);
                worksheet.Cells[idx, 88] = new Cell(SurveyData.p.Question6a2);
                worksheet.Cells[idx, 89] = new Cell(SurveyData.p.Question6b1);
                worksheet.Cells[idx, 90] = new Cell(SurveyData.p.Question6b2);
                worksheet.Cells[idx, 91] = new Cell(SurveyData.p.Question6c1);
                worksheet.Cells[idx, 92] = new Cell(SurveyData.p.Question6c2);
                worksheet.Cells[idx, 93] = new Cell(SurveyData.p.Question6d1);
                worksheet.Cells[idx, 94] = new Cell(SurveyData.p.Question6d2);
                worksheet.Cells[idx, 95] = new Cell(SurveyData.p.Question6e1);
                worksheet.Cells[idx, 96] = new Cell(SurveyData.p.Question6e2);
                worksheet.Cells[idx, 97] = new Cell(SurveyData.p.Question6f1);
                worksheet.Cells[idx, 98] = new Cell(SurveyData.p.Question6f2);
                worksheet.Cells[idx, 99] = new Cell(SurveyData.p.Question6g1);
                worksheet.Cells[idx, 100] = new Cell(SurveyData.p.Question6g2);
                worksheet.Cells[idx, 101] = new Cell(SurveyData.p.Question6h1);
                worksheet.Cells[idx, 102] = new Cell(SurveyData.p.Question6h2);

                worksheet.Cells[idx, 103] = new Cell(SurveyData.p.Question7a);
                worksheet.Cells[idx, 104] = new Cell(SurveyData.p.Question7b);
                worksheet.Cells[idx, 105] = new Cell(SurveyData.p.Question7c);
                worksheet.Cells[idx, 106] = new Cell(SurveyData.p.Question8);
                worksheet.Cells[idx, 107] = new Cell(SurveyData.p.Question8Other1);
                worksheet.Cells[idx, 108] = new Cell(SurveyData.p.Question8Other2);

                idx++;
            }
            workbook.Worksheets.Add(worksheet);
            workbook.Save(output);
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=surveydata.xls");
            Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            output.Close();
        }
    }
}