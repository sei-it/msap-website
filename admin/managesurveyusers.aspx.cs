﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class managesurveyusers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (MagnetGrantee grantee in MagnetGrantee.All())
            {
                ddlGrantee.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
            }
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetSurveyUser.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(0);
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = (from m in db.MagnetSurveyUsers
                   from n in db.MagnetGrantees
                   from l in db.MagnetSchools
                    from p in db.MagnetGranteeContacts
                   where m.GranteeID == n.ID
                   && m.SchoolID == l.ID
                   && m.PrincipalID == p.ID
                   select new { m.ID, n.GranteeName, l.SchoolName, UserName=p.ContactFirstName + " " + p.ContactLastName, m.Passcode}).ToList();

        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtPasscode.Text = "";
        ddlGrantee.SelectedIndex = 0;
        ddlSchool.Items.Clear();
        ddlSchool.Items.Add(new ListItem("Please select", ""));
        ddlPrincipal.Items.Clear();
        ddlPrincipal.Items.Add(new ListItem("Please select", ""));
        ddlSchool.Enabled = false;
        ddlPrincipal.Enabled = false;
        hfID.Value = "";
    }
    protected void OnGranteeChanged(object sender, EventArgs e)
    {
        if (ddlGrantee.SelectedIndex > 0)
        {
            LoadSchool();
        }
        mpeResourceWindow.Show();
    }
    private void LoadSchool()
    {
        ddlSchool.Items.Clear();
        ddlSchool.Items.Add(new ListItem("Please select", ""));
        foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == Convert.ToInt32(ddlGrantee.SelectedValue)))
        {
            ddlSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
        }
        ddlSchool.SelectedIndex = 0;
        ddlSchool.Enabled = true;
    }
    protected void OnSchoolChanged(object sender, EventArgs e)
    {
        if (ddlSchool.SelectedIndex > 0)
        {
            LoadPrincipal();
        }
        mpeResourceWindow.Show();
    }
    private void LoadPrincipal()
    {
        ddlPrincipal.Items.Clear();
        ddlPrincipal.Items.Add(new ListItem("Please select", ""));
        foreach (MagnetGranteeContact principal in MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(ddlSchool.SelectedValue) && x.ContactType == 6))
        {
            ddlPrincipal.Items.Add(new ListItem(principal.ContactFirstName + " " + principal.ContactLastName, principal.ID.ToString()));
        }
        ddlPrincipal.SelectedIndex = 0;
        ddlPrincipal.Enabled = true;
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        //Generate passcode
        string passcode = ManageUtility.RandomNumbers(5);
        while(MagnetSurveyUser.Find(x=>x.Passcode.Equals(passcode)).Count>0)
            passcode = ManageUtility.RandomNumbers(5);
        txtPasscode.Text = passcode;
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetSurveyUser item = MagnetSurveyUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtPasscode.Text = item.Passcode;
        if (item.GranteeID != null)
        {
            ddlGrantee.SelectedValue = Convert.ToString(item.GranteeID);
            LoadSchool();
            ddlSchool.Enabled = true;
            if (item.SchoolID != null)
            {
                ddlSchool.SelectedValue = Convert.ToString(item.SchoolID);
                LoadPrincipal();
                ddlPrincipal.Enabled = true;
                if (item.PrincipalID != null)
                    ddlPrincipal.SelectedValue = Convert.ToString(item.PrincipalID);
            }
        }
        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetSurveyUser item = new MagnetSurveyUser();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetSurveyUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        if (ddlGrantee.SelectedIndex > 0) item.GranteeID = Convert.ToInt32(ddlGrantee.SelectedValue);
        if (ddlSchool.SelectedIndex > 0) item.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        if (ddlPrincipal.SelectedIndex > 0) item.PrincipalID = Convert.ToInt32(ddlPrincipal.SelectedValue);
        item.Passcode = txtPasscode.Text;
        item.Save();
        LoadData(0);
        mpeResourceWindow.Show();
    }
}