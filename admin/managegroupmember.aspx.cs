﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class managegroupmember : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
       MagnetGroupMember.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(0);
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        GridView1.DataSource = MagnetGroupMember.All().OrderBy(x => x.MemberName).ToList();
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtUserName.Text = "";
        txtTitle.Text = "";
        txtEmail.Text = "";
        txtSequence.Text = "";
        hfID.Value = "";
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetGroupMember item = MagnetGroupMember.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtUserName.Text = item.MemberName;
        txtTitle.Text = item.Title;
        txtEmail.Text = item.Email;
        txtSequence.Text = Convert.ToString(item.Priority);
        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetGroupMember item = new MagnetGroupMember();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetGroupMember.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        item.MemberName = txtUserName.Text;
        item.Title = txtTitle.Text;
        item.Email = txtEmail.Text;
        item.Priority = !string.IsNullOrEmpty(txtSequence.Text)?Convert.ToInt32(txtSequence.Text):0;
        item.Save();
        LoadData(0);
    }
}