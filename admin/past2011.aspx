<%@ Page Title="MSAP Past Conferences" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">Past MSAP Conferences - 2011
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="border:none;">Past Conferences</h1>
 	<div class="titles">2011 MSAP Project Directors Meeting, December 8-9, 2011</div>
    <div class="tab_area" style="width:840px;">
    		<ul class="tabs">
                <div runat="server" style="width: 600px; float: right;">
                <li><a href="past.aspx">2010</a></li> 
                <li class="tab_active">2011</li>
                <li><a href="past2012.aspx">2012</a></li>
                <li><a href="past2012.aspx">2013</a></li>
                
                </div>
        	</ul>
    	</div>
    	<br/>
      
	<div style="float: left; margin-top: 0; width: 20%;;padding-bottom:15px;">
            <p>
            <strong>Meeting Agenda</strong>
            <br />
            <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                    vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/Agenda_2011.pdf" target="_blank">Download
                        PDF</a> <small>(232 KB)</small>
  		 </p>
         <p>
        	<strong>Meeting Participant List</strong>
			<br />
            <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/ParticipantList.pdf" target="_blank">Download
                    PDF</a> <small>(284 KB)</small>
        </p>
    </div>
	<div style="float: left; margin-top: 0; width: 75%;;padding-bottom:15px;border-left:1px solid #A9DEF0;padding-left:20px;">
     <h3 class="pastconf_subsection" style="padding-top:0px;">Day 1: December 8, 2011</h3>

     <p>   
        <strong class="pastconf_subtitle">Grants Management and Annual Performance Reporting</strong><br />
            
            <span class="pastconf_author"><i>Anna Hinton, Ph.D.; Rosie Kelley; Brittany Beth; Tyrone Harris</i><br />
            U.S. Department of Education</span>
       
     </p>
     <p>
        ED staff responded to common budget questions, such as which costs are and are not allowable. They then discussed the Annual Performance Report, referring to areas that may cause confusion and clearly explaining how to complete the table for the Government Performance and Results Act (GPRA) measures. 
	</p>
	<div id="Div3" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Grants Management Presentation
                </td>
                <td width='20%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/ED_GrantsManagement.pdf" target="_blank">Download
                    PDF</a> <small>(1.5 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">MSAP Compliance Monitoring</strong><br />
            <span class="pastconf_author"><i>Sara Allender, Seewan Eng</i><br />
            WestEd</span>
       
     </p>
     <p>
        This two-part session had three goals: (1) to introduce the purpose and process of project monitoring, (2) to demystify and clarify misconceptions about monitoring, and (3) to help project directors prepare for site visits. ED staff responded to common budget questions, such as which costs are and are not allowable. They then discussed the Annual Performance Report, referring to areas that may cause confusion and clearly explaining how to complete the table for the Government Performance and Results Act (GPRA) measures. 
	</p>
	<div id="Div4" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Compliance Monitoring Presentation.
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/ComplianceMonitoring.pdf" target="_blank">Download
                    PDF</a> <small>(2.8 MB)</small>
                </td>
            </tr>
            <tr>
                <td>
                    Compliance Monitoring Indicators.
                </td>
                <td>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/MonitoringIndicators.pdf" target="_blank">Download
                    PDF</a> <small>(492 KB)</small>
                </td>
            </tr>
        </table>
    </div>
   	<h3 class="pastconf_subsection">Day 2: December 9, 2012</h3>
    <p>
    	<strong class="pastconf_subtitle">Building Civic Capacity: New Imperatives, New Policies</strong><br /> 
       <span class="pastconf_author"><i>Claire Smrekar, Ph.D.</i><br/>
        Peabody College at Vanderbilt University</span>
       
     </p>
     <p>
        This session discusses the income gap and its relation to the achievement gap, then explains how magnet schools can play a role in closing the achievement gap. The presentation also covers the importance of entire neighborhood mobilization to improving education equality. 
    </p>
    <div id="Div5" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Building Civic Capacity: New Imperatives, New Policies.
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/Smrekar_CivicCapacity.pdf" target="_blank">Download
                    PDF</a> <small>(578 KB)</small>
                </td>
            </tr>
            <tr>
                <td>
                    Rethinking Magnet School Policies and Practices: A Response to Declining Diversity &amp; Judicial Constraints.
                </td>
                <td>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px;margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/SmrekarMagnetSchools.pdf" target="_blank">Download
                    PDF</a> <small>(85.45 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
    	<strong class="pastconf_subtitle">Housing and School Integration Panel: Implications for Magnet Schools</strong><br />
        	<span class="pastconf_author"><i>Luke Tate, Moderator</i><br />
            U.S. Department of Housing and Urban Development</span>
        
    </p>

    <div id="Div6" runat="server" class="indent">
          <strong>Fulfilling the Promise of Brown: Promoting School Integration</strong><br/>
        		<span class="pastconf_author"><i>Damon Hewitt.</i><br/>
                NAACP Legal Defense and Educational Fund, Inc</span>
        <p>
        This presentation introduced and explained the legal history around desegregation as it applies to magnet schools.</p>
    
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                   Promoting School Integration Presentation
                </td>
                <td>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/Hewitt_FulfillingthePromise.pdf" target="_blank">Download
                    PDF</a> <small>(381 KB)</small>
                </td>
            </tr>
        </table>
        <p>
      <strong>A Synthesis of Social Science Research About the School-Housing Integration Nexus: Implications for Magnet Schools.</strong><br/>
      <span class="pastconf_author"><i>Roslyn Mickelson, Ph.D.</i><br/>
      University of North Carolina at Charlotte</span>
      </p>
      <p>This presentation reported on education research on racial and socioeconomic diversity, the reciprocal connection between diverse neighborhoods and integrated schools, and parent involvement in community schools. </p>
    
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                   School-Housing Integration Presentation 
                </td>
                <td>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/Mickelson_SchoolHousingNexus.pdf" target="_blank">Download PDF</a> <small>(1.1 MB)</small>
                </td>
            </tr>
        </table>
	<p>
      <strong>The Learning Corridor Project: Economic Development and Urban Renewal.</strong><br/>
          <span class="pastconf_author"><i>Denise Gallucci</i><br/>
          Capitol Region Education Council</span>
     </p>
      <p>Starting with an initiative that established interdistrict magnet schools in Hartford, Connecticut, community, higher education, and government organizations have revitalized a deprived area and opened the door to higher education for many students. </p>
         
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                   Economic Development and Urban Renewal Presentation 
                </td>
                <td>
                     <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/Gallucci_LearningCorridor.pdf" target="_blank">Download
                    PDF</a> <small>(647 KB)</small>
                </td>
            </tr>
        </table>
    
    </div>
    <p>
        <strong class="pastconf_subtitle">
        	Vibrant and Diverse Magnet Schools: Strategies for Financing and Family Outreach</strong><br />
            <span class="pastconf_author"><i>Laura Martinez, Rachel Scott</i><br />
            The Finance Project</span>
        
	</p>
    <p>
        In this session, participants learned about eight key elements of sustainability and discussed strategies, such as needs assessments and identity building, for implementing them. 
	</p>
	<div id="Div7" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                   Strategies for Financing and Family Outreach Presentation
                </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2011_Events/FinanceProject.pdf" target="_blank">Download PDF</a> <small>(426 KB)</small>
                </td>
            </tr>
        </table>
    </div>
   </div>
<!--<div style="text-align:right;clear:both;width:820px;padding-right:20px;" class="space_top">
	2011 Meeting&nbsp;&nbsp;
	<a href="past.aspx"><strong>2010 Meeting</strong></a>
</div>
-->
</asp:Content>
