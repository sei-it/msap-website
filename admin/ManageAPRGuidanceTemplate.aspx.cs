﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Telerik.Web.UI;

public partial class admin_ManageAPRGuidanceTemplate : System.Web.UI.Page
{
    public APRGuidanceDataClassesDataContext db = new APRGuidanceDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        CustomContentTemplate template = new CustomContentTemplate();

        IQueryable<APRGuidanceCategory> pwcategories;
        if (HttpContext.Current.User.IsInRole("Administrators") || HttpContext.Current.User.IsInRole("Data"))
        {
            pwcategories = from c in db.APRGuidanceCategories where c.id != 1 && c.isActive == true orderby c.category select c;
        }
        else
        {
            pwcategories = from c in db.APRGuidanceCategories where c.isActive == true && c.id != 1 orderby c.category select c;
        }

        short TabIndexholder = 0;

        if (!IsPostBack)
        {
            foreach (var itm in pwcategories)
            {
                RadPanelItem radItem = new RadPanelItem();
                radItem.Text = itm.category;

                TabIndexholder = radItem.TabIndex;
                radItem.TabIndex = Convert.ToInt16(itm.id);
                //radItem.ContentTemplate = new CustomContentTemplate();
                radItem.ContentTemplate = template;
                template.InstantiateIn(radItem);
                //radItem.AccessKey = accesskeyholder;
                radItem.DataBind();
                radItem.TabIndex = 0;
                radpnlbarCategory.Items.Add(radItem);
            }

        }

        radpnlbarCategory.DataBind();

    }


    class CustomContentTemplate : ITemplate
    {
        string VideoTypes = "264,3g2,3gp,3gp2,3gpp,3gpp2,3mm,3p2,60d,787,89,aaf,aec,aep," +
"aepx,aet,aetx,ajp,ale,am,amc,amv,amx,anim,aqt,arcut,arf,asf,asx,avb,avc,avd,avi," +
"avp,avs,avs,avv,axm,bdm,bdmv,bdt2,bdt3,bik,bin,bix,bmc,bmk,bnp,box,bs4,bsf,bvr," +
"byu,camproj,camrec,camv,ced,cel,cine,cip,clpi,cmmp,cmmtpl,cmproj,cmrec,cpi,cst,cvc,cx3," +
"d2v,d3v,dat,dav,dce,dck,dcr,dcr,ddat,dif,dir,divx,dlx,dmb,dmsd,dmsd3d,dmsm,dmsm3d," +
"dmss,dmx,dnc,dpa,dpg,dream,dsy,dv,dv-avi,dv4,dvdmedia,dvr,dvr-ms,dvx,dxr,dzm,dzp," +
"dzt,edl,evo,eye,ezt,f4f,f4p,f4v,fbr,fbr,fbz,fcp,fcproject,ffd,flc,flh,fli,flv," +
"flx,ftc,gfp,gl,gom,grasp,gts,gvi,gvp,h264,hdmov,hkm,ifo,imovieproj,imovieproject,ircp," +
"irf,ism,ismc,ismclip,ismv,iva,ivf,ivr,ivs,izz,izzy,jmv,jss,jts,jtv,k3g,kdenlive," +
"kmv,ktn,lrec,lsf,lsx,m15,m1pg,m1v,m21,m21,m2a,m2p,m2t,m2ts,m2v,m4e,m4u,m4v,m75," +
"mani,meta,mgv,mj2,mjp,mjpg,mk3d,mkv,mmv,mnv,mob,mod,modd,moff,moi,moov,mov,movie," +
"mp21,mp21,mp2v,mp4,mp4v,mpe,mpeg,mpeg1,mpeg4,mpf,mpg,mpg2,mpgindex,mpl,mpl,mpls," +
"mpsub,mpv,mpv2,mqv,msdvd,mse,msh,mswmm,mts,mtv,mvb,mvc,mvd,mve,mvex,mvp,mvp,mvy," +
"mxf,mxv,mys,ncor,nsv,nut,nuv,nvc,ogm,ogv,ogx,orv,osp,otrkey,pac,par,pds,pgi," +
"photoshow,piv,pjs,playlist,plproj,pmf,pmv,pns,ppj,prel,pro,prproj,prtl,psb,psh,pssd," +
"pva,pvr,pxv,qt,qtch,qtindex,qtl,qtm,qtz,r3d,rcd,rcproject,rdb,rec,rm,rmd,rmd,rmp," +
"rms,rmv,rmvb,roq,rp,rsx,rts,rts,rum,rv,rvid,rvl,sbk,sbt,scc,scm,scm,scn," +
"screenflow,sec,sedprj,seq,sfd,sfvidcap,siv,smi,smi,smil,smk,sml,smv,spl,sqz,srt,ssf," +
"ssm,stl,str,stx,svi,swf,swi,swt,tda3mt,tdx,thp,tivo,tix,tod,tp,tp0,tpd,tpr,trp," +
"ts,tsp,ttxt,tvs,usf,usm,vc1,vcpf,vcr,vcv,vdo,vdr,vdx,veg,vem,vep,vf,vft,vfw," +
"vfz,vgz,vid,video,viewlet,viv,vivo,vlab,vob,vp3,vp6,vp7,vpj,vro,vs4,vse,vsp,w32," +
"wcp,webm,wlmp,wm,wmd,wmmp,wmv,wmx,wot,wp3,wpl,wtv,wve,wvx,xej,xel,xesc,xfl," +
"xlmv,xmv,xvid,y4m,yog,yuv,zeg,zm1,zm2,zm3,zmv";

        APRGuidanceDataClassesDataContext db = new APRGuidanceDataClassesDataContext();
        public void InstantiateIn(Control container)
        {
            Literal ltlplaceholder = new Literal();
            StringBuilder sb = new StringBuilder();
            RadPanelItem radPnlItem = container as RadPanelItem;
            int catid = Convert.ToInt32(radPnlItem.TabIndex);

            IQueryable<APRGuidance> webinarRCD;
            if (HttpContext.Current.User.IsInRole("Administrators") || HttpContext.Current.User.IsInRole("Data"))
            {
                webinarRCD = from w in db.APRGuidances where w.cat_id == catid && w.isActive == true orderby w.displayorder select w;
            }
            else
            {
                webinarRCD = from w in db.APRGuidances where w.cat_id == catid && w.isActive == true orderby w.displayorder select w;
            }
            if (webinarRCD != null && webinarRCD.Count() > 0)
            {
                sb.Append("<div class='Panel_slide'>");
                foreach (var witem in webinarRCD)
                {
                    sb.Append("<div style='padding-bottom:1px;'>");
                    if (witem.images != null)
                    {
                        sb.Append("<img width='20' style='vertical-align:middle;' src='");
                        sb.Append("../img/Handler.ashx?PhotoID=" + witem.id + "'");
                        sb.Append(" alt='");
                        sb.Append(witem.imageAlt + "'>");

                    }
                    else if (!string.IsNullOrEmpty(witem.ImageTag))
                    {
                        sb.Append("<img width='20' style='vertical-align:middle;' src='");
                        sb.Append(witem.ImageTag.Trim() + "'");
                        sb.Append(" alt='");
                        sb.Append(witem.imageAlt + "'>&nbsp;");

                    }

                    sb.Append("<strong>");
                    sb.Append(witem.Title);
                    sb.Append("&nbsp;&nbsp;</strong><i>(");
                    sb.Append(Convert.ToDateTime(witem.EventDate).ToShortDateString());
                    sb.Append(")</i><br/>");
                    sb.Append(witem.Description);
                    sb.Append("<div class='indent'>");
                    sb.Append("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
                    sb.Append("<tr><td width='85%'><strong class='teal_txt'>File name</strong></td><td width='15%'> <strong class='teal_txt'>Files available</strong></td></tr>");

                    IQueryable<APRGuidanceUploadFile> filesRCD;

                    if (HttpContext.Current.User.IsInRole("Administrators") || HttpContext.Current.User.IsInRole("Data"))
                    {
                        filesRCD = from f in db.APRGuidanceUploadFiles where f.webinar_id == witem.id && f.isActive == true orderby f.FileName select f;
                    }
                    else
                    {
                        filesRCD = from f in db.APRGuidanceUploadFiles where f.isActive == true && f.webinar_id == witem.id orderby f.FileName select f;
                    }

                    bool isAlternative = false;
                    foreach (var fitem in filesRCD)
                    {
                        isAlternative = !isAlternative;
                        if (isAlternative)
                        {
                            sb.Append("<tr bgcolor='#E8E8E8'>");
                        }
                        else
                            sb.Append("<tr>");

                        sb.Append("<td>");
                        sb.Append(fitem.FileName);
                        sb.Append("</td><td width='10%'>");
                        sb.Append("<a href='" + fitem.FileURI + "' target='_blank'>");

                        string fileType = fitem.FileType;
                        bool containsVideoType = VideoTypes.Split(',')
                                               .Where(s => string.Compare(fitem.FileType.ToLower().Trim(), s, true) == 0)
                                               .Count() > 0;
                        if (containsVideoType)
                            fileType = "VIDEO";


                        sb.Append(fileType + "</a>");
                        sb.Append("</td></tr>");
                    }
                    sb.Append("</table>");
                    sb.Append("</div>");


                }
                sb.Append("</div>");
            }


            ltlplaceholder.Text = sb.ToString();
            container.Controls.Add(ltlplaceholder);
        }

    }

    protected void radpnlbarCategory_ItemDataBound(object sender, RadPanelBarEventArgs e)
    {

    }
}