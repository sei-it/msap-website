﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="events.aspx.cs" Inherits="admin_events" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP events
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            MSAP Events</h1>
        <h2>
            MSAP Project Directors Meeting, November 4-5, 2010</h2>
        <p>
            <strong>Download Participant List</strong><br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/MSAP_2010_PD_Mtg_Part_List.pdf" target="_blank">Download
                    PDF</a> <small>(1 MB)</small></p>
        <p>
            <strong>Bird’s Eye View of 2010 MSAP Grantees<br />
                November 4, 2010. U.S. Department of Education<br />
                Anna Hinton, PhD, and Brittany Beth<br />
            </strong>The session highlighted tidbits from this year’s new MSAP grants. Participants
            heard and shared highlights from the new projects. Participants were made aware
            of the diverse themes and resources among their own community.
            <br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/BirdsEyeView.pdf" target="_blank">Download
                    PDF</a> <small>(1 MB)</small></p>
        <p>
            <strong>Introduction to Grants Management and MSAP<br />
                November 4, 2010. Office of the Secretary<br />
                Otis Wilson<br />
            </strong>This session provided an overview of the U.S. Department of Education’s
            financial policies. It also provided guidance on managing your grant project budget,
            including direction for avoiding audit issues and understanding grantees’ budget
            flexibility.<br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Grantsmngmnt.pdf" target="_blank">Download
                    PDF&mdash;Presentation</a> <small>(2.4 MB)</small><br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/ExpandedAuthorities.pdf" target="_blank">Download
                    PDF&mdash;Expanded Authorities</a> <small>(24 KB)</small><br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Resource List.pdf" target="_blank">Download
                    PDF&mdash;Resource List</a> <small>(44 KB)</small><br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Appendix.pdf" target="_blank">Download PDF&mdash;Appendix</a>
            <small>(88 KB)</small>
        </p>
        <p>
            <strong>Program- and Project-Level Goals and Objectives<br />
                November 4, 2010. Center for Evaluation and Education Policy (CEEP)<br />
                Courtney Brown, PhD<br />
            </strong>The Center for Evaluation and Education Policy (CEEP) presented a session
            on developing a high quality performance measurement system. This session provided
            a framework for performance measurement with guidelines on writings strong project
            objectives and high quality performance measures. 2010 grantees received an analysis
            of their project objectives and measures with tips on how to strengthen them.<br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/CEEP.pdf">Download PDF</a>
            <small>(616 KB)</small></p>
        <p>
            <strong>2011 Civil Rights Review of MSAP Grantees<br />
                November 5, 2010. Office of Civil Rights<br />
                Ricardo Soto, Christine Bischoff, and Richard Foster<br />
            </strong>The purposes of this session were to provide information about compliance
            with the MSAP statute’s nondiscrimination requirements and to inform MSAP grantees
            about OCR’s continuation year performance review process. Mr. Soto discussed the
            documents and information needed to conduct the 2011 Civil Rights Reviews and answered
            questions that grantees may have had about OCR’s review process.<br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/OCR.pdf">Download PDF</a>
            <small>(60 KB)</small></p>
        <p>
            <strong>Equity Assistance Centers<br />
                November 5, 2010. Equity Assistance Centers<br />
                JoEtta Gonzales, Velma Cobb, Gail Sunderman, and Maria Pachenco<br />
            </strong>The panel of Equity Assistance Centers discussed how the centers assist
            public schools to promote equitable education opportunities to all children. The
            panel discussed resources, materials and professional development activities that
            are available to magnet schools through the Equity Assistance Centers<br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/JoEttaGonzales.pdf">Download
                    PDF&mdash;JoEtta Gonzales, PhD, Region IX</a> <small>(4.5 MB)</small><br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/VelmaCobb.pdf"> Download
                    PDF&mdash;Velma Cobb, PhD, Region II</a> <small>(660 KB)</small><br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/GailSunderman.pdf">Download
                    PDF&mdash;Gail Sunderman, PhD, Region III</a> <small>(96 KB)</small><br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/MariaPacheco.pdf">Download
                    PDF&mdash;Maria Pachenco, PhD, Region I</a> <small>(148 KB)</small>
        </p>
        <p>
            <strong>Next Steps: MSAP Technical Assistance Center<br />
                November 5, 2010. MSAP Center<br />
                Manya Walton, PhD, Project Director</strong><br />
            This session provided overviews of the MSAP Center's purpose and strategies for
            providing technical assistance to the 2010 grantee cohort. The presentation detailed
            the needs assessment process and technical assistance approach.<br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/NextSteps.pdf">Download
                    PDF</a> <small>(532 KB)</small>
        </p>
        <p>
            <strong>Magnet Schools and School Improvement<br />
                November 5, 2010. Learning Points Associates<br />
                Jessica Johnson, Chief Program Officer</strong><br />
            Ms. Johnson presented on the district's role in school improvement. She compared
            the role of the traditional district with that of the school improvement-focused
            district, concentrating on commitment to success, principal autonomy, initiative
            alignment, accountability and community engagement. Ms. Johnson also provided resources
            on how best to engage districts in magnet schools' mission to improve schools.<br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Student_Achieve-tv.pdf">Download
                    PDF</a> <small>(1.5 MB)</small>
        </p>
        <p>
            <strong>Marketing and Recruitment<br />
                November 5, 2010. Friends of Choice in Urban Schools (FOCUS)<br />
                Ariana Quiñones-Miranda</strong><br />
            Ms. Quiñones-Miranda discussed the importance of student recruitment and the Six
            Rs of Recruitment. She provided examples of traditional and out-of-the box recruitment
            strategies and spoke about the importance of creating recruitment plans and branding.
            She also discussed the importance of evaluating recruitment strategies and managing
            expectations.<br />
            <img src="../images/PDFlogo.jpg" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/ArianaQuinones.pdf">Download
                    PDF</a> <small>(212 KB)</small></p>
    </div>
</asp:Content>
