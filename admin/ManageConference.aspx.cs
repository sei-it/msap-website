﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_ManageConference : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {

        MagnetHomepageEvent.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    protected void OnGridViewPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = (from m in db.MagnetHomepageEvents
                    where m.EventType=="c"
                    orderby m.ID descending
                    select new { m.ID, m.Title, m.Date, Display = (bool)m.Display ? "Yes" : "False", m.Description, m.SortField, m.CreateDate }).ToList();
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtTitle.Text = "";
        txtSortField.Text = "";
        txtDescription.Content = "";
        txtDate.Text = "";
        rblDisplay.SelectedIndex = 0;
        hfID.Value = "";
    }
    protected void OnAddNews(object sender, EventArgs e)
    {
        ClearFields();
        mpeNewsWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetHomepageEvent data = MagnetHomepageEvent.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtTitle.Text = data.Title;
        txtSortField.Text = data.SortField.ToString();
        txtDescription.Content = data.Description;
        txtOrganization.Text = data.Organization;
        txtLocation.Text = data.Location;
        txtDate.Text = data.Date;
        rblDisplay.SelectedIndex = (bool)data.Display ? 1 : 0;
        mpeNewsWindow.Show();
    }
    protected void OnSaveNews(object sender, EventArgs e)
    {
        MagnetHomepageEvent data = new MagnetHomepageEvent();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetHomepageEvent.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            data.CreateDate = DateTime.Now;
        }
        data.Title = txtTitle.Text;
        data.Date = txtDate.Text;
        data.SortField = string.IsNullOrEmpty(txtSortField.Text) ? 1 : Convert.ToInt32(txtSortField.Text);
        data.Description = txtDescription.Content;
        data.Display = rblDisplay.SelectedIndex == 1 ? true : false;
        data.Organization = txtOrganization.Text;
        data.Location = txtLocation.Text;
        data.EventType = "c";
        data.Save();
        LoadData(GridView1.PageIndex);
    }
}