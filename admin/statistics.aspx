﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="statistics.aspx.cs" Inherits="admin_statistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    AWStat Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../js/thickbox-compressed.js"></script>
    <link rel="stylesheet" href="../css/thickbox.css" type="text/css" media="screen" />
    <script language="javascript" type="text/javascript">
<!--        //
        function PopupWin( windowname, w, h, x, y) {
            window.open('https://www.google.com/analytics/web/?hl=en&pli=1#report/visitors-overview/a35011708w62701845p64281551/',
            windowname, "'resizable=yes,location=no, titlebar=no, toolbar=no,scrollbars=yes,menubar=no,status=no,directories=no,width=" + w + ",height=" + h + ",left=" + x + ",top=" + y + "'");
        }
//-->
</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<h1 style="text-align:center">Web Analytics</h1>
    <a name="skip"></a>
    <div class="mainContent">
    <div>
        <h1>AWStat Report</h1>
        <a href="http://nidadev.seiservices.com/awstats/awstats.pl?config=msapcenter&keepThis=true&TB_iframe=true&height=800&width=1000"
            title="AWStat" class="thickbox">View AWStat Report</a>
     </div>
      <h1>Google Analytics Report</h1>
        <a href="javascript:PopupWin( 'GAData','1200','1000','220','20')" >View Google Analytics Report</a>
    </div>
</asp:Content>
