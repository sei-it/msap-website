﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ManageAPRGuidanceCat : System.Web.UI.Page
{
    APRGuidanceDataClassesDataContext db = new APRGuidanceDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {


        //if (ddlwebinarCategory.SelectedValue == "" || ddlwebinarCategory.SelectedValue == "1")
        //    pnlWebinar.Visible = false;
        //else
        //    pnlWebinar.Visible = true;

        if (!IsPostBack)
        {
            loaddata();
        }
        if (Session["guidanceID"] != null)
            ddlAPRCategory.SelectedValue = Session["guidanceID"].ToString();
        Session["guidanceID"] = null;
    }

    private void loaddata()
    {
        var data = cbxAll.Checked ? (from rd in db.APRGuidanceCategories orderby rd.category select rd) : (from rd in db.APRGuidanceCategories where rd.isActive orderby rd.category select rd);
        var data1 = cbxAll.Checked ? (from rd in db.APRGuidanceCategories orderby rd.category select rd) : (from rd in db.APRGuidanceCategories where rd.isActive || rd.category.Trim().ToLower() == "select one" orderby rd.category select rd);

        gvAPRCat.DataSource = data;
        ddlAPRCategory.DataSource = data1;
        gvAPRCat.DataBind();
        ddlAPRCategory.DataBind();
    }


    protected void OnAddCategory(object sender, EventArgs e)
    {
        mpeNewsWindow.Show();
    }
    protected void OnAddWebinar(object sender, EventArgs e)
    {
        Response.Redirect("editAPRGuidance.aspx?catid=" + ddlAPRCategory.SelectedValue);
    }

    protected void OnEdit(object sender, EventArgs e)
    {
        hfID.Value = (sender as LinkButton).CommandArgument;
        Response.Redirect("editAPRGuidance.aspx?webinarid=" + hfID.Value);
    }

    protected void OnSave(object sender, EventArgs e)
    {
        APRGuidanceCategory rcd = hfCatID.Value=="" ? new APRGuidanceCategory() : db.APRGuidanceCategories.SingleOrDefault(x=>x.id == Convert.ToInt32(hfCatID.Value));

        rcd.category = txtCatName.Text.Trim();
        rcd.note = txtDes.Text.Trim();
        rcd.isActive = cbxActive.Checked;
        rcd.isPublish = cbxPublish.Checked;
        rcd.CreatedDate = DateTime.Now;
        rcd.CreatedBy = HttpContext.Current.User.Identity.Name;

        if(hfCatID.Value=="")
            db.APRGuidanceCategories.InsertOnSubmit(rcd);
        db.SubmitChanges();

        Response.Redirect("manageAPRGuidance.aspx");
    }

    protected void OnEditCat(object sender, EventArgs e)
    {
        int Catid = Convert.ToInt32((sender as LinkButton).CommandArgument);
        hfCatID.Value = Catid.ToString();
        var data = db.APRGuidanceCategories.SingleOrDefault(x => x.id == Catid);
        if (data != null)
        {
            txtCatName.Text = data.category;
            txtDes.Text = data.note;
            cbxActive.Checked = data.isActive;
            cbxPublish.Checked = data.isPublish;

            mpeNewsWindow.Show();
        }
    }
    protected void cbxAll_CheckedChanged(object sender, EventArgs e)
    {
        loaddata();
    }
}