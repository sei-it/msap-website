﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageconsultants.aspx.cs" Inherits="admin_managemembers" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Consultants
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Manage Consultants</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p style="text-align: left">
        <asp:Button ID="Newbutton" runat="server" Text="New Consultant" CssClass="msapBtn"
            OnClick="OnAdd" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="ConsultTbl">
        <Columns>
            <asp:BoundField DataField="Name" SortExpression="" HeaderText="Consultant" />
            <asp:BoundField DataField="Expertise" SortExpression="" HeaderText="Expertise" HtmlEncode="false" />
            <asp:BoundField DataField="Image" SortExpression="" HeaderText="Image" HtmlEncode="false" HeaderStyle-CssClass="ConsultTbl_header">  <ItemStyle CssClass="Consult_centerAlign">
              </ItemStyle>
</asp:boundfield>
            <asp:BoundField DataField="Active" SortExpression="" HeaderText="Active" HtmlEncode="false" HeaderStyle-CssClass="ConsultTbl_header"> <ItemStyle CssClass="Consult_centerAlign">
              </ItemStyle>
</asp:boundfield>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton><br/>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <div class="Consult_footer">&nbsp;</div>
    <asp:HiddenField ID="hfID" runat="server" />
    <%-- News --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult">
                Add/Edit Consultant
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" Text="Close" CssClass="closeit_link" />
                </span>
            </div>
            <div style="height:410px;overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                    <tr>
                        <td>
                        	<div class="consult_popupTxt">Active</div>
                            <asp:RadioButtonList ID="rblActive" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:RadioButtonList></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <div class="consult_popupTxt">Consultant Name</div>
                            <asp:TextBox ID="txtMemberName" runat="server" MaxLength="250" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="consult_popupTxt">Expertise</div>

                            <cc1:Editor ID="txtExpertise" runat="server" Width="470" Height="100" SupportFolder="~/aspnet_client/FreeTextBox"
                                JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000">
                            </cc1:Editor>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="consult_popupTxt">Bio</div>
                            <cc1:Editor ID="txtBio" runat="server" Width="470" Height="200" SupportFolder="~/aspnet_client/FreeTextBox"
                                JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000">
                            </cc1:Editor>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="consult_popupTxt">Image</div>
                            <asp:FileUpload ID="FileUpload1" runat="server" /><asp:Label ID="lblMessage" runat="server"
                                Visible="false" Text="You must upload a file that is 100px wide and a max of 117px in height."
                                ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeNewsWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>
