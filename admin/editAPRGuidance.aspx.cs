﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;

public partial class admin_editAPRGuidance : System.Web.UI.Page
{
    APRGuidanceDataClassesDataContext db = new APRGuidanceDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int webinarid = Request.QueryString["webinarid"] == "" ? 0 : Convert.ToInt32(Request.QueryString["webinarid"]);
            int webcatid = Request.QueryString["catid"] == "" ? 0 : Convert.ToInt32(Request.QueryString["catid"]);

            Session["APRGuidancewebinarID"] = webinarid;
            if (webinarid > 0)
            {
                EditWebinar(webinarid);
                ldsFileUpload.Where = " webinar_id=" + webinarid;

                if (webinarid > 0 && webcatid == 0)
                    Session["APRGuidancewebCatID"] = (int)db.APRGuidances.SingleOrDefault(x => x.id == webinarid).cat_id;
            }
            else
            {
                if (webcatid == 0)
                    Response.Redirect("manageAPRGuidance.aspx");
                AddWebinar(webcatid);
                btnNewUploadFile.Visible = false;
            }
        }
    }

    private void AddWebinar(int webinarcategoryid)
    {
        resetFields();
        ldsFileUpload.Where = "webinar_id=0";
        Session["APRGuidancewebCatID"] = webinarcategoryid;
    }

    private void EditWebinar(int webinarid)
    {
        //resetFields();
        var item = db.APRGuidances.SingleOrDefault(x => x.id == webinarid);
        if (item != null)
        {
            //txtAlt.Text = item.imageAlt;
            txtTitle.Text = item.Title;
            txtEventDate.Text = item.EventDate == null ? "" : Convert.ToDateTime(item.EventDate).ToString("MM/dd/yyyy"); txtDescription.Content = item.Description;
            txtDisOrder.Text = item.displayorder.ToString();

            rblwPublish.SelectedIndex = (bool)item.isActive ? 1 : 0;


            Image ib = new Image();
            Literal imgName = new Literal();

            if (item.images != null)
            {
                string iconName = item.ImageTag.Split('/')[item.ImageTag.Split('/').Count() - 1];
                ib.ID = "image" + webinarid;
                ib.Width = Unit.Pixel(280);
                ib.Height = Unit.Pixel(220);

                ib.ImageUrl = "../img/Handler.ashx?PhotoID=" + item.id.ToString();
                pnlImage.Controls.Add(ib);

                imgName.Text = "<br/><br/><br/><div style='text-align:center'>" + iconName + "</div>";
                pnlImage.Controls.Add(imgName);
            }

        }


    }

    protected void OnSaveUploadFile(object sender, EventArgs e)
    {
        int catid = Session["APRGuidancewebCatID"] == null ? 0 : Convert.ToInt32(Session["APRGuidancewebCatID"]);
        int webinarid = Session["APRGuidancewebinarID"] == null ? 0 : Convert.ToInt32(Session["APRGuidancewebinarID"]);
        string catname = "", webinarName = "";

        if (webinarid > 0 && catid == 0)
            catid = (int)db.APRGuidances.SingleOrDefault(x => x.id == webinarid).cat_id;

        catname = db.APRGuidanceCategories.SingleOrDefault(x => x.id == catid).category.Trim().Replace(" ","");
        webinarName = db.APRGuidances.SingleOrDefault(x => x.id == webinarid).Title.Trim().Replace(" ", ""); 

        APRGuidanceUploadFile newfile;
        if (hfFileID.Value == "")
            //create new upload file
            newfile = new APRGuidanceUploadFile();
        else
        {
            newfile = db.APRGuidanceUploadFiles.SingleOrDefault(x => x.id == Convert.ToInt32(hfFileID.Value));
        }

        if (catid > 0 && fileuploadDoc.HasFile) //new webinar, create one past webinar first
        {

            //remove old file
            if (!string.IsNullOrEmpty(newfile.FileURI))
            {
                string PathAndFile = Server.MapPath("") + "/doc/Guidances/" + catname + "/" + webinarName + "/" + newfile.FileURI;
                //filePath = filePath + @"/" + newfile.FileURI.Split('/')[newfile.FileURI.Split('/').Count() - 1];
                if (File.Exists(PathAndFile))
                {
                    File.Delete(PathAndFile);
                }
            }

            string strFileroot = Server.MapPath("") + "/doc/Guidances/" + catname + "/" + webinarName + "/";
            //string strCategoryName = db.APRGuidanceCategories.SingleOrDefault(x => x.id == catid).category;
            //string strWebinarName = db.APRGuidances.SingleOrDefault(x => x.id == webinarid).Title;
            //string strPath = strCategoryName + "/" + strWebinarName + "/";
            string strFileName = fileuploadDoc.FileName.Replace("#", "");
            //string strPathandFile = strPath + strFileName;
            newfile.FilePath = strFileroot;

            if (!Directory.Exists(strFileroot))
            {
                Directory.CreateDirectory(strFileroot);
            }
            fileuploadDoc.SaveAs(strFileroot + strFileName);

            string[] tempfilename = Regex.Split(fileuploadDoc.PostedFile.FileName, @"\\");

            if (tempfilename.Length > 0)
            {
                newfile.FileURI = "admin/doc/Guidances/" + catname + "/" + webinarName + "/" + tempfilename[tempfilename.Length - 1];
            }
            else
                newfile.FileURI = "admin/doc/Guidances/" + catname + "/" + webinarName + "/" + tempfilename[0];

            string[] filePieces = fileuploadDoc.PostedFile.FileName.Split('.');

            newfile.FileType = filePieces[filePieces.Count() - 1].ToUpper();
        }
        newfile.webinar_id = webinarid;
        newfile.FileName = txtFileName.Text.Trim();
        newfile.CreatedBy = HttpContext.Current.User.Identity.Name;
        newfile.CreatedDate = DateTime.Now;
        newfile.isActive = rblfPublish.SelectedIndex == 1 ? true : false;
        newfile.displayorder = Convert.ToInt32(txtDisplayOrder.Text.Trim() == "" ? "99" : txtDisplayOrder.Text.Trim());

        if (hfFileID.Value == "")
            db.APRGuidanceUploadFiles.InsertOnSubmit(newfile);
        else
        {
            newfile.ModifiedBy = HttpContext.Current.User.Identity.Name;
            newfile.ModifiedDate = DateTime.Now;

        }
        db.SubmitChanges();



        Response.Redirect("editAPRGuidance.aspx?webinarid=" + webinarid);



    }

    protected void OnSaveWebinar(object sender, EventArgs e)
    {
        int catid = Session["APRGuidancewebCatID"] == null ? 0 : Convert.ToInt32(Session["APRGuidancewebCatID"]);
        int webinarid = Session["APRGuidancewebinarID"] == null ? 0 : Convert.ToInt32(Session["APRGuidancewebinarID"]);
        //int len = fuploadimg.PostedFile.ContentLength;
        APRGuidance webinar;
        if (webinarid == 0)
        {
            webinar = new APRGuidance();
            webinar.cat_id = catid;
        }
        else
            webinar = db.APRGuidances.SingleOrDefault(x => x.id == webinarid);
        catid = Convert.ToInt32(webinar.cat_id);
        webinar.Title = txtTitle.Text;
        webinar.Description = txtDescription.Content;
        //if (fuploadimg.HasFile)
        //    webinar.ImageTag = fuploadimg.PostedFile.FileName;
        webinar.EventDate = Convert.ToDateTime(txtEventDate.Text);
        //webinar.imageAlt = txtAlt.Text;
        webinar.isActive = rblwPublish.SelectedIndex == 1 ? true : false;
        webinar.displayorder = Convert.ToInt32(string.IsNullOrEmpty(txtDisOrder.Text) ? "99" : txtDisOrder.Text.Trim());

        //if (len > 0)
        //{
        //    byte[] pic = new byte[len];
        //    fuploadimg.PostedFile.InputStream.Read(pic, 0, len);
        //    webinar.images = pic;

        //    string[] imgPath_file_name = fuploadimg.PostedFile.FileName.Split('\\');

        //    if (imgPath_file_name.Length > 0)
        //    {
        //        webinar.ImageTag = imgPath_file_name[imgPath_file_name.Length - 1];
        //    }
        //    else
        //        webinar.ImageTag = imgPath_file_name[0];
        //}

        webinar.CreatedBy = HttpContext.Current.User.Identity.Name;
        webinar.CreatedDate = DateTime.Now;

        if (webinarid == 0)
            db.APRGuidances.InsertOnSubmit(webinar);
        else
        {
            webinar.ModifiedBy = HttpContext.Current.User.Identity.Name;
            webinar.ModifiedDate = DateTime.Now;

        }

        //db.PastedWebinars.Attach(webinar, true);
        db.SubmitChanges();
        Response.Redirect("manageAPRGuidance.aspx?catid=" + catid);
    }

    private void resetFields()
    {
        //txtAlt.Text = "";
        txtTitle.Text = "";
        txtEventDate.Text = "";
        txtDescription.Content = "";
        rblwPublish.SelectedIndex = 0;

    }
    protected void OnAddUploadFile(object sender, EventArgs e)
    {
        mpeAddUploadFile.Show();
    }

    protected void OnEditFile(object sender, EventArgs e)
    {
        //resetFields();
        hfFileID.Value = (sender as LinkButton).CommandArgument;
        APRGuidanceUploadFile fileinfo = db.APRGuidanceUploadFiles.SingleOrDefault(x => x.id == Convert.ToInt32(hfFileID.Value));
        txtFileName.Text = fileinfo.FileName;
        ltlfilepath.Text = fileinfo.FileURI;
        txtDisplayOrder.Text = fileinfo.displayorder.ToString();
        rblfPublish.SelectedIndex = Convert.ToInt32(fileinfo.isActive);


        mpeAddUploadFile.Show();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        int catid = Session["APRGuidancewebCatID"] == null ? 0 : Convert.ToInt32(Session["APRGuidancewebCatID"]);
        Response.Redirect("manageAPRGuidance.aspx?catid=" + catid);
    }
}