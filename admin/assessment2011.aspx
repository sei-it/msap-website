﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" CodeFile="assessment2011.aspx.cs" Inherits="admin_assessment2011"%>

<script runat="server">

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Needs Assessment 2011
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Needs Assessment Information Webinars</h1>
    <h2>
Needs Assessment Plan</h2>
    <div id="Div1" runat="server">
       
    </div>
    <br/>
      <h2>Needs Assessment Findings</h2>
     <div id="Div2" runat="server">
    	
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr>
                <td width='90%'>
                    <strong>File name</strong>
                </td>
                <td width='10%'>
                    <strong>Files available</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Needs Assessment Findings Presentation
                </td>
                <td width='15%'>
                    <a href='doc/NA_files/Needs_Assessment_Findings_Presentation.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>Needs Assessment Findings Transcript</td>
                <td width='15%'>
                    <a href='doc/NA_files/Needs_Assessment_Findings_Transcript.pdf' target='_blank'>PDF</a>
                </td>
            </tr>
            <tr>
                <td>
                    Needs Assessment Findings Webinar
                </td>
                <td width='15%'>
                    <a href='doc/NA_files/Needs_Assessment_Findings_4_11_12.wmv' target='_blank'>Video</a>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
