﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="resources.aspx.cs" Inherits="admin_resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Resources
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
    <asp:ListItem Value="2010" >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2013" Selected="True">MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
<asp:Panel ID="pnlCohort2010" runat="server">
    <h1> MSAP Program Officers for 2010 Cohort</h1>
    <div>
        <div class="onethirdDiv">
            <div class="PO_txt1">
                <img src="../images/BrittanyBeth.jpg" title="Rosie Kelley" style="float: left; margin-right: 8px;
                    border: 1px solid #078576; padding: 4px; background: #fff;" /><h2>
                        Brittany Beth</h2>
                <a href="mailto:brittany.beth@ed.gov">brittany.beth@ed.gov</a><br />
                202-453-6653<br />
                <div class="clear">
                </div>
                    <p>
                         <strong>Connecticut</strong><br />
                        Area Cooperative Educational Services<br />
                        New Haven Public Schools
                    </p> 
                    <p>
                        <strong>Florida</strong><br />
                        Duval County Public Schools
                    </p>
                    <p>
                    	<strong>Louisiana</strong><br />
                    	Tangipahoa Parish School District
                    </p>
                    <p>
                        <strong>Minnesota</strong><br />
                        Independent School District 197
                    </p>
                    <p>
                    	<strong>Mississippi</strong><br />
                    	Cleveland School District
                    </p>
                    <p>
                    	<strong>New York</strong><br/>
                        City School District of Albany<br/>
                        New York City Community School District 3<br/>
                        New York City Community School District 30
                    </p>
                    <p>
                        <strong>South Carolina</strong><br />
                        Orangeburg County Consolidated School District 3
                    </p>
                    <p>
                    	<strong>Texas</strong><br />
	                    Houston Independent School District<br />
                	</p>
            </div>
        </div>
        <div class="onethirdDiv">
            <div class="PO_txt2">
                <img src="../images/Ty.jpg" title="Tyrone (Ty) Harris" style="float: left; margin-right: 8px;
                    border: 1px solid #078576; padding: 4px; background: #fff;" />
                <h2>
                    Tyrone (Ty) Harris</h2>
                <a href="mailto:brittany.beth@ed.gov">tyrone.harris@ed.gov</a><br />
                202-453-5629<br />
                <div class="clear">
                </div>
                <p>
                	<strong>Arizona</strong><br/>
                    Tucson Unified School District
                </p>
                <p>
                    <strong>California</strong><br />
                        ABC Unified School District<br />
                        Desert Sands Unified School District<br />
                        Los Angeles Unified School District<br />
                        Moorpark Unified School District<br />
                        Napa Valley Unified School District<br />
                        San Diego Unified School District<br />
                        Ventura Unified School District
                    </p>
                <p>
                    <strong>Colorado</strong><br />
                    El Paso County School District #11
                </p>
                 <p>
                	<strong>Florida</strong><br/>
                    Hillsborough County Public Schools
                </p>
                 <p>
                	<strong>Indiana</strong><br/>
                    Metropolitan School District of Lawrence Township
                </p>
                <p>
                    <strong>Kansas</strong><br />
                    Unified School District 259 dba Wichita Public<br /> Schools
                </p>
                <p>
                    <strong>Texas</strong><br />
                    Corpus Christi Independent School District<br />
                    The Rhodes School
                </p>
            </div>
        </div>
        <div class="onethirdDiv">
            <div class="PO_txt2">
                <img src="../images/VE_ED2.jpg" title="Tyrone (Ty) Harris" style="float: left; margin-right: 8px;
                    border: 1px solid #078576; padding: 4px; background: #fff;" />
                <h2>Veronica Edwards</h2>
                <a href="mailto:veronica.edwards@ed.gov">veronica.edwards@ed.gov</a><br />
                202-260-2403<br />
                <div class="clear">
                </div>
                <p>
                	<strong>California</strong><br/>
                    Glendale Unified School District
                </p>
                <p>
                    <strong>Connecticut</strong><br />
                       Capitol Region Education Council
                    </p>
                <p>
                    <strong>Florida</strong><br />
                    The School Board of Broward County<br />
                    The School District of Lee County<br />
                    The School Board of Polk County<br />
                </p>
                <p>
                	<strong>Illinois</strong><br/>
                    Champaign Community Unit School District 4
                </p>
                <p>
                    <strong>Louisiana</strong><br />
                    Lafayette Parish School Board
                </p>
                <p>
                    <strong>Massachusetts</strong><br />
                    Springfield Public Schools
                </p>
                <p>
                <strong>New York</strong><br />
                New York City Community School District 14
                </p>
                <p>
                <strong>South Carolina</strong><br />
                Richland School District Two
                </p>
                 <p>
                <strong>Tennessee</strong><br />
                Metropolitan Nashville Public Schools
                </p>
                  <p>
                <strong>Texas</strong><br />
                Galveston Independent School District
                </p>
            </div>
        </div>
    </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlCohort2013" runat="server">
    <h1> MSAP Program Officers for 2013 Cohort</h1>
 
        <div class="onethirdDiv">
            <div class="PO_txt1">
                <img src="../images/BrittanyBeth.jpg" title="Rosie Kelley" style="float: left; margin-right: 8px;
                    border: 1px solid #078576; padding: 4px; background: #fff;" /><h2>
                        Brittany Beth</h2>
                <a href="mailto:brittany.beth@ed.gov">brittany.beth@ed.gov</a><br />
                202-453-6653<br />
                <div class="clear">
                </div>
                <p>
                    <strong>California</strong><br />
                       Los Angeles Unified School District<br />
                        Napa Valley Unified School District<br />
                        Oxnard School District<br />
                        Pasadena Unified School District<br />
                        San Diego Unified School District<br />
                        Ventura Unified School District
                    </p>
                <p>
                <strong>Colorado</strong><br />
                    Pueblo City School District
                </p>
                    <p>
                        <strong>Connecticut</strong><br />
                        Bridgeport City School District<br />
                        LEARN<br />
                        New Haven Public Schools
                    </p> 
<%--                    <p>
                        <strong>Florida</strong><br />
                        Duval County Public Schools
                    </p>
                    <p>
                    <strong>Kansas</strong><br />
                    Wichita Public Schools
                    </p>
                    <p>
                    	<strong>Louisiana</strong><br />
                    	Tangipahoa Parish School District
                    </p>--%>
                     <p>
                        <strong>Kansas</strong><br />
                        Wichita Public Schools
                    </p>
   <p>
                    <strong>Massachusetts</strong><br />
                    Springfield Public Schools
                </p>
                <p>
                <strong>Michigan</strong><br />
                Lansing School District
                </p>
                   <%-- <p>
                        <strong>Minnesota</strong><br />
                        Independent School District 197
                    </p>
                    <p>
                    	<strong>Mississippi</strong><br />
                    	Cleveland School District
                    </p>--%>
             

                    <p>
                    	<strong>New York</strong><br/>
                        New York City Community School Districts 13 and 15
                        New York City Community School District 28<br />
                    </p>
                   
            </div>
        </div>
       <%-- <div class="onethirdDiv">
            <div class="PO_txt2">
                <img src="../images/Ty.jpg" title="Tyrone (Ty) Harris" style="float: left; margin-right: 8px;
                    border: 1px solid #078576; padding: 4px; background: #fff;" />
                <h2>
                    Tyrone (Ty) Harris</h2>
                <a href="mailto:brittany.beth@ed.gov">tyrone.harris@ed.gov</a><br />
                202-453-5629<br />
                <div class="clear">
                </div>
                <p>
                	<strong>Arizona</strong><br/>
                    Tucson Unified School District
                </p>
                <p>
                    <strong>California</strong><br />
                       Los Angeles Unified School District<br />
                        Napa Valley Unified School District<br />
                        Oxnard School District<br />
                        Pasadena Unified School District<br />
                        San Diego Unified School District<br />
                        Ventura Unified School District
                    </p>
                <p>
                    <strong>Colorado</strong><br />
                    El Paso County School District #11
                </p>
                 <p>
                	<strong>Florida</strong><br/>
                    Hillsborough County Public Schools
                </p>
                 <p>
                	<strong>Indiana</strong><br/>
                    Metropolitan School District of Lawrence Township
                </p>
                <p>
                    <strong>Kansas</strong><br />
                    Unified School District 259 dba Wichita Public<br /> Schools
                </p>
                      <p>
                    	<strong>Texas</strong><br />
	                    Galveston Independent School District<br />
                        Houston Independent School District<br />
                        Waco Independent School District
                	</p>

            </div>
        </div>--%>
        <div class="onethirdDiv">
            <div class="PO_txt2">
                <img src="../images/Justis.jpg" height="129px" title="Tyrone (Ty) Harris" style="float: left; margin-right: 8px;
                    border: 1px solid #078576; padding: 4px; background: #fff;" />
                <h2>Justis Tuia</h2>
                <a href="mailto:Justis.tuia@ed.gov">justis.tuia@ed.gov</a><br />
                202-453-6654<br />
                <div class="clear">
                </div>
<p>
<strong>Arkansas</strong><br/>
	Texarkana Arkansas School District
	
</p> 
<p>
	<strong>Florida</strong><br/>
	Brevard Public Schools<br/>
	 School Board of Miami-Dade County<br/>
	School Board of Polk County<br/>
	The School Board of Broward County<br/>
Seminole County Public Schools
</p>
<p>
<strong>Mississippi</strong><br/>
Clarksdale Municipal School District
</p>
<p>
<strong>South Carolina</strong><br/>
Richland School District Two<br/>
School District Five of Lexington and Richland Counties
</p>
<p>
    <strong>Texas</strong><br />
	Galveston Independent School District<br />
    Houston Independent School District<br />
    Waco Independent School District
</p>
            </div>
        </div>
    
    </asp:Panel>
</asp:Content>
