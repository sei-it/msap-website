﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using ExcelLibrary.SpreadSheet;

public partial class admin_managenews : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.Identity.Name == "jwang")
            btnExcel.Visible = true;
        else
            btnExcel.Visible = false;

        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetNews.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    protected void OnGridViewPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = (from m in db.MagnetNews
                    orderby m.ID descending
                    select new { m.ID, m.Title, m.Source, m.Author, m.Date, m.CreatedDate}).ToList();
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtTitle.Text = "";
        txtSource.Text = "";
        txtAuthor.Text = "";
        txtDate.Text = "";
        lblMessage.Visible = false;
        txtContent.Content = "";
        rblDisplay.SelectedIndex = 0;
        hfID.Value = "";
    }
    protected void OnAddNews(object sender, EventArgs e)
    {
        ClearFields();
        mpeNewsWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetNews news = MagnetNews.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtTitle.Text = news.Title;
        txtSource.Text = news.Source;
        txtAuthor.Text = news.Author;
        txtDate.Text = news.Date;
        txtContent.Content = news.NewsContent;
        rblDisplay.SelectedIndex = (bool)news.Display ? 1 : 0;
        mpeNewsWindow.Show();
    }
    protected void OnSaveNews(object sender, EventArgs e)
    {
        DateTime dt = new DateTime();
        if (DateTime.TryParse(txtDate.Text, out dt))
        {
            MagnetNews news = new MagnetNews();
            if (!string.IsNullOrEmpty(hfID.Value))
            {
                news = MagnetNews.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
            }
            else
            {
                news.CreatedDate = DateTime.Now;
            }
            news.Title = txtTitle.Text;
            news.Source = txtSource.Text;
            news.Author = txtAuthor.Text;
            news.Date = txtDate.Text;
            news.NewsContent = txtContent.Content;
            news.Display = rblDisplay.SelectedIndex == 1 ? true : false;
            news.Save();
            LoadData(GridView1.PageIndex);
        }
        else
        {
            lblMessage.Visible = true;
            mpeNewsWindow.Show();
        }
    }

    protected void ExportExcelReport(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.Identity.Name == "jwang")
        {
            using (System.IO.MemoryStream output = new System.IO.MemoryStream())
            {
                Workbook workbook = new Workbook();

                int row = 0;

                Worksheet worksheet = new Worksheet("Magnet User Log");

                worksheet.Cells[row, 0] = new Cell("User Name");
                worksheet.Cells[row, 1] = new Cell("Active Date");
                worksheet.Cells[row, 2] = new Cell("Active Time");
                worksheet.Cells[row, 3] = new Cell("Login Date");
                worksheet.Cells[row, 4] = new Cell("Login Time");
                worksheet.Cells[row, 5] = new Cell("Topic");
                //ReportType: true --- first time a report created
                dcvwTrackUserDataContext db = new dcvwTrackUserDataContext();

                foreach (var log in db.vwTrackUserLogs)
                {
                    row++;
                    worksheet.Cells[row, 0] = new Cell(log.UserName);
                    worksheet.Cells[row, 1] = new Cell(log.ActiveDate);
                    worksheet.Cells[row, 2] = new Cell(log.ActiveTime);
                    worksheet.Cells[row, 3] = new Cell(log.LoginDate);
                    worksheet.Cells[row, 4] = new Cell(log.LoginTime);
                    worksheet.Cells[row, 5] = new Cell(log.Topic);

                }
                workbook.Worksheets.Add(worksheet);
                workbook.Save(output);
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + "TrackUserLog.xls");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
                output.Close();
            }



        }
        else
            Response.Redirect("~/Login.aspx");
    }
}