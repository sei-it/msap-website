﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_memberdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                MagnetTWGMember member = MagnetTWGMember.SingleOrDefault(x => x.ID == Convert.ToInt32(Request.QueryString["id"]));
                content.InnerHtml = member.Bio;
            }
        }
    }
}