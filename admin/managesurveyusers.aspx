﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managesurveyusers.aspx.cs" Inherits="managesurveyusers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Survey Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Manage Survey Users</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New User" CausesValidation="false"
                CssClass="msapBtn" OnClick="OnAdd" />
        </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            OnPageIndexChanging="OnPageIndexChanging">
            <Columns>
                <asp:BoundField DataField="GranteeName" SortExpression="" HeaderText="Grantee Name" />
                <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School Name" />
                <asp:BoundField DataField="UserName" SortExpression="" HeaderText="Principal" />
                <asp:BoundField DataField="Passcode" SortExpression="" HeaderText="Passcode" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CausesValidation="false"
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false"
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hfID" runat="server" />
        <%-- Event --%>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Survey Users</div>
                <table>
                    <tr>
                        <td>
                            Grantee:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGrantee" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnGranteeChanged">
                                <asp:ListItem Value="">Please select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required!"
                                ControlToValidate="ddlGrantee"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            School:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSchoolChanged">
                                <asp:ListItem Value="">Please select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required!"
                                ControlToValidate="ddlSchool"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Principal:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPrincipal" runat="server">
                                <asp:ListItem Value="">Please select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required!"
                                ControlToValidate="ddlPrincipal"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Passcode:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPasscode" runat="server" MaxLength="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                        </td>
                        <td>
                            <asp:Button ID="Button15" runat="server" CausesValidation="false" CssClass="msapBtn"
                                Text="Close Window" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" CausesValidation="false" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
            BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
