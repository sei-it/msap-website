﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Web.Security;


public partial class admin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int reportyear = 1;
            DateTime today = DateTime.Now;
            CalendarControl1.Month = today.Month;
            CalendarControl1.Year = today.Year;
            CalendarControl1.GroupID = 0;

            var report = MagnetReportPeriod.SingleOrDefault(x => x.isReportPeriod);
            if (report == null)
            {
                MAPSID.Visible = false;
                //ToolkitAppleID.Attributes["style"] = "text-align: center; float: left; width: 106px; margin-left: 0px;";
                reportyear = MagnetReportPeriod.Find(x => x.isActive).Last().ID;
            }
            else
            {   // ED, ProjectDirector, Evaluator, Project Director TWG, EvaluatorTWG, Data, GrantStaff"
                if(HttpContext.Current.User.IsInRole("Administrators") ||
                    HttpContext.Current.User.IsInRole("ED") ||
                    HttpContext.Current.User.IsInRole("ProjectDirector") ||
                    HttpContext.Current.User.IsInRole("Evaluator") ||
                    HttpContext.Current.User.IsInRole("Project Director TWG") ||
                    HttpContext.Current.User.IsInRole("EvaluatorTWG") ||
                    HttpContext.Current.User.IsInRole("Data") ||
                    HttpContext.Current.User.IsInRole("GrantStaff"))
                    MAPSID.Visible = true;
                else
                    MAPSID.Visible = false;
                //ToolkitAppleID.Attributes["style"] = "margin: 0 auto; text-align: center; width: 106px;";
                reportyear = report.ID;

            }
            if (HttpContext.Current.User.IsInRole("Administrators"))
            {
                MAPSID.Visible = true;
                //ToolkitAppleID.Attributes["style"] = "margin: 0 auto; text-align: center; width: 106px;";
            }

            Session["ReportPeriodID"] = reportyear;

            switch (reportyear)
            {
                case 1:
                case 2:
                    reportyear = 1;
                    break;
                case 3:
                case 4:
                    reportyear = 2;
                    break;
                case 5:
                case 6:
                    reportyear = 3;
                    break;
                default:
                    reportyear = 1;
                    break;

            }
            Session["ReportYear"] = reportyear;
        }
    }
}