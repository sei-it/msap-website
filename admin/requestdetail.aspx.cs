﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class admin_requestdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && Request.QueryString["id"] != null)
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            MagnetTARequest request = MagnetTARequest.SingleOrDefault(x => x.ID == id);
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='msapTbl'><tr><td colspan='2' align='center'><b>TA Request</b></td></tr>");
            sb.AppendFormat("<tr><td>First Name:</td><td>{0}</td>", request.FirstName);
            sb.AppendFormat("<tr><td>Last Name:</td><td>{0}</td>", request.LastName);
            sb.AppendFormat("<tr><td>Title:</td><td>{0}</td>", request.Title);
            sb.AppendFormat("<tr><td>District Name:</td><td>{0}</td>", request.District);
            sb.AppendFormat("<tr><td>Address:</td><td>{0}</td>", request.Address);
            sb.AppendFormat("<tr><td>Address 2:</td><td>{0}</td>", request.Addresscon);
            sb.AppendFormat("<tr><td>City:</td><td>{0}</td>", request.City);
            sb.AppendFormat("<tr><td>State:</td><td>{0}</td>", request.State);
            sb.AppendFormat("<tr><td>Zip code:</td><td>{0}</td>", request.Zipcode);
            sb.AppendFormat("<tr><td>Telephone:</td><td>{0}</td>", request.Telephone);
            sb.AppendFormat("<tr><td>Alternative Telephone:</td><td>{0}</td>", request.AlternatePhone);
            sb.AppendFormat("<tr><td>Fax:</td><td>{0}</td>", request.Fax);
            sb.AppendFormat("<tr><td>Email:</td><td>{0}</td>", request.Email);
            sb.AppendFormat("<tr><td>Preferred contact method:</td><td>{0}</td>", (bool)request.PreferredMethod ? "Phone" : "Email");
            sb.AppendFormat("<tr><td>Best time to be reached:</td><td>{0}</td>", request.BestTime);
            sb.AppendFormat("<tr><td>TA request description:</td><td>{0}</td>", Server.HtmlDecode(request.TARequest));
            sb.Append("</table>");
            Content.InnerHtml = sb.ToString();
        }
    }
}