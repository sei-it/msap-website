﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Configuration;

public partial class admin_managepublicevents : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetPublicEvent.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    protected void OnGridViewPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = (from m in db.MagnetPublicEvents
                    orderby m.CreateDate descending
                    select new { m.ID, EventType = (bool)m.EventType == false ? "Webinars" : "Conferences", m.EventTitle, m.Organization, m.Date, m.Time, m.Registration, m.Location }).ToList();
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    protected void OnEventTypeChanged(object sender, EventArgs e)
    {
        switch (rblEventType.SelectedIndex)
        {
            case 0:
                rwStartDate.Visible = false;
                rwTime.Visible = true;
                rwRegistration.Visible = true;
                rwLocation.Visible = false;
                break;
            case 1:
                rwStartDate.Visible = true;
                rwTime.Visible = false;
                rwRegistration.Visible = false;
                rwLocation.Visible = true;
                break;
        }
    }
    private void ClearFields()
    {
        rblEventType.SelectedIndex = 0;
        txtEventTitle.Text = "";
        txtOrganization.Text = "";
        txtDate.Text = "";
        txtTime.Text = "";
        txtRegistration.Text = "";
        txtLocation.Text = "";
        txtDescription.Content = "";
        rblDisplay.SelectedIndex = 1;
        txtStartDate.Text = "";
        rwStartDate.Visible = false;
        rwTime.Visible = true;
        rwRegistration.Visible = true;
        rwLocation.Visible = false;
        hfID.Value = "";
    }
    protected void OnAddEvent(object sender, EventArgs e)
    {
        ClearFields();
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetPublicEvent publicEvent = MagnetPublicEvent.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        rblEventType.SelectedIndex = (bool)publicEvent.EventType ? 1 : 0;
        txtEventTitle.Text = publicEvent.EventTitle;
        txtOrganization.Text = publicEvent.Organization;
        txtStartDate.Text = publicEvent.StartDate;
        txtDate.Text = publicEvent.Date;
        txtTime.Text = publicEvent.Time;
        txtRegistration.Text = publicEvent.Registration;
        txtLocation.Text = publicEvent.Location;
        txtDescription.Content = publicEvent.Description;
        rblDisplay.SelectedIndex = (bool)publicEvent.Display ? 1 : 0;

        switch (publicEvent.EventType)
        {
            case false:
                rwStartDate.Visible = false;
                rwTime.Visible = true;
                rwRegistration.Visible = true;
                rwLocation.Visible = false;
                break;
            case true:
                rwStartDate.Visible = true;
                rwTime.Visible = false;
                rwRegistration.Visible = false;
                rwLocation.Visible = true;
                break;
        }
        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetPublicEvent publicEvent = new MagnetPublicEvent();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            publicEvent = MagnetPublicEvent.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            publicEvent = new MagnetPublicEvent();
            publicEvent.CreateDate = DateTime.Now;
        }
        publicEvent.EventType = Convert.ToBoolean(rblEventType.SelectedIndex);
        publicEvent.EventTitle = txtEventTitle.Text;
        publicEvent.Organization= txtOrganization.Text;
        publicEvent.StartDate = txtStartDate.Text;
        publicEvent.Date = txtDate.Text;
        publicEvent.Time = txtTime.Text;
        publicEvent.Registration= txtRegistration.Text;
        publicEvent.Location= txtLocation.Text;
        publicEvent.Display = Convert.ToBoolean(rblDisplay.SelectedIndex);
        string description = txtDescription.Content;
        if (string.IsNullOrEmpty(hfID.Value))
        {
            //New link
            description = description.Replace("<a ", "<a target='_blank'");
        }
        publicEvent.Description = description;
        publicEvent.Save();
        LoadData(GridView1.PageIndex);
    }
}