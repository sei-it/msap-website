﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/admin/magnetadmin.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_Default" %>

<%@ Register Src="CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP private site
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/thickbox-compressed.js"></script>
    <link rel="stylesheet" href="../css/thickbox.css" type="text/css" media="screen" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="bnrImg">
<img src="../images/privatehp_pic.gif" alt="teacher helping students on the computer" /></div>
    <a name="skip"></a>
    <div class="msapHP_Content">
        <div class="granteeCorner">
            <div class="gcTop">
                &nbsp;</div>
            <div class="gcContent">
                <div class="gcTxt">
                <a href="messageboard/default.aspx?g=forum">
                            <img src="../images/MagnetConnector.png" alt="sticky note reading message board" border="0" /></a>
                            <p>Join a conversation and read online forums to learn from your colleagues. <a href="messageboard/default.aspx?g=forum">Learn More</a></p>
                            
                            </div>
                </div>
                <!-- /gcTxt -->
            
            <div class="gcBot">
                &nbsp;</div>
                </div>
                <div class="msapHP_colR" style="width: 240px; margin-top: 10px;">
                <uc1:CalendarControl ID="CalendarControl1" runat="server" />

</div>
        <div class="msapHPcolL">
        <p class="welcomeTxt">Welcome to the MSAP Center Private Workspace!</p>
     
        
        <p>
            The MSAP Center’s Private Workspace is an extension of the MSAP Center's website.
            It was designed to provide information and resources that are tailored for the MSAP grantees.</p>
        <p>
            In the Private Workspace, you will find resources and tools to help you implement,
            manage, and evaluate your magnet program(s).</p>
            
           <div class="msapHP_blurbs">
                <p class="msapHPadmin">Quick Start</p>
       
           <div style="text-align: center; float: left; width: 106px;">
           <a href="TA_courses.aspx"><img src="../images/PuzzlePieces.gif" alt="a book icon"  style="border:0px;" /></a>
           <p><a href="TA_courses.aspx">Courses</a></p>
         </div>
          
          <%--<div id="ToolkitAppleID" runat="server" style="margin: 0 auto; text-align: center; width: 106px;">--%>
          <div style="text-align: center; float: left; width: 106px;">
			<a href="../admin/TA_toolkits.aspx" style="border:0px;">
            	 <img src="../images/ToolkitApple.gif" alt="an icon of a TA Toolkits"  style="border:0px;"/>
            </a>
            <p> <a href="../admin/TA_toolkits.aspx" >Toolkits</a></p>
          </div>

          <div style="text-align: center; float: left; width: 106px;">
        	<a href="../admin/past_webinars.aspx"><img src="../images/php_iconbook.gif" alt="book icon"  style="border:0px;" /></a>
           	<p><a href="../admin/past_webinars.aspx">Past Webinars</a></p>
    	  </div>
             <%--<div id="MAPSID" runat="server" style="text-align: center; float: left; width: 106px; margin-left: 0px;">--%>
             <div id="MAPSID" runat="server" visible="false" style="margin: 0 auto; text-align: center; width: 106px;">
			<a href="../admin/data/mygrantee.aspx" style="border:0px;">
            	<img src="../images/MAPS.gif" alt="an icon of a person"  style="border:0px;"/>
            </a>
            <p> <a href="../admin/data/mygrantee.aspx">MAPS</a></p>
          </div>
      </div>
    </div></div>
</asp:Content>
