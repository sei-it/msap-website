﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"%>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
Annual Performance Reporting 2012
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>Annual Performance Reporting 2012</h1>
    <h2>Annual Performance Reporting Guidance Webinar</h2>
<!--<div id="Div1" runat="server"></div>-->
    <div id="Div3" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr>
                <td width='90%'>
                    <b>File name</b>
                </td>
                <td width='10%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                    Annual Performance Reporting Guidance Presentation
                </td>
                <td width='10%'>
                    <a href='doc/APR_Files/APR_2012_Guidance_Presentation.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    Annual Performance Reporting Guidance Recording
                </td>
                <td width='10%'>
                  <a href='doc/APR_Files/2pm_APR_2012_recording.wmv' target='_blank'>Video</a>
                </td>
            </tr>
            <tr>
                <td>
                    Annual Performance Reporting Guidance Transcript
                </td>
                <td width='10%'>
                    <a href='doc/APR_Files/2pm_APR_2012_Transcript.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    Dear Colleague Letter
                </td>
                <td width='10%'>
                  <a href='doc/APR_Files/APR_2012_Dear_Colleague_Letter.pdf' target='_blank'>PDF</a>
                </td>
            </tr>
           
        </table>
        
        <h2>APR MAPS Demonstration Webinar</h2>
        <table width='100%' border='0' cellspacing='0' cellpadding='3' >
            <tr>
                <td width='90%'>
                    <b>File name</b>
                </td>
                <td width='10%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
          
            <tr>
                <td>
                    APR MAPS Demonstration Webinar Transcript
            </td>
                <td width='10%'>
                    <a href='doc/APR_Files/APR_2012_MAPS_Transcript.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    APR MAPS Demonstration Webinar Recording
            </td>
                <td width='10%'>
                  <a href='doc/APR_Files/MAPS_APR_2012_recording.wmv' target='_blank'>Video</a>
                </td>
            </tr>
           
        </table>
            
            <h2>Ad Hoc Reporting Guidance Webinar</h2>
            <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr>
                <td width='90%'>
                    <b>File name</b>
                </td>
                <td width='10%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                    Dear Colleague Letter
                </td>
                <td width='10%'>
                  <a href='doc/AdHoc_Files/Ad_Hoc_Dear_Colleague_2012.pdf' target='_blank'>PDF</a>
                </td>
            </tr>
           <tr  bgcolor='#E8E8E8'>
                <td>
                    Ad Hoc Reporting Guidance Presentation
                </td>
                <td width='10%'>
                    <a href='doc/AdHoc_Files/Ad_Hoc_2012_Webinar.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
           <tr>
                <td>
                    Guide to Reporting MSAP GPRA Measures
                </td>
                <td width='10%'>
                    <a href='doc/AdHoc_Files/MSAP_GPRA_Guide_AdHoc_2012.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
        	<tr  bgcolor='#E8E8E8'>
                <td>
                    MAPS User Guide
                </td>
                <td width='10%'>
                    <a href='doc/AdHoc_Files/MAPS_User_Guide_AdHoc_2012.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
            <tr>
                <td>
                    Ad Hoc Reporting Guidance Recording
                </td>
                <td width='10%'>
                    <a href='doc/AdHoc_Files/Ad_Hoc_2012_Guidance_Webinar_recording.wmv' target='_blank'>
                        Video</a>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Ad Hoc Reporting Guidance Transcript
                </td>
                <td width='10%'>
                    <a href='doc/AdHoc_Files/Ad_Hoc_Reporting_Guidance_Transcript_2012.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
        </table>
        
        <h2>Ad Hoc MAPS Demonstration Webinar</h2>
        <table width='100%' border='0' cellspacing='0' cellpadding='3' >
            <tr>
                <td width='90%'>
                    <b>File name</b>
                </td>
                <td width='10%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
          
            <tr>
                <td>
                    Ad Hoc MAPS Demonstration Webinar Transcript
              </td>
                <td width='10%'>
                    <a href='doc/AdHoc_Files/MAPS_091212_transcript.pdf' target='_blank'>
                        PDF</a>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    Ad Hoc MAPS Demonstration Webinar Recording
                </td>
                <td width='10%'>
                  <a href='doc/AdHoc_Files/MAPS_Demonstration_Webinar_recording_2012.wmv' target='_blank'>Video</a>
                </td>
            </tr>
           
      </table>
      <div style="padding-top: 40px;float:left;"><a href="webinars_2013.aspx"><strong>&lt;&lt; Annual Performance Reporting 2013</strong></a></div>
        <div style="text-align: right; padding-top: 40px;"><a href="webinars.aspx"><strong>Annual Performance Reporting 2011 &gt;&gt;</strong></a></div>
    </div>
</asp:Content>
