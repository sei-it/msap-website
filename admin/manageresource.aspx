﻿<%@ Page Title="Manage Resource" Language="C#" MasterPageFile="~/admin/magnetadmin.master"
    AutoEventWireup="true" CodeFile="manageresource.aspx.cs" Inherits="admin_manageresource" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Publications
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Manage Resources</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p style="text-align: left">
        <asp:Button ID="Newbutton" runat="server" Text="New Resource" CssClass="msapBtn"
            OnClick="OnAddPublication" />
        <asp:Button ID="btnNewTopic" runat="server" CssClass="msapBtn" Text="New Topic" 
            onclick="btnNewTopic_Click" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        OnPageIndexChanging="OnGridViewPageIndexChanged">
        <Columns>
            <asp:BoundField DataField="PublicationTitle" SortExpression="" HeaderText="Publication" />
            <asp:BoundField DataField="PublicationType" SortExpression="" HeaderText="Type" />
            <asp:BoundField DataField="PublicationKeyword" SortExpression="" HeaderText="Keywords" />
            <asp:BoundField DataField="Organization" SortExpression="" HeaderText="Organization" />
            <asp:BoundField DataField="PublicationDate" SortExpression="" HeaderText="Publication Date" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hfID" runat="server" />
    <%-- Resource --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Resource</div>
            <table>
                <tr>
                    <td>
                        Publication Title:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPublicationName" runat="server" MaxLength="5000" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Publication Type:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPublicationType" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Publication Sub Type:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSubType" runat="server">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="1">Webinar</asp:ListItem>
                            <asp:ListItem Value="2">video</asp:ListItem>
                            <asp:ListItem Value="3">Audio</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                      Publication Sub Topic:
                    </td>
                    <td>
                            <asp:CheckBoxList ID="ddlTopics" runat="server" DataSourceID="dsPulicationTopic" RepeatColumns="2" DataTextField="topicname" DataValueField="id" RepeatDirection="Vertical">
                                                            </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Keywords:
                    </td>
                    <td>
                        <asp:TextBox ID="txtKeywords" runat="server" MaxLength="1000" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Organization:
                    </td>
                    <td>
                        <asp:TextBox ID="txtOrganization" runat="server" MaxLength="3000" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Publication Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPublicationDate" runat="server" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description:
                    </td>
                    <td>
                        <cc1:Editor id="txtDescription" runat="server" width="470" height="200">
                                </cc1:Editor>
                    </td>
                </tr>
                <tr>
                    <td>
                        Document:
                    </td>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        &nbsp;<asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSavePublication" />
                    </td>
                    <td>
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <asp:LinkButton ID="btnDump" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />

  <%--  new topic--%>
  <asp:Panel ID="pnlNewTopic" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                New Topic</div>
            <table>
                <tr>
                    <td>
                        New Topic:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewTopic" runat="server" MaxLength="5000" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        New Topic Description:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewTopicDes" runat="server" TextMode="MultiLine" Height="120px" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        &nbsp;<asp:Label ID="Label1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSaveTopic" runat="server" CssClass="msapBtn" Text="Save Topic" OnClick="OnSaveTopic" />
                    </td>
                    <td>
                        <asp:Button ID="btnClossWindow" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

  <ajax:ModalPopupExtender ID="mpeTopicWindow" runat="server" TargetControlID="btnDump"
        PopupControlID="pnlNewTopic" DropShadow="true" OkControlID="btnClossWindow" CancelControlID="btnClossWindow"
        BackgroundCssClass="magnetMPE" Y="20" />

  <asp:SqlDataSource ID="dsPulicationTopic" runat="server" 
    ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
    SelectCommand="SELECT * FROM [MagnetPublicationSubTopic]"></asp:SqlDataSource>
</asp:Content>
