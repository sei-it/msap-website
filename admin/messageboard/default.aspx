﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="default.aspx.cs" Inherits="admin_messageboard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center Message Board
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <h1>
            Magnet Connector
      </h1>
        <p>
            Click on a discussion topic to read, start, or join a conversation. These moderated discussions will help you create communities of practice (CoPs) around issues that matter to magnet schools. The Magnet Connector is a safe and secure environment for working with each other to solve persistent problems or to improve practice in areas that are important to the success of your magnet project.</p>
    </div>
    
    <YAF:Forum runat="server" ID="forum"></YAF:Forum>
</asp:Content>
