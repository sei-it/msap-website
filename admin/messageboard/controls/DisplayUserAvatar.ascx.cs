﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Globalization;
using System.Collections.Specialized;
using YAF.Classes;
using YAF.Classes.Core;
using YAF.Classes.Utils;

namespace YAF.Controls
{
    public partial class DisplayUsersAvatar : YAF.Classes.Core.BaseUserControl
    {
        private int CurrentUserID; 
        //private bool AdminEditMode = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            //PageContext.QueryIDs = new QueryStringIDHelper("u");

            //if (AdminEditMode && PageContext.IsAdmin && this.PageContext.QueryIDs.ContainsKey("u"))
            //{
            //    CurrentUserID = (int)PageContext.QueryIDs["u"];
            //}
            //else
            //{
                CurrentUserID = PageContext.PageUserID;
            //}

            if (!IsPostBack)
            {
                //// check if it's a link from the avatar picker
                //if (Request.QueryString["av"] != null)
                //{
                //    // save the avatar right now...
                //    YAF.Classes.Data.DB.user_saveavatar(CurrentUserID, string.Format("{0}{1}/{2}", YafForumInfo.ForumBaseUrl, YafBoardFolders.Current.Avatars, Request.QueryString["av"]), null, null);
                //}

                //UpdateRemote.Text = PageContext.Localization.GetText("COMMON", "UPDATE");
                //UpdateUpload.Text = PageContext.Localization.GetText("COMMON", "UPDATE");
                //Back.Text = PageContext.Localization.GetText("COMMON", "BACK");

                NoAvatar.Text = PageContext.Localization.GetText("CP_EDITAVATAR", "NOAVATAR");

                //DeleteAvatar.Text = PageContext.Localization.GetText("CP_EDITAVATAR", "AVATARDELETE");
                //DeleteAvatar.Attributes["onclick"] = string.Format("return confirm('{0}?')", PageContext.Localization.GetText("CP_EDITAVATAR", "AVATARDELETE"));

                string addAdminParam = "";
                //if (AdminEditMode) addAdminParam = "u=" + CurrentUserID.ToString();

                //OurAvatar.NavigateUrl = YafBuildLink.GetLinkNotEscaped(ForumPages.avatar, addAdminParam);
                //OurAvatar.Text = PageContext.Localization.GetText("CP_EDITAVATAR", "OURAVATAR_SELECT");
            }

            BindData();
        }

        private void BindData()
        {
            DataRow row;

            using (DataTable dt = YAF.Classes.Data.DB.user_list(PageContext.PageBoardID, CurrentUserID, null))
            {
                row = dt.Rows[0];
            }

            AvatarImg.Visible = true;
            //Avatar.Text = "";
            //DeleteAvatar.Visible = false;
            NoAvatar.Visible = false;

            if (PageContext.BoardSettings.AvatarUpload && row["HasAvatarImage"] != null && long.Parse(row["HasAvatarImage"].ToString()) > 0)
            {
                AvatarImg.ImageUrl = String.Format("{0}resource.ashx?u={1}", YafForumInfo.ForumRoot, CurrentUserID);
                //Avatar.Text = "";
               // DeleteAvatar.Visible = true;
            }
            else if (row["Avatar"].ToString().Length > 0) // Took out PageContext.BoardSettings.AvatarRemote
            {
                AvatarImg.ImageUrl = String.Format("{3}resource.ashx?url={0}&width={1}&height={2}",
                    Server.UrlEncode(row["Avatar"].ToString()),
                    PageContext.BoardSettings.AvatarWidth,
                    PageContext.BoardSettings.AvatarHeight,
                    YafForumInfo.ForumRoot);

                //Avatar.Text = row["Avatar"].ToString();
               // DeleteAvatar.Visible = true;
            }
            else if (PageContext.BoardSettings.AvatarGravatar)
            {
                System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] bs = System.Text.Encoding.UTF8.GetBytes(PageContext.User.Email);
                bs = x.ComputeHash(bs);
                System.Text.StringBuilder s = new System.Text.StringBuilder();
                foreach (byte b in bs)
                {
                    s.Append(b.ToString("x2").ToLower());
                }
                string emailHash = s.ToString();

                string gravatarUrl = "http://www.gravatar.com/avatar/" + emailHash + ".jpg?r=" + PageContext.BoardSettings.GravatarRating;

                AvatarImg.ImageUrl = String.Format("{3}resource.ashx?url={0}&width={1}&height={2}",
                Server.UrlEncode(gravatarUrl),
                PageContext.BoardSettings.AvatarWidth,
                PageContext.BoardSettings.AvatarHeight,
                YafForumInfo.ForumRoot);

                NoAvatar.Text = "Gravatar Image";
                NoAvatar.Visible = true;
            }
            else
            {
                AvatarImg.ImageUrl = "../images/noavatar.gif";
                NoAvatar.Visible = true;
            }

            int rowSpan = 2;

            //AvatarUploadRow.Visible = (AdminEditMode ? true : PageContext.BoardSettings.AvatarUpload);
            //AvatarRemoteRow.Visible = (AdminEditMode ? true : PageContext.BoardSettings.AvatarRemote);

            //if (AdminEditMode || PageContext.BoardSettings.AvatarUpload) rowSpan++;
            //if (AdminEditMode || PageContext.BoardSettings.AvatarRemote) rowSpan++;

            avatarImageTD.RowSpan = rowSpan;
        }

       
       
        
    }
}