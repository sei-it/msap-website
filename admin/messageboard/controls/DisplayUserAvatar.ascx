﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DisplayUserAvatar.ascx.cs" Inherits="YAF.Controls.DisplayUsersAvatar" %>
<table width="100%" class="content" cellspacing="1" cellpadding="4">
   <tr>
        <td class="post" align="center" rowspan="4" runat="server" id="avatarImageTD">
            <asp:Image ID="AvatarImg" runat="server" Visible="true" AlternateText="Avatar Image" />
            <br />
            <br />
            <asp:Label runat="server" ID="NoAvatar" Visible="false" />
         </td>
    </tr>
</table>