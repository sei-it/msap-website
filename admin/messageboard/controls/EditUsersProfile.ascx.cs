/* Yet Another Forum.NET
 * Copyright (C) 2006-2009 Jaben Cargman
 * http://www.yetanotherforum.net/
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Linq;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Globalization;
using System.Collections.Specialized;
using YAF.Classes.Core;
using YAF.Classes;
using YAF.Classes.Utils;

namespace YAF.Controls
{
	public partial class EditUsersProfile : YAF.Classes.Core.BaseUserControl
	{
		private int CurrentUserID;
		private bool AdminEditMode = false;

        private UserProfileDataContext db = new UserProfileDataContext();
        yaf_prov_Profile userData;

        private UsersDataContext userDB = new UsersDataContext();
        yaf_User userInfo;


		protected void Page_Load( object sender, EventArgs e )
		{
			PageContext.QueryIDs = new QueryStringIDHelper( "u" );

			if ( AdminEditMode && PageContext.IsAdmin && this.PageContext.QueryIDs.ContainsKey( "u" ) )
			{
				CurrentUserID = ( int )this.PageContext.QueryIDs ["u"];
			}
			else
			{
				CurrentUserID = PageContext.PageUserID;
			}

			if ( !IsPostBack )
			{
				LoginInfo.Visible = true;

				// Begin Modifications for enhanced profile
				Gender.Items.Add( PageContext.Localization.GetText( "PROFILE", "gender0" ) );
				Gender.Items.Add( PageContext.Localization.GetText( "PROFILE", "gender1" ) );
				Gender.Items.Add( PageContext.Localization.GetText( "PROFILE", "gender2" ) );
				// End Modifications for enhanced profile				

				UpdateProfile.Text = PageContext.Localization.GetText( "COMMON", "SAVE" );
				Cancel.Text = PageContext.Localization.GetText( "COMMON", "CANCEL" );

				ForumSettingsRows.Visible = PageContext.BoardSettings.AllowUserTheme || PageContext.BoardSettings.AllowUserLanguage || PageContext.BoardSettings.AllowPMEmailNotification;
				UserThemeRow.Visible = PageContext.BoardSettings.AllowUserTheme;
				UserLanguageRow.Visible = PageContext.BoardSettings.AllowUserLanguage;
				PMNotificationRow.Visible = PageContext.BoardSettings.AllowPMEmailNotification;
				MetaWeblogAPI.Visible = PageContext.BoardSettings.AllowPostToBlog;
                LoginInfo.Visible = PageContext.BoardSettings.AllowEmailChange;

				BindData();
			}
		}
		private void BindData()
		{
			TimeZones.DataSource = StaticDataHelper.TimeZones();
			Theme.DataSource = StaticDataHelper.Themes();
			Theme.DataTextField = "Theme";
			Theme.DataValueField = "FileName";
			Language.DataSource = StaticDataHelper.Languages();
			Language.DataTextField = "Language";
			Language.DataValueField = "FileName";

			DataBind();

			// get an instance of the combined user data class.
			//CombinedUserDataHelper userData = new CombinedUserDataHelper( CurrentUserID );
            var query = from p in db.GetTable<yaf_prov_Profile>()
                        where p.UserID == CurrentUserID.ToString()
                        select p;


            if (query.Count()>0)
            {
                userData = db.yaf_prov_Profiles.SingleOrDefault(x => x.UserID == CurrentUserID.ToString());
                 if (!string.IsNullOrEmpty(userData.Location))
                    ddlState.Items.FindByValue(Convert.ToString(userData.Location)).Selected = true;
                HomePage.Text = userData.Homepage;

                this.txtFirstName.Text = userData.FirstName;
                this.txtLastName.Text = userData.LastName;

                Realname.Text = userData.RealName;
                Occupation.Text = userData.Occupation;
                Interests.Text = userData.Interests;
                Weblog.Text = userData.Blog;
                WeblogUrl.Text = userData.BlogServiceUrl;
                WeblogID.Text = userData.BlogServicePassword;
                WeblogUsername.Text = userData.BlogServiceUsername;
                //MSN.Text = userData.Profile.MSN;
                //YIM.Text = userData.Profile.YIM;
                //AIM.Text = userData.Profile.AIM;
                //ICQ.Text = userData.Profile.ICQ;
                //Skype.Text = userData.Profile.Skype;

                Gender.SelectedIndex =userData.Gender==null? 0: (int)userData.Gender;
            }

            if (userDB.yaf_Users.Count() > 0)
            {
                userInfo = userDB.yaf_Users.Single(u => u.UserID == CurrentUserID);

                Email.Text = userInfo.Email;

                PMNotificationEnabled.Checked = userInfo.PMNotification;

                ListItem timeZoneItem = TimeZones.Items.FindByValue(userInfo.TimeZone.ToString());
                if (timeZoneItem != null) timeZoneItem.Selected = true;
            }

			OverrideForumThemeRow.Visible = PageContext.BoardSettings.AllowUserTheme;

            //if ( PageContext.BoardSettings.AllowUserTheme )
            //{
            //    // Allows to use different per-forum themes,
            //    // While "Allow User Change Theme" option in hostsettings is true
            //    string themeFile = PageContext.BoardSettings.Theme;
            //    if ( userData.ThemeFile != null ) themeFile = userData.ThemeFile;
				
            //    ListItem themeItem = Theme.Items.FindByValue( themeFile );
            //    if (themeItem != null) themeItem.Selected = true;

            //    OverrideDefaultThemes.Checked = userData.OverrideDefaultThemes;
            //}

            //if ( PageContext.BoardSettings.AllowUserLanguage )
            //{
            //    string languageFile = PageContext.BoardSettings.Language;
            //    if ( userData.LanguageFile != string.Empty ) languageFile = userData.LanguageFile;

            //    ListItem foundItem = Language.Items.FindByValue( languageFile );
            //    if ( foundItem != null ) foundItem.Selected = true;
            //}
		}

		protected void UpdateProfile_Click( object sender, System.EventArgs e )
		{
			if ( HomePage.Text.Length > 0 && !HomePage.Text.StartsWith( "http://" ) )
				HomePage.Text = "http://" + HomePage.Text;

            //if ( MSN.Text.Length > 0 && !ValidationHelper.IsValidEmail( MSN.Text ) )
            //{
            //    PageContext.AddLoadMessage( PageContext.Localization.GetText( "PROFILE", "BAD_MSN" ) );
            //    return;
            //}
			if ( HomePage.Text.Length > 0 && !ValidationHelper.IsValidURL( HomePage.Text ) )
			{
				PageContext.AddLoadMessage( PageContext.Localization.GetText( "PROFILE", "BAD_HOME" ) );
				return;
			}
			if ( Weblog.Text.Length > 0 && !ValidationHelper.IsValidURL( Weblog.Text ) )
			{
				PageContext.AddLoadMessage( PageContext.Localization.GetText( "PROFILE", "BAD_WEBLOG" ) );
				return;
			}
            //if ( ICQ.Text.Length > 0 && !ValidationHelper.IsValidInt( ICQ.Text ) )
            //{
            //    PageContext.AddLoadMessage( PageContext.Localization.GetText( "PROFILE", "BAD_ICQ" ) );
            //    return;
            //}

			if ( UpdateEmailFlag )
			{
				string newEmail = Email.Text.Trim();

				if ( !ValidationHelper.IsValidEmail( newEmail ) )
				{
					PageContext.AddLoadMessage( PageContext.Localization.GetText( "PROFILE", "BAD_EMAIL" ) );
					return;
				}

				if ( PageContext.BoardSettings.EmailVerification )
				{
					string hashinput = DateTime.Now.ToString() + Email.Text + Security.CreatePassword( 20 );
					string hash = FormsAuthentication.HashPasswordForStoringInConfigFile( hashinput, "md5" );

					// Create Email
					YafTemplateEmail changeEmail = new YafTemplateEmail( "CHANGEEMAIL" );

					changeEmail.TemplateParams ["{user}"] = PageContext.PageUserName;
					changeEmail.TemplateParams ["{link}"] = String.Format( "{0}\r\n\r\n", YafBuildLink.GetLinkNotEscaped( ForumPages.approve, true, "k={0}", hash ) );
					changeEmail.TemplateParams ["{newemail}"] = Email.Text;
					changeEmail.TemplateParams ["{key}"] = hash;
					changeEmail.TemplateParams ["{forumname}"] = PageContext.BoardSettings.Name;
					changeEmail.TemplateParams ["{forumlink}"] = YafForumInfo.ForumURL;

					// save a change email reference to the db
					YAF.Classes.Data.DB.checkemail_save( CurrentUserID, hash, newEmail );

					//  send a change email message...
					changeEmail.SendEmail( new System.Net.Mail.MailAddress( newEmail ), PageContext.Localization.GetText( "COMMON", "CHANGEEMAIL_SUBJECT" ), true );

					// show a confirmation
					PageContext.AddLoadMessage( String.Format( PageContext.Localization.GetText( "PROFILE", "mail_sent" ), Email.Text ) );
				}
				else
				{
					// just update the e-mail...
					UserMembershipHelper.UpdateEmail( CurrentUserID, Email.Text.Trim() );
				}
			}

			string userName = UserMembershipHelper.GetUserNameFromID( CurrentUserID );

			//YafUserProfile userProfile = YafUserProfile.GetProfile( userName );

            var query = from p in db.GetTable<yaf_prov_Profile>()
                        where p.UserID == CurrentUserID.ToString()
                        select p;
            //add new record for user profile
            if (db.yaf_prov_Profiles.Count()==0 || query.Count() ==0)
            {
               userData = setRecord();
               
               userData.UserID = CurrentUserID.ToString();
               db.yaf_prov_Profiles.InsertOnSubmit(userData);
               db.SubmitChanges();
            }
            else  //update current user record
            {
                yaf_prov_Profile   userProfile = db.yaf_prov_Profiles.Single(p => p.UserID == CurrentUserID.ToString());
                userProfile.Location = ddlState.SelectedValue.Trim();
                userProfile.Homepage = HomePage.Text.Trim();
                //userProfile.MSN = MSN.Text.Trim();
                //userProfile.YIM = YIM.Text.Trim();
                //userProfile.AIM = AIM.Text.Trim();
                //userProfile.ICQ = ICQ.Text.Trim();
                //userProfile.Skype = Skype.Text.Trim();

                userProfile.FirstName = this.txtFirstName.Text.Trim();
                userProfile.LastName = this.txtLastName.Text.Trim();

                userProfile.LastUpdatedDate = DateTime.Now;
                userProfile.RealName = Realname.Text.Trim();
                userProfile.Occupation = Occupation.Text.Trim();
                userProfile.Interests = Interests.Text.Trim();
                userProfile.Gender = Gender.SelectedIndex;
                userProfile.Blog = Weblog.Text.Trim();
                userProfile.BlogServiceUrl = WeblogUrl.Text.Trim();
                userProfile.BlogServiceUsername = WeblogUsername.Text.Trim();
                userProfile.BlogServicePassword = WeblogID.Text.Trim();
                userProfile.UserID = CurrentUserID.ToString();
                db.SubmitChanges();
            }

           
			//userProfile.Save();

			// save remaining settings to the DB
			YAF.Classes.Data.DB.user_save( CurrentUserID, PageContext.PageBoardID, null, null,
				Convert.ToInt32( TimeZones.SelectedValue ), Language.SelectedValue, Theme.SelectedValue, OverrideDefaultThemes.Checked, null, PMNotificationEnabled.Checked );

            userInfo = userDB.yaf_Users.Single(u => u.UserID == CurrentUserID);
            userInfo.TimeZone = Convert.ToInt32(TimeZones.SelectedValue);
            userInfo.PMNotification = PMNotificationEnabled.Checked;
            userInfo.Email = Email.Text;

            userDB.SubmitChanges();
            
            
            if ( !AdminEditMode )
			{
				YafBuildLink.Redirect( ForumPages.cp_profile );
			}
			else
			{
				BindData();
			}
		}

        private yaf_prov_Profile setRecord()
        {
            yaf_prov_Profile userProfile = new yaf_prov_Profile();

            userProfile.LastUpdatedDate = DateTime.Now;
            userProfile.Location = ddlState.SelectedValue.Trim();
            userProfile.Homepage = HomePage.Text.Trim();
            //userProfile.MSN = MSN.Text.Trim();
            //userProfile.YIM = YIM.Text.Trim();
            //userProfile.AIM = AIM.Text.Trim();
            //userProfile.ICQ = ICQ.Text.Trim();
            //userProfile.Skype = Skype.Text.Trim();

            userProfile.FirstName = this.txtFirstName.Text.Trim();
            userProfile.LastName = this.txtLastName.Text.Trim();

            userProfile.RealName = Realname.Text.Trim();
            userProfile.Occupation = Occupation.Text.Trim();
            userProfile.Interests = Interests.Text.Trim();
            userProfile.Gender = Gender.SelectedIndex;
            userProfile.Blog = Weblog.Text.Trim();
            userProfile.BlogServiceUrl = WeblogUrl.Text.Trim();
            userProfile.BlogServiceUsername = WeblogUsername.Text.Trim();
            userProfile.BlogServicePassword = WeblogID.Text.Trim();
            userProfile.UserID = CurrentUserID.ToString();

            return userProfile;

        }

		protected void Cancel_Click( object sender, System.EventArgs e )
		{
			if ( AdminEditMode )
				YafBuildLink.Redirect( ForumPages.admin_users );
			else
				YafBuildLink.Redirect( ForumPages.cp_profile );
		}

		protected void Email_TextChanged( object sender, System.EventArgs e )
		{
			UpdateEmailFlag = true;
		}

		protected bool UpdateEmailFlag
		{
			get { return ViewState ["bUpdateEmail"] != null ? Convert.ToBoolean( ViewState ["bUpdateEmail"] ) : false; }
			set { ViewState ["bUpdateEmail"] = value; }
		}

		public bool InAdminPages
		{
			get
			{
				return AdminEditMode;
			}
			set
			{
				AdminEditMode = value;
			}
		}

	}
}