<%@ Control Language="c#" AutoEventWireup="True" CodeFile="DisplayPost.ascx.cs" Inherits="YAF.Controls.DisplayPost"
    EnableViewState="false" %>
<%@ Import Namespace="YAF.Classes.Core" %>
<%@ Register TagPrefix="AVT" Namespace="YAF.Controls" Assembly="YAF.Controls" %>
<%@ Register TagPrefix="YAF" TagName="ProfileEdit" Src="../controls/EditUsersAvatar.ascx" %>
<tr class="postheader">
    <%#GetIndentCell()%>
    <td id="NameCell" class="postUser" runat="server" colspan="3">
        <div class="leftItem postedLeft">
            <b><a href='<%# YAF.Classes.Utils.YafBuildLink.GetLink(ForumPages.posts,"m={0}#post{0}",DataRow["MessageID"]) %>'>
                #<%# Convert.ToInt32((DataRow["Position"]))+1 %></a>
                <YAF:LocalizedLabel ID="LocalizedLabel1" runat="server" LocalizedTag="POSTED" />
                :</b>
            <%# YafServices.DateTime.FormatDateTime((System.DateTime)DataRow["Posted"]) %>
            <br />
            <b>Posted By:</b> <a name="post<%# DataRow["MessageID"] %>" />
            <YAF:UserLink ID="UserProfileLink" runat="server" UserID='<%#DataRow["UserID"]%>'
                UserName='<%#DataRow["UserName"]%>' />
        </div>
        <div class="rightItem postedRight" style="width: 200px; float: right;">
            <YAF:ThemeButton ID="Attach" runat="server" CssClass="yaflittlebutton" TextLocalizedTag="BUTTON_ATTACH"
                TitleLocalizedTag="BUTTON_ATTACH_TT" />
            <YAF:ThemeButton ID="Edit" runat="server" CssClass="yaflittlebutton" TextLocalizedTag="BUTTON_EDIT"
                TitleLocalizedTag="BUTTON_EDIT_TT" />
            <YAF:ThemeButton ID="MovePost" runat="server" CssClass="yaflittlebutton" TextLocalizedTag="BUTTON_MOVE"
                TitleLocalizedTag="BUTTON_MOVE_TT" />
            <YAF:ThemeButton ID="Delete" runat="server" CssClass="yaflittlebutton" TextLocalizedTag="BUTTON_DELETE"
                TitleLocalizedTag="BUTTON_DELETE_TT" />
            <YAF:ThemeButton ID="UnDelete" runat="server" CssClass="yaflittlebutton" TextLocalizedTag="BUTTON_UNDELETE"
                TitleLocalizedTag="BUTTON_UNDELETE_TT" />
            <YAF:ThemeButton ID="Quote" runat="server" CssClass="yaflittlebutton" TextLocalizedTag="BUTTON_QUOTE"
                TitleLocalizedTag="BUTTON_QUOTE_TT" />
        </div>
    </td>
</tr>
<tr class="<%#GetPostClass()%>">
    <td width="15%"  style="border-right:1px solid #A9DEF2;">
        <div>
            <AVT:FirstControl ID="AvatarBox" runat="server" Visible="<%# !IsSponserMessage %>"
                PageCache="<%# ParentPage.PageCache %>" DataRow="<%# DataRow %>">
            </AVT:FirstControl>
            <div>
                <%# getFirstLastName()%></div>
        </div>
    </td>
    <%--<td width="1" bgcolor="#00FFFF"><br/></td>--%>
    <td width="71%" valign="top" height="<%# GetUserBoxHeight() %>" class="message" >
        <%--<td valign="top" height="<%# GetUserBoxHeight() %>"  class="message" colspan="<%#GetIndentSpan()%>">--%>
        <%--<div class="postAvatar" style="width: 150px; height: 100px">
       <YAF:ProfileEdit runat="server" ID="ProfileEditor" />
       </div>--%>
        <div class="postdiv1" >
            <asp:Panel ID="panMessage" runat="server">
                <YAF:MessagePostData ID="MessagePost1" runat="server" DataRow="<%# DataRow %>">
                </YAF:MessagePostData>
            </asp:Panel>
        </div>
    </td>
</tr>
<tr class="postfooter">
    <td class="small postTop" colspan='<%#GetIndentSpan()%>'>
        <div class="leftItem postInfoLeft">
            <YAF:ThemeButton ID="Pm" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="PM" ImageThemeTag="PM" />
            <YAF:ThemeButton ID="btnTogglePost" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="TOGGLEPOST" Visible="false" />
            <!--<YAF:ThemeButton ID="Email" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                        TextLocalizedTag="EMAIL" ImageThemeTag="EMAIL" />-->
            <YAF:ThemeButton ID="Home" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="HOME" ImageThemeTag="HOME" />
            <YAF:ThemeButton ID="Blog" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="BLOG" ImageThemeTag="BLOG" />
            <YAF:ThemeButton ID="Msn" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="MSN" ImageThemeTag="MSN" />
            <YAF:ThemeButton ID="Aim" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="AIM" ImageThemeTag="AIM" />
            <YAF:ThemeButton ID="Yim" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="YIM" ImageThemeTag="YIM" />
            <YAF:ThemeButton ID="Icq" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="ICQ" ImageThemeTag="ICQ" />
            <YAF:ThemeButton ID="Skype" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
                TextLocalizedTag="SKYPE" ImageThemeTag="SKYPE" />
        </div>
        <!--  <div class="rightItem postInfoRight">
           &nbsp;<asp:LinkButton ID="ReportButton" CommandName="ReportAbuse" CommandArgument='<%# DataRow["MessageID"] %>'
                runat="server"></asp:LinkButton>
            |
            <asp:LinkButton ID="ReportSpamButton" CommandName="ReportSpam" CommandArgument='<%# DataRow["MessageID"] %>'
                runat="server"></asp:LinkButton>
            <span id="AdminInformation" runat="server" class="smallfont"></span>
        </div>-->
    </td>
    <td class="postfooter" style="float: right; text-align: right;">
        <a href="javascript:scroll(0,0)">
            <YAF:LocalizedLabel ID="LocalizedLabel2" runat="server" LocalizedTag="TOP" />
        </a>
    </td>
</tr>
<tr class="postsep">
    <td colspan="3">
        <YAF:PopMenu runat="server" ID="PopMenu1" Control="UserName" />
    </td>
</tr>
