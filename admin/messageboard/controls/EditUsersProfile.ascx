<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUsersProfile.ascx.cs"
    Inherits="YAF.Controls.EditUsersProfile" %>
<table width="100%" class="content" cellspacing="1" cellpadding="4">
    <tr>
        <td class="header1" colspan="2">
            <YAF:LocalizedLabel runat="server" LocalizedPage="CP_EDITPROFILE" LocalizedTag="title" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="header2">
            <b>
                <YAF:LocalizedLabel ID="LocalizedLabel1" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="aboutyou" />
            </b>
        </td>
    </tr>
    <tr>
        <td class="postheader" style="width:50%">
            <YAF:LocalizedLabel ID="LocalizedLabel38" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="FIRSTNAME" />
        </td>
        <td class="post">
            <asp:TextBox ID="txtFirstName" runat="server" CssClass="edit" />
        </td>
     </tr>
	 <tr>
        <td class="postheader" style="width:50%">
            <YAF:LocalizedLabel ID="LocalizedLabel39" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="LASTNAME" />
        </td>
        <td class="post">
            <asp:TextBox ID="txtLastName" runat="server" CssClass="edit" />
        </td>
     </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel2" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="realname2" Visible="false" />
        </td>
        <td class="post">
            <asp:TextBox ID="Realname" runat="server" CssClass="edit" Visible="false" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel3" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="occupation" />
        </td>
        <td class="post">
            <asp:TextBox ID="Occupation" runat="server" CssClass="edit" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel4" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="interests" />
        </td>
        <td class="post">
            <asp:TextBox ID="Interests" runat="server" CssClass="edit" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel5" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="gender" />
        </td>
        <td class="post">
            <asp:DropDownList ID="Gender" runat="server" CssClass="edit" /></td>
    </tr>
    <tr>
        <td colspan="2" class="header2">
            <b>
                <YAF:LocalizedLabel ID="LocalizedLabel6" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="location" />
            </b>
        </td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel7" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="where" />
        </td>
        <td class="post">
    <asp:DropDownList ID="ddlState" runat="server">
    <asp:ListItem Value="">Select a State</asp:ListItem>
    <asp:ListItem value="AL">Alabama</asp:ListItem>
    <asp:ListItem value="AK">Alaska</asp:ListItem>
    <asp:ListItem value="AZ">Arizona</asp:ListItem>
    <asp:ListItem value="AR">Arkansas</asp:ListItem>
    <asp:ListItem value="CA">California</asp:ListItem>
    <asp:ListItem value="CO">Colorado</asp:ListItem>
    <asp:ListItem value="CT">Connecticut</asp:ListItem>
    <asp:ListItem value="DC">D.C.</asp:ListItem>
    <asp:ListItem value="DE">Delaware</asp:ListItem>
    <asp:ListItem value="FL">Florida</asp:ListItem>
    <asp:ListItem value="GA">Georgia</asp:ListItem>
    <asp:ListItem value="HI">Hawaii</asp:ListItem>
    <asp:ListItem value="ID">Idaho</asp:ListItem>
    <asp:ListItem value="IL">Illinois</asp:ListItem>
    <asp:ListItem value="IN">Indiana</asp:ListItem>
    <asp:ListItem value="IA">Iowa</asp:ListItem>
    <asp:ListItem value="KS">Kansas</asp:ListItem>
    <asp:ListItem value="KY">Kentucky</asp:ListItem>
    <asp:ListItem value="LA">Louisiana</asp:ListItem>
    <asp:ListItem value="ME">Maine</asp:ListItem>
    <asp:ListItem value="MD">Maryland</asp:ListItem>
    <asp:ListItem value="MA">Massachusetts</asp:ListItem>
    <asp:ListItem value="MI">Michigan</asp:ListItem>
    <asp:ListItem value="MN">Minnesota</asp:ListItem>
    <asp:ListItem value="MS">Mississippi</asp:ListItem>
    <asp:ListItem value="MO">Missouri</asp:ListItem>
    <asp:ListItem value="MT">Montana</asp:ListItem>
    <asp:ListItem value="NE">Nebraska</asp:ListItem>
    <asp:ListItem value="NV">Nevada</asp:ListItem>
    <asp:ListItem value="NH">New Hampshire</asp:ListItem>
    <asp:ListItem value="NJ">New Jersey</asp:ListItem>
    <asp:ListItem value="NM">New Mexico</asp:ListItem>
    <asp:ListItem value="NY">New York</asp:ListItem>
    <asp:ListItem value="NC">North Carolina</asp:ListItem>
    <asp:ListItem value="ND">North Dakota</asp:ListItem>
    <asp:ListItem value="OH">Ohio</asp:ListItem>
    <asp:ListItem value="OK">Oklahoma</asp:ListItem>
    <asp:ListItem value="OR">Oregon</asp:ListItem>
    <asp:ListItem value="PA">Pennsylvania</asp:ListItem>
    <asp:ListItem value="RI">Rhode Island</asp:ListItem>
    <asp:ListItem value="SC">South Carolina</asp:ListItem>
    <asp:ListItem value="SD">South Dakota</asp:ListItem>
    <asp:ListItem value="TN">Tennessee</asp:ListItem>
    <asp:ListItem value="TX">Texas</asp:ListItem>
    <asp:ListItem value="UT">Utah</asp:ListItem>
    <asp:ListItem value="VT">Vermont</asp:ListItem>
    <asp:ListItem value="VA">Virginia</asp:ListItem>
    <asp:ListItem value="WA">Washington</asp:ListItem>
    <asp:ListItem value="WV">West Virginia</asp:ListItem>
    <asp:ListItem value="WI">Wisconsin</asp:ListItem>
    <asp:ListItem value="WY">Wyoming</asp:ListItem>
</asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="header2">
            <b>
                <YAF:LocalizedLabel ID="LocalizedLabel8" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="homepage" />
            </b>
        </td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel9" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="homepage2" />
        </td>
        <td class="post">
            <asp:TextBox runat="server" ID="HomePage" CssClass="edit" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel10" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="weblog2" />
        </td>
        <td class="post">
            <asp:TextBox runat="server" ID="Weblog" CssClass="edit" /></td>
    </tr>
    <asp:PlaceHolder runat="server" ID="MetaWeblogAPI" Visible="true">
        <tr>
            <td colspan="2" class="header2">
                <b>
                    <YAF:LocalizedLabel ID="LocalizedLabel11" runat="server" LocalizedPage="CP_EDITPROFILE"
                        LocalizedTag="METAWEBLOG_TITLE" />
                </b>
            </td>
        </tr>
        <tr>
            <td class="postheader">
                <YAF:LocalizedLabel ID="LocalizedLabel12" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="METAWEBLOG_API_URL" />
            </td>
            <td class="post">
                <asp:TextBox runat="server" ID="WeblogUrl" CssClass="edit" /></td>
        </tr>
        <tr>
            <td class="postheader">
                <YAF:LocalizedLabel ID="LocalizedLabel13" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="METAWEBLOG_API_ID" />
                <br />
                <YAF:LocalizedLabel ID="LocalizedLabel14" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="METAWEBLOG_API_ID_INSTRUCTIONS" />
            </td>
            <td class="post">
                <asp:TextBox runat="server" ID="WeblogID" CssClass="edit" /></td>
        </tr>
        <tr>
            <td class="postheader">
                <YAF:LocalizedLabel ID="LocalizedLabel15" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="METAWEBLOG_API_USERNAME" />
            </td>
            <td class="post">
                <asp:TextBox runat="server" ID="WeblogUsername" CssClass="edit" /></td>
        </tr>
    </asp:PlaceHolder>
  <%--  <tr>
        <td colspan="2" class="header2">
            <b>
                <YAF:LocalizedLabel ID="LocalizedLabel16" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="messenger" />
            </b>
        </td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel29" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="msn" />
        </td>
        <td class="post">
            <asp:TextBox runat="server" ID="MSN" CssClass="edit" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel28" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="yim" />
        </td>
        <td class="post">
            <asp:TextBox runat="server" ID="YIM" CssClass="edit" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel27" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="aim" />
        </td>
        <td class="post">
            <asp:TextBox runat="server" ID="AIM" CssClass="edit" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel26" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="icq" />
        </td>
        <td class="post">
            <asp:TextBox runat="server" ID="ICQ" CssClass="edit" /></td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel30" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="SKYPE" />
        </td>
        <td class="post">
            <asp:TextBox runat="server" ID="Skype" CssClass="edit" /></td>
    </tr>  --%>  
    <tr>
        <td colspan="2" class="header2">
            <b>
                <YAF:LocalizedLabel ID="LocalizedLabel25" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="timezone" />
            </b>
        </td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel24" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="timezone2" />
        </td>
        <td class="post">
            <asp:DropDownList runat="server" ID="TimeZones" DataTextField="Name" DataValueField="Value" /></td>
    </tr>
    <tr runat="server" id="ForumSettingsRows">
        <td colspan="2" class="header2">
            <b>
                <YAF:LocalizedLabel ID="LocalizedLabel23" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="FORUM_SETTINGS" />
            </b>
        </td>
    </tr>
    <tr runat="server" id="UserThemeRow">
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel22" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="SELECT_THEME" />
        </td>
        <td class="post">
            <asp:DropDownList runat="server" ID="Theme" /></td>
    </tr>
    <tr runat="server" id="OverrideForumThemeRow">
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel21" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="OVERRIDE_DEFAULT_THEMES" />
        </td>
        <td class="post">
            <asp:CheckBox ID="OverrideDefaultThemes" runat="server" />
        </td>
    </tr>
    <tr runat="server" id="UserLanguageRow">
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel20" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="SELECT_LANGUAGE" />
        </td>
        <td class="post">
            <asp:DropDownList runat="server" ID="Language" /></td>
    </tr>
    <tr runat="server" id="PMNotificationRow">
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel19" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="PM_EMAIL_NOTIFICATION" />
        </td>
        <td class="post">
            <asp:CheckBox ID="PMNotificationEnabled" runat="server" /></td>
    </tr>
    <asp:PlaceHolder runat="server" ID="LoginInfo" Visible="false">
        <tr>
            <td colspan="2" class="header2">
                <YAF:LocalizedLabel ID="LocalizedLabel18" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="change_email" />
            </td>
        </tr>
        <tr>
            <td class="postheader">
                <YAF:LocalizedLabel ID="LocalizedLabel17" runat="server" LocalizedPage="CP_EDITPROFILE"
                    LocalizedTag="email" />
            </td>
            <td class="post">
                <asp:TextBox ID="Email" CssClass="edit" runat="server" OnTextChanged="Email_TextChanged" /></td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td class="footer1" colspan="2" align="center">
            <asp:Button ID="UpdateProfile" CssClass="pbutton" runat="server" OnClick="UpdateProfile_Click" />
            |
            <asp:Button ID="Cancel" CssClass="pbutton" runat="server" OnClick="Cancel_Click" />
        </td>
    </tr>
</table>
