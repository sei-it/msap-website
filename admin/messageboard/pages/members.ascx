<%@ Control Language="c#" CodeFile="members.ascx.cs" AutoEventWireup="True" Inherits="YAF.Pages.members" %>
<%@ Import Namespace="YAF.Classes.Core"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"
         TagPrefix="ajaxToolkit" %>

<YAF:PageLinks runat="server" ID="PageLinks" />

<table class="content" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td class="post" valign="top" style="padding:0px;">
      
				<table cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td class="header2" style="background-color:#A9DEF2;" colspan="6">
							<b>Members Search</b>
						</td>
					</tr>
			
					<tr class="post">
                    <td colspan="2">
                   Last Name: <asp:TextBox ID="txtSearch" runat="server" Width="95%"></asp:TextBox>
                   
                   <ajaxToolkit:AutoCompleteExtender ID="autoComplete1" runat="server"
  EnableCaching="true"
  BehaviorID="AutoCompleteEx"
  MinimumPrefixLength="1"
  TargetControlID="txtSearch"
  ServicePath="~/AutoComplete.asmx"
  ServiceMethod="GetCompletionList" 
  CompletionInterval="500"  
  CompletionSetCount="12"
  CompletionListCssClass="autocomplete_completionListElement"
  CompletionListItemCssClass="autocomplete_listItem"
  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
  DelimiterCharacters=";, :"
  ShowOnlyCurrentWordInCompletionListItem="true">
  <Animations>
  <OnShow>
  <Sequence>
  <%-- Make the completion list transparent and then show it --%>
  <OpacityAction Opacity="0" />
  <HideAction Visible="true" />

  <%--Cache the original size of the completion list the first time
    the animation is played and then set it to zero --%>
  <ScriptAction Script="// Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
  <%-- Expand from 0px to the appropriate size while fading in --%>
  <Parallel Duration=".4">
  <FadeIn />
  <Length PropertyKey="height" StartValue="0" 
	EndValueScript="$find('AutoCompleteEx')._height" />
  </Parallel>
  </Sequence>
  </OnShow>
  <OnHide>
  <%-- Collapse down to 0px and fade out --%>
  <Parallel Duration=".4">
  <FadeOut />
  <Length PropertyKey="height" StartValueScript=
	"$find('AutoCompleteEx')._height" EndValue="0" />
  </Parallel>
  </OnHide>
  </Animations>
  </ajaxToolkit:AutoCompleteExtender>


						</td>
						<td>
                        
                        </td>
						<td>Location:
							<asp:DropDownList ID="ddlStates" runat="server" EnableViewState="false" Width="95%">
                               <asp:ListItem Value="" >All</asp:ListItem>
    <asp:ListItem value="AL">Alabama</asp:ListItem>
    <asp:ListItem value="AK">Alaska</asp:ListItem>
    <asp:ListItem value="AZ">Arizona</asp:ListItem>
    <asp:ListItem value="AR">Arkansas</asp:ListItem>
    <asp:ListItem value="CA">California</asp:ListItem>
    <asp:ListItem value="CO">Colorado</asp:ListItem>
    <asp:ListItem value="CT">Connecticut</asp:ListItem>
    <asp:ListItem value="DC">D.C.</asp:ListItem>
    <asp:ListItem value="DE">Delaware</asp:ListItem>
    <asp:ListItem value="FL">Florida</asp:ListItem>
    <asp:ListItem value="GA">Georgia</asp:ListItem>
    <asp:ListItem value="HI">Hawaii</asp:ListItem>
    <asp:ListItem value="ID">Idaho</asp:ListItem>
    <asp:ListItem value="IL">Illinois</asp:ListItem>
    <asp:ListItem value="IN">Indiana</asp:ListItem>
    <asp:ListItem value="IA">Iowa</asp:ListItem>
    <asp:ListItem value="KS">Kansas</asp:ListItem>
    <asp:ListItem value="KY">Kentucky</asp:ListItem>
    <asp:ListItem value="LA">Louisiana</asp:ListItem>
    <asp:ListItem value="ME">Maine</asp:ListItem>
    <asp:ListItem value="MD">Maryland</asp:ListItem>
    <asp:ListItem value="MA">Massachusetts</asp:ListItem>
    <asp:ListItem value="MI">Michigan</asp:ListItem>
    <asp:ListItem value="MN">Minnesota</asp:ListItem>
    <asp:ListItem value="MS">Mississippi</asp:ListItem>
    <asp:ListItem value="MO">Missouri</asp:ListItem>
    <asp:ListItem value="MT">Montana</asp:ListItem>
    <asp:ListItem value="NE">Nebraska</asp:ListItem>
    <asp:ListItem value="NV">Nevada</asp:ListItem>
    <asp:ListItem value="NH">New Hampshire</asp:ListItem>
    <asp:ListItem value="NJ">New Jersey</asp:ListItem>
    <asp:ListItem value="NM">New Mexico</asp:ListItem>
    <asp:ListItem value="NY">New York</asp:ListItem>
    <asp:ListItem value="NC">North Carolina</asp:ListItem>
    <asp:ListItem value="ND">North Dakota</asp:ListItem>
    <asp:ListItem value="OH">Ohio</asp:ListItem>
    <asp:ListItem value="OK">Oklahoma</asp:ListItem>
    <asp:ListItem value="OR">Oregon</asp:ListItem>
    <asp:ListItem value="PA">Pennsylvania</asp:ListItem>
    <asp:ListItem value="RI">Rhode Island</asp:ListItem>
    <asp:ListItem value="SC">South Carolina</asp:ListItem>
    <asp:ListItem value="SD">South Dakota</asp:ListItem>
    <asp:ListItem value="TN">Tennessee</asp:ListItem>
    <asp:ListItem value="TX">Texas</asp:ListItem>
    <asp:ListItem value="UT">Utah</asp:ListItem>
    <asp:ListItem value="VT">Vermont</asp:ListItem>
    <asp:ListItem value="VA">Virginia</asp:ListItem>
    <asp:ListItem value="WA">Washington</asp:ListItem>
    <asp:ListItem value="WV">West Virginia</asp:ListItem>
    <asp:ListItem value="WI">Wisconsin</asp:ListItem>
    <asp:ListItem value="WY">Wyoming</asp:ListItem>
							</asp:DropDownList>
						</td>
                        <td>
                        
                        </td>
						<td>Role:
							<asp:DropDownList ID="ddlgroup" runat="server" Width="95%">
							</asp:DropDownList>
						</td>
					</tr>
                    <tr>
						<td align="right" colspan="6">
							<asp:Button ID="search" runat="server" OnClick="search_Click" Text="Search"></asp:Button>
						    <asp:Button ID="Clear" runat="server" OnClick="Clear_Click" Text="Clear"></asp:Button>
                        </td>
					</tr>
				</table>

              
			</td>
		</tr>
	</table>

<YAF:AlphaSort ID="AlphaSort1" runat="server" />
<YAF:Pager runat="server" ID="Pager" OnPageChange="Pager_PageChange" />

<table class="content" width="100%" cellspacing="1" cellpadding="0">
	<tr>
		<td class="header1" colspan="6">
			<YAF:LocalizedLabel ID="LocalizedLabel1" runat="server" LocalizedTag="title" />
		</td>
	</tr>
	<tr>
   
		<td class="header2">
			<img runat="server" id="SortUserName" alt="Sort User Name" style="vertical-align: middle" />
			<asp:LinkButton runat="server" ID="UserName" OnClick="UserName_Click" /></td>
		<td class="header2">
			<%--<img runat="server" id="SortRoles" alt="Sort User Name" style="vertical-align: middle" />--%>
			<asp:Label runat="server" ID="Roles"  /></td>

        <td class="header2">
			<%--<img runat="server" id="SortRank" alt="Sort Rank" style="vertical-align: middle" />
			<asp:LinkButton runat="server" ID="Rank" OnClick="Rank_Click" />--%>
            <img runat="server" id="SortOrganization" alt="Sort Organization" style="vertical-align: middle" />
			<asp:LinkButton runat="server" ID="Organization" OnClick="Organization_Click" />
            </td>
        <td class="header2">
        <img runat="server" id="SortLocation" alt="Sort Location" style="vertical-align: middle" />
			<asp:LinkButton runat="server" ID="Location" OnClick="Location_Click" /></td>

		<td class="header2">
			<img runat="server" id="SortJoined" alt="Sort Joined" style="vertical-align: middle" />
			<asp:LinkButton runat="server" ID="Joined" OnClick="Joined_Click" /></td>
		<td class="header2" align="center">
			<img runat="server" id="SortPosts" alt="Sort Posts" style="vertical-align: middle" />
			<asp:LinkButton runat="server" ID="Posts" OnClick="Posts_Click" /></td>
		
	</tr>
	<asp:Repeater ID="MemberList" runat="server">
		<ItemTemplate>
			<tr>
				<td class="post" style="height:35px;">
                <span style="float:left;"><img src="<%# this.GetAvatarUrlFileName(this.Eval("UserID").ToType<int>()) %>" alt="<%# DataBinder.Eval(Container.DataItem,"Name").ToString() %>"
                   width="35px" height="35px"  title="<%# DataBinder.Eval(Container.DataItem,"Name").ToString() %>" class="avatarimage" />
				</span>
                <div style="padding-top:10px;margin-left:5px;float:left;">
                <a href="/admin/messageboard/default.aspx?g=profile&amp;u=<%# Eval("UserID") %>" > <%# getUserFullName(this.Eval("UserID").ToString(), this.Eval("Name").ToString())%></a>
                </div>
                   <%-- <YAF:UserLink ID="UserProfileLink" runat="server" UserID='<%# Convert.ToInt32(Eval("UserID")) %>'
						UserName='<%# Eval("Name") %>' />--%>
				</td>
                <td class="post">
                <%# this.GetUserRoles(this.Eval("UserID").ToType<int>()) %>
                </td>
				<td class="post">
					<%# getOccupation(this.Eval("UserID").ToType<int>())%>
				</td>
                <td class="post">
					<%--<%# GetStringSafely(YafUserProfile.GetProfile(DataBinder.Eval(Container.DataItem,"Name").ToString()).Location) %>--%>
				    <%# getLocation(this.Eval("UserID").ToType<int>())%>
                </td>
				<td class="post">
					<%# YafServices.DateTime.FormatDateLong((System.DateTime)((System.Data.DataRowView)Container.DataItem)["Joined"]) %>
				</td>
				<td class="post" align="center">
					<%# String.Format("{0:N0}",((System.Data.DataRowView)Container.DataItem)["NumPosts"]) %>
				</td>
				
			</tr>
		</ItemTemplate>
	</asp:Repeater>
</table>
<YAF:Pager runat="server" LinkedPager="Pager" OnPageChange="Pager_PageChange" />
<div id="DivSmartScroller">
	<YAF:SmartScroller ID="SmartScroller1" runat="server" />
</div>
