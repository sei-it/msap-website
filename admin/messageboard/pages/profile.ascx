<%@ Control Language="c#" CodeFile="profile.ascx.cs" AutoEventWireup="True" Inherits="YAF.Pages.profile" %>
<%@ Import Namespace="YAF.Classes.Core" %>
<%@ Register TagPrefix="YAF" TagName="SignatureEdit" Src="../controls/EditUsersSignature.ascx" %>
<%@ Register TagPrefix="YAF" TagName="SuspendUser" Src="../controls/EditUsersSuspend.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ForumAccess" Src="../controls/ForumProfileAccess.ascx" %>

<YAF:PageLinks runat="server" ID="PageLinks" />
<table class="content" width="100%" cellspacing="1" cellpadding="0">
	<tr>
		<td class="header1" colspan="2">
			<YAF:LocalizedLabel runat="server" LocalizedTag="profile" />
			<asp:Label ID="UserName" runat="server" />
		</td>
	</tr>
	<tr class="post">
		<td colspan="2">
			<YAF:ThemeButton ID="PM" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="PM" ImageThemeTag="PM" />
			<!--<YAF:ThemeButton ID="Email" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="EMAIL" ImageThemeTag="EMAIL" />-->
			<YAF:ThemeButton ID="Home" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="HOME" ImageThemeTag="HOME" />
			<YAF:ThemeButton ID="Blog" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="BLOG" ImageThemeTag="BLOG" />
			<YAF:ThemeButton ID="MSN" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="MSN" ImageThemeTag="MSN" />
			<YAF:ThemeButton ID="AIM" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="AIM" ImageThemeTag="AIM" />
			<YAF:ThemeButton ID="YIM" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="YIM" ImageThemeTag="YIM" />
			<YAF:ThemeButton ID="ICQ" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="ICQ" ImageThemeTag="ICQ" />
			<YAF:ThemeButton ID="Skype" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
				TextLocalizedTag="SKYPE" ImageThemeTag="SKYPE" />
			<YAF:ThemeButton ID="AdminUserButton" runat="server" CssClass="yaflittlebutton" Visible="false"
				TextLocalizedTag="ADMIN_USER" NavigateUrl='<%# YafBuildLink.GetLinkNotEscaped( ForumPages.admin_edituser,"u={0}", Request.QueryString.Get("u") ) %>'>
			</YAF:ThemeButton>
		</td>
	</tr>
	<tr class="post">
		<td valign="top" rowspan="2">
			<DotNetAge:Tabs ID="ProfileTabs" runat="server" ActiveTabEvent="Click" AsyncLoad="false"
				AutoPostBack="false" Collapsible="false" ContentCssClass="" ContentStyle="" Deselectable="false"
				EnabledContentCache="false" HeaderCssClass="" HeaderStyle="" OnClientTabAdd=""
				OnClientTabDisabled="" OnClientTabEnabled="" OnClientTabLoad="" OnClientTabRemove=""
				OnClientTabSelected="" OnClientTabShow="" SelectedIndex="0" Sortable="false" Spinner="">
				<Animations>
					<DotNetAge:AnimationAttribute Name="HeightTransition" AnimationType="height" Value="toggle" />
				</Animations>
				<Views>
					<DotNetAge:View runat="server" ID="AboutTab" Text="About" NavigateUrl="" HeaderCssClass=""
						HeaderStyle="" Target="_blank">
						<table width="100%" cellspacing="1" cellpadding="0">
							<tr>
								<td width="50%" class="postheader">
									<b>
								<YAF:LocalizedLabel ID="LocalizedLabel2" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="FIRSTNAME" />
									</b>
								</td>
								<td width="50%" class="post" ID="lblFirstName" runat="server" >
								
								</td>
							</tr>
                            	<tr>
								<td width="50%" class="postheader">
									<b>
								<YAF:LocalizedLabel ID="LocalizedLabel39" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="LASTNAME" />
									</b>
								</td>
								<td width="50%" class="post" ID="lblLastName" runat="server">
								</td>
							</tr>
							<tr runat="server" id="userGroupsRow">
								<td class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel3" runat="server" LocalizedTag="groups" />
								</td>
								<td class="post">
                                
 								<asp:Repeater ID="Groups" runat="server">
										<ItemTemplate>
											<%# getUserGroup(Container.DataItem.ToString())%>
										</ItemTemplate>
										<SeparatorTemplate>, </SeparatorTemplate>
									</asp:Repeater>
								</td>
							</tr>
							<tr>
								<td class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel7" runat="server" LocalizedTag="occupation" />
								</td>
								<td class="post" runat="server" id="Occupation" />
							</tr>
							
							<tr>
								<td class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel6" runat="server" LocalizedTag="location" />
								</td>
								<td class="post">
									<asp:Label ID="Location" runat="server" />
								</td>
							</tr>
							<tr>
                            <td class="postheader">
                                <YAF:LocalizedLabel ID="LocalizedLabel24" runat="server" LocalizedPage="CP_EDITPROFILE"
                                    LocalizedTag="TIMEZONE" />
                            </td>
                            <td class="post" ID="TimeZones" runat="server">
          
                            </td>
                            </tr>

							<tr>
								<td class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel8" runat="server" LocalizedTag="interests" />
								</td>
								<td class="post" runat="server" id="Interests" />
							</tr>
							<tr>
								<td class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel9" runat="server" LocalizedTag="gender" />
								</td>
								<td class="post" runat="server" id="Gender" />
							</tr>
							 <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel1" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="homepage3" />
        </td>
        <td class="post" runat="server" ID="lblHomePage">
          </td>
    </tr>
    <tr>
        <td class="postheader">
            <YAF:LocalizedLabel ID="LocalizedLabel10" runat="server" LocalizedPage="CP_EDITPROFILE"
                LocalizedTag="weblog3" />
        </td>
        <td class="post" runat="server" ID="lblWeblog">
        </td>
    </tr>

						</table>
					</DotNetAge:View>
					<DotNetAge:View runat="server" ID="StatisticsTab" Text="Statistics" NavigateUrl=""
						HeaderCssClass="" HeaderStyle="" Target="_blank">
						<table width="100%" cellspacing="1" cellpadding="0">
							<tr>
								<td width="50%" class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel11" runat="server" LocalizedTag="joined" />
								</td>
								<td width="50%" class="post">
									<asp:Label ID="Joined" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel12" runat="server" LocalizedTag="lastvisit" />
								</td>
								<td class="post">
									<asp:Label ID="LastVisit" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="postheader">
									<YAF:LocalizedLabel ID="LocalizedLabel13" runat="server" LocalizedTag="numposts" />
								</td>
								<td class="post" runat="server" id="Stats" />
							</tr>
						</table>
					</DotNetAge:View>
					<DotNetAge:View runat="server" ID="AvatarTab" Text="Avatar" NavigateUrl="" HeaderCssClass=""
						HeaderStyle="" Target="_blank">
						<table align="center" width="100%" cellspacing="1" cellpadding="0">
							<tr>
								<td class="post" colspan="2" align="center">
									<asp:Image ID="Avatar" runat="server" CssClass="avatarimage" />
								</td>
							</tr>
						</table>
					</DotNetAge:View>
					<DotNetAge:View runat="server" ID="Last10PostsTab" Text="Last 10 Posts Tab" NavigateUrl=""
						HeaderCssClass="" HeaderStyle="" Target="_blank">
						<YAF:ThemeButton ID="SearchUser" runat="server" CssClass="yafcssimagebutton" TextLocalizedPage="POSTS"
							TextLocalizedTag="SEARCHUSER" ImageThemeTag="SEARCH" />
						<br style="clear: both" />
						<table width="100%" cellspacing="1" cellpadding="0">
			
							<asp:Repeater ID="LastPosts" runat="server">
								<ItemTemplate>
									<tr class="postheader">
										<td class="small" align="left" colspan="2">
											<b>
												<YAF:LocalizedLabel ID="LocalizedLabel16" runat="server" LocalizedTag="topic" />
											</b><a href='<%# YafBuildLink.GetLink(ForumPages.posts,"t={0}",DataBinder.Eval(Container.DataItem,"TopicID")) %>'>
												<%# YafServices.BadWordReplace.Replace(Convert.ToString(DataBinder.Eval(Container.DataItem,"Subject"))) %>
											</a>
											<br />
											<b>
												<YAF:LocalizedLabel ID="LocalizedLabel17" runat="server" LocalizedTag="posted" />
											</b>
											<%# YafServices.DateTime.FormatDateTime((System.DateTime)((System.Data.DataRowView)Container.DataItem)["Posted"]) %>
										</td>
									</tr>
									<tr class="post">
										<td valign="top" class="message" colspan="2">
											<YAF:MessagePostData ID="MessagePost" runat="server" ShowAttachments="false" DataRow="<%# Container.DataItem %>">
											</YAF:MessagePostData>
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</table>
					</DotNetAge:View>
					<DotNetAge:View runat="server" ID="ModerateTab" Text="Moderation" NavigateUrl=""
						HeaderCssClass="" HeaderStyle="" Visible="false" Target="_blank">
						<YAF:ForumAccess runat="server" ID="ForumAccessControl" />
						<table width="100%" cellspacing="1" cellpadding="0">
							<tr class="header2">
								<td class="header2" colspan="2">
									User Moderation
								</td>
							</tr>
						</table>
						<YAF:SuspendUser runat="server" ID="SuspendUserControl" ShowHeader="False" />
						<YAF:SignatureEdit runat="server" ID="SignatureEditControl" ShowHeader="False" />
					</DotNetAge:View>
				</Views>
			</DotNetAge:Tabs>
		</td>
	</tr>
</table>
<div id="DivSmartScroller">
	<YAF:SmartScroller ID="SmartScroller1" runat="server" />
</div>
