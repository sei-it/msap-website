<%@ Control Language="c#" CodeFile="forum.ascx.cs" AutoEventWireup="True" Inherits="YAF.Pages.forum" %>
<%@ Register TagPrefix="YAF" TagName="ForumWelcome" Src="../controls/ForumWelcome.ascx" %>

<%@ Register TagPrefix="YAF" TagName="ForumIconLegend" Src="../controls/ForumIconLegend.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ForumStatistics" Src="../controls/ForumStatistics.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ForumActiveDiscussion" Src="../controls/ForumActiveDiscussion.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ForumCategoryList" Src="../controls/ForumCategoryList.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ShoutBox" Src="../controls/ShoutBox.ascx" %>

 <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title> MSAP Center Message Board</title>
    <link rel="stylesheet" href="../../../css/feature-carousel.css" charset="utf-8" />
    <script src="../../../js/jquery-1.7.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="../../../js/jquery.featureCarousel.min.js" type="text/javascript" charset="utf-8"></script>

 <script type="text/javascript">
     $(document).ready(function () {
         var carousel = $("#carousel").featureCarousel({
             // include options like this:
             // (use quotes only for string values, and no trailing comma after last option)
             // option: value,
             // option: value
             //         trackerIndividual: false,
             //  //        trackerSummation: false,
             //          smallFeatureWidth: 05,
             //          smallFeatureHeight: 0.5
         });

         $("#but_prev").click(function () {
             carousel.prev();
         });
         $("#but_pause").click(function () {
             carousel.pause();
         });
         $("#but_start").click(function () {
             carousel.start();
         });
         $("#but_next").click(function () {
             carousel.next();
         });
     });
    </script>
    <YAF:PageLinks runat="server" ID="PageLinks" />

<table>
<tr>
<td>
<div class="carousel-container">
    
      
      <div id="carousel">
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide1_2.jpg"></a>
          <div class="carousel-caption">
           <h2>Welcome</h2>
            <p>
              The Magnet Connector, a professional community of practice, is a place to learn from and share with colleagues and content area experts. Please come in and join the discussion!
            </p>
            <p>For more information, read these documents:</p>
            <p><a href="/doc/carousel_docs/Magnet_Connector_Quick_Guide.pdf" target="_blank">Magnet Connector Quick Guide</a><br/><br/>
            <a href="/doc/carousel_docs/CommunityNorms.pdf" target="_blank">Community Norms</a></p>
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide2_2.jpg"></a>
          <div class="carousel-caption">
           <h2>Compliance Monitoring Technical Assistance</h2>
            <p><!--<img src="../../../images/Puzzle_icon.png" alt="" width="50" style="float:left;padding-right:7px;"> -->
            The MSAP Center has developed a new, easy-to-use crosswalk tool. It shows examples of technical assistance resources that relate to the compliance monitoring indicators.</p>
            <p>Use the compliance monitoring crosswalk to spark thinking about documentation as you prepare for your monitoring visit. <a href="/admin/doc/past_webinars/Compliance_Monitoring_crosswalk.pdf" target="_blank">Download now</a>
        </p>   
         <!-- <p class="cc_link">
            <a href="">link</a>
            </p>-->
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide3_2.jpg"></a>
          <div class="carousel-caption">
            <h2>Curriculum Mapping</h2>
            <p>Our curriculum mapping course provides tools that help magnet leaders and teachers work together to create curriculum maps-or to revise existing ones-that integrate theme-based content into teaching, learning, and assessment activities.</p>

			
            <p>View technical assistance courses <a href="http://msapcenter.com/admin/TA_courses.aspx">here</a>.</p>
             
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide4_2.jpg" /></a>
           <div class="carousel-caption">
           <h2 style="line-height:20px;">Talking Points<br />Sustainability Toolkit</h2>
        <p>Read and use the Planning for Sustainability Toolkit to help you map your sustainability activities. Then, share your experiences with the MSAP community. 
        Share&nbsp;<a href="http://msapcenter.com/admin/messageboard/default.aspx?g=posts&m=265#265" target="_self">here</a>.</p>
           </div>
        </div>
        <!--<div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide4.jpg"></a>
           <div class="carousel-caption">
           <h2>Links to Other sites</h2>
            <p>Links to approved sites...
            </p>
             <p class="cc_link">
            <a href="">link</a>
            </p>
           </div>
        </div>-->
      </div>
    
      <div id="carousel-left"><img src="../../../img/carousel/arrow-left.png" alt="Image Caption"/></div>
      <div id="carousel-right"><img src="../../../img/carousel/arrow-right.png" /></div>
    </div>
</td>
</tr>
</table>
<YAF:ForumWelcome runat="server" ID="Welcome" />

<YAF:ForumCategoryList ID="ForumCategoryList" runat="server"></YAF:ForumCategoryList>
<br />
<YAF:ShoutBox ID="ShoutBox1" runat="server" />
<br />
<!-- <YAF:ForumActiveDiscussion ID="ActiveDiscussions" runat="server" />
<br />
<YAF:ForumStatistics ID="ForumStats" runat="Server" />
<YAF:ForumIconLegend ID="IconLegend" runat="server" /> -->
<div id="DivSmartScroller">
	<YAF:SmartScroller ID="SmartScroller1" runat="server" />
</div>
