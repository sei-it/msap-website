/* Yet Another Forum.NET
 * Copyright (C) 2003-2005 Bj�rnar Henden
 * Copyright (C) 2006-2009 Jaben Cargman
 * http://www.yetanotherforum.net/
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.Security;
using YAF.Classes;
using YAF.Classes.Core;
using YAF.Classes.Utils;
using YAF.Controls;
using System.Web;
using System.Linq;

namespace YAF.Pages // YAF.Pages
{
	/// <summary>
	/// Summary description for members.
	/// </summary>
    public partial class members : YAF.Classes.Core.ForumPage
    {

        public members()
            : base("MEMBERS")
        {
        }

        UsersDataContext UserGroupdb = new UsersDataContext();
        UserProfileDataContext UProfiledb = new UserProfileDataContext();

        /// <summary>
        /// Called when the page loads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            ddlgroup.Items.Clear();

            DataTable grp = UserGroupdb.yaf_Groups.ToDataTable();

            ddlgroup.Items.Add(new ListItem("All", ""));
            string grpid="",  s="";
            foreach (DataRow dr in grp.Rows)
            {
                s = dr["Name"].ToString();
                grpid = dr["GroupID"].ToString();
                if (s.ToLower() == "guests" || s.ToLower() == "registered" || s.ToLower() == "testgroup" || s.ToLower() == "web" || s.ToLower() == "data"|| s=="")
                    continue;

                if (s.ToLower() == "projectdirector")
                    s = "Project Director";
                else if (s.ToLower() == "evaluatortwg")
                    s = "Evaluator/TWG";
                else if (s.ToLower() == "projectdirectortwg")
                    s = "Project Director/TWG";
                else if (s.ToLower() == "administrators")
                    s = "MSAP Center";

                ddlgroup.Items.Add(new ListItem(s, grpid));
            }



            if (!IsPostBack)
            {
                PageLinks.AddLink(PageContext.BoardSettings.Name, YafBuildLink.GetLink(ForumPages.forum));
                PageLinks.AddLink(GetText("TITLE"), "");

                SetSort("LastName", true);

                Roles.Text = GetText("roles");
                UserName.Text = GetText("username");
                //Rank.Text = GetText("rank");
                Organization.Text = GetText("organization");
                Joined.Text = GetText("joined");
                Posts.Text = GetText("posts");
                Location.Text = GetText("location");

                BindData();
            }
        }

        /// <summary>
        /// protects from script in "location" field	    
        /// </summary>
        /// <param name="svalue"></param>
        /// <returns></returns>
        protected string GetStringSafely(object svalue)
        {
            return HtmlEncode(svalue.ToString());
        }

        /// <summary>
        /// Helper function for setting up the current sort on the memberlist view
        /// </summary>
        /// <param name="field"></param>
        /// <param name="asc"></param>
        private void SetSort(string field, bool asc)
        {
            if (ViewState["SortField"] != null && (string)ViewState["SortField"] == field)
            {
                ViewState["SortAscending"] = !(bool)ViewState["SortAscending"];
            }
            else
            {
                ViewState["SortField"] = field;
                ViewState["SortAscending"] = asc;
            }
        }

        protected void UserName_Click(object sender, System.EventArgs e)
        {
            SetSort("LastName", true);
            BindData();
        }

        protected void Roles_Click(object sender, System.EventArgs e)
        {
            SetSort("Roles", true);
            BindData();
        }

        protected void Joined_Click(object sender, System.EventArgs e)
        {
            SetSort("Joined", true);
            BindData();
        }

        protected void Posts_Click(object sender, System.EventArgs e)
        {
            SetSort("NumPosts", false);
            BindData();
        }

        protected void Location_Click(object sender, System.EventArgs e)
        {
            SetSort("Location", true);
            BindData();
        }

        protected void Organization_Click(object sender, System.EventArgs e)
        {
            SetSort("Organization", true);
            BindData();
        }

        protected void Pager_PageChange(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {

            Pager.PageSize = 20;
            vwUserProfileDataContext vwUserProfileDB = new vwUserProfileDataContext();

            // get the user list
            //DataTable userListDataTable = YAF.Classes.Data.DB.user_list(PageContext.PageBoardID, null, true);

            DataTable userListDataTable = vwUserProfileDB.vwUserProfiles.ToDataTable();


            string filter = "";

            if (!string.IsNullOrEmpty(txtSearch.Text))   //table.Select("Col1 like '%" + parameter+ "%' ");
            {
                if (txtSearch.Text.Split(',').Count() > 1)
                    filter = " LastName Like '%" + txtSearch.Text.Split(',')[0].Trim() + "%' AND FirstName like '%" + txtSearch.Text.Split(',')[1].Trim() + "%'";
                else
                    filter = " LastName like '%" + txtSearch.Text.Trim() + "%'";
            }

            if (ddlStates.SelectedIndex != 0)
                if (!string.IsNullOrWhiteSpace(filter))
                    filter += " AND Location = '" + ddlStates.SelectedValue + "'";
                else
                    filter += " Location = '" + ddlStates.SelectedValue + "'";

            string userlist = "";

            if (!string.IsNullOrWhiteSpace(ddlgroup.SelectedValue))
            {
                var user_group = from c in UserGroupdb.yaf_UserGroups
                                 where c.GroupID == Convert.ToInt32(ddlgroup.SelectedValue)
                                 select c;

                foreach (yaf_UserGroup ugp in user_group)
                {
                    userlist += "'" + ugp.UserID + "',";
                }


                if (!string.IsNullOrWhiteSpace(userlist))
                {
                    userlist = userlist.Substring(0, userlist.Length - 1);  //trim last ","
                    if (!string.IsNullOrWhiteSpace(filter))
                        filter += " AND UserID IN ( " + userlist + ")";
                    else
                        filter += " UserID IN ( " + userlist + ")";
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(filter))
                        filter += " AND UserID = -1 ";  //return 0 record
                    else
                        filter += " UserID = -1 ";  //return 0 record
                }
            }

            userListDataTable = userListDataTable.Select(filter).Count() > 0 ? userListDataTable.Select(filter).CopyToDataTable() : userListDataTable.Clone();


            // get the view from the datatable
            DataView userListDataView = userListDataTable.DefaultView;


            char selectedLetter = AlphaSort1.CurrentLetter;

            // handle dataview filtering
            if (selectedLetter != char.MinValue)
            {
                if (selectedLetter == '#')
                {
                    filter = string.Empty;
                    foreach (char letter in GetText("LANGUAGE", "CHARSET"))
                    {
                        if (filter == string.Empty)
                            filter = string.Format("LastName not like '{0}%'", letter);
                        else
                            filter += string.Format("and LastName not like '{0}%'", letter);
                    }
                    userListDataView.RowFilter = filter;
                }
                else if (selectedLetter != '*')
                {
                    userListDataView.RowFilter = string.Format("LastName like '{0}%'", selectedLetter);
                }
            }

            Pager.Count = userListDataView.Count;

            // create paged data source for the memberlist
            userListDataView.Sort = String.Format("{0} {1}", ViewState["SortField"], (bool)ViewState["SortAscending"] ? "asc" : "desc");
            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = userListDataView;
            pds.AllowPaging = true;
            pds.CurrentPageIndex = Pager.CurrentPageIndex;
            pds.PageSize = Pager.PageSize;

            MemberList.DataSource = pds;
            DataBind();

            // handle the sort fields at the top
            // TODO: make these "sorts" into controls
            SortUserName.Visible = (string)ViewState["SortField"] == "LastName";
            SortUserName.Src = GetThemeContents("SORT", (bool)ViewState["SortAscending"] ? "ASCENDING" : "DESCENDING");

            //SortRoles.Visible = (string)ViewState["SortField"] == "Roles";
            //SortRoles.Src = SortUserName.Src;

            //SortRank.Visible = (string)ViewState["SortField"] == "RankName";
            //SortRank.Src = SortUserName.Src;
            SortOrganization.Visible = (string)ViewState["SortField"] == "Organization";
            SortOrganization.Src = SortUserName.Src;

            SortLocation.Visible = (string)ViewState["SortField"] == "Location";
            SortLocation.Src = SortUserName.Src;

            SortJoined.Visible = (string)ViewState["SortField"] == "Joined";
            SortJoined.Src = SortUserName.Src;
            SortPosts.Visible = (string)ViewState["SortField"] == "NumPosts";
            SortPosts.Src = SortUserName.Src;
        }

        /// <summary>
        /// Gets the avatar Url for the user
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="avatarString">The avatar string.</param>
        /// <param name="hasAvatarImage">if set to <c>true</c> [has avatar image].</param>
        /// <param name="email">The email.</param>
        /// <returns>Returns the File Url</returns>
        protected string GetAvatarUrlFileName(int userId)
        {

            CombinedUserDataHelper userData = new CombinedUserDataHelper(userId);

            string avatarUrl = new YafAvatars().GetAvatarUrlForUser(userData);

            if (string.IsNullOrWhiteSpace(avatarUrl))
            {
                if (string.IsNullOrEmpty(YafForumInfo.ForumRoot))
                    //avatarUrl = "../resource.ashx?url=images/noavatar.gif&width=35&height=35";
                    return avatarUrl = "../images/noavatar.gif";
                else
                    //avatarUrl = YafForumInfo.ForumRoot + "resource.ashx?url=images/noavatar.gif&width=35&height=35";
                    return avatarUrl = YafForumInfo.ForumRoot + "images/noavatar.gif";
            }
            else
                return avatarUrl.Contains(".ashx") ? avatarUrl + "&insID=" + Guid.NewGuid() : avatarUrl; //avatarUrl= "../resource.ashx?url= " + avatarUrl + "&width=35&height=35";


        }


        private string FormatWith(string s)
        {
            return string.IsNullOrEmpty(s) ? null : s;
        }

        protected string GetUserRoles(int userID)
        {
            string roles = "";

            string[] Arry = RoleMembershipHelper.GetRolesForUser(UserMembershipHelper.GetUserNameFromID(userID));

            foreach (string s in Arry)
            {
                string strTmp = "";
                if (s.ToLower() == "projectdirector")
                    strTmp = "Project Director";
                else if (s.ToLower() == "evaluatortwg")
                    strTmp = "Evaluator/TWG";
                else if (s.ToLower() == "projectdirectortwg")
                    strTmp = "Project Director/TWG";
                else if (s.ToLower() == "administrators")
                    strTmp = "MSAP Center";
                else
                    strTmp = s;
                roles += strTmp + "<br/>";
            }


            return roles == "" ? "" : roles.Remove(roles.Length - 5);
        }

        protected string getUserFullName(string userId, string Name)
        {
            string strFullName = "";

            UserProfileDataContext db = new UserProfileDataContext();
            yaf_prov_Profile uprofile = db.yaf_prov_Profiles.SingleOrDefault(up => up.UserID == userId);
            if (uprofile != null)
                strFullName = uprofile.LastName + ", " + uprofile.FirstName;

            if (string.IsNullOrWhiteSpace(strFullName))
                return Name;
            else
                return strFullName;
        }

        protected string getLocation(int uID)
        {
            string strLoc = "";
            UserProfileDataContext db = new UserProfileDataContext();
            yaf_prov_Profile uprofile = db.yaf_prov_Profiles.SingleOrDefault(up => up.UserID == uID.ToString());

            if (uprofile != null)
                strLoc = uprofile.Location;

            return strLoc;

        }
        protected string getOccupation(int UID)
        {
            string strOccupation = "";
            UserProfileDataContext db = new UserProfileDataContext();
            yaf_prov_Profile uprofile = db.yaf_prov_Profiles.SingleOrDefault(up => up.UserID == UID.ToString());

            if (uprofile != null)
                strOccupation = uprofile.Occupation;

            return strOccupation;
        }
        protected void search_Click(object sender, EventArgs e)
        {
            // re-bind data
            this.BindData();
        }
        protected void Clear_Click(object sender, EventArgs e)
        {
            // re-direct to self.
            YafBuildLink.Redirect(ForumPages.members);
        }



    }
}
