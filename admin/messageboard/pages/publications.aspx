﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="publications.aspx.cs" Inherits="seeresource" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center Resources - Publications
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <a name="skip"></a>
    <div class="mainContent">
    <a href="resource.aspx">Library</a> &nbsp;&nbsp;> &nbsp;&nbsp;Publications &nbsp;&nbsp;<a href="multimedia.aspx">Multimedia</a> &nbsp;&nbsp;<a href="toolkits.aspx">Toolkits &amp; Guides</a>
    <h1>Publications</h1>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false" ShowHeader="false"
            AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" GridLines="None"
            OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
            <RowStyle BorderStyle="None" BorderColor="White" />
            <HeaderStyle BorderStyle="None" BorderColor="White" />
            <Columns>
                <asp:BoundField DataField="DisplayData" HtmlEncode="false" SortExpression="" HeaderText="" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
