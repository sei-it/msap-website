<%@ Control Language="c#" CodeFile="forum.ascx.cs" AutoEventWireup="True" Inherits="YAF.Pages.forum" %>
<%@ Register TagPrefix="YAF" TagName="ForumWelcome" Src="../controls/ForumWelcome.ascx" %>

<%@ Register TagPrefix="YAF" TagName="ForumIconLegend" Src="../controls/ForumIconLegend.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ForumStatistics" Src="../controls/ForumStatistics.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ForumActiveDiscussion" Src="../controls/ForumActiveDiscussion.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ForumCategoryList" Src="../controls/ForumCategoryList.ascx" %>
<%@ Register TagPrefix="YAF" TagName="ShoutBox" Src="../controls/ShoutBox.ascx" %>

 <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title> MSAP Center Message Board</title>
    <link rel="stylesheet" href="../../../css/feature-carousel.css" charset="utf-8" />
    <script src="../../../js/jquery-1.7.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="../../../js/jquery.featureCarousel.min.js" type="text/javascript" charset="utf-8"></script>

 <script type="text/javascript">
     $(document).ready(function () {
         var carousel = $("#carousel").featureCarousel({
             // include options like this:
             // (use quotes only for string values, and no trailing comma after last option)
             // option: value,
             // option: value
             //         trackerIndividual: false,
             //  //        trackerSummation: false,
             //          smallFeatureWidth: 05,
             //          smallFeatureHeight: 0.5
         });

         $("#but_prev").click(function () {
             carousel.prev();
         });
         $("#but_pause").click(function () {
             carousel.pause();
         });
         $("#but_start").click(function () {
             carousel.start();
         });
         $("#but_next").click(function () {
             carousel.next();
         });
     });
    </script>
    <YAF:PageLinks runat="server" ID="PageLinks" />

<table>
<tr>
<td>
<div class="carousel-container">
    
      <div id="carousel">
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide1.jpg"></a>
          <div class="carousel-caption">
           <h2>Welcome</h2>
            <p>
              The Magnet Connector, a professional community of practice, is a place to learn from and share with colleagues and content area experts. Please come in and join the discussion!
            </p>
            <p>For more information, read these documents:</p>
            <p><a href="/doc/carousel_docs/Magnet_Connector_Quick_Guide.pdf" target="_blank">Magnet Connector Quick Guide</a><br/><br/>
            <a href="/doc/carousel_docs/CommunityNorms.pdf" target="_blank">Community Norms</a></p>
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide2.jpg"></a>
          <div class="carousel-caption">
           <h2>Current Conversations</h2>
            <p><!--<img src="../../../images/Puzzle_icon.png" alt="" width="50" style="float:left;padding-right:7px;"> -->Like other schools, you may be integrating the Common Core standards into your magnet curriculum. Our expert offers information on how the Common Core may affect your magnet program. Share your experiences, and ask for advice or ideas.
           <a href="/admin/messageboard/default.aspx?g=posts&t=40">Share here</a>
            </p>
            <!-- <p class="cc_link">
            <a href="">link</a>
            </p>-->
          </div>
        </div>
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide3.jpg"></a>
          <div class="carousel-caption">
            <h2>Marketing Webinars</h2>
            <p>The MSAP Center will host a series of webinars on strategies, ideas, and best practices for marketing your magnet program and recruiting students. The first webinar will take place on November 14, 2012 at 1:00 p.m. Eastern. Ongoing facilitated discussions about marketing and recruitment will also be shared on the Magnet Connector.
            </p>
            <p>See more upcoming events <a href="/admin/upcoming_events.aspx">here</a>.</p>
             
          </div>
        </div>
        <!--<div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/test4.jpg"></a>
           <div class="carousel-caption">
           <h2>Links to Other sites</h2>
            <p>Links to approved sites...
            </p>
             <p class="cc_link">
            <a href="">link</a>
            </p>
           </div>
        </div>-->
        <div class="carousel-feature">
          <a href="#"><img class="carousel-image" alt="Image Caption" src="../../../img/carousel/imgslide5.jpg"></a>
          <div class="carousel-caption">
          	 <h2>Talking Points</h2>
            <p>As most of you enter year 3 of your MSAP grant, program sustainability is a priority.</p>
            <p>Talk about how the planning process is progressing in your magnet program and how the planning tools have helped you with this process. </p>
            <p><a href="/admin/messageboard/default.aspx?g=posts&t=26">Share here</a></p>
          </div>
        </div>
      </div>
    
      <div id="carousel-left"><img src="../../../img/carousel/arrow-left.png" /></div>
      <div id="carousel-right"><img src="../../../img/carousel/arrow-right.png" /></div>
    </div>
</td>
</tr>
</table>
<YAF:ForumWelcome runat="server" ID="Welcome" />

<YAF:ForumCategoryList ID="ForumCategoryList" runat="server"></YAF:ForumCategoryList>
<br />
<YAF:ShoutBox ID="ShoutBox1" runat="server" />
<br />
<!-- <YAF:ForumActiveDiscussion ID="ActiveDiscussions" runat="server" />
<br />
<YAF:ForumStatistics ID="ForumStats" runat="Server" />
<YAF:ForumIconLegend ID="IconLegend" runat="server" /> -->
<div id="DivSmartScroller">
	<YAF:SmartScroller ID="SmartScroller1" runat="server" />
</div>
