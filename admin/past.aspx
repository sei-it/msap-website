﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="past.aspx.cs" Inherits="admin_path" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
Past MSAP Conferences - 2010
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1 style="border:none;">Past Conferences</h1>
<div class="titles">2010 MSAP Project Directors Meeting, November 4-5, 2010</div>
    <div class="tab_area" style="width:840px;">
    		<ul class="tabs">
                <div runat="server" style="width: 600px; float: right;">
                    <li class="tab_active">2010</li> 
                    <li><a href="past2011.aspx">2011</a></li>
                    <li><a href="past2012.aspx">2012</a></li>
                    <li><a href="past2013.aspx">2013</a></li>
                </div>
        	</ul>
    	</div>
    	<br/>
   <div style="float: left; margin-top: 0; width: 20%;;padding-bottom:15px;">
   	<p>
            <strong>Meeting Participant List</strong><br />
            <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/MSAP_2010_PD_Mtg_Part_List.pdf" target="_blank">Download
                    PDF</a> <small>(1 MB)</small>
    </p>
    </div>
    <div style="float: left; margin-top: 0; width: 75%;;padding-bottom:15px;border-left:1px solid #A9DEF0;padding-left:20px;">
                    
         <h3 class="pastconf_subsection" style="padding-top:0px;">Day 1: November 4, 2010</h3>
            <strong class="pastconf_subtitle">Bird’s Eye View of 2010 MSAP Grantees</strong><br />
			<span class="pastconf_author"><i>Anna Hinton, PhD, and Brittany Beth</i><br />
            U.S. Department of Education</span>
            
            <p>
            The session highlighted tidbits from this year’s new MSAP grants. Participants
            heard and shared highlights from the new projects. Participants were made aware
            of the diverse themes and resources among their own community.
            </p> 
          
<div id="Div1" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Birds Eye View - A look at the FY 2010 Grantees
                </td>
                <td>
                      <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/BirdsEyeView.pdf" target="_blank">Download
                    PDF</a> <small>(1 MB)</small>
                </td>
            </tr>
        </table>
    	</div>
        <p>
            <strong class="pastconf_subtitle">Introduction to Grants Management and MSAP</strong><br />
               <span class="pastconf_author"><i>Otis Wilson</i><br/>
          Office of the Secretary</span>
  <p>
            This session provided an overview of the U.S. Department of Education’s
            financial policies. It also provided guidance on managing your grant project budget,
            including direction for avoiding audit issues and understanding grantees’ budget
            flexibility.</p>
</p>
           
       <div id="Div2" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Discretionary Grant Administration
                </td>
                <td>
                      <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/Grantsmngmnt.pdf" target="_blank">Download
                    PDF</a> <small>(2.4 MB)</small><br />
                </td>
            </tr>
             <tr>
                <td>
                    The "Expanded Authorities" Amendments
                </td>
                <td>
                    
            <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/ExpandedAuthorities.pdf" target="_blank">Download
                    PDF</a> <small>(24 KB)</small><br />
            
                </td>
            </tr>
             <tr  bgcolor='#E8E8E8'>
                <td>
                    Grant Administration Resources
                </td>
                <td>
            		<img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/GrantsManagementResourceList.pdf" target="_blank">Download
                    PDF</a> <small>(44 KB)</small><br />
                </td>
            </tr>
             <tr>
                <td>
                    Appendix A: Prior Approval Requirements in the Cost Principles
                </td>
                <td>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                        vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/Appendix.pdf" target="_blank">Download PDF</a>
                    <small>(88 KB)</small>
                </td>
            </tr>
        </table>
    	</div>
        <p>
            <strong class="pastconf_subtitle">Program- and Project-Level Goals and Objectives</strong><br />
                <span class="pastconf_author"><i>Courtney Brown, PhD</i><br/>
                Center for Evaluation and Education Policy (CEEP)</span>
           
  <p>The Center for Evaluation and Education Policy (CEEP) presented a session
            on developing a high quality performance measurement system. This session provided
            a framework for performance measurement with guidelines on writings strong project
            objectives and high quality performance measures. 2010 grantees received an analysis
            of their project objectives and measures with tips on how to strengthen them.</p>
</p>
          
        <div id="Div4" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    High Quality Objectives and Performance Measurement
                </td>
                <td>
                       <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/CEEP.pdf" target="_blank">Download PDF</a>
            	<small>(616 KB)</small>
                </td>
            </tr>
        </table>
    	</div>
	<h3 class="pastconf_subsection">Day 2: November 5, 2010</h3>
    
	<strong class="pastconf_subtitle">2011 Civil Rights Review of MSAP Grantees</strong><br />
      <span class="pastconf_author"><i>Ricardo Soto, Christine Bischoff, and Richard Foster</i><br/>
      Office of Civil Rights</span>
  
    <p>
      The purposes of this session were to provide information about compliance
      with the MSAP statute’s nondiscrimination requirements and to inform MSAP grantees
      about OCR’s continuation year performance review process. Mr. Soto discussed the
      documents and information needed to conduct the 2011 Civil Rights Reviews and answered
      questions that grantees may have had about OCR’s review process.</p>
</p>   
           
        <div id="Div3" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Magnet Schools Assistance Program (MSAP)
                </td>
                <td>
                      <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/OCR.pdf" target="_blank">Download PDF</a>
            <small>(60 KB)</small> 
                </td>
            </tr>
        </table>
    	</div>
        <p>
            <strong class="pastconf_subtitle">Equity Assistance Centers</strong><br />
                <span class="pastconf_author"><i>JoEtta Gonzales, Velma Cobb, Gail Sunderman, and Maria Pachenco</i><br/>
                Equity Assistance Centers</span>
          
  <p>The panel of Equity Assistance Centers discussed how the centers assist
            public schools to promote equitable education opportunities to all children. The
            panel discussed resources, materials and professional development activities that
            are available to magnet schools through the Equity Assistance Centers.
  </p>
</p>

       <div id="Div5" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    JoEtta Gonzales, Director - Equity Alliance
                </td>
                <td>             
            	<img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/JoEttaGonzales.pdf" target="_blank">Download
                    PDF</a> <small>(4.5 MB)</small><br />
                </td>
            </tr>
            <tr>
                <td>
                    Velma Cobb, PhD, Region II - Equity Assistance Center
                </td>
                <td>
            	<img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/VelmaCobb.pdf" target="_blank">Download
                    PDF</a> <small>(660 KB)</small><br />
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Gail Sunderman, PhD, Region III - Equity: Promoting Academic Rigor for Magnet School Students
                </td>
                <td>
            	<img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/GailSunderman.pdf" target="_blank">Download
                    PDF</a> <small>(96 KB)</small><br />
                </td>
            </tr>
            <tr>
                <td>
                    Maria Pachenco, PhD, Region I - Region I EAC: The New England Equity Assistance Center (NEEAC)
                </td>
                <td>
            		<img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/MariaPacheco.pdf" target="_blank">Download
                    PDF</a> <small>(148 KB)</small>
                </td>
            </tr>
        </table>
    	</div>
        <p>
            <strong class="pastconf_subtitle">Next Steps: MSAP Technical Assistance Center</strong><br />
               <span class="pastconf_author"><i>Manya Walton, PhD, Project Director</i><br/>
                MSAP Center</span>
          
              
  <p>This session provided overviews of the MSAP Center's purpose and strategies for
            providing technical assistance to the 2010 grantee cohort. The presentation detailed
            the needs assessment process and technical assistance approach.
  </p>
</p>  
        <div id="Div6" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Technical Assistance Center
                </td>
                <td>
                      <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/NextSteps.pdf" target="_blank">Download
                    PDF</a> <small>(532 KB)</small>
                </td>
            </tr>
        </table>
    	</div>
        <p>
            <strong class="pastconf_subtitle">Magnet Schools and School Improvement</strong><br />
               <span class="pastconf_author"><i>Jessica Johnson, Chief Program Officer</i><br/>
                Learning Points Associates</span>
  <p>
            Ms. Johnson presented on the district's role in school improvement. She compared
            the role of the traditional district with that of the school improvement-focused
            district, concentrating on commitment to success, principal autonomy, initiative
            alignment, accountability and community engagement. Ms. Johnson also provided resources
            on how best to engage districts in magnet schools' mission to improve schools.</p>
</p>  

        <div id="Div7" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Supporting Magnet Schools in School Improvement</td>
                <td>
                               <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/Student_Achieve-tv.pdf" target="_blank">Download
                    PDF</a> <small>(1.5 MB)</small>
                </td>
            </tr>
        </table>
    	</div>
        <p>
            <strong class="pastconf_subtitle">Marketing and Recruitment</strong><br />
                <span class="pastconf_author"><i>Ariana Quiñones-Miranda</i><br/>
                Friends of Choice in Urban Schools (FOCUS)</span>
  <p>
            Ms. Quiñones-Miranda discussed the importance of student recruitment and the Six
            Rs of Recruitment. She provided examples of traditional and out-of-the box recruitment
            strategies and spoke about the importance of creating recruitment plans and branding.
            She also discussed the importance of evaluating recruitment strategies and managing
            expectations.</p>
 </p>
      <div id="Div8" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Best Practices in Student Recruitment
                </td>
                <td>
                           <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2010_Events/ArianaQuinones.pdf" target="_blank">Download
                    PDF</a> <small>(212 KB)</small>
                </td>
            </tr>
        </table>
    	</div>
       </div>             
         <!--<div style="text-align:right;padding-right:18px;" class="space_top">
			<a href="past_conferences.aspx"><strong>2011 Meeting</strong></a>&nbsp;&nbsp;
			2010 Meeting
		</div>-->

</asp:Content>

