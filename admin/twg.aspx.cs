﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class admin_twg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='twgTbl'>");
            var data2 = MagnetTWGMember.All().ToList();
            data2.Sort(CompareMemberName);
            var data = data2.ToArray();
            int pos = 0;
            while (pos < data.Length)
            {
                if (pos % 3 == 0)
                {
                    if(pos>0)
                        sb.Append("</tr>");
                    sb.Append("<tr>");
                }
                sb.AppendFormat("<td href='memberdetail.aspx?id={1}&height=200&width=300' class='thickbox' >{0}</td>", data[pos].MemberName, data[pos].ID);
                pos++;
            }
            sb.Append("</tr>");
            sb.Append("</table>");
            content.InnerHtml = sb.ToString();
        }
    }
    private static int CompareMemberName(MagnetTWGMember x, MagnetTWGMember y)
    {
        string x1 = x.MemberName.Split(' ')[1];
        string x2 = y.MemberName.Split(' ')[1];
        return x1.CompareTo(x2);
    }
}