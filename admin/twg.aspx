﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="twg.aspx.cs" Inherits="admin_twg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Technical Working Group
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery.colorbox.js"></script>
    <link media="screen" rel="stylesheet" href="../css/colorbox.css" />
    <script type="text/javascript" >
        $(document).ready(function () {
            $(".thickbox").colorbox({ width: 600, opacity:0.6 });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Technical Working Group</h1>
    The Technical Working Group (TWG) consists of 10 outstanding leaders in the education
    field and magnet schools community. These leaders will draw upon their magnet school
    experience and knowledge to advise the U.S. Department of Education and the MSAP
    Center on ways to improve technical assistance to MSAP grantees and the magnet schools
    community in areas of program implementation, management, and evaluation.
    <h2>TWG Members</h2>
    
    <div id="content" runat="server">
    </div>
</asp:Content>
