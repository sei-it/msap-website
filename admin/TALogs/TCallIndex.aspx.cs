﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Synergy.Magnet;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Collections;
using System.Reflection;
using System.Xml;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Services;



public partial class admin_TCallLogs_TCallIndex : System.Web.UI.Page
{
    DataTable tblParticipant = new DataTable();
    DataTable tblKeyPersonnel = new DataTable();

    MagnetDBDB db = new MagnetDBDB();
    vwContextualfactorDataClassesDataContext lnqdb = new vwContextualfactorDataClassesDataContext();
    int reportPeriod = 0, reportYear = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        //ddlgrant.Items.Clear();
        //ddlgrant.Items.Add(new ListItem("Select one",""));
        if (Session["TALogGranteeID"] != null)
            btnNew2ndCall.Visible = true;

        if (Session["TALogCohortType"] != null && Session["TALogCohortType"] != null && string.IsNullOrEmpty(Session["TALogCohortType"].ToString()))
            Session["TALogCohortType"] = null;
        if (Session["TALogGranteeID"] != null && Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
            Session["TALogGranteeID"] = null;

        if (!IsPostBack)
        {
            rbtnCohortType.SelectedValue = Session["TALogCohortType"] == null ? "2" : Session["TALogCohortType"].ToString();
            Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
            ddlgrant.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();
        }

        //int granteeId = string.IsNullOrEmpty(ddlgrant.SelectedValue)? 0 : Convert.ToInt32(ddlgrant.SelectedValue);
        var rptPeriod = MagnetReportPeriod.Find(x => x.isActive && x.CohortType == Convert.ToInt32(rbtnCohortType.SelectedValue )).Last();
        hfCohortType.Value = rptPeriod.CohortType.ToString();
        int ReportPeriodID = rptPeriod.ID;
        hfReportperiodID.Value = ReportPeriodID.ToString();
        hfReportYear.Value = rptPeriod.reportYear.ToString();
        reportPeriod = rptPeriod.ID;
        reportYear = Convert.ToInt32(rptPeriod.reportYear);
        ////always getting year1 APR reports
        //int cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        //var TCallrptPeriods = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType).OrderBy(y => y.ID);
        //foreach (var period in TCallrptPeriods)
        //{
        //    reportPeriod = period.ID;
        //    reportYear = Convert.ToInt32(period.reportYear);
        //    break;
        //}


        if (!IsPostBack)
        {
            foreach (MagnetDLL Item in MagnetDLL.Find(x => x.TypeID == 29).OrderBy(x => x.TypeIndex))  //29: GPRA 1 grades
            {
                cblGrades.Items.Add(new ListItem(Item.TypeName, Item.TypeIndex.ToString()));
            }

            if (ReportPeriodID < 6)
            {
                //Disable pre-k
                cblGrades.Items[0].Enabled = false;
                cblGrades.Items[0].Attributes.Add("color", "gray");
            }

           
        }

        //int factor =  ddlCohort.SelectedValue==""? 0 : Convert.ToInt32(ddlCohort.SelectedValue) - 1;
        //hfReportperiodID.Value = (1 + factor * 6).ToString();  //using APR year 1 only for each report period
        //hfReportperiodID.Value = ddlRptYr.SelectedValue;
        var rptPeriods = MagnetReportPeriod.Find(x => x.isActive);
        hfCohortType.Value = rptPeriods.Last().CohortType.ToString();
        if (string.IsNullOrEmpty(ddlgrant.SelectedValue) && Session["TALogGranteeID"]==null)
            btnNewCall.Visible = false;
        else
        {
            int numrows=0;
            if(!string.IsNullOrEmpty(ddlgrant.SelectedValue))
              numrows = TCallMain.Find(x => x.granteeid == Convert.ToInt32(ddlgrant.SelectedValue)).Count;
            else
                numrows = TCallMain.Find(x => x.granteeid == Convert.ToInt32(Session["TALogGranteeID"])).Count;
            if (numrows == 0)
            btnNewCall.Visible = true;
        }
        
      
    }

    private void check_updateTCallSchools()
    {
        int rptPeriodId = Convert.ToInt32(hfReportperiodID.Value);
        int rptYear = Convert.ToInt32(hfReportYear.Value);
        int granteeId = Convert.ToInt32(ddlgrant.SelectedValue);
        TCallSchool inputdata = new TCallSchool();

        var msapschooldata = from rdata in lnqdb.vwTAlogcontextualfactors
                         where rdata.ReportPeriodID == rptPeriodId && rdata.PK_GenInfoid == granteeId
                         && rdata.ReportYear == rptYear
                         select rdata;
        var tcallschooldata = TCallSchool.Find(x => x.granteeid == granteeId);
        if (!string.IsNullOrEmpty(hfCallLogID.Value))
        {
            tcallschooldata = TCallSchool.Find(x => x.granteeid == granteeId
                && x.Pk_tcallmainid == Convert.ToInt32(hfCallLogID.Value) && x.isActive);
             if (tcallschooldata.Count == 0)
                 tcallschooldata = TCallSchool.Find(x => x.granteeid == granteeId && x.tmpid == (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) && x.isActive);
        }
        else
            tcallschooldata = TCallSchool.Find(x => x.granteeid == granteeId && x.tmpid == (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) && x.isActive);

        if (tcallschooldata.Count == 0)
        {
            List<int> tcallschoolSavedIDs = new List<int>();
            foreach (var item in msapschooldata)
            {
                inputdata.tmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
                inputdata.isActive = true;
                inputdata.granteeid = granteeId;
                inputdata.PK_schoolID = (int)item.ID;
                inputdata.SchoolName = item.SchoolName;
                inputdata.SchoolGrade = item.SchoolGrade;
                if(item.theme!=null)
                    inputdata.MagnetThemes = item.theme;

                inputdata.Save();
                tcallschoolSavedIDs.Add(inputdata.id);
                inputdata = new TCallSchool();

            }
            Session["tcallschoolSavedIDs"] = tcallschoolSavedIDs;
        }
        else
        {
            foreach (var item in msapschooldata)
            {
                foreach (var schooldata in tcallschooldata)
                {
                    if (item.ID == schooldata.PK_schoolID)
                    {
                        schooldata.SchoolName = item.SchoolName;
                        schooldata.SchoolGrade = item.SchoolGrade;
                        schooldata.MagnetThemes = item.theme;
                        schooldata.Save();
                    }
                }
            }

        }

        gvSchools.DataBind();
    }

    private void openFormPanel()
    {
        pnlList.Visible = false;
        pnlPage1.Visible = true;
    }

    private void closeFormPanel()
    {
        pnlList.Visible = true;
        pnlPage1.Visible = false;
    }
   
    //create / edit a phone call based on granteeid in TCallMain table
    //The relation beteween a phone call and grantee is Many to One
    protected void OnEditCallLog(object sender, EventArgs e)
    {
        int granteeId = Convert.ToInt32(ddlgrant.SelectedValue);
        hfCallLogID.Value = (sender as LinkButton).CommandArgument;
        var grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeId);
        var contacts = MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(ddlgrant.SelectedValue) && x.ContactType == 1);
        lblPRAward.Text = grantee.PRAward;
        lblGrantee.Text = grantee.GranteeName;
        lblstate.Text = contacts[0].State;
        lblPofficer.Text = grantee.ProgramOfficerID==null ? "" : TALogProgramOffer.SingleOrDefault(x=>x.id ==Convert.ToInt32(grantee.ProgramOfficerID)).name;
        openFormPanel();
        TCallMain data = TCallMain.SingleOrDefault(x => x.id == Convert.ToInt32(hfCallLogID.Value));
        resetFormfields("update", data);
        check_updateTCallSchools();

        updateSubRecords(data);

        dsTCallSchools.SelectCommand = "SELECT [id], [PK_schoolID], [Pk_tcallmainid], [granteeid], [tmpid], [TcallSchoolName], [SchoolName], " +
       " [TcallSchoolGrades], [SchoolGrade], [TcallThemes], [MagnetThemes], [CreatedBy], [CreatedOn], [ModifiedBy], " +
       " [ModifiedOn], [isActive] FROM [TCallSchools] WHERE (Pk_tcallmainid= " + hfCallLogID.Value + " AND granteeid =" + grantee.ID + "  AND isActive=1 )";
        gvSchools.DataBind();

        dsParticipants.SelectCommand = "SELECT TCallParticipants.* FROM TCallParticipants WHERE ((Pk_tcallmainid = " +
        +Convert.ToInt32(hfCallLogID.Value) + "  OR tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "') AND granteeid = " +
        Convert.ToInt32(ddlgrant.SelectedValue) + " AND isActive=1)";
        gvParticipants.DataBind();

        dsKeyPersonnel.SelectCommand = "SELECT id, Pk_tcallmainid, Name, Title, LevelCommitpcnt, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, isActive " +
        " FROM TCallKeyPersonnel WHERE ((Pk_tcallmainid = " + Convert.ToInt32(hfCallLogID.Value) + " OR tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString())
        + "' ) AND granteeid = " + Convert.ToInt32(ddlgrant.SelectedValue) + " AND isActive=1)";
        gvKeyPersonnel.DataBind();

        dsDocs.SelectCommand = "SELECT id, isCompleted, Pk_tcallmainid, DocName, isOnFile, expDate, RecDate, CreatedBy, CreatedOn, ModifiedBy, "
        + " ModifiedOn, isActive FROM TCallDocs WHERE ((Pk_tcallmainid = "
        + Convert.ToInt32(hfCallLogID.Value) + " OR tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString())
        + "' ) AND granteeid = " + Convert.ToInt32(ddlgrant.SelectedValue) + "  AND isActive=1)";
        gvDocs.DataBind();
    }

    protected void NewCalllog_Click(object sender, EventArgs e)
    {
        resetFormfields("new", null);
        var grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlgrant.SelectedValue));
        var contacts = MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(ddlgrant.SelectedValue) && x.ContactType == 1);
        lblPRAward.Text = grantee.PRAward;
        lblGrantee.Text = grantee.GranteeName;
        lblstate.Text = contacts[0].State;
        lblPofficer.Text = grantee.ProgramOfficerID == null ? "" : TALogProgramOffer.SingleOrDefault(x => x.id == Convert.ToInt32(grantee.ProgramOfficerID)).name;
        check_updateTCallSchools();
        dsTCallSchools.SelectCommand = "SELECT [id], [PK_schoolID], [Pk_tcallmainid], [granteeid], [tmpid], [TcallSchoolName], [SchoolName], " +
       " [TcallSchoolGrades], [SchoolGrade], [TcallThemes], [MagnetThemes], [CreatedBy], [CreatedOn], [ModifiedBy], " +
       " [ModifiedOn], [isActive] FROM [TCallSchools] WHERE (tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "' AND granteeid =" + grantee.ID + "  AND isActive=1 )";
        openFormPanel();

    }

    protected void deleteTempSchools(object sender, CommandEventArgs e)
    {
        if (Session["tcallschoolSavedIDs"] != null)
        {
            List<int> tcallschoolids = (List<int>)Session["tcallschoolSavedIDs"];
            foreach (int id in tcallschoolids)
            {
                var tcallschool = TCallSchool.SingleOrDefault(x => x.id == id);
                if (tcallschool != null)
                    tcallschool.Delete();
            }
        }
        if (e != null)
        {
            Response.Redirect(e.CommandArgument.ToString());
        }
    }

    protected void OnCloseLog(object sender, EventArgs e)
    {
        deleteTempSchools(this, null);
        closeFormPanel();
    }
    protected void OnSaveLog(object sender, EventArgs e)
    {
        TCallMain data;
        if (string.IsNullOrEmpty(hfCallLogID.Value))
        {
            data = new TCallMain();
            data.CreatedBy = HttpContext.Current.User.Identity.Name;
            data.CreatedOn = DateTime.Now;
        }
        else
        {
            data = TCallMain.SingleOrDefault(x => x.id == Convert.ToInt32(hfCallLogID.Value));
        }
        data.ModifiedBy = HttpContext.Current.User.Identity.Name;
        data.ModifiedOn = DateTime.Now;

        data.granteeid = Convert.ToInt32(ddlgrant.SelectedValue);
        if (!string.IsNullOrEmpty(txtCalldate.Text))
            data.Calldate = DateTime.Parse(txtCalldate.Text);
        else
            data.Calldate = (DateTime?)null;
        data.ProjectDirector = txtPDirctor.Text;
        data.Phone = txtPhone.Text;
        data.Email = txtEmail.Text;
        data.status = rbtnlstSatus.Items[1].Selected;

        if (!string.IsNullOrEmpty(rbtnlistGAN.SelectedValue))
            data.isReceivedGAN = (rbtnlistGAN.SelectedValue=="0") ? false : true;
        else
            data.isReceivedGAN = (bool?)null;

        if (!string.IsNullOrEmpty(txtFY2013.Text))
            data.FY2013Amt = Convert.ToDouble(txtFY2013.Text);
        else
            data.FY2013Amt = (double?)null;

        if (!string.IsNullOrEmpty(txtFY2014.Text))
            data.FY2014Amt = Convert.ToDouble(txtFY2014.Text);
        else
            data.FY2014Amt = (double?)null;
        if (!string.IsNullOrEmpty(txtFY2015.Text))
            data.FY2015Amt = Convert.ToDouble(txtFY2015.Text);
        else
            data.FY2015Amt = (double?)null;

        data.chkisOnfile = chkOnfile.Checked;
        data.chkExpectedDate = chkExpDate.Checked;
        data.Y1costdeleted = txtY1costDeleted.Text;
        data.Y1costreduct = txtY1costReduct.Text;
        data.Y2costdeleted = txtY2costDeleted.Text;
        data.Y2costreduct = txtY2costReduct.Text;
        data.Y3costdeleted = txtY3costDeleted.Text;
        data.Y3costreduct = txtY3costReduct.Text;
        if (!string.IsNullOrEmpty(rbtnlstSpecialterms.SelectedValue))
            data.specialconditiontype = Convert.ToInt32(rbtnlstSpecialterms.SelectedValue);
        else
            data.specialconditiontype = (int?)null;

        data.OtherText = txtstOther.Text;
        data.chkAccepted = chkAccepted.Checked;
        data.chk3Yaward = chk3yrAward.Checked;
        if (!string.IsNullOrEmpty(rbtnlistIsImpact.SelectedValue))
            data.isImpact = (rbtnlistIsImpact.SelectedValue=="0") ? false : true;
        else
            data.isImpact = (bool?)null;

        data.ImpactText = txtOccureDes.Text;
        if (!string.IsNullOrEmpty(rbtnlistIsCannotConduct.SelectedValue))
            data.isCannotConduct = (rbtnlistIsCannotConduct.SelectedValue=="0") ? false : true;
        else
            data.isCannotConduct = (bool?)null;

        data.ProgramConcernDes = txtAdjustedDes.Text;
        data.chkPersonnelchange = chkPersonnelchange.Checked;
        if (!string.IsNullOrEmpty(rbtnlistchangeaftersubmit.SelectedValue))
            data.isChangedAfterSubmit = (rbtnlistchangeaftersubmit.SelectedValue=="0") ? false : true;
        else
            data.isChangedAfterSubmit = (bool?)null;

        data.ChangeDes = txtResume.Text;
        data.chkConfirm = chkConfirm.Checked;
        data.PersonnelAdditionalInfo = txtPersonnelAdditionalInfo.Text;
        data.chkReadGAN = chkReadGAN.Checked;
        if (!string.IsNullOrEmpty(rbtnlistIsReviewGAN.SelectedValue))
            data.isGANAccurate = (rbtnlistIsReviewGAN.SelectedValue=="0") ? false : true;
        else
            data.isGANAccurate = (bool?)null;

        data.InaccuracyOther = txtInaccuracyOther.Text;
        data.chkRegulations = chkRegulations.Checked;
        data.chkCostPrinciple = chkCostPrinciples.Checked;
        data.chkFiscal = chkFiscal.Checked;
        data.chk3dayrule = chk3dayrule.Checked;
        data.chkG5Hotline = chkG5hotline.Checked;
        if (!string.IsNullOrEmpty(rbtnlistisIRBClearance.SelectedValue))
            data.isIRBclearance = (rbtnlistisIRBClearance.SelectedValue == "0") ? false : true;
        else
            data.isIRBclearance = (bool?)null;

        if (!string.IsNullOrEmpty(rbtnlistisIRBextend.SelectedValue))
            if(rbtnlistisIRBextend.SelectedValue=="0")
                data.isIRBextend = 0;
            else if(rbtnlistisIRBextend.SelectedValue=="1")
                data.isIRBextend = 1;
            else
                data.isIRBextend = 2;
        else
            data.isIRBextend = 3;

        data.chkPublicAnnounce = chkPublicAnnounce.Checked;
        data.chkAPRs = chkAPRs.Checked;
        data.chkSummary = chkSummary.Checked;
        data.TeamMember = ddlEDStaff.SelectedValue;
        if (!string.IsNullOrEmpty(txtCompletedDate.Text))
            data.Completeddate = DateTime.Parse(txtCompletedDate.Text);
        else
            data.Completeddate = (DateTime?)null;

        if (!string.IsNullOrEmpty(txtMailonDate.Text))
            data.Mailondate = DateTime.Parse(txtMailonDate.Text);
        else
            data.Mailondate = (DateTime?)null;

        data.isSchoolchange = chkisSchoolchanged.Checked;
        if (!string.IsNullOrEmpty(txtApprovedpcnt.Text))
            data.Approvedpcnt = Convert.ToDouble(txtApprovedpcnt.Text);
        else
            data.Approvedpcnt = (double?)null;

        if (!string.IsNullOrEmpty(txtProposedpcnt.Text))
            data.Proposedpcnt = Convert.ToDouble(txtProposedpcnt.Text);
        else
            data.Proposedpcnt = (double?)null;

        if (!string.IsNullOrEmpty(txtExpectedDate.Text))
            data.ExpectedDate = Convert.ToDateTime(txtExpectedDate.Text);
        else
            data.ExpectedDate = (DateTime?)null;

        data.PRAward = lblPRAward.Text.Trim();
        data.GranteeName = lblGrantee.Text.Trim();
        data.GranteeState = lblstate.Text.Trim();
        data.ProgramOfficer = lblPofficer.Text.Trim();

        data.Save();

        updateSubRecords(data);


            hfCallLogID.Value = "";

        closeFormPanel();

        gvGranteeList.DataBind();
        Response.Redirect("TCallIndex.aspx");
    }

    private void updateSubRecords(TCallMain rcd)
    {
        int logid = rcd.id;
        var participants = TCallParticipant.Find(x => x.tmpid == (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()));
        var personnels = TCallKeyPersonnel.Find(x => x.tmpid == (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()));
        var schools = TCallSchool.Find(x => x.tmpid == (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()));
        var files = TCallDoc.Find(x => x.tmpid == (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()));
        var tallmain = TCallMain.SingleOrDefault(x => x.id == rcd.id);

        if (participants != null)
        {
            foreach (var item in participants)
            {
                item.tmpid = "";
                item.Pk_tcallmainid = logid;
                item.Save();
            }
        }

        if (personnels != null)
        {
            foreach (var item in personnels)
            {
                item.tmpid = "";
                item.Pk_tcallmainid = logid;
                item.Save();
            }
        }

        if (schools != null)
        {
            foreach (var item in schools)
            {
                item.tmpid = "";
                item.Pk_tcallmainid = logid;
                item.Save();
            }
        }

        if (files != null)
        {
            foreach (var item in files)
            {
                item.tmpid = "";
                item.Pk_tcallmainid = logid;
                item.Save();
            }
        }

        var numofComplete = from r in db.TCallDocs
                            where r.Pk_tcallmainid == logid
                            group r by r.finishlevel into g
                            select new { g.Key, Count = g.Sum(x=> x.finishlevel) };
        //finishlevel = 1 dateRec not set; finishlevel = 0 dateRecDate not empty
        //if (numofComplete.Count() > 0)
        //{
        //    foreach (var g in numofComplete)
        //    {
        //        if (g.Count == 0)
        //        {
        //            tallmain.status = true;
        //        }
        //        else
        //            tallmain.status = false;
        //    }
        //}
        //else
        //    tallmain.status = true;

        tallmain.Save();
    }

    private void resetFormfields(string mode, TCallMain data)
    {
        if (Session["TCALLtmpID"] == null)
        {
            //GenerateUniqueRandomNumber randomClass = new GenerateUniqueRandomNumber(rangelow, rangehight);
            //Session["TCALLtmpID"] = randomClass.NewRandomNumber();
            Session["TCALLtmpID"] = HttpContext.Current.Session.SessionID;
           
        }

        if (mode == "update")
        {
            
            var grantee = MagnetGrantee.SingleOrDefault(x=>x.ID==data.granteeid);
            if (data.Calldate != null)
                txtCalldate.Text = Convert.ToDateTime(data.Calldate).ToShortDateString();
            else
                txtCalldate.Text = "";
            chkisSchoolchanged.Checked = data.isSchoolchange == null ? false : (bool)data.isSchoolchange;
            txtPDirctor.Text= data.ProjectDirector;
            txtPhone.Text= data.Phone;
            txtEmail.Text=data.Email;
            if (data.isReceivedGAN != null)
                rbtnlistGAN.SelectedValue = (bool)data.isReceivedGAN ? "1" : "0";

            if (data.status)
                rbtnlstSatus.Items[1].Selected = true;

            if (data.FY2013Amt != null)
                txtFY2013.Text = Convert.ToDouble(data.FY2013Amt).ToString("C");
            else
                txtFY2013.Text = "";

            if (data.FY2014Amt != null)
                txtFY2014.Text = Convert.ToDouble(data.FY2014Amt).ToString("C");
            else
                txtFY2014.Text = "";

            if (data.FY2015Amt != null)
                txtFY2015.Text = Convert.ToDouble(data.FY2015Amt).ToString("C");
            else
                txtFY2015.Text = "";

            if (data.chkisOnfile != null)
                chkOnfile.Checked = (bool)data.chkisOnfile;
            else
                chkOnfile.Checked = false;

            if (data.chkExpectedDate != null)
                chkExpDate.Checked = (bool)data.chkExpectedDate;
            else
                chkExpDate.Checked = false;

            txtY1costDeleted.Text = data.Y1costdeleted;
            txtY1costReduct.Text = data.Y1costreduct;
            txtY2costDeleted.Text = data.Y2costdeleted;
            txtY2costReduct.Text = data.Y2costreduct;
            txtY3costDeleted.Text = data.Y3costdeleted;
            txtY3costReduct.Text = data.Y3costreduct;
            if (data.specialconditiontype != null)
                rbtnlstSpecialterms.SelectedValue = data.specialconditiontype.ToString();
        

            txtstOther.Text = data.OtherText;
            if (data.chkAccepted != null)
                chkAccepted.Checked = (bool)data.chkAccepted;
            else
                chkAccepted.Checked = false;

            if (data.chk3Yaward != null)
                chk3yrAward.Checked = (bool)data.chk3Yaward;
            else
                chk3yrAward.Checked = false;

            if (data.isImpact != null)
                rbtnlistIsImpact.SelectedValue = data.isImpact==true ? "1" : "0";
       

            txtOccureDes.Text = data.ImpactText;
            if (data.isCannotConduct != null)
                rbtnlistIsCannotConduct.SelectedValue = data.isCannotConduct == true ? "1" : "0";
         

            txtAdjustedDes.Text =data.ProgramConcernDes;
            if (data.chkPersonnelchange != null)
                chkPersonnelchange.Checked = (bool)data.chkPersonnelchange;
            else
                chkPersonnelchange.Checked = false;

            if (data.isChangedAfterSubmit != null)
                rbtnlistchangeaftersubmit.SelectedValue = data.isChangedAfterSubmit == true ? "1" : "0";
    

            txtResume.Text = data.ChangeDes;
            if (data.chkConfirm != null)
                chkConfirm.Checked = (bool)data.chkConfirm;
            else
                chkConfirm.Checked = false;

            txtPersonnelAdditionalInfo.Text = data.PersonnelAdditionalInfo;
            if (data.chkReadGAN != null)
                chkReadGAN.Checked = (bool)data.chkReadGAN;
            else
                chkReadGAN.Checked = false;

            if (data.isGANAccurate != null)
                rbtnlistIsReviewGAN.SelectedValue = (bool)data.isGANAccurate ? "1" : "0";
           

            txtInaccuracyOther.Text = data.InaccuracyOther;
            if (data.chkRegulations != null)
                chkRegulations.Checked = (bool)data.chkRegulations;
            else
                chkRegulations.Checked = false;

            if (data.chkCostPrinciple != null)
                chkCostPrinciples.Checked = (bool)data.chkCostPrinciple;
            else
                chkCostPrinciples.Checked = false;

            if (data.chkFiscal != null)
                chkFiscal.Checked = (bool)data.chkFiscal;
            else
                chkFiscal.Checked = false;

            if (data.chk3dayrule != null)
                chk3dayrule.Checked = (bool)data.chk3dayrule;
            else
                chk3dayrule.Checked = false;

            if (data.chkG5Hotline != null)
                chkG5hotline.Checked = (bool)data.chkG5Hotline;
            else
                chkG5hotline.Checked = false;

            if (data.isIRBclearance != null)
                rbtnlistisIRBClearance.SelectedValue = (bool)data.isIRBclearance ? "1" : "0";


            if (data.isIRBextend != null)
                if(data.isIRBextend==0)
                    rbtnlistisIRBextend.SelectedValue = "0";
                else if(data.isIRBextend==1)
                    rbtnlistisIRBextend.SelectedValue = "1";
                else if (data.isIRBextend == 2)
                    rbtnlistisIRBextend.SelectedValue = "2";
         

            if (data.chkPublicAnnounce != null)
                chkPublicAnnounce.Checked = (bool)data.chkPublicAnnounce;
            else
                chkPublicAnnounce.Checked = false;

            if (data.chkAPRs != null)
                chkAPRs.Checked = (bool)data.chkAPRs;
            else
                chkAPRs.Checked = false;

            if (data.chkSummary != null)
                chkSummary.Checked = (bool)data.chkSummary;
            else
                chkSummary.Checked = false;

            ddlEDStaff.SelectedValue = data.TeamMember;
            if (data.Completeddate != null)
                txtCompletedDate.Text = Convert.ToDateTime(data.Completeddate).ToShortDateString();
            else
                txtCompletedDate.Text = "";

            if (data.Mailondate != null)
                txtMailonDate.Text = Convert.ToDateTime(data.Mailondate).ToShortDateString();
            else
                txtMailonDate.Text = "";
            
            if (data.ExpectedDate != null)
                txtExpectedDate.Text = Convert.ToDateTime(data.ExpectedDate).ToShortDateString();
            else
                txtExpectedDate.Text = "";

            if (data.Proposedpcnt != null)
                txtProposedpcnt.Text = data.Proposedpcnt.ToString();
            else
                txtProposedpcnt.Text = "";

            if (data.Approvedpcnt != null)
                txtApprovedpcnt.Text = data.Approvedpcnt.ToString();
            else
                txtApprovedpcnt.Text = "";
        }
        else
        {
            txtApprovedpcnt.Text = "";
            txtProposedpcnt.Text = "";
            txtExpectedDate.Text = "";
            chkisSchoolchanged.Checked = false;
            hfCallLogID.Value = "";
            txtCalldate.Text = "";
            txtMailonDate.Text = "";
            txtPDirctor.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            //rbtnlistGAN.ClearSelection();
            txtFY2013.Text = "";
            txtFY2014.Text = "";
            txtFY2015.Text = "";
            chkOnfile.Checked = false;
            chkExpDate.Checked = false;
            txtY1costDeleted.Text = "";
            txtY1costReduct.Text = "";
            txtY2costDeleted.Text = "";
            txtY2costReduct.Text = "";
            txtY3costDeleted.Text = "";
            txtY3costReduct.Text = "";
            //rbtnlstSpecialterms.ClearSelection();
            txtstOther.Text = "";
            chkAccepted.Checked = false;
            chk3yrAward.Checked = false;
            //rbtnlistIsImpact.ClearSelection();
            txtOccureDes.Text = "";
            //rbtnlistIsCannotConduct.ClearSelection();
            txtAdjustedDes.Text = "";
            chkPersonnelchange.Checked = false;
            //rbtnlistchangeaftersubmit.ClearSelection();
            txtResume.Text = "";
            chkConfirm.Checked = false;
            txtPersonnelAdditionalInfo.Text = "";
            chkReadGAN.Checked = false;
            //rbtnlistIsReviewGAN.ClearSelection();
            txtInaccuracyOther.Text = "";
            chkRegulations.Checked = false;
            chkCostPrinciples.Checked = false;
            chkFiscal.Checked = false;
            chk3dayrule.Checked = false;
            chkG5hotline.Checked = false;
           // rbtnlistisIRBClearance.ClearSelection();
           // rbtnlistisIRBextend.ClearSelection();
            chkPublicAnnounce.Checked = false;
            chkAPRs.Checked = false;
            chkSummary.Checked = false;
            if(ddlEDStaff.Items.Count>0)
                ddlEDStaff.SelectedIndex = 0;

            txtCompletedDate.Text = "";
        }

        gvParticipants.DataBind();
        gvSchools.DataBind();
        gvKeyPersonnel.DataBind();
        gvDocs.DataBind();
    }

    protected void OnNewParticipant(object sender, EventArgs e)
    {
        txtname.Text = "";
        txtTitle.Text = "";
        txtRole.Text = "";
        mpeParticipant.Show();
    }
    protected void OnEditParticipant(object sender, EventArgs e)
    {
        hfParticipantID.Value = (sender as LinkButton).CommandArgument;
        var participant = TCallParticipant.SingleOrDefault(x => x.id == Convert.ToInt32(hfParticipantID.Value));
        txtname.Text = participant.Name;
        txtTitle.Text = participant.Title;
        txtRole.Text = participant.Role;
        mpeParticipant.Show();
       
    }
    protected void OnSaveParticipant(object sender, EventArgs e)
    {
        TCallParticipant participant;
        if (string.IsNullOrEmpty(hfParticipantID.Value))
        {
            participant = new TCallParticipant();
        }
        else
        {
            participant = TCallParticipant.SingleOrDefault(x => x.id == Convert.ToInt32(hfParticipantID.Value));
        }

        if (string.IsNullOrEmpty(hfCallLogID.Value))
            participant.tmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
        else
            participant.Pk_tcallmainid = Convert.ToInt32(hfCallLogID.Value);

        participant.granteeid = Convert.ToInt32(ddlgrant.SelectedValue);
        participant.Name = txtname.Text.Trim();
        participant.Title = txtTitle.Text.Trim();
        participant.Role = txtRole.Text.Trim();
        participant.isActive = true;
        participant.Save();
        hfParticipantID.Value = "";
        string strquery="";
        if (string.IsNullOrEmpty(hfCallLogID.Value))
            strquery = "SELECT TCallParticipants.* FROM TCallParticipants WHERE (( tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "') AND granteeid = " +
                        ddlgrant.SelectedValue + " AND isActive=1)";
        else
          strquery = "SELECT TCallParticipants.* FROM TCallParticipants WHERE ((Pk_tcallmainid = " +
         hfCallLogID.Value + "  OR tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "') AND granteeid = " +
        Convert.ToInt32(ddlgrant.SelectedValue) + " AND isActive=1)";
        dsParticipants.SelectCommand = strquery;
        gvParticipants.DataBind();
    }

    protected void OnEditSchool(object sender, EventArgs e)
    {
        int rptPeriodId = Convert.ToInt32(hfReportperiodID.Value);
        int granteeId = Convert.ToInt32(ddlgrant.SelectedValue);

        hfSchoolID.Value = (sender as LinkButton).CommandArgument;
        TCallSchool school = TCallSchool.SingleOrDefault(x => x.id == Convert.ToInt32(hfSchoolID.Value));
        if (school == null)
            //school = new TCallSchool();
            return;
        int schoolID = Convert.ToInt32(school.PK_schoolID);
        int rptId = GranteeReport.SingleOrDefault(x=>x.GranteeID == granteeId && x.ReportPeriodID == rptPeriodId).ID;
           var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == rptId);


           foreach (ListItem li in cblGrades.Items)
           {
               li.Selected = false;
           }


           if (gpraData.Count > 0)
           {
               //hfID.Value = gpraData[0].ID.ToString();
               MagnetGPRA item = gpraData[0];
               if (!string.IsNullOrEmpty(item.SchoolGrade))
               {
                   if (item.SchoolGrade.Split(';').Count() > 0)
                   {
                       foreach (string str in item.SchoolGrade.Split(';'))
                       {
                           foreach (ListItem li in cblGrades.Items)
                           {
                               //li.Selected = false;
                               if (li.Value.Equals(str))
                                   li.Selected = true;
                           }
                       }
                   }
                   
               }
           }

           txtSchoolnames.Text = school.SchoolName;
           txtTheme.Text = school.MagnetThemes;
           mpeSchool.Show();
        //var msapschool = lnqdb.vwTAlogcontextualfactors.FirstOrDefault(ms => ms.ID == schoolID);
        //if (msapschool != null)
        //{
        //    txtSchoolnames.Text = msapschool.SchoolName;
        //    txtTheme.Text = msapschool.MagnetTheme;

        //    mpeSchool.Show();
        //}

    }
    protected void OnSaveSchoolInfo(object sender, EventArgs e)
    {
        int rptPeriodId = Convert.ToInt32(hfReportperiodID.Value);
        int granteeId = Convert.ToInt32(ddlgrant.SelectedValue);
        int rptYear = (int)MagnetReportPeriod.SingleOrDefault(y=>y.ID == rptPeriodId).reportYear;
        int TcallschoolId = Convert.ToInt32(hfSchoolID.Value);


        TCallSchool school = TCallSchool.SingleOrDefault(x => x.id == TcallschoolId);
        var msapschool = lnqdb.vwTAlogcontextualfactors.FirstOrDefault(ms => ms.ReportPeriodID == rptPeriodId && ms.PK_GenInfoid == granteeId && ms.ID == school.PK_schoolID);
        if (school == null)
        {
            school = new TCallSchool();
            school.CreatedBy = HttpContext.Current.Profile.UserName;
            school.CreatedOn = DateTime.Now;
            school.ModifiedBy = HttpContext.Current.Profile.UserName;
            school.ModifiedOn = DateTime.Now;

            school.PK_schoolID = msapschool.ID;

            msapschool.CreatedBy = HttpContext.Current.Profile.UserName;
            msapschool.CreatedOn = DateTime.Now;
            msapschool.ModifiedBy = HttpContext.Current.Profile.UserName;
            msapschool.ModifiedOn = DateTime.Now;
        }
        else
        {
            school.ModifiedBy = HttpContext.Current.Profile.UserName;
            school.ModifiedOn = DateTime.Now;
        }

        if (string.IsNullOrEmpty(hfCallLogID.Value))
            school.tmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
        else
            school.Pk_tcallmainid = Convert.ToInt32(hfCallLogID.Value);

        school.SchoolName = msapschool.SchoolName;
        school.TcallSchoolName = msapschool.SchoolName.Equals(txtSchoolnames.Text) ? "" : txtSchoolnames.Text.Trim();

        school.SchoolGrade = msapschool.SchoolGrade;
        school.MagnetThemes = msapschool.theme;

        school.isActive = true;
        school.granteeid = Convert.ToInt32(ddlgrant.SelectedValue);
        string str = "";
        foreach (ListItem li in cblGrades.Items)
        {
            if (li.Selected)
            {
                if (!string.IsNullOrEmpty(str))
                    str += ";";
                str += li.Value;
            }
        }
        GradeRangeDataClassesDataContext gcodedb = new GradeRangeDataClassesDataContext();
        string gradeRange = string.IsNullOrEmpty(str)?"": gcodedb.getSchoolGradeRange(str);
        school.TcallSchoolGrades = gradeRange;

        school.TcallThemes = txtTheme.Text.Trim();

        school.Save();

        if (string.IsNullOrEmpty(hfCallLogID.Value))
            dsTCallSchools.SelectCommand = "SELECT [id], [PK_schoolID], [Pk_tcallmainid], [granteeid], [tmpid], [TcallSchoolName], [SchoolName], " +
       " [TcallSchoolGrades], [SchoolGrade], [TcallThemes], [MagnetThemes], [CreatedBy], [CreatedOn], [ModifiedBy], " +
       " [ModifiedOn], [isActive] FROM [TCallSchools] WHERE (tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "' AND granteeid =" + ddlgrant.SelectedValue + "  AND isActive=1 )";
        else
        {
            dsTCallSchools.SelectCommand = "SELECT [id], [PK_schoolID], [Pk_tcallmainid], [granteeid], [tmpid], [TcallSchoolName], [SchoolName], " +
       " [TcallSchoolGrades], [SchoolGrade], [TcallThemes], [MagnetThemes], [CreatedBy], [CreatedOn], [ModifiedBy], " +
       " [ModifiedOn], [isActive] FROM [TCallSchools] WHERE (Pk_tcallmainid= " + hfCallLogID.Value + " AND granteeid =" + ddlgrant.SelectedValue + "  AND isActive=1 )";
        }


        hfSchoolID.Value = "";
        
        gvSchools.DataBind();

       
    }

    protected void OnNewPersonnel(object sender, EventArgs e)
    {
        txtPsnlName.Text = "";
        txtPsnlTitle.Text = "";
        txtLevel.Text = "";

        mpePersonnel.Show();
    }
    protected void OnEditPersonnel(object sender, EventArgs e)
    {
        hfPersonnelID.Value = (sender as LinkButton).CommandArgument;
        var personnel = TCallKeyPersonnel.SingleOrDefault(x => x.id == Convert.ToInt32(hfPersonnelID.Value));

        txtPsnlName.Text = personnel.Name;
        txtPsnlTitle.Text = personnel.Title;
        if (personnel.LevelCommitpcnt != null)
        txtLevel.Text = personnel.LevelCommitpcnt.ToString();

        mpePersonnel.Show();
    }
    protected void OnSavePersonnel(object sender, EventArgs e)
    {
        TCallKeyPersonnel personnel = new TCallKeyPersonnel();
        if (string.IsNullOrEmpty(hfPersonnelID.Value))
        {
            personnel.CreatedBy = HttpContext.Current.Profile.UserName;
            personnel.CreatedOn = DateTime.Now;
            personnel.ModifiedBy = HttpContext.Current.Profile.UserName;
            personnel.ModifiedOn = DateTime.Now;
        }
        else
        {
            personnel = TCallKeyPersonnel.SingleOrDefault(x => x.id == Convert.ToInt32(hfPersonnelID.Value));
            personnel.ModifiedBy = HttpContext.Current.Profile.UserName;
            personnel.ModifiedOn = DateTime.Now;
        }

        if (string.IsNullOrEmpty(hfCallLogID.Value))
            personnel.tmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
        else
            personnel.Pk_tcallmainid = Convert.ToInt32(hfCallLogID.Value);

        personnel.granteeid = Convert.ToInt32(ddlgrant.SelectedValue);
        personnel.Name = txtPsnlName.Text;
        personnel.Title = txtPsnlTitle.Text;
        if (!string.IsNullOrEmpty(txtLevel.Text))
            personnel.LevelCommitpcnt = Convert.ToDouble(txtLevel.Text)/100;
        else
            personnel.LevelCommitpcnt = (double?)null;

        personnel.isActive = true;

        personnel.Save();

        hfPersonnelID.Value = "";
        string strquery = "";
        if (string.IsNullOrEmpty(hfCallLogID.Value))
            strquery = "SELECT id, Pk_tcallmainid, Name, Title, LevelCommitpcnt, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, isActive " +
       " FROM TCallKeyPersonnel WHERE (( tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString())
       + "' ) AND granteeid = " + Convert.ToInt32(ddlgrant.SelectedValue) + " AND isActive=1)";
        else
            strquery = "SELECT id, Pk_tcallmainid, Name, Title, LevelCommitpcnt, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, isActive " +
            " FROM TCallKeyPersonnel WHERE ((Pk_tcallmainid = " + hfCallLogID.Value + " OR tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString())
            + "' ) AND granteeid = " + ddlgrant.SelectedValue + " AND isActive=1)";
        dsKeyPersonnel.SelectCommand = strquery;
        gvKeyPersonnel.DataBind();
    }

    protected void OnNewFile(object sender, EventArgs e)
    {
        txtDocName.Text = "";
        chkisOnfile.Checked = false;
        txtExpDate.Text = "";
        txtReceivedDate.Text = "";
        mpeFollowupFile.Show();
    }
    protected void OnEditFile(object sender, EventArgs e)
    {
        hfFileID.Value = (sender as LinkButton).CommandArgument;
        var dataitem = TCallDoc.SingleOrDefault(x=>x.id == Convert.ToInt32(hfFileID.Value));
        txtDocName.Text = dataitem.DocName;
        if (dataitem.isOnFile != null)
            chkisOnfile.Checked = (bool)dataitem.isOnFile;
        else
            chkisOnfile.Checked = false;
        if (dataitem.expDate != null)
            txtExpDate.Text = Convert.ToDateTime(dataitem.expDate).ToShortDateString();
        else
            txtExpDate.Text = "";
        if (dataitem.RecDate != null)
            txtReceivedDate.Text = Convert.ToDateTime(dataitem.RecDate).ToShortDateString();
        else
            txtReceivedDate.Text = "";

        mpeFollowupFile.Show();
    }
    protected void OnSaveFile(object sender, EventArgs e)
    {
        TCallDoc dataitem;
        if (string.IsNullOrEmpty(hfFileID.Value))
        {
            dataitem = new TCallDoc();
        }
        else
        {
            dataitem = TCallDoc.SingleOrDefault(x => x.id == Convert.ToInt32(hfFileID.Value));
        }

        if (string.IsNullOrEmpty(hfCallLogID.Value))
        {
            dataitem.tmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
        }
        else
        {
            dataitem.tmpid = "";
            dataitem.Pk_tcallmainid = Convert.ToInt32(hfCallLogID.Value);
        }

        dataitem.granteeid = Convert.ToInt32(ddlgrant.SelectedValue);
        dataitem.DocName = txtDocName.Text;
        dataitem.isOnFile = chkisOnfile.Checked;
        dataitem.expDate = string.IsNullOrEmpty(txtExpDate.Text) ? (DateTime?)null : Convert.ToDateTime(txtExpDate.Text);
        if (string.IsNullOrEmpty(txtReceivedDate.Text))
        {
            dataitem.finishlevel = 1;
            dataitem.RecDate = (DateTime?)null;
        }
        else
        {
            dataitem.RecDate = Convert.ToDateTime(txtReceivedDate.Text);
            dataitem.finishlevel = 0;
        }
        dataitem.isActive = true;
        dataitem.Save();
        hfFileID.Value = "";

        if (string.IsNullOrEmpty(hfCallLogID.Value))
            dsDocs.SelectCommand = "SELECT id, isCompleted, Pk_tcallmainid, DocName, isOnFile, expDate, RecDate, CreatedBy, CreatedOn, ModifiedBy, "
       + " ModifiedOn, isActive FROM TCallDocs WHERE ((  "
        + " tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString())
       + "' ) AND granteeid = " + Convert.ToInt32(ddlgrant.SelectedValue) + "  AND isActive=1)";
        else
            dsDocs.SelectCommand = "SELECT id, isCompleted, Pk_tcallmainid, DocName, isOnFile, expDate, RecDate, CreatedBy, CreatedOn, ModifiedBy, "
            + " ModifiedOn, isActive FROM TCallDocs WHERE ((Pk_tcallmainid = "
            + Convert.ToInt32(hfCallLogID.Value) + " OR tmpid= '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString())
            + "' ) AND granteeid = " + Convert.ToInt32(ddlgrant.SelectedValue) + "  AND isActive=1)";
        gvDocs.DataBind();
    }


    //protected void dsGrantee_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    //{
    //    ddlgrant.Items.Clear();
    //    ddlgrant.Items.Add(new ListItem("Select One", ""));
    //    e.Command.Parameters["@CohortType"].Value = hfCohortType.Value;
      
    //}

   
    private DataSet getData(string calllogID, string granteeid)
    {
        string strQuery = "select * from tcallmain where id = " + calllogID + " AND granteeid = " + granteeid;
        
        DataSet rtnDS = new DataSet();
        string stcnn = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString;
        SqlConnection cnn = new SqlConnection(stcnn);
        cnn.Open();
        SqlDataAdapter adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCallMain");

        strQuery = "select * from TCallParticipants where Pk_tcallmainid = " + calllogID + " AND granteeid = " + granteeid;
        adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCallParticipants");

        strQuery = "select * from TCallSchools where Pk_tcallmainid = " + calllogID + " AND granteeid = " + granteeid;
        adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCallSchools");

        strQuery = "select * from TCallKeyPersonnel where Pk_tcallmainid = " + calllogID + " AND granteeid = " + granteeid;
        adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCallKeyPersonnel");

        foreach (DataRow drw in rtnDS.Tables["TCallKeyPersonnel"].Rows)
        {
            drw["LevelCommitpcnt"] = Convert.ToDouble(drw["LevelCommitpcnt"]) * 100;
        }

        strQuery = "select * from TCallDocs where Pk_tcallmainid = " + calllogID + " AND granteeid = " + granteeid;
        adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCallDocs");

        cnn.Close();

        return rtnDS;
    }

    private DataSet getQuarterData(string calllogID, string granteeid)
    {
        string strQuery = "select * from TCallamendments where id = " + calllogID + " AND granteeid = " + granteeid;

        DataSet rtnDS = new DataSet();
        string stcnn = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString;
        SqlConnection cnn = new SqlConnection(stcnn);
        cnn.Open();
        SqlDataAdapter adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCallamendments");

        strQuery = "select * from TCalladtParticipant where FK_tcalladm = " + calllogID;
        adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCalladtParticipant");

        strQuery = "select * from TCalladtPersonnel where FK_tcalladm = " + calllogID ;
        adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, "TCalladtPersonnel");

        foreach (DataRow drw in rtnDS.Tables["TCalladtPersonnel"].Rows)
        {
            drw["Commitment"] = Convert.ToDouble(drw["Commitment"]) * 100;
        }


        cnn.Close();

        return rtnDS;
    }

    protected void PrintQuarterPDFReport(object sender, EventArgs e)
    {
        int callLogId = Convert.ToInt32((sender as LinkButton).CommandArgument);

        DataSet sqlDS = getQuarterData(callLogId.ToString(), ddlgrant.SelectedValue);

        OpenPDF("QuarterReport.rpt", sqlDS);
    }

    protected void PrintPDFReport(object sender, EventArgs e)
    {
        int callLogId = Convert.ToInt32((sender as LinkButton).CommandArgument);

        DataSet sqlDS = getData(callLogId.ToString(), ddlgrant.SelectedValue);
       
        OpenPDF("PhoneCallReport.rpt", sqlDS);
    }
    private void OpenPDF(string downloadAsFilename, DataSet ds)
    {
        ReportDocument Rel = new ReportDocument();
        Rel.Load(Server.MapPath("./reports/" + downloadAsFilename));

        Rel.SetDataSource(ds);
        // Stop buffering the response
        Response.Buffer = false;
        // Clear the response content and headers
        Response.ClearContent();
        Response.ClearHeaders();

        ExportFormatType format = ExportFormatType.PortableDocFormat;

        string reportName = downloadAsFilename.Substring(0,downloadAsFilename.Length - 4);

        try
        {
            Rel.ExportToHttpResponse(format, Response, true, reportName);
        }
        catch (System.Threading.ThreadAbortException)
        {
            //ThreadException can happen for internale Response implementation
        }
        catch (Exception ex)
        {
            //other exeptions will be managed   
            throw;
        }

    }

    protected void rbtnCohortType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
        Session["TALogGranteeID"] = null;
        hfCohortType.Value = rbtnCohortType.SelectedValue;

        ddlgrant.Items.Clear();
        ddlgrant.Items.Add(new System.Web.UI.WebControls.ListItem("Select one", ""));
        Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
        Session["TALogGranteeID"] = null;
        LoadData();
        btnNewCall.Visible = false;
        btnNew2ndCall.Visible = false;
    }

    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();

        int granteeid = 0;

        if (Session["TALogGranteeID"] != null)
            granteeid = Convert.ToInt32(Session["TALogGranteeID"]);

        int cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        var grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeid && x.isActive == true && x.CohortType == cohortType);
        var schools = MagnetSchool.Find(x => x.GranteeID == granteeid && x.ReportYear == reportYear && x.isActive);

        var ContactInfos = TALogContact.Find(x => x.PK_GenInfoID == granteeid && x.isActive == true).OrderByDescending(odr => odr.id).OrderBy(ord => ord.ContactName);
        var Comms = TALogCommunication.Find(x => x.PK_GenInfoID == granteeid && x.isActive == true).OrderByDescending(ord => ord.id).OrderBy(ord => ord.PsnCommWith);

        var Uploadfiles = (from fileitm in db.TALogUploadFiles
                           join commitm in db.TALogCommunications
                           on fileitm.PK_CommID equals commitm.id
                           where commitm.PK_GenInfoID == granteeid
                           select fileitm).OrderByDescending(x => x.id);

        int reportid = 0;
        var Activities = TALogActParticipation.Find(x => x.granteeID == granteeid).OrderByDescending(ord => ord.id);
        //var prjdirector = ContactInfos.SingleOrDefault(x=>x.role_id== 2);
        var reports = GranteeReport.Find(x => x.GranteeID == granteeid && x.ReportPeriodID == reportPeriod && x.ReportType == false);
        if (reports.Count > 0)
            reportid = reports[0].ID;

        //var prjdirector = MagnetGranteeDatum.SingleOrDefault(x => x.GranteeReportID == reportid);
        //ltlGranteeName.Text = grantee == null || grantee.GranteeName == null ? "" : grantee.GranteeName;
        //if (prjdirector != null)
        //    ltlAdress.Text = prjdirector.Address + " " + prjdirector.City + ", " + prjdirector.State + " " + prjdirector.Zipcode;
        //else
        //    ltlAdress.Text = "";
        //ltlOfficer.Text = grantee == null || grantee.ProgramOfficerID == null ? "" : TALogProgramOffer.SingleOrDefault(x => x.id == Convert.ToInt32(grantee.ProgramOfficerID)).name;
        //ltlStatus.Text = grantee == null || grantee.GranteeStatusID == null ? "" : (grantee.GranteeStatusID == 1 ? "New" : "Veteran");
        //ltltotalschools.Text = schools.Count() > 0 ? schools.Count().ToString() : "";

        DataTable Contactdt = new DataTable();
        DataColumn colid = new DataColumn("id", System.Type.GetType("System.Int32"));
        DataColumn colRole = new DataColumn("Role");
        DataColumn colContactor = new DataColumn("Name");
        DataColumn colEmail = new DataColumn("Email");
        DataColumn colPhone = new DataColumn("Phone");
        DataColumn col2Phone = new DataColumn("Secondaryphone");
        DataColumn col2Modifiedby = new DataColumn("ModifiedBy");
        DataColumn col2Modifiedon = new DataColumn("ModifiedOn", System.Type.GetType("System.DateTime"));

        Contactdt.Columns.Add(colid);
        Contactdt.Columns.Add(colRole);
        Contactdt.Columns.Add(colContactor);
        Contactdt.Columns.Add(colEmail);
        Contactdt.Columns.Add(colPhone);
        Contactdt.Columns.Add(col2Phone);
        Contactdt.Columns.Add(col2Modifiedby);
        Contactdt.Columns.Add(col2Modifiedon);

        foreach (var itm in ContactInfos)
        {
            DataRow rw = Contactdt.NewRow();
            rw[colid] = itm.id;
            var role = TALogRole.SingleOrDefault(x => x.id == itm.role_id);
            if (role.id != 7)
            {
                rw[colRole] = role.rolename;
            }
            else
                rw[colRole] = itm.OtherText;

            rw[colContactor] = itm.ContactName;
            rw[colEmail] = itm.Email;
            rw[colPhone] = itm.PhoneNumber;
            rw[col2Phone] = itm.ScndPhoneNumber;
            rw[col2Modifiedby] = itm.ModifiedBy;

            if (itm.ModifiedOn != null)
                rw[col2Modifiedon] = itm.ModifiedOn;

            Contactdt.Rows.Add(rw);

        }


    }

    protected void ddlgrant_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlgrant.SelectedValue))
        {
            Session["TALogGranteeID"] = ddlgrant.SelectedValue;
            int numrows = TCallMain.Find(x => x.granteeid == Convert.ToInt32(ddlgrant.SelectedValue)).Count;
            if (numrows > 0)
                btnNewCall.Visible = false;
            else
                btnNewCall.Visible = true;

            btnNew2ndCall.Visible = true;
        }

        else
        {
            Session["TALogGranteeID"] = null;
            btnNewCall.Visible = false;
            btnNew2ndCall.Visible = false;
        }
        //ddlTcallmains.Items.Clear();
        //ddlTcallmains.Items.Add(new ListItem("Select one", ""));
 
        hfCohortType.Value = rbtnCohortType.SelectedValue;
        //gvGranteeList.DataBind();
        LoadData();

        //string filter = ddlgrant.SelectedValue;
        //dsTCalladt.SelectCommand = "SELECT * FROM [vwTCallamendments] WHERE ([granteeid] = " + filter + ")";
        gv2ndCall.DataBind();

    }
    ////// 2nd call function//////
    protected void On2ndNewPersonnel(object Sender, EventArgs e)
    {
        initialPersonnelFields(true);
        mpext2ndPersonnel.Show();
    }

    protected void OnEdit2ndPersonnel(object sender, EventArgs e)
    {
        hf2ndPersonnel.Value = (sender as LinkButton).CommandArgument;
        initialPersonnelFields(false);
        mpext2ndPersonnel.Show();
    }

    private void initialPersonnelFields(bool isReset)
    {
        if (Session["TCALLtmpID"] == null)
        {
            //GenerateUniqueRandomNumber randomClass = new GenerateUniqueRandomNumber(rangelow, rangehight);
            //Session["TCALLtmpID"] = randomClass.NewRandomNumber();
            Session["TCALLtmpID"] = HttpContext.Current.Session.SessionID;
        }

        if (isReset)
        {
            hf2ndPersonnel.Value = "";
            txt2ndPersonnelName.Text = "";
            txt2ndPersonnelRole.Text = "";
            txt2ndPersonnelCommit.Text = "";
            txt2ndPersonnelNote.Text = "";
        }
        else
        {
            int personid = Convert.ToInt32(hf2ndPersonnel.Value);
            var persondata = TCalladtPersonnel.SingleOrDefault(x => x.id == personid);
            txt2ndPersonnelName.Text = persondata.Name;
            txt2ndPersonnelRole.Text = persondata.Role;
            txt2ndPersonnelCommit.Text = (persondata.Commitment).ToString() + "%";
            txt2ndPersonnelNote.Text = persondata.note;
        }
    }

    protected void OnSave2ndCall(object Sender, EventArgs e)
    {
        TCallamendment dtble = new TCallamendment();

        if (!string.IsNullOrEmpty(hf2ndCallID.Value))
        {
            dtble = TCallamendment.SingleOrDefault(x => x.id == Convert.ToInt32(hf2ndCallID.Value));
        }
        else
        {
            dtble.CreatedBy = HttpContext.Current.User.Identity.Name;
            dtble.CreatedOn = DateTime.Now;
        }
        dtble.ModifiedBy = HttpContext.Current.User.Identity.Name;
        dtble.ModifiedOn = DateTime.Now;

        //dtble.FK_TCallMainId = Convert.ToInt32(ddlTcallmains.SelectedValue);
        dtble.GranteeId = Convert.ToInt32(ddlgrant.SelectedValue);
        dtble.PRAward = txt2ndPRaward.Text;
        dtble.Name = txt2ndGranteeName.Text;
        dtble.State = txt2ndState.Text;
        dtble.PD = txt2ndPdirector.Text;
        dtble.Phone = txt2ndPhone.Text;
        dtble.Email = txt2ndEmail.Text;
        dtble.PO = txt2ndPofficer.Text;
        dtble.Date = Convert.ToDateTime(txt2ndCallDate.Text);
        dtble.isOnCall = cbxIntrod.Checked;
        dtble.Hasimpacted = txtHasimpacted.Text;
        dtble.challengessuccesses = txtchallengessuccesses.Text;
        dtble.isNotFilled = cbxConfirm.Checked;
        dtble.Challenges = txtChallenges.Text;
        dtble.Budgetissues = txtBudgetissues.Text;
        dtble.Budgetchanges = txtBudgetchanges.Text;
        dtble.StudentRecruitment = txtStudentRecruitment.Text;
        dtble.ProfDev = txtProfDev.Text;
        dtble.Partnerships = txtPartnerships.Text;
        dtble.siteevaluation = txtsiteevaluation.Text;
        dtble.PerfObjectives = txtPerfObjectives.Text;
        dtble.Additionalcomments = txtAdditionalcomments.Text;
        dtble.Save();

        string parttmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
        string keyPersontmpid = parttmpid;
        IList<TCalladtParticipant> partData = TCalladtParticipant.Find(x => x.tmpid == parttmpid);
        foreach (var drwp in partData)
        {
            drwp.FK_tcalladm = dtble.id;
            drwp.tmpid = "";
            drwp.Save();
        }

        IList<TCalladtPersonnel> personData = TCalladtPersonnel.Find(x => x.tmpid == keyPersontmpid);
        foreach (var drwp in personData)
        {
            drwp.FK_tcalladm = dtble.id;
            drwp.tmpid = "";
            drwp.Save();
        }

        Session["TCALLtmpID"] = null;

        //
        //string filter = string.IsNullOrEmpty(ddlTcallmains.SelectedValue) ? "0" : ddlTcallmains.SelectedValue;
        //string filter = ddlgrant.SelectedValue;
        //dsTCalladt.SelectCommand = "SELECT * FROM [vwTCallamendments] WHERE ([granteeid] = " + filter + ")";
        gv2ndCall.DataBind();

        pnlList.Visible = true;
        pnl2ndCall.Visible = false;


    }

    protected void OnClose2ndCall(object Sender, EventArgs e)
    {
        string parttmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
        string keyPersontmpid = parttmpid;
        IList<TCalladtParticipant> partData = TCalladtParticipant.Find(x => x.tmpid == parttmpid);
        foreach (var drwp in partData)
        {
            drwp.Delete();
        }

        IList<TCalladtPersonnel> personData = TCalladtPersonnel.Find(x => x.tmpid == keyPersontmpid);
        foreach (var drwp in partData)
        {
            drwp.Delete();
        }

        Session["TCALLtmpID"] = null;
        //dsvwTCallMain.SelectCommand = "";
        //gvGranteeList.DataBind();
        pnl2ndCall.Visible = false;
        pnlList.Visible = true;



    }

    protected void OnNew2ndCallParticipant(object Sender, EventArgs e)
    {
        hf2ndParticipantID.Value = "";
        initialParticipantFields(true);
        mpext2ndParticipant.Show();
    }

    protected void OnEdit2ndParticipant(object sender, EventArgs e)
    {
        hf2ndParticipantID.Value = (sender as LinkButton).CommandArgument;
        initialParticipantFields(false);
        mpext2ndParticipant.Show();
    }

    private void initialParticipantFields(bool isReset)
    {
        if (Session["TCALLtmpID"] == null)
        {
            //GenerateUniqueRandomNumber randomClass = new GenerateUniqueRandomNumber(rangelow, rangehight);
            //Session["TCALLtmpID"] = randomClass.NewRandomNumber();
            Session["TCALLtmpID"] = HttpContext.Current.Session.SessionID;
        }

        if (isReset)
        {
            hf2ndParticipantID.Value = "";
            txt2ndParticipantName.Text = "";
            txt2ndParticipantRole.Text = "";
            txt2ndParticipantTitle.Text = "";
            txt2ndParticipantNote.Text = "";
        }
        else
        {
            int partid = Convert.ToInt32(hf2ndParticipantID.Value);
            var pdata = TCalladtParticipant.SingleOrDefault(x => x.id == partid);
            txt2ndParticipantName.Text = pdata.Name;
            txt2ndParticipantRole.Text = pdata.Role;
            txt2ndParticipantTitle.Text = pdata.Title;
            txt2ndParticipantNote.Text = pdata.note;
        }
    }

    //protected void ddlTcallmains_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(ddlTcallmains.SelectedValue))
    //    {
    //        foreach (var itm in TCallamendment.Find(x => x.GranteeId == Convert.ToInt32(ddlgrant.SelectedValue)))
    //        {
    //            if (itm.FK_TCallMainId == null)
    //            {
    //                itm.FK_TCallMainId = Convert.ToInt32(ddlTcallmains.SelectedValue);
    //                itm.Save();
    //            }
    //        }
    //    }

    //    string filter = ddlgrant.SelectedValue;
    //    dsTCalladt.SelectCommand = "SELECT * FROM [vwTCallamendments] WHERE ([granteeid] = " + filter + ")";
    //    gv2ndCall.DataBind();
    //}
    protected void btnNew2ndCall_Click(object sender, EventArgs e)
    {
        initalItemfields(true);
        pnl2ndCall.Visible = true;
        pnlList.Visible = false;
        gv2ndParticipant.DataBind();
        gv2ndPersonnel.DataBind();

    }
    protected void OnEdit2ndCall(object sender, EventArgs e)
    {
        hf2ndCallID.Value = (sender as LinkButton).CommandArgument;
        initalItemfields(false);
        pnl2ndCall.Visible = true;
        pnlList.Visible = false;

        ds2ndParticipant.SelectCommand = "SELECT [id], [FK_tcalladm], [Name], [Title], [Role], [note] FROM [TCalladtParticipant] WHERE ([FK_tcalladm] = "
                                            + hf2ndCallID.Value  + ")";

        gv2ndParticipant.DataBind();


        ds2ndKeyPersonnel.SelectCommand = "SELECT [id], [FK_tcalladm], [Name], [Role], [Commitment], [note] FROM [TCalladtPersonnel] WHERE ([FK_tcalladm] = "
                                            + hf2ndCallID.Value + ")";

        gv2ndPersonnel.DataBind();
    }

    private void initalItemfields(bool reSetFields)
    {
        int granteeId = Convert.ToInt32(ddlgrant.SelectedValue);
        var grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeId);
        var contacts = MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(ddlgrant.SelectedValue) && x.ContactType == 1);
        txt2ndPRaward.Text = grantee.PRAward;
        txt2ndGranteeName.Text = grantee.GranteeName;
        txt2ndState.Text = contacts[0].State;
        txt2ndPofficer.Text = grantee.ProgramOfficerID == null ? "" : TALogProgramOffer.SingleOrDefault(x => x.id == Convert.ToInt32(grantee.ProgramOfficerID)).name;


        if (reSetFields) //new call
        {
            hf2ndCallID.Value = "";
            txt2ndPhone.Text = "";
            txt2ndEmail.Text ="";
            txt2ndPdirector.Text = "";
            txt2ndCallDate.Text = "";
            cbxIntrod.Checked = false;
            txtHasimpacted.Text = "";
            txtchallengessuccesses.Text = "";
            cbxConfirm.Checked = false;
            txtChallenges.Text = "";
            txtBudgetissues.Text = "";
            txtBudgetchanges.Text = "";
            txtStudentRecruitment.Text = "";
            txtProfDev.Text = "";
            txtPartnerships.Text = "";
            txtsiteevaluation.Text = "";
            txtPerfObjectives.Text = "";
            txtAdditionalcomments.Text = "";
        }
        else
        {
            int editedItemID = Convert.ToInt32(hf2ndCallID.Value);
            var dataItems = TCallamendment.SingleOrDefault(x => x.id == editedItemID);

            txt2ndPRaward.Text = dataItems.PRAward;
            txt2ndState.Text = dataItems.State;
            txt2ndPofficer.Text = dataItems.PO;

            txt2ndPhone.Text = dataItems.Phone;
            txt2ndEmail.Text = dataItems.Email;
            txt2ndPdirector.Text = dataItems.PD;
            txt2ndCallDate.Text = dataItems.Date==null?"" : Convert.ToDateTime(dataItems.Date).ToShortDateString();
            cbxIntrod.Checked = dataItems.isOnCall==null? false : Convert.ToBoolean(dataItems.isOnCall);
            txtHasimpacted.Text = dataItems.Hasimpacted;
            txtchallengessuccesses.Text = dataItems.challengessuccesses;
            cbxConfirm.Checked = dataItems.isNotFilled == null ? false : Convert.ToBoolean(dataItems.isNotFilled);
            txtChallenges.Text = dataItems.Challenges;
            txtBudgetissues.Text = dataItems.Budgetissues;
            txtBudgetchanges.Text = dataItems.Budgetchanges;
            txtStudentRecruitment.Text = dataItems.StudentRecruitment;
            txtProfDev.Text = dataItems.ProfDev;
            txtPartnerships.Text = dataItems.Partnerships;
            txtsiteevaluation.Text = dataItems.siteevaluation;
            txtPerfObjectives.Text = dataItems.PerfObjectives;
            txtAdditionalcomments.Text = dataItems.Additionalcomments;
        }

    }
    protected void OnSave2ndKeyPersonnel(object sender, EventArgs e)
    {
        TCalladtPersonnel keyPerson = new TCalladtPersonnel();
        if (!string.IsNullOrEmpty(hf2ndPersonnel.Value))
        {
            keyPerson = TCalladtPersonnel.SingleOrDefault(x => x.id == Convert.ToInt32(hf2ndPersonnel.Value));
            keyPerson.ModifiedBy = HttpContext.Current.User.Identity.Name;
            keyPerson.ModifiedOn = DateTime.Now;
        }
        else
        {
            keyPerson.CreatedBy = HttpContext.Current.User.Identity.Name;
            keyPerson.CreatedOn = DateTime.Now;
        }

        keyPerson.tmpid =  Session["TCALLtmpID"]==null ? "-1" : Session["TCALLtmpID"].ToString();
        keyPerson.Name = txt2ndPersonnelName.Text.Trim();
        keyPerson.Role = txt2ndPersonnelRole.Text.Trim();
        keyPerson.Commitment = Convert.ToDouble(txt2ndPersonnelCommit.Text.Trim())/100;
        keyPerson.note = txt2ndPersonnelNote.Text.Trim();

        keyPerson.Save();

        if (string.IsNullOrEmpty(hf2ndCallID.Value))
            ds2ndKeyPersonnel.SelectCommand = "SELECT [id], [FK_tcalladm], [Name], [Role], [Commitment], [note] FROM [TCalladtPersonnel] WHERE ( "
                                                + " tmpid = '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "')";
        else
        {
            ds2ndKeyPersonnel.SelectCommand = "SELECT [id], [FK_tcalladm], [Name], [Role], [Commitment], [note] FROM [TCalladtPersonnel] WHERE ([FK_tcalladm] = "
                                                + hf2ndCallID.Value + " OR tmpid = '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "')";
        }

        gv2ndPersonnel.DataBind();

    }

    protected void OnSave2ndParticipant(object sender, EventArgs e)
    {
        TCalladtParticipant PartData = new TCalladtParticipant();
        if (!string.IsNullOrEmpty(hf2ndParticipantID.Value))
        {
            PartData = TCalladtParticipant.SingleOrDefault(x => x.id == Convert.ToInt32(hf2ndParticipantID.Value));
            PartData.ModifiedBy = HttpContext.Current.User.Identity.Name;
            PartData.ModifiedOn = DateTime.Now;
        }
        else
        {
            PartData.CreatedBy = HttpContext.Current.User.Identity.Name;
            PartData.CreatedOn = DateTime.Now;
        }

       
        PartData.tmpid = Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString();
        PartData.Name = txt2ndParticipantName.Text.Trim();
        PartData.Role = txt2ndParticipantRole.Text.Trim();
        PartData.Title = txt2ndParticipantTitle.Text.Trim();
        PartData.note = txt2ndParticipantNote.Text.Trim();

        PartData.Save();
        if (string.IsNullOrEmpty(hf2ndCallID.Value))
            ds2ndParticipant.SelectCommand = " SELECT [id], [FK_tcalladm], [Name], [Title], [Role], [note] FROM [TCalladtParticipant] WHERE ( tmpid = '"
                                              + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "')";
        else
        {
            ds2ndParticipant.SelectCommand = "SELECT [id], [FK_tcalladm], [Name], [Title], [Role], [note] FROM [TCalladtParticipant] WHERE ([FK_tcalladm] = "
                                                + hf2ndCallID.Value + " OR tmpid = '" + (Session["TCALLtmpID"] == null ? "-1" : Session["TCALLtmpID"].ToString()) + "')";
        }

        gv2ndParticipant.DataBind();

    }

   
    [WebMethod(EnableSession=true)]
    public static void BrowserCloseMethod()
    {
        clearTempData();
    }

    private static void clearTempData()
    {
        string tmpid = HttpContext.Current.Session.SessionID;
        ////////////phone calls /////////////////////////////////////////
        var participants = TCallParticipant.Find(x => x.tmpid == tmpid);
        var personnels = TCallKeyPersonnel.Find(x => x.tmpid == tmpid);
        var schools = TCallSchool.Find(x => x.tmpid == tmpid);
        var files = TCallDoc.Find(x => x.tmpid == tmpid);

        if (participants != null)
        {
            foreach (var item in participants)
            {
                item.Delete();
            }
        }

        if (personnels != null)
        {
            foreach (var item in personnels)
            {
                item.Delete();
            }
        }

        if (schools != null)
        {
            foreach (var item in schools)
            {
                item.Delete();
            }
        }

        if (files != null)
        {
            foreach (var item in files)
            {
                item.Delete();
            }
        }
        ///////////////////////////////////2nd quarter //////////////////

        IList<TCalladtParticipant> partData = TCalladtParticipant.Find(x => x.tmpid == tmpid);
        foreach (var drwp in partData)
        {
            drwp.Delete();
        }

        IList<TCalladtPersonnel> personData = TCalladtPersonnel.Find(x => x.tmpid == tmpid);
        foreach (var drwp in partData)
        {
            drwp.Delete();
        }


    }   


}