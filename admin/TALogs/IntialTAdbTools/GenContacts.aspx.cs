﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_TALogs_IntialTAdbTools_GenContacts : System.Web.UI.Page
{
    MagnetDBDB db = new MagnetDBDB();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        
        var MSAPContacts = from itm in db.MagnetGranteeContacts
                           select itm;

        foreach (var rcd in MSAPContacts)
        {
            TALogContact tacontact = new TALogContact();
            tacontact.PK_GenInfoID = rcd.LinkID;
            tacontact.ContactName = rcd.ContactFirstName + " " + rcd.ContactLastName;
            tacontact.Address1 = rcd.Address;
            tacontact.City = rcd.City;
            tacontact.State = rcd.State;
            tacontact.ZipCode = rcd.Zipcode;
            tacontact.PhoneNumber = rcd.Phone;
            tacontact.Email = rcd.Email;
            switch(rcd.ContactType)
            {
                case 1:
                    tacontact.role_id = 2;
                    break;
                case 2:
                    tacontact.role_id = 7;
                    tacontact.OtherText = "Other - Superintendent";
                    break;
                case 3:
                    tacontact.role_id = 3;
                    break;
                case 4:
                    tacontact.role_id = 7;
                    tacontact.OtherText = "Other - Parent Coordinator";
                    break;
                case 5:
                    tacontact.role_id = 7;
                    tacontact.OtherText = "Other - Title I Coordinator";
                    break;
                case 6:
                    tacontact.role_id = 4;
                    break;

            }
            tacontact.Save();
        }
    }
}