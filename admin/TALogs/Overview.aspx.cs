﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using MKB.TimePicker;
using Telerik.Web.UI;

public partial class admin_TALogs_Overview : System.Web.UI.Page
{
    int reportPeriod = 0, reportYear = 0;
    vwContextualfactorDataClassesDataContext schooldb = new vwContextualfactorDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["showNongrantee"] = null;
            Session["showNGFollowUp"] = null;
        }
        if (rbtnCohortType.SelectedIndex == 2) //non-grantee
        {
            pnlGrantee.Visible=false;
            pnlNongrantee.Visible=true;
        }
        else
        {
            pnlGrantee.Visible=true;
            pnlNongrantee.Visible=false;
        }
        //always getting year1 APR reports
        int cohortType=0;
        if (Session["TALogCohortType"] == null)
        {
            cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
            Session["TALogCohortType"] = cohortType;
        }
        else
            cohortType = Convert.ToInt32(Session["TALogCohortType"]);
        if (cohortType < 3)
        {
            var rptPeriods = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType).Last();

            reportPeriod = rptPeriods.ID;
            reportYear = Convert.ToInt32(rptPeriods.reportYear);
        }

        hfReportYearID.Value = reportYear.ToString();
        hfReportPeriodID.Value = reportPeriod.ToString();

        if (Session["TALogCohortType"] != null && Session["TALogCohortType"] != null && string.IsNullOrEmpty(Session["TALogCohortType"].ToString()))
            Session["TALogCohortType"] = null;
        if (Session["TALogGranteeID"] != null && Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
          Session["TALogGranteeID"] = null;

        if (!IsPostBack)
        { 
            rbtnCohortType.SelectedValue = Session["TALogCohortType"] == null ? "2" : Session["TALogCohortType"].ToString();
            Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
            ddlGrantees.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();
        }
        LoadData();
    }

  
    private void LoadData()
    { 
         MagnetDBDB db = new MagnetDBDB();
         
        int granteeid=0;

        if(Session["TALogGranteeID"]!=null)
            granteeid = Convert.ToInt32(Session["TALogGranteeID"]);
        
        int cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        var grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeid && x.isActive == true && x.CohortType == cohortType);
        var schools = MagnetSchool.Find(x => x.GranteeID == granteeid && x.ReportYear == reportYear && x.isActive);

        var ContactInfos = TALogContact.Find(x => x.PK_GenInfoID == granteeid && x.isActive == true).OrderByDescending(odr => odr.id).OrderBy(ord=>ord.ContactName);
        var Comms = TALogCommunication.Find(x => x.PK_GenInfoID == granteeid && x.isActive == true).OrderByDescending(ord => ord.id).OrderBy(ord=>ord.PsnCommWith);

        var Uploadfiles = (from fileitm in db.TALogUploadFiles
                          join commitm in db.TALogCommunications
                          on fileitm.PK_CommID equals commitm.id
                          where commitm.PK_GenInfoID == granteeid
                          select fileitm).OrderByDescending(x=>x.id);

        int reportid = 0;
        var Activities = TALogActParticipation.Find(x => x.granteeID == granteeid).OrderByDescending(ord => ord.id);
        //var prjdirector = ContactInfos.SingleOrDefault(x=>x.role_id== 2);
        var reports = GranteeReport.Find(x => x.GranteeID == granteeid && x.ReportPeriodID == reportPeriod && x.ReportType == false);
        if (reports.Count > 0)
            reportid = reports[0].ID;

        var prjdirector = MagnetGranteeDatum.SingleOrDefault(x => x.GranteeReportID == reportid);
        ltlGranteeName.Text = grantee == null || grantee.GranteeName == null ? "" : grantee.GranteeName;
        if (prjdirector != null)
            ltlAdress.Text = prjdirector.Address + " " + prjdirector.City + ", " + prjdirector.State + " " + prjdirector.Zipcode;
        else
            ltlAdress.Text = "";
        ltlOfficer.Text = grantee == null || grantee.ProgramOfficerID == null ? "" : TALogProgramOffer.SingleOrDefault(x => x.id == Convert.ToInt32(grantee.ProgramOfficerID)).name;
        ltlStatus.Text = grantee == null || grantee.GranteeStatusID ==null ? "" : (grantee.GranteeStatusID==1 ? "New" : "Veteran");
        ltltotalschools.Text = schools.Count() > 0 ? schools.Count().ToString() : "";

        DataTable Contactdt = new DataTable();
        DataColumn colid = new DataColumn("id", System.Type.GetType("System.Int32"));
        DataColumn colRole = new DataColumn("Role");
        DataColumn colContactor = new DataColumn("Name");
        DataColumn colEmail = new DataColumn("Email");
        DataColumn colPhone = new DataColumn("Phone");
        DataColumn col2Phone = new DataColumn("Secondaryphone");
        DataColumn col2Modifiedby = new DataColumn("ModifiedBy");
        DataColumn col2Modifiedon = new DataColumn("ModifiedOn", System.Type.GetType("System.DateTime"));

        Contactdt.Columns.Add(colid);
        Contactdt.Columns.Add(colRole);
        Contactdt.Columns.Add(colContactor);
        Contactdt.Columns.Add(colEmail);
        Contactdt.Columns.Add(colPhone);
        Contactdt.Columns.Add(col2Phone);
        Contactdt.Columns.Add(col2Modifiedby);
        Contactdt.Columns.Add(col2Modifiedon);

        foreach(var itm in ContactInfos)
        {
            DataRow rw = Contactdt.NewRow();
            rw[colid] = itm.id; 
            var role = TALogRole.SingleOrDefault(x=>x.id == itm.role_id);
            if (role.id != 7)
            {
                rw[colRole] = role.rolename;
            }
            else
                rw[colRole] = itm.OtherText;

            rw[colContactor] = itm.ContactName;
            rw[colEmail] = itm.Email;
            rw[colPhone] = itm.PhoneNumber;
            rw[col2Phone] = itm.ScndPhoneNumber;
            rw[col2Modifiedby] = itm.ModifiedBy;

            if(itm.ModifiedOn !=null)
                rw[col2Modifiedon] = itm.ModifiedOn;

            Contactdt.Rows.Add(rw);

        }
       

    }
    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
        {
            Session["TALogGranteeID"] = ddlGrantees.SelectedValue;
            Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
        }
        else
            Session["TALogGranteeID"] = null;

        LoadData();

        foreach (RadPanelItem rpi in RadPanelBar1.Items)
        {
            rpi.Expanded = false;
        }
    }
    protected void rbtnCohortType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnCohortType.SelectedIndex == 2) //non-grantee selected
        {
            pnlNongrantee.Visible = true;
            pnlGrantee.Visible = false;
            ddlComm.SelectedIndex = 0;
        }
        else
        {
            pnlNongrantee.Visible = false;
            pnlEditFollowup.Visible = false;
            pnlFollowupUpload.Visible = false;
            pnlGrantee.Visible = true; ;
            ddlGrantees.Items.Clear();
            ddlGrantees.Items.Add(new System.Web.UI.WebControls.ListItem("Select One", ""));
            Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
            Session["TALogGranteeID"] = null;
            LoadData();
        }
        foreach (RadPanelItem rpi in RadPanelBar1.Items)
        {
            rpi.Expanded = false;
        }
    }

    protected string getRoleName(int roleID)
    {
        TALogRole roleRcd = TALogRole.SingleOrDefault(x => x.id == roleID);
        return roleRcd.rolename;
    }
    
     protected void btnExport_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        this.Page.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }

     protected string getPhoneFormat(string phoneNo)
     {
         if(string.IsNullOrEmpty(phoneNo))
             return "";
         else
             return phoneNo;
     }

     private void intialFields(bool isEdit)
     {
         setDDLItems();
         if (!isEdit)
         {
            txtName.Text ="";
            txtRole.Text ="";
            txtOrganization.Text = "";
            txtPhone.Text ="";
            txtEmail.Text ="";
            txtNotes.Text ="";
            txtAddress.Text = "";
            txtCity.Text = "";
            ddlState.SelectedIndex = 0;
            hfNonegreanteeID.Value = "";

            ddlcontacttype.SelectedIndex = 0;
            ddlStatus.SelectedValue = "Edit Record";

            DateTextBox.Text = "";
            TimeSelector1.SetTime(0, 0, TimeSelector.AmPmSpec.AM);
            TimeSelector2.SetTime(0, 0, TimeSelector.AmPmSpec.AM);
            //PsnCommWithTextBox.Text = "";
            

            ddlGeneralComm.SelectedIndex = 0;

            OtherCommTextBox.Text = "";
            CommDescriptionTextBox.Text = "";
            ActionTakenTextBox.Text = "";
            RequiredDesTextBox.Text = "";
            ddlfollowupmember.SelectedIndex = 0;
            ddlRequestStatus.SelectedIndex = 0;

            txtFulfillDate.Text = "";


            RequestStatus_otherTextBox.Visible = false;
            RequestStatus_otherTextBox.Text = "";

            ContactType_otherTextBox.Visible = false;
            ContactType_otherTextBox.Text = "";

         }
         else
         {
            int ID = Convert.ToInt32(hfNonegreanteeID.Value);
            var item = TALogNoneGrantee.SingleOrDefault(x=>x.id ==ID);
            txtName.Text =item.NoneGranteeName;
            txtRole.Text =item.Role;
            txtOrganization.Text = item.Organization;
            txtPhone.Text =item.Phone;
            txtEmail.Text =item.Email;
            txtNotes.Text = item.Note;
            txtAddress.Text = item.Address;
            txtCity.Text = item.City;
            ddlState.SelectedValue = item.State;
            ddlStatus.SelectedValue = item.Status;

            ddlcontacttype.SelectedValue = item.ContactType;
            if (item.ContactType != null && item.ContactType.ToLower() == "other")
            {
                ContactType_otherTextBox.Visible = true;

            }
            else
            {
                ContactType_otherTextBox.Visible = false;
            }
            ContactType_otherTextBox.Text = item.ContactType_other;
            DateTextBox.Text = item.Date == null ? "" : Convert.ToDateTime(item.Date).ToShortDateString();
            DateTime dt1 = item.commTime == null ? DateTime.Now : Convert.ToDateTime(item.commTime);
            TimeSelector1.SetTime(dt1.Hour, dt1.Minute, dt1.ToString("tt") == "AM" ? TimeSelector.AmPmSpec.AM : TimeSelector.AmPmSpec.PM);
            DateTime dt2 = item.completetime == null ? DateTime.Now : Convert.ToDateTime(item.completetime);
            TimeSelector2.SetTime(dt2.Hour, dt2.Minute, dt2.ToString("tt") == "AM" ? TimeSelector.AmPmSpec.AM : TimeSelector.AmPmSpec.PM);
            //PsnCommWithTextBox.Text = item.PsnCommWith;
           
            ddlGeneralComm.SelectedValue = item.CommType;

            OtherCommTextBox.Text = item.OtherComm;
            CommDescriptionTextBox.Text = item.CommDescription;
            ActionTakenTextBox.Text = item.ActionTaken;
            RequiredDesTextBox.Text = item.RequiredDes;
            ddlfollowupmember.SelectedValue = item.FollowupStaff;
            ddlRequestStatus.SelectedValue = item.RequestStatus;
            if (ddlRequestStatus.SelectedValue.ToLower() == "other")
            {
                RequestStatus_otherTextBox.Visible = true;
            }
            else
            {
                RequestStatus_otherTextBox.Visible = false;
            }
            RequestStatus_otherTextBox.Text = item.RequestStatus_other;
            txtFulfillDate.Text = item.FulfillDate == null ? "" : Convert.ToDateTime(item.FulfillDate).ToString("d"); ;
         }
         setStaffmemberfields(); 
     }

     protected void AddNoneGrantee(object sender, EventArgs e)
     {
         
         intialFields(false);
         PopupPanel.Visible = true;
         mpeResourceWindow.Show();
     }

     protected void OnEditNoneGrantee(object sender, EventArgs e)
     {
         hfNonegreanteeID.Value = (sender as LinkButton).CommandArgument;
         intialFields(true);
         PopupPanel.Visible = true;
         mpeResourceWindow.Show();
     }

     protected void OnSaveNoneGrantee(object sender, EventArgs e)
     {
         TALogNoneGrantee nonegrantee = new TALogNoneGrantee();
         int ID = hfNonegreanteeID.Value==""?0 : Convert.ToInt32(hfNonegreanteeID.Value);

         if (ID != 0)
         {
             nonegrantee=TALogNoneGrantee.SingleOrDefault(x => x.id == ID);
             nonegrantee.ModifiedBy = HttpContext.Current.User.Identity.Name;
             nonegrantee.ModifiedOn = DateTime.Now;
         }
         else
         {
             nonegrantee.ModifiedBy = HttpContext.Current.User.Identity.Name;
             nonegrantee.ModifiedOn = DateTime.Now;
             nonegrantee.CreatedBy= HttpContext.Current.User.Identity.Name;
             nonegrantee.CreatedOn = DateTime.Now;
         }
         nonegrantee.NoneGranteeName = txtName.Text.Trim();
         nonegrantee.Role = txtRole.Text.Trim();
         nonegrantee.Organization = txtOrganization.Text.Trim();
         nonegrantee.Address = txtAddress.Text.Trim();
         nonegrantee.City = txtCity.Text.Trim();
         nonegrantee.State = ddlState.SelectedValue;
         nonegrantee.Phone = txtPhone.Text.Trim();
         nonegrantee.Email = txtEmail.Text.Trim();
         nonegrantee.Note = txtNotes.Text.Trim();
         nonegrantee.Status = ddlStatus.SelectedValue;

         nonegrantee.ContactType =  ddlcontacttype.SelectedValue;
         nonegrantee.ContactType_other = ContactType_otherTextBox.Text;
         nonegrantee.Date = string.IsNullOrEmpty(DateTextBox.Text) ? (DateTime?)null : Convert.ToDateTime(DateTextBox.Text);
         nonegrantee.commTime = DateTime.Parse(TimeSelector1.Hour + ":" + TimeSelector1.Minute + ":00 " + TimeSelector1.AmPm);
         nonegrantee.completetime = DateTime.Parse(TimeSelector2.Hour + ":" + TimeSelector2.Minute + ":00 " + TimeSelector2.AmPm);
         //nonegrantee.PsnCommWith = PsnCommWithTextBox.Text.Trim();

         nonegrantee.CommType = ddlGeneralComm.SelectedValue;

         nonegrantee.OtherComm = OtherCommTextBox.Text.Trim();
         nonegrantee.CommDescription = CommDescriptionTextBox.Text.Trim();
         nonegrantee.ActionTaken = ActionTakenTextBox.Text.Trim();
         nonegrantee.RequiredDes = RequiredDesTextBox.Text.Trim();
         nonegrantee.FollowupStaff = ddlfollowupmember.SelectedValue;
         nonegrantee.RequestStatus = ddlRequestStatus.SelectedValue;

         nonegrantee.RequestStatus_other = RequestStatus_otherTextBox.Text.Trim();
         nonegrantee.FulfillDate = string.IsNullOrEmpty(txtFulfillDate.Text) ? (DateTime?)null : Convert.ToDateTime(txtFulfillDate.Text);

         nonegrantee.Save();

        dsNoneGrantee.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc, NoneGranteeName) as item#, * FROM [TALogNoneGrantees] ";

         ddlComm.Items.Clear();
         ddlComm.Items.Add(new System.Web.UI.WebControls.ListItem("Select One", ""));
         dsNoneGrantee.Select(DataSourceSelectArguments.Empty);
         dsNoneGrantee.DataBind();
         ddlComm.DataBind();
         gvNoneGrantee.DataBind();
         pnlFollowupUpload.Visible = false;

     }

     protected void OnDeleteNoneGrantee(object sender, EventArgs e)
     {
         int ID = Convert.ToInt32((sender as LinkButton).CommandArgument);
         var item = TALogNoneGrantee.SingleOrDefault(x => x.id == ID);
         item.isActive = false;
         item.Delete();
         gvNoneGrantee.DataBind();
     }
     protected void ClosePan(object sender, EventArgs e)
     {
         mpeResourceWindow.Hide();
     }

     protected void ddlcontacttype_SelectedIndexChanged(object sender, EventArgs e)
     {
         if (ddlcontacttype.SelectedValue == "Other")
             ContactType_otherTextBox.Visible = true;
         else
         {
             ContactType_otherTextBox.Visible = false;
             ContactType_otherTextBox.Text = "";
         }
     }

     protected void ddlRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
     {
         if (ddlRequestStatus.SelectedValue == "Other")
             RequestStatus_otherTextBox.Visible = true;
         else
         {
             RequestStatus_otherTextBox.Visible = false;
             RequestStatus_otherTextBox.Text = "";
         }
     }

     private void setStaffmemberfields()
     {
         
         vwUserProfileDataContext db = new vwUserProfileDataContext();
         if (HttpContext.Current.User.IsInRole("ED"))
         {
             lblCntrstaffmember.Text = "ED staff member who will follow up:";
             lblstaffmember.Text = "ED staff member:";
         }
         else
         {
             lblCntrstaffmember.Text = "MSAP Center staff member who will follow up:";
             lblstaffmember.Text = "MSAP Center staff member:";
         }

         string userName = HttpContext.Current.User.Identity.Name;
         var usrdata = from r in db.vwUserProfiles
                       where r.Name == userName
                       select r;
         string FullName = "";
         foreach (var itm in usrdata)
         {
             FullName = (itm.FirstName + " " + itm.LastName).Trim();
             break;
         }
         if (string.IsNullOrEmpty(FullName))
             FullName = userName;
         txtstafffname.Text = FullName;

         //txtName.Text = ddlGrantees.SelectedItem.Text;
     }

     protected void setDDLItems()
     {
         //DropDownList ddlfollowupstaffmember = sender as DropDownList;
         ddlfollowupmember.Items.Clear();
         ddlfollowupmember.Items.Add(new System.Web.UI.WebControls.ListItem("Select One", ""));

         if (HttpContext.Current.User.IsInRole("ED"))
         {
             var data = TALogEDStaff.Find(x => x.name != null);
             foreach (TALogEDStaff itm in data)
             {
                 ddlfollowupmember.Items.Add(new System.Web.UI.WebControls.ListItem(itm.name, itm.name));
             }
         }
         else
         {
             var data = TALogMSAPStaff.Find(x => x.name != null);
             foreach (TALogMSAPStaff itm in data)
             {
                 ddlfollowupmember.Items.Add(new System.Web.UI.WebControls.ListItem(itm.name, itm.name));
             }
         }
         
     }

     protected void AddNewFollowup(object sender, EventArgs e)
    {
        txtDate.Text = "";
        TimeSelector3.SetTime(0, 0, TimeSelector.AmPmSpec.AM);
        txtStaffmember.Text = "";
        txtFollowupPerson.Text = "";
        edtAbsContent.Content = "";
        edtFollowupResult.Content = "";
        hfEditFollowupRcdID.Value = "";
        ddlStatus.SelectedValue = "Edit Record";

        openFollowupPan();

    }

    private void openFollowupPan()
    {
       pnlNongrantee.Visible = false;
       pnlFollowupUpload.Visible = false;
       pnlEditFollowup.Visible = true;
    }

    protected void CloseEditPan(object sender, EventArgs e)
    {
        pnlNongrantee.Visible = true;
        pnlFollowupUpload.Visible = true;
        pnlEditFollowup.Visible = false;
    }

    protected void OnEditFollowup(object sender, EventArgs e)
    {
        LinkButton btnEdite = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdite.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvFollowup.DataKeys[row.RowIndex].Values["ID"]);
        hfEditFollowupRcdID.Value = rcdid.ToString();

        TALogNGFollowUp followupRcd = TALogNGFollowUp.SingleOrDefault(x => x.id == rcdid);

        txtDate.Text = followupRcd.Date != null ? Convert.ToDateTime(followupRcd.Date).ToShortDateString() : "";
        DateTime dt3 = followupRcd.Time== null ? DateTime.Now : Convert.ToDateTime(followupRcd.Time);
        TimeSelector3.SetTime(dt3.Hour, dt3.Minute, dt3.ToString("tt") == "AM" ? TimeSelector.AmPmSpec.AM : TimeSelector.AmPmSpec.PM);
        txtStaffmember.Text = followupRcd.StaffMember;
        txtFollowupPerson.Text = followupRcd.FollowupPerson;
        edtAbsContent.Content = followupRcd.Notes;
        edtFollowupResult.Content = followupRcd.Result;
        ddlfupStatus.SelectedValue = followupRcd.Status;

        openFollowupPan();

    }
    protected void OnDeleteFollowup(object sender, EventArgs e)
    {
        LinkButton btnDelete = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnDelete.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvFollowup.DataKeys[row.RowIndex].Values["ID"]);
        TALogNGFollowUp rcd = TALogNGFollowUp.SingleOrDefault(x => x.id == rcdid);
        rcd.isActive = false;
        rcd.Save();

        gvFollowup.DataBind();

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            TAlog EDSender = new TAlog();
            EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "TALogNGFollowUp", DateTime.Now.ToShortDateString(), "Deleted");

        }

        CloseEditPan(this, e);
    }
    protected void OnSaveFollowup(object sender, EventArgs e)
    {
        TALogNGFollowUp followupRcd;

        if (string.IsNullOrEmpty(hfEditFollowupRcdID.Value))
        {
            followupRcd = new TALogNGFollowUp();
            followupRcd.CreatedBy = HttpContext.Current.User.Identity.Name;
            followupRcd.CreatedOn = DateTime.Now;
            followupRcd.ModifiedBy = HttpContext.Current.User.Identity.Name;
            followupRcd.ModifiedOn = DateTime.Now;
        }
        else
        {
            followupRcd = TALogNGFollowUp.SingleOrDefault(x => x.id == Convert.ToInt32(hfEditFollowupRcdID.Value));
            followupRcd.ModifiedBy = HttpContext.Current.User.Identity.Name;
            followupRcd.ModifiedOn = DateTime.Now;
        }

        followupRcd.Date = string.IsNullOrEmpty(txtDate.Text) ? (DateTime?)null : Convert.ToDateTime(txtDate.Text);
        followupRcd.Time = DateTime.Parse(TimeSelector3.Hour + ":" + TimeSelector3.Minute + ":00 " + TimeSelector3.AmPm);
        followupRcd.StaffMember = txtStaffmember.Text;
        followupRcd.FollowupPerson = txtFollowupPerson.Text;
        followupRcd.Notes = edtAbsContent.Content;
        followupRcd.Result = edtFollowupResult.Content;
        followupRcd.PK_CommID = string.IsNullOrEmpty(ddlComm.SelectedValue) ? (int?)null : Convert.ToInt32(ddlComm.SelectedValue);
        followupRcd.isActive = true;
        followupRcd.Status = ddlfupStatus.SelectedValue;

        followupRcd.Save();

        //if (Session["showNGFollowUp"] !=null && Session["showNGFollowUp"].ToString() == "1")
        //{
            dsFollowup.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc, StaffMember) as item#, * "
                                        + "  FROM [TALogNGFollowUp] WHERE ([PK_CommID] = " + ddlComm.SelectedValue + " )";

        //}
        //else
        //{
        //    dsFollowup.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc, StaffMember) as item#, * "
        //                                + "  FROM [TALogNGFollowUp] WHERE ([PK_CommID] = " + ddlComm.SelectedValue + " AND Status = 'Edit Record')";

        //}

        dsFollowup.Select(DataSourceSelectArguments.Empty);

        gvFollowup.DataBind();
        gvNoneGrantee.DataBind();

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            string action = string.IsNullOrEmpty(hfEditFollowupRcdID.Value) ? "Added" : "Updated";
            TAlog EDSender = new TAlog();
            EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "TALogNGFollowUp", DateTime.Now.ToShortDateString(), action);

        }
        string commid = string.IsNullOrEmpty(ddlComm.SelectedValue) ? "" : ddlComm.SelectedValue;
        CloseEditPan(this, e);
        //Button2.Visible = true;

    }

    protected void ddlComm_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["showNGFollowUp"] = null;
        Button2.Text = "Show All";

        if (string.IsNullOrEmpty(ddlComm.SelectedValue))
        {
            pnlFollowupUpload.Visible = false;
            btnNewFollowup.Visible = false;
            Button2.Visible = false;
            
        }
        else
        {
            pnlFollowupUpload.Visible = true;
            btnNewFollowup.Visible = true;
            //Button2.Visible = true;
            

            bool isMSAPstaff = HttpContext.Current.User.IsInRole("ED") ? false : true;
            int granteeID = Convert.ToInt32(ddlComm.SelectedValue);
            var data = TALogNGFollowUp.Find(x => x.PK_CommID == Convert.ToInt32(ddlComm.SelectedValue));

            if (data.Count > 0)
                //Button2.Visible = true
                ;
            else
                Button2.Visible = false;
            Button2.Text = "Show All";
        }
        gvFollowup.DataBind();
    }
    protected void showAll(object sender, EventArgs e)
    {
        //if (Session["showNongrantee"] == null || Session["showNongrantee"].ToString() == "0")
        //{
            btnShowContact.Text = "Show Edit Records";
            dsNoneGrantee.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc, NoneGranteeName) as item#, * FROM [TALogNoneGrantees] ";
            Session["showNongrantee"] = "1";
        //}
        //else
        //{
        //    btnShowContact.Text = "Show All";
        //    dsNoneGrantee.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc, NoneGranteeName) as item#, * FROM [TALogNoneGrantees] "
        //         + " WHERE Status='Edit Record'" ;
        //    Session["showNongrantee"] = "0";
        //}
        ddlComm.Items.Clear();
        ddlComm.Items.Add(new System.Web.UI.WebControls.ListItem("Select One", ""));
        dsNoneGrantee.Select(DataSourceSelectArguments.Empty);
        dsNoneGrantee.DataBind();
        ddlComm.DataBind();
        gvNoneGrantee.DataBind();
        pnlFollowupUpload.Visible = false;
    }
    protected void showAllfollowup(object sender, EventArgs e)
    {
        //if (Session["showNGFollowUp"] == null || Session["showNGFollowUp"].ToString() == "0")
        //{
            Button2.Text = "Show Edit Records";
            dsFollowup.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc, StaffMember) as item#, * "
                                        + "  FROM [TALogNGFollowUp] WHERE ([PK_CommID] = " + ddlComm.SelectedValue + " )  ";

            Session["showNGFollowUp"] = "1"; 
        //}
        //else
        //{
        //    Button2.Text = "Show All";
        //    dsFollowup.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc, StaffMember) as item#, * "
        //                                + "  FROM [TALogNGFollowUp] WHERE ([PK_CommID] = " + ddlComm.SelectedValue + " AND Status = 'Edit Record') ";

        //    Session["showNGFollowUp"] = "0";
        //}

        dsFollowup.Select(DataSourceSelectArguments.Empty);
        dsFollowup.DataBind();
        //gvNoneGrantee.DataBind();
    }

    protected string hasFollowup(object commID)
    {
        if (commID != null)
        {
            var follopUps = TALogNGFollowUp.Find(f => f.PK_CommID == Convert.ToInt32(commID));
            if (follopUps.Count > 0)
               // return "&#8730;";
                return follopUps.Count.ToString();
            else
                return "";
        }
        else
            return "";
    }
}
