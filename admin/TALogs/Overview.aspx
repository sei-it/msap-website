﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="Overview.aspx.cs" Inherits="admin_TALogs_Overview" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"  TagPrefix="cc1" %>
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
    <script src="../../js/jquery.maskedinput.js" type="text/javascript"></script> 
     <script type="text/javascript" src="../../js/jquery.timePicker.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <link href="../../css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    document.onkeydown = function (e) {
        // keycode for F5 function
        if (e.keyCode === 116) {
            return false;
        }
        
    };

    jQuery(function ($) {
        $("#<%= txtPhone.ClientID %>").mask("(999) 999-9999? x99999");
    });
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

     <h1>
       Communication Management 
    </h1>
<asp:RadioButtonList ID="rbtnCohortType" runat="server" RepeatColumns="3" 
        RepeatDirection="Horizontal" AutoPostBack="true" onselectedindexchanged="rbtnCohortType_SelectedIndexChanged" >
        <asp:ListItem Value="1" >Cohort 2010</asp:ListItem>
        <asp:ListItem Value="2" Selected="True">Cohort 2013</asp:ListItem> 
        <asp:ListItem Value="3" >Non-grantee</asp:ListItem>
    </asp:RadioButtonList>
<asp:Panel ID="pnlNongrantee" runat="server" Visible="false">
        <hr />
     <h3>Non-grantees</h3>
       <p style="text-align:left">
        <asp:Button ID="btnNoneGrantee" runat="server" Text="New Non-grantee" OnClick="AddNoneGrantee" 
               CausesValidation="false" CssClass="surveyBtn2" />&nbsp;&nbsp;&nbsp;
         <asp:Button ID="btnShowContact" runat="server" Text="Show All" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAll"  />
    </p>
    <asp:GridView ID="gvNoneGrantee" runat="server" DataSourceID="dsNoneGrantee" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
          AutoGenerateColumns="False" DataKeyNames="ID" CssClass="msapTbl" AllowPaging="true" PageSize="10">
        <Columns>
            <asp:BoundField DataField="item#" SortExpression="" HeaderText="Item #" />
            <asp:BoundField DataField="date"  SortExpression="" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:BoundField DataField="NoneGranteeName" SortExpression="" HeaderText="Name" />
            <asp:BoundField DataField="Role" SortExpression="" HeaderText="Role" />
            <asp:BoundField DataField="Organization" SortExpression="" HeaderText="Organization" />
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
            <asp:TemplateField>
            <HeaderTemplate>Follow up status</HeaderTemplate>
                <ItemTemplate>
                <asp:Label ID="LblhasFollowup" runat="server" Text='<%# hasFollowup(Eval("ID")) %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnNonegEdit" runat="server" Text="Edit" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnEditNoneGrantee"></asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDeleteNoneGrantee" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
    <br />

    <hr />
    <table>
     <tr>
     <td class="consult_popupTxt"><asp:literal runat="server" ID="ltlItemNo" Text="Item number:" /></td>
      <td>   <asp:DropDownList ID="ddlComm" runat="server" AutoPostBack="True" AppendDataBoundItems="true"
               DataSourceID="dsNoneGrantee" DataTextField="item#" DataValueField="id" CssClass="DropDownListCssClass"
          onselectedindexchanged="ddlComm_SelectedIndexChanged" >
               <asp:ListItem Value="">Select one</asp:ListItem>
           </asp:DropDownList>
      </td>  
     </tr>
     </table>
    </asp:Panel>  <%--end pnlNongrantee--%>
    <asp:Panel ID ="pnlFollowupUpload" runat="server" Visible="false">
  
       <h3>Follow up details</h3>
       <p style="text-align:left">
        <asp:Button ID="btnNewFollowup" runat="server" Text="New Follow up" OnClick="AddNewFollowup" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" />
        &nbsp;&nbsp;&nbsp;
         <asp:Button ID="Button2" runat="server" Text="Show All" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAllfollowup"  />
    </p>
    <asp:GridView ID="gvFollowup" runat="server" DataSourceID="dsFollowup" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
          AutoGenerateColumns="False" DataKeyNames="ID" CssClass="msapTbl" AllowPaging="true" PageSize="10">

        <Columns>
         
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
            <asp:BoundField DataField="Time" SortExpression="" HeaderText="Time" DataFormatString="{0:T}"/>
            <asp:BoundField DataField="StaffMember" SortExpression="" HeaderText="Staff member" />
            <asp:BoundField DataField="FollowupPerson" SortExpression="" HeaderText="Follow up person" />
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnFollowup" runat="server" Text="Edit" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnEditFollowup"></asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDeleteFollowup" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
    <hr />
    </asp:Panel>
    <asp:Panel ID="pnlEditFollowup" ViewStateMode="Disabled" runat="server" Visible="false">
        <div class="mpeDivConsult" >
            <div class="mpeDivHeaderConsult" >
                Add/Edit follow up
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" Text="Close" CausesValidation="false" CssClass="closeit_link" OnClick="CloseEditPan" />
                </span>
           
            </div>
            <div style="height:720px;overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                  
                    <tr>
                    <td class="consult_popupTxt">Date:</td>
                        <td>
                            <asp:TextBox ID="txtDate" runat="server" MaxLength="250" Width="390"></asp:TextBox>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtDate" ValidationGroup="gvpFollowup"
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate"  CssClass="AjaxCalendar" />
                        </td>
                    </tr>
                     <tr>
                    <td class="consult_popupTxt">Time:</td>
                        <td>
                           <cc2:timeselector ID="TimeSelector3" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                                    runat="server" MinuteIncrement="30" />
                            <%--<asp:TextBox ID="txtTime" runat="server" MaxLength="250" Width="390"></asp:TextBox>--%>
                            
                        </td>
                    </tr>
                     <tr>
                    <td class="consult_popupTxt">Staff member:</td>
                        <td>
                           
                            <asp:TextBox ID="txtStaffmember" runat="server" MaxLength="250" Width="390"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Follow up person:</td>
                        <td>
                           
                            <asp:TextBox ID="txtFollowupPerson" runat="server" MaxLength="250" Width="390"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Notes:</td>
                        <td>
                            <cc1:Editor ID="edtAbsContent" runat="server" Width="495" Height="100"  JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000" />

                        </td>
                    </tr>
                       <tr>
                     <td class="consult_popupTxt">Result of follow up:</td>
                        <td>
                            <cc1:Editor ID="edtFollowupResult" runat="server" Width="495" Height="100"  JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000" />

                        </td>
                    </tr>
                <tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlfupStatus" CssClass="DropDownListCssClass" runat="server"   SelectedValue='<%# Bind("Status") %>'>
         <asp:ListItem Value="">Select one</asp:ListItem>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem Selected="True">Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>
    <tr><td colspan="2" style="padding-top:50px;"></td></tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" ValidationGroup="gvpFollowup" CssClass="surveyBtn2" Text="Save Record" OnClick="OnSaveFollowup" />
                    </td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="surveyBtn2" CausesValidation="false" Text="Close Window" OnClick="CloseEditPan"/>
                    </td>
                </tr>
                <tr><td colspan="2" style="padding-top:50px;"></td></tr>
                </table>
            </div>
        </div>
    </asp:Panel>
      <%-- Panel --%>
    <asp:Panel ID="PopupPanel" runat="server"  >
       <div class="mpeDivConsult" style="width:740px" >
            <div class="mpeDivHeader" style="width:730px">
                Add/Edit Non-grantee
                 <span class="closeit" style="padding-right:20px;"><asp:LinkButton ID="lbtnClose" runat="server" Text="Close" OnClick="ClosePan"  CausesValidation="false" CssClass="closeit_link" />
                </span>
                </div>
<div style="height:680px;overflow: auto;">
                  <table >
                        <tr>
                            <td class="consult_popupTxt">
                                Name*:
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" MaxLength="500" Width="450"></asp:TextBox>
                            </td>
                            <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName" ErrorMessage="A non-grantee name is Required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="consult_popupTxt">
                                Role:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtRole" runat="server" MaxLength="50" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="consult_popupTxt">
                                Organization*:
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrganization" runat="server" MaxLength="250" Width="450"></asp:TextBox><br />
                            </td><td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtName" ErrorMessage="An organization is Required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="consult_popupTxt">
                                Address:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtAddress" runat="server" MaxLength="250" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="consult_popupTxt">
                                City:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtCity" runat="server" MaxLength="250" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="consult_popupTxt">
                                State*:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlState" runat="server" CssClass="DropDownListCssClass dropdownstate"  DataSourceID="dsState"  DataTextField="StateFullName" DataValueField="StateAbb" AppendDataBoundItems="true">
                                <asp:ListItem Value="">Select one</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlState" ErrorMessage="State is Required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="consult_popupTxt">
                                Phone:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="150" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="consult_popupTxt">
                                Email:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="150" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                         <tr>
        <td class="consult_popupTxt">Contact method:</td>
        <td  colspan="2">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
              <asp:DropDownList ID="ddlcontacttype" runat="server" AutoPostBack="true" CssClass="DropDownListCssClass ddlcontacttypechange"
                    onselectedindexchanged="ddlcontacttype_SelectedIndexChanged">
                        <asp:ListItem Value="">Select one</asp:ListItem>
                        <asp:ListItem>Phone</asp:ListItem>
                        <asp:ListItem>E-mail</asp:ListItem>
                        <asp:ListItem>Online TA request</asp:ListItem>
                        <asp:ListItem>Magnet connector post</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
          </asp:DropDownList>
            <asp:TextBox ID="ContactType_otherTextBox" runat="server" Visible="false" ></asp:TextBox>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
	 <tr>
        <td class="consult_popupTxt">Date:</td>
        <td  colspan="2">
		 <span><asp:TextBox ID="DateTextBox" runat="server"  />
		 <asp:RegularExpressionValidator ID="RegExpVldDate" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="DateTextBox" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="cldextDate" runat="server" Format="MM/dd/yyyy" TargetControlID="DateTextBox"  CssClass="AjaxCalendar" />
		</span>
        </td>
    </tr>
     
    <tr>
        <td class="consult_popupTxt">Time of communication:</td>

        <td colspan="2">
      <span style="float:left;"><cc2:timeselector ID="TimeSelector1"  AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                                    runat="server" MinuteIncrement="30" /></span>
       <span class="consult_popupTxt" style="padding-left: 100px; padding-right: 10px; float:left;"> Completed time:</span>

        <cc2:timeselector ID="TimeSelector2" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                                    runat="server" MinuteIncrement="30" /></td>

    </tr>

	<tr>
        <td class="consult_popupTxt"><asp:Label ID="lblstaffmember" runat="server"  /></td>
        <td colspan="2">
		 <asp:TextBox ID="txtstafffname" runat="server"  Enabled="false" CssClass="txtboxDisableClass"/>
        </td>
    </tr>
    
	<%-- <tr>
        <td class="consult_popupTxt">Caller:</td>
        <td colspan="2">
		 <asp:TextBox ID="PsnCommWithTextBox" runat="server" />
        </td>
    </tr>--%>
    <tr>
        <td class="consult_popupTxt">General communication:</td>
        <td colspan="2">
			  <asp:DropDownList ID="ddlGeneralComm" CssClass="DropDownListCssClass" runat="server" >
                        <asp:ListItem Value="">Select one</asp:ListItem>
                        <asp:ListItem>Regular update call</asp:ListItem>
                        <asp:ListItem>Grantee interview</asp:ListItem>
                        <asp:ListItem>Key personnel change</asp:ListItem>
          </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Other communication:</td>
        <td colspan="2">
		 <asp:TextBox ID="OtherCommTextBox" runat="server"  />
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">Description of communication/request:</td>
        <td colspan="2">
		 <asp:TextBox ID="CommDescriptionTextBox" runat="server" CssClass="text"  TextMode="MultiLine" Width="390" />
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Actions taken:</td>
        <td colspan="2">
		<asp:TextBox ID="ActionTakenTextBox" runat="server" CssClass="text" TextMode="MultiLine" Width="390" />
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">Description of follow up required:</td>
        <td colspan="2">
		 <asp:TextBox ID="RequiredDesTextBox" runat="server" CssClass="text"  TextMode="MultiLine" Width="390" />
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt"><asp:Label ID="lblCntrstaffmember" runat="server" /></td>
        <td colspan="2">
         <asp:DropDownList ID="ddlfollowupmember" CssClass="DropDownListCssClass" runat="server" AppendDataBoundItems="true" >
        <asp:ListItem Value="">Select one</asp:ListItem>
        </asp:DropDownList>
        </td>
    </tr>
                
    <tr>
        <td class="consult_popupTxt">
            Request status:</td>
        <td colspan="2">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
               <span>
            <asp:DropDownList ID="ddlRequestStatus" runat="server" AutoPostBack="true" 
                CssClass="ddlRequestStatuschange DropDownListCssClass"
                onselectedindexchanged="ddlRequestStatus_SelectedIndexChanged" >
                <asp:ListItem Value="">Select one</asp:ListItem>
                <asp:ListItem>Gathering additional information</asp:ListItem>
                <asp:ListItem>Awaiting response from ED</asp:ListItem>
                <asp:ListItem>Awaiting response from grantee</asp:ListItem>
                <asp:ListItem>Referred to consultant/subject matter expert</asp:ListItem>
                <asp:ListItem>Resolved by MSAP Center</asp:ListItem>
                <asp:ListItem>Resolved by MSAP Team</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>
            </span><span>
            <asp:TextBox ID="RequestStatus_otherTextBox" runat="server" Visible="false"/>
            </span>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">TA request fulfillment date:</td>
        <td colspan="2">
         <asp:TextBox ID="txtFulfillDate" runat="server"  />
          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtFulfillDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="MM/dd/yyyy" TargetControlID="txtFulfillDate"  CssClass="AjaxCalendar" />
        </td>
    </tr>


                        <tr>
                            <td style="vertical-align:top" class="consult_popupTxt">
                                Notes:
                            </td>
                            <td colspan="2">
                                
                                <asp:TextBox ID="txtNotes" runat="server" MaxLength="150" Height="100" TextMode="MultiLine" Width="500"></asp:TextBox>
                                
                            </td>
                        </tr>
                        <tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlStatus"  CssClass="DropDownListCssClass" runat="server" >
         <asp:ListItem Value="">Select one</asp:ListItem>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem >Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>
    <tr><td colspan="2" style="padding-top:50px;"></td></tr>

                        <tr>
                            <td>
                                <asp:Button ID="Button14" runat="server" CssClass="surveyBtn2" Text="Save Record" OnClick="OnSaveNoneGrantee" />
                            </td>
                            <td colspan="2">
                                <asp:Button ID="Button15" runat="server" CssClass="surveyBtn2" CausesValidation="false" Text="Close Window" />
                            </td>  
                        </tr>
                        <tr><td colspan="2" style="padding-top:50px;"></td></tr>
                    </table>
    </div>
        </div>
          
    </asp:Panel>
    <asp:LinkButton ID="LinkButton10" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton10" 
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="0" />
<asp:Panel ID="pnlGrantee" runat="server">
<hr />
    <div>
 
</div>
   <br />
    <asp:DropDownList ID="ddlGrantees" CssClass="DropDownListCssClass" runat="server" DataSourceID="dsGrantee" 
        AppendDataBoundItems="true" AutoPostBack="true"
        DataTextField="GranteeName" DataValueField="ID" 
        onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
        <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>

    <br /><br />

    <div class="titles">Grantee Overview</div>
    <br />
  <div class="tab_area">
    	<ul class="tabs">
        <li><asp:LinkButton ID="LinkButton3" PostBackUrl="TAReports.aspx" CausesValidation="false" ToolTip="Reports" runat="server">Page 8</asp:LinkButton></li>
         <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" CausesValidation="false" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton1" PostBackUrl="MSAPCenterMassCommunication.aspx" CausesValidation="false" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="MSAPCenterActParticipation.aspx" CausesValidation="false" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton9" PostBackUrl="Communications.aspx" CausesValidation="false" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContextualFactors.aspx" CausesValidation="false" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="ContactInfo.aspx" CausesValidation="false" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li class="tab_active">Page 1</li>
        </ul>
    </div>
    <br/>
    <hr />

               <table >
                
                <tr>
                    <td >
                        <strong>Grantee name:
                    </strong>
                    </td>
                    <td >
                        <asp:Literal ID="ltlGranteeName" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td valign="top" >
                        <strong>Grantee address:</strong>
                    </td>
                    <td >
                    <asp:Literal ID="ltlAdress" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td >
                        <strong>Program officer:</strong>
                        
                    </td>
                    <td >
                    <asp:Literal ID="ltlOfficer" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                        <strong>Grantee status:</strong></td>
                    <td >
                    <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
                    </td>
                </tr>
                 <tr>
                    <td>
                        
                        <strong>Total schools:</strong></td>
                    <td >
                    <asp:Literal ID="ltltotalschools" runat="server"></asp:Literal>
                    </td>
                </tr>
                </table>

<telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
          <telerik:RadPanelBar runat="server" ID="RadPanelBar1" Skin="Windows7" style="width:860px;">
               <Items>
                    <telerik:RadPanelItem Text="MSAP schools"  Expanded="false">
                         <ContentTemplate>
                         <h4><a href="ContextualFactors.aspx">Edit</a></h4>
                         <asp:GridView ID="gdvSchool" runat="server" AllowPaging="true" AllowSorting="true" DataSourceID="dsSchool"
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" >
        <Columns>
            <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School name" />
            <asp:BoundField DataField="SchoolType" SortExpression="" HeaderText="School type" />
            <asp:BoundField DataField="ProgramType" SortExpression="" HeaderText="Program type" />
            <asp:BoundField DataField="theme" SortExpression="" HeaderText="Theme" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on"  DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false" />
           
        </Columns>
    </asp:GridView>
                         </ContentTemplate>
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem Text="Contact information">
                         <ContentTemplate>
                          <h4><a href="ContactInfo.aspx">Edit</a></h4>
                    <asp:GridView ID="gvContact" runat="server" AllowPaging="true" AllowSorting="true" DataSourceID="dsContact"
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
           <%--     <asp:TemplateField>
            <HeaderTemplate>Role</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="Roles" runat="server" Text='<%# this.getRoleName(Eval("role_id").ToType<int>()) %>' />
            </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:BoundField DataField="rolename" SortExpression="" HeaderText="Role" />
            <asp:BoundField DataField="ContactName" SortExpression="" HeaderText="Contact name" />
            <asp:BoundField DataField="Email" SortExpression="" HeaderText="Email" />
            <%--<asp:BoundField DataField="PhoneNumber" SortExpression="" HeaderText="Phone" />--%>
            <asp:TemplateField>
            <HeaderTemplate>Phone</HeaderTemplate>
                <ItemTemplate>
                <asp:Literal ID="litPhone" runat="server" Text='<%# getPhoneFormat(Eval("PhoneNumber").ToString()) %>' />
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
            <HeaderTemplate>Secondary phone</HeaderTemplate>
                <ItemTemplate>
                <asp:Literal ID="litPhone" runat="server" Text='<%# getPhoneFormat(Eval("ScndPhoneNumber").ToString()) %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="ScndPhoneNumber" SortExpression="" HeaderText="Secondary Phone" />--%>
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnEditImage"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDeleteImage" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
    </asp:GridView>
                         </ContentTemplate>
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem Text="Recent communication activity"  Visible="false">
                              <ContentTemplate>
                              <h4><a href="Communications.aspx">Recent communication activity</a></h4>
                              <asp:GridView ID="gdvComm" runat="server" AllowPaging="true" AllowSorting="true" DataSourceID="dsComm"
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
            <asp:BoundField DataField="PsnCommWith" SortExpression="" HeaderText="Name" />
            <asp:BoundField DataField="MSAPStaffMember" SortExpression="" HeaderText="Staff Member" />
            <asp:BoundField DataField="CommType" SortExpression="" HeaderText="Comm. Type" />
            <asp:BoundField DataField="RequestStatus" SortExpression="" HeaderText="Status" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified By" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified On" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <%--<asp:BoundField DataField="isMSAPGrantee" SortExpression="" HeaderText="Is MSAP Grantee" />--%>
            <asp:TemplateField>
                <ItemTemplate>
                   <%-- <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditGrantee"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteGrantee" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
    </asp:GridView>
                              </ContentTemplate>
                    </telerik:RadPanelItem>
                              
                    <telerik:RadPanelItem Text="Files Uploaded"  Visible="false">
                        <ContentTemplate>
                        <h4><a href="Communications.aspx">Uploaded files</a></h4>
                        <asp:GridView ID="gvFileUpload" runat="server" AllowPaging="true"  DataSourceID="dsFileUpload"
            AllowSorting="true" AutoGenerateColumns="false" CssClass="msapTbl" 
            DataKeyNames="ID" PageSize="10">
            <Columns>
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression=""  DataFormatString="{0:MM/dd/yyyy}" />
               <asp:TemplateField>
                <HeaderTemplate>Uploaded File</HeaderTemplate>
                <ItemTemplate>
                <a href='<%# Eval("FileURL") %>' target="_blank" ><%# Eval("FileName")%></a>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ModifedBy" HeaderText="Modified By" 
                    SortExpression="" />
                <asp:BoundField DataField="ModifyOn" DataFormatString="{0:MM/dd/yyyy}" 
                    HeaderText="Modified On" HtmlEncode="false" SortExpression="" />
                <asp:TemplateField>
                    <ItemTemplate>
                      <%--  <asp:LinkButton ID="lbtnEditUpload" runat="server" CausesValidation="false" 
                            CommandArgument='<%# Eval("id") %>' OnClick="OnEditImage" Text="Edit"></asp:LinkButton>
                          <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDeleteImage" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem Text="Recent participation in MSAP Center activities"  Visible="false">
                         <ContentTemplate>
                         <h4><a href="MSAPCenterActParticipation.aspx">Recent participation in MSAP activities</a></h4>
                         <asp:GridView ID="gdvActParticipat" runat="server" AllowPaging="true" AllowSorting="true" DataSourceID="dsActParticipat"
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" >
        <Columns>
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" />
            <asp:BoundField DataField="name" SortExpression="" HeaderText="Participant name" />
            <asp:TemplateField>
            <HeaderTemplate>Role</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="Roles" runat="server" Text='<%# this.getRoleName(Eval("role_id").ToType<int>()) %>' />
            </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ParticipationType" SortExpression="" HeaderText="Type of Participation" />
            <asp:BoundField DataField="Modifiedby" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified On"  DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false" />

            <asp:TemplateField>
                <ItemTemplate>
                   <%-- <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditGC"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteGC" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
--%>                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
                         </ContentTemplate>
                    </telerik:RadPanelItem>
               </Items>
               <ExpandAnimation Type="None" />
               <CollapseAnimation Type="None" />
          </telerik:RadPanelBar>

        <div style="text-align: right; margin-top: 20px;">
           Page 1&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton7" PostBackUrl="ContactInfo.aspx" CausesValidation="false" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                       <asp:LinkButton ID="LinkButton13" PostBackUrl="ContextualFactors.aspx" CausesValidation="false" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton14" PostBackUrl="Communications.aspx" CausesValidation="false" ToolTip="Communications" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="MSAPCenterActParticipation.aspx" CausesValidation="false" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" PostBackUrl="MSAPCenterMassCommunication.aspx" CausesValidation="false" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton2" PostBackUrl="TCallIndex.aspx" CausesValidation="false" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
         &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton8" PostBackUrl="TAReports.aspx" CausesValidation="false" ToolTip="Reports" runat="server">Page 8</asp:LinkButton>
        </div>
</asp:Panel>
        <asp:HiddenField ID="hfReportYearID" runat="server" />
        <asp:HiddenField ID="hfReportPeriodID" runat="server" />
        <asp:HiddenField ID="hfNonegreanteeID" runat="server" />
        <asp:HiddenField ID="hfEditFollowupRcdID" runat="server" />
          <asp:SqlDataSource ID="dsGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [MagnetGrantees] WHERE (([isActive] = 1) AND MagnetGrantees.ID not in (39, 40, 41, 69,70,1001, 1002, 1003) AND [CohortType] = @CohortType) order by GranteeName"  >
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohortType" Name="CohortType"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsSchool" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
       SelectCommand="SELECT distinct *
            FROM vwTAlogcontextualfactors WHERE (PK_GenInfoid =@granteeID AND isActive =1  AND (ReportPeriodID =@ReportPeriodID) AND (ReportYear =@ReportYear))  ORDER BY [SchoolName]" >
             <SelectParameters>
            <asp:Parameter DefaultValue="true" Name="isActive" Type="Boolean" />
            <asp:ControlParameter ControlID="ddlGrantees" Name="GranteeID" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="hfReportYearID" Name="ReportYear" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hfReportPeriodID" Name="ReportPeriodID" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsContact" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [vwTALogContacts] WHERE (([isActive] = 1) AND ([PK_GenInfoID] = @PK_GenInfoID)) ORDER BY [rolename]">
        <SelectParameters>
            <asp:Parameter DefaultValue="true" Name="isActive" Type="Boolean" />
            <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsComm" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [TALogCommunications] WHERE (([isActive] = 1) AND ([PK_GenInfoID] = @PK_GenInfoID))">
        <SelectParameters>
            <asp:Parameter DefaultValue="true" Name="isActive" Type="Boolean" />
            <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsFileUpload" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT TALogUploadFiles.* FROM TALogUploadFiles INNER JOIN  TALogCommunications ON TALogUploadFiles.PK_CommID = TALogCommunications.id 
        WHERE ((TALogUploadFiles.isActive = 1) AND (TALogCommunications.PK_GenInfoID = @PK_GenInfoID)) ORDER BY TALogUploadFiles.id DESC">
        <SelectParameters>
            <asp:Parameter DefaultValue="true" Name="isActive" Type="Boolean" />
             <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID"  PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsACtParticipat" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [TALogActParticipation] WHERE ( ([granteeID] = @PK_GenInfoID)) ORDER BY [id] DESC">
        <SelectParameters>
            <asp:Parameter DefaultValue="true" Name="isActive" Type="Boolean" />
            <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsNoneGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT ROW_NUMBER() OVER (order by date desc, NoneGranteeName) as item#, * FROM [TALogNoneGrantees]  ">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsState" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [ID], [StateFullName], [StateAbb] FROM [MagnetStates]"></asp:SqlDataSource>
<asp:SqlDataSource ID="dsFollowup" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [id], [PK_CommID], [Date], [Time], [StaffMember], [FollowupPerson], [Notes], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [Status] FROM [TALogNGFollowUp] WHERE ([PK_CommID] = @PK_CommID ) order by date desc, StaffMember ">
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlComm" Name="PK_CommID"   PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>


