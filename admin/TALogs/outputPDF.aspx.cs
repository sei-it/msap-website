﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

public partial class admin_TALogs_outputPDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnPDF_Click(object sender, EventArgs e)
    {
        using (System.IO.MemoryStream Output = new MemoryStream())
        {
            Document document = new Document(PageSize.A4);
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, Output);
            document.Open();
            //PdfContentByte pdfContentByte = pdfWriter.DirectContent;
            PdfPTable table = new PdfPTable(2) { WidthPercentage = 100, RunDirection = PdfWriter.RUN_DIRECTION_LTR, ExtendLastRow = false };
            //table.DefaultCell.Border = Rectangle.NO_BORDER;
            float[] widths = new float[] { 40f, 550f };
            table.SetWidthPercentage(widths, PageSize.A4);
            PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));
            cell.Colspan = 3;
            //cell.Border = Rectangle.NO_BORDER;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            string imagepath = Server.MapPath(".") + " /images";
            iTextSharp.text.Image L = iTextSharp.text.Image.GetInstance(imagepath + "/checkbox_yes.png");
            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
            imgCell1.AddElement(new Chunk(L, 0, 0));
            imgCell1.VerticalAlignment = Element.ALIGN_TOP;
            //imgCell1.Border = Rectangle.NO_BORDER;
            table.AddCell(imgCell1);
            //table.AddCell("Col 1 Row 1");
            table.AddCell("Col 2 Row 1 Col 2 Row 1 Col 2 Row 1 Col 2 Row 1 Col 2 Row 1 Col 2 Row 1 Col 2 Row 1 Col 2 Row 1 Col 2 Row 1 Col 2 Row 1");
            //table.AddCell("Col 3 Row 1");
            L = iTextSharp.text.Image.GetInstance(imagepath + "/checkbox_clear.png");
            iTextSharp.text.pdf.PdfPCell imgCell2 = new iTextSharp.text.pdf.PdfPCell();
            imgCell2.AddElement(new Chunk(L, 0, 0));
            imgCell2.Border = Rectangle.NO_BORDER;
            imgCell2.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(imgCell2);

            //table.AddCell("Col 1 Row 2");
            table.AddCell("Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2 Col 2 Row 2");
            //table.AddCell("Col 3 Row 2");
            document.Add(table);

            document.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=mytest.pdf");
            Response.OutputStream.Write(Output.GetBuffer(), 0, Output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            Output.Close();

        }


    }
}