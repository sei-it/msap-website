﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_TALogs_mktRecruitTmplate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void textboxState(object sender, EventArgs e)
    {
        CheckBox mycbx = sender as CheckBox;
        string rowID = mycbx.ID.Replace("cbx", "");

        // find other text box
        TextBox mytxtBox = mycbx.Parent.FindControl("txtOther" + rowID) as TextBox;

        if (mycbx.Checked)
            mytxtBox.Visible = true;
        else
        {
            mytxtBox.Visible = false;
            mytxtBox.Text = "";
        }
    }

    protected void ddlmc14_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmc14.SelectedValue == "147")  //other
            txtmc15.Visible = true;
        else
        {
            txtmc15.Visible = false;
            txtmc15.Text = "";
        }
    }
    protected void ddlmc24_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmc24.SelectedValue == "247")  //other
            txtmc25.Visible = true;
        else
        {
            txtmc25.Visible = false;
            txtmc25.Text = "";
        }
    }
    protected void ddlmc34_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmc34.SelectedValue == "347")  //other
            txtmc35.Visible = true;
        else
        {
            txtmc35.Visible = false;
            txtmc35.Text = "";
        }
    }
    protected void ddlmc11_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmc11.SelectedValue == "115")  //other
            mcOther11.Visible = true;
        else
        {
            mcOther11.Visible = false;
            mcOther11.Text = "";
        }
    }
    protected void ddlmc21_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmc21.SelectedValue == "215")  //other
            mcOther21.Visible = true;
        else
        {
            mcOther21.Visible = false;
            mcOther21.Text = "";
        }
    }
    protected void ddlmc31_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmc31.SelectedValue == "315")  //other
            mcOther31.Visible = true;
        else
        {
            mcOther31.Visible = false;
            mcOther31.Text = "";
        }
    }
}