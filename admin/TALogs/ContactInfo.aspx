﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="ContactInfo.aspx.cs" Inherits="admin_TALogs_ContactInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script src="../../js/jquery.maskedinput.js" type="text/javascript"></script> 
    <script type="text/javascript">

        document.onkeydown = function (e) {
            // keycode for F5 function
            if (e.keyCode === 116) {
                return false;
            }

        };


        $(function () {
        if( $("select[name=ctl00$ContentPlaceHolder1$fvContact$ddlRole] option:selected").val() !="7")
            $("#ctl00_ContentPlaceHolder1_fvContact_Role_otherTextBox").hide();

            $(".change").change(function () {
                var value = $("select[name=ctl00$ContentPlaceHolder1$fvContact$ddlRole] option:selected").val();
                if (value == "7") {
                    $("#ctl00_ContentPlaceHolder1_fvContact_Role_otherTextBox").show();
                }
                else {
                    $("#ctl00_ContentPlaceHolder1_fvContact_Role_otherTextBox").hide();
                    $("#ctl00_ContentPlaceHolder1_fvContact_Role_otherTextBox").val('');
                }
            });
        });

        jQuery(function ($) {
            $("#ctl00_ContentPlaceHolder1_fvContact_txtPhone").mask("(999) 999-9999");
            $("#ctl00_ContentPlaceHolder1_fvContact_txt2ndPhone").mask("(999) 999-9999? x99999");
        });

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        
     <h1>
        Communication Management 
    </h1>

    <div>
 <asp:RadioButtonList ID="rbtnCohortType" runat="server" RepeatColumns="2" 
        RepeatDirection="Horizontal" AutoPostBack="true" onselectedindexchanged="rbtnCohortType_SelectedIndexChanged" >
        <asp:ListItem Value="1" >Cohort 2010</asp:ListItem>
        <asp:ListItem Value="2" Selected="True">Cohort 2013</asp:ListItem> 
    </asp:RadioButtonList>
</div>
   <br />
    <asp:DropDownList ID="ddlGrantees" runat="server" DataSourceID="dsGrantee" Visible="true"
        AppendDataBoundItems="true" AutoPostBack="true" CssClass="DropDownListCssClass"
        DataTextField="GranteeName" DataValueField="ID" 
        onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
        <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>
<%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    <br /><br />
    <div class="titles">General Information</div>
  <div class="tab_area">
    	<ul class="tabs">
         <li><asp:LinkButton ID="LinkButton3" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton></li>
         <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
                        <li><asp:LinkButton ID="LinkButton9" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContextualFactors.aspx" ToolTip="TA communication" runat="server">Page 3</asp:LinkButton></li>
            <li class="tab_active">Page 2</li>
            <li><asp:LinkButton ID="LinkButton10" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/>
    <asp:Panel ID="pnllistGrantee" runat="server">
    <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New Grantee" CssClass="surveyBtn2" OnClick="OnAddGrantee" Visible="false" />
    
    </p>
    <asp:GridView ID="gdvGrantee" runat="server" AllowPaging="True" 
            DataSourceID="dsgvGrantee" AutoGenerateColumns="False" DataKeyNames="ID" CssClass="msapTbl" 
        >
        <Columns>
            <asp:BoundField DataField="GranteeName" SortExpression="" HeaderText="Grantee name" />
            <asp:BoundField DataField="GranteeStatus" SortExpression="" HeaderText="Grantee status" />
            <asp:BoundField DataField="ProgramOfficer" SortExpression="" HeaderText="Program officer" />
            <asp:BoundField DataField="TAassociate" SortExpression="" HeaderText="TA associate" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
<%--            <asp:BoundField DataField="CohortType" SortExpression="" HeaderText="Is MSAP Grantee" />--%>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditGrantee"></asp:LinkButton>
                    <%--<asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteGrantee" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
        <HeaderStyle Wrap="False" />
        
    </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnllistContact" runat="server"  Visible="false">
   <br /><br />
    <hr />
    <div class="titles">Contact Information</div>
    <br /><br />
       <p style="text-align:left">
        <asp:Button ID="btnNewContact" runat="server" Text="New Contact" 
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="btnNewContact_Click"  />&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnShowContact" runat="server" Text="Show All" Visible="false" 
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAll"  />
           </p>
    <asp:GridView ID="gvContact" runat="server" AllowPaging="true" AllowSorting="false" 
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" DataSourceID="dsContactView"
        >
        <Columns>
            <%--<asp:BoundField DataField="role_id" SortExpression="" HeaderText="Role" />
             <asp:TemplateField>
             <HeaderTemplate>Role</HeaderTemplate>
                <ItemTemplate>
                    <asp:Literal ID="ltlRoleName" runat="server" Text='<%# this.getUserRole(this.Eval("id").ToType<int>()) %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:BoundField DataField="RoleName" SortExpression="" HeaderText="Role" />
            <asp:BoundField DataField="ContactName" SortExpression="" HeaderText="Contact name" />
            <asp:BoundField DataField="Email" SortExpression="" HeaderText="Email" />
             <asp:TemplateField>
            <HeaderTemplate>Phone</HeaderTemplate>
                <ItemTemplate>
                <asp:Literal ID="litPhone" runat="server" Text='<%# getPhoneWithExt(Eval("ID")) %>' />
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
            <HeaderTemplate>Secondary phone</HeaderTemplate>
                <ItemTemplate>
                <asp:Literal ID="lit2ndPhone" runat="server" Text='<%# Eval("ScndPhoneNumber") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnEditContact" runat="server" Text="Edit"  CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditContact"></asp:LinkButton>
                  <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete"  CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteContact" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlGrantee" runat="server" Visible="false">
    <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult">
                Add/Edit Grantee
                <span class="closeit"><asp:LinkButton ID="lbtnClose" runat="server" Text="Close" OnClick="ClosePan" CausesValidation="false" CssClass="closeit_link" />
                </span>
            </div>
            <asp:FormView ID="fvGrantee" runat="server" DefaultMode="Edit" 
                DataSourceID="dsGrantee" >
            <EditItemTemplate>
            <div style="overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                     <tr>
                    <td class="consult_popupTxt">Grantee name:</td>
                        <td>
                            <asp:TextBox ID="GranteeNameTextBox" runat="server" Text='<%# Bind("GranteeName") %>' Enabled="false" CssClass="txtboxDisableClass" />
                        </td>
                    </tr>
  
                         <tr >
                    <td class="consult_popupTxt">Grantee status:</td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="DropDownListCssClass" DataSourceID="dsStatus" DataTextField="statusName" DataValueField="id" AppendDataBoundItems="true"  SelectedValue='<%# Bind("GranteeStatusID") %>'>
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Grantee state:</td>
                        <td>
                            <asp:DropDownList ID="ddlGranteeState" AppendDataBoundItems="true" runat="server" Enabled="false" CssClass="DropDownListCssClass"
                             SelectedValue='<%# Bind("State") %>' DataSourceID="dsState" DataTextField="StateAbb" DataValueField="StateAbb" />
                        </td>
                    </tr> 
                    <tr>
                    <td class="consult_popupTxt">Program officer:</td>
                        <td> 
                            <asp:DropDownList ID="ddlOfficer" AppendDataBoundItems="true" runat="server" CssClass="DropDownListCssClass"
                             SelectedValue='<%# Bind("ProgramOfficerID") %>' DataSourceID="dsPOfficer" DataTextField="name" DataValueField="id">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                     </tr>
                    <tr>
                    <td class="consult_popupTxt">TA associate:</td>
                        <td> 
                            <asp:DropDownList ID="ddlTAassociate" AppendDataBoundItems="true" runat="server" CssClass="DropDownListCssClass" 
                            DataSourceID="dsMSAPStaff" DataTextField="name" DataValueField="id" SelectedValue='<%# Bind("TAassociateID") %>'>
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                     </tr>
                     <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                     <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr>
                  <td colspan='2'>
  
                        <asp:TextBox ID="ModifiedByTextBox" runat="server" Text='<%# Bind("ModifiedBy") %>' Visible="false"/>

                        <asp:TextBox ID="ModifiedOnTextBox" runat="server" Text='<%# Bind("ModifiedOn") %>' Visible="false"/>
                   </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnGranteeSave" runat="server" CssClass="surveyBtn2" Text="Save Record" CausesValidation="True" CommandName="Update"  />
                    </td>
                    <td >
                        <asp:Button ID="btnGranteeClose" runat="server" CssClass="surveyBtn2" CausesValidation="False" OnClick="ClosePan" Text="Close Window" />
                    </td>
                </tr>

                </table>

            </div>
            </EditItemTemplate>
            <InsertItemTemplate>
            <div style="overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                     <tr>
                    <td class="consult_popupTxt">Grantee name:</td>
                        <td>
                            <asp:TextBox ID="GranteeNameTextBox" runat="server" Text='<%# Bind("GranteeName") %>' Enabled="false" CssClass="txtboxDisableClass" />
                        </td>
                    </tr>
                
                         <tr>
                    <td class="consult_popupTxt">Grantee status:</td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="DropDownListCssClass" AppendDataBoundItems="true" DataSourceID="dsStatus" DataTextField="statusName" DataValueField="id" SelectedValue='<%# Bind("GranteeStatusID") %>'>
                            <asp:ListItem Value="">Select one</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Grantee state:</td>
                        <td>
                            <asp:DropDownList ID="ddlGranteeState" AppendDataBoundItems="true" runat="server" CssClass="DropDownListCssClass"
                             SelectedValue='<%# Bind("State") %>' DataSourceID="dsState" DataTextField="StateAbb" DataValueField="StateAbb" Enabled="false" />
                        </td>
                    </tr> 
                    <tr>
                    <td class="consult_popupTxt">Program officer:</td>
                        <td> 
                            <asp:DropDownList ID="ddlOfficer" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass"
                             DataSourceID="dsPOfficer" DataTextField="name" DataValueField="id" SelectedValue='<%# Bind("ProgramOfficerID") %>'>
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                     </tr>
                    <tr>
                    <td class="consult_popupTxt">TA Associate:</td>
                        <td> 
                            <asp:DropDownList ID="ddlTAassociate" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass"
                            DataSourceID="dsMSAPStaff" DataTextField="name" DataValueField="id" SelectedValue='<%# Bind("TAassociateID") %>' >
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                     </tr>
                <tr>
                  <td colspan='2'>
                        <asp:TextBox ID="CreatedByTextBox" runat="server" Text='<%# Bind("CreatedBy") %>' Visible="false"/>
                        <asp:TextBox ID="CreatedOnTextBox" runat="server" Text='<%# Bind("CreatedOn") %>' Visible="false"/> 
                   </td>
                </tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                 <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr>
                    <td>
                        <asp:Button ID="btnGranteeSave" runat="server" CssClass="surveyBtn2" Text="Save Record"  CausesValidation="True" CommandName="Insert"  />
                    </td>
                    <td >
                        <asp:Button ID="btnGranteeClose" runat="server" CssClass="surveyBtn2"  CausesValidation="False" OnClick="ClosePan"  Text="Close Window" />
                    </td>
                </tr>
                
                </table>
            </div>
            </InsertItemTemplate>
            </asp:FormView>
            
        
        </div>
    
    </asp:Panel>
   <%-- <asp:LinkButton ID="LinkButton8" runat="server"></asp:LinkButton>

    <ajaxToolkit:ModalPopupExtender ID="mpextGrantee" runat="server" TargetControlID="LinkButton8"
        PopupControlID="pnlEdtGrantee" DropShadow="true" OkControlID="btnGranteeClose" CancelControlID="btnGranteeClose"
        BackgroundCssClass="magnetMPE" Y="20" />--%>
  
    <asp:Panel ID="pnlContact" runat="server" Visible="false">
        <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult">
                Add/Edit contact
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" Text="Close" CausesValidation="false" OnClick="ClosePan" CssClass="closeit_link" />
                </span>
            </div>

          
            <div style="overflow: auto;">
            <asp:FormView ID="fvContact" runat="server" DataKeyNames="id" 
                    DataSourceID ="dsContact" DefaultMode="Edit" ondatabound="fvContact_DataBound" 
                   >
            <EditItemTemplate>
            <table style="padding:0px 10px 10px 10px;">
                  
                    <tr>
                    <td class="consult_popupTxt">Contact name*:</td>
                        <td>
                            <asp:TextBox ID="txtContactName" runat="server" MaxLength="500" Width="470" Text='<%# Bind("ContactName") %>' /><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="txtContactName"
                            ErrorMessage="A contact name is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Address1:</td>
                        <td>
                            <asp:TextBox ID="txtAddress1" runat="server" MaxLength="250" Width="470"  Text='<%# Bind("Address1") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Address2:</td>
                        <td>
                            <asp:TextBox ID="txtAddress2" runat="server" MaxLength="500" Width="470" Text='<%# Bind("Address2") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">City:</td>
                        <td>
                            <asp:TextBox ID="txtCity" runat="server" MaxLength="250" Width="470" Text='<%# Bind("City") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">State*:</td>
                        <td>
                            
                            <asp:DropDownList ID="ddlState" runat="server"  DataSourceID="dsState" DataTextField="StateAbb" CssClass="DropDownListCssClass"
                            DataValueField="StateAbb"  SelectedValue='<%# Bind("State") %>'>
                            </asp:DropDownList><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlState" InitialValue="" ForeColor="Red"
                             runat="server" ErrorMessage="Please select one of states."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Zip code:</td>
                        <td>
                            <asp:TextBox ID="txtZipcode" runat="server" MaxLength="250" Width="470" Text='<%# Bind("ZipCode") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Phone*:</td>
                        <td>
                            <asp:TextBox ID="txtPhone" runat="server" MaxLength="21" Width="470" Text='<%# Bind("PhoneNumber") %>' /><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtPhone" ForeColor="Red"
                            runat="server" ErrorMessage="A phone number is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Phone Ext.:</td>
                        <td>
                            <asp:TextBox ID="txtExt" runat="server" MaxLength="21" Width="470" Text='<%# Bind("Ext") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Secondary phone:</td>
                        <td>
                            <asp:TextBox ID="txt2ndPhone" runat="server" MaxLength="21" Width="470" Text='<%# Bind("ScndPhoneNumber") %>' /> 
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">E-mail*:</td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" Width="470" Text='<%# Bind("Email") %>' /><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtEmail" ForeColor="Red"
                            runat="server" ErrorMessage="An email is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                   <tr>
        <td class="consult_popupTxt">Role:</td>
        <td>
		<span>
		 <asp:DropDownList ID="ddlRole" runat="server" CssClass="DropDownListCssClass change"
                AppendDataBoundItems="true"  DataTextField="rolename" 
                DataValueField="id"  SelectedValue='<%# Bind("role_id") %>' OnDataBinding ="ddlRole_OnDataBinding" 
                >
         
         </asp:DropDownList>
		</span>
		<span>
		<asp:TextBox ID="Role_otherTextBox" runat="server" Text='<%# Bind("OtherText") %>' />
		</span>
        </td>
    </tr>
    <tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="DropDownListCssClass"   SelectedValue='<%# Bind("Status") %>'>
        <asp:ListItem Value="">Select one</asp:ListItem>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem >Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>
         <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>             
                <tr>
                    <td colspan='2'>
                     <asp:TextBox ID="CreatedByTextBox" runat="server" Text='<%# Bind("CreatedBy") %>' Visible="false" />

                    <asp:TextBox ID="CreatedOnTextBox" runat="server" Text='<%# Bind("CreatedOn") %>' Visible="false"/>

                    <asp:TextBox ID="ModifiedByTextBox" runat="server" Text='<%# Bind("ModifiedBy") %>' Visible="false" />
 
                    <asp:TextBox ID="ModifiedOnTextBox" runat="server" Text='<%# Bind("ModifiedOn") %>' Visible="false"/>
					</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="surveyBtn2" Text="Save Record" CausesValidation="True" CommandName="Update" />
                    </td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="surveyBtn2" Text="Close Window" OnClick="ClosePan" />
                    </td>
                </tr>
                </table>
            </EditItemTemplate>
            
            <InsertItemTemplate>
             <table style="padding:0px 10px 10px 10px;">
                  
                    <tr>
                    <td class="consult_popupTxt">Contact name*:</td>
                        <td>
                           
                            <asp:TextBox ID="txtContactName" runat="server" MaxLength="500" Width="470" Text='<%# Bind("ContactName") %>' /><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtContactName" ForeColor="Red"
                            ErrorMessage="A contact name is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Address1:</td>
                        <td>
                            <asp:TextBox ID="txtAddress1" runat="server" MaxLength="250" Width="470"  Text='<%# Bind("Address1") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Address2:</td>
                        <td>
                            <asp:TextBox ID="txtAddress2" runat="server" MaxLength="500" Width="470" Text='<%# Bind("Address2") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">City:</td>
                        <td>
                            <asp:TextBox ID="txtCity" runat="server" MaxLength="250" Width="470" Text='<%# Bind("City") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">State*:</td>
                        <td>
                            
                            <asp:DropDownList ID="ddlState" runat="server" CssClass="DropDownListCssClass"  DataSourceID="dsState" DataTextField="StateAbb" DataValueField="StateAbb"  SelectedValue='<%# Bind("State") %>'>
                            </asp:DropDownList><br />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlState" InitialValue="" ForeColor="Red"
                             runat="server" ErrorMessage="Please select one of states."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Zip code:</td>
                        <td>
                            <asp:TextBox ID="txtZipcode" runat="server" MaxLength="250" Width="470" Text='<%# Bind("ZipCode") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Phone*:</td>
                        <td>
                            <asp:TextBox ID="txtPhone" runat="server" MaxLength="250" Width="470" Text='<%# Bind("PhoneNumber") %>' /><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtPhone" ForeColor="Red"
                            runat="server" ErrorMessage="A phone number is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Phone Ext.:</td>
                        <td>
                            <asp:TextBox ID="txtExt" runat="server" MaxLength="21" Width="470" Text='<%# Bind("Ext") %>' />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Secondary phone:</td>
                        <td>
                            <asp:TextBox ID="txt2ndPhone" runat="server" MaxLength="250" Width="470" Text='<%# Bind("ScndPhoneNumber") %>' /> 
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">E-mail*:</td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" Width="470" Text='<%# Bind("Email") %>' /><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtEmail" ForeColor="Red"
                            runat="server" ErrorMessage="An email is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                   <tr>
        <td class="consult_popupTxt">Role:</td>
        <td>
		<span>
		 <asp:DropDownList ID="ddlRole" runat="server" DataSourceID="dsRole" CssClass="change DropDownListCssClass" AppendDataBoundItems="true"  DataTextField="rolename" DataValueField="id"  SelectedValue='<%# Bind("role_id") %>'>
         
         </asp:DropDownList>
		</span>
		<span>
		<asp:TextBox ID="Role_otherTextBox" runat="server" Text='<%# Bind("OtherText") %>' />
		</span>
        </td>
    </tr>
    <tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="DropDownListCssClass"  SelectedValue='<%# Bind("Status") %>'>
         <asp:ListItem Value="">Select one</asp:ListItem>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem Selected="True">Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>          
                <tr>
                    <td colspan='2'>
                     <asp:TextBox ID="CreatedByTextBox" runat="server" Text='<%# Bind("CreatedBy") %>' Visible="false" />

                    <asp:TextBox ID="CreatedOnTextBox" runat="server" Text='<%# Bind("CreatedOn") %>' Visible="false"/>

                    <asp:TextBox ID="ModifiedByTextBox" runat="server" Text='<%# Bind("ModifiedBy") %>' Visible="false" />
 
                    <asp:TextBox ID="ModifiedOnTextBox" runat="server" Text='<%# Bind("ModifiedOn") %>' Visible="false"/>
					</td>
                </tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="surveyBtn2" Text="Save Record" CausesValidation="True" CommandName="Insert" />
                    </td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="surveyBtn2" CausesValidation="false" Text="Close Window" OnClick="ClosePan" />
                    </td>
                </tr>
                </table>
            </InsertItemTemplate>    
            </asp:FormView>
            </div>
        </div>
    </asp:Panel>
   <%-- <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajaxToolkit:ModalPopupExtender ID="mpeContentWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="pnlContact" DropShadow="true" OkControlID="btnClose" CancelControlID="btnClose"
        BackgroundCssClass="magnetMPE" Y="20" />--%>
    
    <br />
     <asp:HiddenField ID="hfCreatedBy" runat="server" />
     <asp:HiddenField ID="hfCreatedOn" runat="server" />
     <asp:HiddenField ID="hfModifiedBy" runat="server" />
     <asp:HiddenField ID="hfModifiedOn" runat="server" />
     <asp:HiddenField ID="hfeditGranteeID" runat="server" />
     <asp:HiddenField ID="hfeditContactID" runat="server" />
     <asp:HiddenField ID="hfReportperiodID" runat="server" />

        <div style="text-align: right; margin-top: 20px;">
            <asp:LinkButton ID="LinkButton15" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton>
           Page 2&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton13" PostBackUrl="ContextualFactors.aspx" ToolTip="TA communication" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton14" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton7" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
          &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton8" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton>
          
        </div>
         <asp:SqlDataSource ID="dsDDLgrantees" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [MagnetGrantees] WHERE (([isActive] = 1) AND [CohortType] = @CohortType)"  >
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohortType" Name="CohortType"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="dsgvGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
         
         SelectCommand="SELECT MagnetGrantees.ID, MagnetGrantees.GranteeName, MagnetGrantees.PRAward, MagnetGrantees.NCESID, 
         MagnetGrantees.ProjectTitle, MagnetGrantees.Desegregation, MagnetGrantees.CreatedBy, MagnetGrantees.CreatedOn, 
         MagnetGrantees.ModifiedBy, MagnetGrantees.ModifiedOn, MagnetGrantees.isActive, MagnetGrantees.CohortType,  
         TAGranteeStatus.statusName as GranteeStatus, TALogMSAPStaff.name AS TAassociate, TALogProgramOffer.name AS ProgramOfficer 
         FROM MagnetGrantees 
         LEFT OUTER JOIN TALogProgramOffer ON MagnetGrantees.ProgramOfficerID = TALogProgramOffer.id 
         LEFT OUTER JOIN TALogMSAPStaff ON MagnetGrantees.TAassociateID = TALogMSAPStaff.id 
         LEFT OUTER JOIN TAGranteeStatus ON MagnetGrantees.GranteeStatusID = TAGranteeStatus.id 
         WHERE (MagnetGrantees.ID = @ID) AND (MagnetGrantees.isActive = 1) order by ID DESC " >
         <SelectParameters>
             <asp:ControlParameter ControlID="ddlGrantees" Name="ID" 
                 PropertyName="SelectedValue" Type="Int32" />
         </SelectParameters>
        
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="dsGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 

        
         SelectCommand="SELECT * FROM [vwTALogMagnetGrantees] WHERE (ReportPeriodID =@ReportPeriodID  AND [CohortType] = @CohortType AND isActive=1) order by GranteeName " 
         DeleteCommand="DELETE FROM vwTALogMagnetGrantees WHERE (id = @theID)" 
         InsertCommand="INSERT INTO vwTALogMagnetGrantees(GranteeStatusID, ProgramOfficerID, TAassociateID, CohortType, CreatedBy, CreatedOn) 
         VALUES (@GranteeStatusID, @ProgramOfficerID, @TAassociateID, @CohortType, @CreatedBy, @CreatedOn)" 
         
         UpdateCommand="UPDATE vwTALogMagnetGrantees SET  GranteeStatusID = @GranteeStatusID, 
         ProgramOfficerID = @ProgramOfficerID, TAassociateID = @TAassociateID, ModifiedBy = @ModifiedBy, ModifiedOn = @ModifiedOn WHERE id = @theID" 
         oninserting="dsGrantee_Inserting" onupdating="dsGrantee_Updating" 
         oninserted="dsGrantee_Inserted" onupdated="dsGrantee_Updated" 
         onselecting="dsGrantee_Selecting" >
             <DeleteParameters>
                 <asp:ControlParameter ControlID="hfeditGranteeID" Name="theID" PropertyName="Value" Type="Int32" />
             </DeleteParameters>
             <InsertParameters>
                 <asp:ControlParameter ControlID="hfReportperiodID" Name="ReportPeriodID" PropertyName="Value" Type="Int32" />
                 <asp:Parameter Name="GranteeName" />
                 <asp:Parameter Name="State" />
                 <asp:Parameter Name="GrantSize" />
                 <asp:Parameter Name="GranteeStatusID" />
                 <asp:Parameter Name="ProgramOfficerID" />
                 <asp:Parameter Name="TAassociateID" />
                 <asp:Parameter Name="CohortType" />
                 <asp:Parameter Name="CreatedBy" />
                 <asp:Parameter Name="CreatedOn" />
                 <asp:Parameter Name="ModifiedBy" />
                 <asp:Parameter Name="ModifiedOn" />
             </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohortType" Name="CohortType" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="hfReportperiodID" Name="ReportPeriodID" PropertyName="Value" Type="Int32" />
        </SelectParameters>
             <UpdateParameters>
                 <asp:ControlParameter ControlID="hfeditGranteeID" Name="theID" PropertyName="Value" Type="Int32" />
                 <asp:Parameter Name="GranteeName" />
                 <asp:Parameter Name="State" />
                 <asp:Parameter Name="GrantSize" />
                 <asp:Parameter Name="GranteeStatusID" />
                 <asp:Parameter Name="ProgramOfficerID" />
                 <asp:Parameter Name="TAassociateID" />
                 <asp:Parameter Name="CohortType" />
                 <asp:Parameter Name="ModifiedBy" />
                 <asp:Parameter Name="ModifiedOn" />
             </UpdateParameters>
    </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsRole" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [TALogRoles] WHERE isActive=1" >
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="dsState" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [MagnetStates] " >
    </asp:SqlDataSource>
      <asp:SqlDataSource ID="dsPOfficer" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [TALogProgramOffer]  WHERE isActive=1" >
    </asp:SqlDataSource>
<asp:SqlDataSource ID="dsContact" runat="server" 
         ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
         
         SelectCommand="SELECT id, PK_GenInfoID, ContactName, Status, Address1, Address2, City, State, ZipCode, PhoneNumber, Ext, ScndPhoneNumber, Email, role_id, OtherText, 
                        CreatedBy, CreatedOn, ModifiedBy, ModifiedOn FROM TALogContacts WHERE ID = @ContactID  " 
         oninserted="dsContact_Inserted" oninserting="dsContact_Inserting" 
         onupdated="dsContact_Updated" onupdating="dsContact_Updating" 
         InsertCommand="INSERT INTO TALogContacts(PK_GenInfoID, ContactName, Address1, Address2, City, State, ZipCode, PhoneNumber, Ext, ScndPhoneNumber, Email, 
         role_id, OtherText, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, Status) VALUES (@PK_GenInfoID, @ContactName, @Address1, @Address2, @City, @State, @ZipCode,
         @PhoneNumber, @Ext, @ScndPhoneNumber, @Email, @role_id, @OtherText, @CreatedBy, @CreatedOn, @ModifiedBy, @ModifiedOn, @Status)" 
         
         UpdateCommand="UPDATE TALogContacts SET PK_GenInfoID = @PK_GenInfoID, ContactName = @ContactName, Address1 = @Address1, Address2 = @Address2, 
         City = @City, State = @State, ZipCode = @ZipCode, PhoneNumber = @PhoneNumber, Ext=@Ext, ScndPhoneNumber = @ScndPhoneNumber, Email = @Email, role_id = @role_id, 
         OtherText = @OtherText, ModifiedBy = @ModifiedBy, ModifiedOn = @ModifiedOn, Status=@Status WHERE (id =@theID)" 
         onselecting="dsContact_Selecting">
    <InsertParameters>
        <asp:Parameter Name="Status" />
        <asp:Parameter Name="PK_GenInfoID" />
        <asp:Parameter Name="ContactName" />
        <asp:Parameter Name="Address1" />
        <asp:Parameter Name="Address2" />
        <asp:Parameter Name="City" />
        <asp:Parameter Name="State" />
        <asp:Parameter Name="ZipCode" />
        <asp:Parameter Name="PhoneNumber" />
        <asp:Parameter Name="Ext" />
        <asp:Parameter Name="ScndPhoneNumber" />
        <asp:Parameter Name="Email" />
        <asp:Parameter Name="role_id" />
        <asp:Parameter Name="OtherText" />
        <asp:Parameter Name="CreatedBy" />
        <asp:Parameter Name="CreatedOn" />
        <asp:Parameter Name="ModifiedBy" />
        <asp:Parameter Name="ModifiedOn" />
    </InsertParameters>
    <SelectParameters>
         <asp:ControlParameter ControlID="hfeditContactID" Name="ContactID" 
             PropertyName="Value" Type="Int32" />
    </SelectParameters>
     <UpdateParameters>
        <asp:Parameter Name="Status" />
         <asp:Parameter Name="PK_GenInfoID" />
         <asp:Parameter Name="ContactName" />
         <asp:Parameter Name="Address1" />
         <asp:Parameter Name="Address2" />
         <asp:Parameter Name="City" />
         <asp:Parameter Name="State" />
         <asp:Parameter Name="ZipCode" />
         <asp:Parameter Name="PhoneNumber" />
        <asp:Parameter Name="Ext" />
         <asp:Parameter Name="ScndPhoneNumber" />
         <asp:Parameter Name="Email" />
         <asp:Parameter Name="role_id" />
         <asp:Parameter Name="OtherText" />
         <asp:Parameter Name="ModifiedBy" />
         <asp:Parameter Name="ModifiedOn" />
         <asp:ControlParameter ControlID="hfeditContactID" Name="theID" PropertyName="Value" Type="Int32" />
    </UpdateParameters>
     </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsContactView" runat="server"
    ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
         
         SelectCommand="SELECT TALogContacts.id, PK_GenInfoID, ContactName, Address1, Address2, City, State, TALogRoles.rolename as RoleName,
ZipCode, PhoneNumber, ScndPhoneNumber, Email, role_id, OtherText, TALogContacts.Status,
CreatedBy, CreatedOn, ModifiedBy, ModifiedOn 
FROM TALogContacts JOIN TALogRoles ON
TALogContacts.role_id = TALogRoles.ID
WHERE (PK_GenInfoID = @granteeID) order by TALogRoles.rolename" >
    <SelectParameters>
         <asp:ControlParameter ControlID="ddlGrantees" Name="granteeID" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsMSAPStaff" runat="server" 
         ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
         SelectCommand="SELECT [id], [name], [notes] FROM [TALogMSAPStaff]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsStatus" runat="server" 
         ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
         SelectCommand="SELECT * FROM [TAGranteeStatus] WHERE ([isActive] = @isActive)">
        <SelectParameters>
            <asp:Parameter DefaultValue="true" Name="isActive" Type="Boolean" />
        </SelectParameters>
     </asp:SqlDataSource>
</asp:Content>

