﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="MSAPCenterActParticipation.aspx.cs" Inherits="admin_TALogs_MSAPCenterActParticipation" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
  
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
    <script type="text/javascript" src="../../js/jquery.timePicker.js"></script>
    <link href="../../css/timePicker.css" rel="Stylesheet" type="text/css" />

    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <link href="../../css/calendar.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">

         $(function () {


             $("#ctl00_ContentPlaceHolder1_txtTimeAct").timePicker({
                 //startTime: "00.00",  // Using string. Can take string or Date object.
                 //endTime: new Date(0, 0, 0, 15, 30, 0),  // Using Date object.
                 //endTime: "23.59",
                 //show24Hours: false,
                 //separator: '.',
                 step: 10
             });

             $("#ctl00_ContentPlaceHolder1_txtEnddate").timePicker({
                 //startTime: "00.00",  // Using string. Can take string or Date object.
                 //endTime: new Date(0, 0, 0, 15, 30, 0),  // Using Date object.
                 //endTime: "23.59",
                 //show24Hours: false,
                 //separator: '.',
                 step: 10
             });

             if ($("select[name=ctl00$ContentPlaceHolder1$ddlRole] option:selected").val() != "7")
                 $("#ctl00_ContentPlaceHolder1_Role_otherTextBox").hide();
             else
                 $("#ctl00_ContentPlaceHolder1_Role_otherTextBox").show();
             if ($("select[name=ctl00$ContentPlaceHolder1$ddlParticipationType] option:selected").val() != "Other")
                 $("#ctl00_ContentPlaceHolder1_txtTypeother").hide();
             else
                 $("#ctl00_ContentPlaceHolder1_txtTypeother").show();

             //role
             $(".Rolechange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$ddlRole] option:selected").val();
                 if (value == "7") {
                     $("#ctl00_ContentPlaceHolder1_txtRoleOther").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_txtRoleOther").hide();
                     $("#ctl00_ContentPlaceHolder1_txtRoleOther").val('');
                 }
             });

             //type change 
             $(".typeChange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$ddlParticipationType] option:selected").val();
                 if (value == "Other") {
                     $("#ctl00_ContentPlaceHolder1_txtTypeother").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_txtTypeother").hide();
                     $("#ctl00_ContentPlaceHolder1_txtTypeother").val('');
                 }
             });
         });
     </script>

     <style type="text/css">
    .wrap { white-space: normal; width: 100px; }
</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <h1>
        Communication Management 
    </h1>
  <%--  <table runat="server" visible="false">
    <tr>
    <td><strong>MSAP Grantee:</strong></td>
    <td> <asp:RadioButtonList ID="rbtnIsMSAPGrantee" runat="server" RepeatColumns="2" 
        RepeatDirection="Horizontal" AutoPostBack="true" 
            onselectedindexchanged="rbtnIsMSAPGrantee_SelectedIndexChanged">
        <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
        <asp:ListItem Value="0">No</asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    </table>
   
    <asp:DropDownList ID="ddlGrantees" runat="server" DataSourceID="dsGrantee" Visible="false"
        AppendDataBoundItems="true" AutoPostBack="true"
        DataTextField="GranteeName" DataValueField="ID" 
        onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
        <asp:ListItem Value="">Select On</asp:ListItem>
    </asp:DropDownList>--%>
    <br /><br />

    <div class="titles">Participation Information</div>
  <div class="tab_area">
    	<ul class="tabs">
            <li><asp:LinkButton ID="LinkButton3" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton></li>
           <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li class="tab_active">Page 5</li>
                        <li><asp:LinkButton ID="LinkButton9" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContextualFactors.aspx" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton10" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/>
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<asp:Panel ID="pnlEventAct" runat="server">
 <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New Event Activity" CssClass="surveyBtn2" OnClick="AddnewRecord" />
    &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnShowAllAct" runat="server" Text="Show All" Visible="false" 
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAllAct"  />
    </p>
    <asp:GridView ID="gdvEventActivity" runat="server" AllowPaging="true" PageSize ="10"
        EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
        DataSourceID="dsEventActivity" AutoGenerateColumns="False" 
        DataKeyNames="ID" CssClass="msapTbl" onpageindexchanging="gdvEventActivity_PageIndexChanging" >
        <Columns>
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Event Date"  DataFormatString="{0:MM/dd/yyyy}" />
            <asp:BoundField DataField="Title" SortExpression="" HeaderText="Event Title" />
              
            <asp:BoundField DataField="ParticipationType" SortExpression="" HeaderText="Event Type" />
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
            <asp:BoundField DataField="Createdby" SortExpression="" HeaderText="Created by" />
            <asp:BoundField DataField="CreatedOn" SortExpression="" HeaderText="Created On"  DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false" />

            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditEventActivity"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteEventActivity" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
 
          <br/>
     <br /><br />
    <hr />
     <table runat="server" id="eventddl">
     <tr>
     <td class="consult_popupTxt"><asp:literal runat="server" ID="ltlEvent" Text="Events:" /></td>
      <td>   <asp:DropDownList ID="ddlEvents" runat="server" AutoPostBack="True" CssClass="DropDownListCssClass" 
              DataTextField="Title" DataValueField="id" 
              onselectedindexchanged="ddlEvents_SelectedIndexChanged1"  >
           </asp:DropDownList>
      </td>  
     </tr>
     </table>

     <p style="text-align:left">
        <asp:Button ID="btnNewParticipant" runat="server" Text="New Participant" CssClass="surveyBtn2" OnClick="AddnewParticipant" />
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnShowParticipant" runat="server" Text="Show All" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAllParticipant"  />
    </p>
    <p>
        <asp:Literal ID="ltlTotal" runat="server"></asp:Literal>
    </p>
    <asp:GridView ID="gvParticipant" runat="server" AllowPaging="true" AllowSorting="false" DataSourceID="dsActParticipation"
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
            <asp:BoundField DataField="name" SortExpression="" HeaderText="Participant name" />
              <asp:TemplateField>
             <HeaderTemplate>Role</HeaderTemplate>
                <ItemTemplate>
                    <asp:Literal ID="ltlRoleName" runat="server" Text='<%# this.getUserRole(this.Eval("id").ToType<int>()) %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
            <HeaderTemplate>Grantee Name</HeaderTemplate>
            <ItemTemplate>
            <asp:Literal ID="ltlGranteeName" runat="server" Text='<%# this.getGranteeName(this.Eval("id").ToType<int>()) %>'></asp:Literal>
            </ItemTemplate>
            </asp:TemplateField>
          <%--  <asp:BoundField DataField="ParticipationType" SortExpression="" HeaderText="Participation Role" />
            <asp:BoundField DataField="Modifiedby" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified On"  DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false" />--%>
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditActParticipation"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteActParticipation" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
</asp:Panel>
<asp:Panel ID="PopupPanel" runat="server" Visible="false" >
        <div class="mpeDivConsult" style="width: 655px;">
            <div class="mpeDivHeaderConsult" style="width: 640px;">
                Add/Edit Event Activity
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" CausesValidation="false" Text="Close" CssClass="closeit_link" OnClick="CloseEditPan" />
                </span>
            </div>
            <div style="height:410px; overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                  
                 <%--   <tr>
                    <td class="consult_popupTxt">Grantee name:</td>
                        <td>
                            <asp:TextBox ID="txtGranteeName" runat="server" MaxLength="250" Width="470" Enabled="false" />
                        </td>
                    </tr>--%>
                    <tr>
                    <td class="consult_popupTxt">Activity title:</td>
                    <td><asp:TextBox ID="txtActTitle" runat="server" Width="343"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtActTitle" ErrorMessage="Activity title is required"></asp:RequiredFieldValidator>
                    </td>
                    </tr>
                     <tr>
                    <td class="consult_popupTxt">Activity type:</td>
                        <td> 
                         <asp:DropDownList ID="ddlParticipationType" runat="server" CssClass="DropDownListCssClass typeChange" AppendDataBoundItems="true" >
                         <asp:ListItem Value="">Select one</asp:ListItem>
                          <asp:ListItem Value="Webinar">Webinar</asp:ListItem>
                          <asp:ListItem Value="Project directors meeting">Project directors meeting</asp:ListItem>
                          <asp:ListItem Value="Community of Practice post">Community of practice post</asp:ListItem>
                          <asp:ListItem Value="Live chat">Live chat</asp:ListItem>
                          <asp:ListItem Value="Conference call">Conference call</asp:ListItem>
                          <asp:ListItem Value="Subject matter expert consultation">Subject matter expert consultation</asp:ListItem>
                          <asp:ListItem Value="Other">Other</asp:ListItem>
                         </asp:DropDownList>
                          <asp:TextBox ID="txtTypeother" runat="server" Width="248px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="ddlParticipationType" 
                            runat="server" InitialValue="" Display="Dynamic" 
                            ErrorMessage="Activity type is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Activity start date:</td>
                        <td>
                        <asp:TextBox ID="txtActStartDate" runat="server" MaxLength="80"  />
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtActStartDate" ErrorMessage="Activity start date is required"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtActStartDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtActStartDate"  CssClass="AjaxCalendar" />
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Activity end date:</td>
                        <td>
                        <asp:TextBox ID="txtActEnddate" runat="server" MaxLength="80"  />
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtActEnddate" ErrorMessage="Activity date is required"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtActEnddate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy" TargetControlID="txtActEnddate"  CssClass="AjaxCalendar" />
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Start time:</td>
                      <%--  <td> 
                        <asp:TextBox ID="txtTimeAct" runat="server" />
                        </td>--%>
                        <td >    
	                       <cc2:timeselector ID="TimeSelector1" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                            runat="server" MinuteIncrement="30" />
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">End time:</td>
                        <%--<td> 
                        <asp:TextBox ID="txtEnddate" runat="server" />
                        </td>--%>
                         <td >    
	                       <cc2:timeselector ID="TimeSelector2" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                            runat="server" MinuteIncrement="30" />
                        </td>
                    </tr>
                  <%--  <tr>
                    <td class="consult_popupTxt">Participants name:</td>
                        <td>
                            <asp:TextBox ID="txtParticipantName" runat="server" MaxLength="250" Width="470" />
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Participants role:</td>
                        <td> 
                        <span>
		                 <asp:DropDownList ID="ddlRole" runat="server" DataSourceID="dsRole" CssClass="Rolechange" AppendDataBoundItems="true"  DataTextField="rolename" DataValueField="id"  >
                         <asp:ListItem Value="">Select One</asp:ListItem>
                         </asp:DropDownList>
		                </span>
		                <span>
		                <asp:TextBox ID="Role_otherTextBox" runat="server"  />
		                </span>
                        </td>
                    </tr>--%>
                         <tr>
                    <td class="consult_popupTxt">Notes:</td>
                        <td>
                            <asp:TextBox ID="txtNotes" runat="server" MaxLength="250" Height="120" Width="450" TextMode="MultiLine" />
                        </td>
                    </tr>
               <tr id="Tr1" runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlActStatus" CssClass="DropDownListCssClass" runat="server"   SelectedValue='<%# Bind("Status") %>'>
        <asp:ListItem Value="">Select one</asp:ListItem>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem Selected="True">Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>   
    <tr><td colspan="2" style="padding-top:50px;"></td></tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="surveyBtn2" Text="Save Record" OnClick="OnSaveEvent" />
                    </td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="surveyBtn2" CausesValidation="false" Text="Close Window" OnClick="CloseEditPan" />
                    </td>
                </tr>
                <tr><td colspan="2" style="padding-top:50px;"></td></tr>
                </table>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlParticipant" runat="server" Visible="false">
    <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult" >
                Add/Edit Participation Information
                <span class="closeit"><asp:LinkButton ID="LinkButton17" runat="server" CausesValidation="false" Text="Close" CssClass="closeit_link" OnClick="CloseParticipantEditPan" />
                </span>
            </div>
            <div style="height:220px; overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                    <tr>
                    <td class="consult_popupTxt">Participant Name:</td>
                    <td><asp:TextBox ID="txtParticipantName" runat="server" Width="300"></asp:TextBox></td>
                    </tr>
                     <tr>
                    <td class="consult_popupTxt">Participant Role:</td>
                        <td> 
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                          <asp:DropDownList ID="ddlRole" runat="server" DataSourceID="dsRole" AutoPostBack="true"
                         DataTextField="rolename" DataValueField="id"
                          CssClass="typeChange Rolechange DropDownListCssClass"
                                    onselectedindexchanged="ddlRole_SelectedIndexChanged"  >
                         </asp:DropDownList>
                         <asp:RequiredFieldValidator InitialValue="1" ID="Req_ID" Display="Dynamic" 
     runat="server" ControlToValidate="ddlRole"
    Text="*Please select a role" ErrorMessage="ErrorMessage"></asp:RequiredFieldValidator>
        <asp:TextBox ID="txtRoleOther" runat="server" Width="280" Visible="false" />
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                <tr>
                <td></td>
                     <td>
                         <asp:RadioButtonList ID="rbtnCohortType" runat="server" AutoPostBack="true" 
                             onselectedindexchanged="rbtnCohortType_SelectedIndexChanged" RepeatColumns="2" 
                             RepeatDirection="Horizontal">
                             <asp:ListItem Value="1">Cohort 2010</asp:ListItem>
                             <asp:ListItem Selected="True" Value="2">Cohort 2013</asp:ListItem>
                         </asp:RadioButtonList>
                    </td>
                    </tr>
                    <tr>
                        <td class="consult_popupTxt">
                            Grantee:</td>
                        <td>
                            <asp:DropDownList ID="ddlGrantees" runat="server" AutoPostBack="true" 
                                CssClass="GranteeChange DropDownListCssClass" DataTextField="GranteeName" DataValueField="ID" 
                                onselectedindexchanged="ddlGrantees_SelectedIndexChanged" 
                                ondatabound="ddlGrantees_DataBound">
                                <asp:ListItem Value="">Select one</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="ddlGrantees" Display="Dynamic" ErrorMessage="ErrorMessage" 
                                InitialValue="" Text="*Select a grantee"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td  class="consult_popupTxt" >
                       <asp:Literal ID="ltlgOther" Text="Other:" runat="server" Visible="false" ></asp:Literal></td>
                    <td ><asp:TextBox ID="txtGranteeOther" runat="server" Width ="320"  Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td >
                    </td>
                    <td >
                    </td>
                </tr>
<tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlParticipantStatus" runat="server" CssClass="DropDownListCssClass"  SelectedValue='<%# Bind("Status") %>'>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem Selected="True">Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>
<tr><td colspan="2" style="padding-top:30px;"></td></tr>
                <tr>
                    <td colspan="2">
                     <asp:Button ID="Button4" runat="server" CssClass="surveyBtn2" Text="Save Record" OnClick="OnSaveParticipant" />
                        <asp:Button ID="Button2" runat="server" CssClass="surveyBtn2" Text="Save Another" OnClick="OnSaveParticipantAnother" />
                        <asp:Button ID="Button3" runat="server" CssClass="surveyBtn2" Text="Close Window" CausesValidation="false" OnClick="CloseParticipantEditPan" />
                    </td>
                </tr>

                </table>
            </div>
        </div>
    </asp:Panel>
     <asp:HiddenField ID="hfGranteeID" runat="server" />
     <asp:HiddenField ID="hfRowID" runat="server" />
     <asp:HiddenField ID="hfParticipantID" runat="server" />
     <asp:HiddenField ID="hfRefresh" runat="server" Value="" />
     
        <div style="text-align: right; margin-top: 20px;">
           <asp:LinkButton ID="LinkButton15" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton13" PostBackUrl="ContextualFactors.aspx" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton14" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           Page 5&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton7" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
          &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton8" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton>
        </div>
    <%--      <asp:SqlDataSource ID="dsGrantee" runat="server"  
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
 
        SelectCommand="SELECT * FROM [MagnetGrantees] WHERE (isActive = 1 and isMSAPGrantee=1 and id not in (38,39,40)) order by granteename" 
        onunload="dsGrantee_Unload" >
 
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnIsMSAPGrantee" Name="isMSAPGrantee" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>--%>
     <asp:SqlDataSource ID="dsRole" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [id], [rolename] FROM [TALogRoles] WHERE isActive = 1">
    </asp:SqlDataSource>
<asp:SqlDataSource ID="dsActparticipation" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        
        
        SelectCommand="SELECT * FROM [TALogActParticipation] WHERE ( FK_EventID=@EvtID ) order by name" 
        onselected="dsActparticipation_Selected" >
       <SelectParameters>
            <asp:ControlParameter ControlID="ddlEvents" Name="EvtID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters> 

</asp:SqlDataSource>
    <asp:SqlDataSource ID="dsEventActivity" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="select * from dbo.TALogEventActivity  order by Date desc">
    
    </asp:SqlDataSource>
</asp:Content>


