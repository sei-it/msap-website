﻿using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Data.SqlClient;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using Synergy.Magnet;
using docTable = DocumentFormat.OpenXml.Wordprocessing.Table;
using docTableRow = DocumentFormat.OpenXml.Wordprocessing.TableRow;
using docTableCell = DocumentFormat.OpenXml.Wordprocessing.TableCell;
using System.Web;
using System.Xml;
using System.Text.RegularExpressions;



public partial class admin_TALogs_TAReports : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["TALogIsMSAPGrantee"] != null && string.IsNullOrEmpty(Session["TALogIsMSAPGrantee"].ToString()))
            Session["TALogIsMSAPGrantee"] = null;
        if (Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
            Session["TALogGranteeID"] = null;

        if (!IsPostBack)
        {
            rbtnCohort.SelectedValue = Session["TALogCohortType"] == null ? "2" : Session["TALogCohortType"].ToString();
            Session["TALogCohortType"] = rbtnCohort.SelectedValue;
            //if(ddlGrantees.Items.Count>1)
            ddlGrantees.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();

            Session["ReportYear"] = MagnetReportPeriod.Find(x => x.isActive).Last().reportYear;
            Session["CohortType"] = MagnetReportPeriod.Find(x => x.isActive).Last().CohortType;
            
        }

    }
    protected void menuTabs_MenuItemClick(object sender, MenuEventArgs e)
    {
        multiTabs.ActiveViewIndex = Int32.Parse(menuTabs.SelectedValue);
    }

    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
        {
            Session["TALogGranteeID"] = ddlGrantees.SelectedValue;
            Session["TALogCohortType"] = rbtnCohort.SelectedValue;
        }
        else
            Session["TALogGranteeID"] = null;
    }
    protected void rbtnCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGrantees.Items.Clear();
        ddlGrantees.Items.Add(new System.Web.UI.WebControls.ListItem("Select One", ""));
        Session["TALogIsMSAPGrantee"] = rbtnCohort.SelectedValue;
        Session["TALogGranteeID"] = null;
    }

    protected void lbtnGranteeCharact_Click(object sender, EventArgs e)
    {
        string strGranteeCharact = "";
        string cohortType = rbtnCohort.SelectedValue;
        strGranteeCharact = " DECLARE @sum as int " +

                            " SET @sum = (Select COUNT(*) from MagnetGrantees WHERE (MagnetGrantees.CohortType = " + cohortType + ") AND (MagnetGrantees.isActive = 1)and MagnetGrantees.id NOT IN(39,40,41)) " +
                            " SELECT    ( select Location from states where Abbrev=MagnetGranteeContacts.State) as State, " +

                            " ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage, COUNT(*) as n " +
                            " FROM         MagnetGrantees INNER JOIN " +
                            "            MagnetGranteeContacts ON MagnetGrantees.ID = MagnetGranteeContacts.LinkID  " +

                            " WHERE  (MagnetGranteeContacts.ContactType =  1) AND (MagnetGrantees.isActive = 1)and MagnetGrantees.id NOT IN(39,40,41) " +
                            " group by MagnetGranteeContacts.State ";

        DataSet dsGranteeCharact = new DataSet();
        dsGranteeCharact.Tables.Add(getData(strGranteeCharact, "GranteeCharact"));

        strGranteeCharact = "";

        strGranteeCharact = " DECLARE @sum as int " +
                            " SELECT  @sum = COUNT(*) from dbo.vwGranteeSchoolNo " +

                            " select CAST(SchoolNo AS varchar(10)) +  " +
                            " CASE WHEN SchoolNo=1 THEN ' school' ELSE ' schools' END as Grantsize, ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage, COUNT(*) as n " +
                            " from dbo.vwGranteeSchoolNo " +
                            " group by SchoolNo " +
                            " order by SchoolNo ";

        dsGranteeCharact.Tables.Add(getData(strGranteeCharact, "GrantSize"));

        OpenPDF("GranteeReport.rpt", dsGranteeCharact);
    }

    protected void lbtnSchoolCharact_Click(object sender, EventArgs e)
    {
        DataSet dsSchoolCharact = new DataSet();
        int reportyear = 3, reportPeriodId = 6;
        string strQuery = "";


        #region school type
        strQuery =  " DECLARE @sum as int " +
                    " SET @sum = (Select COUNT(*) FROM [dbo].[vwTAlogcontextualfactors] " +
                    "   where reportyear = " + reportyear + " and reportperiodid=" + reportPeriodId + " and schooltype<>'Other') " +

                    " SELECT  " +
                    " (CASE schooltype  " +
                    " 	WHEN 'Elementary/middle' " +
                    " 	THEN '			Elementary/middle' " +
                    " 	WHEN 'Middle/high' " +
                    " 	THEN '			Middle/high' " +
                    " 	WHEN 'Combination all' " +
                    " 	THEN '			K-12' " +
                    " 	ELSE schooltype END)  " +
                    " 	as  " +
                    " 	schooltype, ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage, COUNT(*) as n " +

                    "    FROM [dbo].[vwTAlogcontextualfactors] " +
                    "   where reportyear = "  + reportyear + " and reportperiodid=" + reportPeriodId + " and schooltype<>'Other' " +
                    "   group by Schooltype  " +
                    "   order by Schooltype  ";
        DataTable rtntbl = getData(strQuery, "schooltype");
        DataTable sortedTBL = rtntbl.Clone();
        
        //Elementary
        DataRow rw1 = sortedTBL.NewRow();
        rw1[0] = rtntbl.Rows[3][0];
        rw1[1] = rtntbl.Rows[3][1];
        rw1[2] = rtntbl.Rows[3][2];
        sortedTBL.Rows.Add(rw1);
        //Middle
        DataRow rw2 = sortedTBL.NewRow();
        rw2[0] = rtntbl.Rows[5][0];
        rw2[1] = rtntbl.Rows[5][1];
        rw2[2] = rtntbl.Rows[5][2];
        sortedTBL.Rows.Add(rw2);
        //High
        DataRow rw3 = sortedTBL.NewRow();
        rw3[0] = rtntbl.Rows[4][0];
        rw3[1] = rtntbl.Rows[4][1];
        rw3[2] = rtntbl.Rows[4][2];
        sortedTBL.Rows.Add(rw3);
        //Combination
        DataRow rw4 = sortedTBL.NewRow();
        rw4[0] = "Combination";
        rw4[1] = Convert.ToInt32(rtntbl.Rows[0][1]) + Convert.ToInt32(rtntbl.Rows[1][1]) + Convert.ToInt32(rtntbl.Rows[2][1]);
        rw4[2] = Convert.ToInt32(rtntbl.Rows[0][2]) + Convert.ToInt32(rtntbl.Rows[1][2]) + Convert.ToInt32(rtntbl.Rows[2][2]);
        sortedTBL.Rows.Add(rw4);

        //  Elementary/Middle
        DataRow rw5 = sortedTBL.NewRow();
        rw5[0] = rtntbl.Rows[0][0];
        rw5[1] = rtntbl.Rows[0][1];
        rw5[2] = rtntbl.Rows[0][2];
        sortedTBL.Rows.Add(rw5);

        //  Middle/High
        DataRow rw6 = sortedTBL.NewRow();
        rw6[0] = rtntbl.Rows[2][0];
        rw6[1] = rtntbl.Rows[2][1];
        rw6[2] = rtntbl.Rows[2][2];
        sortedTBL.Rows.Add(rw6);

        //  K-12
        DataRow rw7 = sortedTBL.NewRow();
        rw7[0] = rtntbl.Rows[1][0];
        rw7[1] = rtntbl.Rows[1][1];
        rw7[2] = rtntbl.Rows[1][2];
        sortedTBL.Rows.Add(rw7);

        sortedTBL.TableName = "Schooltype";
        dsSchoolCharact.Tables.Add(sortedTBL);

        DataTable tblSchooltypechart = sortedTBL.Copy();
        tblSchooltypechart.TableName = "Schooltypechart";

        for (int i = tblSchooltypechart.Rows.Count - 1; i >= 4; i--)
        {
            tblSchooltypechart.Rows[i].Delete();
        }

        dsSchoolCharact.Tables.Add(tblSchooltypechart);

#endregion

        #region program type
        strQuery = " DECLARE @sum as int " +
                            " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where ProgramType IS NOT NULL " +
                            " and reportyear = "  + reportyear + " and reportperiodid=" + reportPeriodId + 
                            " SELECT  [ProgramType],ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +
                            "   FROM [dbo].[vwTAlogcontextualfactors] " +
                            "   where ProgramType IS NOT NULL  and reportyear = "  + reportyear + " and reportperiodid=" + reportPeriodId + 
                            "   group by ProgramType";
        dsSchoolCharact.Tables.Add(getData(strQuery,"Programtype"));
        #endregion

        #region program status
        strQuery = " DECLARE @sum as int " +

                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where Programstatus IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  [Programstatus],ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where Programstatus IS NOT NULL  and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by Programstatus ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "Programstatus"));
        #endregion

        #region Urbanicity
        strQuery = " DECLARE @sum as int " +

                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where Urbanicity IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  [Urbanicity],ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where Urbanicity IS NOT NULL  and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by Urbanicity ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "Urbanicity"));
        #endregion

        #region Free and reduced-price lunch
        strQuery = " DECLARE @sum as int " +

                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where FreeReduceLunch IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  FreeReduceLunch,ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where FreeReduceLunch IS NOT NULL  and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by FreeReduceLunch ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "Freelunch"));
        #endregion

        #region Title I status
        strQuery = " DECLARE @sum as int " +
                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where Title1Status IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  CASE WHEN Title1Status='Yes' THEN 'Title I school' ELSE 'Not a Title I school' END," +
                   " ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where Title1Status IS NOT NULL and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by Title1Status order by Title1Status desc ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "TitleIschool"));
        #endregion

        OpenPDF("SchoolCharactReport.rpt", dsSchoolCharact);
    }

    private DataTable getData(string strQuery, string tableName)
    {
        DataSet rtnDS = new DataSet();
        string stcnn = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString;
        SqlConnection cnn = new SqlConnection(stcnn);
        cnn.Open();
        SqlDataAdapter adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, tableName);
        cnn.Close();

        DataTable rtnTBL = new DataTable();
        rtnTBL = rtnDS.Tables[0].Copy();
        rtnTBL.TableName = tableName;
        return rtnTBL;
    }


    private void OpenPDF(string downloadAsFilename, DataSet ds)
    {
        ReportDocument Rel = new ReportDocument();
        Rel.Load(Server.MapPath("./reports/Commlog/" + downloadAsFilename));

        Rel.SetDataSource(ds);
        // Stop buffering the response
        Response.Buffer = false;
        // Clear the response content and headers
        Response.ClearContent();
        Response.ClearHeaders();

        ExportFormatType format = ExportFormatType.PortableDocFormat;

        string reportName = downloadAsFilename.Substring(0, downloadAsFilename.Length - 4);

        try
        {
            Rel.ExportToHttpResponse(format, Response, true, reportName);
        }
        catch (System.Threading.ThreadAbortException)
        {
            //ThreadException can happen for internale Response implementation
        }
        catch (Exception ex)
        {
            //other exeptions will be managed   
            throw;
        }

    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMonth.SelectedValue != "")
        {
            btnReport.Visible = true;
            pnlMain.Visible = true;
            DataTable tblSummary = initDataTable();
            Session["tblSummary"] = tblSummary;
            setFields(tblSummary);
        }
        else
        {
            btnReport.Visible = false;
            pnlMain.Visible = false;
        }
    }

    private void setFields(DataTable tblSummary)
    {
        foreach (DataRow drw in tblSummary.Rows)
        {
            for (int i = 0; i < 31; i++)
            {
                if (i < 3)
                {
                    Literal ltlItem = multiTabs.Views[0].FindControl("ltl" + i) as Literal;
                    ltlItem.Text = drw[i].ToString();
                }
                else if (i > 2 && i < 23)
                {
                    Literal ltlItem = multiTabs.Views[1].FindControl("ltl" + i) as Literal;
                    ltlItem.Text = drw[i].ToString();
                }
                else if (i > 22 && i < 27)
                {
                    Literal ltlItem = multiTabs.Views[2].FindControl("ltl" + i) as Literal;
                    ltlItem.Text = drw[i].ToString();
                }
                else if (i > 26 && i < 32)
                {
                    Literal ltlItem = multiTabs.Views[2].FindControl("ltl" + i) as Literal;
                    ltlItem.Text = drw[i].ToString();
                }
            }
        }
    }


    private DataTable initDataTable()
    {
        DataTable tblSummary = new DataTable();
        System.Type typeInt32 = System.Type.GetType("System.Int32");
        DataColumn column;

        column = new DataColumn("Regularupdatecall", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Granteeinterview", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Keypersonnelchange", typeInt32);
        tblSummary.Columns.Add(column);

        column = new DataColumn("Consultationwithexpert", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("MSAPresources", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("MSAPcontent", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("MSAPlogin", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("MSAPregistration", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("MSAPother", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("PDlogistics", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("PDregistration", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("PDother", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("TApartnerships", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("TAintegration", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("TAengagement", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("TArecruitment", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("TAsustainability", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("TAother", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Weblogin", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Webaccess", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Webrequest", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Webother", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("OtherTA", typeInt32);
        tblSummary.Columns.Add(column);

        column = new DataColumn("Needassessment", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Consultantassessment", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("TAevaluation", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Otheractivity", typeInt32);
        tblSummary.Columns.Add(column);

        column = new DataColumn("APRreporting", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("MAPSquestions", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Adhocreporting", typeInt32);
        tblSummary.Columns.Add(column);
        column = new DataColumn("Otherreportingrequest", typeInt32);
        tblSummary.Columns.Add(column);


        int year = Convert.ToInt32(ddlYears.SelectedValue);
        int month = Convert.ToInt32(ddlMonth.SelectedValue);
        int day = System.DateTime.DaysInMonth(year, month);

        DateTime StartDate = Convert.ToDateTime(year + "/" + month + "/01");
        DateTime EndDate = Convert.ToDateTime(year + "/" + month + "/" + day);

        var comRcds = TALogCommunication.Find(x => x.Date >= StartDate && x.Date <= EndDate);


        DataRow drw = tblSummary.NewRow();

        for (int i = 0; i < 31; i++)
        {
            drw[i] = 0;
        }

        foreach (var row in comRcds)
        {
            switch (row.CommType)
            {
                case "Regular update call":
                    drw[0] = Convert.ToInt32(drw[0]) + 1;
                    break;
                case "Grantee interview":
                    drw[1] = Convert.ToInt32(drw[1]) + 1;
                    break;
                case "Key personnel change":
                    drw[2] = Convert.ToInt32(drw[2]) + 1;
                    break;
            }

            switch (row.TAComm)
            {
                case "Consultation with expert":
                    drw[3] = Convert.ToInt32(drw[3]) + 1;
                    break;
                case "MSAP Center event—Accessing resources":
                    drw[4] = Convert.ToInt32(drw[4]) + 1;
                    break;
                case "MSAP Center event—Content":
                    drw[5] = Convert.ToInt32(drw[5]) + 1;
                    break;
                case "MSAP Center event—Logging in":
                    drw[6] = Convert.ToInt32(drw[6]) + 1;
                    break;
                case "MSAP Center event—Registration":
                    drw[7] = Convert.ToInt32(drw[7]) + 1;
                    break;
                case "MSAP Center event—Other":
                    drw[8] = Convert.ToInt32(drw[8]) + 1;
                    break;
                case "Project directors meeting—Logistics":
                    drw[9] = Convert.ToInt32(drw[9]) + 1;
                    break;
                case "Project directors meeting—Registration":
                    drw[10] = Convert.ToInt32(drw[10]) + 1;
                    break;
                case "Project directors meeting—Sessions":
                    drw[11] = Convert.ToInt32(drw[11]) + 1;
                    break;
                case "TA request—Community partnerships":
                    drw[12] = Convert.ToInt32(drw[12]) + 1;
                    break;
                case "TA request—Curriculum and theme integration":
                    drw[13] = Convert.ToInt32(drw[13]) + 1;
                    break;
                case "TA request—Family engagement":
                    drw[14] = Convert.ToInt32(drw[14]) + 1;
                    break;
                case "TA request—Marketing and recruitment":
                    drw[15] = Convert.ToInt32(drw[15]) + 1;
                    break;
                case "TA request—Sustainability":
                    drw[16] = Convert.ToInt32(drw[16]) + 1;
                    break;
                case "TA request—Other":
                    drw[17] = Convert.ToInt32(drw[17]) + 1;
                    break;
                case "Website assistance—Logging in":
                    drw[18] = Convert.ToInt32(drw[18]) + 1;
                    break;
                case "Website assistance—New staff access":
                    drw[19] = Convert.ToInt32(drw[19]) + 1;
                    break;
                case "Website assistance—Password request":
                    drw[20] = Convert.ToInt32(drw[20]) + 1;
                    break;
                case "Website assistance—Other ":
                    drw[21] = Convert.ToInt32(drw[21]) + 1;
                    break;
                case "Other request":
                    drw[22] = Convert.ToInt32(drw[22]) + 1;
                    break;
            }

            switch (row.DataCollectionComm)
            {
                case "Needs assessment":
                    drw[23] = Convert.ToInt32(drw[23]) + 1;
                    break;
                case "Consultant assessment":
                    drw[24] = Convert.ToInt32(drw[24]) + 1;
                    break;
                case "TA evaluation":
                    drw[25] = Convert.ToInt32(drw[25]) + 1;
                    break;
                case "Other data collection activity":
                    drw[26] = Convert.ToInt32(drw[26]) + 1;
                    break;
            }

            switch (row.ReportComm)
            {
                case "APR reporting":
                    drw[27] = Convert.ToInt32(drw[27]) + 1;
                    break;

                case "MAPS questions":
                    drw[28] = Convert.ToInt32(drw[28]) + 1;
                    break;
                case "Ad hoc reporting":
                    drw[29] = Convert.ToInt32(drw[29]) + 1;
                    break;
                case "Other":
                    drw[30] = Convert.ToInt32(drw[30]) + 1;
                    break;
            }
        }
        tblSummary.Rows.Add(drw);

        return tblSummary;
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        WordReport(Convert.ToInt32(ddlYears.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue));
    }

    private void WordReport(int Year, int month)
    {
        string sourceFile = Server.MapPath(".") + ConfigurationManager.AppSettings["SourceFilePath"].ToString();
        //string sFileName = ConfigurationManager.AppSettings["SourceTempFileName"].ToString();

        string sTempFileName = Server.MapPath(".") + "\\tmp\\" + Guid.NewGuid().ToString() + ".docx";
        //(Path.Combine("/", "../Temp/ReportByCountry_" + Guid.NewGuid().ToString() + ".docx"));

        File.Copy(sourceFile, sTempFileName, true);
        WriteToWordDoc(sTempFileName, Year, month);
        try
        {
            //Response.Write("<script>window.close();</script>")
            Response.Clear();
            // string sFilename = "Dos Report.docx";
            string sFilename = "Report.docx";
            //Response.ContentType = "application/msword";
            Response.ClearHeaders();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);

            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            //Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);
            }
        }
    }
    

    public void WriteToWordDoc(string filepath, int year, int month)
    {
     
        using (WordprocessingDocument wordprocessingDocument =
             WordprocessingDocument.Open(filepath, true))
        {
            //MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;

            //XmlDocument mainPartXml = new XmlDocument();
            //mainPartXml.Load(mainPart.GetStream());
            //string mainPartString = mainPartXml.OuterXml;
            //string rptDateYear = ddlMonth.SelectedItem.Text + " " + ddlYears.SelectedValue;

            //mainPartString = mainPartString.Replace("[[timeholder]]", rptDateYear);

            //StringReader reader = new StringReader(mainPartString);

            //mainPartXml.Load(reader);
            //mainPartXml.Save(mainPart.GetStream());

            //find the Date field within the header stream and replace it
            string content = null;
            string rptDateYear = ddlMonth.SelectedItem.Text + " " + ddlYears.SelectedValue;

            using (StreamReader reader = new StreamReader(
                wordprocessingDocument.MainDocumentPart.HeaderParts.First().GetStream()))
            {
                content = reader.ReadToEnd();
            }

            content = content.Replace("[[timeholder]]", rptDateYear);

            using (StreamWriter writer = new StreamWriter(
                wordprocessingDocument.MainDocumentPart.HeaderParts.First().GetStream(FileMode.Create)))
            {
                writer.Write(content);
            }

            //save                
            wordprocessingDocument.MainDocumentPart.Document.Save();

            // Assign a reference to the existing document body.
            Body body = wordprocessingDocument.MainDocumentPart.Document.Body;

            Paragraph wrdParaTitle = new Paragraph(
            new ParagraphProperties(
                new ParagraphStyleId() { Val = "comlogSectionHeading" }),
            new Run(
                new Text("Summary of requests")));
            body.Append(wrdParaTitle);


            Paragraph wrdParaProject = new Paragraph(
            new ParagraphProperties(
                new ParagraphStyleId() { Val = "comlogSectionTitle" }),
            new Run(
                new Text("General Communication")));
            body.Append(wrdParaProject);

            #region Summery of requests

            docTable table = new docTable();

            TableProperties tblPr = new TableProperties();
            DocumentFormat.OpenXml.Wordprocessing.TableStyle tblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
            tblStyle.Val = "comlogtblstyle";
            tblPr.AppendChild(tblStyle);
            table.AppendChild(tblPr);
            #region General Communication (1 - 3)
            //-----------First Row
            docTableRow tr = new docTableRow();
            docTableCell tc1 = new docTableCell();
            tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tc1.Append(new Paragraph(new Run(new Text("Regular update call: "))));
            tr.Append(tc1);

            docTableCell tc2 = new docTableCell();
            tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tc2.Append(new Paragraph(new Run(new Text(ltl0.Text))));
            tr.Append(tc2);
            table.Append(tr);

            //-----------2nd Row
            docTableRow tr2 = new docTableRow();
            docTableCell tr2tc1 = new docTableCell();
            tr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr2tc1.Append(new Paragraph(new Run(new Text("Grantee interview: "))));
            tr2.Append(tr2tc1);

            docTableCell tr2tc2 = new docTableCell();
            tr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr2tc2.Append(new Paragraph(new Run(new Text(ltl1.Text))));
            tr2.Append(tr2tc2);
            table.Append(tr2);

            //-----------3rd Row
            docTableRow tr3 = new docTableRow();
            docTableCell tr3tc1 = new docTableCell();
            tr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr3tc1.Append(new Paragraph(new Run(new Text("Key personnel change: "))));
            tr3.Append(tr3tc1);

            docTableCell tr3tc2 = new docTableCell();
            tr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr3tc2.Append(new Paragraph(new Run(new Text(ltl2.Text))));
            tr3.Append(tr3tc2);
            table.Append(tr3);

            body.Append(table);
            #endregion

            docTable TAtable = new docTable();
            TableProperties TAtblPr = new TableProperties();
            DocumentFormat.OpenXml.Wordprocessing.TableStyle TAtblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
            TAtblStyle.Val = "comlogtblstyle";
            TAtblPr.AppendChild(TAtblStyle);

            TAtable.AppendChild(TAtblPr);

            Paragraph wrdParaProjectTA = new Paragraph(
           new ParagraphProperties(
               new ParagraphStyleId() { Val = "comlogSectionTitle" }),
           new Run(
               new Text("Technical Assistance")));
            body.Append(wrdParaProjectTA);

            #region Tech. Assistance (4 - 23)
            //-----------4th Row
            docTableRow tr4 = new docTableRow();
            docTableCell tr4tc1 = new docTableCell();
            tr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr4tc1.Append(new Paragraph(new Run(new Text("Consultation with expert: "))));
            tr4.Append(tr4tc1);

            docTableCell tr4tc2 = new docTableCell();
            tr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr4tc2.Append(new Paragraph(new Run(new Text(ltl3.Text))));
            tr4.Append(tr4tc2);
            TAtable.Append(tr4);

            //-----------5th Row
            docTableRow tr5 = new docTableRow();
            docTableCell tr5tc1 = new docTableCell();
            tr5tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr5tc1.Append(new Paragraph(new Run(new Text("MSAP Center event – accessing resources: "))));
            tr5.Append(tr5tc1);

            docTableCell tr5tc2 = new docTableCell();
            tr5tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr5tc2.Append(new Paragraph(new Run(new Text(ltl4.Text))));
            tr5.Append(tr5tc2);
            TAtable.Append(tr5);

            //-----------6th Row
            docTableRow tr6 = new docTableRow();
            docTableCell tr6tc1 = new docTableCell();
            tr6tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr6tc1.Append(new Paragraph(new Run(new Text("MSAP Center event – content: "))));
            tr6.Append(tr6tc1);

            docTableCell tr6tc2 = new docTableCell();
            tr6tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr6tc2.Append(new Paragraph(new Run(new Text(ltl5.Text))));
            tr6.Append(tr6tc2);
            TAtable.Append(tr6);

            //-----------7th Row
            docTableRow tr7 = new docTableRow();
            docTableCell tr7tc1 = new docTableCell();
            tr7tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr7tc1.Append(new Paragraph(new Run(new Text("MSAP Center event – logging in: "))));
            tr7.Append(tr7tc1);

            docTableCell tr7tc2 = new docTableCell();
            tr7tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr7tc2.Append(new Paragraph(new Run(new Text(ltl6.Text))));
            tr7.Append(tr7tc2);
            TAtable.Append(tr7);

            //-----------8th Row
            docTableRow tr8 = new docTableRow();
            docTableCell tr8tc1 = new docTableCell();
            tr8tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr8tc1.Append(new Paragraph(new Run(new Text("MSAP Center event – registration: "))));
            tr8.Append(tr8tc1);

            docTableCell tr8tc2 = new docTableCell();
            tr8tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr8tc2.Append(new Paragraph(new Run(new Text(ltl7.Text))));
            tr8.Append(tr8tc2);
            TAtable.Append(tr8);

            //-----------9th Row
            docTableRow tr9 = new docTableRow();
            docTableCell tr9tc1 = new docTableCell();
            tr9tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr9tc1.Append(new Paragraph(new Run(new Text("MSAP Center event – other: "))));
            tr9.Append(tr9tc1);

            docTableCell tr9tc2 = new docTableCell();
            tr9tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr9tc2.Append(new Paragraph(new Run(new Text(ltl8.Text))));
            tr9.Append(tr9tc2);
            TAtable.Append(tr9);

            //-----------10th Row
            docTableRow tr10 = new docTableRow();
            docTableCell tr10tc1 = new docTableCell();
            tr10tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr10tc1.Append(new Paragraph(new Run(new Text("Project directors meeting – logistics: "))));
            tr10.Append(tr10tc1);

            docTableCell tr10tc2 = new docTableCell();
            tr10tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr10tc2.Append(new Paragraph(new Run(new Text(ltl9.Text))));
            tr10.Append(tr10tc2);
            TAtable.Append(tr10);

            //-----------11th Row
            docTableRow tr11 = new docTableRow();
            docTableCell tr11tc1 = new docTableCell();
            tr11tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr11tc1.Append(new Paragraph(new Run(new Text("Project directors meeting – registration: "))));
            tr11.Append(tr11tc1);

            docTableCell tr11tc2 = new docTableCell();
            tr11tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr11tc2.Append(new Paragraph(new Run(new Text(ltl10.Text))));
            tr11.Append(tr11tc2);
            TAtable.Append(tr11);

            //-----------12th Row
            docTableRow tr12 = new docTableRow();
            docTableCell tr12tc1 = new docTableCell();
            tr12tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr12tc1.Append(new Paragraph(new Run(new Text("Project directors meeting – sessions: "))));
            tr12.Append(tr12tc1);

            docTableCell tr12tc2 = new docTableCell();
            tr12tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr12tc2.Append(new Paragraph(new Run(new Text(ltl11.Text))));
            tr12.Append(tr12tc2);
            TAtable.Append(tr12);

            //-----------13th Row
            docTableRow tr13 = new docTableRow();
            docTableCell tr13tc1 = new docTableCell();
            tr13tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr13tc1.Append(new Paragraph(new Run(new Text("TA request – community partnerships: "))));
            tr13.Append(tr13tc1);

            docTableCell tr13tc2 = new docTableCell();
            tr13tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr13tc2.Append(new Paragraph(new Run(new Text(ltl12.Text))));
            tr13.Append(tr13tc2);
            TAtable.Append(tr13);

            //-----------14th Row
            docTableRow tr14 = new docTableRow();
            docTableCell tr14tc1 = new docTableCell();
            tr14tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr14tc1.Append(new Paragraph(new Run(new Text("TA request – curriculum and theme integration: "))));
            tr14.Append(tr14tc1);

            docTableCell tr14tc2 = new docTableCell();
            tr14tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr14tc2.Append(new Paragraph(new Run(new Text(ltl13.Text))));
            tr14.Append(tr14tc2);
            TAtable.Append(tr14);

            //-----------15th Row
            docTableRow tr15 = new docTableRow();
            docTableCell tr15tc1 = new docTableCell();
            tr15tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr15tc1.Append(new Paragraph(new Run(new Text("TA request –family engagement: "))));
            tr15.Append(tr15tc1);

            docTableCell tr15tc2 = new docTableCell();
            tr15tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr15tc2.Append(new Paragraph(new Run(new Text(ltl14.Text))));
            tr15.Append(tr15tc2);
            TAtable.Append(tr15);

            //-----------16th Row
            docTableRow tr16 = new docTableRow();
            docTableCell tr16tc1 = new docTableCell();
            tr16tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr16tc1.Append(new Paragraph(new Run(new Text("TA request – marketing and recruitment: "))));
            tr16.Append(tr16tc1);

            docTableCell tr16tc2 = new docTableCell();
            tr16tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr16tc2.Append(new Paragraph(new Run(new Text(ltl15.Text))));
            tr16.Append(tr16tc2);
            TAtable.Append(tr16);

            //-----------17th Row
            docTableRow tr17 = new docTableRow();
            docTableCell tr17tc1 = new docTableCell();
            tr17tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr17tc1.Append(new Paragraph(new Run(new Text("TA request – sustainability: "))));
            tr17.Append(tr17tc1);

            docTableCell tr17tc2 = new docTableCell();
            tr17tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr17tc2.Append(new Paragraph(new Run(new Text(ltl16.Text))));
            tr17.Append(tr17tc2);
            TAtable.Append(tr17);

            //-----------18th Row
            docTableRow tr18 = new docTableRow();
            docTableCell tr18tc1 = new docTableCell();
            tr18tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr18tc1.Append(new Paragraph(new Run(new Text("TA request – other: "))));
            tr18.Append(tr18tc1);

            docTableCell tr18tc2 = new docTableCell();
            tr18tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr18tc2.Append(new Paragraph(new Run(new Text(ltl17.Text))));
            tr18.Append(tr18tc2);
            TAtable.Append(tr18);

            //-----------19th Row
            docTableRow tr19 = new docTableRow();
            docTableCell tr19tc1 = new docTableCell();
            tr19tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr19tc1.Append(new Paragraph(new Run(new Text("Website assistance – logging in: "))));
            tr19.Append(tr19tc1);

            docTableCell tr19tc2 = new docTableCell();
            tr19tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr19tc2.Append(new Paragraph(new Run(new Text(ltl18.Text))));
            tr19.Append(tr19tc2);
            TAtable.Append(tr19);

            //-----------20th Row
            docTableRow tr20 = new docTableRow();
            docTableCell tr20tc1 = new docTableCell();
            tr20tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr20tc1.Append(new Paragraph(new Run(new Text("Website assistance – new staff access: "))));
            tr20.Append(tr20tc1);

            docTableCell tr20tc2 = new docTableCell();
            tr20tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr20tc2.Append(new Paragraph(new Run(new Text(ltl19.Text))));
            tr20.Append(tr20tc2);
            TAtable.Append(tr20);

            //-----------21th Row
            docTableRow tr21 = new docTableRow();
            docTableCell tr21tc1 = new docTableCell();
            tr21tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr21tc1.Append(new Paragraph(new Run(new Text("Website assistance – password request: "))));
            tr21.Append(tr21tc1);

            docTableCell tr21tc2 = new docTableCell();
            tr21tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr21tc2.Append(new Paragraph(new Run(new Text(ltl20.Text))));
            tr21.Append(tr21tc2);
            TAtable.Append(tr21);

            //-----------22th Row
            docTableRow tr22 = new docTableRow();
            docTableCell tr22tc1 = new docTableCell();
            tr22tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr22tc1.Append(new Paragraph(new Run(new Text("Website assistance – other: "))));
            tr22.Append(tr22tc1);

            docTableCell tr22tc2 = new docTableCell();
            tr22tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr22tc2.Append(new Paragraph(new Run(new Text(ltl21.Text))));
            tr22.Append(tr22tc2);
            TAtable.Append(tr22);

            //-----------23th Row
            docTableRow tr23 = new docTableRow();
            docTableCell tr23tc1 = new docTableCell();
            tr23tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr23tc1.Append(new Paragraph(new Run(new Text("Other TA request: "))));
            tr23.Append(tr23tc1);

            docTableCell tr23tc2 = new docTableCell();
            tr23tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr23tc2.Append(new Paragraph(new Run(new Text(ltl22.Text))));
            tr23.Append(tr23tc2);
            TAtable.Append(tr23);

            body.Append(TAtable);
            #endregion

            docTable DCtable = new docTable();
            TableProperties DCtblPr = new TableProperties();
            DocumentFormat.OpenXml.Wordprocessing.TableStyle DCtblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
            DCtblStyle.Val = "comlogtblstyle";
            DCtblPr.AppendChild(DCtblStyle);
            DCtable.AppendChild(DCtblPr);

            Paragraph wrdParaProjectDC = new Paragraph(
           new ParagraphProperties(
               new ParagraphStyleId() { Val = "comlogSectionTitle" }),
           new Run(
               new Text("Data Collection")));
            body.Append(wrdParaProjectDC);

            #region Data Collection
            //-----------24th Row
            docTableRow tr24 = new docTableRow();
            docTableCell tr24tc1 = new docTableCell();
            tr24tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr24tc1.Append(new Paragraph(new Run(new Text("Needs assessment: "))));
            tr24.Append(tr24tc1);

            docTableCell tr24tc2 = new docTableCell();
            tr24tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr24tc2.Append(new Paragraph(new Run(new Text(ltl23.Text))));
            tr24.Append(tr24tc2);
            DCtable.Append(tr24);

            //-----------25th Row
            docTableRow tr25 = new docTableRow();
            docTableCell tr25tc1 = new docTableCell();
            tr25tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr25tc1.Append(new Paragraph(new Run(new Text("Consultant assessment: "))));
            tr25.Append(tr25tc1);

            docTableCell tr25tc2 = new docTableCell();
            tr25tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr25tc2.Append(new Paragraph(new Run(new Text(ltl24.Text))));
            tr25.Append(tr25tc2);
            DCtable.Append(tr25);

            //-----------26th Row
            docTableRow tr26 = new docTableRow();
            docTableCell tr26tc1 = new docTableCell();
            tr26tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr26tc1.Append(new Paragraph(new Run(new Text("TA evaluation: "))));
            tr26.Append(tr26tc1);

            docTableCell tr26tc2 = new docTableCell();
            tr26tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr26tc2.Append(new Paragraph(new Run(new Text(ltl25.Text))));
            tr26.Append(tr26tc2);
            DCtable.Append(tr26);

            //-----------27th Row
            docTableRow tr27 = new docTableRow();
            docTableCell tr27tc1 = new docTableCell();
            tr27tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr27tc1.Append(new Paragraph(new Run(new Text("Other data collection activity: "))));
            tr27.Append(tr27tc1);

            docTableCell tr27tc2 = new docTableCell();
            tr27tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr27tc2.Append(new Paragraph(new Run(new Text(ltl26.Text))));
            tr27.Append(tr27tc2);
            DCtable.Append(tr27);

            #endregion

            body.Append(DCtable);

            docTable rpttable = new docTable();
            TableProperties rpttblPr = new TableProperties();
            DocumentFormat.OpenXml.Wordprocessing.TableStyle rpttblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
            rpttblStyle.Val = "comlogtblstyle";
            rpttblPr.AppendChild(rpttblStyle);
            rpttable.AppendChild(rpttblPr);

            Paragraph wrdParaProjectrpt = new Paragraph(
           new ParagraphProperties(
               new ParagraphStyleId() { Val = "comlogSectionTitle" }),
           new Run(
               new Text("Reporting")));
            body.Append(wrdParaProjectrpt);

            #region Reporting
            //-----------28th Row
            docTableRow tr28 = new docTableRow();
            docTableCell tr28tc1 = new docTableCell();
            tr28tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr28tc1.Append(new Paragraph(new Run(new Text("APR reporting: "))));
            tr28.Append(tr28tc1);

            docTableCell tr28tc2 = new docTableCell();
            tr28tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr28tc2.Append(new Paragraph(new Run(new Text(ltl27.Text))));
            tr28.Append(tr28tc2);
            rpttable.Append(tr28);

            //-----------29th Row
            docTableRow tr29 = new docTableRow();
            docTableCell tr29tc1 = new docTableCell();
            tr29tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr29tc1.Append(new Paragraph(new Run(new Text("MAPS questions: "))));
            tr29.Append(tr29tc1);

            docTableCell tr29tc2 = new docTableCell();
            tr29tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr29tc2.Append(new Paragraph(new Run(new Text(ltl28.Text))));
            tr29.Append(tr29tc2);
            rpttable.Append(tr29);

            //-----------30th Row
            docTableRow tr30 = new docTableRow();
            docTableCell tr30tc1 = new docTableCell();
            tr30tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr30tc1.Append(new Paragraph(new Run(new Text("Ad hoc reporting: "))));
            tr30.Append(tr30tc1);

            docTableCell tr30tc2 = new docTableCell();
            tr30tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr30tc2.Append(new Paragraph(new Run(new Text(ltl29.Text))));
            tr30.Append(tr30tc2);
            rpttable.Append(tr30);

            //-----------31th Row
            docTableRow tr31 = new docTableRow();
            docTableCell tr31tc1 = new docTableCell();
            tr31tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
            tr31tc1.Append(new Paragraph(new Run(new Text("Other reporting  request: "))));
            tr31.Append(tr31tc1);

            docTableCell tr31tc2 = new docTableCell();
            tr31tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
            tr31tc2.Append(new Paragraph(new Run(new Text(ltl30.Text))));
            tr31.Append(tr31tc2);
            rpttable.Append(tr31);
            #endregion

            body.Append(rpttable);

            #endregion

            #region request details

           Paragraph wrdParaProjectDetail = new Paragraph(
           new ParagraphProperties(
               new ParagraphStyleId() { Val = "comlogSectionHeading" }),
           new Run(
               new Text("Request details")));
            body.Append(wrdParaProjectDetail);

          
            Paragraph wrdParaProjectDetailDGC = new Paragraph(
           new ParagraphProperties(
               new ParagraphStyleId() { Val = "comlogSectionTitle" }),
           new Run(
               new Text("General Communication")));
            body.Append(wrdParaProjectDetailDGC);

            DateTime startDate = Convert.ToDateTime(month + "/01/" + year);
            if (month == 12)
            {
                month = 1;
                year += year;
            }
            DateTime endDate = Convert.ToDateTime(month+1 + "/01/" + year);

            MagnetDBDB db = new MagnetDBDB();

            #region records of General Communications

            var GCrws = from rws in db.TALogCommunications
                        join grantee in db.MagnetGrantees on
                         rws.PK_GenInfoID equals grantee.ID
                        join contact in db.TALogRoles on
                        rws.role_id equals contact.id
                        where rws.Date >= startDate &&
                              rws.Date < endDate &&
                              rws.CommType != ""
                        orderby rws.CommType, rws.Date
                        select new
                        {
                            CommID = rws.id,
                            GCCategory = rws.CommType,
                            grantee.GranteeName,
                            caller = rws.PsnCommWith,
                            contact.rolename,
                            rws.RequestStatus,
                            rws.Date,
                            rws.commTime,
                            rws.FulfillDate,
                            rws.completetime,
                            rws.CommDescription,
                            rws.ActionTaken,
                            rws.RequiredDes
                        };

            foreach (var comlog in GCrws)
            {
                if (!string.IsNullOrEmpty(comlog.GCCategory.Trim()))
                {
                    docTable GCtable = new docTable();
                    TableProperties GCtblPr = new TableProperties();
                    DocumentFormat.OpenXml.Wordprocessing.TableStyle GCtblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                    GCtblStyle.Val = "comlogtblstyle";
                    GCtblPr.AppendChild(GCtblStyle);
                    GCtable.AppendChild(GCtblPr);                 

                    #region 12 items
                    //-----------First Row
                    docTableRow gctr = new docTableRow();
                    docTableCell gctc1 = new docTableCell();
                    gctc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctc1.Append(new Paragraph(new Run(new Text("General communication category: "))));
                    gctr.Append(gctc1);

                    docTableCell gctc2 = new docTableCell();
                    gctc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctc2.Append(new Paragraph(new Run(new Text(comlog.GCCategory))));
                    gctr.Append(gctc2);
                    GCtable.Append(gctr);

                    //-----------2nd Row
                    docTableRow gctr2 = new docTableRow();
                    docTableCell gctr2tc1 = new docTableCell();
                    gctr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr2tc1.Append(new Paragraph(new Run(new Text("Grantee name: "))));
                    gctr2.Append(gctr2tc1);

                    docTableCell gctr2tc2 = new docTableCell();
                    gctr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr2tc2.Append(new Paragraph(new Run(new Text(comlog.GranteeName))));
                    gctr2.Append(gctr2tc2);
                    GCtable.Append(gctr2);

                    //-----------3rd Row
                    docTableRow gctr3 = new docTableRow();
                    docTableCell gctr3tc1 = new docTableCell();
                    gctr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr3tc1.Append(new Paragraph(new Run(new Text("Caller: "))));
                    gctr3.Append(gctr3tc1);

                    docTableCell gctr3tc2 = new docTableCell();
                    gctr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr3tc2.Append(new Paragraph(new Run(new Text(comlog.caller))));
                    gctr3.Append(gctr3tc2);
                    GCtable.Append(gctr3);

                    //-----------4th Row
                    docTableRow gctr4 = new docTableRow();
                    docTableCell gctr4tc1 = new docTableCell();
                    gctr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr4tc1.Append(new Paragraph(new Run(new Text("Role: "))));
                    gctr4.Append(gctr4tc1);

                    docTableCell gctr4tc2 = new docTableCell();
                    gctr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr4tc2.Append(new Paragraph(new Run(new Text(comlog.rolename))));
                    gctr4.Append(gctr4tc2);
                    GCtable.Append(gctr4);

                    //-----------5th Row
                    docTableRow gctr5 = new docTableRow();
                    docTableCell gctr5tc1 = new docTableCell();
                    gctr5tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr5tc1.Append(new Paragraph(new Run(new Text("Request status: "))));
                    gctr5.Append(gctr5tc1);

                    docTableCell gctr5tc2 = new docTableCell();
                    gctr5tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr5tc2.Append(new Paragraph(new Run(new Text(comlog.RequestStatus))));
                    gctr5.Append(gctr5tc2);
                    GCtable.Append(gctr5);

                    //-----------6th Row
                    docTableRow gctr6 = new docTableRow();
                    docTableCell gctr6tc1 = new docTableCell();
                    gctr6tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr6tc1.Append(new Paragraph(new Run(new Text("Date: "))));
                    gctr6.Append(gctr6tc1);

                    docTableCell gctr6tc2 = new docTableCell();
                    gctr6tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr6tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.Date)))));
                    gctr6.Append(gctr6tc2);
                    GCtable.Append(gctr6);

                    //-----------7th Row
                    docTableRow gctr7 = new docTableRow();
                    docTableCell gctr7tc1 = new docTableCell();
                    gctr7tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr7tc1.Append(new Paragraph(new Run(new Text("Time: "))));
                    gctr7.Append(gctr7tc1);

                    docTableCell gctr7tc2 = new docTableCell();
                    gctr7tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr7tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.commTime).ToString("h:mm tt")))));
                    gctr7.Append(gctr7tc2);
                    GCtable.Append(gctr7);

                    //-----------8th Row
                    docTableRow gctr8 = new docTableRow();
                    docTableCell gctr8tc1 = new docTableCell();
                    gctr8tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr8tc1.Append(new Paragraph(new Run(new Text("TA request fulfillment date: "))));
                    gctr8.Append(gctr8tc1);

                    docTableCell gctr8tc2 = new docTableCell();
                    gctr8tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr8tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.FulfillDate)))));
                    gctr8.Append(gctr8tc2);
                    GCtable.Append(gctr8);

                    //-----------9th Row
                    docTableRow gctr9 = new docTableRow();
                    docTableCell gctr9tc1 = new docTableCell();
                    gctr9tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr9tc1.Append(new Paragraph(new Run(new Text("Time completed: "))));
                    gctr9.Append(gctr9tc1);

                    docTableCell gctr9tc2 = new docTableCell();
                    gctr9tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr9tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.completetime).ToString("h:mm tt")))));
                    gctr9.Append(gctr9tc2);
                    GCtable.Append(gctr9);

                    //-----------10th Row
                    docTableRow gctr10 = new docTableRow();
                    docTableCell gctr10tc1 = new docTableCell();
                    gctr10tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr10tc1.Append(new Paragraph(new Run(new Text("Description of communication/request: "))));
                    gctr10.Append(gctr10tc1);

                    docTableCell gctr10tc2 = new docTableCell();
                    gctr10tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr10tc2.Append(new Paragraph(new Run(new Text(comlog.CommDescription))));
                    gctr10.Append(gctr10tc2);
                    GCtable.Append(gctr10);

                    //-----------11th Row
                    docTableRow gctr11 = new docTableRow();
                    docTableCell gctr11tc1 = new docTableCell();
                    gctr11tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr11tc1.Append(new Paragraph(new Run(new Text("Actions taken: "))));
                    gctr11.Append(gctr11tc1);

                    docTableCell gctr11tc2 = new docTableCell();
                    gctr11tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr11tc2.Append(new Paragraph(new Run(new Text(comlog.ActionTaken))));
                    gctr11.Append(gctr11tc2);
                    GCtable.Append(gctr11);

                    //-----------12th Row
                    docTableRow gctr12 = new docTableRow();
                    docTableCell gctr12tc1 = new docTableCell();
                    gctr12tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    gctr12tc1.Append(new Paragraph(new Run(new Text("Description of follow-up required: "))));
                    gctr12.Append(gctr12tc1);

                    docTableCell gctr12tc2 = new docTableCell();
                    gctr12tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    gctr12tc2.Append(new Paragraph(new Run(new Text(comlog.RequiredDes))));
                    gctr12.Append(gctr12tc2);
                    GCtable.Append(gctr12);
                    body.Append(GCtable);

                    #endregion

                    var followups = TALogFollowUp.Find(x => x.PK_CommID == comlog.CommID);

                        if (followups.Count > 0)
                        {
                            foreach (var item in followups)
                            {

                                Paragraph wrdParaProjectDetailGCFup = new Paragraph(
                                new ParagraphProperties(
                                new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                                new Run(
                                new Text("Follow up items")));
                                body.Append(wrdParaProjectDetailGCFup);

                                docTable GCFuptable = new docTable();
                                TableProperties GCFuptblPr = new TableProperties();
                                DocumentFormat.OpenXml.Wordprocessing.TableStyle GCFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                                GCFuptblStyle.Val = "comlogtblstyle";
                                GCFuptblPr.AppendChild(GCFuptblStyle);
                                GCFuptable.AppendChild(GCFuptblPr);

                                //-----------First Row
                                docTableRow gcfuptr = new docTableRow();
                                docTableCell gcfuptc1 = new docTableCell();
                                gcfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                gcfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                                gcfuptr.Append(gcfuptc1);

                                docTableCell gcfuptc2 = new docTableCell();
                                gcfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                gcfuptc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", item.Date)))));
                                gcfuptr.Append(gcfuptc2);
                                GCFuptable.Append(gcfuptr);

                                //-----------2nd Row
                                docTableRow gcfuptr2 = new docTableRow();
                                docTableCell gcfuptr2tc1 = new docTableCell();
                                gcfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                gcfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                                gcfuptr2.Append(gcfuptr2tc1);

                                docTableCell gcfuptr2tc2 = new docTableCell();
                                gcfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                gcfuptr2tc2.Append(new Paragraph(new Run(new Text(item.FollowupPerson))));
                                gcfuptr2.Append(gcfuptr2tc2);
                                GCFuptable.Append(gcfuptr2);

                                //-----------3rd Row
                                docTableRow gcfuptr3 = new docTableRow();
                                docTableCell gcfuptr3tc1 = new docTableCell();
                                gcfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                gcfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                                gcfuptr3.Append(gcfuptr3tc1);

                                docTableCell gcfuptr3tc2 = new docTableCell();
                                gcfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                gcfuptr3tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Notes)))));
                                gcfuptr3.Append(gcfuptr3tc2);
                                GCFuptable.Append(gcfuptr3);

                                //-----------4th Row
                                docTableRow gcfuptr4 = new docTableRow();
                                docTableCell gcfuptr4tc1 = new docTableCell();
                                gcfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                gcfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                                gcfuptr4.Append(gcfuptr4tc1);

                                docTableCell gcfuptr4tc2 = new docTableCell();
                                gcfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                gcfuptr4tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Result)))));
                                gcfuptr4.Append(gcfuptr4tc2);
                                GCFuptable.Append(gcfuptr4);
                                body.Append(GCFuptable);
                            }
                        }
                        else
                        {

                            Paragraph wrdParaProjectDetailGCFup = new Paragraph(
                            new ParagraphProperties(
                            new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                            new Run(
                            new Text("Follow up items")));
                            body.Append(wrdParaProjectDetailGCFup);

                            docTable GCFuptable = new docTable();
                            TableProperties GCFuptblPr = new TableProperties();
                            DocumentFormat.OpenXml.Wordprocessing.TableStyle GCFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                            GCFuptblStyle.Val = "comlogtblstyle";
                            GCFuptblPr.AppendChild(GCFuptblStyle);
                            GCFuptable.AppendChild(GCFuptblPr);

                            //-----------First Row
                            docTableRow gcfuptr = new docTableRow();
                            docTableCell gcfuptc1 = new docTableCell();
                            gcfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            gcfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                            gcfuptr.Append(gcfuptc1);

                            docTableCell gcfuptc2 = new docTableCell();
                            gcfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            gcfuptc2.Append(new Paragraph(new Run(new Text())));
                            gcfuptr.Append(gcfuptc2);
                            GCFuptable.Append(gcfuptr);

                            //-----------2nd Row
                            docTableRow gcfuptr2 = new docTableRow();
                            docTableCell gcfuptr2tc1 = new docTableCell();
                            gcfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            gcfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                            gcfuptr2.Append(gcfuptr2tc1);

                            docTableCell gcfuptr2tc2 = new docTableCell();
                            gcfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            gcfuptr2tc2.Append(new Paragraph(new Run(new Text())));
                            gcfuptr2.Append(gcfuptr2tc2);
                            GCFuptable.Append(gcfuptr2);

                            //-----------3rd Row
                            docTableRow gcfuptr3 = new docTableRow();
                            docTableCell gcfuptr3tc1 = new docTableCell();
                            gcfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            gcfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                            gcfuptr3.Append(gcfuptr3tc1);

                            docTableCell gcfuptr3tc2 = new docTableCell();
                            gcfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            gcfuptr3tc2.Append(new Paragraph(new Run(new Text())));
                            gcfuptr3.Append(gcfuptr3tc2);
                            GCFuptable.Append(gcfuptr3);

                            //-----------4th Row
                            docTableRow gcfuptr4 = new docTableRow();
                            docTableCell gcfuptr4tc1 = new docTableCell();
                            gcfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            gcfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                            gcfuptr4.Append(gcfuptr4tc1);

                            docTableCell gcfuptr4tc2 = new docTableCell();
                            gcfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            gcfuptr4tc2.Append(new Paragraph(new Run(new Text())));
                            gcfuptr4.Append(gcfuptr4tc2);
                            GCFuptable.Append(gcfuptr4);
                            body.Append(GCFuptable);
                        }
                        //empty space
                        Paragraph wrdParaProjectDetailDGCempty = new Paragraph(
                    new ParagraphProperties(
                        new ParagraphStyleId() { Val = "comlogEmpty" }),
                    new Run(
                        new Text()));
                        body.Append(wrdParaProjectDetailDGCempty);
                } // end if not empty
            } //end foreach

            #endregion

            #region records of Tech. Assistance

            Paragraph wrdParaTA = new Paragraph(
        new ParagraphProperties(
            new ParagraphStyleId() { Val = "comlogSectionHeading" }),
        new Run(
            new Text("TA Communication")));
            body.Append(wrdParaTA);

            var TArws = from rws in db.TALogCommunications
                        join grantee in db.MagnetGrantees on
                         rws.PK_GenInfoID equals grantee.ID
                        join contact in db.TALogRoles on
                        rws.role_id equals contact.id
                        where rws.Date >= startDate &&
                              rws.Date < endDate &&
                              rws.TAComm != ""
                        orderby rws.TAComm, rws.Date
                        select new
                        {
                            CommID = rws.id,
                            TACategory = rws.TAComm,
                            grantee.GranteeName,
                            caller = rws.PsnCommWith,
                            contact.rolename,
                            rws.RequestStatus,
                            rws.Date,
                            rws.commTime,
                            rws.FulfillDate,
                            rws.completetime,
                            rws.CommDescription,
                            rws.ActionTaken,
                            rws.RequiredDes
                        };

            foreach (var comlog in TArws)
            {
                if (!string.IsNullOrEmpty(comlog.TACategory.Trim()))
                {
                    docTable TADtltable = new docTable();
                    TableProperties TADtblPr = new TableProperties();
                    DocumentFormat.OpenXml.Wordprocessing.TableStyle TADtblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                    TADtblStyle.Val = "comlogtblstyle";
                    TADtblPr.AppendChild(TADtblStyle);
                    TADtltable.AppendChild(TADtblPr);

                    #region 12 items
                    //-----------First Row
                    docTableRow TAtr = new docTableRow();
                    docTableCell TAtc1 = new docTableCell();
                    TAtc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtc1.Append(new Paragraph(new Run(new Text("TA communication category: "))));
                    TAtr.Append(TAtc1);

                    docTableCell TAtc2 = new docTableCell();
                    TAtc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtc2.Append(new Paragraph(new Run(new Text(comlog.TACategory))));
                    TAtr.Append(TAtc2);
                    TADtltable.Append(TAtr);

                    //-----------2nd Row
                    docTableRow TAtr2 = new docTableRow();
                    docTableCell TAtr2tc1 = new docTableCell();
                    TAtr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr2tc1.Append(new Paragraph(new Run(new Text("Grantee name: "))));
                    TAtr2.Append(TAtr2tc1);

                    docTableCell TAtr2tc2 = new docTableCell();
                    TAtr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr2tc2.Append(new Paragraph(new Run(new Text(comlog.GranteeName))));
                    TAtr2.Append(TAtr2tc2);
                    TADtltable.Append(TAtr2);

                    //-----------3rd Row
                    docTableRow TAtr3 = new docTableRow();
                    docTableCell TAtr3tc1 = new docTableCell();
                    TAtr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr3tc1.Append(new Paragraph(new Run(new Text("Caller: "))));
                    TAtr3.Append(TAtr3tc1);

                    docTableCell TAtr3tc2 = new docTableCell();
                    TAtr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr3tc2.Append(new Paragraph(new Run(new Text(comlog.caller))));
                    TAtr3.Append(TAtr3tc2);
                    TADtltable.Append(TAtr3);

                    //-----------4th Row
                    docTableRow TAtr4 = new docTableRow();
                    docTableCell TAtr4tc1 = new docTableCell();
                    TAtr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr4tc1.Append(new Paragraph(new Run(new Text("Role: "))));
                    TAtr4.Append(TAtr4tc1);

                    docTableCell TAtr4tc2 = new docTableCell();
                    TAtr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr4tc2.Append(new Paragraph(new Run(new Text(comlog.rolename))));
                    TAtr4.Append(TAtr4tc2);
                    TADtltable.Append(TAtr4);

                    //-----------5th Row
                    docTableRow TAtr5 = new docTableRow();
                    docTableCell TAtr5tc1 = new docTableCell();
                    TAtr5tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr5tc1.Append(new Paragraph(new Run(new Text("Request status: "))));
                    TAtr5.Append(TAtr5tc1);

                    docTableCell TAtr5tc2 = new docTableCell();
                    TAtr5tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr5tc2.Append(new Paragraph(new Run(new Text(comlog.RequestStatus))));
                    TAtr5.Append(TAtr5tc2);
                    TADtltable.Append(TAtr5);

                    //-----------6th Row
                    docTableRow TAtr6 = new docTableRow();
                    docTableCell TAtr6tc1 = new docTableCell();
                    TAtr6tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr6tc1.Append(new Paragraph(new Run(new Text("Date: "))));
                    TAtr6.Append(TAtr6tc1);

                    docTableCell TAtr6tc2 = new docTableCell();
                    TAtr6tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr6tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.Date)))));
                    TAtr6.Append(TAtr6tc2);
                    TADtltable.Append(TAtr6);

                    //-----------7th Row
                    docTableRow TAtr7 = new docTableRow();
                    docTableCell TAtr7tc1 = new docTableCell();
                    TAtr7tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr7tc1.Append(new Paragraph(new Run(new Text("Time: "))));
                    TAtr7.Append(TAtr7tc1);

                    docTableCell TAtr7tc2 = new docTableCell();
                    TAtr7tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr7tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.commTime).ToString("h:mm tt")))));
                    TAtr7.Append(TAtr7tc2);
                    TADtltable.Append(TAtr7);

                    //-----------8th Row
                    docTableRow TAtr8 = new docTableRow();
                    docTableCell TAtr8tc1 = new docTableCell();
                    TAtr8tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr8tc1.Append(new Paragraph(new Run(new Text("TA request fulfillment date: "))));
                    TAtr8.Append(TAtr8tc1);

                    docTableCell TAtr8tc2 = new docTableCell();
                    TAtr8tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr8tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.FulfillDate)))));
                    TAtr8.Append(TAtr8tc2);
                    TADtltable.Append(TAtr8);

                    //-----------9th Row
                    docTableRow TAtr9 = new docTableRow();
                    docTableCell TAtr9tc1 = new docTableCell();
                    TAtr9tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr9tc1.Append(new Paragraph(new Run(new Text("Time completed: "))));
                    TAtr9.Append(TAtr9tc1);

                    docTableCell TAtr9tc2 = new docTableCell();
                    TAtr9tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr9tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.completetime).ToString("h:mm tt")))));
                    TAtr9.Append(TAtr9tc2);
                    TADtltable.Append(TAtr9);

                    //-----------10th Row
                    docTableRow TAtr10 = new docTableRow();
                    docTableCell TAtr10tc1 = new docTableCell();
                    TAtr10tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr10tc1.Append(new Paragraph(new Run(new Text("Description of communication/request: "))));
                    TAtr10.Append(TAtr10tc1);

                    docTableCell TAtr10tc2 = new docTableCell();
                    TAtr10tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr10tc2.Append(new Paragraph(new Run(new Text(comlog.CommDescription))));
                    TAtr10.Append(TAtr10tc2);
                    TADtltable.Append(TAtr10);

                    //-----------11th Row
                    docTableRow TAtr11 = new docTableRow();
                    docTableCell TAtr11tc1 = new docTableCell();
                    TAtr11tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr11tc1.Append(new Paragraph(new Run(new Text("Actions taken: "))));
                    TAtr11.Append(TAtr11tc1);

                    docTableCell TAtr11tc2 = new docTableCell();
                    TAtr11tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr11tc2.Append(new Paragraph(new Run(new Text(comlog.ActionTaken))));
                    TAtr11.Append(TAtr11tc2);
                    TADtltable.Append(TAtr11);

                    //-----------12th Row
                    docTableRow TAtr12 = new docTableRow();
                    docTableCell TAtr12tc1 = new docTableCell();
                    TAtr12tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    TAtr12tc1.Append(new Paragraph(new Run(new Text("Description of follow-up required: "))));
                    TAtr12.Append(TAtr12tc1);

                    docTableCell TAtr12tc2 = new docTableCell();
                    TAtr12tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    TAtr12tc2.Append(new Paragraph(new Run(new Text(comlog.RequiredDes))));
                    TAtr12.Append(TAtr12tc2);
                    TADtltable.Append(TAtr12);

                    body.Append(TADtltable);
                    #endregion

                    var followups = TALogFollowUp.Find(x => x.PK_CommID == comlog.CommID);

                        if (followups.Count > 0)
                        {
                            foreach (var item in followups)
                            {
                                Paragraph wrdParaProjectDetailTAFup = new Paragraph(
                                            new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                                            new Run(
                                            new Text("Follow up items")));
                                body.Append(wrdParaProjectDetailTAFup);
                                docTable TAFuptable = new docTable();
                                TableProperties TAFuptblPr = new TableProperties();
                                DocumentFormat.OpenXml.Wordprocessing.TableStyle TAFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                                TAFuptblStyle.Val = "comlogtblstyle";
                                TAFuptblPr.AppendChild(TAFuptblStyle);
                                TAFuptable.AppendChild(TAFuptblPr);

                                //-----------First Row
                                docTableRow TAfuptr = new docTableRow();
                                docTableCell TAfuptc1 = new docTableCell();
                                TAfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                TAfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                                TAfuptr.Append(TAfuptc1);

                                docTableCell TAfuptc2 = new docTableCell();
                                TAfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                TAfuptc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", item.Date)))));
                                TAfuptr.Append(TAfuptc2);
                                TAFuptable.Append(TAfuptr);

                                //-----------2nd Row
                                docTableRow TAfuptr2 = new docTableRow();
                                docTableCell TAfuptr2tc1 = new docTableCell();
                                TAfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                TAfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                                TAfuptr2.Append(TAfuptr2tc1);

                                docTableCell TAfuptr2tc2 = new docTableCell();
                                TAfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                TAfuptr2tc2.Append(new Paragraph(new Run(new Text(item.FollowupPerson))));
                                TAfuptr2.Append(TAfuptr2tc2);
                                TAFuptable.Append(TAfuptr2);

                                //-----------3rd Row
                                docTableRow TAfuptr3 = new docTableRow();
                                docTableCell TAfuptr3tc1 = new docTableCell();
                                TAfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                TAfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                                TAfuptr3.Append(TAfuptr3tc1);

                                docTableCell TAfuptr3tc2 = new docTableCell();
                                TAfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                TAfuptr3tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Notes)))));
                                TAfuptr3.Append(TAfuptr3tc2);
                                TAFuptable.Append(TAfuptr3);

                                //-----------4th Row
                                docTableRow TAfuptr4 = new docTableRow();
                                docTableCell TAfuptr4tc1 = new docTableCell();
                                TAfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                                TAfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                                TAfuptr4.Append(TAfuptr4tc1);

                                docTableCell TAfuptr4tc2 = new docTableCell();
                                TAfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                                TAfuptr4tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Result)))));
                                TAfuptr4.Append(TAfuptr4tc2);
                                TAFuptable.Append(TAfuptr4);
                                body.Append(TAFuptable);
                            }

                        }
                        else 
                        {
                            Paragraph wrdParaProjectDetailTAFup = new Paragraph(
                                        new ParagraphProperties(
                                        new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                                        new Run(
                                        new Text("Follow up items")));
                            body.Append(wrdParaProjectDetailTAFup);
                            docTable TAFuptable = new docTable();
                            TableProperties TAFuptblPr = new TableProperties();
                            DocumentFormat.OpenXml.Wordprocessing.TableStyle TAFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                            TAFuptblStyle.Val = "comlogtblstyle";
                            TAFuptblPr.AppendChild(TAFuptblStyle);
                            TAFuptable.AppendChild(TAFuptblPr);

                            //-----------First Row
                            docTableRow TAfuptr = new docTableRow();
                            docTableCell TAfuptc1 = new docTableCell();
                            TAfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            TAfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                            TAfuptr.Append(TAfuptc1);

                            docTableCell TAfuptc2 = new docTableCell();
                            TAfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            TAfuptc2.Append(new Paragraph(new Run(new Text())));
                            TAfuptr.Append(TAfuptc2);
                            TAFuptable.Append(TAfuptr);

                            //-----------2nd Row
                            docTableRow TAfuptr2 = new docTableRow();
                            docTableCell TAfuptr2tc1 = new docTableCell();
                            TAfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            TAfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                            TAfuptr2.Append(TAfuptr2tc1);

                            docTableCell TAfuptr2tc2 = new docTableCell();
                            TAfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            TAfuptr2tc2.Append(new Paragraph(new Run(new Text())));
                            TAfuptr2.Append(TAfuptr2tc2);
                            TAFuptable.Append(TAfuptr2);

                            //-----------3rd Row
                            docTableRow TAfuptr3 = new docTableRow();
                            docTableCell TAfuptr3tc1 = new docTableCell();
                            TAfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            TAfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                            TAfuptr3.Append(TAfuptr3tc1);

                            docTableCell TAfuptr3tc2 = new docTableCell();
                            TAfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            TAfuptr3tc2.Append(new Paragraph(new Run(new Text())));
                            TAfuptr3.Append(TAfuptr3tc2);
                            TAFuptable.Append(TAfuptr3);

                            //-----------4th Row
                            docTableRow TAfuptr4 = new docTableRow();
                            docTableCell TAfuptr4tc1 = new docTableCell();
                            TAfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            TAfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                            TAfuptr4.Append(TAfuptr4tc1);

                            docTableCell TAfuptr4tc2 = new docTableCell();
                            TAfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            TAfuptr4tc2.Append(new Paragraph(new Run(new Text())));
                            TAfuptr4.Append(TAfuptr4tc2);
                            TAFuptable.Append(TAfuptr4);
                            body.Append(TAFuptable);
                        }
                        //empty space
                        Paragraph wrdParaempty = new Paragraph(
                    new ParagraphProperties(
                        new ParagraphStyleId() { Val = "comlogEmpty" }),
                    new Run(
                        new Text()));
                        body.Append(wrdParaempty);
                }
            }

            #endregion

            #region records of Data Collection

            Paragraph wrdParaDC = new Paragraph(
        new ParagraphProperties(
            new ParagraphStyleId() { Val = "comlogSectionHeading" }),
        new Run(
            new Text("Data Collection Communication")));
            body.Append(wrdParaDC);

            var DCrws = from rws in db.TALogCommunications
                        join grantee in db.MagnetGrantees on
                         rws.PK_GenInfoID equals grantee.ID
                        join contact in db.TALogRoles on
                        rws.role_id equals contact.id
                        where rws.Date >= startDate &&
                              rws.Date < endDate &&
                              rws.DataCollectionComm != ""
                        orderby rws.DataCollectionComm, rws.Date
                        select new
                        {
                            CommID = rws.id,
                            DCCategory = rws.DataCollectionComm,
                            grantee.GranteeName,
                            caller = rws.PsnCommWith,
                            contact.rolename,
                            rws.RequestStatus,
                            rws.Date,
                            rws.commTime,
                            rws.FulfillDate,
                            rws.completetime,
                            rws.CommDescription,
                            rws.ActionTaken,
                            rws.RequiredDes
                        };

            foreach (var comlog in DCrws)
            {
                if (!string.IsNullOrEmpty(comlog.DCCategory.Trim()))
                {

                    docTable DCdTLtable = new docTable();
                    TableProperties DCdTLtblPr = new TableProperties();
                    DocumentFormat.OpenXml.Wordprocessing.TableStyle DCdTLtblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                    DCdTLtblStyle.Val = "comlogtblstyle";
                    DCdTLtblPr.AppendChild(DCdTLtblStyle);
                    DCdTLtable.AppendChild(DCdTLtblPr);

                    #region 12 items
                    //-----------First Row
                    docTableRow DCtr = new docTableRow();
                    docTableCell DCtc1 = new docTableCell();
                    DCtc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtc1.Append(new Paragraph(new Run(new Text("Data collection communication category: "))));
                    DCtr.Append(DCtc1);

                    docTableCell DCtc2 = new docTableCell();
                    DCtc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtc2.Append(new Paragraph(new Run(new Text(comlog.DCCategory))));
                    DCtr.Append(DCtc2);
                    DCdTLtable.Append(DCtr);

                    //-----------2nd Row
                    docTableRow DCtr2 = new docTableRow();
                    docTableCell DCtr2tc1 = new docTableCell();
                    DCtr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr2tc1.Append(new Paragraph(new Run(new Text("Grantee name: "))));
                    DCtr2.Append(DCtr2tc1);

                    docTableCell DCtr2tc2 = new docTableCell();
                    DCtr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr2tc2.Append(new Paragraph(new Run(new Text(comlog.GranteeName))));
                    DCtr2.Append(DCtr2tc2);
                    DCdTLtable.Append(DCtr2);

                    //-----------3rd Row
                    docTableRow DCtr3 = new docTableRow();
                    docTableCell DCtr3tc1 = new docTableCell();
                    DCtr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr3tc1.Append(new Paragraph(new Run(new Text("Caller: "))));
                    DCtr3.Append(DCtr3tc1);

                    docTableCell DCtr3tc2 = new docTableCell();
                    DCtr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr3tc2.Append(new Paragraph(new Run(new Text(comlog.caller))));
                    DCtr3.Append(DCtr3tc2);
                    DCdTLtable.Append(DCtr3);

                    //-----------4th Row
                    docTableRow DCtr4 = new docTableRow();
                    docTableCell DCtr4tc1 = new docTableCell();
                    DCtr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr4tc1.Append(new Paragraph(new Run(new Text("Role: "))));
                    DCtr4.Append(DCtr4tc1);

                    docTableCell DCtr4tc2 = new docTableCell();
                    DCtr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr4tc2.Append(new Paragraph(new Run(new Text(comlog.rolename))));
                    DCtr4.Append(DCtr4tc2);
                    DCdTLtable.Append(DCtr4);

                    //-----------5th Row
                    docTableRow DCtr5 = new docTableRow();
                    docTableCell DCtr5tc1 = new docTableCell();
                    DCtr5tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr5tc1.Append(new Paragraph(new Run(new Text("Request status: "))));
                    DCtr5.Append(DCtr5tc1);

                    docTableCell DCtr5tc2 = new docTableCell();
                    DCtr5tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr5tc2.Append(new Paragraph(new Run(new Text(comlog.RequestStatus))));
                    DCtr5.Append(DCtr5tc2);
                    DCdTLtable.Append(DCtr5);

                    //-----------6th Row
                    docTableRow DCtr6 = new docTableRow();
                    docTableCell DCtr6tc1 = new docTableCell();
                    DCtr6tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr6tc1.Append(new Paragraph(new Run(new Text("Date: "))));
                    DCtr6.Append(DCtr6tc1);

                    docTableCell DCtr6tc2 = new docTableCell();
                    DCtr6tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr6tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.Date)))));
                    DCtr6.Append(DCtr6tc2);
                    DCdTLtable.Append(DCtr6);

                    //-----------7th Row
                    docTableRow DCtr7 = new docTableRow();
                    docTableCell DCtr7tc1 = new docTableCell();
                    DCtr7tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr7tc1.Append(new Paragraph(new Run(new Text("Time: "))));
                    DCtr7.Append(DCtr7tc1);

                    docTableCell DCtr7tc2 = new docTableCell();
                    DCtr7tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr7tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.commTime).ToString("h:mm tt")))));
                    DCtr7.Append(DCtr7tc2);
                    DCdTLtable.Append(DCtr7);

                    //-----------8th Row
                    docTableRow DCtr8 = new docTableRow();
                    docTableCell DCtr8tc1 = new docTableCell();
                    DCtr8tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr8tc1.Append(new Paragraph(new Run(new Text("TA request fulfillment date: "))));
                    DCtr8.Append(DCtr8tc1);

                    docTableCell DCtr8tc2 = new docTableCell();
                    DCtr8tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr8tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.FulfillDate)))));
                    DCtr8.Append(DCtr8tc2);
                    DCdTLtable.Append(DCtr8);

                    //-----------9th Row
                    docTableRow DCtr9 = new docTableRow();
                    docTableCell DCtr9tc1 = new docTableCell();
                    DCtr9tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr9tc1.Append(new Paragraph(new Run(new Text("Time completed: "))));
                    DCtr9.Append(DCtr9tc1);

                    docTableCell DCtr9tc2 = new docTableCell();
                    DCtr9tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr9tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.completetime).ToString("h:mm tt")))));
                    DCtr9.Append(DCtr9tc2);
                    DCdTLtable.Append(DCtr9);

                    //-----------10th Row
                    docTableRow DCtr10 = new docTableRow();
                    docTableCell DCtr10tc1 = new docTableCell();
                    DCtr10tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr10tc1.Append(new Paragraph(new Run(new Text("Description of communication/request: "))));
                    DCtr10.Append(DCtr10tc1);

                    docTableCell DCtr10tc2 = new docTableCell();
                    DCtr10tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr10tc2.Append(new Paragraph(new Run(new Text(comlog.CommDescription))));
                    DCtr10.Append(DCtr10tc2);
                    DCdTLtable.Append(DCtr10);

                    //-----------11th Row
                    docTableRow DCtr11 = new docTableRow();
                    docTableCell DCtr11tc1 = new docTableCell();
                    DCtr11tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr11tc1.Append(new Paragraph(new Run(new Text("Actions taken: "))));
                    DCtr11.Append(DCtr11tc1);

                    docTableCell DCtr11tc2 = new docTableCell();
                    DCtr11tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr11tc2.Append(new Paragraph(new Run(new Text(comlog.ActionTaken))));
                    DCtr11.Append(DCtr11tc2);
                    DCdTLtable.Append(DCtr11);

                    //-----------12th Row
                    docTableRow DCtr12 = new docTableRow();
                    docTableCell DCtr12tc1 = new docTableCell();
                    DCtr12tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    DCtr12tc1.Append(new Paragraph(new Run(new Text("Description of follow-up required: "))));
                    DCtr12.Append(DCtr12tc1);

                    docTableCell DCtr12tc2 = new docTableCell();
                    DCtr12tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    DCtr12tc2.Append(new Paragraph(new Run(new Text(comlog.RequiredDes))));
                    DCtr12.Append(DCtr12tc2);
                    DCdTLtable.Append(DCtr12);
                    body.Append(DCdTLtable);
                    #endregion

                    var followups = TALogFollowUp.Find(x => x.PK_CommID == comlog.CommID);

                    if (followups.Count > 0)
                    {

                        foreach (var item in followups)
                        {
                            Paragraph wrdParaProjectDetailDCFup = new Paragraph(
                                        new ParagraphProperties(
                                        new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                                        new Run(
                                        new Text("Follow up items")));
                            body.Append(wrdParaProjectDetailDCFup);

                            docTable DCFuptable = new docTable();
                            TableProperties DCFuptblPr = new TableProperties();
                            DocumentFormat.OpenXml.Wordprocessing.TableStyle DCFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                            DCFuptblStyle.Val = "comlogtblstyle";
                            DCFuptblPr.AppendChild(DCFuptblStyle);
                            DCFuptable.AppendChild(DCFuptblPr);

                            //-----------First Row
                            docTableRow DCfuptr = new docTableRow();
                            docTableCell DCfuptc1 = new docTableCell();
                            DCfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            DCfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                            DCfuptr.Append(DCfuptc1);

                            docTableCell DCfuptc2 = new docTableCell();
                            DCfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            DCfuptc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", item.Date)))));
                            DCfuptr.Append(DCfuptc2);
                            DCFuptable.Append(DCfuptr);

                            //-----------2nd Row
                            docTableRow DCfuptr2 = new docTableRow();
                            docTableCell DCfuptr2tc1 = new docTableCell();
                            DCfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            DCfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                            DCfuptr2.Append(DCfuptr2tc1);

                            docTableCell DCfuptr2tc2 = new docTableCell();
                            DCfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            DCfuptr2tc2.Append(new Paragraph(new Run(new Text(item.FollowupPerson))));
                            DCfuptr2.Append(DCfuptr2tc2);
                            DCFuptable.Append(DCfuptr2);

                            //-----------3rd Row
                            docTableRow DCfuptr3 = new docTableRow();
                            docTableCell DCfuptr3tc1 = new docTableCell();
                            DCfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            DCfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                            DCfuptr3.Append(DCfuptr3tc1);

                            docTableCell DCfuptr3tc2 = new docTableCell();
                            DCfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            DCfuptr3tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Notes)))));
                            DCfuptr3.Append(DCfuptr3tc2);
                            DCFuptable.Append(DCfuptr3);

                            //-----------4th Row
                            docTableRow DCfuptr4 = new docTableRow();
                            docTableCell DCfuptr4tc1 = new docTableCell();
                            DCfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            DCfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                            DCfuptr4.Append(DCfuptr4tc1);

                            docTableCell DCfuptr4tc2 = new docTableCell();
                            DCfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            DCfuptr4tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Result)))));
                            DCfuptr4.Append(DCfuptr4tc2);
                            DCFuptable.Append(DCfuptr4);
                            body.Append(DCFuptable);
                        }

                    }
                    else
                    {
                        Paragraph wrdParaProjectDetailDCFup = new Paragraph(
                                       new ParagraphProperties(
                                       new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                                       new Run(
                                       new Text("Follow up items")));
                        body.Append(wrdParaProjectDetailDCFup);

                        docTable DCFuptable = new docTable();
                        TableProperties DCFuptblPr = new TableProperties();
                        DocumentFormat.OpenXml.Wordprocessing.TableStyle DCFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                        DCFuptblStyle.Val = "comlogtblstyle";
                        DCFuptblPr.AppendChild(DCFuptblStyle);
                        DCFuptable.AppendChild(DCFuptblPr);

                        //-----------First Row
                        docTableRow DCfuptr = new docTableRow();
                        docTableCell DCfuptc1 = new docTableCell();
                        DCfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        DCfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                        DCfuptr.Append(DCfuptc1);

                        docTableCell DCfuptc2 = new docTableCell();
                        DCfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        DCfuptc2.Append(new Paragraph(new Run(new Text())));
                        DCfuptr.Append(DCfuptc2);
                        DCFuptable.Append(DCfuptr);

                        //-----------2nd Row
                        docTableRow DCfuptr2 = new docTableRow();
                        docTableCell DCfuptr2tc1 = new docTableCell();
                        DCfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        DCfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                        DCfuptr2.Append(DCfuptr2tc1);

                        docTableCell DCfuptr2tc2 = new docTableCell();
                        DCfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        DCfuptr2tc2.Append(new Paragraph(new Run(new Text())));
                        DCfuptr2.Append(DCfuptr2tc2);
                        DCFuptable.Append(DCfuptr2);

                        //-----------3rd Row
                        docTableRow DCfuptr3 = new docTableRow();
                        docTableCell DCfuptr3tc1 = new docTableCell();
                        DCfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        DCfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                        DCfuptr3.Append(DCfuptr3tc1);

                        docTableCell DCfuptr3tc2 = new docTableCell();
                        DCfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        DCfuptr3tc2.Append(new Paragraph(new Run(new Text())));
                        DCfuptr3.Append(DCfuptr3tc2);
                        DCFuptable.Append(DCfuptr3);

                        //-----------4th Row
                        docTableRow DCfuptr4 = new docTableRow();
                        docTableCell DCfuptr4tc1 = new docTableCell();
                        DCfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        DCfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                        DCfuptr4.Append(DCfuptr4tc1);

                        docTableCell DCfuptr4tc2 = new docTableCell();
                        DCfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        DCfuptr4tc2.Append(new Paragraph(new Run(new Text())));
                        DCfuptr4.Append(DCfuptr4tc2);
                        DCFuptable.Append(DCfuptr4);
                        body.Append(DCFuptable);
                    }
                    //empty space
                    Paragraph wrdParaProjectDetailDGCempty = new Paragraph(
                new ParagraphProperties(
                    new ParagraphStyleId() { Val = "comlogEmpty" }),
                new Run(
                    new Text()));
                    body.Append(wrdParaProjectDetailDGCempty);
                }
            } //end foreach
            #endregion

            #region Reporting Communication

            Paragraph wrdParaRecRpt = new Paragraph(
        new ParagraphProperties(
            new ParagraphStyleId() { Val = "comlogSectionHeading" }),
        new Run(
            new Text("Reporting Communication")));
            body.Append(wrdParaRecRpt);

            var RPTdtlrws = from rws in db.TALogCommunications
                            join grantee in db.MagnetGrantees on
                             rws.PK_GenInfoID equals grantee.ID
                            join contact in db.TALogRoles on
                            rws.role_id equals contact.id
                            where rws.Date >= startDate &&
                                  rws.Date < endDate &&
                                  rws.CommType != ""
                            orderby rws.CommType, rws.Date
                            select new
                            {
                                CommID = rws.id,
                                RPTdtlCategory = rws.CommType,
                                grantee.GranteeName,
                                caller = rws.PsnCommWith,
                                contact.rolename,
                                rws.RequestStatus,
                                rws.Date,
                                rws.commTime,
                                rws.FulfillDate,
                                rws.completetime,
                                rws.CommDescription,
                                rws.ActionTaken,
                                rws.RequiredDes,
                                rws.ReportComm
                            };

            foreach (var comlog in RPTdtlrws)
            {
                if (!string.IsNullOrEmpty(comlog.RPTdtlCategory.Trim()))
                {

                    docTable RPTdtltable = new docTable();
                    TableProperties RPTdtltblPr = new TableProperties();
                    DocumentFormat.OpenXml.Wordprocessing.TableStyle RPTdtltblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                    RPTdtltblStyle.Val = "comlogtblstyle";
                    RPTdtltblPr.AppendChild(RPTdtltblStyle);
                    RPTdtltable.AppendChild(RPTdtltblPr);

                    #region 12 items
                    //-----------First Row
                    docTableRow RPTdtltr = new docTableRow();
                    docTableCell RPTdtltc1 = new docTableCell();
                    RPTdtltc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltc1.Append(new Paragraph(new Run(new Text("Reporting communication category: "))));
                    RPTdtltr.Append(RPTdtltc1);

                    docTableCell RPTdtltc2 = new docTableCell();
                    RPTdtltc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltc2.Append(new Paragraph(new Run(new Text(comlog.ReportComm))));
                    RPTdtltr.Append(RPTdtltc2);
                    RPTdtltable.Append(RPTdtltr);

                    //-----------2nd Row
                    docTableRow RPTdtltr2 = new docTableRow();
                    docTableCell RPTdtltr2tc1 = new docTableCell();
                    RPTdtltr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr2tc1.Append(new Paragraph(new Run(new Text("Grantee name: "))));
                    RPTdtltr2.Append(RPTdtltr2tc1);

                    docTableCell RPTdtltr2tc2 = new docTableCell();
                    RPTdtltr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr2tc2.Append(new Paragraph(new Run(new Text(comlog.GranteeName))));
                    RPTdtltr2.Append(RPTdtltr2tc2);
                    RPTdtltable.Append(RPTdtltr2);

                    //-----------3rd Row
                    docTableRow RPTdtltr3 = new docTableRow();
                    docTableCell RPTdtltr3tc1 = new docTableCell();
                    RPTdtltr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr3tc1.Append(new Paragraph(new Run(new Text("Caller: "))));
                    RPTdtltr3.Append(RPTdtltr3tc1);

                    docTableCell RPTdtltr3tc2 = new docTableCell();
                    RPTdtltr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr3tc2.Append(new Paragraph(new Run(new Text(comlog.caller))));
                    RPTdtltr3.Append(RPTdtltr3tc2);
                    RPTdtltable.Append(RPTdtltr3);

                    //-----------4th Row
                    docTableRow RPTdtltr4 = new docTableRow();
                    docTableCell RPTdtltr4tc1 = new docTableCell();
                    RPTdtltr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr4tc1.Append(new Paragraph(new Run(new Text("Role: "))));
                    RPTdtltr4.Append(RPTdtltr4tc1);

                    docTableCell RPTdtltr4tc2 = new docTableCell();
                    RPTdtltr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr4tc2.Append(new Paragraph(new Run(new Text(comlog.rolename))));
                    RPTdtltr4.Append(RPTdtltr4tc2);
                    RPTdtltable.Append(RPTdtltr4);

                    //-----------5th Row
                    docTableRow RPTdtltr5 = new docTableRow();
                    docTableCell RPTdtltr5tc1 = new docTableCell();
                    RPTdtltr5tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr5tc1.Append(new Paragraph(new Run(new Text("Request status: "))));
                    RPTdtltr5.Append(RPTdtltr5tc1);

                    docTableCell RPTdtltr5tc2 = new docTableCell();
                    RPTdtltr5tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr5tc2.Append(new Paragraph(new Run(new Text(comlog.RequestStatus))));
                    RPTdtltr5.Append(RPTdtltr5tc2);
                    RPTdtltable.Append(RPTdtltr5);

                    //-----------6th Row
                    docTableRow RPTdtltr6 = new docTableRow();
                    docTableCell RPTdtltr6tc1 = new docTableCell();
                    RPTdtltr6tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr6tc1.Append(new Paragraph(new Run(new Text("Date: "))));
                    RPTdtltr6.Append(RPTdtltr6tc1);

                    docTableCell RPTdtltr6tc2 = new docTableCell();
                    RPTdtltr6tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr6tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.Date)))));
                    RPTdtltr6.Append(RPTdtltr6tc2);
                    RPTdtltable.Append(RPTdtltr6);

                    //-----------7th Row
                    docTableRow RPTdtltr7 = new docTableRow();
                    docTableCell RPTdtltr7tc1 = new docTableCell();
                    RPTdtltr7tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr7tc1.Append(new Paragraph(new Run(new Text("Time: "))));
                    RPTdtltr7.Append(RPTdtltr7tc1);

                    docTableCell RPTdtltr7tc2 = new docTableCell();
                    RPTdtltr7tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr7tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.commTime).ToString("h:mm tt")))));
                    RPTdtltr7.Append(RPTdtltr7tc2);
                    RPTdtltable.Append(RPTdtltr7);

                    //-----------8th Row
                    docTableRow RPTdtltr8 = new docTableRow();
                    docTableCell RPTdtltr8tc1 = new docTableCell();
                    RPTdtltr8tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr8tc1.Append(new Paragraph(new Run(new Text("TA request fulfillment date: "))));
                    RPTdtltr8.Append(RPTdtltr8tc1);

                    docTableCell RPTdtltr8tc2 = new docTableCell();
                    RPTdtltr8tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr8tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", comlog.FulfillDate)))));
                    RPTdtltr8.Append(RPTdtltr8tc2);
                    RPTdtltable.Append(RPTdtltr8);

                    //-----------9th Row
                    docTableRow RPTdtltr9 = new docTableRow();
                    docTableCell RPTdtltr9tc1 = new docTableCell();
                    RPTdtltr9tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr9tc1.Append(new Paragraph(new Run(new Text("Time completed: "))));
                    RPTdtltr9.Append(RPTdtltr9tc1);

                    docTableCell RPTdtltr9tc2 = new docTableCell();
                    RPTdtltr9tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr9tc2.Append(new Paragraph(new Run(new Text(Convert.ToDateTime(comlog.completetime).ToString("h:mm tt")))));
                    RPTdtltr9.Append(RPTdtltr9tc2);
                    RPTdtltable.Append(RPTdtltr9);

                    //-----------10th Row
                    docTableRow RPTdtltr10 = new docTableRow();
                    docTableCell RPTdtltr10tc1 = new docTableCell();
                    RPTdtltr10tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr10tc1.Append(new Paragraph(new Run(new Text("Description of communication/request: "))));
                    RPTdtltr10.Append(RPTdtltr10tc1);

                    docTableCell RPTdtltr10tc2 = new docTableCell();
                    RPTdtltr10tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr10tc2.Append(new Paragraph(new Run(new Text(comlog.CommDescription))));
                    RPTdtltr10.Append(RPTdtltr10tc2);
                    RPTdtltable.Append(RPTdtltr10);

                    //-----------11th Row
                    docTableRow RPTdtltr11 = new docTableRow();
                    docTableCell RPTdtltr11tc1 = new docTableCell();
                    RPTdtltr11tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr11tc1.Append(new Paragraph(new Run(new Text("Actions taken: "))));
                    RPTdtltr11.Append(RPTdtltr11tc1);

                    docTableCell RPTdtltr11tc2 = new docTableCell();
                    RPTdtltr11tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr11tc2.Append(new Paragraph(new Run(new Text(comlog.ActionTaken))));
                    RPTdtltr11.Append(RPTdtltr11tc2);
                    RPTdtltable.Append(RPTdtltr11);

                    //-----------12th Row
                    docTableRow RPTdtltr12 = new docTableRow();
                    docTableCell RPTdtltr12tc1 = new docTableCell();
                    RPTdtltr12tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                    RPTdtltr12tc1.Append(new Paragraph(new Run(new Text("Description of follow-up required: "))));
                    RPTdtltr12.Append(RPTdtltr12tc1);

                    docTableCell RPTdtltr12tc2 = new docTableCell();
                    RPTdtltr12tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                    RPTdtltr12tc2.Append(new Paragraph(new Run(new Text(comlog.RequiredDes))));
                    RPTdtltr12.Append(RPTdtltr12tc2);
                    RPTdtltable.Append(RPTdtltr12);
                    body.Append(RPTdtltable);
                    #endregion

                    var followups = TALogFollowUp.Find(x => x.PK_CommID == comlog.CommID);

                    if (followups.Count > 0)
                    {

                        foreach (var item in followups)
                        {
                            Paragraph wrdParaProjectDetailRPTdtlFup = new Paragraph(
                                        new ParagraphProperties(
                                        new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                                        new Run(
                                        new Text("Follow up items")));
                            body.Append(wrdParaProjectDetailRPTdtlFup);

                            docTable RPTdtlFuptable = new docTable();
                            TableProperties RPTdtlFuptblPr = new TableProperties();
                            DocumentFormat.OpenXml.Wordprocessing.TableStyle RPTdtlFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                            RPTdtlFuptblStyle.Val = "comlogtblstyle";
                            RPTdtlFuptblPr.AppendChild(RPTdtlFuptblStyle);
                            RPTdtlFuptable.AppendChild(RPTdtlFuptblPr);


                            //-----------First Row
                            docTableRow RPTdtlfuptr = new docTableRow();
                            docTableCell RPTdtlfuptc1 = new docTableCell();
                            RPTdtlfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            RPTdtlfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                            RPTdtlfuptr.Append(RPTdtlfuptc1);

                            docTableCell RPTdtlfuptc2 = new docTableCell();
                            RPTdtlfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            RPTdtlfuptc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM dd, yyyy}", item.Date)))));
                            RPTdtlfuptr.Append(RPTdtlfuptc2);
                            RPTdtlFuptable.Append(RPTdtlfuptr);

                            //-----------2nd Row
                            docTableRow RPTdtlfuptr2 = new docTableRow();
                            docTableCell RPTdtlfuptr2tc1 = new docTableCell();
                            RPTdtlfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            RPTdtlfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                            RPTdtlfuptr2.Append(RPTdtlfuptr2tc1);

                            docTableCell RPTdtlfuptr2tc2 = new docTableCell();
                            RPTdtlfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            RPTdtlfuptr2tc2.Append(new Paragraph(new Run(new Text(item.FollowupPerson))));
                            RPTdtlfuptr2.Append(RPTdtlfuptr2tc2);
                            RPTdtlFuptable.Append(RPTdtlfuptr2);

                            //-----------3rd Row
                            docTableRow RPTdtlfuptr3 = new docTableRow();
                            docTableCell RPTdtlfuptr3tc1 = new docTableCell();
                            RPTdtlfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            RPTdtlfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                            RPTdtlfuptr3.Append(RPTdtlfuptr3tc1);

                            docTableCell RPTdtlfuptr3tc2 = new docTableCell();
                            RPTdtlfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            RPTdtlfuptr3tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Notes)))));
                            RPTdtlfuptr3.Append(RPTdtlfuptr3tc2);
                            RPTdtlFuptable.Append(RPTdtlfuptr3);

                            //-----------4th Row
                            docTableRow RPTdtlfuptr4 = new docTableRow();
                            docTableCell RPTdtlfuptr4tc1 = new docTableCell();
                            RPTdtlfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                            RPTdtlfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                            RPTdtlfuptr4.Append(RPTdtlfuptr4tc1);

                            docTableCell RPTdtlfuptr4tc2 = new docTableCell();
                            RPTdtlfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                            RPTdtlfuptr4tc2.Append(new Paragraph(new Run(new Text(GetPlainTextFromHtml(item.Result)))));
                            RPTdtlfuptr4.Append(RPTdtlfuptr4tc2);
                            RPTdtlFuptable.Append(RPTdtlfuptr4);
                            body.Append(RPTdtlFuptable);
                        }
                    }
                    else
                    {
                        Paragraph wrdParaProjectDetailRPTdtlFup = new Paragraph(
                                    new ParagraphProperties(
                                    new ParagraphStyleId() { Val = "comlogSectionTitle" }),
                                    new Run(
                                    new Text("Follow up items")));
                        body.Append(wrdParaProjectDetailRPTdtlFup);

                        docTable RPTdtlFuptable = new docTable();
                        TableProperties RPTdtlFuptblPr = new TableProperties();
                        DocumentFormat.OpenXml.Wordprocessing.TableStyle RPTdtlFuptblStyle = new DocumentFormat.OpenXml.Wordprocessing.TableStyle();
                        RPTdtlFuptblStyle.Val = "comlogtblstyle";
                        RPTdtlFuptblPr.AppendChild(RPTdtlFuptblStyle);
                        RPTdtlFuptable.AppendChild(RPTdtlFuptblPr);


                        //-----------First Row
                        docTableRow RPTdtlfuptr = new docTableRow();
                        docTableCell RPTdtlfuptc1 = new docTableCell();
                        RPTdtlfuptc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        RPTdtlfuptc1.Append(new Paragraph(new Run(new Text("Date: "))));
                        RPTdtlfuptr.Append(RPTdtlfuptc1);

                        docTableCell RPTdtlfuptc2 = new docTableCell();
                        RPTdtlfuptc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        RPTdtlfuptc2.Append(new Paragraph(new Run(new Text())));
                        RPTdtlfuptr.Append(RPTdtlfuptc2);
                        RPTdtlFuptable.Append(RPTdtlfuptr);

                        //-----------2nd Row
                        docTableRow RPTdtlfuptr2 = new docTableRow();
                        docTableCell RPTdtlfuptr2tc1 = new docTableCell();
                        RPTdtlfuptr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        RPTdtlfuptr2tc1.Append(new Paragraph(new Run(new Text("Name of person who called: "))));
                        RPTdtlfuptr2.Append(RPTdtlfuptr2tc1);

                        docTableCell RPTdtlfuptr2tc2 = new docTableCell();
                        RPTdtlfuptr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        RPTdtlfuptr2tc2.Append(new Paragraph(new Run(new Text())));
                        RPTdtlfuptr2.Append(RPTdtlfuptr2tc2);
                        RPTdtlFuptable.Append(RPTdtlfuptr2);

                        //-----------3rd Row
                        docTableRow RPTdtlfuptr3 = new docTableRow();
                        docTableCell RPTdtlfuptr3tc1 = new docTableCell();
                        RPTdtlfuptr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        RPTdtlfuptr3tc1.Append(new Paragraph(new Run(new Text("Notes: "))));
                        RPTdtlfuptr3.Append(RPTdtlfuptr3tc1);

                        docTableCell RPTdtlfuptr3tc2 = new docTableCell();
                        RPTdtlfuptr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        RPTdtlfuptr3tc2.Append(new Paragraph(new Run(new Text())));
                        RPTdtlfuptr3.Append(RPTdtlfuptr3tc2);
                        RPTdtlFuptable.Append(RPTdtlfuptr3);

                        //-----------4th Row
                        docTableRow RPTdtlfuptr4 = new docTableRow();
                        docTableCell RPTdtlfuptr4tc1 = new docTableCell();
                        RPTdtlfuptr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "4400" }));
                        RPTdtlfuptr4tc1.Append(new Paragraph(new Run(new Text("Result of follow-up: "))));
                        RPTdtlfuptr4.Append(RPTdtlfuptr4tc1);

                        docTableCell RPTdtlfuptr4tc2 = new docTableCell();
                        RPTdtlfuptr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "5000" }));
                        RPTdtlfuptr4tc2.Append(new Paragraph(new Run(new Text())));
                        RPTdtlfuptr4.Append(RPTdtlfuptr4tc2);
                        RPTdtlFuptable.Append(RPTdtlfuptr4);

                        body.Append(RPTdtlFuptable);
                    }
                    //empty space
                    Paragraph wrdParaRPTempty = new Paragraph(
                            new ParagraphProperties(
                                new ParagraphStyleId() { Val = "comlogEmpty" }),
                            new Run(new Text()));
                    body.Append(wrdParaRPTempty);
                }

            } //end foreach

            #endregion

            #endregion

        }
    }

    private string GetPlainTextFromHtml(string htmlString)
    {
        string htmlTagPattern = "<.*?>";
        var regexCss = new Regex("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        htmlString = regexCss.Replace(htmlString, string.Empty);
        htmlString = Regex.Replace(htmlString, htmlTagPattern, string.Empty);
        htmlString = Regex.Replace(htmlString, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
        htmlString = htmlString.Replace("&nbsp;", string.Empty);

        return htmlString;
    }


    protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlMonth.Items.Clear();
        ddlMonth.Items.Add(new System.Web.UI.WebControls.ListItem("Select a month", ""));
        btnReport.Visible = false;
        pnlMain.Visible = false;
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue == "1")
        {
            pnlGranteeSchRpt.Visible = false;
            pnlComlogRpt.Visible = true;
        }
        else
        {
            pnlGranteeSchRpt.Visible = true;
            pnlComlogRpt.Visible = false;
        }
    }

    private string getMktstatus(string marketingContents, string IDs)
    {
        string strRtn = "";

                    char Delim = '\'';
                    string cbxValue = "";

                    if (marketingContents != null && marketingContents.Contains(IDs))
                    {
                        int startPos = marketingContents.IndexOf(IDs); //txtOther1311\"market/recruitmentoutreach other<cbx141\"1'txtOther141\"ww'...
                        string includecbxIDsString = marketingContents.Substring(startPos);
                        string temp2 = includecbxIDsString.Split('<')[0];          //cbx133\"1'txtOther1311\"market/recruitmentoutreach other<  or 
                        string temp3 = temp2.Split(Delim)[0];         //cbx133\"1

                        cbxValue = temp3.Split('"')[1];

                        if (cbxValue == "1")
                        {
                            strRtn = "X";
                        }
                        else if (cbxValue == "0")
                        {
                            strRtn = "";
                        }
                        else
                            strRtn = cbxValue;

                    }
                return strRtn;
    }



    private DataTable GContextData()  // G-level
    {
        DataTable rptTbl = new DataTable();
        int reportyear = Convert.ToInt32(Session["ReportYear"]),
            cohorttype = Convert.ToInt32(Session["CohortType"]);
        using (vwContextualfactorDataClassesDataContext db = new vwContextualfactorDataClassesDataContext())
        {
            rptTbl = db.spp_getGContextData(cohorttype, reportyear).ToDataTable();
        }
        return rptTbl;
    }

    private DataTable SchContextData()   //sch-level
    {
        DataTable rptTbl = new DataTable();
        int reportyear = Convert.ToInt32(Session["ReportYear"]),
            cohorttype = Convert.ToInt32(Session["CohortType"]);
        using (vwContextualfactorDataClassesDataContext db = new vwContextualfactorDataClassesDataContext())
        {
            rptTbl = db.spp_getSchContextData(cohorttype, reportyear).ToDataTable();
        }
        return rptTbl;
    }

    private DataTable SchActData()  //sch-level
    {
        DataTable rptTbl = new DataTable();

        rptTbl.Columns.Add("GranteeID", typeof(string));
        rptTbl.Columns.Add("GranteeName", typeof(string));
        rptTbl.Columns.Add("SchoolID", typeof(string));
        rptTbl.Columns.Add("School_Name", typeof(string));

        rptTbl.Columns.Add("Theme_art/hum", typeof(string));
        rptTbl.Columns.Add("Theme_career", typeof(string));
        rptTbl.Columns.Add("Theme_socstdy", typeof(string));
        rptTbl.Columns.Add("Theme_steam", typeof(string));
        rptTbl.Columns.Add("Theme_stem", typeof(string));
        rptTbl.Columns.Add("Theme_other", typeof(string));
        rptTbl.Columns.Add("Theme_other(textbox)", typeof(string));
        rptTbl.Columns.Add("IM_inq", typeof(string));
        rptTbl.Columns.Add("IM_mult_intel", typeof(string));
        rptTbl.Columns.Add("IM_obj", typeof(string));
        rptTbl.Columns.Add("IM_prob", typeof(string));
        rptTbl.Columns.Add("IM_proj", typeof(string));
        rptTbl.Columns.Add("IM_other", typeof(string));
        rptTbl.Columns.Add("IM_other(textbox)", typeof(string));

        var MagnetGrantees = MagnetGrantee.Find(x => x.CohortType == Convert.ToInt32(Session["CohortType"]) && x.isMSAPGrantee == true);

        foreach (MagnetGrantee itm in MagnetGrantees)
        {
            int granteeID = itm.ID;

            var schools = MagnetSchool.Find(x => x.GranteeID == granteeID && x.ReportYear == Convert.ToInt32(Session["ReportYear"]));
            
            foreach (MagnetSchool sch in schools)
            {
                string themecat = sch.TAthemeCat,
                       deliveryMethod = sch.DeliveryMethod;

                Hashtable themes = new Hashtable();
                if (!string.IsNullOrEmpty(themecat))
                {
                    foreach (string key in themecat.Split(','))
                    {
                        themes.Add(key, "X");
                    }
                }

                DataRow dr = rptTbl.NewRow();
                dr["GranteeID"] = granteeID;
                dr["GranteeName"] = itm.GranteeName;

                dr["SchoolID"] = sch.SchoolCode;
                dr["School_Name"] = sch.SchoolName;
                if (!string.IsNullOrEmpty(themecat))
                {
                    dr["Theme_art/hum"] = themes["1"]!=null ? "X" : "";
                    dr["Theme_career"] = themes["2"] != null ? "X" : "";
                    dr["Theme_socstdy"] = themes["7"] != null ? "X" : "";
                    dr["Theme_steam"] = themes["9"] != null ? "X" : "";
                    dr["Theme_stem"] = themes["10"] != null ? "X" : "";
                    dr["Theme_other"] = themes["5"] != null ? "X" : "";
                    dr["Theme_other(textbox)"] = "";
                }
                if (!string.IsNullOrEmpty(deliveryMethod))
                {
                    dr["IM_inq"] = deliveryMethod.Contains("41") ? "X" : "";
                    dr["IM_mult_intel"] = deliveryMethod.Contains("42") ? "X" : "";
                    dr["IM_obj"] = deliveryMethod.Contains("43") ? "X" : "";
                    dr["IM_prob"] = deliveryMethod.Contains("44") ? "X" : "";
                    dr["IM_proj"] = deliveryMethod.Contains("49") ? "X" : "";
                    dr["IM_other"] = deliveryMethod.Contains("48") ? "X" : "";
                    dr["IM_other(textbox)"] = string.IsNullOrEmpty(sch.DeliveryMethodOther) ? "" : sch.DeliveryMethodOther;
                }
                rptTbl.Rows.Add(dr);
            }

        }

        return rptTbl;
    }

    private DataTable ProfDevData() //G-level
    {
        bool isPDD_coluni_prgrm = false,
             isPDD_ddctd_plntime = false,
             isPDD_indv_prof_devl = false,
             isPDD_mentorcoach = false,
             isPDD_peersuper_feedbk = false,
             isPDD_plc = false,
             isPDD_realwrld_exp = false,
             isPDD_wrkshpconfr = false,
             isPDD_other = false,
             isPDT_clsrm_mgmt = false,
             isPDT_commun_outrch = false,
             isPDT_currclm = false,
             isPDT_data_drvn_instruct = false,
             isPDT_diversity = false,
             isPDT_ell = false,
             isPDT_eval = false,
             isPDT_fmly_engage = false,
             isPDT_instruct_method = false,
             isPDT_mag_theme = false,
             isPDT_prgrm_mgmt = false,
             isPDT_prgrm_sustain = false,
             isPDT_special_edu = false,
             isPDT_team_bldg = false,
             isPDT_tech = false,
             isPDT_other=false;

        string txtPDD_other="", txtPDT_other="";
        DataTable rptTbl = new DataTable();
       
        rptTbl.Columns.Add("GranteeID", typeof(string));
        rptTbl.Columns.Add("GranteeName", typeof(string));
        //rptTbl.Columns.Add("SchoolID", typeof(string));
        //rptTbl.Columns.Add("School_Name", typeof(string));

        rptTbl.Columns.Add("PDD_col/uni_prgrm", typeof(string));
        rptTbl.Columns.Add("PDD_ddctd_plntime", typeof(string));
        rptTbl.Columns.Add("PDD_indv_prof_devl", typeof(string));
        rptTbl.Columns.Add("PDD_mentor/coach", typeof(string));
        rptTbl.Columns.Add("PDD_peer/super_feedbk", typeof(string));
        rptTbl.Columns.Add("PDD_plc", typeof(string));
        rptTbl.Columns.Add("PDD_realwrld_exp", typeof(string));
        rptTbl.Columns.Add("PDD_wrkshp/confr", typeof(string));
        rptTbl.Columns.Add("PDD_other", typeof(string));
        rptTbl.Columns.Add("PDD_other(textbox)", typeof(string));
        rptTbl.Columns.Add("PDT_clsrm_mgmt", typeof(string));
        rptTbl.Columns.Add("PDT_commun_outrch", typeof(string));
        rptTbl.Columns.Add("PDT_currclm", typeof(string));
        rptTbl.Columns.Add("PDT_data_drvn_instruct", typeof(string));
        rptTbl.Columns.Add("PDT_diversity", typeof(string));
        rptTbl.Columns.Add("PDT_ell", typeof(string));
        rptTbl.Columns.Add("PDT_eval", typeof(string));
        rptTbl.Columns.Add("PDT_fmly_engage", typeof(string));
        rptTbl.Columns.Add("PDT_instruct_method", typeof(string));
        rptTbl.Columns.Add("PDT_mag_theme", typeof(string));
        rptTbl.Columns.Add("PDT_prgrm_mgmt", typeof(string));
        rptTbl.Columns.Add("PDT_prgrm_sustain", typeof(string));
        rptTbl.Columns.Add("PDT_special_edu", typeof(string));
        rptTbl.Columns.Add("PDT_team_bldg", typeof(string));
        rptTbl.Columns.Add("PDT_tech", typeof(string));
        rptTbl.Columns.Add("PDT_other", typeof(string));
        rptTbl.Columns.Add("PDT_other(textbox)", typeof(string));

        var MagnetGrantees = MagnetGrantee.Find(x => x.CohortType == Convert.ToInt32(Session["CohortType"]));

        foreach (MagnetGrantee itm in MagnetGrantees)
        {
            isPDD_coluni_prgrm = false;
            isPDD_ddctd_plntime = false;
            isPDD_indv_prof_devl = false;
            isPDD_mentorcoach = false;
            isPDD_peersuper_feedbk = false;
            isPDD_plc = false;
            isPDD_realwrld_exp = false;
            isPDD_wrkshpconfr = false;
            isPDD_other = false;
            isPDT_clsrm_mgmt = false;
            isPDT_commun_outrch = false;
            isPDT_currclm = false;
            isPDT_data_drvn_instruct = false;
            isPDT_diversity = false;
            isPDT_ell = false;
            isPDT_eval = false;
            isPDT_fmly_engage = false;
            isPDT_instruct_method = false;
            isPDT_mag_theme = false;
            isPDT_prgrm_mgmt = false;
            isPDT_prgrm_sustain = false;
            isPDT_special_edu = false;
            isPDT_team_bldg = false;
            isPDT_tech = false;
            isPDT_other = false;
            txtPDD_other = ""; txtPDT_other = "";

            var schoolInfos = MagnetSchool.Find(x => x.GranteeID == itm.ID && x.ReportYear == Convert.ToInt32(Session["ReportYear"]));
            foreach (MagnetSchool sch in schoolInfos)
            {
                string profDD = sch.ProfDevAct,
                       profDT = sch.ProfDevTopic;

                if (!string.IsNullOrEmpty(profDD))
                {
                    isPDD_coluni_prgrm = isPDD_coluni_prgrm ? true : (profDD.Contains("50") ? true : false);
                    isPDD_ddctd_plntime = isPDD_ddctd_plntime ? true : (profDD.Contains("51") ? true : false);
                    isPDD_indv_prof_devl = isPDD_indv_prof_devl ? true : (profDD.Contains("52") ? true : false);
                    isPDD_mentorcoach = isPDD_mentorcoach ? true : (profDD.Contains("54") ? true : false);
                    isPDD_peersuper_feedbk = isPDD_peersuper_feedbk ? true : (profDD.Contains("55") ? true : false);
                    isPDD_plc = isPDD_plc ? true : (profDD.Contains("57") ? true : false);
                    isPDD_realwrld_exp = isPDD_realwrld_exp ? true : (profDD.Contains("58") ? true : false);
                    isPDD_wrkshpconfr = isPDD_wrkshpconfr ? true : (profDD.Contains("59") ? true : false);
                    isPDD_other = isPDD_other ? true : (profDD.Contains("60") ? true : false);
                    txtPDD_other += string.IsNullOrEmpty(sch.ProfDevActOther) ? "" : sch.ProfDevActOther +";";
                }

                if (!string.IsNullOrEmpty(profDT))
                {
                    isPDT_clsrm_mgmt = isPDT_clsrm_mgmt ? true : (profDT.Contains("61") ? true : false);
                    isPDT_commun_outrch = isPDT_commun_outrch ? true : (profDT.Contains("62") ? true : false);
                    isPDT_currclm = isPDT_currclm ? true : (profDT.Contains("63") ? true : false);
                    isPDT_data_drvn_instruct = isPDT_data_drvn_instruct ? true : (profDT.Contains("64") ? true : false);
                    isPDT_diversity = isPDT_diversity ? true : (profDT.Contains("65") ? true : false);
                    isPDT_ell = isPDT_ell ? true : (profDT.Contains("66") ? true : false);
                    isPDT_eval = isPDT_eval ? true : (profDT.Contains("67") ? true : false);
                    isPDT_fmly_engage = isPDT_fmly_engage ? true : (profDT.Contains("68") ? true : false);
                    isPDT_instruct_method = isPDT_instruct_method ? true : (profDT.Contains("69") ? true : false);
                    isPDT_mag_theme = isPDT_mag_theme ? true : (profDT.Contains("70") ? true : false);
                    isPDT_prgrm_mgmt = isPDT_prgrm_mgmt ? true : (profDT.Contains("71") ? true : false);
                    isPDT_prgrm_sustain = isPDT_prgrm_sustain ? true : (profDT.Contains("72") ? true : false);
                    isPDT_special_edu = isPDT_special_edu ? true : (profDT.Contains("73") ? true : false);
                    isPDT_team_bldg = isPDT_team_bldg ? true : (profDT.Contains("74") ? true : false);
                    isPDT_tech = isPDT_tech ? true : (profDT.Contains("75") ? true : false);
                    isPDT_other = isPDT_other ? true : (profDT.Contains("76") ? true : false);
                    txtPDT_other += string.IsNullOrEmpty(sch.ProfDevTopicOther) ? "" : sch.ProfDevTopicOther +";";
                }



            }

            DataRow dr = rptTbl.NewRow();
            dr["GranteeID"] = itm.ID;
            dr["GranteeName"] = itm.GranteeName;


            dr["PDD_col/uni_prgrm"] = isPDD_coluni_prgrm ? "X" : "";
            dr["PDD_ddctd_plntime"] = isPDD_ddctd_plntime ? "X" : "";
            dr["PDD_indv_prof_devl"] = isPDD_indv_prof_devl ? "X" : "";
            dr["PDD_mentor/coach"] = isPDD_mentorcoach ? "X" : "";
            dr["PDD_peer/super_feedbk"] = isPDD_peersuper_feedbk ? "X" : "";
            dr["PDD_plc"] = isPDD_plc ? "X" : "";
            dr["PDD_realwrld_exp"] = isPDD_realwrld_exp ? "X" : "";
            dr["PDD_wrkshp/confr"] = isPDD_wrkshpconfr ? "X" : "";
            dr["PDD_other"] = isPDD_other ? "X" : "";
            dr["PDD_other(textbox)"] = txtPDD_other.TrimEnd(';','\r','\n');

            dr["PDT_clsrm_mgmt"] = isPDT_clsrm_mgmt ? "X" : "";
            dr["PDT_commun_outrch"] = isPDT_commun_outrch ? "X" : "";
            dr["PDT_currclm"] = isPDT_currclm ? "X" : "";
            dr["PDT_data_drvn_instruct"] = isPDT_data_drvn_instruct ? "X" : "";
            dr["PDT_diversity"] = isPDT_diversity ? "X" : "";
            dr["PDT_ell"] = isPDT_ell ? "X" : "";
            dr["PDT_eval"] = isPDT_eval ? "X" : "";
            dr["PDT_fmly_engage"] = isPDT_fmly_engage ? "X" : "";
            dr["PDT_instruct_method"] = isPDT_instruct_method ? "X" : "";
            dr["PDT_mag_theme"] = isPDT_mag_theme ? "X" : "";
            dr["PDT_prgrm_mgmt"] = isPDT_prgrm_mgmt ? "X" : "";
            dr["PDT_prgrm_sustain"] = isPDT_prgrm_sustain ? "X" : "";
            dr["PDT_special_edu"] = isPDT_special_edu ? "X" : "";
            dr["PDT_team_bldg"] = isPDT_team_bldg ? "X" : "";
            dr["PDT_tech"] = isPDT_tech ? "X" : "";
            dr["PDT_other"] = isPDT_other ? "X" : "";
            dr["PDT_other(textbox)"] = txtPDT_other.TrimEnd(';');

            rptTbl.Rows.Add(dr);
        }

        return rptTbl;
    }

    private DataTable StaffData()
    {
        bool isStaff_admn_supp = false,
             isStaff_commty_out = false,
             isStaff_constult = false,
             isStaff_curr_spec = false,
             isStaff_extnl_eval = false,
             isStaff_mag_coor = false,
             isStaff_paren_spec = false,
             isStaff_pd = false,
             isStaff_recr_mrkt = false,
             isStaff_tech_supp = false,
             isStaff_theme_spec = false,
             isStaff_other = false;
        string txtStaff_other = "";

        DataTable rptTbl = new DataTable();

        rptTbl.Columns.Add("GranteeID", typeof(string));
        rptTbl.Columns.Add("GranteeName", typeof(string));

        rptTbl.Columns.Add("Staff_admn_supp", typeof(string)); 
        rptTbl.Columns.Add("Staff_commty_out", typeof(string)); 
        rptTbl.Columns.Add("Staff_constult", typeof(string)); 
        rptTbl.Columns.Add("Staff_curr_spec", typeof(string)); 
        rptTbl.Columns.Add("Staff_extnl_eval", typeof(string)); 
        rptTbl.Columns.Add("Staff_mag_coor", typeof(string)); 
        rptTbl.Columns.Add("Staff_paren_spec", typeof(string)); 
        rptTbl.Columns.Add("Staff_pd", typeof(string)); 
        rptTbl.Columns.Add("Staff_recr_mrkt", typeof(string)); 
        rptTbl.Columns.Add("Staff_tech_supp", typeof(string)); 
        rptTbl.Columns.Add("Staff_theme_spec", typeof(string)); 
        rptTbl.Columns.Add("Staff_other", typeof(string)); 
        rptTbl.Columns.Add("Staff_other(textbox)", typeof(string));


        var MagnetGrantees = MagnetGrantee.Find(x => x.CohortType == Convert.ToInt32(Session["CohortType"]) && x.isMSAPGrantee == true);

        foreach (MagnetGrantee itm in MagnetGrantees)
        {
            isStaff_admn_supp = false;
            isStaff_commty_out = false;
            isStaff_constult = false;
            isStaff_curr_spec = false;
            isStaff_extnl_eval = false;
            isStaff_mag_coor = false;
            isStaff_paren_spec = false;
            isStaff_pd = false;
            isStaff_recr_mrkt = false;
            isStaff_tech_supp = false;
            isStaff_theme_spec = false;
            isStaff_other = false;
            txtStaff_other = "";

            foreach (MagnetSchool sch in MagnetSchool.Find(x => x.GranteeID == itm.ID && x.ReportYear == Convert.ToInt32(Session["ReportYear"]))) 
            {
                string staffs = sch.Specialists;

                if (!string.IsNullOrEmpty(staffs))
                {
                    isStaff_admn_supp = isStaff_admn_supp ? true : (staffs.Contains("93") ? true : false);
                    isStaff_commty_out = isStaff_commty_out ? true : (staffs.Contains("77") ? true : false);
                    isStaff_constult = isStaff_constult ? true : (staffs.Contains("78") ? true : false);
                    isStaff_curr_spec = isStaff_curr_spec ? true : (staffs.Contains("79") ? true : false);
                    isStaff_extnl_eval = isStaff_extnl_eval ? true : (staffs.Contains("81") ? true : false);
                    isStaff_mag_coor = isStaff_mag_coor ? true : (staffs.Contains("85") ? true : false);
                    isStaff_paren_spec = isStaff_paren_spec ? true : (staffs.Contains("87") ? true : false);
                    isStaff_pd = isStaff_pd ? true : (staffs.Contains("89") ? true : false);
                    isStaff_recr_mrkt = isStaff_recr_mrkt ? true : (staffs.Contains("90") ? true : false);
                    isStaff_tech_supp = isStaff_tech_supp ? true : (staffs.Contains("82") ? true : false);
                    isStaff_theme_spec = isStaff_theme_spec ? true : (staffs.Contains("94") ? true : false);
                    isStaff_other = isStaff_other ? true : (staffs.Contains("95") ? true : false);
                    txtStaff_other += string.IsNullOrEmpty(sch.SpecialistOther) ? "" : sch.SpecialistOther+";";
                  
                }

            }
            DataRow dr = rptTbl.NewRow();
            dr["GranteeID"] = itm.ID;
            dr["GranteeName"] = itm.GranteeName;

            dr["Staff_admn_supp"] = isStaff_admn_supp ? "X" : "";
            dr["Staff_commty_out"] = isStaff_commty_out ? "X" : "";
            dr["Staff_constult"] = isStaff_constult ? "X" : "";
            dr["Staff_curr_spec"] = isStaff_curr_spec ? "X" : "";
            dr["Staff_extnl_eval"] = isStaff_extnl_eval ? "X" : "";
            dr["Staff_mag_coor"] = isStaff_mag_coor ? "X" : "";
            dr["Staff_paren_spec"] = isStaff_paren_spec ? "X" : "";
            dr["Staff_pd"] = isStaff_pd ? "X" : "";
            dr["Staff_recr_mrkt"] = isStaff_recr_mrkt ? "X" : "";
            dr["Staff_tech_supp"] = isStaff_tech_supp ? "X" : "";
            dr["Staff_theme_spec"] = isStaff_theme_spec ? "X" : "";
            dr["Staff_other"] = isStaff_other ? "X" : "";
            dr["Staff_other(textbox)"] = txtStaff_other.TrimEnd(';');

            rptTbl.Rows.Add(dr);

        }

        return rptTbl;
    }

    private DataTable MarketingData()
    {
        string txtY1_MR_schl_img_other = "",
                txtY1_MR_pln_other = "",
                txtY1_MR_outrch_trgt = "",
                txtY1_MR_outrch_other = "",
                txtY2_MR_schl_img_other = "",
                txtY2_MR_pln_other = "",
                txtY2_MR_outrch_trgt = "",
                txtY2_MR_outrch_other = "",
                txtY3_MR_schl_img_other = "",
                txtY3_MR_pln_other = "",
                txtY3_MR_outrch_trgt = "",
                txtY3_MR_outrch_other = "";

        bool isY1_MR_assess_img = false,
            isY1_MR_dev_img = false,
            isY1_MR_web_redev = false,
            isY1_MR_brand_bldg = false,
            isY1_MR_schl_img_other = false,

            isY1_MR_mrktpln = false,
            isY1_MR_recrtpln = false,
            isY1_MR_retn_pln = false,
            isY1_MR_pln_other = false,

            isY1_MR_outrch_brdcst = false,
            isY1_MR_outrch_prnt = false,
            isY1_MR_outrch_trgt = false,

            isY1_MR_outrch_web = false,
            isY1_MR_outrch_calls = false,
            isY1_MR_outrch_tours = false,
            isY1_MR_outrch_expos = false,
            isY1_MR_outrch_commun = false,
            isY1_MR_outrch_visitprsnt = false,
            isY1_MR_outrch_flwup = false,
            isY1_MR_outrch_assess_flwup = false,
            isY1_MR_outrch_other = false,

            isY2_MR_assess_img = false,
            isY2_MR_dev_img = false,
            isY2_MR_web_redev = false,
            isY2_MR_brand_bldg = false,
            isY2_MR_schl_img_other = false,

            isY2_MR_mrktpln = false,
            isY2_MR_recrtpln = false,
            isY2_MR_retn_pln = false,
            isY2_MR_pln_other = false,

            isY2_MR_outrch_brdcst = false,
            isY2_MR_outrch_prnt = false,
            isY2_MR_outrch_trgt = false,

            isY2_MR_outrch_web = false,
            isY2_MR_outrch_call = false,
            isY2_MR_outrch_tours = false,
            isY2_MR_outrch_expos = false,
            isY2_MR_outrch_commun = false,
            isY2_MR_outrch_visitprsnt = false,
            isY2_MR_outrch_flwup = false,
            isY2_MR_outrch_assess_flwup = false,
            isY2_MR_outrch_other = false,

            isY3_MR_assess_img = false,
            isY3_MR_dev_img = false,
            isY3_MR_web_redev = false,
            isY3_MR_brand_bldg = false,
            isY3_MR_schl_img_other = false,

            isY3_MR_mrktpln = false,
            isY3_MR_recrtpln = false,
            isY3_MR_retn_pln = false,
            isY3_MR_pln_other = false,

            isY3_MR_outrch_brdcst = false,
            isY3_MR_outrch_prnt = false,
            isY3_MR_outrch_trgt = false,

            isY3_MR_outrch_web = false,
            isY3_MR_outrch_call = false,
            isY3_MR_outrch_tours = false,
            isY3_MR_outrch_expos = false,
            isY3_MR_outrch_commun = false,
            isY3_MR_outrch_visitprsnt = false,
            isY3_MR_outrch_flwup = false,
            isY3_MR_outrch_assess_flwup = false,
            isY3_MR_outrch_other = false;
        DataTable rptTbl = new DataTable();

        //1 - 10
        rptTbl.Columns.Add("GranteeID", typeof(string));
        rptTbl.Columns.Add("GranteeName", typeof(string));

        rptTbl.Columns.Add("Y1_M/R_assess_img", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_dev_img", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_web_redev", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_brand_bldg", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_schl_img_other", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_schl_img_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_mrktpln", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_recrtpln", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_retn_pln", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_pln_other", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_pln_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_brdcst", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_prnt", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_trgt", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_trgt(textbox)", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_web", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_calls", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_tours", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_expos", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_commun", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_visit/prsnt", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_flwup", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_assess_flwup", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_other", typeof(string));
        rptTbl.Columns.Add("Y1_M/R_outrch_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_assess_img", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_dev_img", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_web_redev", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_brand_bldg", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_schl_img_other", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_schl_img_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_mrktpln", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_recrtpln", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_retn_pln", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_pln_other", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_pln_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_brdcst", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_prnt", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_trgt", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_trgt(textbox)", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_web", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_call", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_tours", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_expos", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_commun", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_visit/prsnt", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_flwup", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_assess_flwup", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_other", typeof(string));
        rptTbl.Columns.Add("Y2_M/R_outrch_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_assess_img", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_dev_img", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_web_redev", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_brand_bldg", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_schl_img_other", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_schl_img_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_mrktpln", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_recrtpln", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_retn_pln", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_pln_other", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_pln_other(textbox)", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_brdcst", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_prnt", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_trgt", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_trgt(textbox)", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_web", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_call", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_tours", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_expos", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_commun", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_visit/prsnt", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_flwup", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_assess_flwup", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_other", typeof(string));
        rptTbl.Columns.Add("Y3_M/R_outrch_other(textbox)", typeof(string));


        var MagnetGrantees = MagnetGrantee.Find(x => x.CohortType == Convert.ToInt32(Session["CohortType"]) && x.isMSAPGrantee == true);

        foreach (MagnetGrantee itm in MagnetGrantees)
        {
            txtY1_MR_schl_img_other = "";
            txtY1_MR_pln_other = "";
            txtY1_MR_outrch_trgt = "";
            txtY1_MR_outrch_other = "";
            txtY2_MR_schl_img_other = "";
            txtY2_MR_pln_other = "";
            txtY2_MR_outrch_trgt = "";
            txtY2_MR_outrch_other = "";
            txtY3_MR_schl_img_other = "";
            txtY3_MR_pln_other = "";
            txtY3_MR_outrch_trgt = "";
            txtY3_MR_outrch_other = "";

            isY1_MR_assess_img = false;
            isY1_MR_dev_img = false;
            isY1_MR_web_redev = false;
            isY1_MR_brand_bldg = false;
            isY1_MR_schl_img_other = false;

            isY1_MR_mrktpln = false;
            isY1_MR_recrtpln = false;
            isY1_MR_retn_pln = false;
            isY1_MR_pln_other = false;

            isY1_MR_outrch_brdcst = false;
            isY1_MR_outrch_prnt = false;
            isY1_MR_outrch_trgt = false;

            isY1_MR_outrch_web = false;
            isY1_MR_outrch_calls = false;
            isY1_MR_outrch_tours = false;
            isY1_MR_outrch_expos = false;
            isY1_MR_outrch_commun = false;
            isY1_MR_outrch_visitprsnt = false;
            isY1_MR_outrch_flwup = false;
            isY1_MR_outrch_assess_flwup = false;
            isY1_MR_outrch_other = false;

            isY2_MR_assess_img = false;
            isY2_MR_dev_img = false;
            isY2_MR_web_redev = false;
            isY2_MR_brand_bldg = false;
            isY2_MR_schl_img_other = false;

            isY2_MR_mrktpln = false;
            isY2_MR_recrtpln = false;
            isY2_MR_retn_pln = false;
            isY2_MR_pln_other = false;

            isY2_MR_outrch_brdcst = false;
            isY2_MR_outrch_prnt = false;
            isY2_MR_outrch_trgt = false;

            isY2_MR_outrch_web = false;
            isY2_MR_outrch_call = false;
            isY2_MR_outrch_tours = false;
            isY2_MR_outrch_expos = false;
            isY2_MR_outrch_commun = false;
            isY2_MR_outrch_visitprsnt = false;
            isY2_MR_outrch_flwup = false;
            isY2_MR_outrch_assess_flwup = false;
            isY2_MR_outrch_other = false;

            isY3_MR_assess_img = false;
            isY3_MR_dev_img = false;
            isY3_MR_web_redev = false;
            isY3_MR_brand_bldg = false;
            isY3_MR_schl_img_other = false;

            isY3_MR_mrktpln = false;
            isY3_MR_recrtpln = false;
            isY3_MR_retn_pln = false;
            isY3_MR_pln_other = false;

            isY3_MR_outrch_brdcst = false;
            isY3_MR_outrch_prnt = false;
            isY3_MR_outrch_trgt = false;

            isY3_MR_outrch_web = false;
            isY3_MR_outrch_call = false;
            isY3_MR_outrch_tours = false;
            isY3_MR_outrch_expos = false;
            isY3_MR_outrch_commun = false;
            isY3_MR_outrch_visitprsnt = false;
            isY3_MR_outrch_flwup = false;
            isY3_MR_outrch_assess_flwup = false;
            isY3_MR_outrch_other = false;
            foreach (MagnetSchool sch in MagnetSchool.Find(x => x.GranteeID == itm.ID && x.ReportYear == Convert.ToInt32(Session["ReportYear"])))
            {
                string marketings = sch.Marketingrecruitment;

                if (!string.IsNullOrEmpty(marketings))
                {
                    string txtOther118, txtOther127, txtOther133, txtOther1311, txtOther218, txtOther227, txtOther233, 
                           txtOther2311, txtOther318, txtOther327, txtOther333, txtOther3311;

                    isY1_MR_assess_img = isY1_MR_assess_img ? true : (getMktstatus(marketings, "cbx112")=="X" ? true : false);
                    isY1_MR_dev_img = isY1_MR_dev_img ? true : (getMktstatus(marketings, "cbx113")=="X" ? true : false);
                    isY1_MR_web_redev = isY1_MR_web_redev ? true : (getMktstatus(marketings, "cbx114")=="X" ? true : false);
                    isY1_MR_brand_bldg = isY1_MR_brand_bldg ? true : (getMktstatus(marketings, "cbx115")=="X" ? true : false);
                    isY1_MR_schl_img_other = isY1_MR_schl_img_other ? true : (getMktstatus(marketings, "cbx118")=="X" ? true : false);
                    txtOther118 = getMktstatus(marketings, "txtOther118");
                    txtY1_MR_schl_img_other += txtOther118 == "" ? "" : txtOther118 + "; ";
                    isY1_MR_mrktpln = isY1_MR_mrktpln ? true : (getMktstatus(marketings, "cbx121")=="X" ? true : false);
                    isY1_MR_recrtpln = isY1_MR_recrtpln ? true : (getMktstatus(marketings, "cbx122")=="X" ? true : false);
                    isY1_MR_retn_pln = isY1_MR_retn_pln ? true : (getMktstatus(marketings, "cbx123")=="X" ? true : false);
                    isY1_MR_pln_other = isY1_MR_pln_other ? true : (getMktstatus(marketings, "cbx127")=="X" ? true : false);
                    txtOther127 = getMktstatus(marketings, "txtOther127");
                    txtY1_MR_pln_other += txtOther127 == "" ? "" : txtOther127 + "; ";
                    isY1_MR_outrch_brdcst = isY1_MR_outrch_brdcst ? true : (getMktstatus(marketings, "cbx131")=="X" ? true : false);
                    isY1_MR_outrch_prnt = isY1_MR_outrch_prnt ? true : (getMktstatus(marketings, "cbx132")=="X" ? true : false);
                    isY1_MR_outrch_trgt = isY1_MR_outrch_trgt ? true : (getMktstatus(marketings, "cbx133")=="X" ? true : false);
                    txtOther133 = getMktstatus(marketings, "txtOther133");
                    txtY1_MR_outrch_trgt += txtOther133 == "" ? "" : txtOther133 + "; ";
                    isY1_MR_outrch_web = isY1_MR_outrch_web ? true : (getMktstatus(marketings, "cbx134")=="X" ? true : false);
                    isY1_MR_outrch_calls = isY1_MR_outrch_calls ? true : (getMktstatus(marketings, "cbx135")=="X" ? true : false);
                    isY1_MR_outrch_tours = isY1_MR_outrch_tours ? true : (getMktstatus(marketings, "cbx136")=="X" ? true : false);
                    isY1_MR_outrch_expos = isY1_MR_outrch_expos ? true : (getMktstatus(marketings, "cbx137")=="X" ? true : false);
                    isY1_MR_outrch_commun = isY1_MR_outrch_commun? true : (getMktstatus(marketings, "cbx139")=="X" ? true : false);
                    isY1_MR_outrch_visitprsnt = isY1_MR_outrch_visitprsnt ? true : (getMktstatus(marketings, "cbx1310")=="X" ? true : false);
                    isY1_MR_outrch_flwup = isY1_MR_outrch_flwup ? true : (getMktstatus(marketings, "cbx1312")=="X" ? true : false);
                    isY1_MR_outrch_assess_flwup = isY1_MR_outrch_assess_flwup ? true : (getMktstatus(marketings, "cbx1313")=="X" ? true : false);
                    isY1_MR_outrch_other = isY1_MR_outrch_other ? true : (getMktstatus(marketings, "cbx1311")=="X" ? true : false);
                    txtOther1311 = getMktstatus(marketings, "txtOther1311");
                    txtY1_MR_outrch_other += txtOther1311 == "" ? "" : txtOther1311 + ";";

                    //////Grant Year 2/////
                    isY2_MR_assess_img = isY2_MR_assess_img ? true : (getMktstatus(marketings, "cbx212")=="X" ? true : false);
                    isY2_MR_dev_img = isY2_MR_dev_img ? true : (getMktstatus(marketings, "cbx213")=="X" ? true : false);
                    isY2_MR_web_redev = isY2_MR_web_redev ? true : (getMktstatus(marketings, "cbx214")=="X" ? true : false);
                    isY2_MR_brand_bldg = isY2_MR_brand_bldg ? true : (getMktstatus(marketings, "cbx215")=="X" ? true : false);
                    isY2_MR_schl_img_other = isY2_MR_schl_img_other ? true : (getMktstatus(marketings, "cbx218")=="X" ? true : false);
                    txtOther218 = getMktstatus(marketings, "txtOther218");
                    txtY2_MR_schl_img_other += txtOther218 == "" ? "" : txtOther218 + ";";

                    isY2_MR_mrktpln = isY2_MR_mrktpln ? true : (getMktstatus(marketings, "cbx221")=="X" ? true : false);
                    isY2_MR_recrtpln = isY2_MR_recrtpln ? true : (getMktstatus(marketings, "cbx222")=="X" ? true : false);
                    isY2_MR_retn_pln = isY2_MR_retn_pln ? true : (getMktstatus(marketings, "cbx223")=="X" ? true : false);
                    isY2_MR_pln_other = isY2_MR_pln_other ? true : (getMktstatus(marketings, "cbx227")=="X" ? true : false);
                    txtOther227 = getMktstatus(marketings, "txtOther227");
                    txtY2_MR_pln_other += txtOther227=="" ? "" : txtOther227 + ";";

                    isY2_MR_outrch_brdcst = isY2_MR_outrch_brdcst ? true : (getMktstatus(marketings, "cbx231")=="X" ? true : false);
                    isY2_MR_outrch_prnt = isY2_MR_outrch_prnt ? true : (getMktstatus(marketings, "cbx232")=="X" ? true : false);
                    isY2_MR_outrch_trgt = isY2_MR_outrch_trgt ? true : (getMktstatus(marketings, "cbx233")=="X" ? true : false);
                    txtOther233 = getMktstatus(marketings, "txtOther233");
                    txtY2_MR_outrch_trgt += txtOther233 == "" ? "" : txtOther233 + ";";
                    isY2_MR_outrch_web = isY2_MR_outrch_web ? true : (getMktstatus(marketings, "cbx234")=="X" ? true : false);
                    isY2_MR_outrch_call = isY2_MR_outrch_call ? true : (getMktstatus(marketings, "cbx235")=="X" ? true : false);
                    isY2_MR_outrch_tours = isY2_MR_outrch_tours ? true : (getMktstatus(marketings, "cbx236")=="X" ? true : false);
                    isY2_MR_outrch_expos = isY2_MR_outrch_expos ? true : (getMktstatus(marketings, "cbx237")=="X" ? true : false);
                    isY2_MR_outrch_commun = isY2_MR_outrch_commun ? true : (getMktstatus(marketings, "cbx239")=="X" ? true : false);
                    isY2_MR_outrch_visitprsnt = isY2_MR_outrch_visitprsnt ? true : (getMktstatus(marketings, "cbx2310")=="X" ? true : false);
                    isY2_MR_outrch_flwup = isY2_MR_outrch_flwup ? true : (getMktstatus(marketings, "cbx2312")=="X" ? true : false);
                    isY2_MR_outrch_assess_flwup = isY2_MR_outrch_assess_flwup ? true : (getMktstatus(marketings, "cbx2313")=="X" ? true : false);
                    isY2_MR_outrch_other = isY2_MR_outrch_other ? true : (getMktstatus(marketings, "cbx2311")=="X" ? true : false);
                    txtOther2311 = getMktstatus(marketings, "txtOther2311");
                    txtY2_MR_outrch_other += txtOther2311 == "" ? "" : txtOther2311 + ";";

                    ///////year 3/////////
                    isY3_MR_assess_img = isY3_MR_assess_img ? true : (getMktstatus(marketings, "cbx312")=="X" ? true : false);
                    isY3_MR_dev_img = isY3_MR_dev_img ? true : (getMktstatus(marketings, "cbx313")=="X" ? true : false);
                    isY3_MR_web_redev = isY3_MR_web_redev ? true : (getMktstatus(marketings, "cbx314")=="X" ? true : false);
                    isY3_MR_brand_bldg = isY3_MR_brand_bldg ? true : (getMktstatus(marketings, "cbx315")=="X" ? true : false);
                    isY3_MR_schl_img_other = isY3_MR_schl_img_other ? true : (getMktstatus(marketings, "cbx318")=="X" ? true : false);
                    txtOther318 = getMktstatus(marketings, "txtOther318");
                    txtY3_MR_schl_img_other += txtOther318 == "" ? "" : txtOther318 + ";";
                    isY3_MR_mrktpln = isY3_MR_mrktpln ? true : (getMktstatus(marketings, "cbx321")=="X" ? true : false);
                    isY3_MR_recrtpln = isY3_MR_recrtpln ? true : (getMktstatus(marketings, "cbx322")=="X" ? true : false);
                    isY3_MR_retn_pln = isY3_MR_retn_pln ? true : (getMktstatus(marketings, "cbx323")=="X" ? true : false);
                    isY3_MR_pln_other = isY3_MR_pln_other? true : (getMktstatus(marketings, "cbx327")=="X" ? true : false);
                    txtOther327 = getMktstatus(marketings, "txtOther327");
                    txtY3_MR_pln_other += txtOther327 == "" ? "" : txtOther327 + ";";
                    isY3_MR_outrch_brdcst = isY3_MR_outrch_brdcst ? true : (getMktstatus(marketings, "cbx331")=="X" ? true : false);
                    isY3_MR_outrch_prnt = isY3_MR_outrch_prnt ? true : (getMktstatus(marketings, "cbx332")=="X" ? true : false);
                    isY3_MR_outrch_trgt = isY3_MR_outrch_trgt ? true : (getMktstatus(marketings, "cbx333")=="X" ? true : false);
                    txtOther333 = getMktstatus(marketings, "txtOther333");
                    txtY3_MR_outrch_trgt += txtOther333 == "" ? "" : txtOther333 + ";";
                    isY3_MR_outrch_web = isY3_MR_outrch_web ? true : (getMktstatus(marketings, "cbx334")=="X" ? true : false);
                    isY3_MR_outrch_call = isY3_MR_outrch_call ? true : (getMktstatus(marketings, "cbx335")=="X" ? true : false);
                    isY3_MR_outrch_tours = isY3_MR_outrch_tours ? true : (getMktstatus(marketings, "cbx336")=="X" ? true : false);
                    isY3_MR_outrch_expos = isY3_MR_outrch_expos ? true : (getMktstatus(marketings, "cbx337")=="X" ? true : false);
                    isY3_MR_outrch_commun = isY3_MR_outrch_commun ? true : (getMktstatus(marketings, "cbx339")=="X" ? true : false);
                    isY3_MR_outrch_visitprsnt = isY3_MR_outrch_visitprsnt ? true : (getMktstatus(marketings, "cbx3310")=="X" ? true : false);
                    isY3_MR_outrch_flwup = isY3_MR_outrch_flwup ? true : (getMktstatus(marketings, "cbx3312")=="X" ? true : false);
                    isY3_MR_outrch_assess_flwup = isY3_MR_outrch_assess_flwup ? true : (getMktstatus(marketings, "cbx3313")=="X" ? true : false);
                    isY3_MR_outrch_other = isY3_MR_outrch_other ? true : (getMktstatus(marketings, "cbx3311")=="X" ? true : false);
                    txtOther3311 = getMktstatus(marketings, "txtOther3311");
                    txtY3_MR_outrch_other += txtOther3311 == "" ? "" : txtOther3311 + ";";
                }

            }

            DataRow dr = rptTbl.NewRow();
            dr["GranteeID"] = itm.ID;
            dr["GranteeName"] = itm.GranteeName;

            dr["Y1_M/R_assess_img"] = isY1_MR_assess_img ? "X" : "";
            dr["Y1_M/R_dev_img"] = isY1_MR_dev_img ? "X" : "";
            dr["Y1_M/R_web_redev"] = isY1_MR_web_redev ? "X" : "";
            dr["Y1_M/R_brand_bldg"] = isY1_MR_brand_bldg ? "X" : "";
            dr["Y1_M/R_schl_img_other"] = isY1_MR_schl_img_other ? "X" : "";
            dr["Y1_M/R_schl_img_other(textbox)"] = txtY1_MR_schl_img_other.Trim().TrimEnd(';');
            dr["Y1_M/R_mrktpln"] = isY1_MR_mrktpln ? "X" : "";
            dr["Y1_M/R_recrtpln"] = isY1_MR_recrtpln ? "X" : "";
            dr["Y1_M/R_retn_pln"] = isY1_MR_retn_pln ? "X" : "";
            dr["Y1_M/R_pln_other"] = isY1_MR_pln_other ? "X" : "";
            dr["Y1_M/R_pln_other(textbox)"] = txtY1_MR_pln_other.Trim().TrimEnd(';');
            dr["Y1_M/R_outrch_brdcst"] = isY1_MR_outrch_brdcst ? "X" : "";
            dr["Y1_M/R_outrch_prnt"] = isY1_MR_outrch_prnt ? "X" : "";
            dr["Y1_M/R_outrch_trgt"] = isY1_MR_outrch_trgt ? "X" : "";
            dr["Y1_M/R_outrch_trgt(textbox)"] = txtY1_MR_outrch_trgt.Trim().TrimEnd(';');
            dr["Y1_M/R_outrch_web"] = isY1_MR_outrch_web ? "X" : "";
            dr["Y1_M/R_outrch_calls"] = isY1_MR_outrch_calls ? "X" : "";
            dr["Y1_M/R_outrch_tours"] = isY1_MR_outrch_tours ? "X" : "";
            dr["Y1_M/R_outrch_expos"] = isY1_MR_outrch_expos ? "X" : "";
            dr["Y1_M/R_outrch_commun"] = isY1_MR_outrch_commun ? "X" : "";
            dr["Y1_M/R_outrch_visit/prsnt"] = isY1_MR_outrch_visitprsnt ? "X" : "";
            dr["Y1_M/R_outrch_flwup"] = isY1_MR_outrch_flwup ? "X" : "";
            dr["Y1_M/R_outrch_assess_flwup"] = isY1_MR_outrch_assess_flwup ? "X" : "";
            dr["Y1_M/R_outrch_other"] = isY1_MR_outrch_other ? "X" : "";
            dr["Y1_M/R_outrch_other(textbox)"] = txtY1_MR_outrch_other.Trim().TrimEnd(';');
            ///////Grant Year 2 ////////////////////////
            dr["Y2_M/R_assess_img"] = isY2_MR_assess_img ? "X" : "";
            dr["Y2_M/R_dev_img"] = isY2_MR_dev_img ? "X" : "";
            dr["Y2_M/R_web_redev"] = isY2_MR_web_redev ? "X" : "";
            dr["Y2_M/R_brand_bldg"] = isY2_MR_brand_bldg ? "X" : "";
            dr["Y2_M/R_schl_img_other"] = isY2_MR_schl_img_other ? "X" : "";
            dr["Y2_M/R_schl_img_other(textbox)"] = txtY2_MR_schl_img_other.Trim().TrimEnd(';');
            dr["Y2_M/R_mrktpln"] = isY2_MR_mrktpln ? "X" : "";
            dr["Y2_M/R_recrtpln"] = isY2_MR_recrtpln ? "X" : "";
            dr["Y2_M/R_retn_pln"] = isY2_MR_retn_pln ? "X" : "";
            dr["Y2_M/R_pln_other"] = isY2_MR_pln_other ? "X" : "";
            dr["Y2_M/R_pln_other(textbox)"] = txtY2_MR_pln_other.Trim().TrimEnd(';');
            dr["Y2_M/R_outrch_brdcst"] = isY2_MR_outrch_brdcst ? "X" : "";
            dr["Y2_M/R_outrch_prnt"] = isY2_MR_outrch_prnt ? "X" : "";
            dr["Y2_M/R_outrch_trgt"] = isY2_MR_outrch_trgt ? "X" : "";
            dr["Y2_M/R_outrch_trgt(textbox)"] = txtY2_MR_outrch_trgt.Trim().TrimEnd(';');
            dr["Y2_M/R_outrch_web"] = isY2_MR_outrch_web ? "X" : "";
            dr["Y2_M/R_outrch_call"] = isY2_MR_outrch_call ? "X" : "";
            dr["Y2_M/R_outrch_tours"] = isY2_MR_outrch_tours ? "X" : "";
            dr["Y2_M/R_outrch_expos"] = isY2_MR_outrch_expos ? "X" : "";
            dr["Y2_M/R_outrch_commun"] = isY2_MR_outrch_commun ? "X" : "";
            dr["Y2_M/R_outrch_visit/prsnt"] = isY2_MR_outrch_visitprsnt ? "X" : "";
            dr["Y2_M/R_outrch_flwup"] = isY2_MR_outrch_flwup ? "X" : "";
            dr["Y2_M/R_outrch_assess_flwup"] = isY2_MR_outrch_assess_flwup ? "X" : "";
            dr["Y2_M/R_outrch_other"] = isY2_MR_outrch_other ? "X" : "";
            dr["Y2_M/R_outrch_other(textbox)"] = txtY2_MR_outrch_other.Trim().TrimEnd(';');

            ///////year 3///////////////////////////////////////
            dr["Y3_M/R_assess_img"] = isY3_MR_assess_img ? "X" : "";
            dr["Y3_M/R_dev_img"] = isY3_MR_dev_img ? "X" : "";
            dr["Y3_M/R_web_redev"] = isY3_MR_web_redev ? "X" : "";
            dr["Y3_M/R_brand_bldg"] = isY3_MR_brand_bldg ? "X" : "";
            dr["Y3_M/R_schl_img_other"] = isY3_MR_schl_img_other ? "X" : "";
            dr["Y3_M/R_schl_img_other(textbox)"] = txtY3_MR_schl_img_other.Trim().TrimEnd(';');
            dr["Y3_M/R_mrktpln"] = isY3_MR_mrktpln ? "X" : "";
            dr["Y3_M/R_recrtpln"] = isY3_MR_recrtpln ? "X" : "";
            dr["Y3_M/R_retn_pln"] = isY3_MR_retn_pln ? "X" : "";
            dr["Y3_M/R_pln_other"] = isY3_MR_pln_other ? "X" : "";
            dr["Y3_M/R_pln_other(textbox)"] = txtY3_MR_pln_other.Trim().TrimEnd(';');
            dr["Y3_M/R_outrch_brdcst"] = isY3_MR_outrch_brdcst ? "X" : "";
            dr["Y3_M/R_outrch_prnt"] = isY3_MR_outrch_prnt ? "X" : "";
            dr["Y3_M/R_outrch_trgt"] = isY3_MR_outrch_trgt ? "X" : "";
            dr["Y3_M/R_outrch_trgt(textbox)"] = txtY3_MR_outrch_trgt.Trim().TrimEnd(';');
            dr["Y3_M/R_outrch_web"] = isY3_MR_outrch_web ? "X" : "";
            dr["Y3_M/R_outrch_call"] = isY3_MR_outrch_call ? "X" : "";
            dr["Y3_M/R_outrch_tours"] = isY3_MR_outrch_tours ? "X" : "";
            dr["Y3_M/R_outrch_expos"] = isY3_MR_outrch_expos ? "X" : "";
            dr["Y3_M/R_outrch_commun"] = isY3_MR_outrch_commun ? "X" : "";
            dr["Y3_M/R_outrch_visit/prsnt"] = isY3_MR_outrch_visitprsnt ? "X" : "";
            dr["Y3_M/R_outrch_flwup"] = isY3_MR_outrch_flwup ? "X" : "";
            dr["Y3_M/R_outrch_assess_flwup"] = isY3_MR_outrch_assess_flwup ? "X" : "";
            dr["Y3_M/R_outrch_other"] = isY3_MR_outrch_other ? "X" : "";
            dr["Y3_M/R_outrch_other(textbox)"] = txtY3_MR_outrch_other.Trim().TrimEnd(';');

            rptTbl.Rows.Add(dr);
        }

        return rptTbl;
    }

    protected void ddlGrantYears_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlGrantPeriod.SelectedValue == "")
            tblRepots.Visible = false;
        else
            tblRepots.Visible = true;
    }

    protected void ExportExcel(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        DataTable dtbl = new DataTable();

        string fileName = "";

        switch (btn.CommandName)
        {
            case "GContext":
                dtbl = GContextData();
                fileName = "Grantee_Context";
                break;
            case "SchContext":
                dtbl = SchContextData();
                fileName = "School_Context";
                break;
            case "SchAct":
                dtbl = SchActData();
                fileName = "School_Activities";
                break;
            case "ProfDev":
                dtbl = ProfDevData();
                fileName = "Prof_dev";
                break;
            case "Staff":
                dtbl = StaffData();
                fileName = "MSAPStaff";
                break;
            case "Marketing":
                dtbl = MarketingData();
                fileName = "Marketing&Recruitment";
                break;
        }


        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_Export.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvReports.AllowPaging = false;
            gvReports.DataSource = dtbl;
            gvReports.DataBind();

            gvReports.Visible = true;

            if (gvReports.HeaderRow != null)
            {
                gvReports.HeaderRow.BackColor = System.Drawing.Color.White;

                foreach (System.Web.UI.WebControls.TableCell cell in gvReports.HeaderRow.Cells)
                {
                    cell.BackColor = gvReports.HeaderStyle.BackColor;
                }
            }

            foreach (GridViewRow row in gvReports.Rows)
            {
                row.BackColor = System.Drawing.Color.White;
                foreach (System.Web.UI.WebControls.TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvReports.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvReports.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvReports.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();
            //Response.Flush();
            //HttpContext.Current.ApplicationInstance.CompleteRequest();
            gvReports.Visible = false;
        }
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        /* Verifies that the control is rendered */
    }
}