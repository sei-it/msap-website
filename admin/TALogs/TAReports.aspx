﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="TAReports.aspx.cs" Inherits="admin_TALogs_TAReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
Reports
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        html
        {
            background-color:white;
        }
        .menuTabs
        {
            position:relative;
            top:1px;
            left:10px;
        }
        .tab
        {
            border:Solid 1px black;
            border-bottom:none;
            padding:0px 10px;
            background-color:#eeeeee;
        }
        .selectedTab
        {
            border:Solid 1px black;
            border-bottom:Solid 1px white;
            padding:0px 10px;
            background-color:white;
        }
        .tabBody
        {
            border:Solid 1px black;
            padding:20px;
            background-color:white;
        }
    </style>
    <title>Reports</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

     <h1>
       Communication Management 
    </h1>
    <asp:DropDownList ID="DropDownList1" runat="server"  AutoPostBack="true" 
        CssClass="DropDownListCssClass" 
        onselectedindexchanged="DropDownList1_SelectedIndexChanged">
         <asp:ListItem Value="1">Communication log Report</asp:ListItem>
        <asp:ListItem Value="2">Grantee and School Information Report</asp:ListItem>
    </asp:DropDownList>
    <br /><br /><br />
    <asp:Panel ID="pnlGranteeSchRpt" runat="server" Visible="false">
    <br /><br />
    <div class="titles">Reports</div>
  <div class="tab_area">
    	<ul class="tabs">
        <li class="tab_active">Page 8</li>
         <li><asp:LinkButton ID="LinkButton8" PostBackUrl="TCallIndex.aspx" CausesValidation="false" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton10" PostBackUrl="MSAPCenterMassCommunication.aspx" CausesValidation="false" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton17" PostBackUrl="MSAPCenterActParticipation.aspx" CausesValidation="false" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton18" PostBackUrl="Communications.aspx" CausesValidation="false" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton19" PostBackUrl="ContextualFactors.aspx" CausesValidation="false" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton20" PostBackUrl="ContactInfo.aspx" CausesValidation="false" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton21" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/>
    <table>
    <tr>
    <td><strong></strong></td>
    <td> <asp:RadioButtonList ID="rbtnCohort" runat="server" RepeatColumns="2" 
        RepeatDirection="Horizontal" AutoPostBack="true" Visible="false"
            onselectedindexchanged="rbtnCohort_SelectedIndexChanged">
        <asp:ListItem Value="1" Selected="True">2010 Cohort</asp:ListItem>
        <asp:ListItem Value="2">2013 Cohort</asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    </table>
   
    <asp:DropDownList ID="ddlGrantees" runat="server" DataSourceID="dsGrantee" Visible="false"
        AppendDataBoundItems="true" AutoPostBack="true" CssClass="DropDownListCssClass"
        DataTextField="GranteeName" DataValueField="ID" 
        onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
        <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>

    <asp:DropDownList ID="ddlGrantPeriod" runat="server" AutoPostBack="true" Visible="false"
                AppendDataBoundItems="true" DataSourceID="dsReportPeriod" 
         DataTextField="Des" DataValueField="cohortyear" CssClass="DropDownListCssClass" 
                onselectedindexchanged="ddlGrantYears_SelectedIndexChanged">
         <asp:ListItem Value="">Select one</asp:ListItem>
        </asp:DropDownList>
    <table id="tblRepots" runat="server" visible="true">
    <tr>
    <td>
    <asp:Button ID="Button1" runat="server" CssClass="surveyBtn2" 
     Text="Grantee context report" CommandName="GContext" onclick="ExportExcel" />
    </td>
     <td>
     <asp:Button ID="Button2" runat="server" CssClass="surveyBtn2" 
     Text="School context report" CommandName="SchContext" onclick="ExportExcel" />
    </td>
    
    </tr>
    <tr>
    <td>
    <asp:Button ID="Button3" runat="server" CssClass="surveyBtn2" 
     Text="School activities report" CommandName="SchAct" onclick="ExportExcel" />
    </td>
     <td>
     <asp:Button ID="Button4" runat="server" CssClass="surveyBtn2" 
     Text="Professional development" CommandName="ProfDev" onclick="ExportExcel" />
    </td>
    
    </tr>
    <tr>
    <td>
    <asp:Button ID="Button5" runat="server" CssClass="surveyBtn2" 
     Text="Staffing report" CommandName="Staff" onclick="ExportExcel" />
    </td>
     <td>
     <asp:Button ID="Button6" runat="server" CssClass="surveyBtn2" 
     Text="Marketing & recruitment" CommandName="Marketing" onclick="ExportExcel" />
    </td>
    
    </tr>
    </table>
    <asp:GridView ID="gvReports" runat="server" BackColor="White" Visible="false"
        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>
     </asp:Panel>

    <asp:Panel ID ="pnlComlogRpt" runat="server">
    <br /><br />
    <div class="titles">Reports</div>
  <div class="tab_area">
    	<ul class="tabs">
        <li class="tab_active">Page 8</li>
         <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" CausesValidation="false" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton1" PostBackUrl="MSAPCenterMassCommunication.aspx" CausesValidation="false" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="MSAPCenterActParticipation.aspx" CausesValidation="false" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton9" PostBackUrl="Communications.aspx" CausesValidation="false" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContextualFactors.aspx" CausesValidation="false" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="ContactInfo.aspx" CausesValidation="false" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton3" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/>
   
    <asp:Panel ID="Panel1" runat="server">
    <table width="100%">
    <tr>
    
    <td width="5%">
        <asp:DropDownList ID="ddlYears" runat="server" AppendDataBoundItems="true" 
        DataSourceID="dsYears" DataTextField="Yr" DataValueField="Yr" AutoPostBack="true" 
                onselectedindexchanged="ddlYears_SelectedIndexChanged">
        <asp:ListItem Value="">Select a year</asp:ListItem>
        </asp:DropDownList>
    </td>
    <td>
        <asp:DropDownList ID="ddlMonth" runat="server" AppendDataBoundItems="true" 
        DataSourceID="dsMonth" DataTextField="MonthName" DataValueField="Mnth" AutoPostBack="true"
            onselectedindexchanged="ddlMonth_SelectedIndexChanged">
        <asp:ListItem Value="">Select a month</asp:ListItem>
        </asp:DropDownList>

    </td >
    <td  align="right"><asp:Button ID="btnReport" runat="server" CssClass="surveyBtn2" 
            Visible="false" Text="Export report" onclick="btnReport_Click" /></td>
    </tr>
    </table>
    </asp:Panel>

    <div >
     <h2>Summary of requests</h2>
    </div>

    <asp:Panel ID="pnlMain" runat="server" Visible="false">
    <asp:Menu
        id="menuTabs"
        CssClass="menuTabs"
        StaticMenuItemStyle-CssClass="tab"
        StaticSelectedStyle-CssClass="selectedTab"
        Orientation="Horizontal"
        OnMenuItemClick="menuTabs_MenuItemClick"
        Runat="server">
        <Items>
        <asp:MenuItem
            Text="<b>General Communication</b>"
            Value="0" 
            Selected="true" />
        <asp:MenuItem
            Text="<b>Technical Assistance</b>" 
            Value="1"/>
        <asp:MenuItem
            Text="<b>Data Collection</b>"
            Value="2" />
        <asp:MenuItem
            Text="<b>Reporting</b>"
            Value="3" />
        </Items>
    </asp:Menu>    
    <div class="tabBody">
    <asp:MultiView
        id="multiTabs"
        ActiveViewIndex="0"
        Runat="server">
        <asp:View ID="view1" runat="server">
        <table>
        <tr>
        <td>
        Regular update call:
        </td>
        <td>
         <asp:Literal ID="ltl0" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Grantee interview:
        </td>
        <td>
         <asp:Literal ID="ltl1" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Key personnel change:
        </td>
        <td>
         <asp:Literal ID="ltl2" runat="server"></asp:Literal>
        </td>
        </tr>
        </table>
        </asp:View>
        <asp:View ID="view2" runat="server">
        <table>
        <tr>
        <td>
        Consultation with expert:
        </td>
        <td>
         <asp:Literal ID="ltl3" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        MSAP Center event – accessing resources:
        </td>
        <td>
         <asp:Literal ID="ltl4" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        MSAP Center event – content: 
        </td>
        <td>
         <asp:Literal ID="ltl5" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        MSAP Center event – logging in:
        </td>
        <td>
         <asp:Literal ID="ltl6" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        MSAP Center event – registration:
        </td>
        <td>
         <asp:Literal ID="ltl7" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        MSAP Center event – other:
        </td>
        <td>
         <asp:Literal ID="ltl8" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Project directors meeting – logistics:
        </td>
        <td>
         <asp:Literal ID="ltl9" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Project directors meeting – registration:
        </td>
        <td>
         <asp:Literal ID="ltl10" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Project directors meeting – sessions:
        </td>
        <td>
         <asp:Literal ID="ltl11" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        TA request – community partnerships:
        </td>
        <td>
         <asp:Literal ID="ltl12" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        TA request – curriculum and theme integration:
        </td>
        <td>
         <asp:Literal ID="ltl13" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        TA request –family engagement:
        </td>
        <td>
         <asp:Literal ID="ltl14" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        TA request – marketing and recruitment:
        </td>
        <td>
         <asp:Literal ID="ltl15" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        TA request – sustainability:
        </td>
        <td>
         <asp:Literal ID="ltl16" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        TA request – other:
        </td>
        <td>
         <asp:Literal ID="ltl17" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Website assistance – logging in:
        </td>
        <td>
         <asp:Literal ID="ltl18" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Website assistance – new staff access:
        </td>
        <td>
         <asp:Literal ID="ltl19" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Website assistance – password request:
        </td>
        <td>
         <asp:Literal ID="ltl20" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Website assistance – other:
        </td>
        <td>
         <asp:Literal ID="ltl21" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Other TA request:
        </td>
        <td>
         <asp:Literal ID="ltl22" runat="server"></asp:Literal>
        </td>
        </tr>
        </table>
        </asp:View>
        <asp:View ID="view3" runat="server">
        <table>
        <tr>
        <td>
        Needs assessment:
        </td>
        <td>
         <asp:Literal ID="ltl23" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Consultant assessment:
        </td>
        <td>
         <asp:Literal ID="ltl24" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        TA evaluation:
        </td>
        <td>
         <asp:Literal ID="ltl25" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Other data collection activity:
        </td>
        <td>
         <asp:Literal ID="ltl26" runat="server"></asp:Literal>
        </td>
        </tr>
        </table>
        </asp:View>
        <asp:View ID="view4" runat="server">
        <table>
        <tr>
        <td>
        APR reporting:
        </td>
        <td>
         <asp:Literal ID="ltl27" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        MAPS questions:
        </td>
        <td>
         <asp:Literal ID="ltl28" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Ad hoc reporting:
        </td>
        <td>
         <asp:Literal ID="ltl29" runat="server"></asp:Literal>
        </td>
        </tr>
        <tr>
        <td>
        Other reporting  request:
        </td>
        <td>
         <asp:Literal ID="ltl30" runat="server"></asp:Literal>
        </td>
        </tr>
        </table>
        </asp:View>
    </asp:MultiView>    
    </div>
    
    </asp:Panel>
    </asp:Panel>
    
    <div style="text-align: right; margin-top: 20px;">
           <asp:LinkButton ID="LinkButton15" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton2" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton13" PostBackUrl="ContextualFactors.aspx" ToolTip="School Information" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton14" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton7" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
          &nbsp;&nbsp;&nbsp;&nbsp;Page 8
           
          </div>
    <asp:SqlDataSource ID="dsGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 

        SelectCommand="SELECT * FROM [MagnetGrantees] WHERE ([CohortType] = @CohortType AND isActive=1)" >
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohort" Name="CohortType" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsYears" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [Yr] FROM [vw_Year]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="dsMonth" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [Yr], [Mnth], [MonthName] FROM [vw_Year_Month] WHERE ([Yr] = @Yr)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlYears" Name="Yr" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsReportPeriod" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        
        SelectCommand=" select CAST(CohortType as VARCHAR(50)) + '|' + CAST(reportYear as VARCHAR(50)) as cohortyear, des  from [dbo].[MagnetReportPeriods] where isActive = 1 ">
    </asp:SqlDataSource>
</asp:Content>

