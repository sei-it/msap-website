﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Synergy.Magnet;
using MKB.TimePicker;
using System.Text.RegularExpressions;

public partial class admin_TALogs_Communications : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IsPageRefresh"] = false;
        if (!IsPostBack)
        {

            ViewState["ViewStateId"] = System.Guid.NewGuid().ToString();

            Session["SessionId"] = ViewState["ViewStateId"].ToString();

        }

        else
        {

            if (ViewState["ViewStateId"].ToString() != Session["SessionId"].ToString())
            {

                Session["IsPageRefresh"] = true;

            }

            Session["SessionId"] = System.Guid.NewGuid().ToString();

            ViewState["ViewStateId"] = Session["SessionId"].ToString();

        }     

        if (Session["TALogCohortType"] != null && string.IsNullOrEmpty(Session["TALogCohortType"].ToString()))
            Session["TALogCohortType"] = null;
        if (Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
            Session["TALogGranteeID"] = null;
        if (!IsPostBack)
        {
            Session["showTAComm"] = null;
            Session["showTAFup"] = null;
            rbtnCohortType.SelectedValue = Session["TALogCohortType"] == null ? "2" : Session["TALogCohortType"].ToString();
            hfGranteeID.Value = ddlGrantees.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();
            if (string.IsNullOrEmpty(hfGranteeID.Value))
                hfGranteeID.Value = ddlGrantees.SelectedValue;
        }
       

        if (Session["TALogGranteeID"] == null && string.IsNullOrEmpty(ddlGrantees.SelectedValue))
        {
            NewComm.Visible = false;
            btnShowComm.Visible = false;
            pnlFollowupUpload.Visible = false;
        }
        else
        {
            NewComm.Visible = true;
            //btnShowComm.Visible = true;
            bool isMSAPstaff = HttpContext.Current.User.IsInRole("ED")?false:true;
            int granteeID = string.IsNullOrEmpty(hfGranteeID.Value) ? 0 : Convert.ToInt32(hfGranteeID.Value);
            var data = TALogCommunication.Find(x => x.PK_GenInfoID == granteeID && x.isMSAPSCentertaffMember == isMSAPstaff && x.isActive);
            
            if(!string.IsNullOrEmpty(ddlComm.SelectedValue) && data.Count>0)
                pnlFollowupUpload.Visible = true;
            else
                pnlFollowupUpload.Visible = false;
        }

        if(HttpContext.Current.User.IsInRole("ED"))
            hfisCenterstaff.Value = "false";
        else
            hfisCenterstaff.Value = "true";

        //ddlComm_SelectedIndexChanged(this, e);
    }

    private void setStaffmemberfields()
    {
        //Label followupstaffmembertitle = fvCommlog.FindControl("lblCntrstaffmember") as Label;
        
        //Label staffmembertitle = fvCommlog.FindControl("lblstaffmember") as Label;
        //TextBox staffmember = fvCommlog.FindControl("txtstafffname") as TextBox;
        vwUserProfileDataContext db = new vwUserProfileDataContext();
        if (HttpContext.Current.User.IsInRole("ED"))
        {
            lblCntrstaffmember.Text = "ED staff member who will follow up:";
            lblstaffmember.Text = "ED staff member:";
        }
        else
        {
            lblCntrstaffmember.Text = "MSAP Center staff member who will follow up:";
            lblstaffmember.Text = "MSAP Center staff member:";
        }

        string userName = HttpContext.Current.User.Identity.Name;
        var usrdata = from r in db.vwUserProfiles
                      where r.Name == userName
                      select r;
        string FullName = "";
        foreach (var itm in usrdata)
        {
            FullName = (itm.FirstName + " " + itm.LastName).Trim();
            break;
        }
        if (string.IsNullOrEmpty(FullName))
            FullName = userName;
        txtstafffname.Text = FullName;

        txtGranteeName.Text = ddlGrantees.SelectedItem.Text;
    }

    protected void OnNewComm(object sender, EventArgs e)
    {
        pnlCommunications.Visible = false;
        pnlFollowupUpload.Visible = false;
        ddlComm.Visible = false;
        ltlItemNo.Visible = false;
        pnlEditComm.Visible = true;
        hfCommID.Value = "";
        ltlItemNumber.Text = "";
        ddlCommStatus.SelectedValue="Edit Record";

        setCommFormFields(true);
    }

    protected void OnEditComm(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;

        hfCommID.Value = this.gdvComm.DataKeys[row.RowIndex].Values["ID"].ToString();
        pnlCommunications.Visible = false;
        pnlEditFollowup.Visible = false;
        pnlFollowupUpload.Visible = false;
        pnlEditComm.Visible = true;

        ddlComm.Visible = false;
        ltlItemNo.Visible = false;
        PsnCommWithTextBox.Text = "";

        //ddlRole.DataBind();
        setCommFormFields(false);
      
    }

    private void setCommFormFields(bool isResetFields)
    {
        ddlComm.Items.Clear();
        ddlComm.Items.Add(new ListItem("Select one", ""));
        ddlComm.DataBind();

        setDDLItems();
        if (isResetFields) //clear out
        {
            PK_GenInfoIDLabel.Text="";
            ddlcontacttype.SelectedIndex = 0;
           
            DateTextBox.Text ="";
            TimeSelector1.SetTime(0,0,TimeSelector.AmPmSpec.AM);
            TimeSelector2.SetTime(0,0,TimeSelector.AmPmSpec.AM);
            PsnCommWithTextBox.Text="";
            EmailTextBox.Text = "";
            PhoneTextBox.Text = "";
            ddlRole.SelectedIndex = -1;
           
            ddlGeneralComm.SelectedIndex = 0;
            ddlTAComm.SelectedIndex = 0;
           
            ddlDataCollectionComm.SelectedIndex = 0;
            
            ddlReportComm.SelectedIndex = 0;
            
            OtherCommTextBox.Text ="";
            CommDescriptionTextBox.Text = "";
            ActionTakenTextBox.Text ="";
            RequiredDesTextBox.Text ="";
            ddlfollowupmember.SelectedIndex = 0;
            ddlRequestStatus.SelectedIndex = 0;
            
            txtFulfillDate.Text ="";
            hfddlRoleID.Value = "";

            TAComm_otherTextBox.Visible = false;
            TAComm_otherTextBox.Text = "";
            hfTACommTACommCntEvtother.Value = "";
            hfTACommReqOther.Value = "";
            hfTACommWebAssOther.Value = "";
            hfTACommOtherReq.Value = "";
            

            DataCollectionComm_otherTextBox.Visible = false;
            DataCollectionComm_otherTextBox.Text = "";

            ReportComm_otherTextBox.Visible = false;
            ReportComm_otherTextBox.Text = "";

            RequestStatus_otherTextBox.Visible = false;
            RequestStatus_otherTextBox.Text = "";

            ContactType_otherTextBox.Visible = false;
            ContactType_otherTextBox.Text = "";

            Role_otherTextBox.Visible = false;
            Role_otherTextBox.Text = "";
        }
        else
        {
            ltlItemNumber.Text = hfCommID.Value;
             TALogCommunication  data = TALogCommunication.SingleOrDefault(x => x.id == Convert.ToInt32(hfCommID.Value));
             PK_GenInfoIDLabel.Text= data.PK_GenInfoID.ToString();
            ddlcontacttype.SelectedValue = data.ContactType;
            if (data.ContactType != null && data.ContactType.ToLower() == "other")
                ContactType_otherTextBox.Visible = true;
            else
                ContactType_otherTextBox.Visible = false;

            ContactType_otherTextBox.Text=data.ContactType_other;
            DateTextBox.Text = data.Date==null?"" : Convert.ToDateTime(data.Date).ToShortDateString();
            DateTime dt1 = data.commTime==null? DateTime.Now:Convert.ToDateTime(data.commTime);
            TimeSelector1.SetTime(dt1.Hour,dt1.Minute,dt1.ToString("tt")=="AM"?TimeSelector.AmPmSpec.AM:TimeSelector.AmPmSpec.PM);
            DateTime dt2 = data.completetime==null?DateTime.Now:Convert.ToDateTime(data.completetime);
            TimeSelector2.SetTime(dt2.Hour,dt2.Minute,dt2.ToString("tt")=="AM"?TimeSelector.AmPmSpec.AM:TimeSelector.AmPmSpec.PM);
            PsnCommWithTextBox.Text=  data.PsnCommWith;
            EmailTextBox.Text =data.Email;
            PhoneTextBox.Text = data.Phone;

            hfddlRoleID.Value = data.role_id.ToString();
            ddlRole.DataBind();
            ddlGeneralComm.SelectedValue = data.CommType;

            Role_otherTextBox.Text = data.Role_other;
            
            ddlTAComm.SelectedValue = data.TAComm;
            if (ContainOther(ddlTAComm.SelectedValue.ToLower()))
            {
                TAComm_otherTextBox.Visible = true;
                //TAComm_otherTextBox.Text = 
            }
            else
            {
                TAComm_otherTextBox.Visible = false;
                TAComm_otherTextBox.Text = "";
            }

            switch (ddlTAComm.SelectedValue.ToLower().Trim())
            {
                case "msap center event—other":
                    TAComm_otherTextBox.Text = data.TACommCntEvt_other;
                    break;
                case "ta request—other":
                    TAComm_otherTextBox.Text = data.TAComm_other;
                    break;
                case "website assistance—other":
                    TAComm_otherTextBox.Text = data.TACommWebAss_other;
                    break;
                case "other request":
                    TAComm_otherTextBox.Text = data.TAComm_otherReq;
                    break;
            }

            hfTACommReqOther.Value = data.TAComm_other;
            hfTACommTACommCntEvtother.Value = data.TACommCntEvt_other;
            hfTACommWebAssOther.Value = data.TACommWebAss_other;
            hfTACommOtherReq.Value = data.TAComm_otherReq;


            ddlDataCollectionComm.SelectedValue = data.DataCollectionComm;
            if (ddlDataCollectionComm.SelectedValue.ToLower() == "other data collection activity")
                DataCollectionComm_otherTextBox.Visible= true;
            else
                DataCollectionComm_otherTextBox.Visible = false;

            DataCollectionComm_otherTextBox.Text= data.DataCollectionComm_other;

            ddlReportComm.SelectedValue = data.ReportComm;
            if(ddlReportComm.SelectedValue.ToLower()=="other")
                ReportComm_otherTextBox.Visible=true;
            else
                ReportComm_otherTextBox.Visible = false;

            ReportComm_otherTextBox.Text = data.ReportComm_other;
            OtherCommTextBox.Text = data.OtherComm;
            CommDescriptionTextBox.Text = data.CommDescription;
            ActionTakenTextBox.Text =data.ActionTaken;
            RequiredDesTextBox.Text = data.RequiredDes;
            ddlfollowupmember.SelectedValue = data.FollowupStaff;
            ddlRequestStatus.SelectedValue = data.RequestStatus;
            if (ddlRequestStatus.SelectedValue.ToLower()=="other")
                RequestStatus_otherTextBox.Visible = true;
            else
                RequestStatus_otherTextBox.Visible = false;

            RequestStatus_otherTextBox.Text= data.RequestStatus_other;
            txtFulfillDate.Text = data.FulfillDate==null?"" : Convert.ToDateTime(data.FulfillDate).ToString("d"); ;
            ddlCommStatus.SelectedValue = data.Status;
        }

        setStaffmemberfields(); 
    }

    protected void OnDeleteComm(object sender, EventArgs e)
    {
       
        LinkButton btnDelete = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnDelete.NamingContainer;
        int rcdid = Convert.ToInt32(this.gdvComm.DataKeys[row.RowIndex].Values["ID"]);
        var follopUps = TALogFollowUp.Find(f => f.PK_CommID == Convert.ToInt32(rcdid));
        if (follopUps.Count > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('You cannot delete the record with follow up associated.');</script>", false);
            return;
        }


        TALogCommunication item = TALogCommunication.SingleOrDefault(x => x.id == rcdid);
        item.isActive = false;
        item.Delete();

        gdvComm.DataBind();
        dsCommgv.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc) as item#, * FROM [TALogCommunications] WHERE  (PK_GenInfoID = " + ddlGrantees.SelectedValue + "  ) ";
        Session["showTAComm"] = "0";

        ddlComm.Items.Clear();
        ddlComm.Items.Add(new ListItem("Select one", ""));
        ddlComm.DataBind();



        if (HttpContext.Current.User.IsInRole("ED"))
        {
            TAlog EDSender = new TAlog();
            EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "TALogCommunications", DateTime.Now.ToShortDateString(), "Deleted");

        }
    }
    protected void AddNewFollowup(object sender, EventArgs e)
    {
        txtDate.Text = "";
        TimeSelector3.SetTime(0, 0, TimeSelector.AmPmSpec.AM);
        txtStaffmember.Text = "";
        txtFollowupPerson.Text = "";
        edtAbsContent.Content = "";
        edtFollowupResult.Content = "";
        hfEditFollowupRcdID.Value = "";
        ddlfupStatus.SelectedValue = "Edit Record";

        openFollowupPan();

    }

    private void openFollowupPan()
    {
       pnlCommunications.Visible= false;
       pnlFollowupUpload.Visible = false;
       pnlEditComm.Visible = false;
       pnlEditFollowup.Visible = true;
    }

    protected void OnEditFollowup(object sender, EventArgs e)
    {
        LinkButton btnEdite = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdite.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvFollowup.DataKeys[row.RowIndex].Values["ID"]);
        hfEditFollowupRcdID.Value = rcdid.ToString();

        TALogFollowUp followupRcd = TALogFollowUp.SingleOrDefault(x => x.id == rcdid);

        txtDate.Text = followupRcd.Date != null ? Convert.ToDateTime(followupRcd.Date).ToShortDateString() : "";
        DateTime dt3 = followupRcd.Time== null ? DateTime.Now : Convert.ToDateTime(followupRcd.Time);
        TimeSelector3.SetTime(dt3.Hour, dt3.Minute, dt3.ToString("tt") == "AM" ? TimeSelector.AmPmSpec.AM : TimeSelector.AmPmSpec.PM);
        txtStaffmember.Text = followupRcd.StaffMember;
        txtFollowupPerson.Text = followupRcd.FollowupPerson;
        edtAbsContent.Content = followupRcd.Notes;
        edtFollowupResult.Content = followupRcd.Result;
        ddlfupStatus.SelectedValue = followupRcd.Status;

        openFollowupPan();

    }
    protected void OnDeleteFollowup(object sender, EventArgs e)
    {
        LinkButton btnDelete = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnDelete.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvFollowup.DataKeys[row.RowIndex].Values["ID"]);
        TALogFollowUp rcd = TALogFollowUp.SingleOrDefault(x => x.id == rcdid);
        rcd.isActive = false;
        rcd.Delete();

        gvFollowup.DataBind();
        gdvComm.DataBind();

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            TAlog EDSender = new TAlog();
            EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "TALogFollowUp", DateTime.Now.ToShortDateString(), "Deleted");

        }

        CloseEditPan(this, e);
        pnlFollowupUpload.Visible = true;
    }



    protected void OnSaveFollowup(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(Session["IsPageRefresh"]))
        {
            Session["showTAFup"] = "1";
            showAllfup(sender, e);
            CloseEditPan(this, e);
           // btnShowAllfup.Visible = true;
            return;
        }

        TALogFollowUp followupRcd;

        if (string.IsNullOrEmpty(hfEditFollowupRcdID.Value))
        {
            followupRcd = new TALogFollowUp();
            followupRcd.CreatedBy = HttpContext.Current.User.Identity.Name;
            followupRcd.CreatedOn = DateTime.Now;
            followupRcd.ModifiedBy = HttpContext.Current.User.Identity.Name;
            followupRcd.ModifiedOn = DateTime.Now;
        }
        else
        {
            followupRcd = TALogFollowUp.SingleOrDefault(x => x.id == Convert.ToInt32(hfEditFollowupRcdID.Value));
            followupRcd.ModifiedBy = HttpContext.Current.User.Identity.Name;
            followupRcd.ModifiedOn = DateTime.Now;
        }

        followupRcd.Date = string.IsNullOrEmpty(txtDate.Text) ? (DateTime?)null : Convert.ToDateTime(txtDate.Text);
        followupRcd.Time = DateTime.Parse(TimeSelector3.Hour + ":" + TimeSelector3.Minute + ":00 " + TimeSelector3.AmPm);
        followupRcd.StaffMember = txtStaffmember.Text;
        followupRcd.FollowupPerson = txtFollowupPerson.Text;
        followupRcd.Notes = edtAbsContent.Content;
        followupRcd.Result = edtFollowupResult.Content;
        followupRcd.PK_CommID = string.IsNullOrEmpty(ddlComm.SelectedValue) ? (int?)null : Convert.ToInt32(ddlComm.SelectedValue);
        followupRcd.isActive = true;
        followupRcd.Status = ddlfupStatus.SelectedValue;

        followupRcd.Save();

        Session["showTAFup"] = "1";
        showAllfup(sender, e);
        gdvComm.DataBind();

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            string action = string.IsNullOrEmpty(hfEditFollowupRcdID.Value) ? "Added" : "Updated";
            TAlog EDSender = new TAlog();
            EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "TALogFollowUp", DateTime.Now.ToShortDateString(), action);

        }
        string commid = string.IsNullOrEmpty(ddlComm.SelectedValue) ? "" : ddlComm.SelectedValue;
        CloseEditPan(this, e);
       // btnShowAllfup.Visible = true;
       
    }

    protected void CloseEditPan(object sender, EventArgs e)
    {
        pnlCommunications.Visible = true;
        pnlFollowupUpload.Visible = true;
        pnlEditFollowup.Visible = false;
    }
    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
        {
            Session["TALogGranteeID"] = ddlGrantees.SelectedValue;
            NewComm.Visible = true;
           // btnShowComm.Visible = true;
        }
        else
        {
            Session["TALogGranteeID"] = null;
            NewComm.Visible = false;
            btnShowComm.Visible = false;
        }

        int isCenterstaff = hfisCenterstaff.Value == "true" ? 1 : 0;
        btnShowComm.Text = "Show All";
        dsCommgv.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc) as item#, * FROM [TALogCommunications] WHERE  (PK_GenInfoID = " + ddlGrantees.SelectedValue + " ) ";
        Session["showTAComm"] = "0";

        ddlComm.Items.Clear();
        ddlComm.Items.Add(new ListItem("Select one", ""));
        ddlComm.DataBind();
        dsCommgv.Select(DataSourceSelectArguments.Empty);
        dsCommgv.DataBind();
        pnlFollowupUpload.Visible = false;
        ddlComm.SelectedIndex = 0;
    }
    protected void rbtnCohortType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGrantees.Items.Clear();
        ddlGrantees.Items.Add(new ListItem("Select one", ""));
        Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
        Session["TALogGranteeID"] = null;
    }
    protected void SaveUpload(object sender, EventArgs e)
    {
        if (FileUploadComm.HasFile)
        {
            string strFileroot = Server.MapPath("") + "/uploads/" + ddlGrantees.SelectedItem.Text.Trim() + "/";
            string strFileName = FileUploadComm.FileName.Replace("#", "").Trim();
            string strFileURL = "/Admin/TALogs/uploads/" + ddlGrantees.SelectedItem.Text.Trim() + "/" + strFileName;
            if (!Directory.Exists(strFileroot))
            {
                Directory.CreateDirectory(strFileroot);
            }

            FileUploadComm.SaveAs(strFileroot + strFileName);
            int GranteeID = string.IsNullOrEmpty(hfGranteeID.Value) ? 0 : Convert.ToInt32(hfGranteeID.Value);
            int commid = string.IsNullOrEmpty(ddlComm.SelectedValue) ? 0 : Convert.ToInt32(ddlComm.SelectedValue);
            //check uploaded file's name duplicated.
            IList<TALogUploadFile> data = TALogUploadFile.Find(x => x.PK_CommID == commid && x.FileName.ToLower() == strFileName.ToLower());

            if (data.Count == 0)
            {
                TALogUploadFile newItem = new TALogUploadFile();
                newItem.PK_CommID = commid;
                newItem.Date = DateTime.Now;
                newItem.FileName = strFileName;
                newItem.Description = txtDesc.Text;
                newItem.Hlink = "<a href =' " + strFileURL + " '>" + strFileName + "</a>";
                newItem.FileURL = strFileURL;
                newItem.CreatedBy = HttpContext.Current.User.Identity.Name;
                newItem.ModifedBy = HttpContext.Current.User.Identity.Name;
                newItem.CreatedOn = DateTime.Now;
                newItem.ModifyOn = DateTime.Now;
                newItem.isActive = true;
                newItem.PK_CommID = Convert.ToInt32(Convert.ToInt32(ddlComm.SelectedValue));

                newItem.Save();
                txtDesc.Text = "";
                gvFileUpload.DataBind();

                if (HttpContext.Current.User.IsInRole("ED"))
                {
                    TAlog EDSender = new TAlog();
                    EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "TALogUpLoadFiles", DateTime.Now.ToShortDateString(), "Added");
                }
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('This file name already exists. Please rename the file.');</script>", false);


        }
        else
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('A uploading file is required!');</script>", false);

        pnlFollowupUpload.Visible = true;
    }
    protected void OnDeleteUploadFile(object sender, EventArgs e)
    {
        LinkButton btnDelete = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnDelete.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvFileUpload.DataKeys[row.RowIndex].Values["ID"]);
        TALogUploadFile rcd = TALogUploadFile.SingleOrDefault(x => x.id == rcdid);

        string fileURL = rcd.FileURL;
        if(File.Exists(Server.MapPath("../../") + fileURL))
            File.Delete(Server.MapPath("../../") + fileURL);
        rcd.Delete();

        Session["showTAFup"] = "1";
        Session["showTAComm"] = "1";
        showAllfup(sender, e);
        //showAllComm(sender, e);

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            TAlog EDSender = new TAlog();
            EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "TALogUpLoadFiles", DateTime.Now.ToShortDateString(), "Deleted");
        }
        gvFileUpload.DataBind();
        pnlFollowupUpload.Visible = true;

    }

    protected void dsComm_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["@CreatedOn"].Value = DateTime.Now;
        e.Command.Parameters["@ModifiedOn"].Value = DateTime.Now;
        e.Command.Parameters["@PK_GenInfoID"].Value = hfGranteeID.Value;
       
    }
    protected void dsComm_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        pnlFollowupUpload.Visible = true;
        pnlCommunications.Visible = true;
        ddlComm.Visible = true;
        pnlEditComm.Visible = false;
        gdvComm.DataBind();
    }

    protected void CloseFormviewPan(object sender, EventArgs e)
    {
        ltlItemNo.Visible = true;
        pnlFollowupUpload.Visible = true;
        pnlEditComm.Visible = false;
        ddlComm.Visible = true;
        pnlCommunications.Visible = true;
        btnShowAllfup.Text = "Show All";
        gdvComm.DataBind();

    }
    protected void dsComm_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["@CreatedBy"].Value = HttpContext.Current.Profile.UserName;
        e.Command.Parameters["@ModifiedBy"].Value = HttpContext.Current.Profile.UserName;

        e.Command.Parameters["@CreatedOn"].Value = DateTime.Now;
        e.Command.Parameters["@ModifiedOn"].Value = DateTime.Now;
        e.Command.Parameters["@PK_GenInfoID"].Value = hfGranteeID.Value;
    }
    protected void dsComm_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        pnlFollowupUpload.Visible = true;
        pnlCommunications.Visible = true;
        ddlComm.Visible = true;
        pnlEditComm.Visible = false;
        gdvComm.DataBind();
    }
    protected void ddlComm_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        if (string.IsNullOrEmpty(ddlComm.SelectedValue))
        {
            pnlFollowupUpload.Visible = false;
            btnNewFollowup.Visible = false;
            btnSaveUpload.Visible = false;
            FileUploadComm.Visible = false;
            tblDes.Visible = false;

            Session["showTAFup"] = null;
            btnShowAllfup.Text = "Show All";
        }
        else
        {
            pnlFollowupUpload.Visible = true;
            btnNewFollowup.Visible = true;
            btnSaveUpload.Visible = true;
            FileUploadComm.Visible = true;
            tblDes.Visible = true;
       
            bool isMSAPstaff = HttpContext.Current.User.IsInRole("ED") ? false : true;
            int granteeID = Convert.ToInt32(ddlGrantees.SelectedValue);
            var data = TALogCommunication.Find(x => x.PK_GenInfoID == granteeID && x.isMSAPSCentertaffMember == isMSAPstaff && x.isActive);

            if (data.Count > 0)
                pnlFollowupUpload.Visible = true;
            else
                pnlFollowupUpload.Visible = false;

            int commid =Convert.ToInt32(ddlComm.SelectedValue);
            var fup = TALogFollowUp.Find(f => f.PK_CommID ==commid );
            if(fup.Count>0)
             //   btnShowAllfup.Visible = true
                ;
            else
                btnShowAllfup.Visible = false;
        }
    }
    protected void dsComm_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        ddlComm.Items.Clear();
        ddlComm.Items.Add(new ListItem("Select one", ""));
    }


    protected void setDDLItems()
    {
        //DropDownList ddlfollowupstaffmember = sender as DropDownList;
        ddlfollowupmember.Items.Clear();
        ddlfollowupmember.Items.Add(new ListItem("Select One", ""));

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            var data = TALogEDStaff.Find(x => x.name != null);
            foreach (TALogEDStaff itm in data)
            {
                ddlfollowupmember.Items.Add(new ListItem(itm.name, itm.name));
            }
        }
        else
        {
            var data = TALogMSAPStaff.Find(x => x.name != null);
            foreach (TALogMSAPStaff itm in data)
            {
                ddlfollowupmember.Items.Add(new ListItem(itm.name, itm.name));
            }
        }
        //if(string.IsNullOrEmpty(hfCommID.Value))
        //    ddlfollowupmember.SelectedValue = Eval("FollowupStaff").ToString();
    }

    protected void btnSaveComm_Click(object sender, EventArgs e)
    {
        TALogCommunication data = new TALogCommunication();
        if (!string.IsNullOrEmpty(hfCommID.Value))
        {
            data = TALogCommunication.SingleOrDefault(x => x.id == Convert.ToInt32(hfCommID.Value));
            data.ModifiedBy = HttpContext.Current.User.Identity.Name;
            data.ModifiedOn = DateTime.Now;
        }
        else
        {
            data.CreatedBy = HttpContext.Current.User.Identity.Name;
            data.CreatedOn = DateTime.Now;
            data.ModifiedBy = HttpContext.Current.User.Identity.Name;
            data.ModifiedOn = DateTime.Now;
        }

        //data.MassComm_id = 
        data.PK_GenInfoID = Convert.ToInt32(ddlGrantees.SelectedValue);
        data.ContactType = ddlcontacttype.SelectedValue;
        data.ContactType_other = ContactType_otherTextBox.Text;
        if (!string.IsNullOrEmpty(DateTextBox.Text))
            data.Date = Convert.ToDateTime(DateTextBox.Text);
        else
            data.Date = (DateTime?)null;

        data.commTime = DateTime.Parse(TimeSelector1.Hour + ":" + TimeSelector1.Minute + ":00 " + TimeSelector1.AmPm);
        data.completetime = DateTime.Parse(TimeSelector2.Hour + ":" + TimeSelector2.Minute + ":00 " + TimeSelector2.AmPm);
        data.MSAPStaffMember = txtstafffname.Text;
        data.isMSAPSCentertaffMember = HttpContext.Current.User.IsInRole("ED") ? false : true;
        data.PsnCommWith = PsnCommWithTextBox.Text;
        data.Email = EmailTextBox.Text;
        data.Phone = PhoneTextBox.Text;
        if (!string.IsNullOrEmpty(ddlRole.SelectedValue))
            data.role_id = Convert.ToInt32(ddlRole.SelectedValue);
        else
            data.role_id = (int?)null;

        data.Role_other = Role_otherTextBox.Text;
        data.CommType = ddlGeneralComm.SelectedValue;
        data.TAComm = ddlTAComm.SelectedValue;
        //data.TAComm_other = TAComm_otherTextBox.Text;
        switch (ddlTAComm.SelectedValue.ToLower().Trim())
        {
            case "msap center event—other":
                data.TACommCntEvt_other = TAComm_otherTextBox.Text;
                break;
            case "ta request—other":
                data.TAComm_other = TAComm_otherTextBox.Text;
                break;
            case "website assistance—other":
                data.TACommWebAss_other = TAComm_otherTextBox.Text;
                break;
            case "other request":
                data.TAComm_otherReq = TAComm_otherTextBox.Text;
                break;
        }


        data.DataCollectionComm = ddlDataCollectionComm.SelectedValue;
        data.DataCollectionComm_other = DataCollectionComm_otherTextBox.Text;
        data.ReportComm = ddlReportComm.SelectedValue;
        data.ReportComm_other = ReportComm_otherTextBox.Text;

        data.OtherComm = OtherCommTextBox.Text;
        data.CommDescription = CommDescriptionTextBox.Text;
        data.ActionTaken = ActionTakenTextBox.Text;
        data.RequiredDes = RequiredDesTextBox.Text;
        data.FollowupStaff = ddlfollowupmember.SelectedValue;

        data.RequestStatus = ddlRequestStatus.SelectedValue;
        data.RequestStatus_other = RequestStatus_otherTextBox.Text;
        if (!string.IsNullOrEmpty(txtFulfillDate.Text))
            data.FulfillDate = Convert.ToDateTime(txtFulfillDate.Text);
        else
            data.FulfillDate = (DateTime?)null;

        //data.FeedbackRequest = txtfbRequest.Text;
        //data.FeedbackReceived = txtfbReceived.Text;
        //data.Result = txtResault.Text;
        data.Status = ddlCommStatus.SelectedValue;

        data.isActive = true;

        data.Save();

        Session["showTAComm"] = "1";
        showAllComm(sender, e);

        CloseFormviewPan(sender, e);

        ddlComm.Items.Clear();
        ddlComm.Items.Add(new ListItem("Select one", ""));
        ddlComm.DataBind();
        //dsCommgv.Select(DataSourceSelectArguments.Empty);
        //dsCommgv.DataBind();
        pnlFollowupUpload.Visible = false;

        Response.Redirect("Communications.aspx");
    }
    protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRole.SelectedValue == "7")
            Role_otherTextBox.Visible = true;
        else
        {
            Role_otherTextBox.Visible = false;
            Role_otherTextBox.Text = "";
        }
    }
    protected void ddlTAComm_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ContainOther(ddlTAComm.SelectedValue.ToLower()))
        {
            TAComm_otherTextBox.Visible = true;
            switch (ddlTAComm.SelectedValue.ToLower().Trim())
            {
                case "msap center event—other":
                    TAComm_otherTextBox.Text = hfTACommTACommCntEvtother.Value;
                    break;
                case "ta request—other":
                    TAComm_otherTextBox.Text = hfTACommReqOther.Value;
                    break;
                case "website assistance—other":
                    TAComm_otherTextBox.Text = hfTACommWebAssOther.Value;
                    break;
                case "other request":
                    TAComm_otherTextBox.Text = hfTACommOtherReq.Value;
                    break;
            }

        }
        else
        {
            TAComm_otherTextBox.Visible = false;
            TAComm_otherTextBox.Text = "";
        }
    }

    private bool ContainOther(string value)
    {
       string sPattern = "other";
       return Regex.IsMatch(value, sPattern, RegexOptions.IgnoreCase);
    }

    protected void ddlcontacttype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcontacttype.SelectedValue == "Other")
            ContactType_otherTextBox.Visible = true;
        else
        {
            ContactType_otherTextBox.Visible = false;
            ContactType_otherTextBox.Text = "";
        }

    }

    protected void ddlDataCollectionComm_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDataCollectionComm.SelectedValue == "Other data collection activity")
            DataCollectionComm_otherTextBox.Visible = true;
        else
        {
            DataCollectionComm_otherTextBox.Visible = false;
            DataCollectionComm_otherTextBox.Text = "";
        }
    }
    protected void ddlReportComm_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReportComm.SelectedValue == "Other")
            ReportComm_otherTextBox.Visible = true;
        else
        {
            ReportComm_otherTextBox.Visible = false;
            ReportComm_otherTextBox.Text = "";
        }
    }
    protected void ddlRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRequestStatus.SelectedValue == "Other")
            RequestStatus_otherTextBox.Visible = true;
        else
        {
            RequestStatus_otherTextBox.Visible = false;
            RequestStatus_otherTextBox.Text = "";
        }
    }
    protected void ddlRole_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfddlRoleID.Value))
        {
            ddlRole.SelectedValue = hfddlRoleID.Value;
            if (hfddlRoleID.Value == "7")
            {
                Role_otherTextBox.Visible = true; 
            }else
                Role_otherTextBox.Visible = false;
        }
        else
            Role_otherTextBox.Visible = false;
        
    }
    protected void showAllComm(object sender, EventArgs e)
    {
        int isCenterstaff = hfisCenterstaff.Value == "true" ? 1 : 0;
        //if (Session["showTAComm"] == null || Session["showTAComm"].ToString() == "0")
        //{
            btnShowComm.Text = "Show Edit Records";
            dsCommgv.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc) as item#, * FROM [TALogCommunications] WHERE  (PK_GenInfoID = " + ddlGrantees.SelectedValue + "  AND isMSAPSCentertaffMember= " + isCenterstaff + " ) ";
            Session["showTAComm"] = "1";
        //}
        //else
        //{
        //    btnShowComm.Text = "Show All";
        //    dsCommgv.SelectCommand = "SELECT ROW_NUMBER() OVER (order by date desc) as item#, * FROM [TALogCommunications] WHERE  (PK_GenInfoID = " + ddlGrantees.SelectedValue + " AND Status = 'Edit Record' AND isMSAPSCentertaffMember= " + isCenterstaff + " ) ";
        //    Session["showTAComm"] = "0";
        //}
        ddlComm.Items.Clear();
        ddlComm.Items.Add(new ListItem("Select one",""));
        ddlComm.DataBind();
        dsCommgv.Select(DataSourceSelectArguments.Empty);
        dsCommgv.DataBind();
        gdvComm.DataBind();
    }
    protected void showAllfup(object sender, EventArgs e)
    {
        //if (Session["showTAFup"] == null || Session["showTAFup"].ToString() == "0")
        //{
            btnShowAllfup.Text = "Show Edit Records";
            dsFollowup.SelectCommand = "SELECT [id], [PK_CommID], [Date], [Time], [StaffMember], [FollowupPerson], [Notes], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [isActive], Status FROM [TALogFollowUp] WHERE ([PK_CommID] = @PK_CommID )";
            Session["showTAFup"] = "1";
        //}
        //else
        //{
        //    btnShowAllfup.Text = "Show All";
        //    dsFollowup.SelectCommand = "SELECT [id], [PK_CommID], [Date], [Time], [StaffMember], [FollowupPerson], [Notes], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [isActive], Status FROM [TALogFollowUp] WHERE ([PK_CommID] = @PK_CommID AND Status = 'Edit Record')";
        //    Session["showTAFup"] = "0";
        //}
        dsFollowup.Select(DataSourceSelectArguments.Empty);
        dsFollowup.DataBind();
        gvFollowup.DataBind();
    }

    protected string hasFollowup(object commID)
    {
        if (commID!=null)
        {
            var follopUps = TALogFollowUp.Find(f => f.PK_CommID == Convert.ToInt32(commID));
            if (follopUps.Count > 0)
               // return "&#8730;";
                return follopUps.Count.ToString();
            else
                return "";
        }
        else
            return "";
    }
}