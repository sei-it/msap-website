﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Telerik.Web.UI;
using System.Text.RegularExpressions;

public partial class admin_TALogs_ContextualFactors : System.Web.UI.Page
{
    vwContextualfactorDataClassesDataContext schooldb = new vwContextualfactorDataClassesDataContext();

    protected void Page_Init(object sender, EventArgs e)
    {
       
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //always getting year1 APR reports
        int reportPeriod = 0, reportYear = 0, cohortType=0;
        if(Session["TALogCohortType"]==null)
            Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
        cohortType = Convert.ToInt32(Session["TALogCohortType"]);

        var rptPeriods = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType==cohortType).OrderBy(y => y.ID);
        foreach (var period in rptPeriods)
        {
            reportPeriod = period.ID;
            reportYear = Convert.ToInt32(period.reportYear);
            break;
        }
        hfReportYearID.Value = reportYear.ToString();
        hfReportPeriodID.Value = reportPeriod.ToString();

        if (Session["TALogCohortType"] != null && string.IsNullOrEmpty(Session["TALogCohortType"].ToString()))
            Session["TALogCohortType"] = null;
        if (Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
            Session["TALogGranteeID"] = null;
        if (!IsPostBack)
        {
            rbtnCohortType.SelectedValue = Session["TALogCohortType"] == null ? "1" : Session["TALogCohortType"].ToString();
            ddlGrantees.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();
        }
     

    }


    private string getSchoolType(string grades)
    {
        string retType = "";
        if (grades != null)
        {
            string[] arrgrade = grades.Split(';');
            int firstgrade = Convert.ToInt32(arrgrade.First());
            int lastgrade = Convert.ToInt32(arrgrade.Last());

            if (firstgrade >= 1 && lastgrade <= 8)
                retType = "Elementary";
            else if (6 <= firstgrade && lastgrade <= 10)
                retType = "Middle";
            else if (11 <= firstgrade && lastgrade <= 14)
                retType = "High";
            else if (1 <= firstgrade && lastgrade <= 10)
                retType = "Elementary/Middle";
            else if (7 <= firstgrade && lastgrade <= 14)
                retType = "Middle/High";
            else if (1 <= firstgrade && lastgrade <= 14)
                retType = "Combination all";
            else
                retType = "Other -- (" + firstgrade + " - " + lastgrade + ")";
        }
        return retType;
    }
    private void initialFields()
    {
        
        int cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        int reportPeriodID = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType==cohortType).Last().ID;
       
        int theSchoolID = Convert.ToInt32(hfeditSchoolID.Value);
        int granteeID = Convert.ToInt32(ddlGrantees.SelectedValue);
         int reportID = GranteeReport.SingleOrDefault(x=>x.GranteeID==granteeID).ID;
        string state = MagnetGranteeDatum.SingleOrDefault(x=>x.GranteeReportID== reportID).State;
       
        var gpraItem = MagnetGPRA.SingleOrDefault(x=>x.ReportID==reportID && x.SchoolID==theSchoolID);
        var schoolItem = MagnetSchool.SingleOrDefault(x=>x.ID== theSchoolID);


        txtGranteeName.Text = ddlGrantees.SelectedItem.Text;
        txtState.Text = state;
        txtGrantSize.Text = MagnetGPRA.Find(x => x.ReportID == reportID).Count.ToString();

        SchoolNameTextBox.Text = schoolItem.SchoolName;
        
        SchoolTypeTextBox.Text = getSchoolType(gpraItem.SchoolGrade);
        txtprogramType.Text = gpraItem.ProgramType == null ? "" : (bool)gpraItem.ProgramType ? "Partial program" : "Whole program";
        
        ddlProgramStatus.Text = schoolItem.ProgramStatus;
        ddlUrbanicity.SelectedValue = schoolItem.Urbanicity;
        ddlDifRange.SelectedValue = schoolItem.FreeRedueLunchRange;
        FreeReduceLunchTextBox.Text = schoolItem.FreeReduceLunch;
        txtTitle1Status.Text = gpraItem.ProgramType==null ? "" : (bool)gpraItem.TitleISchoolFunding ? "Yes" : "No";
       
        
        txtschoolDesignation.Text="";
        if (gpraItem.TitleISchoolFundingImprovementStatus != null)
        {
            switch (gpraItem.TitleISchoolFundingImprovementStatus)
            {
                case 5:
                    txtschoolDesignation.Text = "Excelling school";
                    break;
                case 3:
                    txtschoolDesignation.Text = "Continuous improvement";
                    break;
                case 8:
                    txtschoolDesignation.Text = "Improvement";
                    break;
                case 11:
                    txtschoolDesignation.Text = "Level 1";
                    break;
                case 27:
                    txtschoolDesignation.Text = "Turnaround";
                    break;
                case 28:
                    txtschoolDesignation.Text = "Other";
                    break;
                case 29:
                    txtschoolDesignation.Text = "Not Applicable";
                    break;
            }
        }
        txtschoolDesignation.Text = gpraItem.TitleISchoolFundingImprovementStatus.ToString();
        
        txtLowestAchieve.Text = gpraItem.PersistentlyLlowestAchievingSchool == null ? "" : (bool)gpraItem.PersistentlyLlowestAchievingSchool ? "Yes" : "No";
        txtSchoolGrant.Text = gpraItem.SchoolImprovementGrant==null ? "" : (bool)gpraItem.SchoolImprovementGrant ? "Yes" : "No";

        foreach (ListItem li in cbxlstMGI.Items)
            li.Selected = false;

        if (!string.IsNullOrEmpty(schoolItem.MGIObjectives))
        {
            foreach (string str in schoolItem.MGIObjectives.Split(';'))
            {
                foreach (ListItem li in cbxlstMGI.Items)
                {
                    if (li.Value.Equals(str))
                        li.Selected = true;
                }

            }
        }


        foreach (ListItem li in cblstMinGroup.Items)
            li.Selected = false;

        if (!string.IsNullOrEmpty(schoolItem.MinorityGroup))
        {
            foreach (string str in schoolItem.MinorityGroup.Split(';'))
            {
                foreach (ListItem li in cblstMinGroup.Items)
                {
                    if (li.Value.Equals(str))
                        li.Selected = true;
                }
            }
        }

        foreach (ListItem li in cbxlstSpecialist.Items)
            li.Selected = false;

        if (!string.IsNullOrEmpty(schoolItem.Specialists))
        {
            foreach (string str in schoolItem.Specialists.Split(';'))
            {
                foreach (ListItem li in cbxlstSpecialist.Items)
                {
                    if (li.Value.Equals(str))
                        li.Selected = true;
                }
            }
        }
        
        txtSpecialistOther.Text = schoolItem.SpecialistOther;
        
        ddlDesegrationPlan.SelectedValue = schoolItem.DesegrationPlan;

        ddlAttendType.SelectedValue = schoolItem.AttendType;

        ddlAdminMethod.SelectedValue = schoolItem.AdmissionMethod;

        txtAdminOther.Text = schoolItem.AdminMethodOther;

        //end theme
        ddlCurriculumnType.SelectedValue = schoolItem.CurriculumnType;

        //change Desegregationact to Marketingrecruitment
        ddlMarketingrecruitment.SelectedValue = schoolItem.Marketingrecruitment;

        ddlProfDevAct.SelectedValue = schoolItem.ProfDevAct;

        ddlDeliveryMethod.SelectedValue = schoolItem.DeliveryMethod;

        ddlengageAct.SelectedValue = schoolItem.engageAct;

        ddlSustainAct.SelectedValue = schoolItem.SustainAct;

        ddlCommPartAct.SelectedValue = schoolItem.CommPartAct;

        txtOther.Text = schoolItem.SchoolOther;

        //inital theme cat and subcat
        var cats = TAThemeCat.All().OrderBy(x => x.CatName);
        var mschool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfeditSchoolID.Value));

        radpnlbarCategory.Items.Clear();

        foreach (var cat in cats)
        {
            CheckBoxList cbsubTheme = new CheckBoxList();
            var subTheme = TAThemeSubCat.Find(sub => sub.ThemeCatID == cat.id);
            cbsubTheme.ID = "ThemeSubCat";
            cbsubTheme.DataTextField = "SubCatName";
            cbsubTheme.DataValueField = "id";
            cbsubTheme.DataSource = subTheme;
            cbsubTheme.DataBound += new EventHandler(CheckChanged);
            cbsubTheme.DataBind();
            RadPanelItem catItem = new RadPanelItem();
            catItem.Text = cat.CatName;
            catItem.Value = cat.id.ToString();
            radpnlbarCategory.Items.Add(catItem);
            RadPanelItem radPanelItemCheckBoxList = new RadPanelItem();
            radPanelItemCheckBoxList.Controls.Add(cbsubTheme);
            catItem.Items.Add(radPanelItemCheckBoxList);
        }

    }

    protected void OnEditSchool(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        hfeditSchoolID.Value = this.gdvSchool.DataKeys[row.RowIndex].Values["ID"].ToString();
        pnlSchoolgv.Visible = false;
        PopupPanel.Visible = true;

        InitalDropdownlists();
        initialFields();

        //ddlschoolDesignation.DataBind();
        
    }

    //the eventhandler
    protected void CheckChanged(object sender, EventArgs e)
    {
        var mschool = MagnetSchool.SingleOrDefault(s=>s.ID == Convert.ToInt32(hfeditSchoolID.Value));
        CheckBoxList lstBox = (CheckBoxList)sender; //the checkboxlist that raised the event
        if (mschool != null)
        {
            foreach (ListItem item in lstBox.Items)
            {
                if (Regex.IsMatch(mschool.TAthemeSubCat, item.Value))
                    item.Selected = true;
            }
        }
    }

    private void InitalDropdownlists()
    {
        string[] ddlTypes = "CT,MR,PDA,IDM,PEA,SA,CPA".Split(',');
        foreach (string type in ddlTypes)
        {
            //ddlThemeCat.DataBind();
            //ddlThemeSubCat.DataBind();
            DataTable dt = TALogDropdownList.Find(x => x.type == type).ToDataTable();
            switch (type)
            {
                case "CT":
                    ddlCurriculumnType.Items.Clear();
                    ddlCurriculumnType.Items.Add(new ListItem("Select One", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        ddlCurriculumnType.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    break;
                case "MR":
                    ddlMarketingrecruitment.Items.Clear();
                    ddlMarketingrecruitment.Items.Add(new ListItem("Select One", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        ddlMarketingrecruitment.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    break;
                case "PDA":
                    ddlProfDevAct.Items.Clear();
                    ddlProfDevAct.Items.Add(new ListItem("Select One", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        ddlProfDevAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    break;
                case "IDM":
                    ddlDeliveryMethod.Items.Clear();
                    ddlDeliveryMethod.Items.Add(new ListItem("Select One", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        ddlDeliveryMethod.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    break;
                case "PEA":
                    ddlengageAct.Items.Clear();
                    ddlengageAct.Items.Add(new ListItem("Select One", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        ddlengageAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    break;
                case "SA":
                    ddlSustainAct.Items.Clear();
                    ddlSustainAct.Items.Add(new ListItem("Select One", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        ddlSustainAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    break;
                case "CPA":
                    ddlCommPartAct.Items.Clear();
                    ddlCommPartAct.Items.Add(new ListItem("Select One", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        ddlCommPartAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    break;
            }
        }
        
    }

    protected void OnSaveSchool(object sender, EventArgs e)
    {
        //Always getting 1st year APR reports.
        int cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        int reportPeriodID = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType).First().ID;

        int theSchoolID = Convert.ToInt32(hfeditSchoolID.Value);
        int granteeID = Convert.ToInt32(ddlGrantees.SelectedValue);
        int reportID = GranteeReport.SingleOrDefault(x => x.GranteeID == granteeID && x.ReportPeriodID==reportPeriodID).ID;

        var granteeDataItem = MagnetGranteeDatum.SingleOrDefault(x => x.GranteeReportID == reportID);
        var granteeItem = MagnetGrantee.SingleOrDefault(x=>x.ID ==Convert.ToInt32(ddlGrantees.SelectedValue));
        var gpraItem = MagnetGPRA.SingleOrDefault(x => x.ReportID == reportID && x.SchoolID == theSchoolID);
        var schoolItem = MagnetSchool.SingleOrDefault(x => x.ID == theSchoolID);


        granteeItem.GranteeName = txtGranteeName.Text.Trim();
        granteeDataItem.State = txtState.Text.Trim();
        //txtGrantSize.Text =

        schoolItem.SchoolName = SchoolNameTextBox.Text.Trim();
        //gpraItem.SchoolGrade = SchoolTypeTextBox.Text; can not convert from 1 to Manay relation
        gpraItem.ProgramType = txtprogramType.Text == "" ? (bool?)null : (txtprogramType.Text == "Whole program" ? false : true);// Convert.ToBoolean(txtprogramType.Text);
        //gpraItem.ProgramType = ddlprogramType.SelectedValue == "" ? (bool?)null : (ddlprogramType.SelectedValue == "0" ? false : true);
        schoolItem.ProgramStatus = ddlProgramStatus.Text.Trim();
        schoolItem.Urbanicity = ddlUrbanicity.SelectedValue.Trim();
        schoolItem.FreeRedueLunchRange = ddlDifRange.SelectedValue;
        schoolItem.FreeReduceLunch = FreeReduceLunchTextBox.Text.Trim();
        //gpraItem.TitleISchoolFunding = ddlTitle1Status.SelectedValue=="" ? (bool?)null : ddlTitle1Status.SelectedValue=="0" ? false : true;
        gpraItem.TitleISchoolFunding = string.IsNullOrEmpty(txtTitle1Status.Text) ? (bool?)null : (txtTitle1Status.Text.ToLower()=="yes"? true : false);
        //gpraItem.TitleISchoolFundingImprovementStatus = ddlschoolDesignation.SelectedValue=="" ? (int?)null : Convert.ToInt32(ddlschoolDesignation.SelectedValue);
        //gpraItem.PersistentlyLlowestAchievingSchool = ddlLowestAchieve.SelectedValue=="" ? (bool?)null : ddlLowestAchieve.SelectedValue =="0" ? false : true;
        gpraItem.PersistentlyLlowestAchievingSchool = string.IsNullOrEmpty(txtLowestAchieve.Text) ? (bool?)null : txtLowestAchieve.Text.Trim() == "No" ? false : true;
        //gpraItem.SchoolImprovementGrant = ddlSchoolGrant.SelectedValue=="" ? (bool?)null : ddlSchoolGrant.SelectedValue=="0" ? false : true;
        gpraItem.SchoolImprovementGrant = string.IsNullOrEmpty(txtSchoolGrant.Text) ? (bool?)null : txtSchoolGrant.Text.ToLower() == "no" ? false : true;
        string strTmp = "";
        foreach (ListItem li in cbxlstMGI.Items)
        {
            if (li.Selected)
            {
                if (strTmp == "")
                    strTmp = li.Value;
                else
                    strTmp += ";" + li.Value;
            }
        }
        schoolItem.MGIObjectives = strTmp;

        strTmp = "";
        foreach (ListItem li in cblstMinGroup.Items)
        {
            if (li.Selected)
            {
                if (strTmp == "")
                    strTmp = li.Value;
                else
                    strTmp += ";" + li.Value;
            }
        }
        schoolItem.MinorityGroup = strTmp;
        strTmp = "";
        foreach (ListItem li in cbxlstSpecialist.Items)
        {
            if (li.Selected)
            {
                if (strTmp == "")
                    strTmp = li.Value;
                else
                    strTmp += ";" + li.Value;
            }
        }
        schoolItem.Specialists = strTmp;

        schoolItem.SpecialistOther = txtSpecialistOther.Text.Trim();

        schoolItem.DesegrationPlan = ddlDesegrationPlan.SelectedValue;

        schoolItem.AttendType = ddlAttendType.SelectedValue;

        schoolItem.AdmissionMethod = ddlAdminMethod.SelectedValue;

        schoolItem.AdminMethodOther = txtAdminOther.Text.Trim();

       
        //save Theme sub categories 
        var themecats = TAThemeCat.All();
        string themecat = "", themesubcat="";

        foreach (TAThemeCat category in themecats)
        {
            foreach (RadPanelItem pnlItem in radpnlbarCategory.Items)
            {
                CheckBoxList cblist = pnlItem.Items[0].FindControl("ThemeSubCat") as CheckBoxList;
            }
            CheckBoxList list = (CheckBoxList)radpnlbarCategory.FindItemByText(category.CatName).FindControl("ThemeSubCat");
            foreach (ListItem item in list.Items)
            {
                if (item.Selected)
                {
                    themecat += category.id + ",";

                    themesubcat += item.Value + ",";
                }
            }
            themecat = themecat.TrimEnd(',');
            themesubcat = themesubcat.TrimEnd(',');

            themecat += ";";
            themesubcat +=  ";";
        }
        themecat = themecat.TrimEnd(';');
        themesubcat = themesubcat.TrimEnd(';');

        schoolItem.TAthemeCat = themecat;
        schoolItem.TAthemeSubCat = themesubcat;

        schoolItem.CurriculumnType = ddlCurriculumnType.SelectedValue;

        schoolItem.Marketingrecruitment = ddlMarketingrecruitment.SelectedValue;

        schoolItem.ProfDevAct = ddlProfDevAct.SelectedValue;

        schoolItem.DeliveryMethod = ddlDeliveryMethod.SelectedValue;

        schoolItem.engageAct = ddlengageAct.SelectedValue;

        schoolItem.SustainAct = ddlSustainAct.SelectedValue;

        schoolItem.CommPartAct = ddlCommPartAct.SelectedValue;

        schoolItem.SchoolOther = txtOther.Text.Trim();

        schoolItem.ModifiedBy = HttpContext.Current.User.Identity.Name;
        schoolItem.ModifiedOn = DateTime.Now;

        granteeDataItem.Save();
        granteeItem.Save();
        gpraItem.Save();
        schoolItem.Save();

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            TAlog talogOjb = new TAlog();
            string user = HttpContext.Current.User.Identity.Name;
            string tablename = "Contextual Factors";
            string datestamp = DateTime.Now.ToString();
            string Action = "Updated";
            talogOjb.TALogDatahasbeenChangedbyED(user, tablename, datestamp, Action);
        }
        gdvSchool.DataBind();
        ClosePan(this, e);
    }


    protected void OnDeleteSchool(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gdvSchool.DataKeys[row.RowIndex].Values["ID"]);
        var data = schooldb.vwTAlogcontextualfactors.SingleOrDefault(x => x.ID == rcdid);
        data.isTALogActive = false;
        schooldb.SubmitChanges();

        gdvSchool.DataBind();
    }
    
     protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
     {
         if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
             Session["TALogGranteeID"] = ddlGrantees.SelectedValue;
     }
     protected void rbtnCohortType_SelectedIndexChanged(object sender, EventArgs e)
     {
         ddlGrantees.Items.Clear();
         ddlGrantees.Items.Add(new System.Web.UI.WebControls.ListItem("Select One", ""));
         Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
         Session["TALogGranteeID"] = null;
         ddlGrantees.DataBind();
     }

     protected void OnddlEdit(object sender, EventArgs e)
     {
         hfddltype.Value = (sender as LinkButton).CommandArgument;

         BindData();
         mpeNewsWindow.Show();
     }

    
   

     protected void ClosePan(object sender, EventArgs e)
     {
         pnlSchoolgv.Visible = true;
         PopupPanel.Visible = false;
     }

     private void BindData()
     {

         SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
         SqlDataAdapter da = new SqlDataAdapter("SELECT id, name, type FROM TALogDropdownLists where type = '" + hfddltype.Value + "'", con);

         DataTable dt = new DataTable();
         da.Fill(dt);

         gvDDLlist.DataSource = dt;

         gvDDLlist.DataBind();

         switch (hfddltype.Value.Trim())
         {
             case "CT":
                 ddlCurriculumnType.Items.Clear();
                 ddlCurriculumnType.Items.Add(new ListItem("Select One", ""));
                 foreach(DataRow rw in dt.Rows)
                 {
                     ddlCurriculumnType.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                 }
                 break;
             case "MR":
                 ddlMarketingrecruitment.Items.Clear();
                 ddlMarketingrecruitment.Items.Add(new ListItem("Select One", ""));
                 foreach(DataRow rw in dt.Rows)
                 {
                     ddlMarketingrecruitment.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                 }
                 break;
             case "PDA":
                 ddlProfDevAct.Items.Clear();
                 ddlProfDevAct.Items.Add(new ListItem("Select One", ""));
                 foreach(DataRow rw in dt.Rows)
                 {
                     ddlProfDevAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                 }
                 break;
             case "IDM":
                 ddlDeliveryMethod.Items.Clear();
                 ddlDeliveryMethod.Items.Add(new ListItem("Select One", ""));
                 foreach(DataRow rw in dt.Rows)
                 {
                     ddlDeliveryMethod.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                 }
                 break;
             case "PEA":
                 ddlengageAct.Items.Clear();
                 ddlengageAct.Items.Add(new ListItem("Select One", ""));
                 foreach (DataRow rw in dt.Rows)
                 {
                     ddlengageAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                 }
                 break;
             case "SA":
                 ddlSustainAct.Items.Clear();
                 ddlSustainAct.Items.Add(new ListItem("Select One", ""));
                 foreach (DataRow rw in dt.Rows)
                 {
                     ddlSustainAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                 }
                 break;
             case "CPA":
                 ddlCommPartAct.Items.Clear();
                 ddlCommPartAct.Items.Add(new ListItem("Select One", ""));
                 foreach (DataRow rw in dt.Rows)
                 {
                     ddlCommPartAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                 }
                 break;
         }

     }

     protected void OnNewddl(object sender, EventArgs e)
     {
         SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
         SqlDataAdapter da = new SqlDataAdapter("SELECT id, name, type FROM TALogDropdownLists where type = '" + hfddltype.Value + "'", con);

         DataTable dt = new DataTable();
         da.Fill(dt);
         
         // Here we'll add a blank row to the returned DataTable
         DataRow dr = dt.NewRow();
         dt.Rows.InsertAt(dr, 0);
         //Creating the first row of GridView to be Editable
         //dt.Rows[0][0] = TADropdownlist.All().Last().id + 1;
         dt.Rows[0][2] = hfddltype.Value;
         gvDDLlist.EditIndex = 0;
         gvDDLlist.DataSource = dt;
         gvDDLlist.DataBind();
         //Changing the Text for Inserting a New Record
         ((LinkButton)gvDDLlist.Rows[0].Cells[0].Controls[0]).Text = "Insert";
         mpeNewsWindow.Show();
     }

     protected void gvDDLlist_RowEditing(object sender, GridViewEditEventArgs e)
     {

         gvDDLlist.EditIndex = e.NewEditIndex;

         BindData();
         mpeNewsWindow.Show();
     }

     protected void gvDDLlist_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
     {

         gvDDLlist.EditIndex = -1;

         BindData();
         mpeNewsWindow.Show();
     }

     protected void gvDDLlist_RowUpdating(object sender, GridViewUpdateEventArgs e)
     {

         if (((LinkButton)gvDDLlist.Rows[0].Cells[0].Controls[0]).Text == "Insert")
         {

             SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
             SqlCommand cmd = new SqlCommand();

             cmd.CommandText = "INSERT INTO TALogDropdownLists(name, type) VALUES(@name, @type)"; 
             cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = ((TextBox)gvDDLlist.Rows[0].Cells[2].Controls[0]).Text;
             cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = gvDDLlist.Rows[0].Cells[3].Text;
             cmd.Connection = con;

             con.Open();

             cmd.ExecuteNonQuery();

             con.Close();

         }

         else
         {

             SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
             SqlCommand cmd = new SqlCommand();

             cmd.CommandText = "UPDATE TALogDropdownLists SET name=@name, type = @type WHERE id=@id";
             cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = ((TextBox)gvDDLlist.Rows[e.RowIndex].Cells[2].Controls[0]).Text;
             cmd.Parameters.Add("@type", SqlDbType.VarChar).Value =gvDDLlist.Rows[e.RowIndex].Cells[3].Text;
             cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(gvDDLlist.Rows[e.RowIndex].Cells[1].Text);
             cmd.Connection = con;

             con.Open();

             cmd.ExecuteNonQuery();

             con.Close();
         }
         BindData();
     }

     protected void gvDDLlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
     {

         SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
         SqlCommand cmd = new SqlCommand();

         cmd.CommandText = "DELETE FROM TALogDropdownLists WHERE id=@id"; cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(gvDDLlist.Rows[e.RowIndex].Cells[1].Text);
         cmd.Connection = con;

         con.Open();

         cmd.ExecuteNonQuery();

         con.Close();

         BindData();

     }

     //protected void ddlThemeCat_SelectedIndexChanged(object sender, EventArgs e)
     //{
     //    hfThemeCatID.Value = ddlThemeCat.SelectedValue;
     //    InitialThemeCat_SubCat(hfThemeCatID.Value=="" ? 0 : Convert.ToInt32(hfThemeCatID.Value), 0);
     //}

     //private void InitialThemeCat_SubCat(int themecatID, int themeSubCatID)
     //{
     //    //create theme categories dropdown list
     //    var themecats = TAThemeCat.All();
     //    ddlThemeCat.Items.Clear();
     //    ddlThemeCat.Items.Add(new ListItem("Select One", ""));

     //    ddlThemeSubCat.Items.Clear();
     //    ddlThemeSubCat.Items.Add(new ListItem("Select One", ""));

     //    foreach (TAThemeCat cat in themecats)
     //    {
     //       ddlThemeCat.Items.Add(new ListItem(cat.CatName, cat.id.ToString()));
     //       if (cat.id == themecatID)
     //           ddlThemeCat.SelectedValue = themecatID.ToString();
     //    }

     //    //create theme sub-categories dropdown list
     //    if (themecatID > 0)
     //    {
     //       var Themesubcats = (TAThemeSubCat.Find(x => x.ThemeCatID == themecatID).OrderBy(s=>s.SubCatName));
     //        foreach (TAThemeSubCat subcat in Themesubcats)
     //        {
     //            ddlThemeSubCat.Items.Add(new ListItem(subcat.SubCatName, subcat.id.ToString()));
     //            if (subcat.id == themeSubCatID)
     //                ddlThemeSubCat.SelectedValue = themeSubCatID.ToString();
     //        }
     //    }
     //}

     //protected void ddlschoolDesignation_DataBound(object sender, EventArgs e)
     //{
     //    ddlschoolDesignation.SelectedValue = hfDesignationvalue.Value;
     //}
}