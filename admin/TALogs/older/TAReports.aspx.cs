﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Data.SqlClient;

public partial class admin_TALogs_TAReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["TALogIsMSAPGrantee"] != null && string.IsNullOrEmpty(Session["TALogIsMSAPGrantee"].ToString()))
            Session["TALogIsMSAPGrantee"] = null;
        if (Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
            Session["TALogGranteeID"] = null;

        if (!IsPostBack)
        {
            rbtnCohort.SelectedValue = Session["TALogIsMSAPGrantee"] == null ? "1" : Session["TALogIsMSAPGrantee"].ToString();
            ddlGrantees.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();
        }

    }
    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
            Session["TALogGranteeID"] = ddlGrantees.SelectedValue;
    }
    protected void rbtnCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGrantees.Items.Clear();
        ddlGrantees.Items.Add(new ListItem("Select One", ""));
        Session["TALogIsMSAPGrantee"] = rbtnCohort.SelectedValue;
        Session["TALogGranteeID"] = null;
    }

    protected void lbtnGranteeCharact_Click(object sender, EventArgs e)
    {
        string strGranteeCharact = "";
        string cohortType = rbtnCohort.SelectedValue;
        strGranteeCharact = " DECLARE @sum as int " +

                            " SET @sum = (Select COUNT(*) from MagnetGrantees WHERE (MagnetGrantees.CohortType = " + cohortType + ") AND (MagnetGrantees.isActive = 1)and MagnetGrantees.id NOT IN(39,40,41)) " +
                            " SELECT    ( select Location from states where Abbrev=MagnetGranteeContacts.State) as State, " +

                            " ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage, COUNT(*) as n " +
                            " FROM         MagnetGrantees INNER JOIN " +
                            "            MagnetGranteeContacts ON MagnetGrantees.ID = MagnetGranteeContacts.LinkID  " +

                            " WHERE  (MagnetGranteeContacts.ContactType =  1) AND (MagnetGrantees.isActive = 1)and MagnetGrantees.id NOT IN(39,40,41) " +
                            " group by MagnetGranteeContacts.State ";

        DataSet dsGranteeCharact = new DataSet();
        dsGranteeCharact.Tables.Add(getData(strGranteeCharact, "GranteeCharact"));

        strGranteeCharact = "";

        strGranteeCharact = " DECLARE @sum as int " +
                            " SELECT  @sum = COUNT(*) from dbo.vwGranteeSchoolNo " +

                            " select CAST(SchoolNo AS varchar(10)) +  " +
                            " CASE WHEN SchoolNo=1 THEN ' school' ELSE ' schools' END as Grantsize, ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage, COUNT(*) as n " +
                            " from dbo.vwGranteeSchoolNo " +
                            " group by SchoolNo " +
                            " order by SchoolNo ";

        dsGranteeCharact.Tables.Add(getData(strGranteeCharact, "GrantSize"));

        OpenPDF("GranteeReport.rpt", dsGranteeCharact);
    }

    protected void lbtnSchoolCharact_Click(object sender, EventArgs e)
    {
        DataSet dsSchoolCharact = new DataSet();
        int reportyear = 3, reportPeriodId = 6;
        string strQuery = "";


        #region school type
        strQuery =  " DECLARE @sum as int " +
                    " SET @sum = (Select COUNT(*) FROM [dbo].[vwTAlogcontextualfactors] " +
                    "   where reportyear = " + reportyear + " and reportperiodid=" + reportPeriodId + " and schooltype<>'Other') " +

                    " SELECT  " +
                    " (CASE schooltype  " +
                    " 	WHEN 'Elementary/middle' " +
                    " 	THEN '			Elementary/middle' " +
                    " 	WHEN 'Middle/high' " +
                    " 	THEN '			Middle/high' " +
                    " 	WHEN 'Combination all' " +
                    " 	THEN '			K-12' " +
                    " 	ELSE schooltype END)  " +
                    " 	as  " +
                    " 	schooltype, ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage, COUNT(*) as n " +

                    "    FROM [dbo].[vwTAlogcontextualfactors] " +
                    "   where reportyear = "  + reportyear + " and reportperiodid=" + reportPeriodId + " and schooltype<>'Other' " +
                    "   group by Schooltype  " +
                    "   order by Schooltype  ";
        DataTable rtntbl = getData(strQuery, "schooltype");
        DataTable sortedTBL = rtntbl.Clone();
        
        //Elementary
        DataRow rw1 = sortedTBL.NewRow();
        rw1[0] = rtntbl.Rows[3][0];
        rw1[1] = rtntbl.Rows[3][1];
        rw1[2] = rtntbl.Rows[3][2];
        sortedTBL.Rows.Add(rw1);
        //Middle
        DataRow rw2 = sortedTBL.NewRow();
        rw2[0] = rtntbl.Rows[5][0];
        rw2[1] = rtntbl.Rows[5][1];
        rw2[2] = rtntbl.Rows[5][2];
        sortedTBL.Rows.Add(rw2);
        //High
        DataRow rw3 = sortedTBL.NewRow();
        rw3[0] = rtntbl.Rows[4][0];
        rw3[1] = rtntbl.Rows[4][1];
        rw3[2] = rtntbl.Rows[4][2];
        sortedTBL.Rows.Add(rw3);
        //Combination
        DataRow rw4 = sortedTBL.NewRow();
        rw4[0] = "Combination";
        rw4[1] = Convert.ToInt32(rtntbl.Rows[0][1]) + Convert.ToInt32(rtntbl.Rows[1][1]) + Convert.ToInt32(rtntbl.Rows[2][1]);
        rw4[2] = Convert.ToInt32(rtntbl.Rows[0][2]) + Convert.ToInt32(rtntbl.Rows[1][2]) + Convert.ToInt32(rtntbl.Rows[2][2]);
        sortedTBL.Rows.Add(rw4);

        //  Elementary/Middle
        DataRow rw5 = sortedTBL.NewRow();
        rw5[0] = rtntbl.Rows[0][0];
        rw5[1] = rtntbl.Rows[0][1];
        rw5[2] = rtntbl.Rows[0][2];
        sortedTBL.Rows.Add(rw5);

        //  Middle/High
        DataRow rw6 = sortedTBL.NewRow();
        rw6[0] = rtntbl.Rows[2][0];
        rw6[1] = rtntbl.Rows[2][1];
        rw6[2] = rtntbl.Rows[2][2];
        sortedTBL.Rows.Add(rw6);

        //  K-12
        DataRow rw7 = sortedTBL.NewRow();
        rw7[0] = rtntbl.Rows[1][0];
        rw7[1] = rtntbl.Rows[1][1];
        rw7[2] = rtntbl.Rows[1][2];
        sortedTBL.Rows.Add(rw7);

        sortedTBL.TableName = "Schooltype";
        dsSchoolCharact.Tables.Add(sortedTBL);

        DataTable tblSchooltypechart = sortedTBL.Copy();
        tblSchooltypechart.TableName = "Schooltypechart";

        for (int i = tblSchooltypechart.Rows.Count - 1; i >= 4; i--)
        {
            tblSchooltypechart.Rows[i].Delete();
        }

        dsSchoolCharact.Tables.Add(tblSchooltypechart);

#endregion

        #region program type
        strQuery = " DECLARE @sum as int " +
                            " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where ProgramType IS NOT NULL " +
                            " and reportyear = "  + reportyear + " and reportperiodid=" + reportPeriodId + 
                            " SELECT  [ProgramType],ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +
                            "   FROM [dbo].[vwTAlogcontextualfactors] " +
                            "   where ProgramType IS NOT NULL  and reportyear = "  + reportyear + " and reportperiodid=" + reportPeriodId + 
                            "   group by ProgramType";
        dsSchoolCharact.Tables.Add(getData(strQuery,"Programtype"));
        #endregion

        #region program status
        strQuery = " DECLARE @sum as int " +

                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where Programstatus IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  [Programstatus],ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where Programstatus IS NOT NULL  and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by Programstatus ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "Programstatus"));
        #endregion

        #region Urbanicity
        strQuery = " DECLARE @sum as int " +

                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where Urbanicity IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  [Urbanicity],ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where Urbanicity IS NOT NULL  and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by Urbanicity ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "Urbanicity"));
        #endregion

        #region Free and reduced-price lunch
        strQuery = " DECLARE @sum as int " +

                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where FreeReduceLunch IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  FreeReduceLunch,ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where FreeReduceLunch IS NOT NULL  and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by FreeReduceLunch ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "Freelunch"));
        #endregion

        #region Title I status
        strQuery = " DECLARE @sum as int " +
                   " Select @sum = COUNT(*) FROM [dbo].[vwTAlogcontextualfactors]  where Title1Status IS NOT NULL " +
                   " and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " SELECT  CASE WHEN Title1Status='Yes' THEN 'Title I school' ELSE 'Not a Title I school' END," +
                   " ROUND(CAST(COUNT(*)*100 AS FLOAT)/@sum ,0) AS pcntage " +

                   " FROM [dbo].[vwTAlogcontextualfactors] " +
                   " where Title1Status IS NOT NULL and reportyear=" + reportyear + " and reportperiodid= " + reportPeriodId +
                   " group by Title1Status order by Title1Status desc ";
        dsSchoolCharact.Tables.Add(getData(strQuery, "TitleIschool"));
        #endregion

        OpenPDF("SchoolCharactReport.rpt", dsSchoolCharact);
    }

    private DataTable getData(string strQuery, string tableName)
    {
        DataSet rtnDS = new DataSet();
        string stcnn = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString;
        SqlConnection cnn = new SqlConnection(stcnn);
        cnn.Open();
        SqlDataAdapter adpt = new SqlDataAdapter(strQuery, cnn);
        adpt.Fill(rtnDS, tableName);
        cnn.Close();

        DataTable rtnTBL = new DataTable();
        rtnTBL = rtnDS.Tables[0].Copy();
        rtnTBL.TableName = tableName;
        return rtnTBL;
    }


    private void OpenPDF(string downloadAsFilename, DataSet ds)
    {
        ReportDocument Rel = new ReportDocument();
        Rel.Load(Server.MapPath("./reports/Commlog/" + downloadAsFilename));

        Rel.SetDataSource(ds);
        // Stop buffering the response
        Response.Buffer = false;
        // Clear the response content and headers
        Response.ClearContent();
        Response.ClearHeaders();

        ExportFormatType format = ExportFormatType.PortableDocFormat;

        string reportName = downloadAsFilename.Substring(0, downloadAsFilename.Length - 4);

        try
        {
            Rel.ExportToHttpResponse(format, Response, true, reportName);
        }
        catch (System.Threading.ThreadAbortException)
        {
            //ThreadException can happen for internale Response implementation
        }
        catch (Exception ex)
        {
            //other exeptions will be managed   
            throw;
        }

    }
   
}