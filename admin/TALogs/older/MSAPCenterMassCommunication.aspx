﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="MSAPCenterMassCommunication.aspx.cs" Inherits="admin_TALogs_MSAPCenterMassCommunication" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
<link href="../../css/calendar.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript">
      document.onkeydown = function (e) {
          // keycode for F5 function
          if (e.keyCode === 116) {
              return false;
          }
      };

//      $(function () {
//         if($("select[name=ctl00$ContentPlaceHolder1$ddlIssue] option:selected").val()=="None")
//             $("#ctl00_ContentPlaceHolder1_txtIssue").hide();

//             //role
//             $(".Issuechange").change(function () {
//                 var value = $("select[name=ctl00$ContentPlaceHolder1$ddlIssue] option:selected").val();
//                 if (value != "None") {
//                     $("#ctl00_ContentPlaceHolder1_txtIssue").show();
//                 }
//                 else {
//                     $("#ctl00_ContentPlaceHolder1_txtIssue").hide();
//                     $("#ctl00_ContentPlaceHolder1_txtIssue").val('');
//                 }
//             });

//         });
         
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1>
       Communication Management 
    </h1>
    <div class="titles">MSAP Mass Communication Log</div>
    <div class="tab_area">
    	<ul class="tabs">
         
            <%--<li><asp:LinkButton ID="LinkButton3" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton></li>--%>
            <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li class="tab_active">Page 6</li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
                        <li><asp:LinkButton ID="LinkButton9" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContextualFactors.aspx" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton10" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/>

       <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlMassRcd" runat="server">
 <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New Mass Communication" CssClass="surveyBtn2" OnClick="onAddnew"  />
    &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnShowAllMassComm" runat="server" Text="Show All" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAllMassComm"  />
    </p>
    <asp:GridView ID="gdvMassComm" runat="server" AllowPaging="true" AllowSorting="false" DataSourceID="dsMassComm"
     PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" >
        <Columns>
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}"/>
            <asp:BoundField DataField="Sender" SortExpression="" HeaderText="Sender" />
            <asp:BoundField DataField="Issue" SortExpression="" HeaderText="Issues" />
            
            <asp:BoundField DataField="type" SortExpression="" HeaderText="Communication Type" />
            <asp:BoundField DataField="description" SortExpression="" HeaderText="Communication Detail" />
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
            <asp:BoundField DataField="Modifiedby" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on"  DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false" />

            <asp:TemplateField>
            <ItemStyle Wrap="false" />
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
    </asp:Panel>

    <asp:Panel ID="PopupPanel" runat="server" Visible="false" >
        <div class="mpeDivConsult" style="width:850px">
            <div class="mpeDivHeaderConsult" style="width:835px">
                Add/Edit Mass Communication
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" Text="Close"  OnClick="OnClose" CssClass="closeit_link" />
                </span>
                </div>
                <table style="padding:0px 10px 10px 10px; width:850px">
                    <tr>
                    <td class="consult_popupTxt">Date:</td>
                        <td>
                            <span><asp:TextBox ID="txtDate" runat="server" />
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate"  CssClass="AjaxCalendar" />
                            </span>
                        </td>
                    </tr>
					<tr>
                    <td class="consult_popupTxt">Type of communication:</td>
                        <td>
						<asp:DropDownList ID="ddltype" runat="server" CssClass="Drop
                            DownListCssClass">
                        <asp:ListItem Value="">Select one</asp:ListItem>
						<asp:ListItem>Email</asp:ListItem>
						</asp:DropDownList>
						</td>
					</tr>
					<tr>
					<td class="consult_popupTxt">Mass communication description:</td>
                        <td>
						 <asp:TextBox ID="txtDes" runat="server"  TextMode="MultiLine" Width="400px" Height="120px">
                         </asp:TextBox>
						</td>
					</tr>
					<tr>
					<td class="consult_popupTxt">Mass communication concerns:</td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                            <asp:DropDownList ID="ddlIssue" runat="server" AutoPostBack="true" 
                                    CssClass="Issuechange DropDownListCssClass" onselectedindexchanged="ddlIssue_SelectedIndexChanged" >
                        <asp:ListItem>None</asp:ListItem>
						<asp:ListItem>Bounce-backs</asp:ListItem>
						<asp:ListItem>Others</asp:ListItem>
						</asp:DropDownList>
                            <asp:TextBox ID="txtIssue" runat="server" Width="430px" Visible="false"></asp:TextBox>
                             <asp:TextBox ID="txtBounceback" runat="server" Width="430px" Visible="false"></asp:TextBox>
                            </ContentTemplate>
                            </asp:UpdatePanel>
						</td>
					</tr>
	<tr>
        <td class="consult_popupTxt"><asp:Label ID="lblstaffmember" runat="server" /></td>
        <td>
		 <asp:TextBox ID="txtsender" runat="server" Enabled="false" CssClass="txtboxDisableClass"></asp:TextBox>
        </td>
    </tr>
					<tr>
					<td class="consult_popupTxt"  style="vertical-align:top;">Communication recipient:</td>
                        <td  style="border-right: 0px !important; color:Black;" width="70%">
                        <div><h3>2010 MSAP Cohort</h3></div>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                            <div style="width:650px"><b><asp:CheckBox ID="cbxAll2010" runat="server" Text="All 2010 grantees" AutoPostBack="true"
                                oncheckedchanged="cbxAll2010_CheckedChanged" /></b>
                        <hr />
                        </div>
                               
                        <div>
                            <asp:CheckBoxList  ID="cblRecipients10" runat="server" RepeatColumns="2" RepeatLayout="table"  
                            DataSourceID="dsGrantee10" DataTextField="GranteeName" DataValueField="id" 
                                CssClass="RadioButtonList" ondatabound="cblRecipients_DataBound"  >
                            </asp:CheckBoxList>
                        </div>
                     </ContentTemplate>
                            </asp:UpdatePanel>
                        <div><h3>2013 MSAP Cohort</h3></div>
                         <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                        <div style="width:650px"><b><asp:CheckBox ID="cbxAll2013" runat="server" Text="All 2013 grantees" AutoPostBack="true" 
                                oncheckedchanged="cbxAll2013_CheckedChanged" /></b>
                                <hr />
                                </div>
                        <div>
                            <asp:CheckBoxList  ID="cblRecipients13" runat="server" RepeatColumns="2" RepeatLayout="table"  
                            DataSourceID="dsGrantee13" DataTextField="GranteeName" DataValueField="id" 
                                CssClass="RadioButtonList" ondatabound="cblRecipients_DataBound"  >
                            </asp:CheckBoxList>
                        </div>
                             </ContentTemplate>
                        </asp:UpdatePanel>
                        </td>
					</tr>
					<tr>
					<td class="consult_popupTxt" style="vertical-align:top;">Role recipient:</td>
                         <td style="border-right: 0px !important; color:Black;" width="70%">
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                             <ContentTemplate>
                              <asp:CheckBoxList ID="cblRole" runat="server" RepeatColumns="3" 
                                     AutoPostBack="true" CssClass="change" 
                            DataSourceID="dsRole" DataTextField="rolename" DataValueField="id" 
                                     onselectedindexchanged="cblRole_SelectedIndexChanged" 
                                     ondatabound="cblRole_DataBound" >
                            </asp:CheckBoxList>
                             <asp:TextBox ID="txtRoleOther" runat="server" Width="570px" Visible="false"/>
                             </ContentTemplate>
                             </asp:UpdatePanel>
                        </td>
					</tr>
                    <tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlMassCommStatus" runat="server"  CssClass="DropDownListCssClass" SelectedValue='<%# Bind("Status") %>'>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem Selected="True">Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>
<tr><td colspan="2" style="padding-top:50px;"></td></tr>
				 <tr>
                    <td>
                        <asp:Button ID="btnAddnew" runat="server" CssClass="surveyBtn2" Text="Save Record" OnClick="OnSave" />
                    </td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="surveyBtn2" Text="Close Window" OnClick="OnClose" />
                    </td>
                </tr>	
		<tr><td colspan="2" style="padding-top:50px;"></td></tr>			
	</table>
        </div>
    </asp:Panel>
    

    <asp:SqlDataSource ID="dsGrantee10" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
         SelectCommand="SELECT * FROM [MagnetGrantees] WHERE ([isActive] = 1 and CohortType=1 and id not in(39,40,41)) order by GranteeName "  >
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsGrantee13" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
         SelectCommand="SELECT * FROM [MagnetGrantees] WHERE ([isActive] = 1 and CohortType=2 and id not in(69,70,1001,1002,1003)) order by GranteeName"  >
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsRole" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        
        SelectCommand="SELECT [id], [rolename] FROM [TALogRoles] WHERE isActive = 1" >
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="dsMassComm" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
      SelectCommand ="select * from dbo.TALogMassComm  order by Date desc" >
    </asp:SqlDataSource>
    <asp:HiddenField ID="hfRoleOther" runat="server" />
    <asp:HiddenField ID="hfRecipientIDs" runat="server" />
    <asp:HiddenField ID="hfRoleIDs" runat="server" />
        <div style="text-align: right; margin-top: 20px;">
            <br />
<asp:LinkButton ID="LinkButton15" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton7" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton13" PostBackUrl="ContextualFactors.aspx" ToolTip="Contextual factors" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton14" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           Page 6&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton11" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
          <%--&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton8" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton>--%>
           
        </div>

    <asp:HiddenField ID="hfID" runat="server" />
</asp:Content>
