﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="TCallIndex.aspx.cs" Inherits="admin_TCallLogs_TCallIndex" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="skmValidators" Namespace="skmValidators" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../../js/jquery.maskedinput.js" type="text/javascript"></script> 
    <link href="../../css/calendar.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    function percentage_vald1(sender, args) {
        args.IsValid = true;
        var num = document.getElementById('<%=txtLevel.ClientID%>').value;
        if (num == '') {
            sender.textContent = sender.innerText = sender.innerHTML = "*This field is required."
            args.IsValid = false;
        }
        else if (num > 100) {
            sender.textContent = sender.innerText = sender.innerHTML = "*A percent cannot be greater than 100."
            args.IsValid = false;
        }
    }

    function percentage_vald(sender, args) {
        args.IsValid = true;
        var num = document.getElementById('<%=txt2ndPersonnelCommit.ClientID%>').value;
        if (num == '') {
            sender.textContent = sender.innerText = sender.innerHTML = "*This field is required."
            args.IsValid = false;
        }
        else if (num > 100) {
            sender.textContent = sender.innerText = sender.innerHTML = "*A percent cannot be greater than 100."
            args.IsValid = false;
        }
    }

    function OnBeforeUnLoad(e) {
        if ((window.event.clientX < 0) || (window.event.clientY < 0)) {
            PageMethods.BrowserCloseMethod();
        }
//        var isSaved = $('input[name="ctl00$ContentPlaceHolder1$hfSaved"]').val();
//        var btnClick = $('input[name="ctl00$ContentPlaceHolder1$hfisbtnClick"]').val();
//        if (isSaved == "0" && btnClick=="0") {
//            return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
//            
//        }
    }
    $(document).ready(function () {
        $(".btnSave").click(function () {
            $('input[name="ctl00$ContentPlaceHolder1$hfSaved"]').val('0');
            $('input[name="ctl00$ContentPlaceHolder1$hfisbtnClick"]').val('1');
        });

        $(".lbtnclik").click(function () {
            var isSaved = $('input[name="ctl00$ContentPlaceHolder1$hfSaved"]').val();
            if (isSaved == "0") {
                PageMethods.BrowserCloseMethod();
            }
        });

        window.onbeforeunload = OnBeforeUnLoad;
    });

    </script>
<script type="text/javascript">

    document.onkeydown = function (e) {
        // keycode for F5 function
        if (e.keyCode === 116) {
            return false;
        }
        // keycode for backspace
//        if (e.keyCode === 8) {
//            // try to cancel the backspace
//            return false;
//        }
    };
    
    jQuery(function($){
        $("#<%= txtPhone.ClientID %>").mask("(999) 999-9999? x99999");
        $("#<%= txt2ndPhone.ClientID %>").mask("(999) 999-9999? x99999");
    });
</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" />

     <h1 >
        Desk Monitoring
    </h1>
     <div class="titles">Desk Monitoring</div>
<div class="tab_area">
    	<ul class="tabs">
            <li class="tab_active">Page 7</li>
            <%--<li><asp:LinkButton ID="LinkButton16" PostBackUrl="TAReports.aspx" ToolTip="Reports " runat="server">Page 8</asp:LinkButton></li>--%>
            <li><asp:LinkButton ID="LinkButton4" CommandArgument="MSAPCenterMassCommunication.aspx"  OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="MSAP Center mass communication"  runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" CommandArgument="MSAPCenterActParticipation.aspx" OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
                        <li><asp:LinkButton ID="LinkButton9" CommandArgument="Communications.aspx" OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton6" CommandArgument="ContextualFactors.aspx" OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="School Information" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton1" CommandArgument="ContactInfo.aspx" OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton10" CommandArgument="Overview.aspx" OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/><br />
    <asp:Panel ID="pnlList" runat="server">
<table>
<tr>
<td colspan="2">
<asp:RadioButtonList ID="rbtnCohortType" runat="server" RepeatColumns="2" 
        RepeatDirection="Horizontal" AutoPostBack="true" onselectedindexchanged="rbtnCohortType_SelectedIndexChanged" >
        <asp:ListItem Value="1" >Cohort 2010</asp:ListItem>
        <asp:ListItem Value="2" Selected="True">Cohort 2013</asp:ListItem> 
    </asp:RadioButtonList>
</td>
</tr>
<tr>
<td class="consult_popupTxt">Grantees: </td>
<td>
<asp:DropDownList ID="ddlgrant" runat="server" DataSourceID="dsGrantee"  CssClass="DropDownListCssClass"
        DataTextField="GranteeName" DataValueField="id"   
        onselectedindexchanged="ddlgrant_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" >
         <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>
     
    </td>
    
</tr>

</table>
<h3>Post Award Call</h3>

        <asp:Button ID="btnNewCall" runat="server" Text="New log" CssClass="surveyBtn2" CausesValidation="false" Visible="false"
             onclick="NewCalllog_Click" />
    

<asp:GridView ID="gvGranteeList" runat="server" DataSourceID="dsvwTCallMain" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
       PageSize="10"  AllowPaging="true" AllowSorting="false" AutoGenerateColumns="False" DataKeyNames="ID" CssClass="msapTbl"  >
        <Columns>
        <%--<asp:BoundField DataField="id" SortExpression="" HeaderText="Item #" />--%>
        <asp:TemplateField HeaderText="Status">
        <ItemTemplate>
        <%# String.IsNullOrEmpty(Eval("status").ToString()) ? "<span  style='color:Red;'><strong>Pending</strong></span>" : ((bool)Eval("status") ? "<span  style='color:Green;'><strong>Completed</strong></span>" : "<span  style='color:Red;'><strong>Pending</strong></span>")%>
        </ItemTemplate>
        </asp:TemplateField>
            <asp:BoundField DataField="PRAward" SortExpression="" HeaderText="PR Award #" />
            <asp:BoundField DataField="GranteeName" SortExpression="" HeaderText="Grantee Name" />
            <asp:BoundField DataField="ProgramOfficer" SortExpression="" HeaderText="Program Officer" />
            <asp:BoundField DataField="state" SortExpression="" HeaderText="State" />

            <asp:BoundField DataField="Calldate" SortExpression="" HeaderText="Date of Call" DataFormatString="{0:MM/dd/yyyy}" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnEdit" runat="server" Text= "Edit" CausesValidation="true" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditCallLog"></asp:LinkButton>
                   <asp:LinkButton ID="lbtnPrint" runat="server" Text="Print" OnClick="PrintPDFReport" CausesValidation="false" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
    <br /><br />

 <hr />
       <h3>Quarterly Call</h3>

     <%--<table >
     <tr>
     <td class="consult_popupTxt"><asp:literal runat="server" ID="ltlItemNo" Text="Item number:" /></td>
      <td>   <asp:DropDownList ID="ddlTcallmains" runat="server" AutoPostBack="True" AppendDataBoundItems="true"
               DataSourceID="dsvwTCallMain" DataTextField="id" DataValueField="id" 
              onselectedindexchanged="ddlTcallmains_SelectedIndexChanged" >
               <asp:ListItem Value="">Select one</asp:ListItem>
           </asp:DropDownList>
      </td>  
     </tr>
     </table>--%>

       <p style="text-align:left">
        <asp:Button ID="btnNew2ndCall" runat="server" Text="New Call"  Visible="false" 
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="btnNew2ndCall_Click" />
        
    </p>
     <asp:GridView ID="gv2ndCall" runat="server" AutoGenerateColumns="False" 
            CssClass="msapTbl" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
             DataSourceID="dsTCalladt" DataKeyNames="ID" AllowPaging="True">
            <Columns>
            <asp:BoundField DataField="PRAward" SortExpression="" HeaderText="PR Award #" />
            <asp:BoundField DataField="GranteeName" SortExpression="" HeaderText="Grantee Name" />
            <asp:BoundField DataField="PO" SortExpression="" HeaderText="Program Officer" />
            <asp:BoundField DataField="state" SortExpression="" HeaderText="State" />

            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date of Call" DataFormatString="{0:MM/dd/yyyy}" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnEdit" runat="server" Text= "Edit" CausesValidation="true" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit2ndCall"></asp:LinkButton>
                   <asp:LinkButton ID="lbtnPrint" runat="server" Text="Print" OnClick="PrintQuarterPDFReport" CausesValidation="false" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
            <HeaderStyle Wrap="False" />
        </asp:GridView>
</asp:Panel>


    <asp:Panel ID="pnl2ndCall" runat="server" Width="100%" Visible="false">
    <table Width="100%">
    <tr>
    <td class="calllog_popupTxt">PR/Award #:</td>
    <td><asp:TextBox ID="txt2ndPRaward" runat="server"  Enabled="false" ValidationGroup="vglogform" CssClass="txtboxDisableClass" /></td>
    <td class="calllog_popupTxt">Grantee Name:</td>
    <td colspan="2"><asp:TextBox ID="txt2ndGranteeName" runat="server"  Enabled="false" ValidationGroup="vglogform" Width="460px" CssClass="txtboxDisableClass" /></td>
    </tr>
    <tr>
    <td class="calllog_popupTxt">Program Officer:</td>
    <td><asp:TextBox ID="txt2ndPofficer" runat="server" Enabled="false"  CssClass="txtboxDisableClass" /> </td>
       <td class="calllog_popupTxt">Phone Number:</td>
     <td><asp:TextBox ID="txt2ndPhone" runat="server"  ValidationGroup="vglogform" />
     </td>
     <td>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ForeColor="Red" ValidationGroup="vglogform" SetFocusOnError="true" ControlToValidate="txt2ndPhone" ErrorMessage="*This field is required."></asp:RequiredFieldValidator>
     <br />
     </td>
        </tr>
    <tr>
    <td class="calllog_popupTxt">Project Director:</td>
    <td><asp:TextBox ID="txt2ndPdirector" runat="server" /></td>
    <td class="calllog_popupTxt">Date of Call:</td>
 <td ><asp:TextBox ID="txt2ndCallDate" runat="server"  />
 </td><td>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ForeColor="Red" ValidationGroup="vglogform" ControlToValidate="txt2ndCallDate" SetFocusOnError="true" ErrorMessage="*This field is required."></asp:RequiredFieldValidator><br />
 <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ValidationGroup="vglogform" SetFocusOnError="true"
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txt2ndCallDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajax:CalendarExtender ID="CalendarExtender8" runat="server" Format="MM/dd/yyyy" TargetControlID="txtCalldate"  CssClass="AjaxCalendar" />
 </td> 
    
    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" SetFocusOnError="true"
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ValidationGroup="vglogform" ControlToValidate="txt2ndCallDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajax:CalendarExtender ID="CalendarExtender7" runat="server" Format="MM/dd/yyyy" TargetControlID="txt2ndCallDate"  CssClass="AjaxCalendar" />

    </tr>
    <tr>
     <td class="calllog_popupTxt">
    State:
   </td>
    <td><asp:TextBox ID="txt2ndState" runat="server" Enabled="false"   CssClass="txtboxDisableClass" /></td>
    <td class="calllog_popupTxt" >Email:</td>
    <td><asp:TextBox ID="txt2ndEmail" runat="server" Width="250px"  /></td>
    <td>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ForeColor="Red" ValidationGroup="vglogform" SetFocusOnError="true" ControlToValidate="txt2ndEmail" ErrorMessage="*This field is required." /><br />
     <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ControlToValidate="txt2ndEmail" runat="server" ValidationGroup="vglogform" SetFocusOnError="true"
                     ValidationExpression="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$"
                      ForeColor="Red"  ErrorMessage="E-mail format is invalid."/> 
    </td>
    </tr>
    </table>
       
        <p> 
        <asp:CheckBox ID="cbxIntrod" runat="server" Text="Introduce yourself and your TA Center Liaison (if they are on the call)." /></p>
        <p>
        Enter  information for anyone other than  the Program Officer and Project Director who  participated in the call.
        </p>
        <p>
            <asp:Button ID="txtNew2ndParticipant" runat="server" CausesValidation="false" 
                CssClass="surveyBtn2" OnClick="OnNew2ndCallParticipant" Text="New Participant" />
        </p>
        <asp:GridView ID="gv2ndParticipant" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
       PageSize="10"  AllowPaging="true" AllowSorting="false"  CssClass="msapTbl" DataKeyNames="ID" DataSourceID="ds2ndParticipant">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="" />
                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="" />
                <asp:BoundField DataField="Role" HeaderText="Role"    SortExpression="" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnEditParticipant" runat="server" CausesValidation="false"
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnEdit2ndParticipant" Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <%--modelpopup--%>
                 <asp:Panel ID="pnl2ndParticipant" runat="server">
    <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult">
                Add/Edit Participant
                <span class="closeit"><asp:LinkButton ID="lbtnClose" runat="server" CausesValidation="false" Text="Close" CssClass="closeit_link" />
                </span>
            </div>
            <div style="height:280px;overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                     <tr>
                    <td class="consult_popupTxt">Name:</td>
                        <td>
                           
                            <asp:TextBox ID="txt2ndParticipantName" runat="server" MaxLength="250" Width="360"></asp:TextBox>
                        </td>
                        <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="Red" ValidationGroup="vg2ndparticipant" ControlToValidate="txt2ndParticipantName" ErrorMessage="*This field is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Title:</td>
                        <td>
                           
                            <asp:TextBox ID="txt2ndParticipantTitle" runat="server" MaxLength="250" Width="360"></asp:TextBox>
                        </td>
                        <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="Red" ControlToValidate="txt2ndParticipantTitle" ValidationGroup="vg2ndparticipant" ErrorMessage="*This field is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td class="consult_popupTxt">Role:</td>
                        <td>
                           
                            <asp:TextBox ID="txt2ndParticipantRole" runat="server" MaxLength="250" Width="360"></asp:TextBox>
                        </td>
                        <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="vg2ndparticipant" runat="server" ForeColor="Red" ControlToValidate="txt2ndParticipantRole" ErrorMessage="*This field is required."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td class="consult_popupTxt" style="vertical-align:top">Note:</td>
                        <td>
                           
                            <asp:TextBox ID="txt2ndParticipantNote" runat="server" TextMode="MultiLine" Height="60" MaxLength="500" Width="360"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td colspan='2' style="padding-top:50px">
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnImgSave" runat="server" CssClass="msapBtn btnSave" ValidationGroup="vg2ndparticipant" Text="Save Record" OnClick="OnSave2ndParticipant" />
                    </td>
                    <td class="style1">
                        <asp:Button ID="btn2ndPClose" runat="server" CausesValidation="false" CssClass="surveyBtn2" Text="Close Window" />
                    </td>
                </tr>
                 <tr>
                    <td colspan='2' style="padding-top:50px">
                    &nbsp;</td>
                </tr>
                </table>
            </div>
        </div>
    
    </asp:Panel>
    <asp:LinkButton ID="lbtn2ndpaticipant" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpext2ndParticipant" runat="server" TargetControlID="lbtn2ndpaticipant"
        PopupControlID="pnl2ndParticipant" DropShadow="true" OkControlID="btn2ndPClose" CancelControlID="btn2ndPClose"
        BackgroundCssClass="magnetMPE" Y="20" />
         <%-- End-modelpopup--%>
        <p><strong>I.&nbsp;Administration</strong></p>
        <p>Has anything occurred since the post award call that could impact grantee ability to implement project as described?</p>
        <asp:TextBox ID="txtHasimpacted"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p>Describe in detail the biggest successes and challenges that the grantee has faced to date.</p>
        <asp:TextBox ID="txtchallengessuccesses"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p><strong>II.&nbsp;Personnel</strong></p>
        <p><asp:CheckBox ID="cbxConfirm" runat="server" Text="Confirm the names and commitment levels of key personnel.  If a position is not yet filled, record the expected commitment." /></p>
        <p>
            <asp:Button ID="btn2ndNewPerson" runat="server" CausesValidation="false" 
                CssClass="surveyBtn2" OnClick="On2ndNewPersonnel" Text="New Key Personnel" />
        </p>
        <asp:GridView ID="gv2ndPersonnel" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
       PageSize="10"  AllowPaging="true" AllowSorting="false"  CssClass="msapTbl" DataKeyNames="ID" DataSourceID="ds2ndKeyPersonnel">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="" />
                <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="" />
                <asp:BoundField DataField="Commitment" HeaderText="Commitment %" DataFormatString="{0:p}"
                    SortExpression="" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnEditPersonnel" runat="server"  CausesValidation="false"
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnEdit2ndPersonnel" Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
         <%--modelpopup--%>
          <asp:Panel ID="pnl2ndPersonnel" runat="server">
    <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult">
                Add/Edit Key Personnel
                <span class="closeit"><asp:LinkButton ID="LinkButton3" runat="server" Text="Close" CausesValidation="false" CssClass="closeit_link" />
                </span>
            </div>
            <div style="height:280px;overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                     <tr>
                    <td class="consult_popupTxt">Name:</td>
                        <td>
                           
                            <asp:TextBox ID="txt2ndPersonnelName" runat="server"  MaxLength="250"  Width="360"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="vgKeyPerson" runat="server" ForeColor="Red" ControlToValidate="txt2ndPersonnelName" SetFocusOnError="true" ErrorMessage="*This field is required."></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Role:</td>
                        <td>
                            <asp:TextBox ID="txt2ndPersonnelRole" runat="server"  MaxLength="250" Width="360"></asp:TextBox>
                        </td>
                        <td><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="vgKeyPerson" runat="server" ForeColor="Red" ControlToValidate="txt2ndPersonnelName" SetFocusOnError="true" ErrorMessage="*This field is required."></asp:RequiredFieldValidator></td>
                    </tr>
                <tr>
                    <td class="consult_popupTxt">Commitment %:</td>
                        <td>
                            <asp:TextBox ID="txt2ndPersonnelCommit" runat="server"  MaxLength="250" Width="360"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Custom"
                            TargetControlID="txt2ndPersonnelCommit" ValidChars="01234567890.">
                        </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                        <asp:CustomValidator ID="CustomValidator1" ValidationGroup="vgKeyPerson" runat="server" ForeColor="Red" ControlToValidate="txt2ndPersonnelName" SetFocusOnError="true" ClientValidationFunction="percentage_vald" ErrorMessage=""></asp:CustomValidator>
                        </td>
                    </tr>
                <tr>
                    <td class="consult_popupTxt" style="vertical-align:top">Note:</td>
                        <td>
                           
                            <asp:TextBox ID="txt2ndPersonnelNote" runat="server" TextMode="MultiLine" 
                                Height="60"  MaxLength="500" Width="360"  ></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                  <tr>
                        <td colspan="3" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave2ndKeyPersonnel" ValidationGroup="vgKeyPerson" runat="server" CssClass="msapBtn btnSave" Text="Save Record" OnClick="OnSave2ndKeyPersonnel" />
                    </td>
                    <td class="style1">
                        <asp:Button ID="btnCancel2ndKeyPersonnel" runat="server" CausesValidation="false" CssClass="surveyBtn2" Text="Close Window" />
                    </td>
                    <td></td>
                </tr>
                  <tr>
                        <td colspan="3" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
    
    </asp:Panel>
    <asp:LinkButton ID="lbtn2ndPersonnel" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpext2ndPersonnel" runat="server" TargetControlID="lbtn2ndPersonnel"
        PopupControlID="pnl2ndPersonnel" DropShadow="true" OkControlID="btnCancel2ndKeyPersonnel" CancelControlID="btnCancel2ndKeyPersonnel"
        BackgroundCssClass="magnetMPE" Y="20" />
         <%-- End-modelpopup--%>
        <p>Have there been any challenges or unexpected  changes to personnel?</p>
        <asp:TextBox ID="txtChallenges"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
         <p><strong>III.&nbsp;Budget</strong></p>
        <p>Has grantee encountered any budget related problems  such as difficulty drawing down funds?</p>
        <asp:TextBox ID="txtBudgetissues"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p>Are there any pending or forthcoming budget  change requests?</p>
        <asp:TextBox ID="txtBudgetchanges"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
         <p><strong>IV.&nbsp;Implementation</strong></p>
        <p>Describe current  student recruitment efforts.</p>
        <asp:TextBox ID="txtStudentRecruitment"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p>What professional development opportunities have been provided to district and school staff? Any feedback?</p>
        <asp:TextBox ID="txtProfDev"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p>How is the grantee leveraging community partnerships and resources,and improving community engagement?</p>
        <asp:TextBox ID="txtPartnerships"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p><strong>V.&nbsp;Evaluation</strong></p>
         <p>How is the grantee evaluating  progress and who is involved  in the evaluation?</p>
        <asp:TextBox ID="txtsiteevaluation"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p>Describe overall grantee progress towards meeting  program  level objectives.  Any significant challenges or changes?</p>
        <asp:TextBox ID="txtPerfObjectives"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <p><strong>Additional Comments</strong></p>
        <asp:TextBox ID="txtAdditionalcomments"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
        <br />
        <br />
        <br />
        <br />
        <p>
            <asp:Button ID="Button1" runat="server" CssClass="msapBtn postbutton" OnClick="OnSave2ndCall" 
                Text="Submit" ValidationGroup="vglogform" />
            <asp:Button ID="Button2" runat="server" CausesValidation="false" 
                CssClass="surveyBtn2" OnClick="OnClose2ndCall" Text="Cancel" />
        </p>
        <br />
        <br />
        <br />
        <br />
    </asp:Panel>

   <%-- ////////end 2nd call //////////--%>

    <asp:Panel ID="pnlPage1" runat="server" Visible="false">
    <table>
    <tr>
    <td class="calllog_popupTxt">PR/Award #:</td>
    <td><asp:TextBox ID="lblPRAward" runat="server"  Enabled="false" ValidationGroup="vglogform" CssClass="txtboxDisableClass" /></td>
    <td class="calllog_popupTxt">Grantee Name:</td>
    <td colspan="2"><asp:TextBox ID="lblGrantee" runat="server"  Enabled="false" ValidationGroup="vglogform" Width="460px" CssClass="txtboxDisableClass" /></td>

    </tr>
    <tr>
    <td class="calllog_popupTxt">Program Officer:</td>
    <td><asp:TextBox ID="lblPofficer" runat="server" Enabled="false" ValidationGroup="vglogform" CssClass="txtboxDisableClass" /> </td>
    <td class="calllog_popupTxt">Date of Call:</td>
    <td ><asp:TextBox ID="txtCalldate" runat="server" ValidationGroup="vglogform"  /></td>
    <td>
<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ForeColor="Red" ValidationGroup="vglogform" ControlToValidate="txtCalldate" SetFocusOnError="true" ErrorMessage="*This field is required."></asp:RequiredFieldValidator><br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"  ValidationGroup="vglogform" SetFocusOnError="true"
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtCalldate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtCalldate"  CssClass="AjaxCalendar" />
    </td>
    </tr>
    <tr>
    <td class="calllog_popupTxt">Project Director:</td>
    <td><asp:TextBox ID="txtPDirctor" runat="server" ValidationGroup="vglogform" /></td>
    <td class="calllog_popupTxt">Phone Number:</td>
     <td><asp:TextBox ID="txtPhone" runat="server"  ValidationGroup="vglogform" /></td>
   <td><asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ForeColor="Red" ValidationGroup="vglogform" SetFocusOnError="true" ControlToValidate="txtPhone" ErrorMessage="*This field is required."></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
   <td class="calllog_popupTxt">State:</td>
    <td><asp:TextBox ID="lblstate" runat="server" Enabled="false"  ValidationGroup="vglogform" CssClass="txtboxDisableClass" /></td>
    <td class="calllog_popupTxt">E-mail:</td>
    <td><asp:TextBox ID="txtEmail" runat="server"  Width="250" />
    </td>
    <td>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ForeColor="Red" ValidationGroup="vglogform" SetFocusOnError="true" ControlToValidate="txtEmail" ErrorMessage="*This field is required."></asp:RequiredFieldValidator><br />
     <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtEmail" runat="server" ValidationGroup="vglogform" SetFocusOnError="true"
                     ValidationExpression="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$"
                      ForeColor="Red"  ErrorMessage="E-mail format is invalid."/> 
    </td>
    </tr>
    </table>
     <p style="text-align:left">
        <asp:Button ID="NewParticipant" runat="server" Text="New Participant" CssClass="surveyBtn2" CausesValidation="false"
             onclick="OnNewParticipant" />
    </p>
    <p><strong>
    Additional Call Participants<br />Enter anyone other than the Program Officer and Project Director who participated in the call.
    </strong></p>
    <asp:GridView ID="gvParticipants" runat="server" DataSourceID="dsParticipants" 
            AutoGenerateColumns="False" DataKeyNames="ID" CssClass="msapTbl"  >
        <Columns>
            <asp:BoundField DataField="Name" SortExpression="" HeaderText="Name" />
            <asp:BoundField DataField="Title" SortExpression="" HeaderText="Title" />
            <asp:BoundField DataField="Role" SortExpression="" HeaderText="Role" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnEditParticipant" runat="server" Text= "Edit" CausesValidation="true" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditParticipant"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
    </asp:GridView>
    <p><strong>
    For the following sections, check off each item as you discuss it with the grantee, and answer any questions related to that item.
    </strong></p>
    <p><strong>I.&nbsp;<u>Introduction</u></strong></p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Introduce yourself as the Program Officer and main point of contact.</p>
    <p>The grantee received a signed Grant Award Notification (GAN), which was mailed on 
        <asp:TextBox ID="txtMailonDate" runat="server"></asp:TextBox>
       <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="vglogform" SetFocusOnError="true"
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtMailonDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajax:CalendarExtender ID="CalendarExtender6" runat="server" Format="MM/dd/yyyy" TargetControlID="txtMailonDate"  CssClass="AjaxCalendar" /></p>
        <asp:RadioButtonList ID="rbtnlistGAN" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" ValidationGroup="vglogform" >
        <asp:ListItem Value="1">Yes</asp:ListItem>
        <asp:ListItem Value="0" Selected="True" >No</asp:ListItem>
        </asp:RadioButtonList>
 
 <p>Inform the applicant that the application was reviewed and recommended for funding at:</p>
 $<asp:TextBox ID="txtFY2013" Text="" runat="server" ValidationGroup="vglogform" />for FY2013<br />
 $<asp:TextBox ID="txtFY2014" Text="" runat="server" ValidationGroup="vglogform" />for FY2014<br />
 $<asp:TextBox ID="txtFY2015" Text="" runat="server" ValidationGroup="vglogform" />for FY2015<br />
 <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom" ValidChars="." TargetControlID="txtFY2013" />
 <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, Custom" ValidChars="." TargetControlID="txtFY2014" />
 <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom" ValidChars="." TargetControlID="txtFY2015" />
 <p><strong>
 II.&nbsp;<u>Specific Award Information</u> <br /><br />
     &nbsp;&nbsp; A. Budget
 </strong></p>
 <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Inform the grantee that the award amounts are based on:
 </p>
 <ul>
 <li>ED’s intent to make awards in a manner generally consistent with estimates for awards, as published in the closing date notice; and<br /><br />
 </li>
 <li>A review of the budget and the deletion of any unallowable, unnecessary, or unallocable costs,
and the adjustment of costs that were not demonstrated to be reasonable.
 </li>
 </ul>
 <p>Signed indirect cost rate agreement:</p>
        <asp:CheckBox ID="chkOnfile"  Text="Is on file" runat="server" ValidationGroup="vglogform" /><br />
        <asp:CheckBox ID="chkExpDate" Text="Date Expected" runat="server" ValidationGroup="vglogform" />
         &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtExpectedDate" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="vglogform" SetFocusOnError="true"
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtExpectedDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajax:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy" TargetControlID="txtExpectedDate"  CssClass="AjaxCalendar" />
<br />
The indirect cost rate proposed in the grant application is <asp:TextBox ID="txtProposedpcnt" runat="server" ValidationGroup="vglogform" />%.<br />
The grant’s approved indirect cost rate is <asp:TextBox ID="txtApprovedpcnt" runat="server" ValidationGroup="vglogform" />%<br /><br />
<ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers, Custom" ValidChars="." TargetControlID="txtProposedpcnt" />
<ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers, Custom" ValidChars="." TargetControlID="txtApprovedpcnt" />
<%--<asp:CheckBox ID="chkIsReview" runat="server"  Text="Review the recommended budget line items with the grantee, and advise as follows:"/>--%>
Review the recommended budget line items with the grantee, and advise as follows:<br /><br />
<b>Year 1:</b><br />
<u>Unallowable costs to be deleted</u><br />
<asp:TextBox ID="txtY1costDeleted"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
<u>Recommended reductions</u><br />
<asp:TextBox ID="txtY1costReduct"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox><br />
<b>Year 2:</b><br />
<u>Unallowable costs to be deleted</u><br />
<asp:TextBox ID="txtY2costDeleted"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform"></asp:TextBox>
<u>Recommended reductions</u><br />
<asp:TextBox ID="txtY2costReduct"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox><br />
<b>Year 3:</b><br />
<u>Unallowable costs to be deleted</u><br />
<asp:TextBox ID="txtY3costDeleted"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox>
<u>Recommended reductions</u><br />
<asp:TextBox ID="txtY3costReduct"  TextMode="MultiLine"  Width="800" Height="80" runat="server" ValidationGroup="vglogform" ></asp:TextBox><br />

<br /><u>Special terms and conditions applicable to this award</u><br />
    <asp:RadioButtonList ID="rbtnlstSpecialterms" runat="server" RepeatColumns="1" ValidationGroup="vglogform" >
    <asp:ListItem Value="1" Selected="True" >There are no special conditions.</asp:ListItem>
    <asp:ListItem Value="2">No draw-down may occur until the final revised budget is approved by the ED Program Officer for this grant. That person will contact the grantee within 30 days of the grant award to explain the budget reductions and to seek clarification on the questions/issues that arose during ED’s budget review. All budget items must be approved as allowable, allocable, and reasonable for the purposes of this grant. As applicable, a grantee may need to submit an updated budget for the approved budget period.</asp:ListItem>
    <asp:ListItem Value="3">Other: Please explain in the text box</asp:ListItem>
    </asp:RadioButtonList>
<asp:TextBox ID="txtstOther" runat="server" Width="800" Height="80" TextMode="MultiLine" ValidationGroup="vglogform" ></asp:TextBox><br />
        <asp:CheckBox ID="chkAccepted" runat="server" ValidationGroup="vglogform"
        Text="Tell the grantee that once the revised materials have been accepted, a formal GAN will be mailed to the grantee. The approximate mailing date will be within two weeks of receipt of all requested materials." /><br />
        <asp:CheckBox ID="chk3yrAward" runat="server"  ValidationGroup="vglogform"
        Text="Tell the grantee that this will be a 3-year award. <b><i>The first performance and budget period will begin October 1, 2013, and will end September 30, 2014</i></b>. Funding for subsequent budget periods is contingent on “substantial progress” made toward meeting the objectives in the approved application and the availability of federal funds."/><br /><br />
        <b>B. School to Be Served</b><br /><br />
        
        Confirm the information for all schools that will be served by the grant.<br />

        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox ID="chkisSchoolchanged" runat="server" text="Are there changes? Yes/No" /> 
        <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If yes, edit the school 
        information below.<br />
        <br />
        <asp:GridView ID="gvSchools" runat="server" AutoGenerateColumns="False" 
            CssClass="msapTbl" DataKeyNames="ID" DataSourceID="dsTCallSchools">
            <Columns>
                <asp:BoundField DataField="SchoolName" HeaderText="School Names" 
                    SortExpression="" />
                <asp:BoundField DataField="TcallSchoolName" HeaderText="School Names Edited" 
                    SortExpression="" />
                <asp:BoundField DataField="SchoolGrade" HeaderText="Grades" SortExpression="" />
                <asp:BoundField DataField="TcallSchoolGrades" HeaderText="Grades Edited" 
                    SortExpression="" />
                <asp:BoundField DataField="MagnetThemes" HeaderText="Themes" 
                    SortExpression="" />
                <asp:BoundField DataField="TcallThemes" HeaderText="Themes Edited" 
                    SortExpression="" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnEditeSchool" runat="server" CausesValidation="true" 
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnEditSchool" Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle Wrap="False" />
        </asp:GridView>
        <br />
        <br />
        <b>III.&nbsp;<u>Program Concerns</u> </b>
        <br />
        <br />
        <i>Ask the Project Director the following questions and note the responses 
        below.</i><br />
        <p>
            Has anything occurred since the original application was submitted that would 
            materially impact your ability to carry out the project as described?
        </p>
        <table>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rbtnlistIsImpact" runat="server" 
                        ValidationGroup="vglogform">
                        <asp:ListItem Value="1">Yes (If yes, please document changes in the text box)</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True" >No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td valign="top">
                    <asp:TextBox ID="txtOccureDes" runat="server" ValidationGroup="vglogform"></asp:TextBox>
                </td>
            </tr>
        </table>
        <p>
            Are there any components or major activities that the grantee believes it cannot 
            conduct as a result of the amount for which the project was approved?
        </p>
        <table>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rbtnlistIsCannotConduct" runat="server" 
                        ValidationGroup="vglogform">
                        <asp:ListItem Value="1">Yes. If yes, document the changes in the text box. Then tell the grantee to identify the specific areas where the scope of the project needs to be adjusted when they submit the revised budget.</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True" >No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td valign="top">
                    <asp:TextBox ID="txtAdjustedDes" runat="server" ValidationGroup="vglogform"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <b>IV.&nbsp; <u>Key Personnel</u></b><br /><br />
        <asp:CheckBox ID="chkPersonnelchange" runat="server" Text="Remind the grantee to let you know if there is a change in the project director, key
personnel, or certifying official (see EDGAR 74.25). (These changes require you to update G5 and
print out the GAN to save the changes. Place a copy of the GAN in the official file and send the
project a copy as well.)" ValidationGroup="vglogform" />
        <br />
        <br />
        <p>
            Are there any changes in key personnel since the appplication was submitted?</p>
        <table>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rbtnlistchangeaftersubmit" runat="server" 
                        ValidationGroup="vglogform" Width="320">
                        <asp:ListItem Value="1">Yes. If yes, please document the changes in the text box. Then inform the grantee to submit resumes of new personnel.</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True" >No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td valign="top">
                    <asp:TextBox ID="txtResume" runat="server" ValidationGroup="vglogform"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <asp:CheckBox ID="chkConfirm" runat="server" 
            Text="Confirm the names, titles, and levels of commitment for key personnel (any position being funded by the grant, including the project director)." 
            ValidationGroup="vglogform" />
        <p>
            <asp:Button ID="btnNewPensonnel" runat="server" CausesValidation="false" 
                CssClass="surveyBtn2" OnClick="OnNewPersonnel" Text="New Key Personnel" />
        </p>
        <asp:GridView ID="gvKeyPersonnel" runat="server" AutoGenerateColumns="False" 
            CssClass="msapTbl" DataKeyNames="ID" DataSourceID="dsKeyPersonnel">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Names" SortExpression="" />
                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="" />
                <asp:BoundField DataField="LevelCommitpcnt" HeaderText="Level of Commitment(%)" DataFormatString="{0:p}"
                    SortExpression="" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnEditPersonnel" runat="server" 
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnEditPersonnel" Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <p>
            Additional Information</p>
        <asp:TextBox ID="txtPersonnelAdditionalInfo" runat="server" Height="80" 
            TextMode="MultiLine" ValidationGroup="vglogform" Width="820"></asp:TextBox>
        <br />
        <p>
            <b>V.&nbsp;<u>Administrative Topics</u></b>
        </p>
        <br />
        <asp:CheckBox ID="chkReadGAN" runat="server" Text="Remind the grantee to read the GAN and other attachments included in the awards packet, if they have not already done so. There are 12 attachments (A, B, C, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enclosure 1, Enclosure 2, Enclosure
4, Enclosure 5, F, N, S, U, and V). Explain each box on the GAN and the information it contains.
" ValidationGroup="vglogform" />
        <br />
        <br />
        These attachments explain payment procedures, reporting requirements, and 
        special provisions that will be in effect for the entire grant period.
        <ol>
            <li>Attachment A: G5</li>
            <li>Attachment B: Performance Reports</li>
            <li>Attachment C: Single Audit Requirements</li>
            <li>*As applicable, Attachment D: Limitation on Indirect Costs Recovery</li>
            <li>E1: Financial Management Requirements</li>
            <li>E2: Memo re Cash Draw Downs</li>
            <li>E4: Memo re Cash Management Policies</li>
            <li>E5: Grant FAQs</li>
            <li>Attachment F: Program Income</li>
            <li>Attachment N: Trafficking in Persons</li>
            <li>*Attachment O: Reporting Executive Compensation Data</li>
            <li>Attachment S: Disclosing Federal Funding in Public Announcements</li>
            <li>Attachment U: Prohibition of Text Messaging and Emailing</li>
            <li>Attachment V: Registration of DUNS, TIN, and CCR</li>
            <li>*Some GANs included Attachment D: Indirect Costs</li>
        </ol>
        *not applicable to MSAP<br />
        <br />
        <table>
            <tr>
                <td colspan="2">
                    Have the grantee review the information on the GAN. Is the information accurate?
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rbtnlistIsReviewGAN" runat="server" 
                        ValidationGroup="vglogform" Width="320">
                        <asp:ListItem Value="1" Selected="True" >Yes</asp:ListItem>
                        <asp:ListItem Value="0">No. If no, document the inaccuracies in the text box.</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td valign="bottom">
                    <asp:TextBox ID="txtInaccuracyOther" runat="server" ValidationGroup="vglogform"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <p>
            <asp:CheckBox ID="chkRegulations" runat="server" 
            Text="Remind the grantee that Program Regulations are found in 34 CFR Part 280. The Education Department General Administrative Regulations (EDGAR) Parts 75, 77, 79, 80, 81, 82, 85 and 86 apply to this grant. 
Advise the grantee that EDGAR can be accessed through ED’s website at <a href='http://ocfo.ed.gov/grntinfo/edgar.htm' target='_blank'>http://ocfo.ed.gov/grntinfo/edgar.htm</a>. These regulations contain information regarding the requirements for financial management, maintenance of records, programmatic changes, budget revisions, and general administrative responsibilities. Advise the grantee to refer to EDGAR and the Program Regulations to see whether prior approval is needed before making any changes in the project or budget."    ValidationGroup="vglogform" />
        </p>
        <p>
            <asp:CheckBox ID="chkCostPrinciples" runat="server" Text="Tell the grantee that cost principles can be found at
<a href='http://www.whitehouse.gov/omb/circulars_default/' target='_blank'>http://www.whitehouse.gov/omb/circulars_default/</a>" ValidationGroup="vglogform" />
        </p>
        <p>
            <asp:CheckBox ID="chkFiscal" runat="server" 
                Text="Review grantee responsibilities for fiscal accountability. Go over basic financial issues, including how
to draw down funds and the importance of keeping proper documentation in the event of an audit (EDGAR 74.26)." 
                ValidationGroup="vglogform" />
        </p>
        <p>
            <asp:CheckBox ID="chk3dayrule" runat="server" 
                Text="Remind the grantee of the 3-day rule for drawing down funds (consistent with the Cash Management Improvement Act): funds must be expended within 3 business days after they are deposited in the grantee’s account. Funds held for more than 3 business days are <b>excess cash</b> and must be returned. If possible, excess cash may be resolved by performing drawdown adjustments." 
                ValidationGroup="vglogform" />
        </p>
        <p>
            <asp:CheckBox ID="chkG5hotline" runat="server" Text=" Tell the grantee about the G5 Hotline phone number—1-888-336-8930. This is the place to call if questions arise about drawing down funds. Remind the grantee that any entity that expends
$500,000 or more in a year in federal awards is required to have an audit done in accordance with
OMB Circular A-133. Refer them to Attachment C of the GAN." />
        </p>
        <br />
        Was the grantee required to have IRB clearance before the grant period began?
        <asp:RadioButtonList ID="rbtnlistisIRBClearance" runat="server" 
            RepeatColumns="2" RepeatDirection="Horizontal" ValidationGroup="vglogform">
            <asp:ListItem Value="1">Yes</asp:ListItem>
            <asp:ListItem Value="0" Selected="True" >No</asp:ListItem>
        </asp:RadioButtonList>
        <br />
        Does the proper IRB human subjects’ approval extend throughout the entire grant 
        period?
        <asp:RadioButtonList ID="rbtnlistisIRBextend" runat="server" RepeatColumns="3" 
            RepeatDirection="Horizontal" ValidationGroup="vglogform">
            <asp:ListItem Value="1">Yes</asp:ListItem>
            <asp:ListItem Value="0" Selected="True" >No</asp:ListItem>
            <asp:ListItem Value="2">N/A</asp:ListItem>
        </asp:RadioButtonList>
        <p>
            <asp:CheckBox ID="chkPublicAnnounce" runat="server" 
                Text="Remind the grantee to include the required direct disclosure language in any publications or public announcements; this which states that federal funding is involved. For public announcements, refer grantees to Attachment S of the GAN and to EDGAR 75.620 for the required statement for publications." 
                ValidationGroup="vglogform" />
        </p>
        <p>
            Tell the grantee that we like to hear if the project receives any special awards 
            or recognition.</p>
        <p>
            <b>VI.&nbsp;<u>Reporting Requirements</u></b>
        </p>
        <p>
            <asp:CheckBox ID="chkAPRs" runat="server" 
                Text="Remind/inform the grantee that they will be required to submit annual performance reports (APRs) in the spring, and ad hoc reports in the fall. Grantees will also complete a final performance report at the end of the grant period. ED will provide more information on these required reports at a later date." 
                ValidationGroup="vglogform" />
        </p>
        <p>
            <b>VII.&nbsp;<u>Summary</u></b>
        </p>
        <p>
            <asp:CheckBox ID="chkSummary" runat="server" 
                Text="Inform the grantee about the MSAP Project Directors’ Meeting on December 5 and 6. Let them know they can use their MSAP funds to attend both Magnet Schools of America conferences and the Project Directors’ Meeting, and that our meeting is mandatory. At a minimum, each grantee is required to bring its project director, superintendent, program evaluator, magnet coordinator, and curriculum specialist. Tell them we will follow up with registration information." 
                ValidationGroup="vglogform" />
        </p>
        <p>
            <asp:Button ID="btnNewfile" runat="server" CausesValidation="false" 
                CssClass="surveyBtn2" onclick="OnNewFile" Text="New File" />
        </p>
        <asp:GridView ID="gvDocs" runat="server" AutoGenerateColumns="False" 
            CssClass="msapTbl" DataKeyNames="ID" DataSourceID="dsDocs">
            <Columns>
                <asp:BoundField DataField="DocName" HeaderText="Name of Document" 
                    SortExpression="" />
                <asp:BoundField DataField="isOnfile" HeaderText="On File" SortExpression="" />
                <asp:BoundField DataField="expDate" DataFormatString="{0:MM/dd/yyyy}" 
                    HeaderText="Date Expected" SortExpression="" />
                <asp:BoundField DataField="RecDate" DataFormatString="{0:MM/dd/yyyy}" 
                    HeaderText="Date Received" SortExpression="" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnEditFile" runat="server" 
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnEditFile" Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle Wrap="False" />
        </asp:GridView>
        <br />
        <p>
            Tell the grantee to call you if they have problems or questions. Make sure the 
            grantee has your contact information—both phone number and e-mail address
        </p>
        <br />
        Name of MSAP Team Member:<asp:DropDownList ID="ddlEDStaff" runat="server" 
            DataSourceID="dsEDStaff" DataTextField="name" DataValueField="name" CssClass="DropDownListCssClass"
            ValidationGroup="vglogform" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Date completed:<asp:TextBox ID="txtCompletedDate" runat="server" 
            ValidationGroup="vglogform" />
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="vglogform" SetFocusOnError="true"
            ControlToValidate="txtCompletedDate" 
            ErrorMessage="Datetime is not well formated." ForeColor="Red" 
            ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator>
        <ajax:CalendarExtender ID="CalendarExtender3" runat="server" 
            CssClass="AjaxCalendar" Format="MM/dd/yyyy" 
            TargetControlID="txtCompletedDate" />
                <br />
        <br />
        <br />
        <table>
        <tr>
        <td class="calllog_popupTxt">Status: </td>
        <td>
            <asp:RadioButtonList ID="rbtnlstSatus" runat="server" RepeatDirection="Horizontal" >
            <asp:ListItem Value="0" Selected="True">Check Pending</asp:ListItem>
            <asp:ListItem Value="1">Check Completed</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        </tr>
        </table>

        <p>
            <b><i>Please print this form and place it in the grant file.</i></b>
        </p>
        <p></p>
        <p>
            <asp:Button ID="btnSave" runat="server" CssClass="msapBtn postbutton" OnClick="OnSaveLog" 
                Text="Submit" ValidationGroup="vglogform" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="false" 
                CssClass="surveyBtn2" OnClick="OnCloseLog" Text="Cancel" />
        </p>
        <br>
        <br>
        <br>
        <br></br>
        <br>
        <br>
        <br>
        <br></br>
        <br>
        <br>
        <br>
        <br></br>
        <%-- participant --%>
        <asp:Panel ID="pnlParticipant" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Participants</div>
                <table>
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtname" runat="server" MaxLength="500" 
                                ValidationGroup="vgParticipant" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" 
                                ValidationGroup="vgParticipant" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Role:
                        </td>
                        <td>
                            <asp:TextBox ID="txtRole" runat="server" MaxLength="250" 
                                ValidationGroup="vgParticipant" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveParticipant" runat="server" CausesValidation="false" 
                                CssClass="msapBtn btnSave" OnClick="OnSaveParticipant" Text="Save Record" 
                                ValidationGroup="vgParticipant" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel1" runat="server" CausesValidation="false" 
                                CssClass="surveyBtn2" Text="Close Window" />
                        </td>
                    </tr>
                      <tr>
                        <td colspan="2" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="lbtnParticipantTrigger" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeParticipant" runat="server" 
            BackgroundCssClass="magnetMPE" CancelControlID="btnCancel1" DropShadow="true" 
            OkControlID="btnCancel1" PopupControlID="pnlParticipant" 
            TargetControlID="lbtnParticipantTrigger" Y="20" />
        <%--school info --%>
        <asp:Panel ID="pnlSchools" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit School Information</div>
                <table>
                    <tr>
                        <td>
                            School Names:
                        </td>
                        <td>
                            <asp:TextBox ID="txtSchoolnames" runat="server" MaxLength="500" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <%--    <tr>
                    <td>
                        Grades Code:
                    </td>
                    <td>
                        <asp:TextBox ID="txtGrade" runat="server" MaxLength="250" Width="470" ></asp:TextBox>
                    </td>
                </tr>--%>
                    <tr>
                        <td>
                            Themes:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTheme" runat="server" MaxLength="250" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            School grades:</td>
                        <td>
                            <asp:CheckBoxList ID="cblGrades" runat="server" CssClass="DotnetTbl change" 
                                RepeatColumns="7">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                      <tr>
                        <td colspan="2" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveSchoolInfo" runat="server" CssClass="msapBtn btnSave" 
                                OnClick="OnSaveSchoolInfo" Text="Save Record" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel2" runat="server" CssClass="surveyBtn2" 
                                Text="Close Window" />
                        </td>
                    </tr>
                      <tr>
                        <td colspan="2" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="lbtnSchlTrigger" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeSchool" runat="server" 
            BackgroundCssClass="magnetMPE" CancelControlID="btnCancel2" DropShadow="true" 
            OkControlID="btnCancel2" PopupControlID="pnlSchools" 
            TargetControlID="lbtnSchlTrigger" Y="20" />
        <%-- Personnel --%>
        <asp:Panel ID="pnlPersonnel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Level of Commitment</div>
                <table>
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPsnlName" runat="server" MaxLength="500" Width="360"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" SetFocusOnError="true"
                                ControlToValidate="txtPsnlName" ErrorMessage="*This field is required." 
                                ForeColor="Red" ValidationGroup="vgKeyPerson1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPsnlTitle" runat="server" MaxLength="500" Width="360"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"  SetFocusOnError="true"
                                ControlToValidate="txtPsnlTitle" ErrorMessage="*This field is required." 
                                ForeColor="Red" ValidationGroup="vgKeyPerson1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Commitment Level(%):
                        </td>
                        <td>
                            <asp:TextBox ID="txtLevel" runat="server" MaxLength="250" Width="360"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" 
                                FilterType="Custom" TargetControlID="txtLevel" ValidChars="01234567890." />
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidator2" runat="server"  SetFocusOnError="true"
                                ClientValidationFunction="percentage_vald1" ErrorMessage="" 
                                ValidationGroup="vgKeyPerson1"></asp:CustomValidator>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="3" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSavePsnl" runat="server" CssClass="msapBtn btnSave" 
                                OnClick="OnSavePersonnel" Text="Save Record" ValidationGroup="vgKeyPerson1" />
                        </td>
                        <td>
                            <asp:Button ID="btnPsnlCancel" runat="server" CausesValidation="false" 
                                CssClass="surveyBtn2" Text="Close Window" />
                        </td>
                    </tr>
                      <tr>
                        <td colspan="3" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="lbtnPsnlTrigger" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpePersonnel" runat="server" 
            BackgroundCssClass="magnetMPE" CancelControlID="btnPsnlCancel" 
            DropShadow="true" OkControlID="btnPsnlCancel" PopupControlID="pnlPersonnel" 
            TargetControlID="lbtnPsnlTrigger" Y="20" />
        <%-- follow up file --%>
        <asp:Panel ID="pnlFollowupFile" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Follow up File</div>
                <table>
                    <tr>
                        <td>
                            Name of Document:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDocName" runat="server" MaxLength="500" 
                                ValidationGroup="vgFollowupfile" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            On File:
                        </td>
                        <td>
                            <asp:CheckBox ID="chkisOnfile" runat="server" 
                                ValidationGroup="vgFollowupfile" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date Expected:
                        </td>
                        <td>
                            <asp:TextBox ID="txtExpDate" runat="server" MaxLength="250" 
                                ValidationGroup="vgFollowupfile" Width="470"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                ControlToValidate="txtExpDate" ErrorMessage="Datetime is not well formated." 
                                ForeColor="Red" SetFocusOnError="true"
                                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator>
                            <ajax:CalendarExtender ID="CalendarExtender4" runat="server" 
                                CssClass="AjaxCalendar" Format="MM/dd/yyyy" TargetControlID="txtExpDate" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date Received:
                        </td>
                        <td>
                            <asp:TextBox ID="txtReceivedDate" runat="server" MaxLength="250" 
                                ValidationGroup="vgFollowupfile" Width="470"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" SetFocusOnError="true"
                                ControlToValidate="txtReceivedDate" 
                                ErrorMessage="Datetime is not well formated." ForeColor="Red" 
                                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator>
                            <ajax:CalendarExtender ID="CalendarExtender5" runat="server" 
                                CssClass="AjaxCalendar" Format="MM/dd/yyyy" TargetControlID="txtReceivedDate" />
                        </td>
                    </tr>
                      <tr>
                        <td colspan="2" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveFile" runat="server" CausesValidation="false" 
                                CssClass="msapBtn btnSave" OnClick="OnSaveFile" Text="Save Record" 
                                ValidationGroup="vgFollowupfile" />
                        </td>
                        <td>
                            <asp:Button ID="btnFileCancel" runat="server" CssClass="surveyBtn2" 
                                Text="Close Window" />
                        </td>
                    </tr>
                      <tr>
                        <td colspan="2" style="padding-top:50px">
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="lbtnFileTrigger" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeFollowupFile" runat="server" 
            BackgroundCssClass="magnetMPE" CancelControlID="btnFileCancel" 
            DropShadow="true" OkControlID="btnFileCancel" PopupControlID="pnlFollowupFile" 
            TargetControlID="lbtnFileTrigger" Y="20" />
     <p></p>
       
    </asp:Panel>
   <div style="text-align: right; margin-top: 20px;">
           <asp:LinkButton ID="LinkButton15" CommandArgument="Overview.aspx" ToolTip="Overview" OnCommand="deleteTempSchools" CssClass="lbtnclik" runat="server">Page 1</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton2" CommandArgument="ContactInfo.aspx" ToolTip="Contact Information" OnCommand="deleteTempSchools" CssClass="lbtnclik" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton13" CommandArgument="ContextualFactors.aspx" ToolTip="School Information" OnCommand="deleteTempSchools" CssClass="lbtnclik" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton14" CommandArgument="Communications.aspx" ToolTip="Communications" OnCommand="deleteTempSchools" CssClass="lbtnclik" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" CommandArgument="MSAPCenterActParticipation.aspx" OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" CommandArgument="MSAPCenterMassCommunication.aspx" OnCommand="deleteTempSchools" CssClass="lbtnclik" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
           <%--&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton7" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton>--%>
           &nbsp;&nbsp;&nbsp;&nbsp;
          Page 7
          </div>

    <asp:HiddenField ID="hfReportperiodID" runat="server"  Value=""/>
    <asp:HiddenField ID="hfReportYear" runat="server"  Value=""/>
    <asp:HiddenField ID="hfCallLogID" runat="server" Value="" />

    <asp:HiddenField ID="hfParticipantID" runat="server"  Value="" />
    <asp:HiddenField ID="hfSchoolID" runat="server"  Value="" />
    <asp:HiddenField ID="hfPersonnelID" runat="server"  Value="" />
    <asp:HiddenField ID="hfFileID" runat="server"  Value="" />
    <asp:HiddenField ID="hfCohortType" runat="server"  Value="" />
    <asp:HiddenField ID="hf2ndCallID" runat="server"  Value="" />
    <asp:HiddenField ID="hf2ndParticipantID" runat="server"  Value="" />
    <asp:HiddenField ID="hf2ndPersonnel" runat="server"  Value="" />
    <asp:HiddenField ID="hfSaved" runat="server"  Value="1" />
   <asp:HiddenField ID="hfisbtnClick" runat="server"  Value="0" />

    <asp:SqlDataSource ID="dsvwTCallMain" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT DISTINCT ID, GranteeName, State, ProgramOfficer,  PRAward, CohortType, isActive, 
        ProjectDirector, Phone, Email, status, ModifiedBy, ModifiedOn, Calldate FROM vwTCallGranteeInfo 
        WHERE (granteeid = @GranteeID AND isActive=1 AND CohortType=@CohortType And ReportPeriodID=@ReportPeriodID) order by calldate desc" 
        CancelSelectOnNullParameter="False">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlgrant" Name="GranteeID" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="rbtnCohortType" Name="CohortType" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="hfReportperiodID" Name="ReportPeriodID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

<asp:SqlDataSource ID="dsParticipants" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" SelectCommand="" 
        CancelSelectOnNullParameter="False" >
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsTCallSchools" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        
        
        SelectCommand="" 
         CancelSelectOnNullParameter="False" >
        <SelectParameters>
            <asp:ControlParameter ControlID="hfCallLogID" Name="Pk_tcallmainid" 
                PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="ddlgrant" Name="granteeid" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsKeyPersonnel" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand=""  
        CancelSelectOnNullParameter="False">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsDocs" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="" 
        CancelSelectOnNullParameter="False" >
    </asp:SqlDataSource>
   
    <asp:SqlDataSource ID="dsGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [MagnetGrantees] WHERE ([CohortType] = @CohortType AND isActive = 1 AND MagnetGrantees.ID not in (39, 40, 41, 69,70,1001, 1002, 1003)) order by GranteeName " >
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohortType" Name="CohortType" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="dsEDStaff" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT id, name, notes FROM TALogEDStaff" 
        CancelSelectOnNullParameter="False"></asp:SqlDataSource>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" Visible="false" />
    <asp:SqlDataSource ID="dsTCalladt" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        
        SelectCommand="SELECT * FROM [vwTCallamendments] WHERE ([GranteeId] = @GranteeId) order by date desc">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlgrant" Name="GranteeId" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="ds2ndParticipant" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="">
    </asp:SqlDataSource>
    <asp:sqlDataSource ID="ds2ndKeyPersonnel" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="">
    </asp:sqlDataSource>
</asp:Content>
