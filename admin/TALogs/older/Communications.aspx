﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="Communications.aspx.cs" Inherits="admin_TALogs_Communications" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"  TagPrefix="cc1" %>
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
    <script type="text/javascript" src="../../js/jquery.timePicker.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
     <script src="../../js/jquery.maskedinput.js" type="text/javascript"></script> 
    <link href="../../css/calendar.css" rel="stylesheet" type="text/css" />
    <link href="../../css/timePicker.css" rel="Stylesheet" type="text/css" />
     <script type="text/javascript">
   document.onkeydown = function(e) {
      // keycode for F5 function
      if (e.keyCode === 116) {
        return false;
      }
    };


         $(function () {
             $("#ctl00_ContentPlaceHolder1_fvCommlog_Role_otherTextBox").hide();

             if($("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlTAComm] option:selected").val()=="")
                 $("#ctl00_ContentPlaceHolder1_fvCommlog_TAComm_otherTextBox").hide();

             $("#ctl00_ContentPlaceHolder1_fvCommlog_DataCollectionComm_otherTextBox").hide();
             $("#ctl00_ContentPlaceHolder1_fvCommlog_RequestStatus_otherTextBox").hide();
             $("#ctl00_ContentPlaceHolder1_fvCommlog_ContactType_otherTextBox").hide();
             $("#ctl00_ContentPlaceHolder1_fvCommlog_ReportComm_otherTextBox").hide();

             if ($("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlGrantsManagement] option:selected").val() == "")
                 $("#ctl00_ContentPlaceHolder1_fvCommlog_txtGrantsManagement").hide();

             $("#ctl00_ContentPlaceHolder1_fvCommlog_commTimeTextBox").timePicker({
                 //startTime: "00.00",  // Using string. Can take string or Date object.
                 //endTime: new Date(0, 0, 0, 15, 30, 0),  // Using Date object.
                 //endTime: "23.59",
                 //show24Hours: false,
                 //separator: '.',
                 step: 10
             });
             
             $("#ctl00_ContentPlaceHolder1_fvCommlog_CompleteTimeTextBox").timePicker({
                 //startTime: "00.00",  // Using string. Can take string or Date object.
                 //endTime: new Date(0, 0, 0, 15, 30, 0),  // Using Date object.
                 //endTime: "23.59",
                 //show24Hours: false,
                 //separator: '.',
                 step: 10
             });

             $("#ctl00_ContentPlaceHolder1_txtTime").timePicker({
                 //startTime: "00.00",  // Using string. Can take string or Date object.
                 //endTime: new Date(0, 0, 0, 15, 30, 0),  // Using Date object.
                 //endTime: "23.59",
                 //show24Hours: false,
                 //separator: '.',
                 step: 10
             });
             //role
             $(".Rolechange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlRole] option:selected").val();
                 if (value == "7") {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_Role_otherTextBox").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_Role_otherTextBox").hide();
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_Role_otherTextBox").val('');
                 }
             });

             //TA communication
             $(".ddlTACommchange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlTAComm] option:selected").val();
                 if (value != "") {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_TAComm_otherTextBox").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_TAComm_otherTextBox").hide();
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_TAComm_otherTextBox").val('');
                 }
             });

             //Grants Management
             $(".ddlGrantsManagementchange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlGrantsManagement] option:selected").val();
                 if (value != "") {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_txtGrantsManagement").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_txtGrantsManagement").hide();
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_txtGrantsManagement").val('');
                 }
             });

             //Data Collection Communication
             $(".ddlDataCollectionCommchange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlDataCollectionComm] option:selected").val();
                 if (value == "Other data collection activity") {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_DataCollectionComm_otherTextBox").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_DataCollectionComm_otherTextBox").hide();
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_DataCollectionComm_otherTextBox").val('');
                 }
             });


             //Request status
             $(".ddlRequestStatuschange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlRequestStatus] option:selected").val();
                 if (value == "Other") {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_RequestStatus_otherTextBox").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_RequestStatus_otherTextBox").hide();
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_RequestStatus_otherTextBox").val('');
                 }
             });

             //Communication method
             $(".ddlcontacttypechange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlcontacttype] option:selected").val();
                 if (value == "Other") {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_ContactType_otherTextBox").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_ContactType_otherTextBox").hide();
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_ContactType_otherTextBox").val('');
                 }
             });

             //Reporting communication
             $(".ddlReportCommchange").change(function () {
                 var value = $("select[name=ctl00$ContentPlaceHolder1$fvCommlog$ddlReportComm] option:selected").val();
                 if (value == "Other") {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_ReportComm_otherTextBox").show();
                 }
                 else {
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_ReportComm_otherTextBox").hide();
                     $("#ctl00_ContentPlaceHolder1_fvCommlog_ReportComm_otherTextBox").val('');
                 }
             });

         });

         function FormatPhoneNumber(elementRef) {
             //5754578787, it should be 575-457-8787

             var elementValue = elementRef.value.trim();

             if (elementValue.length == 0)
                 return;

             // Remove all "(", ")", "-", and spaces...
             elementValue = elementValue.replace(/\(/g, '');
             elementValue = elementValue.replace(/\)/g, '');
             elementValue = elementValue.replace(/\-/g, '');
             elementValue = elementValue.replace(/\s+/g, '')

             if (elementValue.length < 10) {
                 alert('The phone number needs 10 characters');
                 elementRef.select();
                 elementRef.focus();
                 return;
             }

             elementRef.value = ('(' + elementValue.substr(0, 3) + ') ' + elementValue.substr(3, 3) + '-' + elementValue.substr(6, 4));

         }

         $(document).ready(function () {
             $("#<%= PhoneTextBox.ClientID %>").blur(function () {
                 $("#<%= PhoneTextBox.ClientID %>").mask("(999) 999-9999? x99999");
             });
         });

//         jQuery(function ($) {
//             $("#<%= PhoneTextBox.ClientID %>").mask("(999) 999-9999? x99999");
//         });

    </script>
    <style type="text/css">
    .text
    {
       /* font size, line height, face */
        font: Tahoma, Arial, Verdana, sans-serif, "Trebuchet MS";
        font-weight:bold;
    }
</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

     <h1>
        Communication Management 
    </h1>
    <asp:RadioButtonList ID="rbtnCohortType" runat="server" RepeatColumns="2" 
        RepeatDirection="Horizontal" AutoPostBack="true" onselectedindexchanged="rbtnCohortType_SelectedIndexChanged" >
        <asp:ListItem Value="1" >Cohort 2010</asp:ListItem>
        <asp:ListItem Value="2" Selected="True">Cohort 2013</asp:ListItem> 
    </asp:RadioButtonList>
   
    <asp:DropDownList ID="ddlGrantees" runat="server" DataSourceID="dsGrantee"  AppendDataBoundItems="true"
        DataTextField="GranteeName" DataValueField="id" AutoPostBack="true" CssClass="DropDownListCssClass"
        onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
         <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>
    <br /><br />
    <div class="titles">Communications Log</div>
  <div class="tab_area">
     
    	<ul class="tabs">
            <%--<li><asp:LinkButton ID="LinkButton3" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton></li>--%>
             <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
            <li class="tab_active">Page 4</li>
            <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContextualFactors.aspx" ToolTip="TA communication" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton9" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton10" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
  
  <asp:Panel ID="pnlCommunications" runat="server">
   <p style="text-align:left">
        <asp:Button ID="NewComm" runat="server" Text="New Communication" CssClass="surveyBtn2" OnClick="OnNewComm" />
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnShowComm" runat="server" Text="Show All" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAllComm"  />
    </p>
    <asp:GridView ID="gdvComm" runat="server" AllowPaging="true" AllowSorting="false" DataSourceID="dsCommgv"
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"  >
        <Columns>
            <asp:BoundField DataField="item#" SortExpression="" HeaderText="Item #" />
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
            <asp:BoundField DataField="PsnCommWith" SortExpression="" HeaderText="Caller" />
            <asp:BoundField DataField="RequestStatus" SortExpression="" HeaderText="Request status" />
            <asp:BoundField DataField="MSAPStaffMember" SortExpression="" HeaderText="Staff member" />
            <asp:BoundField DataField="ContactType" SortExpression="" HeaderText="Contact method" />
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
             <asp:TemplateField>
            <HeaderTemplate>Follow up status</HeaderTemplate>
                <ItemTemplate>
                <asp:Label ID="LblhasFollowup" runat="server" Text='<%# hasFollowup(Eval("ID")) %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditComm"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteComm" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
        <HeaderStyle Wrap="False" />
        
    </asp:GridView>
    </asp:Panel>
      <br/>
     <br /><br />
    <hr />
    <table>
     <tr>
     <td class="consult_popupTxt"><asp:literal runat="server" ID="ltlItemNo" Text="Item number:" /></td>
      <td>   <asp:DropDownList ID="ddlComm" runat="server" AutoPostBack="True" AppendDataBoundItems="true"
               DataSourceID="dsCommgv" DataTextField="item#" DataValueField="id" CssClass="DropDownListCssClass"
          onselectedindexchanged="ddlComm_SelectedIndexChanged" >
               <asp:ListItem Value="">Select one</asp:ListItem>
           </asp:DropDownList>
      </td>  
     </tr>
     </table>
  <asp:Panel ID ="pnlFollowupUpload" runat="server" Visible="false">
       <h3>Follow up details</h3>
       <p style="text-align:left">
        <asp:Button ID="btnNewFollowup" runat="server" Text="New Follow up" OnClick="AddNewFollowup" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" />
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnShowAllfup" runat="server" Text="Show All" Visible="false" 
               CausesValidation="false" CssClass="surveyBtn2" 
               onclick="showAllfup"  />
    </p>
    <asp:GridView ID="gvFollowup" runat="server" DataSourceID="dsFollowup" 
          AutoGenerateColumns="False" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
         
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
            <asp:BoundField DataField="Time" SortExpression="" HeaderText="Time" DataFormatString="{0:t}" />
            <asp:BoundField DataField="StaffMember" SortExpression="" HeaderText="Staff member" />
            <asp:BoundField DataField="FollowupPerson" SortExpression="" HeaderText="Follow up person" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="Status" SortExpression="" HeaderText="Status" Visible="false" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnFollowup" runat="server" Text="Edit" CausesValidation="true" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnEditFollowup"></asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDeleteFollowup" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
    <hr />
        <h3>Upload file</h3>
    <p style="text-align:left">

        <asp:FileUpload ID="FileUploadComm" runat="server" Visible="false" />&nbsp;&nbsp;
          <asp:Button ID="btnSaveUpload" runat="server" Text="Save & Upload" OnClick="SaveUpload" Visible="false"
               CausesValidation="false" CssClass="surveyBtn2" />&nbsp;&nbsp;&nbsp;&nbsp;
      
    </p>
       <table runat="server" id="tblDes" Visible="false">
       <tr>
       <td class="consult_popupTxt">Document content:</td>
       <td>
       <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Width="650px" Height="30px" ></asp:TextBox>
       </td>
       </tr>
       </table>
        
       
        <asp:GridView ID="gvFileUpload" runat="server" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" CssClass="msapTbl" 
             DataSourceID="dsUpload" DataKeyNames="ID" PageSize="10">
            <Columns>
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="" DataFormatString="{0:MM/dd/yyyy}" />
                <asp:TemplateField>
                <HeaderTemplate>Uploaded file</HeaderTemplate>
                <ItemTemplate>
                <a href='<%# Eval("FileURL") %>' target="_blank"><%# Eval("FileName")%></a>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Description" HeaderText="Document Content" SortExpression="" />

                <asp:BoundField DataField="CreatedBy" HeaderText="Created by" SortExpression="" />
                <asp:BoundField DataField="CreatedOn" DataFormatString="{0:MM/dd/yyyy}" 
                    HeaderText="Created on" HtmlEncode="false" SortExpression="" />
                 <asp:TemplateField>
                    <ItemTemplate>
                 <%--      <asp:LinkButton ID="lbtnEditUpload" runat="server" CausesValidation="false" 
                            CommandArgument='<%# Eval("id") %>' OnClick="OnEditUpload" Text="Edit"></asp:LinkButton>--%>
                          <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDeleteUploadFile" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle Wrap="False" />
        </asp:GridView>

    </asp:Panel>
        

        <asp:Panel ID="pnlEditComm" runat="server" Visible ="false">

    <div class="mpeDivConsult"  style="width:830px">
            <div class="mpeDivHeaderConsult" style="width:815px">
                Add/Edit Communication log
                <span class="closeit"><asp:LinkButton ID="lbtnClose" runat="server" Text="Close" CssClass="closeit_link" OnClick="CloseFormviewPan"  />
                </span>
            </div>

      <div style="height:750px;overflow: auto;">  
     <table style="padding:0px 10px 10px 10px; width: 780px;">
     <tr>
     <td class="consult_popupTxt">Item Number:</td>
     <td><asp:Literal ID="ltlItemNumber" runat="server" /></td>
         
     </tr>
    <tr>
        <td class="consult_popupTxt">Grantee name:</td>
        <td >
		<asp:TextBox ID="txtGranteeName" runat="server" Enabled="false" Width="500px" CssClass="txtboxDisableClass"/>
		<asp:Label ID="PK_GenInfoIDLabel" runat="server"  visible="false" />
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Contact method:</td>
        <td >
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
              <asp:DropDownList ID="ddlcontacttype" runat="server" AutoPostBack="true" 
                    CssClass="ddlcontacttypechange DropDownListCssClass"
                    onselectedindexchanged="ddlcontacttype_SelectedIndexChanged">
                        <asp:ListItem Value="">Select one</asp:ListItem>
                        <asp:ListItem>Phone</asp:ListItem>
                        <asp:ListItem>E-mail</asp:ListItem>
                        <asp:ListItem>Online TA request</asp:ListItem>
                        <asp:ListItem>Magnet Connector post</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
          </asp:DropDownList>
            <asp:TextBox ID="ContactType_otherTextBox" runat="server" Visible="false" ></asp:TextBox>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
	 <tr>
        <td class="consult_popupTxt">Date:</td>
        <td >
		 <span><asp:TextBox ID="DateTextBox" runat="server"  />
		 <asp:RegularExpressionValidator ID="RegExpVldDate" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="DateTextBox" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="cldextDate" runat="server" Format="MM/dd/yyyy" TargetControlID="DateTextBox"  CssClass="AjaxCalendar" />
		</span>
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">TA request fulfillment date:</td>
        <td>
         <asp:TextBox ID="txtFulfillDate" runat="server"  />
          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtFulfillDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="MM/dd/yyyy" TargetControlID="txtFulfillDate"  CssClass="AjaxCalendar" />
        </td>
    </tr>
    </table>
     <table style="padding:00px 10px 10px 10px;">
    <tr>
        <td class="consult_popupTxt">Time of communication:</td>

        <td style="padding-left:117px">
        
          <cc2:timeselector ID="TimeSelector1" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                                    runat="server" MinuteIncrement="30" /></td>
       <td class="consult_popupTxt"> Completed time:</td>
        <td>
        <cc2:timeselector ID="TimeSelector2" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                                    runat="server" MinuteIncrement="30" />
        </td>
    </tr>
    </table>
     <table style="padding:0px 10px 10px 10px;">
	<tr>
        <td class="consult_popupTxt"><asp:Label ID="lblstaffmember" runat="server"  /></td>
        <td>
		 <asp:TextBox ID="txtstafffname" runat="server"  Enabled="false" CssClass="txtboxDisableClass"/>
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Role:</td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <span>
		 <asp:DropDownList ID="ddlRole" runat="server" DataSourceID="dsRole" AutoPostBack="true"
                CssClass="Rolechange DropDownListCssClass"  DataTextField="rolename" 
                DataValueField="id" onselectedindexchanged="ddlRole_SelectedIndexChanged" 
                    ondatabound="ddlRole_DataBound" >
<%--         <asp:ListItem Value="">Please select</asp:ListItem>--%>
         </asp:DropDownList>
		</span>
		<span>
		<asp:TextBox ID="Role_otherTextBox" runat="server" Visible="false" />
		</span>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
	 <tr>
        <td class="consult_popupTxt">Caller:</td>
        <td>
		 <asp:TextBox ID="PsnCommWithTextBox" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">E-mail address:</td>
        <td>
		 <asp:TextBox ID="EmailTextBox" runat="server"  />
		  <asp:RegularExpressionValidator ID="RegularExpressionValidator19" ControlToValidate="EmailTextBox" runat="server"
            ValidationExpression="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$"
            ForeColor="Red"  ErrorMessage="E-mail format is invalid."/>
        </td>
    </tr>
	 <tr>
        <td class="consult_popupTxt">Phone:</td>
        <td>
		<asp:TextBox ID="PhoneTextBox" runat="server"   MaxLength="20" />
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">General communication:</td>
        <td>
			  <asp:DropDownList ID="ddlGeneralComm" runat="server" CssClass="DropDownListCssClass" >
                        <asp:ListItem Value="">Select one</asp:ListItem>
                        <asp:ListItem>Regular update call</asp:ListItem>
                        <asp:ListItem>Grantee interview</asp:ListItem>
                        <asp:ListItem>Key personnel change</asp:ListItem>
          </asp:DropDownList>
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">TA communication:</td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>
            	<span>
		 <asp:DropDownList ID="ddlTAComm" runat="server"  CssClass="ddlTACommchange DropDownListCssClass"  AutoPostBack="true"
                onselectedindexchanged="ddlTAComm_SelectedIndexChanged">
             <asp:ListItem Value="">Select one</asp:ListItem>
                <asp:ListItem>Consultation with expert</asp:ListItem>
              <asp:ListItem>MSAP Center event—Accessing resources</asp:ListItem>
              <asp:ListItem>MSAP Center event—Content</asp:ListItem>
              <asp:ListItem>MSAP Center event—Logging in</asp:ListItem>
              <asp:ListItem>MSAP Center event—Registration</asp:ListItem>
               <asp:ListItem>MSAP Center event—Other</asp:ListItem>
              <asp:ListItem>Project directors meeting—Logistics</asp:ListItem>
              <asp:ListItem>Project directors meeting—Registration</asp:ListItem>
             <asp:ListItem>Project directors meeting—Sessions</asp:ListItem>
             <asp:ListItem>TA request—Community partnerships</asp:ListItem>
             <asp:ListItem>TA request—Curriculum and theme integration</asp:ListItem>
             <asp:ListItem>TA request—Family engagement</asp:ListItem>
             <asp:ListItem>TA request—Marketing and recruitment</asp:ListItem>
             <asp:ListItem>TA request—Sustainability</asp:ListItem>
             <asp:ListItem>TA request—Other</asp:ListItem>
             <asp:ListItem>Website assistance—Logging in</asp:ListItem>
             <asp:ListItem>Website assistance—New staff access</asp:ListItem>
             <asp:ListItem>Website assistance—Password request</asp:ListItem>
             <asp:ListItem>Website assistance—Other </asp:ListItem>
             <asp:ListItem>Other request</asp:ListItem>

          </asp:DropDownList>
		  </span>
		  <asp:TextBox ID="TAComm_otherTextBox" runat="server" Visible="false" />
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Data collection communication:</td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>
            		<span>
		 <asp:DropDownList ID="ddlDataCollectionComm" runat="server" AutoPostBack="true"  
                CssClass="ddlDataCollectionCommchange DropDownListCssClass"
                onselectedindexchanged="ddlDataCollectionComm_SelectedIndexChanged">
             <asp:ListItem Value="">Select one</asp:ListItem>
             <asp:ListItem>Needs assessment</asp:ListItem>
             <asp:ListItem>Consultant assessment</asp:ListItem>
             <asp:ListItem>TA evaluation</asp:ListItem>
			 <asp:ListItem>Other data collection activity</asp:ListItem>
          </asp:DropDownList>
		  </span>
		  <asp:TextBox ID="DataCollectionComm_otherTextBox" runat="server" Visible="false"  />
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">Reporting communication:</td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
            <span>
		 <asp:DropDownList ID="ddlReportComm" runat="server" AutoPostBack="true" 
                CssClass="ddlReportCommchange DropDownListCssClass"
                onselectedindexchanged="ddlReportComm_SelectedIndexChanged">
         <asp:ListItem Value="">Select one</asp:ListItem>
             <asp:ListItem>APR reporting</asp:ListItem>
             <asp:ListItem>Ad hoc reporting</asp:ListItem>
			 <asp:ListItem>MAPS questions</asp:ListItem>
			 <asp:ListItem>Other</asp:ListItem>
          </asp:DropDownList>
		  </span>
            <asp:TextBox ID="ReportComm_otherTextBox" runat="server" Visible="false"></asp:TextBox>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Other communication:</td>
        <td>
		 <asp:TextBox ID="OtherCommTextBox" runat="server"  />
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">Description of communication/request:</td>
        <td>
		 <asp:TextBox ID="CommDescriptionTextBox" runat="server" CssClass="text"  TextMode="MultiLine" Width="390" />
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Actions taken:</td>
        <td>
		<asp:TextBox ID="ActionTakenTextBox" runat="server" CssClass="text" TextMode="MultiLine" Width="390" />
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">Description of follow up required:</td>
        <td>
		 <asp:TextBox ID="RequiredDesTextBox" runat="server" CssClass="text"  TextMode="MultiLine" Width="390" />
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt"><asp:Label ID="lblCntrstaffmember" runat="server" /></td>
        <td>
         <asp:DropDownList ID="ddlfollowupmember" runat="server" CssClass="DropDownListCssClass" AppendDataBoundItems="true" >
        <asp:ListItem Value="">Select one</asp:ListItem>
        </asp:DropDownList>
        </td>
    </tr>
                
    <tr>
        <td class="consult_popupTxt">
            Request status:</td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
               <span>
            <asp:DropDownList ID="ddlRequestStatus" runat="server" AutoPostBack="true" 
                CssClass="ddlRequestStatuschange DropDownListCssClass"
                onselectedindexchanged="ddlRequestStatus_SelectedIndexChanged" >
                <asp:ListItem Value="">Select one</asp:ListItem>
                <asp:ListItem>Gathering additional information</asp:ListItem>
                <asp:ListItem>Awaiting response from ED</asp:ListItem>
                <asp:ListItem>Awaiting response from grantee</asp:ListItem>
                <asp:ListItem>Referred to consultant/subject matter expert</asp:ListItem>
                <asp:ListItem>Resolved by MSAP Center</asp:ListItem>
                <asp:ListItem>Resolved by MSAP Team</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>
            </span><span>
            <asp:TextBox ID="RequestStatus_otherTextBox" runat="server" Visible="false"/>
            </span>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
	
    <tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlCommStatus" runat="server" CssClass="DropDownListCssClass"  SelectedValue='<%# Bind("Status") %>'>
         <asp:ListItem Value="">Select one</asp:ListItem>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem Selected="True">Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>
   <%-- <tr>
        <td class="consult_popupTxt">Evaluation feedback request:</td>
        <td>
        <asp:TextBox ID="txtfbRequest" runat="server"  />
        </td>
    </tr>
	<tr>
        <td class="consult_popupTxt">Evaluation feedback received:</td>
        <td>
        <asp:TextBox ID="txtfbReceived" runat="server"  />
        </td>
    </tr>
    <tr>
        <td class="consult_popupTxt">Result of follow up:</td>
        <td>
        <asp:TextBox ID="txtResault" runat="server"  />
        </td>
    </tr>--%>
    <tr><td colspan="2" style="padding-top:50px;"></td></tr>
   
    <tr>
        <td>
            <asp:Button ID="btnSaveComm" runat="server" CssClass="surveyBtn2" Text="Save Record" CausesValidation="True" OnClick="btnSaveComm_Click" />
        </td>
        <td class="style1">
            <asp:Button ID="btnImgClose" runat="server" CssClass="surveyBtn2"  CausesValidation="False" OnClick="CloseFormviewPan" Text="Close Window" />
        </td>
    </tr>
</table>
      </div>
        </div>
    
    </asp:Panel>

       <asp:Panel ID="pnlEditFollowup" runat="server" Visible="false">
        <div class="mpeDivConsult" style="width: 630px;" >
            <div class="mpeDivHeaderConsult" style="width: 615px;" >
                Add/Edit Follow up
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" Text="Close" CssClass="closeit_link" OnClick="CloseEditPan" />
                </span>
           
            </div>
            <div style="height:750px;overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                  
                    <tr>
                    <td class="consult_popupTxt">Date:</td>
                        <td>
                            <asp:TextBox ID="txtDate" runat="server" MaxLength="250" Width="390"></asp:TextBox>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="txtDate" 
                ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate"  CssClass="AjaxCalendar" />
                        </td>
                    </tr>
                     <tr>
                    <td class="consult_popupTxt">Time:</td>
                        <td>
                           <cc2:timeselector ID="TimeSelector3" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                                    runat="server" MinuteIncrement="30" />
                            <%--<asp:TextBox ID="txtTime" runat="server" MaxLength="250" Width="390"></asp:TextBox>--%>
                            
                        </td>
                    </tr>
                     <tr>
                    <td class="consult_popupTxt">Staff member:</td>
                        <td>
                           
                            <asp:TextBox ID="txtStaffmember" runat="server" MaxLength="250" Width="390"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Follow up person:</td>
                        <td>
                           
                            <asp:TextBox ID="txtFollowupPerson" runat="server" MaxLength="250" Width="390"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Notes:</td>
                        <td>
                            <cc1:Editor ID="edtAbsContent" runat="server" Width="495" Height="100"  JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000" />

                        </td>
                    </tr>
                       <tr>
                     <td class="consult_popupTxt">Result of follow up:</td>
                        <td>
                            <cc1:Editor ID="edtFollowupResult" runat="server" Width="495" Height="100"  JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000" />

                        </td>
                    </tr>
<tr runat="server" visible="false">
    <td class="consult_popupTxt">Status:</td>
    <td>
    <asp:DropDownList ID="ddlfupStatus" runat="server" CssClass="DropDownListCssClass"  SelectedValue='<%# Bind("Status") %>'>
        <asp:ListItem Value="">Select one</asp:ListItem>
         <asp:ListItem>Cancelled due to a duplicate entry</asp:ListItem>
         <asp:ListItem>Cancelled item due to error</asp:ListItem>
         <asp:ListItem>Cancelled item because the selection is not applicable</asp:ListItem>
         <asp:ListItem Selected="True">Edit Record</asp:ListItem>
         <asp:ListItem>Hide/Gray out record</asp:ListItem>
         </asp:DropDownList>
    </td>
    </tr>
      <tr><td colspan="2" style="padding-top:50px;"></td></tr>

                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="surveyBtn2" Text="Save Record" OnClick="OnSaveFollowup" />
                    </td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="surveyBtn2" Text="Close Window" OnClick="CloseEditPan"/>
                    </td>
                </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
   <%-- <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajaxToolkit:ModalPopupExtender ID="mpeContentWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="pnlEditFollowup" DropShadow="true" OkControlID="btnClose" CancelControlID="btnClose"
        BackgroundCssClass="magnetMPE" Y="20" />--%>

          
   

        <div style="text-align: right; margin-top: 20px;">
     
           <asp:LinkButton ID="LinkButton15" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton14" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton13" PostBackUrl="ContextualFactors.aspx" ToolTip="TA communication" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           Page 4&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp; <asp:LinkButton ID="LinkButton7" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
         <%-- &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton8" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton>--%>
          
        </div>
        <asp:HiddenField ID="hfddlRoleID" runat="server" />
        <asp:HiddenField ID="hfGranteeID" runat="server" />
        <asp:HiddenField ID="hfCommID" runat="server" />
        <asp:HiddenField ID="hfEditFollowupRcdID" runat="server" />
        <asp:HiddenField ID="hfisCenterstaff" runat="server" />
        <asp:HiddenField ID="hfTACommReqOther" runat="server" />
        <asp:HiddenField ID="hfTACommTACommCntEvtother" runat="server" />
        <asp:HiddenField ID="hfTACommWebAssOther" runat="server" />
        <asp:HiddenField ID="hfTACommOtherReq" runat="server" />

        <asp:SqlDataSource ID="dsGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 

        SelectCommand="SELECT ID, GranteeName FROM [MagnetGrantees] WHERE ([CohortType] = @CohortType AND isActive = 1 AND MagnetGrantees.ID not in (39, 40, 41, 69,70,1001, 1002, 1003)) order by GranteeName " >
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohortType" Name="CohortType" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsRole" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [rolename], [id] FROM [TALogRoles]">
        </asp:SqlDataSource>

<asp:SqlDataSource ID="dsComm" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT ROW_NUMBER() OVER (order by date desc) as item#, id, PK_GenInfoID, ContactType, ContactType_other, Date, commTime, completetime, MSAPStaffMember, isMSAPSCentertaffMember, PsnCommWith, Email, Phone, role_id, Role_other,
         CommType, TAComm, TAComm_other, DataCollectionComm, DataCollectionComm_other, ReportComm, ReportComm_other, GrantsManagement, KeyPersonenelChange, OtherComm, 
         CommDescription, ActionTaken, RequiredDes, FollowupStaff, isCenterFollowupStaff, RequestStatus, RequestStatus_other, FulfillDate, FeedbackRequest,GrantsManagement_TextBox, 
         FeedbackReceived, Result, PK_MassCommID, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn FROM TALogCommunications WHERE (PK_GenInfoID = @PK_GenInfoID ) " 
        
        UpdateCommand="UPDATE TALogCommunications SET PK_GenInfoID = @PK_GenInfoID, ContactType = @ContactType, Date = @Date, commTime = @commTime, completetime=@completetime,
        MSAPStaffMember = @MSAPStaffMember, PsnCommWith = @PsnCommWith, Email = @Email, Phone = @Phone, role_id = @role_id, CommType = @CommType, 
        TAComm = @TAComm, DataCollectionComm = @DataCollectionComm, ReportComm = @ReportComm, GrantsManagement = @GrantsManagement, KeyPersonenelChange = @KeyPersonenelChange, 
        OtherComm = @OtherComm, CommDescription = @CommDescription, ActionTaken = @ActionTaken, RequiredDes = @RequiredDes, FollowupStaff = @FollowupStaff, 
        RequestStatus = @RequestStatus, FulfillDate = @FulfillDate, FeedbackReceived = @FeedbackReceived, Result = @Result, 
        CreatedBy = @CreatedBy, CreatedOn = @CreatedOn, ModifiedBy = @ModifiedBy, ModifiedOn = @ModifiedOn, FeedbackRequest = @FeedbackRequest, 
        ContactType_other = @ContactType_other, Role_other = @Role_other, TAComm_other = @TAComm_other, DataCollectionComm_other = @DataCollectionComm_other, 
        ReportComm_other = @ReportComm_other, RequestStatus_other = @RequestStatus_other, isMSAPSCentertaffMember = @isMSAPSCentertaffMember, 
        isCenterFollowupStaff = @isCenterFollowupStaff, GrantsManagement_TextBox=@GrantsManagement_TextBox WHERE (id = @theID)" 
        DeleteCommand="DELETE FROM TALogCommunications WHERE (id = @theID)" 
        
        
    InsertCommand="INSERT INTO TALogCommunications(PK_GenInfoID, ContactType, Date, commTime,completetime, MSAPStaffMember, PsnCommWith, Email, Phone, role_id, CommType, 
    TAComm, DataCollectionComm, ReportComm, GrantsManagement, KeyPersonenelChange, OtherComm, CommDescription, ActionTaken, RequiredDes, FollowupStaff, 
    RequestStatus, FulfillDate, FeedbackRequest, FeedbackReceived, Result, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, ContactType_other, 
    Role_other, TAComm_other, DataCollectionComm_other, ReportComm_other, RequestStatus_other, isMSAPSCentertaffMember, isCenterFollowupStaff, GrantsManagement_TextBox) 
    VALUES (@PK_GenInfoID, @ContactType, @Date, @commTime, @completetime, @MSAPStaffMember, @PsnCommWith, @Email, @Phone, @role_id, @CommType, @TAComm, 
    @DataCollectionComm, @ReportComm, @GrantsManagement, @KeyPersonenelChange, @OtherComm, @CommDescription, @ActionTaken, @RequiredDes, @FollowupStaff, 
    @RequestStatus, @FulfillDate, @FeedbackRequest, @FeedbackReceived, @Result, @CreatedBy, @CreatedOn, @ModifiedBy, @ModifiedOn, @ContactType_other, @Role_other, 
    @TAComm_other, @DataCollectionComm_other, @ReportComm_other, @RequestStatus_other, @isMSAPSCentertaffMember, @isCenterFollowupStaff, @GrantsManagement_TextBox)" 
     onupdating="dsComm_Updating"   onupdated="dsComm_Updated" 
        oninserted="dsComm_Inserted" oninserting="dsComm_Inserting" 
        onselecting="dsComm_Selecting">
    <DeleteParameters>
        <asp:Parameter Name="theID" />
    </DeleteParameters>
    <InsertParameters>
    <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID" PropertyName="SelectedValue" Type="Int32" />
        <asp:Parameter Name="GrantsManagement_TextBox" ConvertEmptyStringToNull="true" DbType="String"  />
        <asp:Parameter Name="ContactType" ConvertEmptyStringToNull="true" DbType="String"  />
        <asp:Parameter Name="Date" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="commTime"  ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="completetime"  ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="MSAPStaffMember" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="PsnCommWith" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="Email" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="Phone"  ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="role_id" ConvertEmptyStringToNull="true" DbType="Int32"/>
        <asp:Parameter Name="CommType" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="TAComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="DataCollectionComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="ReportComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="GrantsManagement" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="KeyPersonenelChange" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="OtherComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="CommDescription" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="ActionTaken" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="RequiredDes" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="FollowupStaff" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="RequestStatus" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="FulfillDate" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="FeedbackRequest" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="FeedbackReceived" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="Result" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="PK_MassCommID" ConvertEmptyStringToNull="true" DbType="Int32"/>
        <asp:Parameter Name="CreatedBy" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="CreatedOn" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="ModifiedBy" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="ModifiedOn" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="ContactType_other" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="Role_other" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="TAComm_other" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="DataCollectionComm_other" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="ReportComm_other" ConvertEmptyStringToNull="true" DbType="String"  />
        <asp:Parameter Name="RequestStatus_other" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:ControlParameter ControlID="hfisCenterstaff" Name="isMSAPSCentertaffMember" PropertyName="Value" Type="Boolean" />
        <asp:Parameter Name="isCenterFollowupStaff"  DefaultValue = "0"/>
    </InsertParameters>
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="hfCommID" Name="theID" PropertyName="Value" Type="Int32" />
        <asp:Parameter Name="GrantsManagement_TextBox" ConvertEmptyStringToNull="true" DbType="String"  />
        <asp:Parameter Name="ContactType" ConvertEmptyStringToNull="true" DbType="String"  />
        <asp:Parameter Name="Date" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="commTime"  ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="completetime"  ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="MSAPStaffMember" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="PsnCommWith" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="Email" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="Phone"  ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="role_id" ConvertEmptyStringToNull="true" DbType="Int32"/>
        <asp:Parameter Name="CommType" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="TAComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="DataCollectionComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="ReportComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="GrantsManagement" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="KeyPersonenelChange" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="OtherComm" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="CommDescription" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="ActionTaken" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="RequiredDes" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="FollowupStaff" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="RequestStatus" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="FulfillDate" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="FeedbackRequest" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="FeedbackReceived" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="Result" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="PK_MassCommID" ConvertEmptyStringToNull="true" DbType="Int32"/>
        <asp:Parameter Name="CreatedBy" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="CreatedOn" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="ModifiedBy" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="ModifiedOn" ConvertEmptyStringToNull="true" DbType="DateTime" />
        <asp:Parameter Name="ContactType_other" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="Role_other" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="TAComm_other" ConvertEmptyStringToNull="true" DbType="String"/>
        <asp:Parameter Name="DataCollectionComm_other" ConvertEmptyStringToNull="true" DbType="String" />
        <asp:Parameter Name="ReportComm_other" ConvertEmptyStringToNull="true" DbType="String"  />
        <asp:Parameter Name="RequestStatus_other" ConvertEmptyStringToNull="true" DbType="String" />
       <asp:ControlParameter ControlID="hfisCenterstaff" Name="isMSAPSCentertaffMember" PropertyName="Value" Type="Boolean" />
        <asp:Parameter Name="isCenterFollowupStaff"  DefaultValue = "0"/>
    </UpdateParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="dsCommgv" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 

        SelectCommand="SELECT distinct ROW_NUMBER() OVER (order by date desc) as item#, * FROM [TALogCommunications] WHERE  (PK_GenInfoID = @PK_GenInfoID ) "  >
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoID" PropertyName="SelectedValue" />
             <asp:ControlParameter ControlID="hfisCenterstaff" Name="isMSAPSCentertaffMember" PropertyName="Value" Type="Boolean" />
        </SelectParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="dsFollowup" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [id], [PK_CommID], [Date], [Time], [StaffMember], [FollowupPerson], [Notes], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], Status FROM [TALogFollowUp] WHERE ([PK_CommID] = @PK_CommID )">
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlComm" Name="PK_CommID"   PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="dsUpload" runat="server"
 ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [TALogUploadFiles] WHERE ([PK_CommID] = @PK_CommID AND isActive = 1) ">
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlComm" Name="PK_CommID"   PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
</asp:Content>


