﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using MKB.TimePicker;
using System.Data;

public partial class admin_TALogs_MSAPCenterActParticipation : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            getEventList();
            Session["showAllParticipant"] = null;
            Session["showAllAct"] = null;
        }


        if (!string.IsNullOrEmpty(ddlEvents.SelectedValue))
        {
            btnNewParticipant.Visible = true;
            if (TALogActParticipation.All().Count() > 0)
                //btnShowParticipant.Visible = true;
                ;
            else
                btnShowParticipant.Visible = false;

            ltlTotal.Visible = true;
        }
        else
        {
            btnNewParticipant.Visible = false;
            btnShowParticipant.Visible = false;
            ltlTotal.Visible = false;
        }
       
    }

    protected void AddnewRecord(object sender, EventArgs e)
    {
        eventddl.Visible = false;

        hfRowID.Value = "";
        Session["isRefresh"] = false;
        txtActTitle.Text = "";
        ddlParticipationType.SelectedIndex = 0;
        txtTypeother.Text = "";
        txtActStartDate.Text = "";
        txtActEnddate.Text = "";
        txtNotes.Text = "";
        ddlActStatus.SelectedValue = "Edit Record";

        PopupPanel.Visible = true;
        Newbutton.Visible = false;
        btnShowAllAct.Visible = false;
        gdvEventActivity.Visible = false;
    }
    protected void OnEditEventActivity(object sender, EventArgs e)
    {
        Session["isRefresh"] = false;

        LinkButton btnEdit = sender as LinkButton; 
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gdvEventActivity.DataKeys[row.RowIndex].Values["ID"]);
        hfRowID.Value = rcdid.ToString();
        TALogEventActivity actpart = TALogEventActivity.SingleOrDefault(x => x.id == rcdid);
        txtActTitle.Text = actpart.Title;
        ddlParticipationType.SelectedValue = actpart.ParticipationType;
        if (actpart.ParticipationType != null && actpart.ParticipationType.ToLower().Trim() == "other")
        {
            txtTypeother.Text = actpart.typeothertext;
            txtTypeother.Style.Add(HtmlTextWriterStyle.Display, "");
        }
        else
            txtTypeother.Style.Add(HtmlTextWriterStyle.Display, "none");

        txtActStartDate.Text = actpart.Date==null ? "" : Convert.ToDateTime(actpart.Date).ToShortDateString();
        txtActEnddate.Text = actpart.Enddate == null ? "" : Convert.ToDateTime(actpart.Enddate).ToShortDateString();
        DateTime dt1 = Convert.ToDateTime(actpart.TimeAct);
        TimeSelector1.SetTime(dt1.Hour, dt1.Minute, dt1.ToString("tt") == "AM" ? TimeSelector.AmPmSpec.AM : TimeSelector.AmPmSpec.PM);
        DateTime dt2 = Convert.ToDateTime(actpart.Endtime);
        TimeSelector2.SetTime(dt2.Hour, dt2.Minute, dt2.ToString("tt") == "AM" ? TimeSelector.AmPmSpec.AM : TimeSelector.AmPmSpec.PM);

        txtNotes.Text = actpart.Notes;

        ddlActStatus.SelectedValue = actpart.Status;

        PopupPanel.Visible = true;
        Newbutton.Visible = false;
        btnShowAllAct.Visible = false;
        gdvEventActivity.Visible = false;
        eventddl.Visible = false;
    }

    protected void OnSaveEvent(object sender, EventArgs e)
    {
        string action = "";
        if (!Convert.ToBoolean(Session["isRefresh"]))
        {
            TALogEventActivity actpart = new TALogEventActivity();
            
            int rcdid = string.IsNullOrEmpty(hfRowID.Value) ? 0 : Convert.ToInt32(hfRowID.Value);
            if (rcdid == 0)
            {
                action = "Added";
               
                actpart.CreatedBy = HttpContext.Current.User.Identity.Name;
                actpart.CreatedOn = DateTime.Now;
                actpart.ModifiedBy = HttpContext.Current.User.Identity.Name;
                actpart.ModifiedOn = DateTime.Now;
            }
            else
            {
                action = "Updated";
                actpart = TALogEventActivity.SingleOrDefault(x => x.id == rcdid);
                actpart.ModifiedBy = HttpContext.Current.User.Identity.Name;
                actpart.ModifiedOn = DateTime.Now;
            }

            actpart.Title = txtActTitle.Text.Trim();
            actpart.ParticipationType = ddlParticipationType.SelectedValue;
            actpart.typeothertext = txtTypeother.Text;
            actpart.Date = string.IsNullOrEmpty(txtActStartDate.Text) ? (DateTime?)null : Convert.ToDateTime(txtActStartDate.Text);
            actpart.Enddate = string.IsNullOrEmpty(txtActEnddate.Text) ? (DateTime?)null : Convert.ToDateTime(txtActEnddate.Text);
            actpart.TimeAct = Convert.ToDateTime(TimeSelector1.Hour + ":" + TimeSelector1.Minute + ":00 " + TimeSelector1.AmPm);
            actpart.Endtime = Convert.ToDateTime(TimeSelector2.Hour + ":" + TimeSelector2.Minute + ":00 " + TimeSelector2.AmPm);

            actpart.Notes = txtNotes.Text.Trim();
            actpart.Status = ddlActStatus.SelectedValue;


            actpart.isActive = true;
            actpart.Save();
            Session["isRefresh"] = true;

            TAlog EDSender = new TAlog();
            if(HttpContext.Current.User.IsInRole("ED"))
                EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "Activity Participation", DateTime.Now.ToShortDateString(), action);

        }
            gdvEventActivity.DataBind();
            //ddlEvents.DataBind();

            eventddl.Visible = true;
            PopupPanel.Visible = false;
            Newbutton.Visible = true;
            //btnShowAllAct.Visible = true;
            gdvEventActivity.Visible = true;

            getEventList();

            Session["showAllAct"] = "1";
            showAllAct(sender, e);

    }
    private void getEventList()
    {
        var acts = (TALogEventActivity.Find(act => act.isActive)).OrderBy(a => a.Title);
        ddlEvents.Items.Clear();
        ddlEvents.Items.Add(new ListItem("Select one", ""));

        foreach (TALogEventActivity item in acts)
        {
            ddlEvents.Items.Add(new ListItem(item.Title, item.id.ToString()));
        }

    }
    protected void OnDeleteEventActivity(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gdvEventActivity.DataKeys[row.RowIndex].Values["ID"]);
        hfRowID.Value = rcdid.ToString();
        TALogEventActivity actpart = TALogEventActivity.SingleOrDefault(x => x.id == rcdid);
        actpart.isActive = false;
        actpart.Delete();
        //actpart.Save();

        gdvEventActivity.DataBind();
        getEventList();
        if (HttpContext.Current.User.IsInRole("ED"))
        {
            TAlog EDSender = new TAlog();
            EDSender.TALogDatahasbeenChangedbyED(HttpContext.Current.User.Identity.Name, "Activity Participation", DateTime.Now.ToShortDateString(), "Deleted");
        }

        PopupPanel.Visible = false;
        Newbutton.Visible = true;
        //btnShowAllAct.Visible = true;
        gdvEventActivity.Visible = true;
    }

    protected string getUserRole(int ContactID)
    {   
        string ret = "";
        var item = TALogActParticipation.SingleOrDefault(x => x.id == ContactID);
        if (item.role_id == 7)
            ret = item.OtherText;
        else
            ret = TALogRole.SingleOrDefault(r => r.id == item.role_id).rolename;
        return ret;
    }

    protected string getGranteeName(int participantID)
    {
        string ret = "";
        var item = TALogActParticipation.SingleOrDefault(x => x.id == participantID);
        if (item.granteeID == 999) //other
            ret = item.granteeOther;
        else
            ret = MagnetGrantee.SingleOrDefault(r => r.ID == item.granteeID).GranteeName;
        return ret;
    }



    protected void CloseEditPan(object sender, EventArgs e)
    {
        Session["showAllParticipant"] = "0";
        btnShowParticipant.Text = "Show All";
        PopupPanel.Visible = false;
        Newbutton.Visible = true;
        //btnShowAllAct.Visible = true;
        gdvEventActivity.Visible = true;
        eventddl.Visible = true;
    }

    protected void AddnewParticipant(object sender, EventArgs e)
    {
        Session["isRefresh"] = false;
        pnlParticipant.Visible = true;
        pnlEventAct.Visible = false;
        txtParticipantName.Text = "";
        txtGranteeOther.Text = "";

        ddlParticipantStatus.SelectedValue = "Edit Record";

        txtRoleOther.Text = "";
        txtRoleOther.Visible = false;

        getGranteeList();
        ddlGrantees.SelectedIndex = 0;
        txtGranteeOther.Visible = false;
        ddlRole.SelectedIndex = -1;
        hfParticipantID.Value = "";
        hfGranteeID.Value = "";
    }

    private void getGranteeList()
    {
        int cohortType = 0;
        if (string.IsNullOrEmpty(hfGranteeID.Value) || hfGranteeID.Value=="999")
            cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        else
        {
            if (Convert.ToInt32(hfGranteeID.Value) > 41)
            {
                cohortType = 2;
                rbtnCohortType.SelectedValue = "2";
            }
            else
            {
                cohortType = 1;
                rbtnCohortType.SelectedValue = "1";
            }

        }
        var grantees = MagnetGrantee.Find(x => x.isActive && x.ID != 39 && x.ID != 40 && x.ID != 41 && x.ID != 69 && x.ID != 70 && x.ID != 1001 && x.ID != 1002 && x.ID != 1003 && x.CohortType == cohortType).OrderBy(y => y.GranteeName);
        ddlGrantees.Items.Clear();
        ddlGrantees.Items.Add(new ListItem("Select one", ""));
        foreach (MagnetGrantee item in grantees)
        {
            ddlGrantees.Items.Add(new ListItem(item.GranteeName, item.ID.ToString()));

        }
        ddlGrantees.Items.Add(new ListItem("Other", "999"));
    }
    private void SaveParticipant()
    {
        if (!Convert.ToBoolean(Session["isRefresh"]))
        {
            TALogActParticipation savedData = new TALogActParticipation();
            if (!string.IsNullOrEmpty(hfParticipantID.Value))
            {
                savedData = TALogActParticipation.SingleOrDefault(x => x.id == Convert.ToInt32(hfParticipantID.Value));
            }

            savedData.name = txtParticipantName.Text;
            savedData.role_id = ddlRole.SelectedValue == "" ? (int?)null : Convert.ToInt32(ddlRole.SelectedValue);
            savedData.granteeID = ddlGrantees.SelectedValue == "" ? (int?)null : Convert.ToInt32(ddlGrantees.SelectedValue);
            savedData.granteeOther = txtGranteeOther.Text.Trim();
            savedData.OtherText = txtRoleOther.Text.Trim();
            savedData.FK_EventID = Convert.ToInt32(ddlEvents.SelectedValue);
            savedData.isActive = true;
            savedData.Status = ddlParticipantStatus.SelectedValue;
            savedData.Save();
            gvParticipant.DataBind();
            Session["isRefresh"] = true;

            Session["showAllParticipant"] = "1";
            showAllParticipant(this, new EventArgs());
        }
    }

    protected void OnSaveParticipant(object sender, EventArgs e)
    {
        SaveParticipant();
        CloseParticipantPanel();
    }

    protected void OnSaveParticipantAnother(object sender, EventArgs e)
    {
        SaveParticipant();
        hfParticipantID.Value = "";
        AddnewParticipant(sender, e);
    }
    private void CloseParticipantPanel()
    {
        pnlParticipant.Visible = false;
        pnlEventAct.Visible = true;
        gvParticipant.DataBind();
    }
    protected void CloseParticipantEditPan(object sender, EventArgs e)
    {
        Session["showAllParticipant"] = "0";
        btnShowParticipant.Text = "Show All";
        CloseParticipantPanel();
    }

    protected void OnEditActParticipation(object sender, EventArgs e)
    {
        Session["isRefresh"] = false;
        pnlParticipant.Visible = true;
        pnlEventAct.Visible = false;
        ddlRole.DataBind();

        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvParticipant.DataKeys[row.RowIndex].Values["ID"]);
        hfParticipantID.Value = rcdid.ToString();
        TALogActParticipation participantData = TALogActParticipation.SingleOrDefault(x => x.id == rcdid && x.isActive == true);

        txtParticipantName.Text = participantData.name;
        ddlRole.SelectedValue = participantData.role_id.ToString();
        if (ddlRole.SelectedItem.Text == "Other")
        {
            txtRoleOther.Visible = true;
        }
        hfGranteeID.Value = participantData.granteeID.ToString();
        getGranteeList();
        ddlGrantees.DataBind();
        txtGranteeOther.Text = participantData.granteeOther;

        txtRoleOther.Text = participantData.OtherText;
        ddlParticipantStatus.SelectedValue = participantData.Status;

    }

    protected void OnDeleteActParticipation(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvParticipant.DataKeys[row.RowIndex].Values["ID"]);
        TALogActParticipation participantData = TALogActParticipation.SingleOrDefault(x => x.id == rcdid);
        participantData.isActive = false;

        participantData.Delete();

        gvParticipant.DataBind();
        gdvEventActivity.DataBind();
    }


    protected void dsActparticipation_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        ltlTotal.Text = "Total participants: " + e.AffectedRows.ToString();
    }
    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlGrantees.SelectedItem.Text == "Other")
        {
            ltlgOther.Visible = true;
            txtGranteeOther.Visible = true;
        }
        else
        {
            ltlgOther.Visible = false;
            txtGranteeOther.Visible = false;
            txtGranteeOther.Text = "";
        }
    }
    protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlRole.SelectedItem.Text =="Other")
        {
            txtRoleOther.Visible = true;
        }
        else
        {
            txtRoleOther.Visible = false;
            txtRoleOther.Text = "";
        }
    }
    protected void dsRole_DataBinding(object sender, EventArgs e)
    {
        txtRoleOther.Visible = ddlRole.SelectedItem.Text == "Other" ? true : false;
    }
    protected void rbtnCohortType_SelectedIndexChanged(object sender, EventArgs e)
    {
        hfGranteeID.Value = "";
        txtGranteeOther.Text = "";
        getGranteeList();
        pnlParticipant.Visible = true;
        pnlEventAct.Visible = false;
    }

    protected void ddlGrantees_DataBound(object sender, EventArgs e)
    {
        ddlGrantees.SelectedValue = hfGranteeID.Value;
        if (ddlGrantees.SelectedItem.Text == "Other")
        {
            ltlgOther.Visible = true;
            txtGranteeOther.Visible = true;
        }
    }
    protected void showAllAct(object sender, EventArgs e)
    {
        //if (Session["showAllAct"] == null || Session["showAllAct"].ToString() == "0")
        //{
            btnShowAllAct.Text = "Show Edit Records";
            dsEventActivity.SelectCommand = "select * from dbo.TALogEventActivity order by Date desc";
            Session["showAllAct"] = "1";
        //}
        //else
        //{
        //    btnShowAllAct.Text = "Show All";
        //    dsEventActivity.SelectCommand = "select * from dbo.TALogEventActivity where Status ='Edit Record'  order by Date desc";
        //    Session["showAllAct"] = "0";
        //}
        dsEventActivity.Select(DataSourceSelectArguments.Empty);
        dsEventActivity.DataBind();
        gdvEventActivity.DataBind();

    }
    protected void showAllParticipant(object sender, EventArgs e)
    {

        //if (Session["showAllParticipant"] == null || Session["showAllParticipant"].ToString() == "0")
        //{
            btnShowParticipant.Text = "Show Edit Records";
            dsActparticipation.SelectCommand = "SELECT * FROM [TALogActParticipation] WHERE FK_EventID= " + ddlEvents.SelectedValue + " order by name";
            Session["showAllParticipant"] = "1";
        //}
        //else
        //{
        //    btnShowParticipant.Text = "Show All";
        //    dsActparticipation.SelectCommand = "SELECT * FROM [TALogActParticipation] WHERE Status='Edit Record' AND FK_EventID= " + ddlEvents.SelectedValue + " order by name";
        //    Session["showAllParticipant"] = "0";
        //}
        dsActparticipation.Select(DataSourceSelectArguments.Empty);
        dsActparticipation.DataBind();
        gvParticipant.DataBind();
    }
   
    protected void ddlEvents_SelectedIndexChanged1(object sender, EventArgs e)
    {
        int inputID = string.IsNullOrEmpty(ddlEvents.SelectedValue) ? 0 : Convert.ToInt32(ddlEvents.SelectedValue);
        int evtid = TALogActParticipation.Find(x => x.FK_EventID == inputID).Count();
        if (evtid == 0)
            btnShowParticipant.Visible = false;
        Session["showAllParticipant"] = "0";
        btnShowParticipant.Text = "Show All";
    }
    protected void gdvEventActivity_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //if (Session["showAllAct"] == null || Session["showAllAct"].ToString() == "0")
        //{
            //btnShowAllAct.Text = "Show Edit Records";
            dsEventActivity.SelectCommand = "select * from dbo.TALogEventActivity order by Date desc";
            //Session["showAllAct"] = "1";
        //}
        //else
        //{
        //    //btnShowAllAct.Text = "Show All";
        //    dsEventActivity.SelectCommand = "select * from dbo.TALogEventActivity where Status ='Edit Record'  order by Date desc";
        //    //Session["showAllAct"] = "0";
        //}
        gdvEventActivity.PageIndex = e.NewPageIndex;
        dsEventActivity.Select(DataSourceSelectArguments.Empty);
        dsEventActivity.DataBind();
        gdvEventActivity.DataBind();
    }
}