﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Telerik.Web.UI;
using System.Text.RegularExpressions;
using System.Collections;

public partial class admin_TALogs_ContextualFactors : System.Web.UI.Page
{
    vwContextualfactorDataClassesDataContext schooldb = new vwContextualfactorDataClassesDataContext();
    private void oldinitialCoordinPans()
    {
        var cats = TAThemeCat.All().OrderBy(x => x.CatName);

        foreach (var cat in cats)
        {
            CheckBoxList cbsubTheme = new CheckBoxList();
            cbsubTheme.Attributes.Add("style", "font-size: 11px;font-family: Helvetica,Arial,Sans-serif!important;");
            var subTheme = TAThemeSubCat.Find(sub => sub.ThemeCatID == cat.id).OrderBy(odr => odr.SubCatName);
            cbsubTheme.ID = "ThemeSubCat" + cat.id;
            cbsubTheme.DataTextField = "SubCatName";
            cbsubTheme.DataValueField = "id";
            cbsubTheme.DataSource = subTheme;

            cbsubTheme.DataBind();
            RadPanelItem catItem = new RadPanelItem();
            catItem.Font.Bold = true;
            catItem.Text = cat.CatName;
            catItem.Value = cat.id.ToString();
            radpnlbarCategory.Items.Add(catItem);
            RadPanelItem radPanelItemCheckBoxList = new RadPanelItem();
            radPanelItemCheckBoxList.Controls.Add(cbsubTheme);
            catItem.Items.Add(radPanelItemCheckBoxList);
        }
        //////////////////////Parent Engagement////////////////////////////

        var Pengagecats = TAParentEngagementCat.All().OrderBy(x => x.ordernum);

        foreach (var cat in Pengagecats)
        {
            CheckBoxList cbsubTheme = new CheckBoxList();
            var PengagesubTheme = TAParentEngagementSubCat.Find(sub => sub.CatID == cat.id).OrderBy(odr => odr.ordernum);
            cbsubTheme.ID = "pengageSubCat" + cat.id;
            cbsubTheme.DataTextField = "SubCatName";
            cbsubTheme.DataValueField = "id";
            cbsubTheme.DataSource = PengagesubTheme;

            cbsubTheme.DataBind();
            RadPanelItem catItem = new RadPanelItem();
            catItem.Text = cat.CatName;
            catItem.Value = cat.id.ToString();
            rpnlBarPEngage.Items.Add(catItem);
            RadPanelItem radPanelItemCheckBoxList = new RadPanelItem();
            radPanelItemCheckBoxList.Controls.Add(cbsubTheme);
            catItem.Items.Add(radPanelItemCheckBoxList);

            RadPanelItem radPanelItemotherTxtbox = new RadPanelItem();
            TextBox otherTxtbox = new TextBox();
            otherTxtbox.ID = "txtOther_" + cat.id;
            radPanelItemotherTxtbox.Controls.Add(otherTxtbox);
            catItem.Items.Add(radPanelItemotherTxtbox);
        }
    }
    private void initialThemeCoordinPans()
    {
        if(string.IsNullOrEmpty(hfeditSchoolID.Value))
            hfeditSchoolID.Value = Session["mgtSchoolID"].ToString();

        radpnlbarCategoryReadMode.Visible = true;
        radpnlbarCategory.Visible = false;
        btnFtheme.Text = "Add/edit data";
        var mschool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfeditSchoolID.Value));
        string[] themes = mschool.TAthemeCat != null ? mschool.TAthemeCat.Split(',') : new string[0];
        string[] Allsubthemes = mschool.TAthemeSubCat != null ? mschool.TAthemeSubCat.Split(';') : new string[0];
        int ii = 0;

        ddlPrimarytheme.Items.Clear();
        ddlPrimarytheme.Items.Add(new ListItem("Select one", ""));

        radpnlbarCategoryReadMode.Items.Clear();
        var cats = TAThemeCat.All().OrderBy(x => x.CatName);

        foreach (var cat in cats)
        {
            foreach (var dbid in themes)
            {
                if (cat.id.ToString() != dbid)
                    continue;

                CheckBoxList cbsubTheme = new CheckBoxList();
                TextBox ptxtother = new TextBox();
                ptxtother.ID = "txtOther_" + cat.id;

                var subTheme = TAThemeSubCat.Find(sub => sub.ThemeCatID == cat.id).OrderBy(odr => odr.SubCatName);

                int jj = 0;
                string txtOther = "";
                string[] subthemes = new string[0];
                if (ii < themes.Count())
                {
                    if (!string.IsNullOrEmpty(Allsubthemes[ii]))
                    {
                        if (Allsubthemes[ii].Split(':').Count() > 1)
                            txtOther = Allsubthemes[ii].Split(':')[1];

                        subthemes = Allsubthemes[ii].Split(':')[0].Split(',');
                    }
                }
                foreach (var itm in subTheme)
                {
                    foreach (var dbsubcatid in subthemes)
                    {
                        if (itm.id.ToString() != dbsubcatid)
                            continue;


                        ListItem ckbx = new ListItem();

                        ckbx.Attributes.Add("style", "font-size: 11px;font-family: Helvetica,Arial,Sans-serif!important;");

                        ckbx.Value = subthemes[jj];
                        ckbx.Text = itm.SubCatName;
                        ckbx.Enabled = false;
                        ptxtother.Enabled = false;

                        if (subthemes.Count() > 0 && jj < subthemes.Count())
                        {
                            if (subthemes[jj] == itm.id.ToString())
                            {
                                ckbx.Selected = true;
                                ckbx.Enabled = false;
                                ptxtother.Text = txtOther;
                                ptxtother.Enabled = false;
                            }
                            else
                                ckbx.Selected = false;
                        }
                        cbsubTheme.Items.Add(ckbx);
                        jj++;
                    }
                }
                cbsubTheme.ID = "ThemeSubCat" + cat.id;
                RadPanelItem catItem = new RadPanelItem();
                catItem.Font.Bold = true;
                catItem.Text = cat.CatName;
                catItem.Value = cat.id.ToString();
                catItem.Expanded = false;

                ddlPrimarytheme.Items.Add(new ListItem(cat.CatName, cat.CatName));
                if (mschool.MagnetTheme != null && mschool.MagnetTheme == cat.CatName)
                    ddlPrimarytheme.SelectedValue = cat.CatName;

                radpnlbarCategoryReadMode.Items.Add(catItem);
                RadPanelItem radPanelItemCheckBoxList = new RadPanelItem();
                radPanelItemCheckBoxList.Controls.Add(cbsubTheme);
                catItem.Items.Add(radPanelItemCheckBoxList);

                if (!string.IsNullOrEmpty(ptxtother.Text))
                {
                    ptxtother.Enabled = false;
                    RadPanelItem radPanelItemotherTxtbox = new RadPanelItem();
                    radPanelItemotherTxtbox.Controls.Add(ptxtother);
                    catItem.Items.Add(radPanelItemotherTxtbox);
                }

                ii++;
            }
        }
        ddlPrimarytheme.DataBind();
    }

    private void initialEngageCoordinPans()
    {
        if (string.IsNullOrEmpty(hfeditSchoolID.Value))
            hfeditSchoolID.Value = Session["mgtSchoolID"].ToString();

        rpnlBarPEngage.Visible = false;
        rpnlBarPEngageReadMode.Visible = true;

        btnFEngage.Text = "Add/edit data";
       var mschool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfeditSchoolID.Value));
        string[] pEngages = mschool.TApengageCat != null ? mschool.TApengageCat.Split(',') : new string[0];
        string[] AllengageSubs = mschool.TApengageSubCat != null ? mschool.TApengageSubCat.Split('>') : new string[0];
        int ii = 0;

        rpnlBarPEngageReadMode.Items.Clear();
        var Pengagecats = TAParentEngagementCat.All().OrderBy(x => x.ordernum);

        foreach (var cat in Pengagecats)
        {
            foreach (var dbid in pEngages)
            {
                if (cat.id.ToString() != dbid)
                    continue;

                CheckBoxList cbsubTheme = new CheckBoxList();
                TextBox ptxtother = new TextBox();
                ptxtother.ID = "txtOther_" + cat.id;

                var subTheme = TAParentEngagementSubCat.Find(sub => sub.CatID == cat.id).OrderBy(odr => odr.ordernum);

                int jj = 0;
                string txtOther = "";
                string[] subthemes = new string[0];
                if (ii < pEngages.Count())
                {
                    if (!string.IsNullOrEmpty(AllengageSubs[ii]))
                    {
                        if (AllengageSubs[ii].Split('&').Count() > 1)
                            txtOther = HttpUtility.HtmlDecode(AllengageSubs[ii].Split('&')[1]);

                        subthemes = AllengageSubs[ii].Split('&')[0].Split(',');
                    }
                }
                foreach (var itm in subTheme)
                {
                    foreach (var dbsubcatid in subthemes)
                    {
                        if (itm.id.ToString() != dbsubcatid)
                            continue;


                        ListItem ckbx = new ListItem();
                        ckbx.Attributes.Add("style", "font-size: 11px;font-family: Helvetica,Arial,Sans-serif!important;");
                        ckbx.Value = subthemes[jj];
                        ckbx.Text = itm.SubCatName;
                        ckbx.Enabled = false;
                        ptxtother.Enabled = false;

                        if (subthemes.Count() > 0 && jj < subthemes.Count())
                        {
                            if (subthemes[jj] == itm.id.ToString())
                            {
                                ckbx.Selected = true;
                                ckbx.Enabled = false;
                                ptxtother.Text = txtOther;
                                ptxtother.Enabled = false;
                            }
                            else
                                ckbx.Selected = false;
                        }
                        cbsubTheme.Items.Add(ckbx);
                        jj++;
                    }
                }
                cbsubTheme.ID = "pEngageSubCat" + cat.id;
                RadPanelItem catItem = new RadPanelItem();
                catItem.Font.Bold = true;
                catItem.Text = cat.CatName;
                catItem.Value = cat.id.ToString();
                catItem.Expanded = false;

                rpnlBarPEngageReadMode.Items.Add(catItem);
                RadPanelItem radPanelItemCheckBoxList = new RadPanelItem();

                radPanelItemCheckBoxList.Controls.Add(cbsubTheme);
                catItem.Items.Add(radPanelItemCheckBoxList);

                if (!string.IsNullOrEmpty(ptxtother.Text))
                {
                    RadPanelItem radPanelItemotherTxtbox = new RadPanelItem();
                    radPanelItemotherTxtbox.Controls.Add(ptxtother);
                    catItem.Items.Add(radPanelItemotherTxtbox);
                }

                ii++;
            }
        }

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        var cats = TAThemeCat.All().OrderBy(x => x.CatName);

        foreach (var cat in cats)
        {
            CheckBoxList cbsubTheme = new CheckBoxList();

            cbsubTheme.Attributes.Add("style", "font-size: 11px;font-family: Helvetica,Arial,Sans-serif!important;");

            var subTheme = TAThemeSubCat.Find(sub => sub.ThemeCatID == cat.id).OrderBy(odr => odr.SubCatName);
            cbsubTheme.ID = "ThemeSubCat" + cat.id;
            cbsubTheme.DataTextField = "SubCatName";
            cbsubTheme.DataValueField = "id";
            cbsubTheme.DataSource = subTheme;
            //cbsubTheme.DataBound += new EventHandler(CheckChanged);
            cbsubTheme.DataBind();
            RadPanelItem catItem = new RadPanelItem();
            catItem.Font.Bold = true;
            catItem.Text = cat.CatName;
            catItem.Value = cat.id.ToString();
            radpnlbarCategory.Items.Add(catItem);
            RadPanelItem radPanelItemCheckBoxList = new RadPanelItem();
            radPanelItemCheckBoxList.Controls.Add(cbsubTheme);
            catItem.Items.Add(radPanelItemCheckBoxList);
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int FinalPrintCohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        }
        //always getting year1 APR reports
        int reportPeriod = 0, reportYear = 0, cohortType = 0;
        if (Session["TALogCohortType"] == null)
          Session["TALogCohortType"] = rbtnCohortType.SelectedValue;

        cohortType = Convert.ToInt32(Session["TALogCohortType"]);

        var rptPeriods = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType).Last();

        reportPeriod = rptPeriods.ID;
        reportYear = Convert.ToInt32(rptPeriods.reportYear);
        
        hfReportYearID.Value = reportYear.ToString();
        hfReportPeriodID.Value = reportPeriod.ToString();

        Session["TALogCohortPeriod"] = reportPeriod;

        if (Session["TALogCohortType"] != null && string.IsNullOrEmpty(Session["TALogCohortType"].ToString()))
            Session["TALogCohortType"] = null;
        if (Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
            Session["TALogGranteeID"] = null;
        if (!IsPostBack)
        {
            rbtnCohortType.SelectedValue = Session["TALogCohortType"] == null ? "1" : Session["TALogCohortType"].ToString();
            ddlGrantees.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();
        }


    }


    private string getSchoolType(string grades)
    {
        string retType = "";
        if (grades != null)
        {
            string[] arrgrade = grades.Split(';');
            int firstgrade = Convert.ToInt32(arrgrade.First());
            int lastgrade = Convert.ToInt32(arrgrade.Last());

            if (firstgrade >= 1 && lastgrade <= 8)
                retType = "Elementary";
            else if (6 <= firstgrade && lastgrade <= 10)
                retType = "Middle";
            else if (11 <= firstgrade && lastgrade <= 14)
                retType = "High";
            else if (1 <= firstgrade && lastgrade <= 10)
                retType = "Elementary/Middle";
            else if (7 <= firstgrade && lastgrade <= 14)
                retType = "Middle/High";
            else if (1 <= firstgrade && lastgrade <= 14)
                retType = "Combination all";
            else
                retType = "Other -- (" + firstgrade + " - " + lastgrade + ")";
        }
        return retType;
    }
    private void initialFields()
    {
        btnSaveParent.Visible = false;
        btnSaveTheme.Visible = false;

        int cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        int reportPeriodID = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType).Last().ID;

        int theSchoolID = Convert.ToInt32(hfeditSchoolID.Value);
        int granteeID = Convert.ToInt32(ddlGrantees.SelectedValue);
        int reportID = GranteeReport.SingleOrDefault(x => x.GranteeID == granteeID && x.ReportPeriodID == reportPeriodID).ID;
        string state =  MagnetGranteeDatum.SingleOrDefault(x => x.GranteeReportID == reportID)==null?"": MagnetGranteeDatum.SingleOrDefault(x => x.GranteeReportID == reportID).State;

        var gpraItem = MagnetGPRA.SingleOrDefault(x => x.ReportID == reportID && x.SchoolID == theSchoolID);
        var schoolItem = MagnetSchool.SingleOrDefault(x => x.ID == theSchoolID);


        txtGranteeName.Text = ddlGrantees.SelectedItem.Text;
        txtState.Text = state;
        txtGrantSize.Text = MagnetGPRA.Find(x => x.ReportID == reportID).Count.ToString();
        if (MagnetGPRA.Find(x => x.ReportID == reportID).Count > 1)
            txtGrantSize.Text += " schools";
        else
            txtGrantSize.Text += " school";

        SchoolNameTextBox.Text = schoolItem.SchoolName;

        SchoolTypeTextBox.Text = gpraItem==null?"": getSchoolType(gpraItem.SchoolGrade);
        txtprogramType.Text = gpraItem==null?"": gpraItem.ProgramType == null ? "" : (bool)gpraItem.ProgramType ? "Partial" : "Whole-school";

        ddlProgramStatus.Text = schoolItem.ProgramStatus;
        ddlUrbanicity.SelectedValue = schoolItem.Urbanicity;
        ddlDifRange.SelectedValue = schoolItem.FreeRedueLunchRange;

        FreeReduceLunchTextBox.Text = "";

        var gpraData = MagnetGPRA.SingleOrDefault(x => x.SchoolID == schoolItem.ID && x.ReportID == reportID);
        if (gpraData.FRPMPpercentage != null)
            FreeReduceLunchTextBox.Text = gpraData.FRPMPpercentage;
        //if (schoolItem.FreeReduceLunch != null)
        //    FreeReduceLunchTextBox.Text = schoolItem.FRPMPpercentage;
        txtTitle1Status.Text = gpraItem== null ? "" : gpraItem.ProgramType == null ? "" : gpraItem.TitleISchoolFunding==null ?"" : (bool)gpraItem.TitleISchoolFunding ? "Yes" : "No";


        txtschoolDesignation.Text = "";
        if (gpraItem!=null && gpraItem.TitleISchoolFundingImprovementStatus != null)
        {
            if (gpraItem.TitleISchoolFundingImprovementStatus == 29)
                txtschoolDesignation.Text = "Not Applicable";
            else if (MagnetGPRAImprovementStatus.SingleOrDefault(x => x.statusvalue == gpraItem.TitleISchoolFundingImprovementStatus.ToString()) != null)
                txtschoolDesignation.Text = MagnetGPRAImprovementStatus.SingleOrDefault(x => x.statusvalue == gpraItem.TitleISchoolFundingImprovementStatus.ToString()).statusitem;
           

        }

        //txtschoolDesignation.Text = gpraItem.TitleISchoolFundingImprovementStatus.ToString();
        if (gpraItem != null)
        {
            txtLowestAchieve.Text = gpraItem.PersistentlyLlowestAchievingSchool == null ? "" : (bool)gpraItem.PersistentlyLlowestAchievingSchool ? "Yes" : "No";
            txtSchoolGrant.Text = gpraItem.SchoolImprovementGrant == null ? "" : (bool)gpraItem.SchoolImprovementGrant ? "Yes" : "No";
        }
        foreach (ListItem li in cbxlstMGI.Items)
            li.Selected = false;

        if (!string.IsNullOrEmpty(schoolItem.MGIObjectives))
        {
            foreach (string str in schoolItem.MGIObjectives.Split(';'))
            {
                foreach (ListItem li in cbxlstMGI.Items)
                {
                    if (li.Value.Equals(str))
                        li.Selected = true;
                }

            }
        }

        cblstMinGroup.ClearSelection();
        foreach (ListItem li in cblstMinGroup.Items)
            li.Selected = false;

        if (!string.IsNullOrEmpty(schoolItem.MinorityGroup))
        {
            foreach (string str in schoolItem.MinorityGroup.Split(';'))
            {
                foreach (ListItem li in cblstMinGroup.Items)
                {
                    if (li.Value.Equals(str))
                        li.Selected = true;
                }
            }
        }

        foreach (ListItem li in cbxlstSpecialist.Items)
            li.Selected = false;

        txtSpecialistOther.Visible = false;

        if (!string.IsNullOrEmpty(schoolItem.Specialists))
        {
            foreach (string str in schoolItem.Specialists.Split(';'))
            {
                foreach (ListItem li in cbxlstSpecialist.Items)
                {
                    if (li.Value.Equals(str))
                    {
                        li.Selected = true;
                        if (li.Text.Trim().ToLower() == "other")
                            txtSpecialistOther.Visible = true;
                    }
                }
            }
        }

        txtSpecialistOther.Text = schoolItem.SpecialistOther;

        ddlDesegregationPlan.SelectedValue = schoolItem.DesegregationPlan;

        if (!string.IsNullOrEmpty(schoolItem.DesegregationPlan))
        {
            rwDesegregationPlan.Visible = true;
            ddlDesegregationSubPlan.Items.Clear();
            ddlDesegregationSubPlan.Items.Add(new ListItem("Select one", ""));
            if (schoolItem.DesegregationPlan.ToLower() == "required plan")
            {
                ddlDesegregationSubPlan.Items.Add(new ListItem("Court ordered", "Court ordered"));
                ddlDesegregationSubPlan.Items.Add(new ListItem("State mandated", "State mandated"));

            }
            else if (schoolItem.DesegregationPlan.ToLower() == "voluntary plan")
            {
                ddlDesegregationSubPlan.Items.Add(new ListItem("Office for Civil Rights required", "Office for Civil Rights required"));
                ddlDesegregationSubPlan.Items.Add(new ListItem("Voluntary", "Voluntary"));
            }
            ddlDesegregationSubPlan.SelectedValue = schoolItem.DesegregationSubPlan;
        }
        else
        {
            rwDesegregationPlan.Visible = false;
            ddlDesegregationSubPlan.Items.Clear();
        }

        ddlAttendType.SelectedValue = schoolItem.AttendType;

        ddlAdminMethod.SelectedIndex = ddlAdminMethod.Items.IndexOf(ddlAdminMethod.Items.FindByText(schoolItem.AdmissionMethod));

        txtAdminOther.Text = schoolItem.AdminMethodOther;


        if (schoolItem.AdmissionMethod != null && schoolItem.AdmissionMethod.ToLower() == "weighted lottery")
        {
            trwlottery.Style.Add(HtmlTextWriterStyle.Display, "");
            foreach (ListItem li in cbxlstWlottery.Items)
            {
                li.Selected = false;
            }
            if (schoolItem.wlotterytype != null)
            {
                foreach (string str in schoolItem.wlotterytype.Split(';'))
                {
                    foreach (ListItem li in cbxlstWlottery.Items)
                    {
                        if (li.Value.Equals(str))
                        {
                            li.Selected = true;
                        }

                    }
                }
            }
            else
            {
                foreach (ListItem li in cbxlstWlottery.Items)
                {
                    li.Selected = false;
                }
            }
        }
        else
        {
            trwlottery.Style.Add(HtmlTextWriterStyle.Display, "none");
            foreach (ListItem li in cbxlstWlottery.Items)
            {
                li.Selected = false;
            }
        }

        //end theme
        ddlCurriculumnType.SelectedValue = schoolItem.CurriculumnType;

        //change Desegregationact to Marketingrecruitment
        /////////initial marketing recruitment//////////////////////////
        string mcpart1 ="", mcpart2="";
        if(!string.IsNullOrEmpty(schoolItem.Marketingrecruitment))
        {
            mcpart1 = schoolItem.Marketingrecruitment.Split('&')[0];

            if(schoolItem.Marketingrecruitment.Split('&').Count() > 1)
                mcpart2 = schoolItem.Marketingrecruitment.Split('&')[1];
        }

        ddlmc11.ClearSelection();
        mcOther11.Text = "";
        mcOther11.Visible = false;
        txtmc12.Text = "";
        ddlmc13.ClearSelection();
        ddlmc14.ClearSelection();
        txtmc15.Text = "";
        txtmc15.Visible = false;

        ddlmc21.ClearSelection();
        mcOther21.Text = "";
        mcOther21.Visible = false;
        txtmc22.Text = "";
        ddlmc23.ClearSelection();
        ddlmc24.ClearSelection();
        txtmc25.Text = "";
        txtmc25.Visible = false;

        ddlmc31.ClearSelection();
        mcOther31.Text = "";
        mcOther31.Visible = false;
        txtmc32.Text = "";
        ddlmc33.ClearSelection();
        ddlmc34.ClearSelection();
        txtmc35.Text = "";
        txtmc35.Visible = false;

        int rowNm = 1;
        if (!string.IsNullOrEmpty(mcpart1))
        {
            foreach (string rowlist in mcpart1.Split('>'))
            {
                switch (rowNm)
                {
                    case 1:
                        ddlmc11.SelectedValue = rowlist.Split('\'')[0];
                        mcOther11.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[1]);
                        //if (!string.IsNullOrEmpty(mcOther11.Text))
                        //    mcOther11.Visible = true;

                        txtmc12.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[2]);
                        ddlmc13.SelectedValue = rowlist.Split('\'')[3];
                        ddlmc14.SelectedValue = rowlist.Split('\'')[4];
                        txtmc15.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[5]);
                        if (ddlmc14.SelectedValue.Contains("147")) //other (texbox)
                            txtmc15.Visible = true;
                        break;
                    case 2:
                        ddlmc21.SelectedValue = rowlist.Split('\'')[0];
                         mcOther21.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[1]);
                        //if (!string.IsNullOrEmpty(mcOther21.Text))
                        //    mcOther21.Visible = true;

                        txtmc22.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[2]);
                        ddlmc23.SelectedValue = rowlist.Split('\'')[3];
                        ddlmc24.SelectedValue = rowlist.Split('\'')[4];
                        txtmc25.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[5]);
                        if (ddlmc24.SelectedValue.Contains("247")) //other (texbox)
                            txtmc25.Visible = true;
                        break;
                    case 3:
                        ddlmc31.SelectedValue = rowlist.Split('\'')[0];
                         mcOther31.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[1]);
                        //if (!string.IsNullOrEmpty(mcOther31.Text))
                        //    mcOther31.Visible = true;

                        txtmc32.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[2]);
                        ddlmc33.SelectedValue = rowlist.Split('\'')[3];
                        ddlmc34.SelectedValue = rowlist.Split('\'')[4];
                        txtmc35.Text = HttpUtility.HtmlDecode(rowlist.Split('\'')[5]);
                        if (ddlmc34.SelectedValue.Contains("347")) //other (texbox)
                            txtmc35.Visible = true;
                        break;
                }
                rowNm++;

            }
        }

        //////////////////part 2/////////////////////////////////
        Dictionary<int, Dictionary<int,Hashtable>> GrantYearlist = new Dictionary<int,Dictionary<int,Hashtable>>();
        Hashtable htbIsChecked = new Hashtable();

        htbIsChecked.Add("cbx133", true);
        htbIsChecked.Add("cbx138", true);
        htbIsChecked.Add("cbx141", true);
        htbIsChecked.Add("cbx142", true);
        htbIsChecked.Add("cbx143", true);
        htbIsChecked.Add("cbx144", true);
        htbIsChecked.Add("cbx145", true);

        htbIsChecked.Add("cbx233", true);
        htbIsChecked.Add("cbx238", true);
        htbIsChecked.Add("cbx241", true);
        htbIsChecked.Add("cbx242", true);
        htbIsChecked.Add("cbx243", true);
        htbIsChecked.Add("cbx244", true);
        htbIsChecked.Add("cbx245", true);

        htbIsChecked.Add("cbx333", true);
        htbIsChecked.Add("cbx338", true);
        htbIsChecked.Add("cbx341", true);
        htbIsChecked.Add("cbx342", true);
        htbIsChecked.Add("cbx343", true);
        htbIsChecked.Add("cbx344", true);
        htbIsChecked.Add("cbx345", true);


        if (!string.IsNullOrEmpty(mcpart2))
        {
            int year=1;
            foreach (string cbxlistCat in mcpart2.Split('>')) //loop for Grant year
            {
                int cbxCategoryPos = 1;
                Dictionary<int, Hashtable> mybxlst = new Dictionary<int, Hashtable>();
                foreach(string controllist in cbxlistCat.Split('<'))  //loop for each checkbox Category
                {
                    Hashtable ctlhtbl = new Hashtable();
                    foreach (string ctlobj in controllist.Split('\''))
                    {
                        if (!string.IsNullOrEmpty(ctlobj))
                            ctlhtbl.Add(ctlobj.Split('"')[0], ctlobj.Split('"')[1]);
                    }
                    mybxlst.Add(cbxCategoryPos, ctlhtbl);
                    cbxCategoryPos++;
                }
                GrantYearlist.Add(year, mybxlst);
                year++;
            }

        }

        foreach (RadPanelItem grantpanel in rpnlMarketRec.Items)
        {
            grantpanel.Expanded = false;
            foreach (RadPanelItem ctlItem in grantpanel.Items)
            {
                ctlItem.Expanded = false;
                string position = ctlItem.Value.Split('|')[0];
                int    controlNum = Convert.ToInt32(ctlItem.Value.Split('|')[1]);
                int row = Convert.ToInt32(position.Substring(0, 1)),
                    col = Convert.ToInt32(position.Substring(1,1));
                for (int i = 1; i <= controlNum; i++)
                {
                    CheckBox mcCbx = ctlItem.FindControl("cbx" + position +i) as CheckBox;
                    TextBox txtmcOther = ctlItem.FindControl("txtOther" + position +i) as TextBox;

                    if (mcCbx != null)
                    {
                        mcCbx.Checked = false;
                    }
                    if (txtmcOther != null)
                    {
                        txtmcOther.Text = "";
                        txtmcOther.Visible = false;
                    }
                    if (!string.IsNullOrEmpty(mcpart2))
                    {
                        Hashtable ControlHash = GrantYearlist[row][col];
                        if (mcCbx != null && ControlHash.ContainsKey(mcCbx.ID))
                        {
                            if ("1" == ControlHash[mcCbx.ID].ToString())
                            {
                                mcCbx.Checked = true;
                                if (mcCbx.Text.Trim().ToLower() == "other" || Convert.ToBoolean(htbIsChecked[mcCbx.ID]))
                                {
                                    txtmcOther.Visible = true;
                                    txtmcOther.Text = HttpUtility.HtmlDecode(ControlHash[txtmcOther.ID].ToString());
                                }
                                


                            }
                            else
                                mcCbx.Checked = false;
                        }

                    }

                    
                }
                
            }

        }
        


        //ddlMarketingrecruitment.SelectedValue = schoolItem.Marketingrecruitment;
        /////////end initial marketing recruitment//////////////////////////
        //Professional development Activities
        string[] ProfdevActIDs = string.IsNullOrEmpty(schoolItem.ProfDevAct) ? new string[]{""} : schoolItem.ProfDevAct.Split(',');
        txtPDAOther.Text = "";
        txtPDAOther.Visible = false;
        foreach (ListItem item in cblstProvDevelopmentAct.Items)
        {
            item.Selected = false;
            foreach (string strid in ProfdevActIDs)
            {
                if (strid == item.Value)
                {
                    item.Selected = true;

                    if (item.Text.Trim().ToLower() == "other")
                    {
                        txtPDAOther.Visible = true;
                        txtPDAOther.Text = schoolItem.ProfDevActOther;
                    }
                }
            }
        }
        
        //if (!string.IsNullOrWhiteSpace(schoolItem.ProfDevActOther))
        //{
        //    txtPDAOther.Text = schoolItem.ProfDevActOther;
        //    txtPDAOther.Visible = true;
        //}
        //else
        //    txtPDAOther.Visible = false;

        //Professional development topics
        string[] ProfdevTopicIDs = string.IsNullOrEmpty(schoolItem.ProfDevTopic) ? new string[] { "" } : schoolItem.ProfDevTopic.Split(',');
        txtPDTother.Text = "";
        txtPDTother.Visible = false;
        foreach (ListItem item in cblstPDT.Items)
        {
            item.Selected = false;
            foreach (string strid in ProfdevTopicIDs)
            {
                if (strid == item.Value)
                {
                    item.Selected = true;

                    if (item.Text.Trim().ToLower() == "other")
                    {
                        txtPDTother.Text = schoolItem.ProfDevTopicOther;
                        txtPDTother.Visible = true;
                    }
                }
            }
        }

        //if (!string.IsNullOrWhiteSpace(schoolItem.ProfDevTopicOther))
        //{
        //    txtPDTother.Text = schoolItem.ProfDevTopicOther;
        //    txtPDTother.Visible = true;
        //}
        //else
        //    txtPDTother.Visible = false;
        if (!string.IsNullOrEmpty(schoolItem.DeliveryMethod))
        {
            string[] DeliveryMethodIDs = schoolItem.DeliveryMethod.Split(',');

            foreach (ListItem itm in cbxlstDeliveryMethod.Items)
            {
                itm.Selected = false;
                foreach (string strid in DeliveryMethodIDs)
                {
                    if (strid == itm.Value)
                    {
                        itm.Selected = true;
                    }

                }
            }
        }
        //ddlDeliveryMethod.SelectedValue = schoolItem.DeliveryMethod;
        txtInstructOther.Text = "";
        if (!string.IsNullOrEmpty(schoolItem.DeliveryMethodOther))
        {
            txtInstructOther.Text = schoolItem.DeliveryMethodOther;
        }
        
           // var dm = TALogDropdownList.SingleOrDefault(x => x.id == Convert.ToInt32(string.IsNullOrEmpty(schoolItem.DeliveryMethod) ? "0" : schoolItem.DeliveryMethod));
           // if (dm != null && dm.name != null && (dm.name.ToLower().Trim() == "other" || dm.name.ToLower().Trim() == "combination of methods"))
        txtInstructOther.Visible = false;
        foreach (ListItem item in cbxlstDeliveryMethod.Items)
        {
            if (item.Selected)
                if (item.Text.Trim().ToLower() == "other")
                txtInstructOther.Visible = true;               
        }

        ddlengageAct.SelectedValue = schoolItem.engageAct;

        ddlSustainAct.SelectedValue = schoolItem.SustainAct;

        ddlCommPartAct.SelectedValue = schoolItem.CommPartAct;

        txtOther.Text = schoolItem.SchoolOther;

        setDDlfied(schoolItem.TAPartnerships);

        txtPartnershipOther.Text = schoolItem.TAPartnershipOther;



        initialThemeCoordinPans();
        initialEngageCoordinPans();
    }

    protected void OnEditSchool(object sender, EventArgs e)
    {
        Session["showTheme"] = "0";
        Session["showEngage"] = "0";

        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        hfeditSchoolID.Value = this.gdvSchool.DataKeys[row.RowIndex].Values["ID"].ToString();
        Session["mgtSchoolID"] = hfeditSchoolID.Value;
        pnlSchoolgv.Visible = false;
        PopupPanel.Visible = true;

        InitalDropdownlists();
        initialFields();

        //ddlschoolDesignation.DataBind();

    }

    //the eventhandler
    protected void CheckChanged(object sender, EventArgs e)
    {
        var mschool = MagnetSchool.SingleOrDefault(s => s.ID == Convert.ToInt32(hfeditSchoolID.Value));
        CheckBoxList lstBox = (CheckBoxList)sender; //the checkboxlist that raised the event
        if (mschool != null)
        {
            foreach (ListItem item in lstBox.Items)
            {
                if (Regex.IsMatch(mschool.TAthemeSubCat, item.Value))
                    item.Selected = true;
            }
        }
    }

    private void InitalDropdownlists()
    {
        string[] ddlTypes = "CT,PDA,PDT,IDM,MSAPStaff".Split(',');
        foreach (string type in ddlTypes)
        {
            string otherID = "", otherName = "other";
            DataTable dt = TALogDropdownList.Find(x => x.type == type).OrderBy(x => x.name).ToDataTable();
            switch (type)
            {
                case "CT":
                    ddlCurriculumnType.Items.Clear();
                    ddlCurriculumnType.Items.Add(new ListItem("Select one", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        ddlCurriculumnType.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    ddlCurriculumnType.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "MR":
                    ddlMarketingrecruitment.Items.Clear();
                    ddlMarketingrecruitment.Items.Add(new ListItem("Select one", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        ddlMarketingrecruitment.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    ddlMarketingrecruitment.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "PDA":  //Professional development activities
                    cblstProvDevelopmentAct.Items.Clear();
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        cblstProvDevelopmentAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    cblstProvDevelopmentAct.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "PDT":  //Professional development Topices
                    cblstPDT.Items.Clear();
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        cblstPDT.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    cblstPDT.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "IDM":
                    cbxlstDeliveryMethod.Items.Clear();
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        cbxlstDeliveryMethod.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    cbxlstDeliveryMethod.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "PEA":
                    ddlengageAct.Items.Clear();
                    ddlengageAct.Items.Add(new ListItem("Select one", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        ddlengageAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    ddlengageAct.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "SA":
                    ddlSustainAct.Items.Clear();
                    ddlSustainAct.Items.Add(new ListItem("Select one", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        ddlSustainAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    ddlSustainAct.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "CPA":
                    ddlCommPartAct.Items.Clear();
                    ddlCommPartAct.Items.Add(new ListItem("Select one", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        ddlCommPartAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    ddlCommPartAct.Items.Add(new ListItem(otherName, otherID));
                    break;
                case "MSAPStaff":
                    cbxlstSpecialist.Items.Clear();
                    //cbxlstSpecialist.Items.Add(new ListItem("Select one", ""));
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw["name"].ToString().Trim().ToLower() == "other")
                        {
                            otherID = rw["id"].ToString();
                            otherName = rw["name"].ToString();
                        }
                        else
                        cbxlstSpecialist.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                    }
                    cbxlstSpecialist.Items.Add(new ListItem(otherName, otherID));
                    break;
            }
        }

    }
    protected void SaveToDB(object sender, EventArgs e)
    {
        //Always getting 1st year APR reports.
        int cohortType = Convert.ToInt32(rbtnCohortType.SelectedValue);
        int reportPeriodID = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType).Last().ID;

        int theSchoolID = Convert.ToInt32(hfeditSchoolID.Value);
        int granteeID = Convert.ToInt32(ddlGrantees.SelectedValue);
        int reportID = GranteeReport.SingleOrDefault(x => x.GranteeID == granteeID && x.ReportPeriodID == reportPeriodID).ID;

        var granteeDataItem = MagnetGranteeDatum.SingleOrDefault(x => x.GranteeReportID == reportID);
        var granteeItem = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlGrantees.SelectedValue));
        //var gpraItem = MagnetGPRA.SingleOrDefault(x => x.ReportID == reportID && x.SchoolID == theSchoolID);
        var schoolItem = MagnetSchool.SingleOrDefault(x => x.ID == theSchoolID);

        schoolItem.ProgramStatus = ddlProgramStatus.Text.Trim();
        schoolItem.Urbanicity = ddlUrbanicity.SelectedValue.Trim();
        schoolItem.FreeRedueLunchRange = ddlDifRange.SelectedValue;
        //schoolItem.FreeReduceLunch = FreeReduceLunchTextBox.Text.Trim();
        //gpraItem.TitleISchoolFunding = ddlTitle1Status.SelectedValue=="" ? (bool?)null : ddlTitle1Status.SelectedValue=="0" ? false : true;
        //if (gpraItem != null)
        //{
        //    gpraItem.TitleISchoolFunding = string.IsNullOrEmpty(txtTitle1Status.Text) ? (bool?)null : (txtTitle1Status.Text.ToLower() == "yes" ? true : false);
        //    //gpraItem.TitleISchoolFundingImprovementStatus = ddlschoolDesignation.SelectedValue=="" ? (int?)null : Convert.ToInt32(ddlschoolDesignation.SelectedValue);
        //    //gpraItem.PersistentlyLlowestAchievingSchool = ddlLowestAchieve.SelectedValue=="" ? (bool?)null : ddlLowestAchieve.SelectedValue =="0" ? false : true;
        //    gpraItem.PersistentlyLlowestAchievingSchool = string.IsNullOrEmpty(txtLowestAchieve.Text) ? (bool?)null : txtLowestAchieve.Text.Trim() == "No" ? false : true;
        //    //gpraItem.SchoolImprovementGrant = ddlSchoolGrant.SelectedValue=="" ? (bool?)null : ddlSchoolGrant.SelectedValue=="0" ? false : true;
        //    gpraItem.SchoolImprovementGrant = string.IsNullOrEmpty(txtSchoolGrant.Text) ? (bool?)null : txtSchoolGrant.Text.ToLower() == "no" ? false : true;
        //}
        string strTmp = "";
        foreach (ListItem li in cbxlstMGI.Items)
        {
            if (li.Selected)
            {
                if (strTmp == "")
                    strTmp = li.Value;
                else
                    strTmp += ";" + li.Value;
            }
        }
        schoolItem.MGIObjectives = strTmp;

        strTmp = "";
        foreach (ListItem li in cblstMinGroup.Items)
        {
            if (li.Selected)
            {
                if (strTmp == "")
                    strTmp = li.Value;
                else
                    strTmp += ";" + li.Value;
            }
        }
        schoolItem.MinorityGroup = strTmp;
        strTmp = "";
        foreach (ListItem li in cbxlstSpecialist.Items)
        {
            if (li.Selected)
            {
                if (strTmp == "")
                    strTmp = li.Value;
                else
                    strTmp += ";" + li.Value;
            }
        }
        schoolItem.Specialists = strTmp;

        schoolItem.SpecialistOther = txtSpecialistOther.Text.Trim();

        schoolItem.DesegregationPlan = ddlDesegregationPlan.SelectedValue;
        if (!string.IsNullOrEmpty(ddlDesegregationPlan.SelectedValue))
            schoolItem.DesegregationSubPlan = ddlDesegregationSubPlan.SelectedValue;
      

        schoolItem.AttendType = ddlAttendType.SelectedValue;

        schoolItem.AdmissionMethod = ddlAdminMethod.SelectedItem.Text;

        schoolItem.AdminMethodOther = txtAdminOther.Text.Trim();

        string str = "";
        if (ddlAdminMethod.SelectedItem.Text.ToLower() == "weighted lottery")
        {
            foreach (ListItem li in cbxlstWlottery.Items)
            {
                if (li.Selected)
                {
                    if (!string.IsNullOrEmpty(str))
                        str += ";";
                    str += li.Value;
                }
            }

            schoolItem.wlotterytype = str;
        }

        if (Session["showTheme"] != null && Session["showTheme"].ToString() == "1")
        {
            //save Theme sub categories 
            var themecats = TAThemeCat.All();
            string themecatids = "", themesubcatids = "";
            //bool is1stCat = false;

            //schoolItem.MagnetTheme = "";

            foreach (RadPanelItem pnlItem in radpnlbarCategory.Items)
            {
                bool isNotSaved = true;
                CheckBoxList cblist = pnlItem.Items[0].FindControl("ThemeSubCat" + pnlItem.Value) as CheckBoxList;


                if (cblist != null)
                {
                    if (cblist.Items.Count > 0)
                    {
                        foreach (ListItem itm in cblist.Items)
                        {
                            if (itm.Selected)
                            {
                                //if (!is1stCat)
                                //{
                                //    if (ddlPrimarytheme != null && ddlPrimarytheme.SelectedValue != null)
                                //        schoolItem.MagnetTheme = ddlPrimarytheme.SelectedValue;
                                //        //schoolItem.MagnetTheme = pnlItem.Text;
                                //    is1stCat = true;
                                //}

                                if (isNotSaved)
                                {
                                    themecatids += TAThemeSubCat.SingleOrDefault(sb => sb.id == Convert.ToInt32(itm.Value)).ThemeCatID + ",";
                                    isNotSaved = false;
                                }
                                themesubcatids += itm.Value + ",";
                            }
                        }
                    }


                    themesubcatids = themesubcatids.TrimEnd(',');
                    if (!isNotSaved)
                        themesubcatids += ";";
                }

            }
            themecatids = themecatids.TrimEnd(',');
            themesubcatids = themesubcatids.TrimEnd(';');

            schoolItem.TAthemeCat = themecatids;
            schoolItem.TAthemeSubCat = themesubcatids;
        }
        else
        {
            if (ddlPrimarytheme != null && ddlPrimarytheme.SelectedValue != null)
                schoolItem.MagnetTheme = ddlPrimarytheme.SelectedValue;
            else
                schoolItem.MagnetTheme = "";
        }


        if (Session["showEngage"] != null && Session["showEngage"].ToString() == "1")
        {
            //////Parent Engagement/////////////////////////////////////
            //save Theme sub categories 
            var ParentEngagementcats = TAParentEngagementCat.All();
            string pengagecatids = "", pengagesubcatids = "";

            foreach (RadPanelItem grantpanel in rpnlBarPEngage.Items)
            {
                foreach (RadPanelItem pnlItem in grantpanel.Items)
                {
                    int level = Convert.ToInt32(pnlItem.Value.Split('|')[0]);
                    int upbound = Convert.ToInt32(pnlItem.Value.Split('|')[1]);

                    bool isNotSaved = true;
                    for (int i = 1; i <= upbound; i++)
                    {
                        CheckBox mycbx = pnlItem.FindControl("cbx" + level + i) as CheckBox;
                        TextBox myTextbox = pnlItem.FindControl("txtOther" + level + i) as TextBox;

                        if (mycbx != null && mycbx.Checked)
                        {
                            if (isNotSaved)
                            {
                                pengagecatids += TAParentEngagementCat.SingleOrDefault(x => x.ordernum == level).id.ToString() + ",";
                                isNotSaved = false;
                            }
                            int catid = TAParentEngagementCat.SingleOrDefault(x => x.ordernum == level).id;
                            pengagesubcatids += TAParentEngagementSubCat.SingleOrDefault(x => x.SubCatName == mycbx.Text && x.CatID == catid).id.ToString() + ",";
                            if (myTextbox != null)
                                pengagesubcatids = pengagesubcatids.TrimEnd(',') + "&" + HttpUtility.HtmlEncode(myTextbox.Text);
                        }

                    }
                    pengagesubcatids = pengagesubcatids.TrimEnd(',');
                    if (!isNotSaved)
                        pengagesubcatids += ">";



                    //bool isNotSaved = true;
                    //CheckBoxList cblist = pnlItem.Items[0].FindControl("pengageSubCat" + pnlItem.Value) as CheckBoxList;
                    //TextBox ptxtother = pnlItem.Items[1].FindControl("txtOther_" + pnlItem.Value) as TextBox;

                    //if (cblist != null)
                    //{
                    //    foreach (ListItem itm in cblist.Items)
                    //    {
                    //        if (itm.Selected)
                    //        {
                    //            if (isNotSaved)
                    //            {
                    //                pengagecatids += TAParentEngagementSubCat.SingleOrDefault(sb => sb.id == Convert.ToInt32(itm.Value)).CatID + ",";
                    //                isNotSaved = false;
                    //            }
                    //            pengagesubcatids += itm.Value + ",";
                    //            if (itm.Text == "Other")
                    //                pengagesubcatids = pengagesubcatids.TrimEnd(',') + "&" + HttpUtility.HtmlEncode(ptxtother.Text);
                    //        }
                    //    }
                    //    pengagesubcatids = pengagesubcatids.TrimEnd(',');
                    //    if (!isNotSaved)
                    //        pengagesubcatids += ">";
                    //}
                }
            }
            pengagecatids = pengagecatids.TrimEnd(',');
            pengagesubcatids = pengagesubcatids.TrimEnd(';');

            schoolItem.TApengageCat = pengagecatids;
            schoolItem.TApengageSubCat = pengagesubcatids;
        }
        ///////////////////////////////////////////////////////////

        schoolItem.CurriculumnType = ddlCurriculumnType.SelectedValue;
        //////////////Marketing Recruitment setion//////////////////////////////
        string stringlist = "";
        ///////sestion 1/////////////////////////////////
        stringlist += ddlmc11.SelectedValue + "'";
        stringlist += HttpUtility.HtmlEncode(mcOther11.Text) + "'";
        stringlist += HttpUtility.HtmlEncode(txtmc12.Text) + "'";
        stringlist += ddlmc13.SelectedValue + "'";
        stringlist += ddlmc14.SelectedValue + "'";
        stringlist += HttpUtility.HtmlEncode(txtmc15.Text) + ">";

        stringlist += ddlmc21.SelectedValue + "'";
        stringlist += HttpUtility.HtmlEncode(mcOther21.Text) + "'";
        stringlist += HttpUtility.HtmlEncode(txtmc22.Text) + "'";
        stringlist += ddlmc23.SelectedValue + "'";
        stringlist += ddlmc24.SelectedValue + "'";
        stringlist += HttpUtility.HtmlEncode(txtmc25.Text) + ">";

        stringlist += ddlmc31.SelectedValue + "'";
        stringlist += HttpUtility.HtmlEncode(mcOther31.Text) + "'";
        stringlist += HttpUtility.HtmlEncode(txtmc32.Text) + "'";
        stringlist += ddlmc33.SelectedValue + "'";
        stringlist += ddlmc34.SelectedValue + "'";
        stringlist += HttpUtility.HtmlEncode(txtmc35.Text);

        ///////sestion 2/////////////////////////////////
        stringlist += "&";
        foreach (RadPanelItem grantpanel in rpnlMarketRec.Items)
        {
            foreach (RadPanelItem ctlItem in grantpanel.Items)
            {
                ctlItem.Expanded = false;

                int position = Convert.ToInt32(ctlItem.Value.Split('|')[0]),
                    controlNum = Convert.ToInt32(ctlItem.Value.Split('|')[1]);
                for (int i = 1; i <= controlNum; i++)
                {
                    CheckBox mcCbx = ctlItem.FindControl("cbx" + position + i) as CheckBox;
                    TextBox txtmcOther = ctlItem.FindControl("txtOther" + position + i) as TextBox;

                    if (mcCbx != null)
                    {
                        if (mcCbx.Checked)
                            stringlist += mcCbx.ID + "\"" + 1 + "'";
                        else
                            stringlist += mcCbx.ID + "\"" + 0 + "'";
                    }

                    if (txtmcOther != null)
                    {
                        stringlist += txtmcOther.ID + "\"" + HttpUtility.HtmlEncode(txtmcOther.Text) + "'";
                    }

                }
                stringlist = stringlist.TrimEnd('\'') + "<";
            }
            stringlist = stringlist.TrimEnd('\'') + ">";
        }
        stringlist = stringlist.TrimEnd('>');

        schoolItem.Marketingrecruitment = stringlist;
        //////////////end Marketing Recruitment setion//////////////////////////////

        //schoolItem.ProfDevAct = ddlProfDevAct.SelectedValue;
        string idlist = "";
        foreach (ListItem itm in cblstProvDevelopmentAct.Items)
        {
            if (itm.Selected)
            {
                idlist += itm.Value + ",";
            }
        }
        idlist = idlist.TrimEnd(',');
        schoolItem.ProfDevAct = idlist;
        schoolItem.ProfDevActOther = txtPDAOther.Text.Trim();

        idlist = "";
        foreach (ListItem itm in cblstPDT.Items)
        {
            if (itm.Selected)
            {
                idlist += itm.Value + ",";
            }
        }
        idlist = idlist.TrimEnd(',');
        schoolItem.ProfDevTopic = idlist;
        schoolItem.ProfDevTopicOther = txtPDTother.Text.Trim();

        idlist = "";
        foreach (ListItem itm in cbxlstDeliveryMethod.Items)
        {
            if (itm.Selected)
            {
                idlist += itm.Value + ",";
            }
        }
        idlist = idlist.TrimEnd(',');

        schoolItem.DeliveryMethod = idlist;

        schoolItem.DeliveryMethodOther = txtInstructOther.Text;

        schoolItem.engageAct = ddlengageAct.SelectedValue;

        schoolItem.SustainAct = ddlSustainAct.SelectedValue;

        schoolItem.CommPartAct = ddlCommPartAct.SelectedValue;

        schoolItem.SchoolOther = txtOther.Text.Trim();

        schoolItem.ModifiedBy = HttpContext.Current.User.Identity.Name;
        schoolItem.ModifiedOn = DateTime.Now;

        string datalist = "", tmpdata = "";
        for (int i = 1; i <= 6; i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                DropDownList myddl = this.Master.FindControl("ContentPlaceHolder1").FindControl("ddl" + i + j) as DropDownList;
                tmpdata += myddl.SelectedValue + ",";
            }
            //tmpdata = tmpdata.TrimEnd(',');
            tmpdata += ";";
        }
        //datalist = tmpdata.TrimEnd(',').TrimEnd(';');
        datalist = tmpdata.TrimEnd(';');

        schoolItem.TAPartnerships = datalist;

        schoolItem.TAPartnershipOther = txtPartnershipOther.Text.Trim();

        granteeDataItem.Save();
        granteeItem.Save();

        //if(gpraItem!=null)
        //    gpraItem.Save();

        schoolItem.Save();

        gdvSchool.DataBind();

        if (HttpContext.Current.User.IsInRole("ED"))
        {
            TAlog talogOjb = new TAlog();
            string user = HttpContext.Current.User.Identity.Name;
            string tablename = "Contextual Factors";
            string datestamp = DateTime.Now.ToString();
            string Action = "Updated";
            talogOjb.TALogDatahasbeenChangedbyED(user, tablename, datestamp, Action);
        }
        Button btn = sender as Button;

        if (btn.ID == "btnFEngage" || btn.ID == "btnFtheme")
        {
        }
        else
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }


    protected void OnSaveSchool(object sender, EventArgs e)
    {
        SaveToDB(sender, e);
        ClosePan(this, e);
    }


    protected void OnDeleteSchool(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gdvSchool.DataKeys[row.RowIndex].Values["ID"]);
        var data = schooldb.vwTAlogcontextualfactors.SingleOrDefault(x => x.ID == rcdid);
        data.isTALogActive = false;
        schooldb.SubmitChanges();

        gdvSchool.DataBind();
    }

    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
            Session["TALogGranteeID"] = ddlGrantees.SelectedValue;
        ClosePan(sender, e);
        //ddlPeriod.SelectedValue = Session["TALogCohortPeriod"].ToString();
    }
    protected void rbtnCohortType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClosePan(sender, e);
        ddlGrantees.Items.Clear();
        ddlGrantees.Items.Add(new System.Web.UI.WebControls.ListItem("Select one", ""));
        Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
        Session["TALogGranteeID"] = null;
        ddlGrantees.DataBind();
    }

    protected void OnddlEdit(object sender, EventArgs e)
    {
        Session["hfddltype"] = (sender as Button).CommandArgument;

        BindData();
        mpeNewsWindow.Show();
    }




    protected void ClosePan(object sender, EventArgs e)
    {
        pnlSchoolgv.Visible = true;
        PopupPanel.Visible = false;
    }

    private void BindData()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
        con.Open();
        SqlDataAdapter da = new SqlDataAdapter("SELECT id, name, type, orderby FROM TALogDropdownLists where type = '" + Session["hfddltype"].ToString() + "' order by name ", con);
        
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();

        gvDDLlist.DataSource = dt;
        gvDDLlist.DataBind();

        string otherID = "", otherName = "";
        switch (Session["hfddltype"].ToString().Trim())
        {
            case "MSAPStaff":
                cbxlstSpecialist.Items.Clear();
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                        cbxlstSpecialist.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                cbxlstSpecialist.Items.Add(new ListItem(otherName, otherID));
                cbxlstSpecialist.DataBind();
                break;
            case "CT":
                ddlCurriculumnType.Items.Clear();
                ddlCurriculumnType.Items.Add(new ListItem("Select one", ""));
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    ddlCurriculumnType.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                ddlCurriculumnType.Items.Add(new ListItem(otherName, otherID));
                ddlCurriculumnType.DataBind();
                break;
            case "MR":
                ddlMarketingrecruitment.Items.Clear();
                ddlMarketingrecruitment.Items.Add(new ListItem("Select one", ""));
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    ddlMarketingrecruitment.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                ddlMarketingrecruitment.Items.Add(new ListItem(otherName, otherID));
                ddlMarketingrecruitment.DataBind();
                break;
            case "PDA":
                cblstProvDevelopmentAct.Items.Clear();
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    cblstProvDevelopmentAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                cblstProvDevelopmentAct.Items.Add(new ListItem(otherName, otherID));
                cblstProvDevelopmentAct.DataBind();
                break;
            case "PDT":
                cblstPDT.Items.Clear();
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    cblstPDT.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                cblstPDT.Items.Add(new ListItem(otherName, otherID));
                cblstPDT.DataBind();
                break;

            case "IDM":
                cbxlstDeliveryMethod.Items.Clear();
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    cbxlstDeliveryMethod.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                cbxlstDeliveryMethod.Items.Add(new ListItem(otherName, otherID));
                cbxlstDeliveryMethod.DataBind();
                break;
            case "PEA":
                ddlengageAct.Items.Clear();
                ddlengageAct.Items.Add(new ListItem("Select one", ""));
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    ddlengageAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                ddlengageAct.Items.Add(new ListItem(otherName, otherID));
                ddlengageAct.DataBind();
                break;
            case "SA":
                ddlSustainAct.Items.Clear();
                ddlSustainAct.Items.Add(new ListItem("Select one", ""));
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    ddlSustainAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                ddlSustainAct.Items.Add(new ListItem(otherName, otherID));
                ddlSustainAct.DataBind();
                break;
            case "CPA":
                ddlCommPartAct.Items.Clear();
                ddlCommPartAct.Items.Add(new ListItem("Select one", ""));
                foreach (DataRow rw in dt.Rows)
                {
                    if (rw["name"].ToString().Trim().ToLower() == "other")
                    {
                        otherID = rw["id"].ToString();
                        otherName = rw["name"].ToString();
                    }
                    else
                    ddlCommPartAct.Items.Add(new ListItem(rw["name"].ToString(), rw["id"].ToString()));
                }
                ddlCommPartAct.Items.Add(new ListItem(otherName, otherID));
                ddlCommPartAct.DataBind();
                break;
        }

    }

    protected void OnNewddl(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("SELECT id, name, type FROM TALogDropdownLists where type = '" + Session["hfddltype"].ToString() + "' order by name ", con);

        DataTable dt = new DataTable();
        da.Fill(dt);

        // Here we'll add a blank row to the returned DataTable
        DataRow dr = dt.NewRow();
        dt.Rows.InsertAt(dr, 0);
        //Creating the first row of GridView to be Editable
        //dt.Rows[0][0] = TADropdownlist.All().Last().id + 1;
        dt.Rows[0][2] = Session["hfddltype"].ToString();
        gvDDLlist.EditIndex = 0;
        gvDDLlist.DataSource = dt;
        gvDDLlist.DataBind();
        //Changing the Text for Inserting a New Record
        ((LinkButton)gvDDLlist.Rows[0].Cells[0].Controls[0]).Text = "Insert";
        mpeNewsWindow.Show();
    }

    protected void gvDDLlist_RowEditing(object sender, GridViewEditEventArgs e)
    {

        gvDDLlist.EditIndex = e.NewEditIndex;

        BindData();
        mpeNewsWindow.Show();
    }

    protected void gvDDLlist_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        gvDDLlist.EditIndex = -1;

        BindData();
        mpeNewsWindow.Show();
    }

    protected void gvDDLlist_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        if (((LinkButton)gvDDLlist.Rows[0].Cells[0].Controls[0]).Text == "Insert")
        {

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "INSERT INTO TALogDropdownLists(name, type) VALUES(@name, @type)";
            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = ((TextBox)gvDDLlist.Rows[0].Cells[2].Controls[0]).Text;
            cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = gvDDLlist.Rows[0].Cells[3].Text;
            cmd.Connection = con;

            con.Open();

            cmd.ExecuteNonQuery();

            con.Close();

        }

        else
        {

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "UPDATE TALogDropdownLists SET name=@name, type = @type WHERE id=@id";
            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = ((TextBox)gvDDLlist.Rows[e.RowIndex].Cells[2].Controls[0]).Text;
            cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = gvDDLlist.Rows[e.RowIndex].Cells[3].Text;
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(gvDDLlist.Rows[e.RowIndex].Cells[1].Text);
            cmd.Connection = con;

            con.Open();

            cmd.ExecuteNonQuery();

            con.Close();
        }
        BindData();
    }

    protected void gvDDLlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString);
        SqlCommand cmd = new SqlCommand();

        cmd.CommandText = "DELETE FROM TALogDropdownLists WHERE id=@id"; cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(gvDDLlist.Rows[e.RowIndex].Cells[1].Text);
        cmd.Connection = con;

        con.Open();

        cmd.ExecuteNonQuery();

        con.Close();

        BindData();

    }


    protected void FlipTheme(object sender, EventArgs e)
    {
        
        if (Session["showTheme"] == null || Session["showTheme"].ToString() == "0")
        {
            btnFtheme.Text = "Show Saved Records";
            Session["showTheme"] = "1";
            radpnlbarCategory.Visible = true;
            radpnlbarCategoryReadMode.Visible = false;
            btnSaveTheme.Visible = true;
            ThemeEditMode();
        }
        else
        {
            btnFtheme.Text = "Add/edit data";
            Session["showTheme"] = "0";
            radpnlbarCategory.Visible = false;
            radpnlbarCategoryReadMode.Visible = true;
            btnSaveTheme.Visible = false;
            initialThemeCoordinPans();
        }

        if (Session["showEngage"] == null || Session["showEngage"].ToString() == "0")
        {
            rpnlBarPEngageReadMode.Visible = true;
            rpnlBarPEngage.Visible = false;
            initialEngageCoordinPans();
        }
        else
        {
            SaveToDB(sender, e);
            rpnlBarPEngageReadMode.Visible = false;
            rpnlBarPEngage.Visible = true;
            EngageEditMode();
        }



    }
    protected void FlipEngage(object sender, EventArgs e)
    {
        
        if (Session["showEngage"] == null || Session["showEngage"].ToString() == "0")  //show all options
        {
            rpnlBarPEngageReadMode.Visible = false;
            rpnlBarPEngage.Visible = true;
            btnFEngage.Text = "Show Saved Records";
            Session["showEngage"] = "1";
            btnSaveParent.Visible = true;
            EngageEditMode();
        }
        else
        {
            rpnlBarPEngageReadMode.Visible = true;
            rpnlBarPEngage.Visible = false;
            btnFEngage.Text = "Add/edit data";
            Session["showEngage"] = "0";
            btnSaveParent.Visible = false;
            initialEngageCoordinPans();
        }

        if (Session["showTheme"] == null || Session["showTheme"].ToString() == "0")
        {
            radpnlbarCategory.Visible = false;
            radpnlbarCategoryReadMode.Visible = true;
            initialThemeCoordinPans();
        }
        else
        {
            SaveToDB(sender, e);
            radpnlbarCategory.Visible = true;
            radpnlbarCategoryReadMode.Visible = false;
            //string fieldVals = getThemeStatus();
            ThemeEditMode();
            //SetThemeStatus(fieldVals);
        }
    }
    private string getThemeStatus()
    {
        string fieldVals="";


        return fieldVals;
    }

    private void SetThemeStatus(string fieldVals)
    {
        string[] Allsubthemes = string.IsNullOrEmpty(fieldVals.Trim()) ? fieldVals.Split(';') : new string[0];

        foreach (RadPanelItem cat in radpnlbarCategory.Items)
        {
            cat.Expanded = false;

            TextBox ptxtother = new TextBox();
            ptxtother.ID = "txtOther_" + cat.Value;

            CheckBoxList cblist = cat.Items[0].FindControl("ThemeSubCat" + cat.Value) as CheckBoxList;

            cblist.Attributes.Add("style", "font-size: 11px;font-family: Helvetica,Arial,Sans-serif!important;");

            cblist.ClearSelection();

            foreach (string listids in Allsubthemes)
            {
                if (!string.IsNullOrEmpty(listids))
                {
                    foreach (string strid in listids.Split(':')[0].Split(','))
                    {
                        ListItem listItem = cblist.Items.FindByValue(strid);
                        if (listItem != null)
                        {
                            listItem.Selected = true;
                        }
                    }


                }
            }

        }
    }

    private void ThemeEditMode()
    {
        if (string.IsNullOrEmpty(hfeditSchoolID.Value))
            hfeditSchoolID.Value = Session["mgtSchoolID"].ToString();

        var mschool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfeditSchoolID.Value));
        string[] themes = mschool.TAthemeCat != null ? mschool.TAthemeCat.Split(',') : new string[0];
        string[] Allsubthemes = mschool.TAthemeSubCat != null ? mschool.TAthemeSubCat.Split(';') : new string[0];

        foreach (RadPanelItem cat in radpnlbarCategory.Items)
        {
            cat.Expanded = false;

            TextBox ptxtother = new TextBox();
            ptxtother.ID = "txtOther_" + cat.Value;
           
            CheckBoxList cblist = cat.Items[0].FindControl("ThemeSubCat" + cat.Value) as CheckBoxList;

            cblist.Attributes.Add("style", "font-size: 11px;font-family: Helvetica,Arial,Sans-serif!important;");

            cblist.ClearSelection();

            foreach (string listids in Allsubthemes)
            {
                if (!string.IsNullOrEmpty(listids))
                {
                    foreach (string strid in listids.Split(':')[0].Split(','))
                    {
                        ListItem listItem = cblist.Items.FindByValue(strid);
                        if (listItem != null)
                        {
                            listItem.Selected = true;
                        }
                    }


                }
            }

        }

    }

    private void EngageEditMode()
    {
        if (string.IsNullOrEmpty(hfeditSchoolID.Value))
            hfeditSchoolID.Value = Session["mgtSchoolID"].ToString();

        var mschool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfeditSchoolID.Value));
        string[] pEngages = mschool.TApengageCat != null ? mschool.TApengageCat.Split(',') : new string[0];
        string[] AllengageSubs = mschool.TApengageSubCat != null ? mschool.TApengageSubCat.Split('>') : new string[0];

        foreach (RadPanelItem grantpanel in rpnlBarPEngage.Items)
        {
            grantpanel.Expanded = false;
            foreach (RadPanelItem pnlItem in grantpanel.Items)
            {
                pnlItem.Expanded = false;
                int level = Convert.ToInt32(pnlItem.Value.Split('|')[0]);
                int upbound = Convert.ToInt32(pnlItem.Value.Split('|')[1]);

                for (int i = 1; i <= upbound; i++)
                {
                    CheckBox mycbx = pnlItem.FindControl("cbx" + level + i) as CheckBox;
                    mycbx.Checked = false;

                    TextBox myTextbox = pnlItem.FindControl("txtOther" + level + i) as TextBox;

                    if (myTextbox != null && mycbx != null)
                    {
                        myTextbox.Text = "";
                        myTextbox.Visible = false;
                    }

                    foreach (string listids in AllengageSubs)
                    {
                        if (!string.IsNullOrEmpty(listids))
                        {
                            foreach (string strid in listids.Split('&')[0].Split(','))
                            {
                                var data = TAParentEngagementSubCat.SingleOrDefault(x => x.id == Convert.ToInt32(strid));
                                string subCatname = data.SubCatName.ToString();

                                if (grantpanel.Value == data.CatID.ToString())
                                {
                                    if (subCatname.ToLower().Trim() == mycbx.Text.ToLower().Trim())
                                    {
                                        mycbx.Checked = true;
                                        if (listids.Split('&').Count() > 1 && myTextbox != null)
                                        {
                                            myTextbox.Text = HttpUtility.HtmlDecode(listids.Split('&')[1]);
                                            myTextbox.Visible = true;

                                        }

                                    }
                                }

                            }


                        }

                    }

                }
               
            }
        }

    }
    private void EngageEditMode_org()
    {
        if (string.IsNullOrEmpty(hfeditSchoolID.Value))
            hfeditSchoolID.Value = Session["mgtSchoolID"].ToString();

       // rpnlBarPEngage.Items.Clear();

        var mschool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfeditSchoolID.Value));
        string[] pEngages = mschool.TApengageCat != null ? mschool.TApengageCat.Split(',') : new string[0];
        string[] AllengageSubs = mschool.TApengageSubCat != null ? mschool.TApengageSubCat.Split(';') : new string[0];
        int ii = 0;

        //var cats = TAParentEngagementCat.All().OrderBy(x => x.CatName);

        //foreach (var cat in cats)
        foreach(RadPanelItem cat in rpnlBarPEngage.Items)
        {

            //RadPanelItem catItem = new RadPanelItem();
            //catItem.Text = cat.Text;
            //catItem.Value = cat.Value;
            cat.Expanded = false;

            //CheckBoxList cbsubTheme = new CheckBoxList();
            TextBox otherTxtbox = new TextBox();
            otherTxtbox.ID = "txtOther_" + cat.Value;

            //var engageSubCat = TAParentEngagementSubCat.Find(sub => sub.CatID == Convert.ToInt32(cat.Value)).OrderBy(odr => odr.SubCatName);
            CheckBoxList cbsubTheme = cat.Items[0].FindControl("pengageSubCat" + cat.Value) as CheckBoxList;
            if (cat.Items.Count > 1)
            {
                otherTxtbox = cat.Items[1].FindControl("txtOther_" + cat.Value) as TextBox;
                otherTxtbox.Text = "";
            }

            int jj = 0;
            string txtOther = "";
            string[] subcats = new string[0];
            if (ii < pEngages.Count())
            {
                if (!string.IsNullOrEmpty(AllengageSubs[ii]))
                {
                    if (AllengageSubs[ii].Split(':').Count() > 1)
                        txtOther = AllengageSubs[ii].Split(':')[1];

                    subcats = AllengageSubs[ii].Split(':')[0].Split(',');
                }

            }
            foreach (string strid in subcats)
            {
                ListItem listItem = cbsubTheme.Items.FindByText(strid);
                if (listItem != null) listItem.Selected = true;
                otherTxtbox.Text = txtOther;
            }

            //foreach (ListItem itm in cbsubTheme.Items)
            //{
            //    //ListItem ckbx = new ListItem();
            //    //ckbx.Value = itm.id.ToString();
            //    //ckbx.Text = itm.SubCatName;
            //    itm.Selected = false;

            //    if (jj >= subcats.Count())
            //    {
            //        //cbsubTheme.Items.Add(ckbx);
            //        continue;
            //    }

            //    //if (subcats.Count() > 0 && jj < subcats.Count())
            //    {
            //        if (subcats[jj] == itm.Value)
            //        {
            //            itm.Selected = true;
            //            if (!string.IsNullOrEmpty(txtOther))
            //            {
            //                otherTxtbox.Text = txtOther;
            //               // otherTxtbox.Style.Add(HtmlTextWriterStyle.Display, "");
            //            }
            //            jj++;
            //            ii++;
            //        }
            //        //else
            //        //{
            //        //    //itm.Selected = false;
            //        //   // otherTxtbox.Style.Add(HtmlTextWriterStyle.Display, "none");
            //        //}
            //    }
            //    //cbsubTheme.Items.Add(ckbx);

            //}

            //RadPanelItem radPanelItemCheckBoxList = new RadPanelItem();
            //radPanelItemCheckBoxList.Controls.Add(cbsubTheme);
            //catItem.Items.Add(radPanelItemCheckBoxList);

            //RadPanelItem radPanelItemotherTxtbox = new RadPanelItem();
           
            //radPanelItemotherTxtbox.Controls.Add(otherTxtbox);
            //catItem.Items.Add(radPanelItemotherTxtbox);
            //rpnlBarPEngage.Items.Add(catItem);
        }

    }

    private void setDDlfied(string datalist)
    {

        //string list = "111,1,1,;212,2,8,;313,3,11,;414,4,16,;,2,10,;,,,";
        string list = string.IsNullOrEmpty(datalist) ? ",,,;,,,;,,,;,,,;,,,;,,," : datalist;
        string[] dataArry = list.Split(';');
        int i = 1, j = 1;

        foreach (string itmIDs in dataArry)
        {
            j = 1;
            foreach (string ID in itmIDs.Split(','))
            {
                if (j >= 4)
                    break;

                DropDownList myddl = this.Master.FindControl("ContentPlaceHolder1").FindControl("ddl" + i + j) as DropDownList;
                myddl.ClearSelection();

                if (j == 3)
                {
                    myddl.Items.Clear();
                    myddl.Items.Add(new ListItem("Select one", ""));
                }

                if (!string.IsNullOrEmpty(ID))
                {
                    if (j == 3)
                    {
                        int inputID = string.IsNullOrEmpty(itmIDs.Split(',')[1]) ? 0 : Convert.ToInt32(itmIDs.Split(',')[1]);
                        var data = TAPartnership.Find(x => x.col2_id == inputID);
                        myddl.DataTextField = "col3ItemName";
                        myddl.DataValueField = "id";
                        myddl.DataSource = data;
                        myddl.DataBind();
                    }

                    myddl.SelectedValue = ID;
                }

                //myddl.SelectedValue = ID;
                j++;
            }
            i++;
        }

    }
    protected void onMySelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList myddl = sender as DropDownList;
        int inputID = string.IsNullOrEmpty(myddl.SelectedValue) ? 0 : Convert.ToInt32(myddl.SelectedValue);
        var data = TAPartnership.Find(x => x.col2_id == inputID);

        switch (myddl.ID)
        {
            case "ddl12":
                ddl13.Items.Clear();
                ddl13.Items.Add(new ListItem("Select one", ""));
                ddl13.DataTextField = "col3ItemName";
                ddl13.DataValueField = "id";
                ddl13.DataSource = data;
                ddl13.DataBind();
                break;
            case "ddl22":
                ddl23.Items.Clear();
                ddl23.Items.Add(new ListItem("Select one", ""));
                ddl23.DataTextField = "col3ItemName";
                ddl23.DataValueField = "id";
                ddl23.DataSource = data;
                ddl23.DataBind();
                break;
            case "ddl32":
                ddl33.Items.Clear();
                ddl33.Items.Add(new ListItem("Select one", ""));
                ddl33.DataTextField = "col3ItemName";
                ddl33.DataValueField = "id";
                ddl33.DataSource = data;
                ddl33.DataBind();
                break;
            case "ddl42":
                ddl43.Items.Clear();
                ddl43.Items.Add(new ListItem("Select one", ""));
                ddl43.DataTextField = "col3ItemName";
                ddl43.DataValueField = "id";
                ddl43.DataSource = data;
                ddl43.DataBind();
                break;
            case "ddl52":
                ddl53.Items.Clear();
                ddl53.Items.Add(new ListItem("Select one", ""));
                ddl53.DataTextField = "col3ItemName";
                ddl53.DataValueField = "id";
                ddl53.DataSource = data;
                ddl53.DataBind();
                break;
            case "ddl62":
                ddl63.Items.Clear();
                ddl63.Items.Add(new ListItem("Select one", ""));
                ddl63.DataTextField = "col3ItemName";
                ddl63.DataValueField = "id";
                ddl63.DataSource = data;
                ddl63.DataBind();
                break;

        }
    }
    protected void cbxlstDeliveryMethod_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool isOthercheck = false;
        foreach (ListItem item in cbxlstDeliveryMethod.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                txtInstructOther.Visible = true;
                isOthercheck = true;
            }
            else
            {
                txtInstructOther.Visible = false;
            }
        }
        if (!isOthercheck)
            txtInstructOther.Text = "";
    }
    protected void ddlmc14_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlmc14.SelectedValue == "147")  //other
        //    txtmc15.Visible = true;
        //else
        //{
        //    txtmc15.Visible = false;
        //    txtmc15.Text = "";
        //}
        bool isOthercheck = false;
        foreach (ListItem item in ddlmc14.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                txtmc15.Visible = true;
                isOthercheck = true;
            }
            else
            {
                txtmc15.Visible = false;
                txtmc15.Text = "";
            }
        }
        if (!isOthercheck)
            txtmc15.Text = "";
    }
    protected void ddlmc24_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlmc24.SelectedValue == "247")  //other
        //    txtmc25.Visible = true;
        //else
        //{
        //    txtmc25.Visible = false;
        //    txtmc25.Text = "";
        //}
        bool isOthercheck = false;
        foreach (ListItem item in ddlmc24.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                txtmc25.Visible = true;
                isOthercheck = true;
            }
            else
            {
                txtmc25.Visible = false;
                txtmc25.Text = "";
            }
        }
        if (!isOthercheck)
            txtmc25.Text = "";
    }
    protected void ddlmc34_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlmc34.SelectedValue == "347")  //other
        //    txtmc35.Visible = true;
        //else
        //{
        //    txtmc35.Visible = false;
        //    txtmc35.Text = "";
        //}
        bool isOthercheck = false;
        foreach (ListItem item in ddlmc34.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                txtmc35.Visible = true;
                isOthercheck = true;
            }
            else
            {
                txtmc35.Visible = false;
                txtmc35.Text = "";
            }
        }
        if (!isOthercheck)
            txtmc35.Text = "";
    }
    protected void ddlmc11_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlmc11.SelectedValue == "115")  //other
        //   // mcOther11.Visible = true
        //    ;
        //else
        //{
        //    mcOther11.Visible = false;
        //    mcOther11.Text = "";
        //}
        bool isOthercheck = false;
        foreach (ListItem item in ddlmc11.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                mcOther11.Visible = true;
                isOthercheck = true;
            }
            else
            {
                mcOther11.Visible = false;
                mcOther11.Text = "";
            }
        }
        if (!isOthercheck)
            mcOther11.Text = "";
    }
    protected void ddlmc21_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlmc21.SelectedValue == "215")  //other
        //    //mcOther21.Visible = true
        //    ;
        //else
        //{
        //    mcOther21.Visible = false;
        //    mcOther21.Text = "";
        //}
        bool isOthercheck = false;
        foreach (ListItem item in ddlmc21.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                mcOther21.Visible = true;
                isOthercheck = true;
            }
            else
            {
                mcOther21.Visible = false;
                mcOther21.Text = "";
            }
        }
        if (!isOthercheck)
            mcOther21.Text = "";
    }
    protected void ddlmc31_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlmc31.SelectedValue == "315")  //other
        //    //mcOther31.Visible = true
        //    ;
        //else
        //{
        //    mcOther31.Visible = false;
        //    mcOther31.Text = "";
        //}
        bool isOthercheck = false;
        foreach (ListItem item in ddlmc31.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                mcOther31.Visible = true;
                isOthercheck = true;
            }
            else
            {
                mcOther31.Visible = false;
                mcOther31.Text = "";
            }
        }
        if (!isOthercheck)
            mcOther31.Text = "";
    }

    protected void textboxState(object sender, EventArgs e)
    {
        CheckBox mycbx = sender as CheckBox;
        string rowID = mycbx.ID.Replace("cbx", "");

         //find other text box
        TextBox mytxtBox = mycbx.Parent.FindControl("txtOther" + rowID) as TextBox;

        if (mycbx.Checked)
            mytxtBox.Visible = true;
        else
        {
            mytxtBox.Visible = false;
            mytxtBox.Text = "";
        }
    }
    protected void cbxlstSpecialist_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool isOthercheck = false;
        foreach (ListItem item in cbxlstSpecialist.Items)
        {
            
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                txtSpecialistOther.Visible = true;
                isOthercheck = true;
            }
            else
            {
                txtSpecialistOther.Visible = false;
            }
        }
        
        if(!isOthercheck)
            txtSpecialistOther.Text = "";
    }
    protected void ddlDesegregationPlan_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlDesegregationSubPlan.Items.Clear();
        if (!string.IsNullOrEmpty(ddlDesegregationPlan.SelectedValue))
        {
            rwDesegregationPlan.Visible = true;
            ddlDesegregationSubPlan.Items.Add(new ListItem("Select one", ""));
            if (ddlDesegregationPlan.SelectedValue.ToLower() == "required plan")
            {
                ddlDesegregationSubPlan.Items.Add(new ListItem("Court ordered", "Court ordered"));
                ddlDesegregationSubPlan.Items.Add(new ListItem("State mandated", "State mandated"));

            }
            else if (ddlDesegregationPlan.SelectedValue.ToLower() == "voluntary plan")
            {
                ddlDesegregationSubPlan.Items.Add(new ListItem("Office for Civil Rights required", "Office for Civil Rights required"));
                ddlDesegregationSubPlan.Items.Add(new ListItem("Voluntary", "Voluntary"));
            }
        }
        else
        {
            rwDesegregationPlan.Visible = false;
        }
    }


    protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        int reportYear = 0;
        int cohortType = Convert.ToInt32(Session["TALogCohortType"]),
            periodID = Convert.ToInt32(Session["TALogCohortPeriod"]);

        var rptPeriods = MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType
                                            && x.ID == periodID).OrderBy(y => y.ID);
        foreach (var period in rptPeriods)
        {
            reportYear = Convert.ToInt32(period.reportYear);
            break;
        }
        hfReportYearID.Value = reportYear.ToString();
        hfReportPeriodID.Value = ddlPeriod.SelectedValue;
    }

    protected void gvDDLlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDDLlist.PageIndex = e.NewPageIndex;
        gvDDLlist.DataBind();
        mpeNewsWindow.Show();
    }

    protected void textbox54Other(object sender, EventArgs e)
    {
        CheckBox mycbx = sender as CheckBox;
        string rowID = mycbx.ID.Replace("cbx", "");

        //find other text box
        TextBox mytxtBox = mycbx.Parent.FindControl("txtOther" + rowID) as TextBox;

        if (mycbx.Checked)
            mytxtBox.Visible = true;
        else
        {
            mytxtBox.Visible = false;
            mytxtBox.Text = "";
        }
    }
    

   
    protected void cblstProvDevelopmentAct_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool isOthercheck=false;
        foreach (ListItem item in cblstProvDevelopmentAct.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                txtPDAOther.Visible = true;
                isOthercheck = true;
            }
            else
            {
                txtPDAOther.Visible = false;
            }
        }
        if(!isOthercheck)
            txtPDAOther.Text = "";
    }
    protected void cblstPDT_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool isOthercheck = false;
        foreach (ListItem item in cblstPDT.Items)
        {
            if (item.Selected && item.Text.Trim().ToLower() == "other")
            {
                txtPDTother.Visible = true;
                isOthercheck = true;
            }
            else
            {
                txtPDTother.Visible = false;
            }
        }
        if(!isOthercheck)
            txtPDTother.Text = "";
    }

    protected void btnSaveTheme_Click(object sender, EventArgs e)
    {
        SaveToDB(sender, e);
        FlipTheme(sender, e);
        //updatePrimaryThemeddl();
    }

    private void updatePrimaryThemeddl()
    {
        throw new NotImplementedException();
    }

    protected void btnSaveParent_Click(object sender, EventArgs e)
    {
        SaveToDB(sender, e);
        FlipEngage(sender, e);
    }
    protected void gvDDLlist_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        gvDDLlist.PageIndex = e.NewPageIndex;
        BindData();
        mpeNewsWindow.Show();
    }
    protected void DSPSaveToDB(object sender, EventArgs e)
    {
        SaveToDB(sender, e);
        if (ddlAdminMethod.SelectedValue == "4")
            trwlottery.Style.Add(HtmlTextWriterStyle.Display, "");
        else
            trwlottery.Style.Add(HtmlTextWriterStyle.Display, "none");
    }
    protected void lbtnClose_Click(object sender, EventArgs e)
    {
        mpeNewsWindow.Hide();
    }
}