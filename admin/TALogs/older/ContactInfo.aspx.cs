﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Data;

public partial class admin_TALogs_ContactInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (Session["TALogCohortType"] != null && string.IsNullOrEmpty(Session["TALogCohortType"].ToString()))
            Session["TALogCohortType"] = null;
        if (Session["TALogGranteeID"] != null && string.IsNullOrEmpty(Session["TALogGranteeID"].ToString()))
            Session["TALogGranteeID"] = null;
        if (!IsPostBack)
        {
            rbtnCohortType.SelectedValue = Session["TALogCohortType"] == null ? "2" : Session["TALogCohortType"].ToString();
            Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
            //if(ddlGrantees.Items.Count>1)
                ddlGrantees.SelectedValue = Session["TALogGranteeID"] == null ? "" : Session["TALogGranteeID"].ToString();
        }

        hfReportperiodID.Value = MagnetReportPeriod.Find(x => x.isReportPeriod || x.isActive).Last().ID.ToString();

        if (Session["TALogGranteeID"] == null)
            pnllistContact.Visible = false;
        else
            pnllistContact.Visible = true;
    }

    protected void OnEditGrantee(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        hfeditGranteeID.Value = this.gdvGrantee.DataKeys[row.RowIndex].Values["ID"].ToString();

        var grantees = MagnetGrantee.SingleOrDefault(xmlrpc => xmlrpc.ID == Convert.ToInt32(hfeditGranteeID.Value));
        ((TextBox)fvGrantee.FindControl("GranteeNameTextBox")).Text = grantees.GranteeName;
        ((DropDownList)fvGrantee.FindControl("ddlStatus")).SelectedValue = grantees.GranteeStatusID.ToString();

        MagnetDBDB db = new MagnetDBDB();
        var granteeinfo = from gdata in db.MagnetGranteeData
                    join grpt in db.GranteeReports on gdata.GranteeReportID equals grpt.ID
                    where grpt.GranteeID == Convert.ToInt32(hfeditGranteeID.Value)
                    select new { gdata.State };

        foreach (var itm in granteeinfo)
        {
            ((DropDownList)fvGrantee.FindControl("ddlGranteeState")).SelectedValue = itm.State;
            break;
        }
        ((DropDownList)fvGrantee.FindControl("ddlOfficer")).SelectedValue = grantees.ProgramOfficerID.ToString();
        ((DropDownList)fvGrantee.FindControl("ddlTAassociate")).SelectedValue = grantees.TAassociateID.ToString();
        

        showEditPan("grantee");
        fvGrantee.ChangeMode(FormViewMode.Edit);
    }


    protected void OnAddGrantee(object sender, EventArgs e)
    {
        showEditPan("grantee");
        fvGrantee.ChangeMode(FormViewMode.Insert);
    }
    protected void btnNewContact_Click(object sender, EventArgs e)
    {
        hfeditContactID.Value = "";
        showEditPan("contact");
        fvContact.ChangeMode(FormViewMode.Insert);
    }
    
    protected string getUserRole(int ContactID)
    {
        string ret = "";
        var item = TALogContact.SingleOrDefault(x => x.id == ContactID);
        var role = TALogRole.SingleOrDefault(x => x.id == item.role_id);

        return role.rolename;

    }

    
    protected void OnDeleteGrantee(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gdvGrantee.DataKeys[row.RowIndex].Values["ID"]);
        MagnetGrantee data = MagnetGrantee.SingleOrDefault(x => x.ID == rcdid);
        data.isActive = false;
        data.Save();

        gdvGrantee.DataBind();
        if (HttpContext.Current.User.IsInRole("ED"))
            sendEmailtoMSAPCenter("deleted", "grantee");

    }
    
   
    protected void OnDeleteContact(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int rcdid = Convert.ToInt32(this.gvContact.DataKeys[row.RowIndex].Values["ID"]);
        TALogContact data = TALogContact.SingleOrDefault(x => x.id == rcdid);
        data.isActive = false;
        data.Delete();
        //data.Save();

        gvContact.DataBind();
        if (HttpContext.Current.User.IsInRole("ED"))
            sendEmailtoMSAPCenter("deleted", "contact");
    }

    protected void OnEditContact(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        hfeditContactID.Value = this.gvContact.DataKeys[row.RowIndex].Values["ID"].ToString();
        


        showEditPan("contact");
        fvContact.ChangeMode(FormViewMode.Edit);
    }
    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
        {
            Session["TALogGranteeID"] = ddlGrantees.SelectedValue;
            pnllistContact.Visible = true;
        }
        else
        {
            if(rbtnCohortType.SelectedValue =="1")
                pnllistContact.Visible = false;
            Session["TALogGranteeID"] = null;
            btnNewContact.Visible = false; ;
            btnShowContact.Visible = false;
        }

    }
    protected void rbtnCohortType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGrantees.Items.Clear();
        ddlGrantees.Items.Add(new ListItem("Select One", ""));
        Session["TALogCohortType"] = rbtnCohortType.SelectedValue;
        Session["TALogGranteeID"] = null;
        if (ddlGrantees.SelectedValue == "")
        {
            btnNewContact.Visible = false; ;
            btnShowContact.Visible = false;
        }
        else
        {
            btnNewContact.Visible = true;
            if (TALogContact.Find(x => x.PK_GenInfoID == Convert.ToInt32(ddlGrantees.SelectedValue)).Count > 0)
            {
                btnShowContact.Visible = true;
            }
            else
                btnShowContact.Visible = false;
        }
        gdvGrantee.DataBind();
    }
    protected void dsGrantee_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["@CreatedBy"].Value = HttpContext.Current.Profile.UserName;
        e.Command.Parameters["@ModifiedBy"].Value = HttpContext.Current.Profile.UserName;

        e.Command.Parameters["@CreatedOn"].Value = DateTime.Now;
        e.Command.Parameters["@ModifiedOn"].Value = DateTime.Now;
    }
    protected void dsGrantee_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["@ModifiedBy"].Value = HttpContext.Current.Profile.UserName;
        e.Command.Parameters["@ModifiedOn"].Value = DateTime.Now;
        e.Command.Parameters["@theID"].Value = Convert.ToInt32(hfeditGranteeID.Value);
    }
    protected void ClosePan(object sender, EventArgs e)
    {
        closeEditPan();
    }

    private void showEditPan(string PanName)
    {
        if (PanName == "grantee")
        {
            pnlGrantee.Visible = true;
            pnlContact.Visible = false;
        }
        else
        {
            pnlContact.Visible = true;
            pnlGrantee.Visible = false;

            if (!string.IsNullOrEmpty(hfeditContactID.Value))
            {

            }
        }

        pnllistContact.Visible = false;
        pnllistGrantee.Visible = false;

    }

    private void closeEditPan()
    {
        pnllistContact.Visible = true;
        pnllistGrantee.Visible = true;
        pnlContact.Visible = false;
        pnlGrantee.Visible = false;
    }

    private void sendEmailtoMSAPCenter(string action, string tablename)
    {
            TAlog talogOjb = new TAlog();
            string user = HttpContext.Current.User.Identity.Name;

            string datestamp = DateTime.Now.ToString();
            string Action = "updated";
            talogOjb.TALogDatahasbeenChangedbyED(user, tablename, datestamp, Action);
    }
    protected void dsGrantee_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        closeEditPan();
        gdvGrantee.DataBind();
        if (HttpContext.Current.User.IsInRole("ED"))
            sendEmailtoMSAPCenter("updated", "grantee");
    }
    protected void dsGrantee_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        btnShowContact.Text = "Show All";
        closeEditPan();
        if (HttpContext.Current.User.IsInRole("ED"))
            sendEmailtoMSAPCenter("added", "grantee");
    }
    protected void dsContact_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        closeEditPan();
        if (HttpContext.Current.User.IsInRole("ED"))
            sendEmailtoMSAPCenter("updated", "contact");
        gvContact.DataBind();
    }
    protected void dsContact_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        btnShowContact.Text="Show All";
        closeEditPan();
        if (HttpContext.Current.User.IsInRole("ED"))
            sendEmailtoMSAPCenter("added", "contact");

        gvContact.DataBind();
    }
    protected void dsContact_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        btnShowContact.Text = "Show All";
        Session["showTAcontact"] = "0";
        e.Command.Parameters["@ModifiedBy"].Value = HttpContext.Current.User.Identity.Name;
        e.Command.Parameters["@ModifiedOn"].Value = DateTime.Now;
        e.Command.Parameters["@PK_GenInfoID"].Value = Convert.ToInt32(Session["TALogGranteeID"]);
        e.Command.Parameters["@theID"].Value = Convert.ToInt32(hfeditContactID.Value);

    }
  
    protected void dsContact_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["@CreatedBy"].Value = HttpContext.Current.User.Identity.Name;
        e.Command.Parameters["@ModifiedBy"].Value = HttpContext.Current.User.Identity.Name;

        e.Command.Parameters["@CreatedOn"].Value = DateTime.Now;
        e.Command.Parameters["@ModifiedOn"].Value = DateTime.Now;
        //if (rbtnCohortType.SelectedValue == "0")
        //    e.Command.Parameters["@PK_GenInfoID"].Value = 0;
        //else
        e.Command.Parameters["@PK_GenInfoID"].Value = Convert.ToInt32(Session["TALogGranteeID"]);
    }
    protected void dsContact_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        //if (rbtnCohortType.SelectedValue == "0")
        //    e.Command.Parameters["@granteeID"].Value = 0;
        //else
        //    e.Command.Parameters["@granteeID"].Value = string.IsNullOrEmpty(ddlGrantees.SelectedValue)? -1: Convert.ToInt32(ddlGrantees.SelectedValue);
    }


    protected void ddlRole_OnDataBinding(object sender, EventArgs e)
    {
        DropDownList ddlRole = sender as DropDownList;
        int rcdid = Convert.ToInt32(hfeditContactID.Value);
        int roleid = Convert.ToInt32(TALogContact.SingleOrDefault(x => x.id == rcdid).role_id);
        ddlRole.Items.Clear();

        var data = TALogRole.Find(x => x.id != 0);
        foreach (var itm in data)
        {
            ddlRole.Items.Add(new ListItem(itm.rolename, itm.id.ToString()));
        }

        ddlRole.SelectedValue = roleid.ToString();
    }

    //protected string getPhoneFormat(string phoneNo)
    //{
    //    if (string.IsNullOrEmpty(phoneNo))
    //        return "";
    //    else
    //        return string.Format("{0:(###) ###-####}", Int64.Parse(phoneNo));
    //}
    protected void dsGrantee_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        int cohortType = Session["TALogCohortType"]==null? 1: Convert.ToInt32(Session["TALogCohortType"]);
        int ReportPeriodID = cohortType == 1 ? 6 : Convert.ToInt32(hfReportperiodID.Value);
        
        e.Command.Parameters[0].Value = cohortType;    //cohortType
        e.Command.Parameters[1].Value = ReportPeriodID;    //reportperiodID
    }
    protected void fvContact_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfeditContactID.Value))
        {
            var contact = TALogContact.SingleOrDefault(x => x.id == Convert.ToInt32(hfeditContactID.Value));

            ((TextBox)fvContact.FindControl("txtContactName")).Text = contact.ContactName;
            ((TextBox)fvContact.FindControl("txtAddress1")).Text = contact.Address1;
            ((TextBox)fvContact.FindControl("txtAddress2")).Text = contact.Address2;
            ((TextBox)fvContact.FindControl("txtCity")).Text = contact.City;
            ((DropDownList)fvContact.FindControl("ddlState")).SelectedValue = contact.State;
            ((TextBox)fvContact.FindControl("txtZipcode")).Text = contact.ZipCode;
            ((TextBox)fvContact.FindControl("txtPhone")).Text = contact.PhoneNumber;
            ((TextBox)fvContact.FindControl("txtExt")).Text = contact.Ext;
            ((TextBox)fvContact.FindControl("txt2ndPhone")).Text = contact.ScndPhoneNumber;
            ((TextBox)fvContact.FindControl("txtEmail")).Text = contact.Email;
            ((DropDownList)fvContact.FindControl("ddlRole")).SelectedValue = contact.role_id.ToString();
            ((TextBox)fvContact.FindControl("Role_otherTextBox")).Text = contact.OtherText;
        }

    }
    protected void showAll(object sender, EventArgs e)
    {
        if (Session["showTAcontact"] == null || Session["showTAcontact"].ToString() == "0")
        {
            btnShowContact.Text ="Show Edit Records";
            dsContactView.SelectCommand = "SELECT TALogContacts.id, PK_GenInfoID, ContactName, Address1, Address2, City, State, TALogRoles.rolename as RoleName,"
                                        + " ZipCode, PhoneNumber, ScndPhoneNumber, Email, role_id, OtherText, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, TALogContacts.Status "
                                        + " FROM TALogContacts JOIN TALogRoles ON TALogContacts.role_id = TALogRoles.ID "
                                        + " WHERE (PK_GenInfoID = " + ddlGrantees.SelectedValue + " )  order by TALogRoles.rolename";
            Session["showTAcontact"] = "1";
        }
        else
        {
            btnShowContact.Text = "Show All";
            dsContactView.SelectCommand = "SELECT TALogContacts.id, PK_GenInfoID, ContactName, Address1, Address2, City, State, TALogRoles.rolename as RoleName,"
                                        + " ZipCode, PhoneNumber, ScndPhoneNumber, Email, role_id, OtherText, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, TALogContacts.Status "
                                        + " FROM TALogContacts JOIN TALogRoles ON TALogContacts.role_id = TALogRoles.ID "
                                        + " WHERE (PK_GenInfoID = " + ddlGrantees.SelectedValue + " ) AND TALogContacts.Status='Edit Record' order by TALogRoles.rolename";
            Session["showTAcontact"] = "0";
        }
        dsContactView.Select(DataSourceSelectArguments.Empty);
        dsContactView.DataBind();
        gvContact.DataBind();

    }

    protected string getPhoneWithExt(object rwid)
    {
        string phonenum = "";
        if (rwid != null)
        {
            int id = Convert.ToInt32(rwid);
            TALogContact rcd = TALogContact.SingleOrDefault(x => x.id == id);
            if (rcd != null)
            {
                phonenum = rcd.PhoneNumber;
                if (!string.IsNullOrEmpty(rcd.Ext))
                    phonenum += ", ext. " + rcd.Ext;

            }
        }
        return phonenum;
    }
}