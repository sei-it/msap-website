﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="ContextualFactors.aspx.cs" Inherits="admin_TALogs_ContextualFactors" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

 <script language="javascript" type="text/javascript">

     $(document).ready(function () {

             $('#<%= ddlAdminMethod.ClientID %>').change(function () {
                 if ($('#<%= ddlAdminMethod.ClientID %>').val() == 4)
                     $('#<%= trwlottery.ClientID %>').show();
                 else
                     $('#<%= trwlottery.ClientID %>').hide();
             });

     });


     document.onkeydown = function (e) {
         // keycode for F5 function
         if (e.keyCode === 116) {
             return false;
         }
     };


</script>
<style type="text/css">  
.MyAccordinFont
{  
    font-family: Helvetica,Arial,Sans-serif!important;   
    font-size: 11px;
}  

.notdisplay
{
    display:none;
}

.COMLOG_CHECK td
{
    width: 240px;
}

    </style> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
<asp:Panel ID="pnlMain" runat="server">
 <h1>
        Communication Management 
    </h1>
 <asp:RadioButtonList ID="rbtnCohortType" runat="server" RepeatColumns="2" 
        RepeatDirection="Horizontal" AutoPostBack="true" onselectedindexchanged="rbtnCohortType_SelectedIndexChanged" >
        <asp:ListItem Value="1" >Cohort 2010</asp:ListItem>
        <asp:ListItem Value="2" Selected="True">Cohort 2013</asp:ListItem> 
    </asp:RadioButtonList>
   
    <asp:DropDownList ID="ddlGrantees" runat="server" DataSourceID="dsGrantee" 
        AppendDataBoundItems="true" AutoPostBack="true" CssClass="DropDownListCssClass"
        DataTextField="GranteeName" DataValueField="ID" 
        onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
        <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>
    <br />
   <asp:DropDownList ID="ddlPeriod" runat="server" AutoPostBack="true" CssClass="DropDownListCssClass" Visible="false"
           onselectedindexchanged="ddlPeriod_SelectedIndexChanged" >
        <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>
    <br /><br />

    <div class="titles">Grantee and School Information</div>
  <div class="tab_area">
    	<ul class="tabs">
           <li><asp:LinkButton ID="LinkButton3" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton9" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
           <li class="tab_active">Page 3</li>
           <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton10" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/>
    <asp:Panel ID ="pnlSchoolgv"  runat="server" >
   <%-- <p style="text-align:left">
    <asp:Button ID="Newbutton" runat="server" Text="New Record" CssClass="surveyBtn2" OnClick="OnAddSchool" Visible="false" />
    </p>--%>
    <asp:GridView ID="gdvSchool" runat="server" AllowPaging="true" AllowSorting="false" DataSourceID="dsSchoolgv"
        PageSize="10" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"  EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"  >
        <Columns>
            <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School name" />
            <asp:BoundField DataField="SchoolType" SortExpression="" HeaderText="School type" />
            <asp:BoundField DataField="ProgramType" SortExpression="" HeaderText="Program type" />
            <asp:BoundField DataField="theme" SortExpression="" HeaderText="Theme" />
            <asp:BoundField DataField="ModifiedBy" SortExpression="" HeaderText="Modified by" />
            <asp:BoundField DataField="ModifiedOn" SortExpression="" HeaderText="Modified on"  DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false" />
            <asp:TemplateField>
                <ItemTemplate>
                   <%-- <asp:LinkButton ID="LinkButton3" runat="server" Text="View" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnViewSchool"></asp:LinkButton>--%>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditSchool"></asp:LinkButton>
                    <%--<asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteSchool" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
    </asp:Panel>
     <asp:HiddenField ID="hfeditSchoolID" runat="server" />
     <asp:HiddenField ID="hfContentID" runat="server" />
     <asp:HiddenField ID="hfReportYearID" runat="server" />
     <asp:HiddenField ID="hfReportPeriodID" runat="server" />

        <asp:SqlDataSource ID="dsGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 

        SelectCommand="SELECT * FROM [MagnetGrantees] WHERE ([CohortType] = @CohortType AND MagnetGrantees.ID not in (39, 40, 41, 69,70,1001, 1002, 1003)) AND isActive=1 order by GranteeName " >
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohortType" Name="CohortType" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    </asp:Panel>
       <asp:Panel ID="PopupPanel" runat="server" Visible="false">
        <div class="mpeDivConsult" style="width:854px;">
            <div class="mpeDivHeaderConsult" style="width:840px;">
                Edit Grantee and School Information
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" Text="Close" CausesValidation="false" OnClick="ClosePan" CssClass="closeit_link" />
                </span>
            </div>
            <fieldset>
            <legend><b>Grantee Information</b></legend>
            <table style="padding:0px 10px 10px 10px; width:825px;">
            
                 <tr>
                 <td class="consult_popupTxt">Grantee name:</td>
                 <td style="vertical-align:bottom;"> <asp:Label ID="txtGranteeName" runat="server" Text="" Width="450"  /></td>
                 </tr>
                  <tr>
                    <td class="consult_popupTxt">State:</td>
					<td style="vertical-align:bottom;"> <asp:Label ID="txtState" runat="server" Text="" Width="450"/></td>
				  </tr>
				   <tr>
                    <td class="consult_popupTxt">Grantee size:</td>
					<td style="vertical-align:bottom;"> <asp:Label ID="txtGrantSize" runat="server" Text="" Width="450"   /></td>
				  </tr>
                  <tr>
                      <td colspan="2">
                      </td>
                  </tr>
                  </table>
            </fieldset>
            <fieldset>
            <legend><b>School Information</b></legend>
            <table style="padding:0px 10px 10px 10px; width:825px;">
                      <tr>
                          <td class="consult_popupTxt">
                              School name:</td>
                          <td style="vertical-align:bottom;">
                              <asp:Label ID="SchoolNameTextBox" runat="server" Width="450" />
                          </td>
                      </tr>
                      <tr>
                          <td class="consult_popupTxt">
                              School type:</td>
                          <td style="vertical-align:bottom;">
                              <asp:Label ID="SchoolTypeTextBox" runat="server" Width="450" />
                          </td>
                      </tr>
                      <tr>
                          <td class="consult_popupTxt">
                              Program type:</td>
                          <td style="vertical-align:bottom;">
                              <asp:Label ID="txtprogramType" runat="server" />
                          </td>
                      </tr>
                      <tr>
                          <td class="consult_popupTxt" valign="middle">
                              Program status:</td>
                          <td>
                              <asp:DropDownList ID="ddlProgramStatus" CssClass="DropDownListCssClass" runat="server" Width="450">
                                  <asp:ListItem Value="">Select one</asp:ListItem>
                                  <asp:ListItem>New</asp:ListItem>
                                  <asp:ListItem>Converted</asp:ListItem>
                                  <asp:ListItem>Revised</asp:ListItem>
                              </asp:DropDownList>
                          </td>
                      </tr>
                      <tr>
                          <td class="consult_popupTxt" valign="middle">
                              Urbanicity:</td>
                          <td>
                              <asp:DropDownList ID="ddlUrbanicity" CssClass="DropDownListCssClass" runat="server" Width="450">
                                  <asp:ListItem Value="">Select one</asp:ListItem>
                                  <asp:ListItem>Rural</asp:ListItem>
                                  <asp:ListItem>Suburban</asp:ListItem>
                                  <asp:ListItem>Urban</asp:ListItem>
                              </asp:DropDownList>
                          </td>
                      </tr>
                      </table>
            </fieldset>
            <fieldset>
                <legend><b>Student economic status</b></legend>
                <table style="padding:0px 10px 10px 10px; width:825px;">
                <tr>
                    <td class="consult_popupTxt" valign="middle">
                        Free and reduced-price lunch eligibility percent:</td>
                    <td style="vertical-align:bottom;">
                        <asp:label ID="FreeReduceLunchTextBox" runat="server" Width="450" />

                    </td>
                </tr>
                <tr>
                    <td class="consult_popupTxt" valign="middle">
                        Free and reduced-price lunch eligibility percent category:</td>
                    <td style="vertical-align:bottom;">
                        <asp:DropDownList ID="ddlDifRange" CssClass="DropDownListCssClass" runat="server" Width="450">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem Value="3">More than 75 percent</asp:ListItem>
                            <asp:ListItem Value="2">50 to 75 percent</asp:ListItem>
                            <asp:ListItem Value="1">Less than 50 percent</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                </table>
            </fieldset>
                <fieldset>
                <legend>
                <b>Accountability status</b>
                </legend>
                <table style="padding:0px 10px 10px 10px; width:825px;">
                <tr>
                    <td class="consult_popupTxt">
                        Title I status:</td>
                    <td style="vertical-align:bottom;">
                        <asp:Label ID="txtTitle1Status" runat="server" Width="450" />
                    </td>
                </tr>
                <tr>
                    <td class="consult_popupTxt">
                        School accountability designation:</td>
                    <td style="vertical-align:bottom;">
                        <asp:Label ID="txtschoolDesignation" runat="server" Width="450" />
                    </td>
                </tr>
                <tr>
                    <td class="consult_popupTxt">
                        Persistently lowest achieving:</td>
                    <td style="vertical-align:bottom;">
                        <asp:Label ID="txtLowestAchieve" runat="server" Width="450" />
                    </td>
                </tr>
                <tr>
                    <td class="consult_popupTxt">
                        School Improvement Grant:</td>
                    <td style="vertical-align:bottom;">
                        <asp:Label ID="txtSchoolGrant" runat="server"  Width="450" />
                    </td>
                </tr>
                </table>
                </fieldset>
                <fieldset>
                <legend>
                <b>District and school policy</b>
                </legend>
<table style="padding:0px 10px 10px 10px; width:825px;">
                <tr>
                <td class="consult_popupTxt" style="width: 323px;" >
                        Desegregation plan:
                </td>
                <td >
                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
        <ContentTemplate>
                <table width="82%">
                <tr>
                          
                    <td>
                        <asp:DropDownList ID="ddlDesegregationPlan" CssClass="DropDownListCssClass" 
                            runat="server" AutoPostBack="true" Width="450" 
                            onselectedindexchanged="ddlDesegregationPlan_SelectedIndexChanged">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem>Required plan</asp:ListItem>
                            <asp:ListItem>Voluntary plan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvplan" ControlToValidate="ddlDesegregationPlan" InitialValue="" runat="server" ErrorMessage="please select one."></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr runat="server" id="rwDesegregationPlan" visible="false">
                <td>
                <asp:DropDownList ID="ddlDesegregationSubPlan" CssClass="DropDownListCssClass" runat="server" Width="450">
                </asp:DropDownList>
                </td>
                </tr>
                <tr>
                <td></td>
                <td></td>
                </tr>
                </table>
        </ContentTemplate>
        </asp:UpdatePanel>
                </td>
                </tr>
                
                <tr>
                    <td class="consult_popupTxt">
                        Attendance type:</td>
                    <td style="padding-left:3px;">
                        <asp:DropDownList ID="ddlAttendType" CssClass="DropDownListCssClass" runat="server" Width="450">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem>Attendance zone</asp:ListItem>
                            <asp:ListItem>Dedicated</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="consult_popupTxt">
                        Admissions method:</td>
                    <td style="padding-left:3px;">
                        <asp:DropDownList ID="ddlAdminMethod" CssClass="change DropDownListCssClass" runat="server" 
                            Width="450">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem Value="1">Open enrollment</asp:ListItem>
                            <asp:ListItem Value="2">Random lottery</asp:ListItem>
                            <asp:ListItem Value="3">Selective admissions</asp:ListItem>
                            <asp:ListItem Value="4">Weighted lottery</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="padding-left:3px;">
                        <asp:TextBox ID="txtAdminOther" runat="server" Width="450" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                <td colspan="2" align="right">
                <asp:Button ID="Button2" runat="server" CausesValidation="True" 
                            CommandName="Update" CssClass="surveyBtn2" OnClick="DSPSaveToDB" 
                            Text="Save Record" />
                </td>
                </tr>
                <tr ID="trwlottery" runat="server">
                          <td class="consult_popupTxt">
                              Weighted lottery preferences (Select all that apply):
                          </td>
                          <td>
                              <asp:CheckBoxList ID="cbxlstWlottery" runat="server" RepeatColumns="2"  
                                  RepeatDirection="Horizontal">
                                  <asp:ListItem Value="1">Attending siblings</asp:ListItem>
                                  <asp:ListItem Value="2">Dependent of active military personnel</asp:ListItem>
                                  <asp:ListItem Value="3">Economic status</asp:ListItem>
                                  <asp:ListItem Value="4">Employer of parent/guardian</asp:ListItem>
                                  <asp:ListItem Value="5">English language learner status</asp:ListItem>
                                  <asp:ListItem Value="6">Magnet theme continuity</asp:ListItem>
                                  <asp:ListItem Value="7">Moving&nbsp; from low-performing school</asp:ListItem>
                                  <asp:ListItem Value="8">Race/ethnicity</asp:ListItem>
                                  <asp:ListItem Value="9">Relationship with magnet partners</asp:ListItem>
                                  <asp:ListItem Value="10">Students with disabilities</asp:ListItem>
                                  <asp:ListItem Value="11">Zone/distance from school</asp:ListItem>
                              </asp:CheckBoxList>
                          </td>
                      </tr>
                </table>
                </fieldset>
                
                <fieldset>
                <legend>
                <b>MSAP activities</b>
                </legend>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" Visible="true">
     <ContentTemplate>
<table style="padding:0px 10px 10px 10px; width:825px;">

                      <tr>
                          <td class="consult_popupTxt" valign="middle">
                              Secondary theme: </td>
                          <td>
                              
                              <%--summary list--%>
                              <table width="100%">
                              <tr>
                              <td align="right"><asp:Literal ID="ltlThemelist" runat="server" Visible="false"/>
                              <asp:Button ID="btnFtheme" runat="server" CausesValidation="False" 
                                  CssClass="surveyBtn2" Text="Add/edit data" onclick="FlipTheme" />
                               <asp:Button ID="btnSaveTheme" runat="server" CausesValidation="True" Visible="false"
                                  CommandName="Update" CssClass="surveyBtn2"
                                  Text="Save Record" onclick="btnSaveTheme_Click" />   
                              </td>
                              </tr>
                              </table>
                              
                               
                              <%--accordin pan chcekboxlist--%>

                              <telerik:RadPanelBar ID="radpnlbarCategory" runat="server"  style="width:650px;" >
                                  <ExpandAnimation Type="None" />
                                  <CollapseAnimation Type="None" />
                              </telerik:RadPanelBar>
                              <telerik:RadPanelBar ID="radpnlbarCategoryReadMode" runat="server"   style="width:650px;" >
                                  <ExpandAnimation Type="None" />
                                  <CollapseAnimation Type="None" />
                              </telerik:RadPanelBar>
                               
                          </td>
                      </tr>
                      <tr>
                        <td class="consult_popupTxt">Primary theme</td>
                        <td>
                        <asp:DropDownList ID="ddlPrimarytheme" CssClass="change DropDownListCssClass" runat="server" 
                            Width="450">

                        </asp:DropDownList>
                        </td>
                       </tr>
                      <tr>
                          <td colspan="2">
                             </td>
                      </tr>
            <%--page 1--%>
            
                <%--page2--%>
               </table>
</ContentTemplate>
</asp:UpdatePanel>
              <table style="padding:0px 10px 10px 10px; width:825px;">
                <tr>
                <td colspan="2"></td>
                </tr>
                      <tr runat="server" visible="false">
                          <td class="consult_popupTxt" valign="middle">
                              Curriculum type:</td>
                          <td>
                              <asp:DropDownList ID="ddlCurriculumnType" CssClass="DropDownListCssClass" runat="server">
                                  <asp:ListItem Value="">Select one</asp:ListItem>
                              </asp:DropDownList>
                              <asp:Button ID="lbtnCTedit" runat="server" CommandArgument="CT" CssClass="surveyBtn2"
                                  OnClick="OnddlEdit" Text="Add/edit list" />
                          </td>
                      </tr>
                      <tr>
                          <td class="consult_popupTxt" valign="middle">
                              Instructional delivery methods&nbsp;(Select all that apply):</td>
                          <td>
                          <table>
                          <tr>
                          <td>
                          <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                              <ContentTemplate>
                            <asp:CheckBoxList ID="cbxlstDeliveryMethod" CssClass="COMLOG_CHECK" runat="server" RepeatColumns="2" AutoPostBack="true" 
                                  RepeatDirection="Horizontal"
                                      onselectedindexchanged="cbxlstDeliveryMethod_SelectedIndexChanged">
                              </asp:CheckBoxList>
                              <asp:TextBox ID="txtInstructOther" runat="server" Width="450" Visible="false" ></asp:TextBox>
                               </ContentTemplate>
                              </asp:UpdatePanel>
                          </td>
                          <td>
                            <asp:Button ID="lbtnIDMedit" runat="server" CommandArgument="IDM" CausesValidation="false" CssClass="surveyBtn2"
                                  OnClick="OnddlEdit" Text="Add/edit list" />
                          </td>
                          </tr>
                          </table>
                          </td>
                      </tr>
                      
                      <tr>
                      <td colspan="2" align="right" >
                             <asp:Button ID="Button4" runat="server" CausesValidation="True" 
                                  CommandName="Update" CssClass="surveyBtn2" OnClick="SaveToDB" 
                                  Text="Save Record" />
                      </td>
                      </tr>
                      <tr>
                        <td colspan="2" >
                        <hr />
                        </td>
                      </tr>
            <%--page3--%>

                         <tr>
                          <td class="consult_popupTxt" valign="middle">
                              MSAP staff hired<br /> (Select all that apply):</td>

                          <td>
                          <table>
                          <tr>
                          <td>
                           <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                              <ContentTemplate>
                              <asp:CheckBoxList ID="cbxlstSpecialist" CssClass="COMLOG_CHECK" runat="server" RepeatColumns="2" AutoPostBack="true" 
                                  RepeatDirection="Horizontal" 
                                      onselectedindexchanged="cbxlstSpecialist_SelectedIndexChanged">
                              </asp:CheckBoxList>
                              <asp:TextBox ID="txtSpecialistOther" runat="server" Width="450" Visible="false"></asp:TextBox>
                              </ContentTemplate>
                              </asp:UpdatePanel>
                          </td>
                          <td>
                          <asp:Button ID="btnMSAPStaff" runat="server" CommandArgument="MSAPStaff" CausesValidation="false" CssClass="surveyBtn2"
                                  OnClick="OnddlEdit" Text="Add/edit list" />
                          </td>
                          </tr>
                          </table>
                             
                          </td>
                      </tr>
                  <tr>
                        <td colspan="2" >
                        <hr />
                        </td>
                      </tr>
                      <tr style="padding-top:20px;">
                          <td class="consult_popupTxt" valign="middle">
                              Professional development delivery (Select all that apply):</td>
                          <td>
                          <table>
                          <tr>
                          <td>
                          <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                              <ContentTemplate>
                            <asp:CheckBoxList ID="cblstProvDevelopmentAct" CssClass="COMLOG_CHECK" runat="server" RepeatColumns="2" AutoPostBack="true" 
                                  RepeatDirection="Horizontal" 
                                      onselectedindexchanged="cblstProvDevelopmentAct_SelectedIndexChanged">
                              </asp:CheckBoxList>
                              <asp:TextBox ID="txtPDAOther" runat="server" Width="450" Visible="false"></asp:TextBox>
                                </ContentTemplate>
                              </asp:UpdatePanel>
                          </td>
                          <td>
                          <asp:Button ID="lbtnPDAedit" runat="server" CausesValidation="false" CssClass="surveyBtn2" CommandArgument="PDA" 
                                  OnClick="OnddlEdit" Text="Add/edit list" />
                          </td>
                          </tr>
                          </table>
                          </td>
                      </tr>
                  <tr>
                        <td colspan="2" >
                        <hr />
                        </td>
                      </tr>
                      <tr>
                          <td class="consult_popupTxt" valign="middle">
                              Professional development topics (Select all that apply):</td>
                          <td>
                          <table>
                          <tr>
                          <td>
                          <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                              <ContentTemplate>
                            <asp:CheckBoxList ID="cblstPDT" runat="server" CssClass="COMLOG_CHECK" RepeatColumns="2" AutoPostBack="true" 
                                  RepeatDirection="Horizontal" 
                                      onselectedindexchanged="cblstPDT_SelectedIndexChanged">
                              </asp:CheckBoxList>
                              <asp:TextBox ID="txtPDTother" runat="server" Width="450" Visible="false"></asp:TextBox>
                                </ContentTemplate>
                              </asp:UpdatePanel>
                             
                          </td>
                          <td>
                           <asp:Button ID="LinkButton2" runat="server" CommandArgument="PDT" CssClass="surveyBtn2" CausesValidation="false" 
                                  OnClick="OnddlEdit" Text="Add/edit list" />
                          </td>
                          </tr>
                          </table>
                          </td>
                      </tr>
                      <tr id="Tr1" runat="server" visible="false">
                          <td class="consult_popupTxt">
                              Family engagement activities:</td>
                          <td>
                              <asp:DropDownList ID="ddlengageAct" runat="server" CssClass="DropDownListCssClass">
                                  <asp:ListItem Value="">Select one</asp:ListItem>
                              </asp:DropDownList>
                             <asp:Button ID="lbtnPEAedit" runat="server" CommandArgument="PEA" CssClass="surveyBtn2"
                                  OnClick="OnddlEdit" Text="Add/edit list" />
                          </td> 
                          
                      </tr>
                      <tr>
                          <td class="consult_popupTxt">
                              Parent engagement:</td>
                          <td>
                           <asp:UpdatePanel ID="UpdatePanel2" runat="server" Visible="true">
                                  <ContentTemplate>
                              <%--summary list--%>
                              <table width="100%">
                              <tr>
                              <td align="right">
                              <asp:Literal ID="ltlpengage" runat="server" Visible="false" />
                              <asp:Button ID="btnFEngage" runat="server" CausesValidation="False" 
                                  CssClass="surveyBtn2" OnClick="FlipEngage" Text="Add/edit data" />
                             <asp:Button ID="btnSaveParent" runat="server" CausesValidation="True" Visible="false" 
                                  CommandName="Update" CssClass="surveyBtn2" 
                                  Text="Save Record" onclick="btnSaveParent_Click" />
                              </td>
                              </tr>
                              </table>
                              
                              <%--accordin pan chcekboxlist--%>
                                      <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                      <ContentTemplate>
                                <telerik:RadPanelBar ID="rpnlBarPEngage"  runat="server"  style="width:650px;" >
                                  <Items>
                                    <telerik:RadPanelItem Font-Bold="true" Text="Decision-making" Expanded="false" Value="3">
                                    <Items>
                                      <telerik:RadPanelItem runat="server" Font-Size="11px" Value="1|3">
                                    <ItemTemplate>
                                    <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx11" Text="Parent advisory boards/involvement in school decision making" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx12" Text="PTA/PTSA" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td >
                                    <asp:CheckBox ID="cbx13" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                                    </td>
                                    <td>
                                    <asp:TextBox ID="txtOther13" runat="server" Visible="false"></asp:TextBox>
                                     </td>
                                    </tr>
                                    </table>
                                    </ItemTemplate>
                                    </telerik:RadPanelItem>
                                    </Items>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Font-Bold="true" Text="Cultural competency" Expanded="false" Value="2">
                                    <Items>
                                      <telerik:RadPanelItem runat="server" Font-Size="11px" Value="2|2">
                                    <ItemTemplate>
                                    <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx21" Text="Cultural brokers" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>
                                    <asp:CheckBox ID="cbx22" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                                    </td>
                                    <td>
                                    <asp:TextBox ID="txtOther22" runat="server" Visible="false"></asp:TextBox>
                                     </td>
                                    </tr>
                                    </table>
                                    </ItemTemplate>
                                    </telerik:RadPanelItem>
                                    </Items>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Font-Bold="true" Text="Academic" Expanded="false" Value="1">
                                    <Items>
                                      <telerik:RadPanelItem runat="server" Font-Size="11px" Value="3|3">
                                    <ItemTemplate>
                                    <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx31" Text="Academic support for parents" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx32" Text="Parent skills training" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td >
                                    <asp:CheckBox ID="cbx33" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                                    </td>
                                    <td>
                                    <asp:TextBox ID="txtOther33" Visible="false" runat="server"></asp:TextBox>
                                     </td>
                                    </tr>
                                    </table>
                                    </ItemTemplate>
                                    </telerik:RadPanelItem>
                                    </Items>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Font-Bold="true" Text="Regular communications" Expanded="false" Value="5">
                                     <Items>
                                      <telerik:RadPanelItem runat="server" Font-Size="11px" Value="4|3">
                                    <ItemTemplate>
                                    <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx41" Text="Parent/teacher conferences" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx42" Text="Other regular communications" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td >
                                    <asp:CheckBox ID="cbx43" Text="Other" AutoPostBack="true" OnCheckedChanged="textboxState" runat="server" />
                                    </td>
                                    <td>
                                    <asp:TextBox ID="txtOther43" runat="server" Visible="false"></asp:TextBox>
                                     </td>
                                    </tr>
                                    </table>
                                    </ItemTemplate>
                                    </telerik:RadPanelItem>
                                    </Items>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Font-Bold="true" Text="Other" Expanded="false" Value="4">
                                    <Items>
                                      <telerik:RadPanelItem runat="server" Font-Size="11px" Value="5|2">
                                    <ItemTemplate>
                                    <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                                    <tr>
                                    <td colspan="2">
                                    <asp:CheckBox ID="cbx51" Text="Parent volunteers" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>
                                    <asp:CheckBox ID="cbx52" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                                    </td>
                                    <td>
                                    <asp:TextBox ID="txtOther52" visible="false" runat="server"></asp:TextBox>
                                     </td>
                                    </tr>
                                    </table>
                                    </ItemTemplate>
                                    </telerik:RadPanelItem>
                                    </Items>
                                    </telerik:RadPanelItem>
                                    </Items>
                              </telerik:RadPanelBar>
                                      </ContentTemplate>
                                      </asp:UpdatePanel>
                              
                              <telerik:RadPanelBar ID="rpnlBarPEngageReadMode" runat="server"  style="width:650px;" >
                                  <ExpandAnimation Type="None" />
                                  <CollapseAnimation Type="None" />
                              </telerik:RadPanelBar>
                              </ContentTemplate>
                              </asp:UpdatePanel>
                          </td>
                </tr>
                      <tr id="Tr2" runat="server" visible="false">
                          <td class="consult_popupTxt">
                              Sustainability activities:</td>
                          <td>
                              <asp:DropDownList ID="ddlSustainAct" runat="server" CssClass="DropDownListCssClass">
                                  <asp:ListItem Value="">Select one</asp:ListItem>
                              </asp:DropDownList>
                              <asp:Button ID="lbtnSAedit" runat="server" CommandArgument="SA" CssClass="surveyBtn2"
                                  OnClick="OnddlEdit" Text="Add/edit list" />
                          </td>
                      </tr>
                      </table>
                      </fieldset>
                      <table style="padding:0px 10px 10px 10px; width:825px;">
                      <tr>
                          <td class="consult_popupTxt" colspan="2">
                              Community partnership activities:</td>
                          
                              <asp:DropDownList ID="ddlCommPartAct" runat="server" Visible="false" CssClass="DropDownListCssClass">
                                  <asp:ListItem Value="">Select one</asp:ListItem>
                              </asp:DropDownList>
                              <asp:LinkButton ID="lbtnCPAedit" runat="server" CommandArgument="CPA" Visible="false"
                                  OnClick="OnddlEdit">Add/edit list</asp:LinkButton>

                        
                      </tr>
                      <tr>
                      <td colspan="2">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
        <table>
        <tr>
        <th>Partnership Type</th>
        <th>Partnership Focus</th>
        <th>MSAP Partnership Services/Activities</th>
        </tr>
    <tr>
    <td><asp:DropDownList ID="ddl11" runat="server" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="111">General business</asp:ListItem>
    <asp:ListItem Value="112">General non-profit organization</asp:ListItem>
    <asp:ListItem Value="113">Educational institution</asp:ListItem>
    <asp:ListItem Value="114">Health care institution</asp:ListItem>
    <asp:ListItem Value="115">Individual supporter</asp:ListItem>
    <asp:ListItem Value="116">Cultural, recreational, or media organization</asp:ListItem>
    <asp:ListItem Value="117">Other government agency</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl12" runat="server" AutoPostBack="true" CssClass="DropDownListCssClass"
            onselectedindexchanged="onMySelectedIndexChanged" >
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="1">Student centered</asp:ListItem>
    <asp:ListItem Value="2">Parent/family centered</asp:ListItem>
    <asp:ListItem Value="3">School centered</asp:ListItem>
    <asp:ListItem Value="4">Community centered (service learning)</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl13" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList></td>
    </tr>
    </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
        <table>
    <tr>
    <td><asp:DropDownList ID="ddl21" runat="server" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="211">General business</asp:ListItem>
    <asp:ListItem Value="212">General non-profit organization</asp:ListItem>
    <asp:ListItem Value="213">Educational institution</asp:ListItem>
    <asp:ListItem Value="214">Health care institution</asp:ListItem>
    <asp:ListItem Value="215">Individual supporter</asp:ListItem>
    <asp:ListItem Value="216">Cultural, recreational, or media organization</asp:ListItem>
    <asp:ListItem Value="217">Other government agency</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl22" runat="server" AutoPostBack="true"
            onselectedindexchanged="onMySelectedIndexChanged" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="1">Student centered</asp:ListItem>
    <asp:ListItem Value="2">Parent/family centered</asp:ListItem>
    <asp:ListItem Value="3">School centered</asp:ListItem>
    <asp:ListItem Value="4">Community centered (service learning)</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl23" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList></td>
    </tr>
    </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
        <table>
    <tr>
    <td><asp:DropDownList ID="ddl31" runat="server" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="311">General business</asp:ListItem>
    <asp:ListItem Value="312">General non-profit organization</asp:ListItem>
    <asp:ListItem Value="313">Educational institution</asp:ListItem>
    <asp:ListItem Value="314">Health care institution</asp:ListItem>
    <asp:ListItem Value="315">Individual supporter</asp:ListItem>
    <asp:ListItem Value="316">Cultural, recreational, or media organization</asp:ListItem>
    <asp:ListItem Value="317">Other government agency</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl32" runat="server" AutoPostBack="true"
            onselectedindexchanged="onMySelectedIndexChanged" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="1">Student centered</asp:ListItem>
    <asp:ListItem Value="2">Parent/family centered</asp:ListItem>
    <asp:ListItem Value="3">School centered</asp:ListItem>
    <asp:ListItem Value="4">Community centered (service learning)</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl33" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList></td>
    </tr>
    </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
        <table>
    <tr>
    <td><asp:DropDownList ID="ddl41" runat="server" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="411">General business</asp:ListItem>
    <asp:ListItem Value="412">General non-profit organization</asp:ListItem>
    <asp:ListItem Value="413">Educational institution</asp:ListItem>
    <asp:ListItem Value="414">Health care institution</asp:ListItem>
    <asp:ListItem Value="415">Individual supporter</asp:ListItem>
    <asp:ListItem Value="416">Cultural, recreational, or media organization</asp:ListItem>
    <asp:ListItem Value="417">Other government agency</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl42" runat="server" AutoPostBack="true"
            onselectedindexchanged="onMySelectedIndexChanged" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="1">Student centered</asp:ListItem>
    <asp:ListItem Value="2">Parent/family centered</asp:ListItem>
    <asp:ListItem Value="3">School centered</asp:ListItem>
    <asp:ListItem Value="4">Community centered (service learning)</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl43" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList></td>
    </tr>
    </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
        <ContentTemplate>
        <table>
    <tr>
    <td><asp:DropDownList ID="ddl51" runat="server" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="511">General business</asp:ListItem>
    <asp:ListItem Value="512">General non-profit organization</asp:ListItem>
    <asp:ListItem Value="513">Educational institution</asp:ListItem>
    <asp:ListItem Value="514">Health care institution</asp:ListItem>
    <asp:ListItem Value="515">Individual supporter</asp:ListItem>
    <asp:ListItem Value="516">Cultural, recreational, or media organization</asp:ListItem>
    <asp:ListItem Value="517">Other government agency</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl52" runat="server" AutoPostBack="true"
            onselectedindexchanged="onMySelectedIndexChanged" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="1">Student centered</asp:ListItem>
    <asp:ListItem Value="2">Parent/family centered</asp:ListItem>
    <asp:ListItem Value="3">School centered</asp:ListItem>
    <asp:ListItem Value="4">Community centered (service learning)</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl53" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList></td>
    </tr>
    </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
        <ContentTemplate>
        <table>
    <tr>
    <td><asp:DropDownList ID="ddl61" runat="server" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="611">General business</asp:ListItem>
    <asp:ListItem Value="612">General non-profit organization</asp:ListItem>
    <asp:ListItem Value="613">Educational institution</asp:ListItem>
    <asp:ListItem Value="614">Health care institution</asp:ListItem>
    <asp:ListItem Value="615">Individual supporter</asp:ListItem>
    <asp:ListItem Value="616">Cultural, recreational, or media organization</asp:ListItem>
    <asp:ListItem Value="617">Other government agency</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl62" runat="server" AutoPostBack="true"
            onselectedindexchanged="onMySelectedIndexChanged" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    <asp:ListItem Value="1">Student centered</asp:ListItem>
    <asp:ListItem Value="2">Parent/family centered</asp:ListItem>
    <asp:ListItem Value="3">School centered</asp:ListItem>
    <asp:ListItem Value="4">Community centered (service learning)</asp:ListItem>
    </asp:DropDownList></td>
    <td><asp:DropDownList ID="ddl63" runat="server" AppendDataBoundItems="true" CssClass="DropDownListCssClass">
    <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList></td>
    </tr>
    </table>
        </ContentTemplate>
        </asp:UpdatePanel>
                      </td>
                      </tr>
                     <tr>
                        <td colspan="2" class="consult_popupTxt">
                              Other:
                         </td>
                    </tr>
                    <tr>
                     <td colspan="2">
                         <asp:TextBox ID="txtPartnershipOther" runat="server" Height="80px" TextMode="MultiLine" Width="645px"></asp:TextBox>
                      </td>
                      </tr>
                      <tr>
                       <td colspan="2">
                      </td>
                      </tr>
                      <tr>
                          <td colspan="2" class="consult_popupTxt" style="padding-top: 15px; padding-bottom:15px;">
                              Marketing and recruitment:</td>
                              <asp:DropDownList ID="ddlMarketingrecruitment" runat="server" Visible="false" CssClass="DropDownListCssClass">
                                  <asp:ListItem Value="">Select one</asp:ListItem>
                              </asp:DropDownList>
                              <asp:LinkButton ID="lbtnMRedit" runat="server" CommandArgument="MR" Visible="false" 
                                  OnClick="OnddlEdit">Add/edit data</asp:LinkButton>
                      </tr>
                      <tr>
                      <td  colspan="2">
                      <b>Who has primary responsibility for the school’s marketing and recruitment?</b>
                      </td>
                      </tr>
                      <tr>
                      <td colspan="2"  style=" padding-top:15px; padding-bottom:15px;">
<asp:UpdatePanel ID="UpdatePanel10" runat="server">
<ContentTemplate>
<table width="100%">
<tr>
<th>Staff type</th>
<th>Staff title</th>
<th>Grant funded</th>
<th>Activities responsible for</th>
<th width="200"></th>
</tr>
<tr>
<td><asp:DropDownList ID="ddlmc11" runat="server" AutoPostBack="false" CssClass="DropDownListCssClass"
        onselectedindexchanged="ddlmc11_SelectedIndexChanged">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="111">District staff</asp:ListItem>
<asp:ListItem Value="112">School staff</asp:ListItem>
<asp:ListItem Value="113">Consultant</asp:ListItem>
<asp:ListItem Value="114">Not specified</asp:ListItem>
<asp:ListItem Value="115">Other</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="mcOther11" runat="server" visible="false" />
</td>
<td><asp:TextBox ID="txtmc12" runat="server"></asp:TextBox></td>
<td><asp:DropDownList ID="ddlmc13" runat="server" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem  Value="131">Yes</asp:ListItem>
<asp:ListItem Value="132">No</asp:ListItem>
<asp:ListItem Value="133">Not specified</asp:ListItem>
</asp:DropDownList></td>
<td><asp:DropDownList ID="ddlmc14" runat="server" AutoPostBack="true" 
        onselectedindexchanged="ddlmc14_SelectedIndexChanged" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="141">General marketing and recruitment</asp:ListItem>
<asp:ListItem Value="142">School image development</asp:ListItem>
<asp:ListItem Value="143">Marketing and recruitment plans</asp:ListItem>
<asp:ListItem Value="144">Outreach</asp:ListItem>
<asp:ListItem Value="145">Follow-up outreach</asp:ListItem>
<asp:ListItem Value="146">Not specified</asp:ListItem>
<asp:ListItem Value="147">Other</asp:ListItem>
</asp:DropDownList></td>
<td><asp:TextBox ID="txtmc15" runat="server" Visible="false"></asp:TextBox></td>
</tr>
<tr>
<td><asp:DropDownList ID="ddlmc21" runat="server" AutoPostBack="false"
        onselectedindexchanged="ddlmc21_SelectedIndexChanged" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="211">District staff</asp:ListItem>
<asp:ListItem Value="212">School staff</asp:ListItem>
<asp:ListItem Value="213">Consultant</asp:ListItem>
<asp:ListItem Value="214">Not specified</asp:ListItem>
<asp:ListItem Value="215">Other</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="mcOther21" runat="server" visible="false" />
</td>
<td><asp:TextBox ID="txtmc22" runat="server"></asp:TextBox></td>
<td><asp:DropDownList ID="ddlmc23" runat="server" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem  Value="231">Yes</asp:ListItem>
<asp:ListItem Value="232">No</asp:ListItem>
<asp:ListItem Value="233">Not specified</asp:ListItem>
</asp:DropDownList></td>
<td><asp:DropDownList ID="ddlmc24" runat="server" AutoPostBack="true" 
        onselectedindexchanged="ddlmc24_SelectedIndexChanged" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="241">General marketing and recruitment</asp:ListItem>
<asp:ListItem Value="242">School image development</asp:ListItem>
<asp:ListItem Value="243">Marketing and recruitment plans</asp:ListItem>
<asp:ListItem Value="244">Outreach</asp:ListItem>
<asp:ListItem Value="245">Follow-up outreach</asp:ListItem>
<asp:ListItem Value="246">Not specified</asp:ListItem>
<asp:ListItem Value="247">Other</asp:ListItem>
</asp:DropDownList></td>
<td><asp:TextBox ID="txtmc25" runat="server" Visible="false"></asp:TextBox></td>
</tr>
<tr>
<td><asp:DropDownList ID="ddlmc31" runat="server" AutoPostBack="false"
        onselectedindexchanged="ddlmc31_SelectedIndexChanged" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="311">District staff</asp:ListItem>
<asp:ListItem Value="312">School staff</asp:ListItem>
<asp:ListItem Value="313">Consultant</asp:ListItem>
<asp:ListItem Value="314">Not specified</asp:ListItem>
<asp:ListItem Value="315">Other</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="mcOther31" runat="server" visible="false" />
</td>
<td><asp:TextBox ID="txtmc32" runat="server"></asp:TextBox></td>
<td><asp:DropDownList ID="ddlmc33" runat="server" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem  Value="331">Yes</asp:ListItem>
<asp:ListItem Value="332">No</asp:ListItem>
<asp:ListItem Value="333">Not specified</asp:ListItem>
</asp:DropDownList></td>
<td><asp:DropDownList ID="ddlmc34" runat="server" AutoPostBack="true"
        onselectedindexchanged="ddlmc34_SelectedIndexChanged" CssClass="DropDownListCssClass">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="341">General marketing and recruitment</asp:ListItem>
<asp:ListItem Value="342">School image development</asp:ListItem>
<asp:ListItem Value="343">Marketing and recruitment plans</asp:ListItem>
<asp:ListItem Value="344">Outreach</asp:ListItem>
<asp:ListItem Value="345">Follow-up outreach</asp:ListItem>
<asp:ListItem Value="346">Not specified</asp:ListItem>
<asp:ListItem Value="347">Other</asp:ListItem>
</asp:DropDownList></td>
<td><asp:TextBox ID="txtmc35" runat="server" Visible="false"></asp:TextBox></td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
    </td>
    </tr>
    <tr>
    <td  class="consult_popupTxt" style="padding-top:20px; padding-bottom:15px;"">
    Marketing and recruitment activities:
    </td>
    <td align="right">
     <asp:Button ID="Button6" runat="server" CausesValidation="True" 
      CommandName="Update" CssClass="surveyBtn2" OnClick="SaveToDB" 
      Text="Save Record" />
    </td>
    </tr>
<tr>
        <td colspan="2"  style="padding-bottom:20px;">
  <asp:UpdatePanel ID="UpdatePanel11" runat="server">
<ContentTemplate>
  <telerik:RadPanelBar runat="server" ID="rpnlMarketRec" Width="840px"  >
            <Items>
                <telerik:RadPanelItem  Text="Grant Year 1"  Font-Bold="true" Expanded="false">
                <Items>
                <telerik:RadPanelItem runat="server" Font-Bold="true" ChildGroupCssClass="MyAccordinFont" Text="School image development" Value="11|8">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                <col width="418" />
                <col width="400" />
                <tr>
                <td>
                <asp:CheckBox ID="cbx111" Text="Market, audience, and brand familiarity" runat="server" />
                </td>
                 <td>
                <asp:CheckBox ID="cbx112" Text="Assessment of logo/branding" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx113" Text="Logo/branding development" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx114" Text="Website redesign/development" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx115" Text="Enhancing/decorating school building in accordance with magnet theme" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx116" Text="Assessment of school’s perception in community" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx117" Text="Addressing negative school image" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx118" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther118" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing and recruitment plans" Value="12|7">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                 <col width="418" />
                <col width="400" />
                <tr>
                <td>
                <asp:CheckBox ID="cbx121" Text="Marketing plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx122" Text="Recruitment plan" runat="server" />
                </td>
                <tr>
                <td>
                <asp:CheckBox ID="cbx123" Text="Retention plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx124" Text="Referral plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx125" Text="Re-enrollment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx126" Text="Monitoring plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                 <asp:CheckBox ID="cbx127" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther127" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment outreach" Value="13|11">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                <col width="418" />
                <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx131" Text="Broadcasts (radio/television)" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx132" Text="Print media" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx133" Text="Targeted recruitment" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                    <asp:TextBox ID="txtOther133" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx134" Text="Web-based activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx135" Text="Electronic/direct calls" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx136" Text="Tours and open houses" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx137" Text="Recruitment fairs/expos" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx138" Text="General recruitment" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                    <asp:TextBox ID="txtOther138" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx139" Text="Community activities" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx1310" Text="Feeder/charter/private/other school presentations and visits" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx1311" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                    <asp:TextBox ID="txtOther1311" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment follow-up outreach" Value="14|6">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                  <col width="418" />
                  <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx141" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with inquiries" runat="server" /><br />
                    <asp:TextBox ID="txtOther141" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx142" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with recruitment event participants" runat="server" /><br />
                    <asp:TextBox ID="txtOther142" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx143" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with applicants" runat="server" /><br />
                    <asp:TextBox ID="txtOther143" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx144" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with admits" runat="server" /><br />
                    <asp:TextBox ID="txtOther144" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx145" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Assessed effectiveness of outreach" runat="server" /><br />
                    <asp:TextBox ID="txtOther145" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx146" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                    <asp:TextBox ID="txtOther146" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing research" Value="15|4">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                  <col width="418" />
                  <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx151" Text="Survey" runat="server" /><br />
                </td>
                <td>
                    <asp:CheckBox ID="cbx152"  Text="Focus group" runat="server" /><br />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx153" Text="Extant data" runat="server" /><br />
                </td>
                <td>
                    <asp:CheckBox ID="cbx154" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                    <asp:TextBox ID="txtOther154" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                </Items>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem  Text="Grant Year 2"  Font-Bold="true" Expanded="false">
                <Items>
                <telerik:RadPanelItem runat="server" Font-Bold="true" ChildGroupCssClass="MyAccordinFont" Text="School image development" Value="21|8">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                <col width="418" />
                <col width="400" />
                <tr>
                <td>
                <asp:CheckBox ID="cbx211" Text="Market, audience, and brand familiarity" runat="server" />
                </td>
                 <td>
                <asp:CheckBox ID="cbx212" Text="Assessment of logo/branding" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx213" Text="Logo/branding development" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx214" Text="Website redesign/development" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx215" Text="Enhancing/decorating school building in accordance with magnet theme" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx216" Text="Assessment of school’s perception in community" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx217" Text="Addressing negative school image" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx218" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther218" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing and recruitment plans" Value="22|7">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                <col width="418" />
                <col width="400" />
                <tr>
                <td>
                <asp:CheckBox ID="cbx221" Text="Marketing plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx222" Text="Recruitment plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx223" Text="Retention plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx224" Text="Referral plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx225" Text="Re-enrollment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx226" Text="Monitoring plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                 <asp:CheckBox ID="cbx227" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther227" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment outreach" Value="23|11">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                <col width="418" />
                <col width="400" />
                                <tr>
                <td>
                    <asp:CheckBox ID="cbx231" Text="Broadcasts (radio/television)" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx232" Text="Print media" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx233" Text="Targeted recruitment" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                    <asp:TextBox ID="txtOther233" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx234" Text="Web-based activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx235" Text="Electronic/direct calls" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx236" Text="Tours and open houses" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx237" Text="Recruitment fairs/expos" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx238" Text="General recruitment" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                    <asp:TextBox ID="txtOther238" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx239" Text="Community activities" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx2310" Text="Feeder/charter/private/other school presentations and visits" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx2311" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                    <asp:TextBox ID="txtOther2311" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment follow-up outreach" Value="24|6">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                 <col width="418" />
                <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx241" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with inquiries" runat="server" /><br />
                    <asp:TextBox ID="txtOther241" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx242" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with recruitment event participants" runat="server" /><br />
                    <asp:TextBox ID="txtOther242" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx243" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with applicants" runat="server" /><br />
                    <asp:TextBox ID="txtOther243" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx244" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with admits" runat="server" /><br />
                    <asp:TextBox ID="txtOther244" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx245" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Assessed effectiveness of outreach" runat="server" /><br />
                    <asp:TextBox ID="txtOther245" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx246" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                    <asp:TextBox ID="txtOther246" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing research" Value="25|4">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                <col width="418" />
                <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx251" Text="Survey" runat="server" /><br />
                </td>
                <td>
                    <asp:CheckBox ID="cbx252"  Text="Focus group" runat="server" /><br />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx253" Text="Extant data" runat="server" /><br />
                </td>
                <td>
                    <asp:CheckBox ID="cbx254" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                    <asp:TextBox ID="txtOther254" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                </Items>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem  Text="Grant Year 3" Font-Bold="true"  Expanded="false">
                 <Items>
                <telerik:RadPanelItem runat="server" Font-Bold="true" ChildGroupCssClass="MyAccordinFont" Text="School image development" Value="31|8">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                 <col width="418" />
                <col width="400" />
                <tr>
                <td>
                <asp:CheckBox ID="cbx311" Text="Market, audience, and brand familiarity" runat="server" />
                </td>
                 <td>
                <asp:CheckBox ID="cbx312" Text="Assessment of logo/branding" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx313" Text="Logo/branding development" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx314" Text="Website redesign/development" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx315" Text="Enhancing/decorating school building in accordance with magnet theme" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx316" Text="Assessment of school’s perception in community" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx317" Text="Addressing negative school image" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx318" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther318" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing and recruitment plans" Value="32|7">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                 <col width="418" />
                <col width="400" />
                <tr>
                <td>
                <asp:CheckBox ID="cbx321" Text="Marketing plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx322" Text="Recruitment plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx323" Text="Retention plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx324" Text="Referral plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx325" Text="Re-enrollment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx326" Text="Monitoring plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                 <asp:CheckBox ID="cbx327" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther327" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment outreach" Value="33|11">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                 <col width="418" />
                <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx331" Text="Broadcasts (radio/television)" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx332" Text="Print media" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx333" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Targeted recruitment" runat="server" />
                    <asp:TextBox ID="txtOther333" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td >
                    <asp:CheckBox ID="cbx334" Text="Web-based activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cb335" Text="Electronic/direct calls" runat="server" />
                </td>
                <td >
                    <asp:CheckBox ID="cbx336" Text="Tours and open houses" runat="server" />
                </td>
                </tr>
                <tr>
                <td >
                    <asp:CheckBox ID="cbx337" Text="Recruitment fairs/expos" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx338" AutoPostBack="true" OnCheckedChanged="textboxState" Text="General recruitment" runat="server" />
                    <asp:TextBox ID="txtOther338" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx339" Text="Community activities" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbx3310" Text="Feeder/charter/private/other school presentations and visits" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx3311" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                    <asp:TextBox ID="txtOther3311" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment follow-up outreach" Value="34|6">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                 <col width="418" />
                <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx341" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with inquiries" runat="server" /><br />
                    <asp:TextBox ID="txtOther341" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx342" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with recruitment event participants" runat="server" /><br />
                    <asp:TextBox ID="txtOther342" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx343" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with applicants" runat="server" /><br />
                    <asp:TextBox ID="txtOther343" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx344" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with admits" runat="server" /><br />
                    <asp:TextBox ID="txtOther344" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx345" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Assessed effectiveness of outreach" runat="server" /><br />
                    <asp:TextBox ID="txtOther345" Visible="false" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbx346" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                    <asp:TextBox ID="txtOther346" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                 <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing research" Value="35|4">
                <ItemTemplate>
                <table style="font-family:Helvetica,Arial,Sans-serif; font-size:11px;">
                <col width="418" />
                <col width="400" />
                <tr>
                <td>
                    <asp:CheckBox ID="cbx351" Text="Survey" runat="server" /><br />
                </td>
                <td>
                    <asp:CheckBox ID="cbx352"  Text="Focus group" runat="server" /><br />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx353" Text="Extant data" runat="server" /><br />
                </td>
                <td>
                    <asp:CheckBox ID="cbx354" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                    <asp:TextBox ID="txtOther354" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                </Items>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelBar>
</ContentTemplate>
</asp:UpdatePanel>        
        </td>
        </tr>
                      <tr id="Tr3" runat="server" visible="false">
                          <td class="consult_popupTxt">
                              Other:
                          </td>
                          <td>
                              <asp:TextBox ID="txtOther" runat="server" Width="450"></asp:TextBox>
                          </td>
                      </tr>
                      </table>
                      <fieldset>
                      <legend>
                      <b>Minority Group Isolation</b>
                      </legend>
                    <table style="padding:0px 10px 10px 10px; width:825px;">
                      <tr>
                          <td style="padding-top: 15px" class="consult_popupTxt" >
                              Minority isolated group (Select all that apply):
                           </td>
                          <td>
                              <asp:CheckBoxList ID="cblstMinGroup" runat="server" RepeatColumns="2" 
                                  RepeatDirection="Horizontal">
                                  <asp:ListItem>American Indian/Alaskan Native</asp:ListItem>
                                  <asp:ListItem>Asian</asp:ListItem>
                                  <asp:ListItem>Black or African American</asp:ListItem>
                                  <asp:ListItem>Hispanic/Latino</asp:ListItem>
                                  <asp:ListItem>Native Hawaiian or other Pacific Islander</asp:ListItem>
                                  <asp:ListItem>White</asp:ListItem>
                                  <asp:ListItem>Two or more races</asp:ListItem>
                              </asp:CheckBoxList>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <asp:CheckBoxList ID="cbxlstMGI" runat="server" RepeatColumns="3" CssClass="consult_popupTxt" Visible="false">
                                  <asp:ListItem>Eliminate</asp:ListItem>
                                  <asp:ListItem>Prevent</asp:ListItem>
                                  <asp:ListItem>Reduce</asp:ListItem>
                              </asp:CheckBoxList>
                          </td>
                      </tr>
                      <tr><td colspan="2"></td>
                      </tr>
                      </table>
                      </fieldset>
                 <table style="padding:0px 10px 10px 10px; width:825px;">
                      <tr><td colspan="2" style="padding-top:50px;"></td></tr>
                      <tr>
                          <td>
                              <asp:Button ID="btnSave" runat="server" CausesValidation="True" 
                                  CommandName="Update" CssClass="surveyBtn2" OnClick="OnSaveSchool" 
                                  Text="Save record" /> &nbsp;&nbsp;&nbsp;&nbsp;
                              <asp:Button ID="btnClose" runat="server" CausesValidation="False" 
                                  CssClass="surveyBtn2" OnClick="ClosePan" Text="Close window" />

                          </td>
                      </tr>
                 <tr><td colspan="2" style="padding-top:50px;"></td></tr>
                </table>
        </div>
    <asp:Panel ID="pnlAddDDL" runat="server">
        <div class="mpeDivConsult">
         <div class="mpeDivHeader" >
                Add/Edit Dropdown list
                 <span class="closeit" style="padding-right:20px;">
                <asp:LinkButton ID="lbtnClose" runat="server" Text="Close"  
                    CausesValidation="false" CssClass="closeit_link" onclick="lbtnClose_Click" />
                </span>
                </div>
                <br /><br />
     <asp:Button ID="btnNewddl" runat="server" CausesValidation="False" 
                                   CssClass="surveyBtn2" OnClick="OnNewddl" Text="New dropdown text" />
    <asp:GridView ID="gvDDLlist" runat="server" AllowPaging="True"  
                AutoGenerateColumns="False" DataKeyNames="ID" CssClass="msapTbl"  
                EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" 
                onrowcancelingedit="gvDDLlist_RowCancelingEdit" 
                onrowdeleting="gvDDLlist_RowDeleting" onrowediting="gvDDLlist_RowEditing" 
                onrowupdating="gvDDLlist_RowUpdating" 
                onpageindexchanging="gvDDLlist_PageIndexChanging1"  >
        <Columns>
        <asp:CommandField HeaderText="Edit-Update" ShowEditButton="True" />
        <asp:BoundField DataField="id" HeaderText="Item value" ReadOnly="True" />
        <asp:BoundField DataField="name" HeaderText="Item text" />
        <asp:BoundField DataField="type" HeaderText="Item type" ReadOnly="True" />
        <%--<asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />--%>
        </Columns>
        <HeaderStyle Wrap="False" />
    </asp:GridView>
       
        <br /><br /><br /><br /><br /><br />
         <asp:Button ID="Button15" runat="server" CssClass="surveyBtn2" Text="Close Window" />
         <br /><br /><br /><br /><br /><br />
         </div>
    </asp:Panel>
    <asp:LinkButton ID="lbtnTarget" runat="server"></asp:LinkButton>
    <ajax:modalpopupextender id="mpeNewsWindow" runat="server" targetcontrolid="lbtnTarget"
        popupcontrolid="pnlAddDDL" dropshadow="true" okcontrolid="Button15" cancelcontrolid="Button15"
        backgroundcssclass="magnetMPE" y="20" />
    </asp:Panel>
   <%-- <asp:SqlDataSource ID="dsThemeCat" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [TAThemeCat] ORDER BY [CatName]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsThemeSubCat" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" SelectCommand="SELECT TAThemeSubCat.* FROM TAThemeSubCat
where ThemeCatID =@ThemeCatID">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlThemeCat" Name="ThemeCatID" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsDDLlist" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [id], [name], [type] FROM [TALogDropdownLists] WHERE ([type] = @type)">
        <SelectParameters>
            <asp:ControlParameter ControlID="hfddltype" Name="type" PropertyName="Value" 
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>--%>
    <asp:HiddenField ID="hfThemeCatID" runat="server" />
    <asp:HiddenField ID="hfddltype" runat="server" />
    <div style="text-align: right; margin-top: 20px;">
            <asp:LinkButton ID="LinkButton15" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton7" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           Page 3&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton14" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton13" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
          &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton8" PostBackUrl="TAReports.aspx" ToolTip="Reports" runat="server">Page 8</asp:LinkButton>
          

            <asp:SqlDataSource ID="dsSchoolgv" runat="server" 
            ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
            SelectCommand="SELECT distinct *
            FROM vwTAlogcontextualfactors WHERE (PK_GenInfoid =@granteeID AND isActive =1  AND (ReportPeriodID =@ReportPeriodID) AND (ReportYear =@ReportYear) )  ORDER BY [SchoolName]" >
             <SelectParameters>
                   <asp:ControlParameter ControlID="ddlGrantees" Name="granteeID" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="hfReportYearID" Name="ReportYear" PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="hfReportPeriodID" Name="ReportPeriodID" PropertyName="Value" Type="Int32" />
               </SelectParameters>
</asp:SqlDataSource>
           <asp:SqlDataSource ID="dstheSchool" runat="server" 
                ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
                DeleteCommand="DELETE FROM vwTALogContextualFactors WHERE (id = @theID)" 
                InsertCommand="INSERT INTO vwTALogContextualFactors(PK_GenInfoid, SchoolName, SchoolType, ProgramType, ProgramStatus, Urbanicity, FreeReduceLunch, 
                Title1Status, Designation, LowestAchieve, SchoolGrant, DesegregationPlan, AttendType, AdmissionMethod, MagnetTheme, CurriculumnType, Desegregationact, 
                ProfDevAct, DeliveryMethod, engageAct, SustainAct, CommPartAct, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn) 
                VALUES 
                (@PK_GenInfoid,  @SchoolName,  @SchoolType,  @ProgramType,   @ProgramStatus, @Urbanicity, @FreeReduceLunch,  @Title1Status,  @Designation,  @LowestAchieve,  @SchoolGrant, @DesegregationPlan,  @AttendType,  @AdmissionMethod,  @MagnetTheme,  @CurriculumnType,  @Desegregationact,  @ProfDevAct,  @DeliveryMethod,  @engageAct,  @SustainAct,  @CommPartAct,  @CreatedBy,  @CreatedOn,  @ModifiedBy,  @ModifiedOn)" 
                SelectCommand="SELECT id, PK_GenInfoid,  SchoolName, SchoolType, ProgramType, ProgramStatus, Urbanicity, 
                FreeReduceLunch, Title1Status, Designation, LowestAchieve, SchoolGrant, DesegregationPlan, AttendType,
                 AdmissionMethod, MagnetTheme, CurriculumnType, Desegregationact, ProfDevAct, DeliveryMethod, engageAct,
                 SustainAct, CommPartAct, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn 
                 FROM vwTALogContextualFactors 
                 WHERE (id =@theID AND isActive =1 ) order by id desc " 
                UpdateCommand="UPDATE vwTAlogcontextualfactors SET  ProgramStatus =@ProgramStatus, Urbanicity =@Urbanicity, 
                FreeReduceLunch =@FreeReduceLunch,  DesegregationPlan =@DesegregationPlan, AttendType =@AttendType,
                 AdmissionMethod =@AdmissionMethod, MagnetTheme =@MagnetTheme, CurriculumnType =@CurriculumnType, 
                 Desegregationact =@Desegregationact, ProfDevAct =@ProfDevAct, DeliveryMethod =@DeliveryMethod, 
                 engageAct =@engageAct, SustainAct =@SustainAct, CommPartAct =@CommPartAct, 
                ModifiedBy =@ModifiedBy, ModifiedOn =@ModifiedOn WHERE ID =@theID" >
                 <SelectParameters>
                  <asp:ControlParameter ControlID="hfeditSchoolID" Name="theID" PropertyName="Value" Type="Int32" />
               </SelectParameters>
               <DeleteParameters>
                    <asp:ControlParameter ControlID="hfeditSchoolID" Name="theID" PropertyName="Value" Type="Int32" />
               </DeleteParameters>
               <InsertParameters>
               <asp:ControlParameter ControlID="ddlGrantees" Name="PK_GenInfoid" PropertyName="SelectedValue" Type="Int32" />
                <asp:Parameter Name="schoolname" />
                <asp:Parameter Name="SchoolType" />
                <asp:Parameter Name="programtype" />
                <asp:Parameter Name="programstatus" />
                <asp:Parameter Name="urbanicity" />
                <asp:Parameter Name="freereducelunch" />
                <asp:Parameter Name="title1status" />
                <asp:Parameter Name="designation" />
                <asp:Parameter Name="lowestachieve" />
                <asp:Parameter Name="schoolgrant" />
                <asp:Parameter Name="desegregationplan" />
                <asp:Parameter Name="attendtype" />
                <asp:Parameter Name="admissionmethod" />
                <asp:Parameter Name="magnettheme" />
                <asp:Parameter Name="curriculumntype" />
                <asp:Parameter Name="desegregationact" />
                <asp:Parameter Name="profdevact" />
                <asp:Parameter Name="deliverymethod" />
                <asp:Parameter Name="engageact" />
                <asp:Parameter Name="sustainact" />
                <asp:Parameter Name="CommPartAct" />
                <asp:Parameter Name="createdby" />
                <asp:Parameter Name="createdon" />
                <asp:Parameter Name="modifiedby" />
                <asp:Parameter Name="modifiedon" />
               </InsertParameters>
               <UpdateParameters>
               <asp:ControlParameter ControlID="hfeditSchoolID" Name="theID" PropertyName="Value" Type="Int32" />
                   <asp:Parameter Name="SchoolName" />
                   <asp:Parameter Name="SchoolType" />
                   <asp:Parameter Name="ProgramType" />
                   <asp:Parameter Name="ProgramStatus" />
                   <asp:Parameter Name="Urbanicity" />
                   <asp:Parameter Name="FreeReduceLunch" />
                   <asp:Parameter Name="Title1Status" />
                   <asp:Parameter Name="Designation" />
                   <asp:Parameter Name="LowestAchieve" />
                   <asp:Parameter Name="SchoolGrant" />
                   <asp:Parameter Name="DesegregationPlan" />
                   <asp:Parameter Name="AttendType" />
                   <asp:Parameter Name="AdmissionMethod" />
                   <asp:Parameter Name="MagnetTheme" />
                   <asp:Parameter Name="CurriculumnType" />
                   <asp:Parameter Name="Desegregationact" />
                   <asp:Parameter Name="ProfDevAct" />
                   <asp:Parameter Name="DeliveryMethod" />
                   <asp:Parameter Name="engageAct" />
                   <asp:Parameter Name="SustainAct" />
                   <asp:Parameter Name="CommPartAct" />
                   <asp:Parameter Name="ModifiedBy" />
                   <asp:Parameter Name="ModifiedOn" />
               </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    <asp:SqlDataSource ID="dsDesignation" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT [id], [statusvalue], [statusitem], [note] FROM [MagnetGPRAImprovementStatus]"></asp:SqlDataSource>
    

</asp:Content>

