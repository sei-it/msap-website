﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Web.Security;

public partial class admin_TALogs_MSAPCenterMassCommunication : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["showMassComm"] = null;
        }

        vwUserProfileDataContext db = new vwUserProfileDataContext();
        if (HttpContext.Current.User.IsInRole("ED"))
            lblstaffmember.Text = "ED staff member:";
        else
            lblstaffmember.Text = "MSAP center staff member:";

        string userName = HttpContext.Current.User.Identity.Name;
        var usrdata = from r in db.vwUserProfiles
                      where r.Name == userName
                      select r;
        string FullName = "";
        foreach (var itm in usrdata)
        {
            FullName = (itm.FirstName + " " + itm.LastName).Trim();
            break;
        }
        if (string.IsNullOrEmpty(FullName))
            FullName = userName;
        txtsender.Text = FullName;
    }

    private void initialFields()
    {
        hfID.Value = "";
        txtDate.Text = "";
        ddltype.SelectedIndex = 0;
        txtDes.Text = "";
        ddlIssue.SelectedIndex = 0;
        txtRoleOther.Text = "";
        hfRoleOther.Value = "";
        txtRoleOther.Visible = false;
        txtIssue.Text = "";
        txtIssue.Visible = false;
        txtBounceback.Text = "";
        txtBounceback.Visible = false;
        cbxAll2010.Checked = false;
        cbxAll2013.Checked = false;
        ddlMassCommStatus.SelectedValue = "Edit Record";

        if (HttpContext.Current.User.IsInRole("ED"))
            lblstaffmember.Text = "ED staff member";
        else
            lblstaffmember.Text = "MSAP center staff member:";

        foreach (ListItem li in cblRecipients10.Items)
        {
            li.Selected = false;
        }

        foreach (ListItem li in cblRecipients13.Items)
        {
            li.Selected = false;
        }


        foreach (ListItem li in cblRole.Items)
        {
            li.Selected = false;
        }
    }
    protected void onAddnew(object sender, EventArgs e)
    {
        initialFields();
        pnlMassRcd.Visible = false;
        PopupPanel.Visible = true;

    }

     protected void OnEdit(object sender, EventArgs e)
    {
        LinkButton btnEdit = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
        int itemID = Convert.ToInt32(this.gdvMassComm.DataKeys[row.RowIndex].Values["ID"]);
        hfID.Value = itemID.ToString();
        var data = TALogMassComm.SingleOrDefault(x => x.id == itemID);

        txtDate.Text = Convert.ToDateTime(data.date).ToShortDateString();
        ddltype.SelectedValue = data.type;
        txtDes.Text = data.description;
        ddlIssue.SelectedValue = data.Issue;
        txtBounceback.Visible = false;
        txtBounceback.Text = "";

        txtIssue.Visible = false;
        txtIssue.Text = "";

        if (ddlIssue.SelectedItem.Text.ToLower() == "others")
        {
            txtIssue.Visible = true;
            txtIssue.Text = data.IssueOther;
        }
        else if (ddlIssue.SelectedItem.Text.ToLower() == "bounce-backs")
        {
            txtBounceback.Visible = true;
            txtBounceback.Text = data.IssueBounceback;
        }

        hfRecipientIDs.Value = data.Recipients;
        if (!string.IsNullOrEmpty(hfRecipientIDs.Value))
        {
            foreach (ListItem li in cblRecipients10.Items)
                li.Selected = false;
            foreach (ListItem li in cblRecipients13.Items)
                li.Selected = false;

            foreach (string str in data.Recipients.Split(','))
            {
                foreach (ListItem li in cblRecipients10.Items)
                {
                    if (li.Value.Equals(str))
                    {
                        li.Selected = true;
                        break;
                    }
                   
                }

               
                foreach (ListItem li in cblRecipients13.Items)
                {
                    if (li.Value.Equals(str))
                    {
                        li.Selected = true;
                        break;
                    }
                    
                }
            }
        }

        hfRoleIDs.Value = data.role_ids;
        if (!string.IsNullOrEmpty(hfRoleIDs.Value))
        {
            foreach (ListItem li in cblRole.Items)
                li.Selected = false;
            foreach (string str in data.role_ids.Split(','))
            {
                foreach (ListItem li in cblRole.Items )
                {
                    if (li.Value.Equals(str))
                    {
                        li.Selected = true;
                        break;
                    }
                }
            }
        }

        hfRoleOther.Value = data.Role_other;
        txtRoleOther.Text = data.Role_other;

        ddlMassCommStatus.SelectedValue = data.Status;

        pnlMassRcd.Visible = false;
        PopupPanel.Visible = true;
    }
     protected void OnClose(object sender, EventArgs e)
     {
         pnlMassRcd.Visible = true;
         PopupPanel.Visible = false;
     }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetDBDB db = new MagnetDBDB();

        TALogMassComm data = new TALogMassComm();
        IList<TALogCommunication> commdatas = new List<TALogCommunication>();
        string strRoles = "", strRecipients="";
        int MassCommId =0;

        foreach (ListItem li in  cblRole.Items)
        {
            if (li.Selected)
            {
                if (!string.IsNullOrEmpty(strRoles))
                    strRoles += ",";
                strRoles += li.Value;
            }
        }
        foreach (ListItem li in  cblRecipients10.Items)
        {
            if (li.Selected)
            {
                if (!string.IsNullOrEmpty(strRecipients))
                    strRecipients += ",";
                strRecipients += li.Value;
            }
        }
        foreach (ListItem li in cblRecipients13.Items)
        {
            if (li.Selected)
            {
                if (!string.IsNullOrEmpty(strRecipients))
                    strRecipients += ",";
                strRecipients += li.Value;
            }
        }

        if (!string.IsNullOrEmpty(hfID.Value))  //update
        {
            MassCommId = Convert.ToInt32(hfID.Value);
            data = TALogMassComm.SingleOrDefault(x => x.id == MassCommId);
            commdatas = TALogCommunication.Find(y => y.MassComm_id == MassCommId);
        }
        else //add new record(s)
        {
            data.CreatedBy = HttpContext.Current.User.Identity.Name;
            data.CreatedOn = DateTime.Now;
        }
        data.ModifiedBy = HttpContext.Current.User.Identity.Name;
        data.ModifiedOn = DateTime.Now;

        data.date = Convert.ToDateTime(txtDate.Text);
        data.type = ddltype.SelectedValue;
        data.description = txtDes.Text;
        data.Issue = ddlIssue.SelectedValue;
        data.IssueOther = txtIssue.Text;
        data.IssueBounceback = txtBounceback.Text;
        data.Sender = txtsender.Text;
        data.Recipients = strRecipients;
        data.role_ids = strRoles;
        data.Role_other = txtRoleOther.Text;
        data.isActive = true;
        data.Sender = HttpContext.Current.User.Identity.Name;

        data.Status = ddlMassCommStatus.SelectedValue;

        data.Save();


        if (HttpContext.Current.User.IsInRole("ED"))
            if (!string.IsNullOrEmpty(hfID.Value))
                sendEmailtoMSAPCenter("updated", "Mass Communication");
            else
                sendEmailtoMSAPCenter("added", "Mass Communication");
        pnlMassRcd.Visible = true;
        PopupPanel.Visible = false;

        Session["showMassComm"] = "1";
        showAllMassComm(sender, e);
    }


    private void sendEmailtoMSAPCenter(string action, string tablename)
    {
        TAlog talogOjb = new TAlog();
        string user = HttpContext.Current.User.Identity.Name;

        string datestamp = DateTime.Now.ToString();
        string Action = "updated";
        talogOjb.TALogDatahasbeenChangedbyED(user, tablename, datestamp, Action);
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        LinkButton btnDelete = sender as LinkButton;
        GridViewRow row = (GridViewRow)btnDelete.NamingContainer;
        int itemID = Convert.ToInt32(this.gdvMassComm.DataKeys[row.RowIndex].Values["ID"]);
        hfID.Value = itemID.ToString();
        var data = TALogMassComm.SingleOrDefault(x => x.id == itemID);
        data.isActive = false;
        data.Delete();
        //data.Save();

        if (HttpContext.Current.User.IsInRole("ED"))
            sendEmailtoMSAPCenter("deleted", "Mass Communication");
        gdvMassComm.DataBind();
    }

    protected void ddlIssue_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlIssue.SelectedItem.Text.ToLower()=="others")
        {
            txtIssue.Visible = true;
            txtBounceback.Visible = false;
            txtBounceback.Text = "";
        }
        else if (ddlIssue.SelectedItem.Text.ToLower() == "bounce-backs")
        {
            txtBounceback.Visible = true;
            txtIssue.Visible = false;
            txtIssue.Text = "";
        }
        else
        {
            txtBounceback.Visible = false;
            txtBounceback.Text = "";
            txtIssue.Visible = false;
            txtIssue.Text = "";
        }
    }
    protected void cblRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (ListItem li in cblRole.Items)
        {
            if (li.Text.ToLower() == "other")
            {
                if (li.Selected)
                {
                    txtRoleOther.Visible = true;
                }
                else
                {
                    txtRoleOther.Visible = false;
                    txtRoleOther.Text = "";
                }
            }
        }
    }
 
    protected void cblRole_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfRoleIDs.Value))
        {
            foreach (string str in hfRoleIDs.Value.Split(','))
            {
                foreach (ListItem li in cblRole.Items)
                {
                    if (li.Value.Equals(str))
                    {
                        if (li.Text.ToLower() == "other")
                        {
                            txtRoleOther.Text = hfRoleOther.Value;
                            txtRoleOther.Visible = true;
                        }
                        li.Selected = true;
                        break;
                    }
                }
            }
        }
    }
    protected void cblRecipients_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfRecipientIDs.Value))
        {
            foreach (string str in hfRecipientIDs.Value.Split(','))
            {
                foreach (ListItem li in cblRecipients10.Items)
                {
                    if (li.Value.Equals(str))
                    {
                        li.Selected = true;
                        break;
                    }
                }

                foreach (ListItem li in cblRecipients13.Items)
                {
                    if (li.Value.Equals(str))
                    {
                        li.Selected = true;
                        break;
                    }
                }
            }
        }
    }
    protected void cbxAll2010_CheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem item in cblRecipients10.Items)
        {
            if (cbxAll2010.Checked)
                item.Selected = true;
            else
                item.Selected = false;
        }
    }
    protected void cbxAll2013_CheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem item in cblRecipients13.Items)
        {
            if (cbxAll2013.Checked)
                item.Selected = true;
            else
                item.Selected = false;
        }
    }
    protected void showAllMassComm(object sender, EventArgs e)
    {
        //if (Session["showMassComm"] == null || Session["showMassComm"].ToString() == "0")
        //{
            btnShowAllMassComm.Text = "Show Edit Records";
            dsMassComm.SelectCommand = "select * from dbo.TALogMassComm order by Date desc";
            Session["showMassComm"] = "1";
        //}
        //else
        //{
        //    btnShowAllMassComm.Text = "Show All";
        //    dsMassComm.SelectCommand = "select * from dbo.TALogMassComm where Status = 'Edit Record' order by Date desc";
        //    Session["showMassComm"] = "0";
        //}
        dsMassComm.Select(DataSourceSelectArguments.Empty);
        dsMassComm.DataBind();
        gdvMassComm.DataBind();
    }
}