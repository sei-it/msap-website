﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="TAReports.aspx.cs" Inherits="admin_TALogs_TAReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

     <h1>
       Communication Management 
    </h1>
    <table>
    <tr>
    <td><strong>MSAP Grantee:</strong></td>
    <td> <asp:RadioButtonList ID="rbtnCohort" runat="server" RepeatColumns="2" 
        RepeatDirection="Horizontal" AutoPostBack="true" 
            onselectedindexchanged="rbtnCohort_SelectedIndexChanged">
        <asp:ListItem Value="1" Selected="True">2010 Cohort</asp:ListItem>
        <asp:ListItem Value="2">2013 Cohort</asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    </table>
   
    <asp:DropDownList ID="ddlGrantees" runat="server" DataSourceID="dsGrantee" 
        AppendDataBoundItems="true" AutoPostBack="true" CssClass="DropDownListCssClass"
        DataTextField="GranteeName" DataValueField="ID" 
        onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
        <asp:ListItem Value="">Select one</asp:ListItem>
    </asp:DropDownList>
    <br /><br />
    <div class="titles">Reports</div>
  <div class="tab_area">
    	<ul class="tabs">
        <li class="tab_active">Page 8</li>
         <li><asp:LinkButton ID="LinkButton16" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton4" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton5" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton9" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton6" PostBackUrl="ContextualFactors.aspx" ToolTip="School Information" runat="server">Page 3</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton1" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton></li>
            <li><asp:LinkButton ID="LinkButton10" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton></li>
        </ul>
    </div>
    <br/>
    <ul>
       <%-- <li><asp:LinkButton ID="lbtnGranteeCharacteristics" runat="server" >Grantee Characteristics</asp:LinkButton></li>
        <li><asp:LinkButton ID="LinkButton3" runat="server" >School Characteristics</asp:LinkButton></li>
        <li><asp:LinkButton ID="LinkButton8" runat="server">Student Economic Status</asp:LinkButton></li>
         <li><asp:LinkButton ID="LinkButton17" runat="server" >School Title I status</asp:LinkButton></li>
        <li><asp:LinkButton ID="LinkButton18" runat="server">School Accountability Designations</asp:LinkButton></li>
         <li><asp:LinkButton ID="LinkButton19" runat="server" >District and School Policy</asp:LinkButton></li>--%>
        <li><asp:LinkButton ID="lbtnGranteeCharact" runat="server" 
                onclick="lbtnGranteeCharact_Click">Grantee Characteristics</asp:LinkButton></li>
         <li><asp:LinkButton ID="lbtnSchoolCharact" runat="server" 
                 onclick="lbtnSchoolCharact_Click" >School Characteristics</asp:LinkButton></li>
        <li><asp:LinkButton ID="LinkButton22" runat="server">Student Academic Achievement</asp:LinkButton></li>
         <li><asp:LinkButton ID="LinkButton23" runat="server" >MSAP Activities</asp:LinkButton></li>
      
     </ul>
    <ul>
     <li><a href="#">List of detailed TA requests by grantee</a></li>
     <li><a href="#">List of TA requests by category across all grantees</a></li>
     <li><a href="#">List of TA requests by status across all grantees</a></li>
     <li><a href="#">List of TA requests assigned for follow up to a specific staff member</a></li>
     <li><a href="#">List of all contacts across grantees by role</a></li>
     <li><a href="#">List of participations by grantee and by activity</a></li>
     </ul>
        <div style="text-align: right; margin-top: 20px;">
           <asp:LinkButton ID="LinkButton15" PostBackUrl="Overview.aspx" ToolTip="Overview" runat="server">Page 1</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton2" PostBackUrl="ContactInfo.aspx" ToolTip="Contact Information" runat="server">Page 2</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton13" PostBackUrl="ContextualFactors.aspx" ToolTip="School Information" runat="server">Page 3</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton14" PostBackUrl="Communications.aspx" ToolTip="Communications" runat="server">Page 4</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton12" PostBackUrl="MSAPCenterActParticipation.aspx" ToolTip="MSAP Center activity participation" runat="server">Page 5</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:LinkButton ID="LinkButton11" PostBackUrl="MSAPCenterMassCommunication.aspx" ToolTip="MSAP Center mass communication" runat="server">Page 6</asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton7" PostBackUrl="TCallIndex.aspx" ToolTip="Post Award Phone call " runat="server">Page 7</asp:LinkButton>
          &nbsp;&nbsp;&nbsp;&nbsp;Page 8
           
          </div>
          <asp:SqlDataSource ID="dsGrantee" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 

        SelectCommand="SELECT * FROM [MagnetGrantees] WHERE ([CohortType] = @CohortType AND isActive=1)" >
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnCohort" Name="CohortType" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

