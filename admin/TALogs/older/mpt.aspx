﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="mpt.aspx.cs" Inherits="admin_TALogs_mktRecruitTmplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<style type="text/css">
.otherTxt
{
  display:none;
}
</style>
<script type="text/javascript" language="javascript">
$(document).ready(function () {
        });

   $(".change").change(function () {

               if ($("#r1").attr("checked")) {
            $('#r1edit:input').show();
        }
        else {
            $('#r1edit:input').hide();
            $('#r1edit:input').val("");
        }


   });

    function onPanelItemClicking(sender, eventArgs) {
        eventArgs.get_domEvent().stopPropagation();
    }  

</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <%--<telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" />--%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
   <ContentTemplate>
    <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Width="750px">
     <Items>
                <telerik:RadPanelItem Text="Grant Year 1" Expanded="false">
                <Items>
                <telerik:RadPanelItem runat="server" Text="School image development" Value="11">
                <ItemTemplate>
                    <table>
                    <tr>
                    <td>
                        <asp:CheckBox ID="cbx1" runat="server" CssClass="change11"  AutoPostBack="true" OnCheckedChanged="textboxState" /></td>
                    <td>
                        <asp:TextBox ID="txtOther1" runat="server" Visible="false" ></asp:TextBox></td>
                    </tr>
                    <tr>
                    <td>
                        <asp:CheckBox ID="cbx2" runat="server" AutoPostBack="true" CssClass="change11" OnCheckedChanged="textboxState" /></td>
                    <td>
                        <asp:TextBox ID="txtOther2" runat="server" Visible="false"></asp:TextBox></td>
                    </tr>
                    </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Text="Marketing and recruitment plans" Value="12">
                <ItemTemplate>
                </ItemTemplate>
                </telerik:RadPanelItem>
                </Items>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem Text="Grant Year 2" Expanded="false">
                <Items>
                <telerik:RadPanelItem runat="server" Text="School image development" Value="11">
                </telerik:RadPanelItem>
                </Items>
                
                </telerik:RadPanelItem>
    </Items>
    </telerik:RadPanelBar>
    </ContentTemplate>
    </asp:UpdatePanel>
<h1>Marketing and Recruitment</h1>
<h3>Who has primary responsibility for the school’s marketing and recruitment? </h3>
<asp:UpdatePanel ID="UpdatePanel10" runat="server">
<ContentTemplate>
<table width="100%">
<tr>
<th>Staff type</th>
<th>Staff title</th>
<th>Grant funded</th>
<th>Activities responsible for</th>
</tr>
<tr>
<td><asp:DropDownList ID="ddlmc11" runat="server" AutoPostBack="true"
        onselectedindexchanged="ddlmc11_SelectedIndexChanged">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="111">District staff</asp:ListItem>
<asp:ListItem Value="112">School staff</asp:ListItem>
<asp:ListItem Value="113">Consultant</asp:ListItem>
<asp:ListItem Value="114">Not specified</asp:ListItem>
<asp:ListItem Value="115">Other</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="mcOther11" runat="server" visible="false" />
</td>
<td><asp:TextBox ID="txtmc12" runat="server"></asp:TextBox></td>
<td><asp:DropDownList ID="ddlmc13" runat="server">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem  Value="131">Yes</asp:ListItem>
<asp:ListItem Value="132">No</asp:ListItem>
<asp:ListItem Value="133">Not specified</asp:ListItem>
</asp:DropDownList></td>
<td><asp:DropDownList ID="ddlmc14" runat="server" AutoPostBack="true" 
        onselectedindexchanged="ddlmc14_SelectedIndexChanged">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="141">General marketing and recruitment</asp:ListItem>
<asp:ListItem Value="142">School image development</asp:ListItem>
<asp:ListItem Value="143">Marketing and recruitment plans</asp:ListItem>
<asp:ListItem Value="144">Outreach</asp:ListItem>
<asp:ListItem Value="145">Follow-up outreach</asp:ListItem>
<asp:ListItem Value="146">Not specified</asp:ListItem>
<asp:ListItem Value="147">Other</asp:ListItem>
</asp:DropDownList></td>
<td><asp:TextBox ID="txtmc15" runat="server" Visible="false"></asp:TextBox></td>
</tr>
<tr>
<td><asp:DropDownList ID="ddlmc21" runat="server" 
        onselectedindexchanged="ddlmc21_SelectedIndexChanged">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="211">District staff</asp:ListItem>
<asp:ListItem Value="212">School staff</asp:ListItem>
<asp:ListItem Value="213">Consultant</asp:ListItem>
<asp:ListItem Value="214">Not specified</asp:ListItem>
<asp:ListItem Value="215">Other</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="mcOther21" runat="server" visible="false" />
</td>
<td><asp:TextBox ID="txtmc22" runat="server"></asp:TextBox></td>
<td><asp:DropDownList ID="ddlmc23" runat="server">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem  Value="231">Yes</asp:ListItem>
<asp:ListItem Value="232">No</asp:ListItem>
<asp:ListItem Value="233">Not specified</asp:ListItem>
</asp:DropDownList></td>
<td><asp:DropDownList ID="ddlmc24" runat="server" AutoPostBack="true" 
        onselectedindexchanged="ddlmc24_SelectedIndexChanged">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="241">General marketing and recruitment</asp:ListItem>
<asp:ListItem Value="242">School image development</asp:ListItem>
<asp:ListItem Value="243">Marketing and recruitment plans</asp:ListItem>
<asp:ListItem Value="244">Outreach</asp:ListItem>
<asp:ListItem Value="245">Follow-up outreach</asp:ListItem>
<asp:ListItem Value="246">Not specified</asp:ListItem>
<asp:ListItem Value="247">Other</asp:ListItem>
</asp:DropDownList></td>
<td><asp:TextBox ID="txtmc25" runat="server" Visible="false"></asp:TextBox></td>
</tr>
<tr>
<td><asp:DropDownList ID="ddlmc31" runat="server" 
        onselectedindexchanged="ddlmc31_SelectedIndexChanged">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="311">District staff</asp:ListItem>
<asp:ListItem Value="312">School staff</asp:ListItem>
<asp:ListItem Value="313">Consultant</asp:ListItem>
<asp:ListItem Value="314">Not specified</asp:ListItem>
<asp:ListItem Value="315">Other</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="mcOther31" runat="server" visible="false" />
</td>
<td><asp:TextBox ID="txtmc32" runat="server"></asp:TextBox></td>
<td><asp:DropDownList ID="ddlmc33" runat="server">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem  Value="331">Yes</asp:ListItem>
<asp:ListItem Value="332">No</asp:ListItem>
<asp:ListItem Value="333">Not specified</asp:ListItem>
</asp:DropDownList></td>
<td><asp:DropDownList ID="ddlmc34" runat="server" AutoPostBack="true"
        onselectedindexchanged="ddlmc34_SelectedIndexChanged">
<asp:ListItem Value="">Select one</asp:ListItem>
<asp:ListItem Value="341">General marketing and recruitment</asp:ListItem>
<asp:ListItem Value="342">School image development</asp:ListItem>
<asp:ListItem Value="343">Marketing and recruitment plans</asp:ListItem>
<asp:ListItem Value="344">Outreach</asp:ListItem>
<asp:ListItem Value="345">Follow-up outreach</asp:ListItem>
<asp:ListItem Value="346">Not specified</asp:ListItem>
<asp:ListItem Value="347">Other</asp:ListItem>
</asp:DropDownList></td>
<td><asp:TextBox ID="txtmc35" runat="server" Visible="false"></asp:TextBox></td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>


  <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">  
        <Items> 
            <telerik:RadPanelItem runat="server" Text="Panel 1" Expanded="true">  
                <Items> 
                    <telerik:RadPanelItem runat="server" Expanded="true">  
                        <ItemTemplate> 
                            some text and controls some text and controls  
                            <br /> 
                            some text and controls some text and controls  
                            <br /> 
                            <telerik:RadPanelBar ID="RadPanelBar2" runat="server" Width="100%" OnClientItemClicking="onPanelItemClicking">  
                                <Items> 
                                    <telerik:RadPanelItem runat="server" Text="Sub Panel 1" Expanded="true">  
                                        <Items> 
                                            <telerik:RadPanelItem runat="server" Expanded="true">  
                                                <ItemTemplate> 
                                                    some text and controls some text and controls  
                                                    <br /> 
                                                    some text and controls some text and controls  
                                                    <br /> 
                                                </ItemTemplate> 
                                            </telerik:RadPanelItem> 
                                        </Items> 
                                    </telerik:RadPanelItem> 
                                </Items> 
                            </telerik:RadPanelBar> 
                            some text and controls some text and controls  
                            <br /> 
                            some text and controls some text and controls  
                            <br /> 
                        </ItemTemplate> 
                    </telerik:RadPanelItem> 
                </Items> 
            </telerik:RadPanelItem> 
        </Items> 
    </telerik:RadPanelBar> 

<br /><br /><br />
<h3>Marketing and recruitment activities</h3>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
  <telerik:RadPanelBar runat="server" ID="rpnlMarketRec" Width="750px">
            <Items>
                <telerik:RadPanelItem Font-Bold="true" Text="Grant Year 1" Expanded="false">
                <Items>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="School image development" Value="11">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                <asp:CheckBox ID="cbx111" Text="Market, audience, and brand familiarity" runat="server" />
                </td>
                 <td>
                <asp:CheckBox ID="cbx112" Text="Assessment of logo/branding" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx113" Text="Logo/branding development" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx114" Text="Website redesign/development" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx115" Text="Enhancing/decorating school building in accordance with magnet theme" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx116" Text="Assessment of school’s perception in community" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx117" Text="Addressing negative school image" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx118" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther118" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing and recruitment plans" Value="12">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                <asp:CheckBox ID="cbx121" Text="Marketing plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx122" Text="Recruitment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx123" Text="Retention plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx124" Text="Referral plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx125" Text="Re-enrollment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx126" Text="Monitoring plan" runat="server" />
                </td>
                <td>
                 <asp:CheckBox ID="cbx127" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                </td>
                <td>
                <asp:TextBox ID="txtOther127" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment outreach" Value="13">
                <ItemTemplate>
                <table>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx131" Text="Broadcasts (radio/television)" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx132" Text="Print media" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx133" Text="Targeted recruitment" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther133" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx134" Text="Web-based activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx135" Text="Electronic/direct calls" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx136" Text="Tours and open houses" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx137" Text="Recruitment fairs/expos" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx138" Text="General recruitment" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther138" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx139" Text="Community activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx1310" Text="Feeder/charter/private/other school presentations and visits" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx1311" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther1311" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment follow-up outreach" Value="14">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx141" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with inquiries" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther141" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx142" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with recruitment event participants" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther142" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx143" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with applicants" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther143" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx144" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with admits" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther144" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx145" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Assessed effectiveness of outreach" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther145" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx146" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther146" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                </Items>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem Font-Bold="true" Text="Grant Year 2" Expanded="false">
                <Items>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="School image development" Value="21">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                <asp:CheckBox ID="cbx211" Text="Market, audience, and brand familiarity" runat="server" />
                </td>
                 <td>
                <asp:CheckBox ID="cbx212" Text="Assessment of logo/branding" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx213" Text="Logo/branding development" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx214" Text="Website redesign/development" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx215" Text="Enhancing/decorating school building in accordance with magnet theme" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx216" Text="Assessment of school’s perception in community" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx217" Text="Addressing negative school image" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx218" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther218" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing and recruitment plans" Value="22">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                <asp:CheckBox ID="cbx221" Text="Marketing plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx222" Text="Recruitment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx223" Text="Retention plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx224" Text="Referral plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx225" Text="Re-enrollment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx226" Text="Monitoring plan" runat="server" />
                </td>
                <td>
                 <asp:CheckBox ID="cbx227" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                </td>
                <td>
                <asp:TextBox ID="txtOther227" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment outreach" Value="23">
                <ItemTemplate>
                <table>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx231" Text="Broadcasts (radio/television)" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx232" Text="Print media" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx233" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Targeted recruitment" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther233" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx234" Text="Web-based activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx235" Text="Electronic/direct calls" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx236" Text="Tours and open houses" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx237" Text="Recruitment fairs/expos" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx238" AutoPostBack="true" OnCheckedChanged="textboxState" Text="General recruitment" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther238" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx239" Text="Community activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx2310" Text="Feeder/charter/private/other school presentations and visits" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx2311" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther2311" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment follow-up outreach" Value="24">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx241" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with inquiries" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther241" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx242" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with recruitment event participants" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther242" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx243" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with applicants" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther243" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx244" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with admits" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther244" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx245" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Assessed effectiveness of outreach" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther245" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx246" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther246" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                </Items>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem Font-Bold="true" Text="Grant Year 3" Expanded="false">
                 <Items>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="School image development" Value="31">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                <asp:CheckBox ID="cbx311" Text="Market, audience, and brand familiarity" runat="server" />
                </td>
                 <td>
                <asp:CheckBox ID="cbx312" Text="Assessment of logo/branding" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx313" Text="Logo/branding development" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx314" Text="Website redesign/development" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx315" Text="Enhancing/decorating school building in accordance with magnet theme" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx316" Text="Assessment of school’s perception in community" runat="server" />
                </td>
                </tr>
                 <tr>
                <td>
                <asp:CheckBox ID="cbx317" Text="Addressing negative school image" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx318" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                <asp:TextBox ID="txtOther318" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing and recruitment plans" Value="32">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                <asp:CheckBox ID="cbx321" Text="Marketing plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx322" Text="Recruitment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx323" Text="Retention plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx324" Text="Referral plan" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                <asp:CheckBox ID="cbx325" Text="Re-enrollment plan" runat="server" />
                </td>
                <td>
                <asp:CheckBox ID="cbx326" Text="Monitoring plan" runat="server" />
                </td>
                <td>
                 <asp:CheckBox ID="cbx327" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="textboxState" />
                </td>
                <td>
                <asp:TextBox ID="txtOther327" runat="server" Visible="false"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Font-Bold="true" Text="Marketing/recruitment outreach" Value="33">
                <ItemTemplate>
                <table>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx331" Text="Broadcasts (radio/television)" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx332" Text="Print media" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx333" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Targeted recruitment" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther333" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx334" Text="Web-based activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cb335" Text="Electronic/direct calls" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx336" Text="Tours and open houses" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx337" Text="Recruitment fairs/expos" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx338" AutoPostBack="true" OnCheckedChanged="textboxState" Text="General recruitment" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther338" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx339" Text="Community activities" runat="server" />
                </td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbx3310" Text="Feeder/charter/private/other school presentations and visits" runat="server" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx3311" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther3311" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                <telerik:RadPanelItem runat="server" Text="Marketing/recruitment follow-up outreach" Value="34">
                <ItemTemplate>
                <table>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx341" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with inquiries" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther341" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx342" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with recruitment event participants" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther342" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx343" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with applicants" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther343" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx344" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Followed up with admits" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther344" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx345" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Assessed effectiveness of outreach" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther345" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox ID="cbx346" AutoPostBack="true" OnCheckedChanged="textboxState" Text="Other" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtOther346" Visible="false" runat="server"></asp:TextBox>
                </td>
                </tr>
                </table>
                </ItemTemplate>
                </telerik:RadPanelItem>
                </Items>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelBar>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

