<%@ Page Title="MSAP Upcoming Events" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
     %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Upcoming Events
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1>
        Upcoming Events
    </h1>
<p>
These upcoming MSAP-sponsored technical assistance events will help you implement, manage, and sustain your magnet school projects. Put these events on your calendar and check this page regularly for updates and new events. Registration information for all events will be e-mailed to MSAP grant staff. To view archived webinar recordings and tools, visit the <a href="past_webinars.aspx" target="_blank">Past Webinars page</a>.
</p>

<h2>Webinars</h2>
<h3>Annual Performance Reporting</h3>
<p style="border-top:1px solid #A9DEF2"></p>
<strong>Ad Hoc Report Guidance</strong><br />
<b>Webinar:</b> August 24, 2015<br />
<p>
<b>Description: </b>During this session the MSAP Team at the U.S. Department of Education will provide guidance on preparing your ad hoc performance report by discussing the required sections of the ED 524B Form (i.e., the Cover Sheet; Executive Summary; Project Status; Budget; and Additional Information) and the Government Performance and Results Act (GPRA) Table. This guidance will help you during the upcoming reporting period, which will take place from September 21 to October 30, 2015.
</p>
<p style="border-top:1px solid #A9DEF2"> </p>
<strong>MAPS Demonstration</strong><br />
<b>Webinar:</b> September 9, 2015<br />
<p>
<b>Description: </b>The MSAP Center will demonstrate the features of the MSAP Annual Performance System (MAPS). Participants will learn how to log in, find resource and guidance documents, navigate the system, and enter and save annual performance data to create the performance report.
</p>


<p style="border-top:1px solid #A9DEF2"></p>
<h2>Conferences</h2>
<strong>MSAP Project Directors Meeting</strong><br />
November 9-10, 2015<br />
<p>
The 2015 MSAP Project Directors Meeting will take place November 9-10 at the Capital Hilton in Washington, DC. The Meeting will include content-based sessions designed to help you manage and sustain your magnet programs. Additional information about the meeting will become available in the coming months.
</p>
<p style="border-top:1px solid #A9DEF2"> </p>

<%--<p style="border-top:1px solid #A9DEF2"></p>

<h2>Webinars</h2>

<p class="Events_subhead">Annual Performance Reporting</p>
<p class="Events_subhead"><strong>Integrating STEM Into Magnet Schools</strong></p>

<p>This series of three webinars will present ways to fully integrate STEM into magnet schools.</p>
<p style="border-top:1px solid #A9DEF2"></p>
<strong>Topic 1: Characteristics of a Fully Integrated STEM Magnet</strong><br />
<strong>Webinar:</strong> January 21, 2015, 1:00 to 2:00 p.m. Eastern<br />
<strong>Description:</strong> This webinar will help grantees understand what a fully-integrated STEM program looks like and will help them understand each component and how the components work together to create a successful STEM learning environment for students.
<p style="border-top:1px solid #A9DEF2"></p>

<strong>Topic 2: Mapping STEM Into the Curriculum</strong><br />
    <strong>Webinar:</strong> February 11, 2015, 1:00 to 2:00 p.m. Eastern<br />
    <strong>Description:</strong> This webinar will detail the process for writing a curriculum map that includes integrated STEM lessons. It will also provide information on pre-packaged curricula and other resources that are available to help with curriculum development as well as how to successfully integrate those resources in the larger magnet curriculum.

    <p style="border-top:1px solid #A9DEF2"></p>

 
<strong>Topic 3: Creating Effective STEM Partnerships</strong><br />
    <strong>Webinar:</strong> February 18, 2015, 1:00 to 2:00 p.m. Eastern<br />
    <strong>Description:</strong> This webinar will present ways to identify, utilize, and evaluate STEM partnerships. The session will help grantees to develop partnerships that are long-term and sustainable while providing the highest possible benefit to staff and students. The presenter will discuss how to monitor implementation of the partnership and ways that magnet schools can make partnerships mutually beneficial.
<p style="border-top:1px solid #A9DEF2"></p>
<br />
<p class="Events_subhead"><strong>Annual Performance Reporting</strong></p>
<p style="border-top:1px solid #A9DEF2"></p>
<strong>Topic 1: Annual Performance Reporting Guidance</strong><br />
    <strong>Webinar:</strong> March 4, 2015, 1:00 to 2:00 p.m. Eastern<br />
    <strong>Description:</strong> The U.S. Department of Education (ED) will provide guidance on preparing your annual performance report by discussing the required sections of the ED 524B Form (i.e., the Cover Sheet; Executive Summary; Project Status; Budget; and Additional Information) and the Government Performance and Results Act (GPRA) Table.
<p style="border-top:1px solid #A9DEF2"></p>

<strong>Topic 2: MAPS Demonstration</strong><br />
    <strong>Webinar:</strong> March 16, 2015, 1:00 to 2:00 p.m. Eastern<br />
    <strong>Description:</strong> The MSAP Center will demonstrate the basic features of the MSAP Annual Performance System (MAPS). Participants will learn how to log in, find resource and guidance documents, navigate the system, and enter and save annual performance data to create the performance report.
<p style="border-top:1px solid #A9DEF2"></p>

<br />
<p class="Events_subhead"><strong>Turning Around Low-Performing Magnet Schools</strong></p>

<p>This series of two webinars will provide specific evidence-based strategies grantees can use to help them prioritize and manage improvement initiatives by assessing and modifying current strategies, maintaining a focus on improving instruction, celebrating quick wins, and building a committed staff.  
<p style="border-top:1px solid #A9DEF2"></p>
<strong>Topic 1: Engaging Stakeholders for School Turnaround</strong><br />
<strong>Webinar:</strong> April 1, 2015, 1:00 to 2:00 p.m. Eastern<br />
<strong>Description:</strong> In order to successfully turn around a school, the magnet leaders must effectively engage the community; this webinar will help them do just that.
<p style="border-top:1px solid #A9DEF2"></p>

<strong>Topic 2: Improving Student Engagement and Achievement</strong><br />
<strong>Webinar:</strong> April 15, 2015, 1:00 to 2:00 p.m. Eastern<br />
<strong>Description:</strong> Student engagement is key to increasing student achievement; it ensures that students take ownership of their learning and that the school environment fosters student success. This webinar will discuss concrete strategies for increasing student engagement in magnet schools.

<p style="border-top:1px solid #A9DEF2"></p><br />

<p class="Events_subhead"><strong>Student Recruitment</strong></p>
<p style="border-top:1px solid #A9DEF2"></p>

<strong>Topic 1: Overcoming Obstacles to Recruiting Target Students</strong><br />
<strong>Webinar:</strong> April 29, 2015, 1:00 to 2:00 p.m. Eastern<br />
<strong>Description:</strong> This webinar will focus on overcoming common obstacles in recruiting targeted students as well as how to anticipate and overcome them. The webinar content will offer grantees strategies for analyzing all school and district policies that could affect enrollment and how to factor the policies into enrollment projections. 
<p style="border-top:1px solid #A9DEF2"></p><br />
<p class="Events_subhead"><strong>Stakeholder Support</strong></p>
<p style="border-top:1px solid #A9DEF2"></p>

<strong>Topic 1: Generating Magnet School Buy-In</strong><br />
<strong>Webinar:</strong> May 6, 2015, 1:00 to 2:00 p.m. Eastern<br />
<strong>Description:</strong> This webinar will present ways to generate buy-in from project stakeholders, especially from district and school staff, to gain their support for the change that is occurring by implementing the MSAP grant. 
<p style="border-top:1px solid #A9DEF2"></p>--%>



For information on all events, contact<br/>
Elizabeth (Beth) Ford<br/>
MSAP Center<br/>
(866) 997-6727<br/>
<a href="mailto:msapcenter@leedmci.com">msapcenter@leedmci.com</a>

</asp:Content>