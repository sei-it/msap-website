﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="past2014.aspx.cs" Inherits="admin_past2014" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">Past MSAP Conferences 2014
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="border:none;">Past Conferences</h1>
 	<div class="titles">2014 MSAP Project Directors Meeting, October 14-15, 2014</div>
    <div class="tab_area" style="width:840px;">
    		<ul class="tabs">
                <div id="Div1" runat="server" style="width: 600px; float: right;">
                <li><a href="past.aspx">2010</a></li> 
                <li><a href="past2011.aspx">2011</a></li>
                <li><a href="past2012.aspx">2012</a></li>
                <li><a href="past2013.aspx">2013</a></li>
                <li class="tab_active">2014</li>
                
                </div>
        	</ul>
    	</div>
    	<br/>
      
	<div style="float: left; margin-top: 0; width: 20%;;padding-bottom:15px;">
        <p>
        <strong>Meeting Agenda</strong>
        <br />
        <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/MSAP_PD_MeetingAgenda2014.pdf" target="_blank">Download
                    PDF</a> <small>(183 KB)</small>
        </p>
        <p>
        <strong>Meeting Session Descriptions</strong>
        <br />
        <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
            vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/SessionDescriptions.pdf" target="_blank">Download
                PDF</a> <small>(219 KB)</small>
        </p>
        <p>
        <strong>Speaker Biographies</strong>
        <br />
        <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
            vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/SpeakerBiographies.pdf" target="_blank">Download
                PDF</a> <small>(229 KB)</small>
        </p>
        <p>
        <strong>Meeting Participant List</strong>
        <br />
        <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
            vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/ParticipantList.pdf" target="_blank">Download
                PDF</a> <small>(255 KB)</small>
        </p>
    </div>
	<div style="float: left; margin-top: 0; width: 75%;;padding-bottom:15px;border-left:1px solid #A9DEF0;padding-left:20px;">
     <h3 class="pastconf_subsection" style="padding-top:0px;">Day 1: October 14, 2014</h3>

    <p>   
        <strong class="pastconf_subtitle">Keynote: Making Magnets Work in Extreme Poverty</strong><br />
            
            <span class="pastconf_author"><i>Dennis Dupree, Sr.</i><br />
            Clarksdale Municipal School District </span>
       
     </p>
    <p>
        This presentation discusses Clarksdale Municipal School District’s path to magnet schools. Clarksdale looked to magnet schools to breathe new life into the district and excite students, families, and the community. Becoming a magnet school district has afforded staff the extensive professional development needed to spark a paradigm shift.
	</p>
	<div id="Div3" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Keynote: Magnets in Extreme Poverty Presentation
                </td>
                <td width='20%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/DupreeKeynote.pdf" target="_blank">Download
                    PDF</a> <small>(1.6 MB)</small>
                </td>
            </tr>
        </table>
    </div>



     <p>
        <strong class="pastconf_subtitle">Integrating STEM Initiatives Into Practice</strong><br />
            <span class="pastconf_author"><i>Russell Shilling, Ph.D.; Kumar Garg; Jo Anne Vasquez, Ph.D.; Larry Plank</i>
            </span>
       
     </p>
    <p>
      In this facilitated panel session, the panelists discuss the importance of STEM initiatives from the perspectives of federal and local governments, researchers, and practitioners.</p>
	<div id="Div2" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
           <tr  bgcolor='#E8E8E8'  style="height:28px">
                <td class="style1">
                    No presentation materials </td>
                <td  style="padding-left:33px; " >
                  None available
                </td>
            </tr>
        </table>
    </div>



    <p>
        <strong class="pastconf_subtitle">A Review of the National Effort to Turn Around Schools</strong><br />
            <span class="pastconf_author"><i>Carlas McCauley, Ed.D.</i><br />
            Center on School Turnaround</span>
       
     </p>
    <p>
       This session highlights efforts across the country to improve the nation's schools. The session features a discussion around the important role that all stakeholders play to improve schools and will highlight the endeavors that states, schools, and districts are undertaking.
	</p>
	<div id="Div4" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    The National Effort to Turn Around Schools Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/McCauley_SchoolTurnAround.pdf" target="_blank">Download
                    PDF</a> <small>(1.5 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">High-Quality Professional Development: Who, Why, What, and How!</strong><br />
            <span class="pastconf_author"><i>Patty Born-Selly</i><br />
           </span> <br />   
        National Center for STEM Elementary Education
     </p>
       
    <p>
        The session focuses on professional development (PD) for STEM and includes a “sampler” of different strategies magnet school administrators can apply to support the instructional staff. The presenter addresses the following topics: How to evaluate PD opportunities; best practices for PD; how to scale up your PD; and how to support teachers who are the “STEM Champions.”
	</p>
	<div id="Div11" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    High-Quality Professional Development Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/BornSellyProfessionalDevelopment.pdf" target="_blank">Download
                    PDF</a> <small>(366 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">STEM Lesson Essentials: Moving Beyond the Acronym to Implementation</strong><br />
            <span class="pastconf_author"><i>Jo Anne Vasquez, Ph.D.</i><br />
            Helios Education Foundation</span>
       
     </p>
    <p>
        This workshop explores the core elements of STEM teaching and learning. The workshop also discusses the basics of effective STEM instruction, including the guiding principles for effective STEM teaching, the levels of STEM integration, and moving from a disciplinary approach to a transdisciplinary approach.</p>
	<div id="Div12" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    STEM Lesson Essentials Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/VasquezSTEMLessonEssentials.pdf" target="_blank">Download
                    PDF</a> <small>(3.6 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Myth Versus Reality: Our School’s STEM Playbook</strong><br />
            <span class="pastconf_author"><i>Aimee Kennedy</i><br />
            Battelle</span>
       
     </p>
    <p>
        This session, the presenter discusses her experience as the principal of a top-rated, open enrollment STEM school. The session talks through common pitfalls in STEM and strategies to help all students, especially those who start behind, unlock their potential.
	</p>
	<div id="Div13" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Myth Versus Reality Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/Kennedy_MythvRealty.pdf" target="_blank">Download
                    PDF</a> <small>(2.2 MB)</small>
                </td>
            </tr>
        </table>
    </div>
     <p>
        <strong class="pastconf_subtitle">Change Management and MSAP: What We Have Learned </strong><br />
            <span class="pastconf_author"><i>David Gregory</i><br />
            G&D Associates</span>
       
     </p>
    <p>
        The session discusses common project management challenges, possible solutions, and recommendations for future change management work based on findings from a set of change management site visits.
	</p>
	<div id="Div23" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Change Management and MSAP Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/Gregory_ChangeManagementandMSAP.pdf" target="_blank">Download
                    PDF</a> <small>(385 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Coding in the Classroom</strong><br />
            <span class="pastconf_author"><i>Tammie Schrader</i><br />
            Northeast Washington Education Service District</span>
       
     </p>
    <p>
        This session explains the available computer science curriculum resources and how to use them to create effective, engaging computer science lessons. The presentation also discusses game-based learning and how educational games are changing the landscape of education.
	</p>
	<div id="Div14" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Coding in the Classroom Presentation</td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/Schrader_CodingintheClassroom.pdf" target="_blank">Download
                    PDF</a> <small>(537 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <h3 class="pastconf_subsection" style="padding-top:0px;">Day 2: October 15, 2014</h3>

    <p>
        <strong class="pastconf_subtitle">Strategies for Contemporary Desegregation Challenges</strong><br />
            <span class="pastconf_author"><i>Robert Kim; John Brittain; Willis Hawley, Ph.D.; Genevieve Siegel-Hawley, Ph.D.</i>
          </span>       
     </p>
    <p>
        In this facilitated panel discussion, the panelists discuss contemporary desegregation strategies that can be applied in required and voluntary contexts, and will share viewpoints from law and education research. 
	</p>
	<div id="Div25" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'  style="height:28px">
                <td class="style1">
                    No presentation materials </td>
                <td  style="padding-left:33px; " >
                  None available
                </td>
            </tr>
        </table>
    </div>





    <p>
        <strong class="pastconf_subtitle">MSAP Compliance Monitoring – Three Years Later</strong><br />
            <span class="pastconf_author"><i>Sara Allender; Emanda Thomas, Ph.D.</i><br />
            WestEd </span>
       
     </p>
    <p>
        In this session, compliance monitoring staff review findings from the Fiscal Year 2010 grantee compliance monitoring, highlighting trends in grantee performance, areas for technical assistance or improvement, and changes for the coming monitoring cycle.
	</p>
	<div id="Div17" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    MSAP Compliance Monitoring Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/ComplianceMonitoringGeneralSession.pdf" target="_blank">Download
                    PDF</a> <small>(359 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Buses, Boundaries, and Balancing Acts </strong><br />
            <span class="pastconf_author"><i>Maree Sneed,</i> Hogal Lovells
           </span>       
     </p>
     <p><i>Carolyn Bridges,</i> Polk County Public Schools</p>
    <p>
        This presentation discusses the process used by the School Board of Polk County, Florida, to implement and evaluate its student admissions lottery system. It includes a comprehensive discussion of the mechanics of the new admissions lottery plan, challenges presented by lottery admissions, solutions applied to address those challenges, and strategies used to generate stakeholder buy-in for the new plan.
	</p>
	<div id="Div18" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Buses, Boundaries, and Balancing Acts Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/SneedBridges_BusesBoundaries.pdf" target="_blank">Download
                    PDF</a> <small>(1.8 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Ensuring Effective STEM Instruction for ELLs </strong><br />
            <span class="pastconf_author"><i>Diane August, Ph.D.</i><br />
            American Institutes for Research</span>
       
     </p>
    <p>
        This presentation briefly reviews the research related to effective science and literacy instruction, and then presents examples of effective instructional practices in these areas.
	</p>
	<div id="Div19" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Effective STEM Instruction for ELLs Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/August_STEMInstructionforELLs.pdf" target="_blank">Download
                    PDF</a> <small>(1.3 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Retaining a Diverse Magnet Student Enrollment </strong><br />
            <span class="pastconf_author"><i>Robert Brooks, Ph.D.,</i> Consultant<br /><i>Doreen Marvin,</i> LEARN<br /><i>Kim Morrison,</i> Mount Airy City Schools<br /><i>Gladys Pack,</i> Consultant 
</span>
       
     </p>
    <p>
        This session provides a retention framework and practical strategies that ensure magnet students are successfully retained through high school graduation. The presenters offer student retention and family commitment approaches, strategies, and best practices that work at each school level, even when a magnet program is in the early phases of implementation.
	</p>
	<div id="Div20" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Retaining a Diverse Enrollment Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/Brooks_et_al_RetainingStudents.pdf" target="_blank">Download
                    PDF</a> <small>(802 KB)</small>
                </td>
            </tr>
        </table>
    </div>


     <p>
        <strong class="pastconf_subtitle">LGBTQ Bullying: Understanding the Importance of School Culture </strong><br />
            <span class="pastconf_author"><i>Elizabethe Payne, Ph.D.</i> 
</span>
       
     </p>
    <p>
        This presentation explores bullying from a sociological perspective to better understand how bullying “works.” The session helps educators identify ways that cultural privilege and marginalization are built into school culture, and consider how to address those inherent biases so that all students feel supported and accepted in the school.</p>
	<div id="Div26" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'  style="height:28px">
                <td class="style1">
                    No presentation materials </td>
                <td  style="padding-left:33px; " >
                  None available
                </td>
            </tr>
        </table>
    </div>









    <p>
        <strong class="pastconf_subtitle">Perspectives for a Diverse America: a K-12 Literacy-based Anti-bias Curriculum</strong><br />
            <span class="pastconf_author"><i>Emily Chiariello</i><br />
            Teaching Tolerance at the Southern Poverty Law Center</span>
       
     </p>
    <p>
        This session introduces the pedagogy and core instructional components of the <i>Perspectives</i> curriculum—the Anti-bias Framework, Central Text Anthology, and Integrated Learning Plan. The presenter reviews the results of her pilot study and shares feedback from classrooms around the country that are reporting both social-emotional and academic gains after using the curriculum.
	</p>
	<div id="Div21" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Perspectives for a Diverse America Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/Chiariello_PerspectivesforaDiverseAmerica.pdf" target="_blank">Download
                    PDF</a> <small>(3.8 MB)</small>
                </td>
            </tr>
            <tr>
                <td>
                    20 Face to Face Advisories </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/PDAAdvisoryActivitiesVF.pdf" target="_blank">Download
                    PDF</a> <small>(344 KB)</small>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Perspectives for a Diverse America Pamphlet </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/Perspectivespamphlet_final_web_single.pdf" target="_blank">Download
                    PDF</a> <small>(819 KB)</small>
                </td>
            </tr>
            <tr>
                <td>
                    Perspectives for a Diverse America Brochure </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/TTperspectivesbrochure_v4.pdf" target="_blank">Download
                    PDF</a> <small>(2.2 MB)</small>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Critical Practices for Anti-bias Education </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/CriticalPracticesv4_final.pdf" target="_blank">Download
                    PDF</a> <small>(573 KB)</small>
                </td>
            </tr>
            <tr>
                <td>
                    Perspectives for a Diverse America User Guide </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/PerspectivesforaDiverseAmericaUserExperiencePamphlet_UpdatesFINAL ONLINE.pdf" target="_blank">Download
                    PDF</a> <small>(702 KB)</small>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Introducing the Teaching Tolerance Anti-bias Framework </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/TTantibiasframeworkpamphlet_final.pdf" target="_blank">Download
                    PDF</a> <small>(324 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle">Updates to Compliance Monitoring for FY2013 Grantees</strong><br />
            <span class="pastconf_author"><i>Sara Allender; Emanda Thomas, Ph.D.; Erin Carter</i><br />
            WestEd</span>
       
     </p>
    <p>
        In this presentation, compliance monitoring staff preview many of the changes being put in place to monitor Fiscal Year 2013 grantees. Changes include revisions to the monitoring indicators as well as a new online platform to facilitate monitoring activities. 
	</p>
	<div id="Div22" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Updates to Compliance Monitoring Presentation </td>
                <td>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/Past_Events/2014_Events/ComplianceMonitoring_BreakoutSession.pdf" target="_blank">Download
                    PDF</a> <small>(820 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p>
        <strong class="pastconf_subtitle"><i>Doing it For Me</i>: Engaging Students Through Arts Education</strong></span>


       
     </p>
     <p><i>Leah Edwards, Maureen Dwyer, Terry Liu</i></p>
   
    <p>
       Following a screening of the documentary film Doing it For Me, panelists discuss the power of education, arts, and storytelling in empowering youth to achieve their goals. </p>
	<div id="Div27" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'  style="height:28px">
                <td class="style1">
                    No presentation materials </td>
                <td  style="padding-left:33px; " >
                  None available
                </td>
            </tr>
        </table>
    </div>




   </div>
<!--<div style="text-align:right;clear:both;width:820px;padding-right:20px;" class="space_top">
	2011 Meeting&nbsp;&nbsp;
	<a href="past.aspx"><strong>2010 Meeting</strong></a>
</div>
-->
</asp:Content>


