﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using ExcelLibrary.SpreadSheet;

public partial class admin_managelist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }
    protected void OnUpload(object sender, EventArgs e)
    {
        if (FileUplaod1.HasFile)
        {
            Workbook workbook = Workbook.Load(FileUplaod1.FileContent);
            Worksheet sheet = workbook.Worksheets[0];
            for (int rowIndex = sheet.Cells.FirstRowIndex;rowIndex <= sheet.Cells.LastRowIndex; rowIndex++) 
            {
                Row row = sheet.Cells.GetRow(rowIndex);
                Cell cell = row.GetCell(1);
                MagnetMailList Email = new MagnetMailList();
                Email.Email = cell.StringValue;
                Email.UnSubscribed = false;
                Email.TimeStamp = DateTime.Now;
                Email.Save();
            }
        }
    }
    protected void OnDownload(object sender, EventArgs e)
    {
        using (System.IO.MemoryStream output = new System.IO.MemoryStream())
        {
            Workbook workbook = new Workbook();
            Worksheet worksheet = new Worksheet("Email");
            int idx = 0;
            foreach (MagnetMailList Email in MagnetMailList.All())
            {
                worksheet.Cells[idx++, 0] = new Cell(Email.Email);
            }
            workbook.Worksheets.Add(worksheet);
            workbook.Save(output);
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=emaillist.xls");
            Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            output.Close();
        }
    }
}