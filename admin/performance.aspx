﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="performance.aspx.cs" Inherits="admin_performance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Performance Reporting
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Annual Performance Reporting Overview
    </h1>
    <p>The MSAP performance measures are used by MSAP and its grantees to quantify and report progress toward meeting program- and project-level objectives, as required by the Elementary and Secondary Education Act of 1965, as amended, Title V, Part C. Annual performance reporting (APR) is required by the MSAP regulations and will help the U.S. Department of Education (ED) report to Congress, improve the program, and provide technical assistance. </p>
<p>There are two types of MSAP performance measures: the Government Performance and Results Act (GPRA) and project-level measures. </p>
<p><strong>GPRA Performance Measures</strong><br />
GPRA supports ED’s efforts to focus on program results, quality of service, and customer satisfaction by requiring strategic planning and performance measurement. It also mandates public reporting of progress towards achieving program-level goals. The current MSAP GPRA measures are </p>
<ul style="list-style-type: none;">
	<li><p><strong>GPRA Measure 1:</strong> The percentage of magnet schools receiving assistance whose student enrollment reduces, eliminates, or prevents minority group isolation.</p></li>
<li><p><strong>GPRA Measure 2:</strong> The percentage of students from major racial and ethnic groups in magnet schools receiving assistance who score proficient or above on State assessments in reading/language arts.</p></li>
<li><p><strong>GPRA Measure 3:</strong> The percentage of students from major racial and ethnic groups, in magnet schools receiving assistance who score proficient or above on State assessments in mathematics.</p></li>
<li><p><strong>GPRA Measure 4:</strong> The cost per student in a magnet school receiving assistance.</p></li>
<li><p><strong>GPRA Measure 5:</strong> The percentage of magnet schools that received assistance that are still operating magnet school programs three years after Federal funding ends.</p></li>
<li><p><strong>GPRA Measure 6:</strong> The percentage of magnet schools that received assistance that meet the State’s annual measurable objectives and, for high schools, graduation rate targets at least three years after Federal funding ends.</p></li>
</ul>
<p><strong>Project-Level Performance Measures</strong><br />

Grantees must also report on project-level performance measures, which are established in the approved MSAP application and unique to each project. </p>

</asp:Content>
