﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class admin_password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void OnChangePassword(object sender, EventArgs e)
    {
        MembershipUser user = Membership.GetUser(Context.User.Identity.Name);
        user.ChangePassword(txtOldPassword.Text, txtNewPassword.Text);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('Password change successfully!');</script>", false);
    }
}