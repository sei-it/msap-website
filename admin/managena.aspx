﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="managena.aspx.cs" Inherits="admin_managena" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage NA File
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Manage NA File</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New NA File" CssClass="msapBtn" OnClick="OnAdd" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
            <asp:BoundField DataField="DocumentName" SortExpression="" HeaderText="Document" />
            <asp:BoundField DataField="DocumentType" SortExpression="" HeaderText="First Document Type" />
            <asp:BoundField DataField="UploadDate" SortExpression="" HeaderText="Upload Date" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hfID" runat="server" />
    <%-- News --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Webinar</div>
            <table>
                <tr>
                    <td>
                        Document Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDocumentName" CssClass="msapTxt" runat="server" MaxLength="500" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        File:
                    </td>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="msapTxt" />
                    </td>
                </tr>
                <tr>
                    <td>
                        File Type:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlFirstFileType" runat="server">
                            <asp:ListItem>Video</asp:ListItem>
                            <asp:ListItem>PDF</asp:ListItem>
                            <asp:ListItem>DOC</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                    </td>
                    <td>
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeNewsWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>

