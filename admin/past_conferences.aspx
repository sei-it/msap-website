<%@ Page Title="MSAP Past Conferences" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">MSAP Past Conferences
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>Past Conferences</h1>
    <h2>2011 MSAP Project Directors Meeting, December 8-9, 2011, Washington, DC</h2>
 <!--   <div id="Div1" runat="server">
    </div>-->
    

	<div style="float: left; margin-top: 0; width: 20%;;padding-bottom:15px;">
            <strong>Agenda</strong>
            <br />
            <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                    vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Agenda_2011.pdf" target="_blank">Download
                        PDF</a> <small>(232 KB)</small>
    </div>
    <div style="float: left; margin-top: 0; width: 48%; margin-left: 30px;padding-bottom:15px;">
        	<strong>Participant List</strong>
			<br />
            <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/ParticipantList.pdf" target="_blank">Download
                    PDF</a> <small>(284 KB)</small>
    </div>
    <p class="first_element">
        <strong>
            Grants Management and Annual Performance Reporting<br />
            December 8, 2011. U.S. Department of Education<br />
        </strong>
        Anna Hinton, Ph.D.; Rosie Kelley; Brittany Beth; Tyrone Harris<br />
        ED staff responded to common budget questions, such as which costs are and are not allowable. They then discussed the 	
        Annual Performance Report, referring to areas that may cause confusion and clearly explaining how to complete the table		
         for the Government Performance and Results Act (GPRA) measures. 
	</p>
	<div id="Div3" runat="server" class="indent">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='80%'>
                    <b>File name</b>
                </td>
                <td width='20%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                    Grants Management Presentation
                </td>
                <td width='20%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/ED_GrantsManagement.pdf" target="_blank">Download
                    PDF</a> <small>(1.5 MB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p class="space_top">
        <strong>
            MSAP Compliance Monitoring<br />
            December 8, 2011. WestEd<br />
        </strong>
            Sara Allender, Seewan Eng<br />
            This two-part session had three goals: (1) to introduce the purpose and process of project monitoring, (2) to 
            demystify and clarify misconceptions about monitoring, and (3) to help project directors prepare for site visits.
       
        
        Anna Hinton, Ph.D.; Rosie Kelley; Brittany Beth; Tyrone Harris<br />
        ED staff responded to common budget questions, such as which costs are and are not allowable. They then discussed the 	
        Annual Performance Report, referring to areas that may cause confusion and clearly explaining how to complete the table		
         for the Government Performance and Results Act (GPRA) measures. 
	</p>
	<div id="Div4" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='80%'>
                    <b>File name</b>
                </td>
                <td width='20%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                    Compliance Monitoring Presentation.
                </td>
                <td width='20%'>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/ComplianceMonitoring.pdf" target="_blank">Download
                    PDF</a> <small>(2.8 MB)</small>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    Compliance Monitoring Indicators.
                </td>
                <td width='10%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/MonitoringIndicators.pdf" target="_blank">Download
                    PDF</a> <small>(492 KB)</small>
                </td>
            </tr>
        </table>
    </div>
   	<p class="space_top">
    	<strong>Building Civic Capacity: New Imperatives, New Policies<br /> 
        December 9, 2011. Vanderbilt University<br /></strong>
        Claire Smrekar, Ph.D.<br />
        This session discusses the income gap and its relation to the achievement gap, then explains how magnet schools can 
        play a role in closing the achievement gap. The presentation also covers the importance of entire neighborhood 
        mobilization to improving education equality.
    </p>
    <div id="Div5" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='80%'>
                    <b>File name</b>
                </td>
                <td width='20%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                    Building Civic Capacity: New Imperatives, New Policies.
                </td>
                <td width='20%'>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Smrekar_CivicCapacity.pdf" target="_blank">Download
                    PDF</a> <small>(578 KB)</small>
                </td>
            </tr>
            <tr bgcolor='#E8E8E8'>
                <td>
                    Rethinking Magnet School Policies and Practices: A Response to Declining Diversity &amp; Judicial Constraints.
                </td>
                <td width='10%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px;margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/SmrekarMagnetSchools.pdf" target="_blank">Download
                    PDF</a> <small>(85.45 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <p class="space_top">
    	<strong>
        	Housing and School Integration: Implications for Magnet Schools<br />
        	December 9, 2011.<br />
        </strong>
        Luke Tate, U.S. Department of Housing and Urban Development; Damon T. Hewitt, NAACP Legal Defense and Educational Fund, Inc.; Roslyn A. Mickelson, Ph.D., University of North Carolina at Charlotte; Denise Gallucci, Capitol Region Education 
        Council; Phil Tegeler, Poverty and Race Research Action Council<br />
        This session had three parts:
    </p>
    <div id="Div6" runat="server" class="indent">
    <div class="indent">
        <strong>Damon Hewitt, Fulfilling the Promise of Brown: Promoting School Integration.</strong><br/>
        This presentation introduced and explained the legal history around desegregation as it applies to magnet schools.
    
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='75%'>
                    <b>File name</b>
                </td>
                <td width='25%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                   Promoting School Integration Presentation
                </td>
                <td width='20%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Hewitt_FulfillingthePromise.pdf" target="_blank">Download
                    PDF</a> <small>(381 KB)</small>
                </td>
            </tr>
        </table>
   </div>
   <div class="indent">
      <strong>Roslyn Mickelson, A Synthesis of Social Science Research About the School-Housing Integration 
      Nexus: Implications for Magnet Schools.</strong><br/> This presentation reported on education research on racial
      and socioeconomic diversity, the reciprocal connection between diverse neighborhoods and integrated 
      schools, and parent involvement in community schools.
    
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='75%'>
                    <b>File name</b>
                </td>
                <td width='25%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                   School-Housing Integration Presentation 
                </td>
                <td width='10%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Mickelson_SchoolHousingNexus.pdf" target="_blank">Download PDF</a> <small>(1.1 MB)</small>
                </td>
            </tr>
        </table>
     </div>
     <div class="indent">
      <strong>Denise Gallucci, The Learning Corridor Project: Economic Development and Urban Renewal.</strong><br/> Starting 
      with an initiative that established interdistrict magnet schools in Hartford, Connecticut, community, higher education, 
      and government organizations have revitalized a deprived area and opened the door to higher education for many students.
         
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='75%'>
                    <b>File name</b>
                </td>
                <td width='25%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                   Economic Development and Urban Renewal Presentation 
                </td>
                <td width='20%'>
                     <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/Gallucci_LearningCorridor.pdf" target="_blank">Download
                    PDF</a> <small>(647 KB)</small>
                </td>
            </tr>
        </table>
        </div>
    </div>
    <p class="space_top" style="margin-top:0px;">
        <strong>
        	Vibrant and Diverse Magnet Schools: Strategies for Financing and Family Outreach<br />
            December 9, 2011. The Finance Project<br />
        </strong> 
        Laura Martinez, Rachel Scott<br />
        In this session, participants learned about eight key elements of sustainability and discussed strategies, such as 
        needs assessments and identity building, for implementing them.
	</p>
	<div id="Div7" runat="server">
        <table width='100%' border='0' cellspacing='0' cellpadding='3' class="indent">
            <tr class="teal_txt">
                <td width='80%'>
                    <b>File name</b>
                </td>
                <td width='20%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr>
                <td>
                   Strategies for Financing and Family Outreach Presentation
                </td>
                <td width='20%'>
                   <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="../pdf/FinanceProject.pdf" target="_blank">Download
                    PDF</a> <small>(426 KB)</small>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align:right;padding-top:40px;" class="space_top"><a href="past.aspx"><strong>2010 MSAP Project Directors Meeting >></strong></a></div>

</asp:Content>
