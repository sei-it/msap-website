﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using Synergy.Magnet;

public partial class admin_importdata : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void OnUpload(object sender, EventArgs e)
    {
        string DataFile = Server.MapPath("NEWschoolIDs.xls");
        string excelConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + DataFile + @";Extended Properties=""Excel 8.0;HDR=YES;""";

        // Create Connection to Excel Workbook
        using (OleDbConnection connection = new OleDbConnection(excelConnectionString))
        {
            OleDbCommand command = new OleDbCommand
                    ("Select * FROM [Sheet1$]", connection);

            connection.Open();

            // Create DbDataReader to Data Worksheet
            using (DbDataReader dr = command.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (!dr.IsDBNull(0))
                    {
                        MagnetSchool school = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(dr.GetValue(0)));
                        school.SchoolCode = dr.GetString(4);
                        school.Save();
                        /*MagnetGranteeContact contact = MagnetGranteeContact.SingleOrDefault(x => x.ID == Convert.ToInt32(dr.GetValue(0)));
                        if (!dr.IsDBNull(1)) contact.ContactTitle = dr.GetString(1);
                        if (!dr.IsDBNull(2)) contact.ContactFirstName = dr.GetString(2);
                        if (!dr.IsDBNull(3)) contact.ContactLastName = dr.GetString(3);
                        if (!dr.IsDBNull(4)) contact.Address = dr.GetString(4);
                        if (!dr.IsDBNull(5)) contact.City = dr.GetString(5);
                        if (!dr.IsDBNull(6)) contact.State = dr.GetString(6);
                        if (!dr.IsDBNull(7)) contact.Zipcode = Convert.ToString(dr.GetValue(7));
                        if (!dr.IsDBNull(8)) contact.Phone = Convert.ToString(dr.GetValue(8));
                        if (!dr.IsDBNull(9)) contact.Ext= Convert.ToString(dr.GetValue(9));
                        if (!dr.IsDBNull(10)) contact.Fax = Convert.ToString(dr.GetValue(10));
                        if (!dr.IsDBNull(11)) contact.Email = dr.GetString(11);
                        contact.Save();*/
                    }
                }
            }
        }
    }
}