﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managegroupmember.aspx.cs" Inherits="managegroupmember" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage SEI Support Group Members
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Manage SEI Support Group Members</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New Member" CssClass="msapBtn" OnClick="OnAdd" />
        </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            OnPageIndexChanging="OnPageIndexChanging">
            <Columns>
                <asp:BoundField DataField="MemberName" SortExpression="" HeaderText="Member Name" />
                <asp:BoundField DataField="Title" SortExpression="" HeaderText="Title" />
                <asp:BoundField DataField="Email" SortExpression="" HeaderText="Email" />
                <asp:BoundField DataField="Priority" SortExpression="" HeaderText="Display Order" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hfID" runat="server" />
        <%-- Event --%>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Grantee Users</div>
                <table>
                    <tr>
                        <td>
                            Member Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserName" Width="800" CssClass="msapTxt" runat="server" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTitle" Width="800" CssClass="msapTxt" runat="server" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" Width="800" CssClass="msapTxt" runat="server" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Display Sequence:
                        </td>
                        <td>
                            <asp:TextBox ID="txtSequence"  CssClass="msapTxt" runat="server" MaxLength="250"></asp:TextBox>
                            <ajax:NumericUpDownExtender Width="50" Minimum="1" ID="NumericUpDownExtender1" runat="server" TargetControlID="txtSequence">
                            </ajax:NumericUpDownExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                        </td>
                        <td>
                            <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
            BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
