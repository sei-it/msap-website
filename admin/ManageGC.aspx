﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="ManageGC.aspx.cs" Inherits="admin_ManageGC" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
Manage Grantee Corner
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 490px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true"
        RepeatDirection="Horizontal" 
        onselectedindexchanged="rbtnlstCohort_SelectedIndexChanged">
    <asp:ListItem Value="2010" >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2013" Selected="True">MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
 <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New Grantee Corner" CssClass="msapBtn" OnClick="OnAddGC" />
    
    </p>
    <asp:GridView ID="gdvGC" runat="server" AllowPaging="false" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
            <asp:BoundField DataField="Title" SortExpression="" HeaderText="Title" />
            <asp:BoundField DataField="Createdby" SortExpression="" HeaderText="Created by" />
            <asp:BoundField DataField="CreatedOn" SortExpression="" HeaderText="Created On" />
            <asp:BoundField DataField="isActive" SortExpression="" HeaderText="Active" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditGC"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDeleteGC" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Panel ID="pnlGCImages" runat="server">
   <br /><br />
    <hr />

       <p style="text-align:left">
        <asp:Button ID="btnNewImage" runat="server" Text="New GC Image" 
               CausesValidation="false" CssClass="msapBtn" onclick="btnNewImage_Click"  />
           <asp:DropDownList ID="ddlGranteeCorner" runat="server" AutoPostBack="True" 
               DataSourceID="dsGCContent" DataTextField="Title" DataValueField="id">
           </asp:DropDownList>
    </p>
    <asp:GridView ID="gvImage" runat="server" AllowPaging="false" AllowSorting="false" 
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        >
        <Columns>
             <asp:TemplateField>
             <HeaderTemplate>Image</HeaderTemplate>
                <ItemTemplate>
                <asp:Image ID="imageid" runat="server" Width="100px" ImageUrl='<%#"../img/Handler.ashx?GCID=" + Eval("id")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="caption" SortExpression="" HeaderText="Document Name" />
            <asp:BoundField DataField="CreatedBy" SortExpression="" HeaderText="Created By" />
            <asp:BoundField DataField="CreateOn" SortExpression="" HeaderText="Created Date" />
             <asp:BoundField DataField="isShowOnHomePage" SortExpression="" HeaderText="Display on Home Page" />
            <asp:BoundField DataField="isArchiveImage" SortExpression="" HeaderText="Archive Image" />
            <asp:BoundField DataField="displayOrder" SortExpression="" HeaderText="Image Order" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnEditImage"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                        OnClick="OnDeleteImage" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlImageInpt" runat="server">
    <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult">
                Add/Edit Grantee Corner Image
                <span class="closeit"><asp:LinkButton ID="lbtnClose" runat="server" Text="Close" CssClass="closeit_link" />
                </span>
            </div>
            <div style="height:280px;overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                     <tr>
                    <td class="consult_popupTxt">Caption:</td>
                        <td>
                           
                            <asp:TextBox ID="txtCaption" runat="server" MaxLength="250" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                         <tr>
                    <td class="consult_popupTxt">Alt Text:</td>
                        <td>
                           
                            <asp:TextBox ID="txtAltText" runat="server" MaxLength="250" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Image:</td>
                        <td class="style1"> 
                            <asp:FileUpload ID="fuploadImg" runat="server" CssClass="msapDataTxt" 
                                Height="19px" Width="100%" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="fuploadImg" ErrorMessage="an upload image file is required."></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="fuploadImg" 
ErrorMessage="Invalid Image File" ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.gif|.GIF|.png|.PNG)$"> 
</asp:RegularExpressionValidator> 
                        </td>
                    </tr>
                    <tr>
                      <td class="consult_popupTxt">Display on Home Page:</td>
                        <td class="style1">
                        	
                            <asp:RadioButtonList ID="rbtnlDisplayonHomepage" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem >No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                      <td class="consult_popupTxt">Archive Image:</td>
                        <td class="style1">
                        	
                            <asp:RadioButtonList ID="rbtnlArchImg" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                     <%-- <tr>
                      <td class="consult_popupTxt">Active:</td>
                        <td class="style1">
                        	
                            <asp:RadioButtonList ID="rbtnlActive" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>--%>
                <tr>
                 <td class="consult_popupTxt">Image Display Order:</td>
                 <td><telerik:RadNumericTextBox ID="txtOrder" runat="server" >
                 <NumberFormat DecimalDigits="0" GroupSeparator="" />
                 </telerik:RadNumericTextBox>
                 </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnImgSave" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveImg" />
                    </td>
                    <td class="style1">
                        <asp:Button ID="btnImgClose" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>

                </table>
            </div>
        </div>
    
    </asp:Panel>
    <asp:LinkButton ID="LinkButton8" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpextImage" runat="server" TargetControlID="LinkButton8"
        PopupControlID="pnlImageInpt" DropShadow="true" OkControlID="btnImgClose" CancelControlID="btnImgClose"
        BackgroundCssClass="magnetMPE" Y="20" />

       <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDivConsult">
            <div class="mpeDivHeaderConsult">
                Add/Edit Grantee Corner
                <span class="closeit"><asp:LinkButton ID="Button1" runat="server" Text="Close" CssClass="closeit_link" />
                </span>
            </div>
            <div style="height:480px;overflow: auto;">
                <table style="padding:0px 10px 10px 10px;">
                  
                    <tr>
                    <td class="consult_popupTxt">Title:</td>
                        <td>
                           
                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="250" Width="470"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                     <td class="consult_popupTxt">Abstract Content:</td>
                        <td>
                            <cc1:Editor ID="edtAbsContent" runat="server" Width="470" Height="100"   JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000">
                            </cc1:Editor>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">Content:</td>
                        <td>
                            <cc1:Editor ID="edtContents" runat="server" Width="470" Height="200" 
                                JavaScriptLocation="ExternalFile" ToolbarImagesLocation="ExternalFile" ButtonImagesLocation="ExternalFile"
                                GutterBackColor="GrayText" ToolbarBackColor="GrayText" BackColor="GrayText" ToolbarStyleConfiguration="Office2000">
                            </cc1:Editor>
                        </td>
                    </tr>
                    <tr>
                    <td class="consult_popupTxt">&nbsp;</td>
                        <td> 
                            &nbsp;</td>
                    </tr>
                      <tr>
                      <td class="consult_popupTxt">Active:</td>
                        <td>
                        	
                            <asp:RadioButtonList ID="rblActive" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                <tr>
                    <td colspan='2'>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveGC" />
                    </td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>

                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeContentWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="btnClose" CancelControlID="btnClose"
        BackgroundCssClass="magnetMPE" Y="20" />
     <asp:HiddenField ID="hfImgID" runat="server" />
     <asp:HiddenField ID="hfContentID" runat="server" />
     <asp:HiddenField ID="hfGCID" runat="server" />
     <%--<asp:LinqDataSource ID="ldsGCContent" runat="server" 
        ContextTypeName="GCDataClassesDataContext" EntityTypeName="" 
         OrderBy="Title"
        TableName="MagnetGCs">
    </asp:LinqDataSource>--%>
    <asp:SqlDataSource ID="dsGCContent" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT id, Title, Contents, abs_contents, CreatedOn, Createdby, PublishedDate, isActive FROM MagnetGC WHERE (Cohort = @Cohort)">
        <SelectParameters>
            <asp:ControlParameter ControlID="rbtnlstCohort" Name="Cohort" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

