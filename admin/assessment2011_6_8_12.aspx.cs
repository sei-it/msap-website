﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class admin_assessment2011 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" <table width='100%' border='0' cellspacing='0' cellpadding='3'><tr><td width='90%'><span style='font-weight: bold;'>File name</span></td><td width='10%'><span style='font-weight: bold;'>Files available</span></td></tr>");
            int i = 1;
            foreach (MagnetWebinar webinar in MagnetWebinar.Find(x => x.DocType == false && x.DocCategory == true).OrderBy(x => x.DocumentName))
            {
                i++;
                if (i % 2 == 0)
                    sb.Append("<tr bgcolor='#E8E8E8'>");
                else
                    sb.Append("<tr>");
                sb.AppendFormat("<td>{0}</td><td width='10%'><a href='../applicationdoc/{1}' target='_blank'>{2}</a>", webinar.DocumentName, webinar.PhysicalName, webinar.DocumentType);
                sb.Append("</td></tr>");
            }
            sb.Append("</table>");
            Div1.InnerHtml = sb.ToString();
        }
    }
}