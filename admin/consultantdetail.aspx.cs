﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class admin_memberdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                MagnetConsultant member = MagnetConsultant.SingleOrDefault(x => x.ID == Convert.ToInt32(Request.QueryString["id"]));
                ltTitle.Text = member.Name;
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("<img src='../images/upload/{0}' width='110px' /><br /><br /><i>Expertise</i>", member.ImagePath);
                sb.AppendFormat(member.Expertise);
                sidebar1.InnerHtml = sb.ToString();
                mainContent.InnerHtml = member.ConsultantBio;
            }
        }
    }
}