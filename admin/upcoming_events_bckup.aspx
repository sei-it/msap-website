<%@ Page Title="MSAP Upcoming Events" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
     %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Upcoming Events
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Upcoming Events
    </h1>
        <p>The MSAP Center designed these technical assistance events to help you implement, manage, and sustain your magnet schools projects. For example, the webinars will provide fundamental information for developing and enhancing your magnet schools, and the live chats offer scheduled times to explore the topics with experts. Put these events on your calendar and check this page regularly for updates and new events. Visit the Past Webinars page to view archived webinar recordings and tools.</p>
<p><h2>Planning for Sustainability</h2>
Get help with planning for program sustainability. These highly interactive and lively distance-learning opportunities include webinars, facilitated live chats, and individual and small-group consultations. The upcoming topics and events in the series are

<p class="space_top"><strong>Topic 4: Developing and Writing the Sustainability Plan</strong><br />
<strong>Webinar:</strong> June 6, 2012, 1:00 to 2:00 p.m. Eastern<br/>
<strong>Live Chat:</strong> June 14, 2012, 3:00 to 4:00 p.m. Eastern<br/>
<strong>Description:</strong> Follow an outline and a process for writing a sustainability plan. Tools include a sample sustainability plan with instructions for customization. After completing these activities , you will know the steps in developing a sustainability plan and see how you and your planning team can structure a written, living document. The webinar series will conclude with lessons learned and planning tips.</p>

<!--<p class="space_top"></p>-->

<p class="space_top">For information on all events, contact<br/>
Elizabeth (Beth) Ford<br/>
MSAP Center<br/>
(866) 997-6727<br/>
<a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a>
</p>
</asp:Content>