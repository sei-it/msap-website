﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managesurvey.aspx.cs" Inherits="admin_managesurvey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Download Survey Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Download Survey Data</h1>
    <p style="text-align: left">
        <asp:Button ID="Newbutton" runat="server" Text="Download Survey Data" CssClass="msapBtn"
            OnClick="OnDownload" />
    </p>
    <p>
        1. How many years have you worked in K-12 education?
        <br />
        Value="1" Text="0-2 years"<br />
        Value="2" Text="3-5 years"<br />
        Value="3" Text="6-8 years"<br />
        Value="4" Text="9 or more years"
    </p>
    <p>
        2. Please indicate your program’s anticipated progress towards full implementation
        in Fall 2011.<br />
        Value="1" Text="Program installation"<br />
        Value="2" Text="Initial implementation"<br />
        Value="3" Text="Full operation"
    </p>
    <p>
        3. How satisfied are you with your job as a principal in this magnet school?<br />
        Value="1" Text="Very dissatisfied"<br />
        Value="2" Text="Somewhat dissatisfied"<br />
        Value="3" Text="Somewhat satisfied"<br />
        Value="4" Text="Very satisfied"</p>
    <p>
        Question 4, 5, 6<br />
        0 - 1<br />
        1 - 2<br />
        2 - 3<br />
        3 - 4<br />
        4 - N/A
    </p>
    <p>
        8. Please select your three preferred methods for receiving technical assistance.<br />
        Value="1" Text="Conference workshops"<br />
        Value="2" Text="Emails"<br />
        Value="3" Text="Fact sheets"<br />
        Value="4" Text="Newsletters"<br />
        Value="5" Text="Telephone conferences"<br />
        Value="6" Text="Toolkits"<br />
        Value="7" Text="Webinars"<br />
        Value="8" Text="Website"
    </p>
</asp:Content>
