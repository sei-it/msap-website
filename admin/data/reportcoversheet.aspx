﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="reportcoversheet.aspx.cs" Inherits="admin_reportcoversheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    U.S. Department of Education Grant Performance Report Cover Sheet (ED 524B)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script>
    <!--[if IE]>
<style type="text/css">
table#wrapper { border-collapse: collapse; }
</style>
<![endif]-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h6 style="color: #F58220">
        <a href="quarterreports.aspx">Main Menu</a><img src="button_arrow.jpg" width="15"/>
        Performance Report Cover Sheet (ED 524B)</h6>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <div class="reportHeader">
        <b>General Information</b></div>
    <table class="msapDataTbl">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblGranteeName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="20%">
                1. PR/Award #:
            </td>
            <td>
                <asp:TextBox ID="txtPRAward" runat="server" MaxLength="11" CssClass="msapDataTxt"></asp:TextBox>
            </td>
        </tr>
        <tr class="msapDataTblLastRow">
            <td>
                2. Grantee NCES ID #:
            </td>
            <td>
                <asp:TextBox ID="txtNCESID" runat="server" MaxLength="12" CssClass="msapDataTxt"></asp:TextBox>
            </td>
        </tr>
    </table>
    <div class="reportHeader">
        <b>Report Period Information</b></div>
    <table class="msapDataTbl">
        <tr class="msapDataTblLastRow">
            <td width="20%">
                3. Reporting Period: From
            </td>
            <td>
                <asp:TextBox ID="txtReportPeriodStart" runat="server" MaxLength="15" CssClass="msapDataTxt"></asp:TextBox>
                &nbsp;&nbsp;To&nbsp;&nbsp;<asp:TextBox ID="txtReportPeriodEnd" runat="server" MaxLength="15"
                    CssClass="msapDataTxt"></asp:TextBox>
                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtReportPeriodStart">
                </ajax:CalendarExtender>
                <ajax:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtReportPeriodEnd">
                </ajax:CalendarExtender>
            </td>
        </tr>
    </table>
    <div class="reportHeader">
        <b>Budget Expenditures</b></div>
    <table class="msapDataTbl">
        <tr>
            <td colspan="3">
                4. Budget Expenditures
            </td>
        </tr>
        <tr>
            <td width="46%">
                &nbsp;
            </td>
            <td  width="20%">
                Federal Grant Funds
            </td>
            <td >
                Non-Federal Funds
            </td>
        </tr>
        <tr>
            <td>
                a. Previous Budget Period
            </td>
            <td>
                <asp:TextBox ID="txtPreviousFederalFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtPreviousFederalFunds"
                    Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
            </td>
            <td>
                <asp:TextBox ID="txtPreviousNonFederalFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtPreviousNonFederalFunds"
                    Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
            </td>
        </tr>
        <tr>
            <td>
                b. Current Budget Period
            </td>
            <td>
                <asp:TextBox ID="txtCurrentFederalFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtCurrentFederalFunds"
                    Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
            </td>
            <td>
                <asp:TextBox ID="txtCurrentNonFederalFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:MaskedEditExtender ID="MaskedEditExtender4" TargetControlID="txtCurrentNonFederalFunds"
                    Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
            </td>
        </tr>
        <tr class="msapDataTblLastRow">
            <td>
                c. Entire Project Period (<i>For Final Performance Reports only</i>)
            </td>
            <td>
                <asp:TextBox ID="txtEntireFederalFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:MaskedEditExtender ID="MaskedEditExtender5" TargetControlID="txtEntireFederalFunds"
                    Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
            </td>
            <td>
                <asp:TextBox ID="txtEntireNonFederalFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:MaskedEditExtender ID="MaskedEditExtender6" TargetControlID="txtEntireNonFederalFunds"
                    Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
            </td>
        </tr>
    </table>
    <div class="reportHeader">
        <b>Indirect Cost Information</b></div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="msapDataTbl">
                <tr>
                    <td colspan="3">
                        5. Indirect Costs
                    </td>
                </tr>
                <tr>
                    <td width="62%">
                        &nbsp;&nbsp;a. Are you claiming indirect costs under this grant?
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblClaimingIndrectCost" CssClass="msapBlankTbl" runat="server" RepeatDirection="Horizontal"
                         AutoPostBack="true" OnSelectedIndexChanged="OnClaimingIndirect">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="Required."
                            ControlToValidate="rblClaimingIndrectCost"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;b. If yes, do you have an Indirect Cost Rate Agreement approved by the
                        Federal Government?
                    </td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rblRageAggrement" CssClass="msapBlankTbl" runat="server" RepeatDirection="Horizontal"
                         AutoPostBack="true" OnSelectedIndexChanged="OnRateAgreementChanged">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;&nbsp;c. If yes, provide the following information:<br />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Period Covered by the Indirect Cost Rate Agreement:
                    </td>
                    <td colspan="2">
                        From
                        <asp:TextBox ID="txtAggrementStart" runat="server" CssClass="msapDataTxt"></asp:TextBox>&nbsp;&nbsp;To&nbsp;&nbsp;<asp:TextBox
                            ID="txtAggrementEnd" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtAggrementStart">
                        </ajax:CalendarExtender>
                        <ajax:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtAggrementEnd">
                        </ajax:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Approving Federal Agency:
                    </td>
                    <td colspan="2">
                        <asp:RadioButton ID="rbED" Text="ED" runat="server" GroupName="federalagency" />&nbsp;&nbsp;<asp:RadioButton
                            Text="Other" ID="rbOtherAgency" runat="server" GroupName="federalagency" />
                        (<i>Please specify</i>):
                        <asp:TextBox ID="txtOtherApprovalAgnecy" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type of Rate (<i>For Final Performance Reports Only</i>):
                    </td>
                    <td colspan="2">
                        <asp:RadioButton ID="rbProvisional" Text="Provisional" runat="server" GroupName="typerate" />&nbsp;&nbsp;<asp:RadioButton
                            ID="rbFinal" Text="Final" runat="server" GroupName="typerate" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton
                                Text="Other" runat="server" ID="rbOtherRateType" GroupName="typerate" />
                        (<i>Please specify</i>):
                        <asp:TextBox ID="txtOtherRateType" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;&nbsp;d. For Restricted Rate Programs (Check one) -- Are you using a restricted
                        indirect cost rate that:
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbIncludedApprovedAgreement"
                            runat="server" Text="Is included in your approved Indirect Cost Rate Agreement?" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbComplyCFR" runat="server"
                            Text="Complies with 34 CFR 76.564(c)(2)?" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;e. Indirect Cost Rate:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtIndirectCostRate" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender7" TargetControlID="txtIndirectCostRate"
                            Mask="99.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;e. Indirect Cost:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtIndirectCost" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender8" TargetControlID="txtIndirectCost"
                            Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;e. Restricted Indirect Cost Rate:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtRestrictedIndirectCostRate" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender9" TargetControlID="txtRestrictedIndirectCostRate"
                            Mask="99.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr class="msapDataTblLastRow">
                    <td>
                        &nbsp;&nbsp;e. Training Stipends:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtTrainingStipends" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender27" TargetControlID="txtTrainingStipends"
                            Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="reportHeader">
        <b>Human Subjects (Annual Institutional Review Board (IRB) Certification)</b></div>
    <table class="msapDataTbl">
        <tr class="msapDataTblLastRow">
            <td  width="52%">
                6. Annual Certification of Institutional Review Board (IRB) Approval?
            </td>
            <td>
                <asp:DropDownList ID="ddlIRBApproval" runat="server" CssClass="msapDataTxt">
                    <asp:ListItem Value="">Please select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="2">No</asp:ListItem>
                    <asp:ListItem Value="3">N/A</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="Required."
                    ControlToValidate="ddlIRBApproval"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <div class="reportHeader">
        <b>Performance Measures Status and Certification</b></div>
    <table class="msapDataTbl">
        <tr>
            <td colspan="2">
                7. Performance Measures Status
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;a. Are complete data on performance measures for the current budget
                period included in the Project Status Chart?
            </td>
            <td>
                <asp:DropDownList ID="ddlProjectStatusChart" runat="server" CssClass="msapDataTxt">
                    <asp:ListItem Value="">Please select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="2">No</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="Required."
                    ControlToValidate="ddlProjectStatusChart"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="msapDataTblLastRow">
            <td>
                &nbsp;&nbsp;b. If no, when will the data be available and submitted to the Department?
            </td>
            <td>
                <asp:TextBox ID="txtDataAvailable" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtDataAvailable">
                </ajax:CalendarExtender>
            </td>
        </tr>
    </table>
    <div class="reportHeader">
        <b>Executive Summary</b></div>
    <asp:TextBox ID="txtExecutiveSummary" runat="server" Columns="130" Rows="8" TextMode="MultiLine"
        CssClass="msapDataTxt"></asp:TextBox>
    <br /><br />
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="Button1" runat="server" Text="Save" CssClass="surveyBtn" OnClick="OnSaveData" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" Text="Print" CssClass="surveyBtn" OnClick="OnPrint" />
            </td>
        </tr>
    </table>
</asp:Content>
