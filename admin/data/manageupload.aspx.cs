﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using System.Net;
using System.Net.Mail;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Data;

public partial class admin_data_manageupload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = ManageUtility.FormatInteger((int)Session["ReportID"]);
            LoadData();
        }
            ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();
        
    }
    protected void OnDelete(object sender, EventArgs e)
    {
       var loadfile = MagnetUpload.SingleOrDefault(y => y.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
 
        System.IO.File.Delete(Server.MapPath("") + "/../upload/" + loadfile.PhysicalName);
        MagnetUpload.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        //required
        MagnetDesegregationPlan Desegregation = MagnetDesegregationPlan.SingleOrDefault(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
        int count = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == 9).Count; //required formID =9
        if (Desegregation !=null && count == 0)
        {
            Desegregation.isRequiredUpload = false;
            Desegregation.Save();
        }

       

        //voluntary
        if (loadfile.FormID == 10 || loadfile.FormID == 11)
        {
            var files = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && (x.FormID == 10 || x.FormID == 11));
            foreach (MagnetUpload file in files)
            {
                if (System.IO.File.Exists(Server.MapPath("../upload/") + file.PhysicalName))
                    System.IO.File.Delete(Server.MapPath("../upload/") + file.PhysicalName);
                file.Delete();

                MagnetUpload.Delete(x => x.ID == file.ID);
            }
        }

        count = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && (x.FormID == 10||x.FormID ==11)).Count;
        if (Desegregation !=null && count == 0)
        {
            Desegregation.isVoluntaryUpload = false;
            Desegregation.Save();
        }

        LoadData();
    }
    protected void OnSendEmail(object sender, EventArgs e)
    {
        //Build zip file
        GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
        string zipfile = Server.MapPath("docs/" + grantee.GranteeName.Trim() + "-" + DateTime.Now.ToString("MM-dd-yyyy") + ".zip");

        try
        {
            if (File.Exists(zipfile))
                File.Delete(zipfile);
            ZipOutputStream zipOut = new ZipOutputStream(File.Create(zipfile));

            foreach (MagnetUpload upload in MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value)))
            {
                FileStream sReader = File.OpenRead(Server.MapPath("") + "/../upload/" + upload.PhysicalName);
                ZipEntry entry = new ZipEntry(upload.FileName);
                byte[] buff = new byte[Convert.ToInt32(sReader.Length)];
                sReader.Read(buff, 0, (int)sReader.Length);
                entry.DateTime = DateTime.Now;
                entry.Size = sReader.Length;
                sReader.Close();
                zipOut.PutNextEntry(entry);
                zipOut.Write(buff, 0, buff.Length);
            }

            zipOut.Finish();
            zipOut.Close();

            //Create maildefination
            System.Web.UI.WebControls.MailDefinition mail = new System.Web.UI.WebControls.MailDefinition();
            mail.IsBodyHtml = true;

            mail.From = "msapcenter@leedmci.com"; //"msapcenter@seiservices.com";
            mail.Subject = "Grantee quarter report";
            string htmlBody = "<div>Dear manager,</div><div></div>";
            //Create email message
            MailMessage mess = mail.CreateMailMessage("lsun@seiservices.com", new ListDictionary(), htmlBody, new System.Web.UI.Control());
            //mess.CC.Add(new MailAddress("ahendrickson@seiservices.com"));
            //mess.CC.Add(new MailAddress("mperezrivera@seiservices.com"));

            SmtpClient objSMTPClient = new SmtpClient();
            objSMTPClient.Host = "sei-exch01.synergyentinc.local";
            NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
            objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;

            mess.IsBodyHtml = true;
            mess.Attachments.Add(new Attachment(zipfile));
            objSMTPClient.Send(mess);

            mess.Dispose();

            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your reports have been successfully sent to ED!');</script>", false);
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (File.Exists(zipfile))
                File.Delete(zipfile);
        }
    }
    private void LoadData()
    {
        int ReportPeriodID = 1;

        if (Session["ReportPeriodID"] != null)
        {
            ReportPeriodID=Convert.ToInt32(Session["ReportPeriodID"]);
        }
        else
        {
            ReportPeriodID = MagnetReportPeriod.Find(x => x.isActive == true).Last().ID;
        }

        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        MagnetDBDB db = new MagnetDBDB();
        var data = from n in db.MagnetUploads
                   where n.ProjectID == Convert.ToInt32(hfReportID.Value)
                   orderby n.FormID
                   select new
                   {
                       n.ID,
                       n.FormID,
                       n.UploadDate,
                       isEnable = ((ReportPeriodID % 2) != 0 || n.FormID == -1 || n.FormID == 0 || n.FormID == 20) ? "Y" : "N",
                       FileNames = "<a target='_blank' href='../upload/" + n.PhysicalName + "' title='" + n.PhysicalName + "'>" + n.FileName + "</a>",
                       FileType =
                       n.FormID == -1 ? "Executive Summary" :
                       n.FormID == 0 ? "Cover Sheet" :
                       n.FormID == 1 ? "Project Status Chart" :
                       n.FormID == 2 ? "GPRA Table" :
                       n.FormID == 3 ? "ED 524B Budget Summary" :
                       n.FormID == 4 ? "Desegregation Plan Information Forms" :
                       n.FormID == 5 ? "Table 7: Enrollment Data-LEA Level" :
                       n.FormID == 6 ? "Table 8: Year of Implementation for Existing Magnet Schools" :
                       n.FormID == 7 ? "Table 9: Enrollment Data-Magnet Schools" : 
                       n.FormID == 8? "Table 11: Feeder School-Enrollment Data":
                       n.FormID == 9 ? "Required Desegregation Plan" :
                       n.FormID == 10 ? "Voluntary Desegregation Plan" :
                       n.FormID == 11 ? "Voluntary Desegregation Plan" :
                       n.FormID == 12 ? "Assurances and Certifications" :
                       n.FormID == 13 ? "Budget Narrative" :
                       n.FormID == 14 ? "Itemized Budget Spreadsheet" :
                       n.FormID == 20 ? "Additional Information" :
                       "Undefined",
                       sortindex =
                       
                       n.FormID == 0 ? "a" :
                       n.FormID == -1 ? "b" :
                       n.FormID == 13 ? "c" :
                       n.FormID == 14 ? "d" :
                       n.FormID == 11 ? "e" :
                       n.FormID == 4 ? "e" :
                       n.FormID == 10 ? "e" :
                        n.FormID == 12 ? "f" :
                       n.FormID == 20 ? "g" :
                       n.FormID == 5 ? "h" :
                       n.FormID == 6 ? "i" :
                       n.FormID == 7 ? "j" :
                       n.FormID == 8 ? "k" :
                       n.FormID == 9 ? "e" :
                       
                       
                      
                      
                       n.FormID == 1 ? "n" :
                       n.FormID == 2 ? "o" :
                       n.FormID == 3 ? "p" :

                        "q"
                   };
        var sortdata = data.OrderBy(x=>x.sortindex);
        DataTable tmp = sortdata.ToDataTable();

        foreach (DataRow dr in tmp.Rows)
        {
            string mylist=dr[0].ToString();
            mylist = dr[1].ToString();
            mylist = dr[2].ToString();
            mylist = dr[3].ToString();
        }
        GridView1.DataSource = sortdata;
        GridView1.DataBind();
    }
}