﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageupload.aspx.cs" Inherits="admin_data_manageupload" ErrorPage="~/Error_page.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Uploaded Documents
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
<script type="text/javascript" language="javascript">
    function confirmMsg(formid) {

        if (formid == 10 || formid == 11)
            return confirm("Both Voluntary Plan documents will be removed. Are you sure you want to delete these uploaded files?");
        else
            return confirm("Are you sure you want to delete this uploaded file?");
    }

</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Uploaded Documents</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <div class="titles_noTabs">
        Uploaded Documents</div>
        <br/>
    <p class="clear">Displayed below is a list of all the documents that have been uploaded. Click on a file name to open it; click on Delete to remove a file.</p>
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl clear" GridLines="None">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    <table class="DotnetTbl">
                        <tr>
                            <td>
                                <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                            </td>
                            <td>
                                Uploaded Files
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Eval("FileNames")%></font>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <table class="DotnetTbl">
                        <tr>
                            <td>
                                <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                            </td>
                            <td>
                                Form
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Eval("FileType")%></font>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <table class="DotnetTbl">
                        <tr>
                            <td>
                                <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                            </td>
                            <td>
                                Upload Date
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
            <ItemTemplate>
            <font color="#4e8396">
                        <%# Convert.ToDateTime(Eval("UploadDate")).ToShortDateString()%></font>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                   <%-- <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete" Enabled='<%# Eval("isEnable").Equals("Y") %>'
                        CausesValidation="false" CommandArgument='<%# Eval("ID") %>' OnClientClick='<%# Eval("isEnable").Equals("N")?" return false; " : " return confirmMsg("+ Eval("FormID") + ");"  %>'></asp:LinkButton>--%>
                         <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete" 
                        CausesValidation="false" CommandArgument='<%# Eval("ID") %>' OnClientClick='<%# " return confirmMsg("+ Eval("FormID") + ");"  %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
