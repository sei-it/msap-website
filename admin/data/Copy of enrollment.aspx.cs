﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
public partial class admin_report_enrollment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int reportPeriod = 1;
            if (Session["ReportID"] != null)
            {
                reportPeriod = (int)Session["ReportID"];
                hfReportID.Value = reportPeriod.ToString();
            }
            else
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }

            lblFormTitle.Text = "Enrollment Data-Magnet Schools";
            hfStageID.Value = "2";

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            hfGranteeID.Value = granteeID.ToString();

            //Schools
            GridView2.DataSource = MagnetSchool.Find(x => x.GranteeID == granteeID).OrderBy(x => x.SchoolName);
            GridView2.DataBind();
        }
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        using (System.IO.MemoryStream Output = new MemoryStream())
        {
            //Print
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, Output);
            document.Open();
            PdfContentByte pdfContentByte = pdfWriter.DirectContent;

            MagnetDBDB db = new MagnetDBDB();
            List<string> TmpPDFs = new List<string>();
            foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == Convert.ToInt32(hfGranteeID.Value)).OrderBy(x => x.SchoolName))
            {
                try
                {
                    string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                    if (File.Exists(TmpPDF))
                        File.Delete(TmpPDF);
                    TmpPDFs.Add(TmpPDF);

                    PdfStamper ps = null;

                    // Fill out form
                    PdfReader r = new PdfReader(Server.MapPath("../doc/Table9.pdf"));
                    ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                    AcroFields af = ps.AcroFields;

                    af.SetField("SchoolName", School.SchoolName);

                    var Data = from m in db.MagnetSchoolEnrollments
                               where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                               && m.SchoolID == School.ID
                               && m.StageID == 2//School enrollment data
                               orderby m.GradeLevel
                               select new { m.GradeLevel, m.AmericanIndian, m.Asian, m.AfricanAmerican, m.Hispanic, m.Hawaiian, m.White, m.MultiRacial, GradeTotal = (m.AmericanIndian + m.Asian + m.Hispanic + m.Hawaiian + m.White + m.MultiRacial + m.AfricanAmerican) };

                    int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                    foreach (var Enrollment in Data)
                    {
                        af.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger((int)Enrollment.AmericanIndian));
                        af.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger((int)Enrollment.Asian));
                        af.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican));
                        af.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger((int)Enrollment.Hispanic));
                        af.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger((int)Enrollment.Hawaiian));
                        af.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger((int)Enrollment.White));
                        af.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger((int)Enrollment.MultiRacial));
                        af.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger((int)Enrollment.GradeTotal));

                        af.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", (double)Enrollment.AmericanIndian / Enrollment.GradeTotal * 100));
                        af.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Asian / Enrollment.GradeTotal * 100));
                        af.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", (double)Enrollment.AfricanAmerican / Enrollment.GradeTotal * 100));
                        af.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hispanic / Enrollment.GradeTotal * 100));
                        af.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hawaiian / Enrollment.GradeTotal * 100));
                        af.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", (double)Enrollment.White / Enrollment.GradeTotal * 100));
                        af.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", (double)Enrollment.MultiRacial / Enrollment.GradeTotal * 100));

                        TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                        TotalEnrollments[1] += (int)Enrollment.Asian;
                        TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                        TotalEnrollments[3] += (int)Enrollment.Hispanic;
                        TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                        TotalEnrollments[5] += (int)Enrollment.White;
                        TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                        TotalEnrollments[7] += (int)Enrollment.GradeTotal;
                    }

                    af.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                    af.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                    af.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                    af.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                    af.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                    af.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                    af.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                    af.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));

                    af.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                    af.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                    af.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                    af.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                    af.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                    af.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                    af.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));

                    ps.FormFlattening = true;

                    r.Close();
                    ps.Close();

                    //Add to final report
                    PdfReader reader = new PdfReader(TmpPDF);
                    document.NewPage();
                    PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                    pdfContentByte.AddTemplate(importedPage, 0, 0);
                    document.NewPage();
                    reader.Close();
                }
                catch (System.Exception ex)
                {
                    ILog Log = LogManager.GetLogger("EventLog");
                    Log.Error("School Enrollment report:", ex);
                }
                finally
                {
                }
            }

            document.Close();
            foreach (string str in TmpPDFs)
            {
                if (File.Exists(str))
                    File.Delete(str);
            }

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_Table_9.pdf");
            Response.OutputStream.Write(Output.GetBuffer(), 0, Output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            Output.Close();
        }
    }
}