﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Data.SqlClient;
using System.Data;

public partial class admin_data_schools: System.Web.UI.Page
{
    int reportyear = 1, cohortType =0;
    
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        reportyear = rblActive.SelectedIndex + 1;
        //ClientScript.RegisterOnSubmitStatement(this.GetType(), String.Concat(this.ClientID, "_OnSubmit"), "javascript: return OvrdSubmit();");
        if (!Page.IsPostBack)
        {
            if (Session["ReportYear"] != null || rblActive.SelectedIndex != 0)
                reportyear = Convert.ToInt32(Session["ReportYear"]);
            else
            {
                var report = MagnetReportPeriod.SingleOrDefault(x => x.isReportPeriod);
                if(report !=null)
                {
                    switch (report.ID)
                    {
                        case 1:
                        case 2:
                            reportyear = 1;
                            break;
                        case 3:
                        case 4:
                            reportyear = 2;
                            break;
                        case 5:
                        case 6:
                            reportyear = 3;
                            break;
                        default:
                            reportyear = 1;
                            break;
                    }
                }

                Session["ReportYear"] = reportyear;

            }

            cohortType = (int)MagnetReportPeriod.Find(x => x.isActive).Last().CohortType;
            rbtnlstCohort.SelectedValue = cohortType.ToString();
            setGranteelist();
        }
        LoadData(0);
    }

    private void setGranteelist()
    {
        ddlGrantees.Items.Clear();

        string Username = HttpContext.Current.User.Identity.Name;
        var users = MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()));
        if (users.Count > 0)
        {
            hfID.Value = users[0].ID.ToString();
            var reportPeriods = MagnetReportPeriod.Find(x => x.isReportPeriod == true);
            if (reportPeriods.Count == 0)
            {
                reportPeriods = MagnetReportPeriod.Find(x => x.isActive == true);
            }

            foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == users[0].ID))
            {
                MagnetGrantee grantee = null;
                if (reportPeriods.Last().ID > 8)
                    grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == cohortType && x.ID != 69 && x.ID != 70);  //Lee county and CHampaign
                else
                    grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == cohortType && x.ID != 71);  //Wake County

               
                String GranteeName = "";
                if (grantee != null)
                {
                    var contacts = MagnetGranteeContact.Find(x => x.LinkID == district.GranteeID && x.ContactType == 1);
                    if (HttpContext.Current.User.IsInRole("Administrators") || Context.User.IsInRole("Data"))
                    {
                        GranteeName = contacts.Count > 0 ? contacts[0].State + " " + grantee.GranteeName : grantee.GranteeName;
                    }
                    else
                        GranteeName = grantee.GranteeName;
                    System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem();
                    item.Value = grantee.ID.ToString();
                    item.Text = GranteeName;
                    ddlGrantees.Items.Add(item);
                }
               
               
            }

            List<System.Web.UI.WebControls.ListItem> li = new List<System.Web.UI.WebControls.ListItem>();
            foreach (System.Web.UI.WebControls.ListItem list in ddlGrantees.Items)
            {
                li.Add(list);
            }
            li.Sort((x, y) => string.Compare(x.Text, y.Text));
            ddlGrantees.Items.Clear();
            ddlGrantees.DataSource = li;
            ddlGrantees.DataTextField = "Text";
            ddlGrantees.DataValueField = "Value";

            ddlGrantees.DataBind();
            ddlGrantees.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Please select", "-1"));

            ListItem oListItem = ddlGrantees.Items.FindByValue(users[0].GranteeID.ToString());
            if (oListItem != null)
            {
                ddlGrantees.SelectedValue = oListItem.Value;
                Session["SchGranteeid"] = oListItem.Value;
            }
            else
                Session["SchGranteeid"] = 0;
           
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        var school=MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        school.isActive = school.isActive ? false : true;
        school.Save();
        LoadData(0);
    }
    private void LoadData(int PageNumber)
    {
        int granteeID = 0;
        if (Session["SchGranteeid"] != null)
        {
            granteeID = Convert.ToInt32(Session["SchGranteeid"]);
        }
        else
        {
            //string Username = HttpContext.Current.User.Identity.Name;
            //granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            granteeID = 0;
        }
        rblActive.SelectedIndex = reportyear - 1;
        GridView1.DataSource = (MagnetSchool.Find(x => x.GranteeID == granteeID && x.ReportYear==reportyear).OrderBy(x=>x.SchoolName));
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtSchoolName.Text = "";
        txtTheme.Text = "";
        txtWebsite.Text = "";
        txtNotes.Content = "";
        ddlStatus.SelectedIndex = 0;
        hfID.Value = "";
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        PopupPanel.Visible = true;
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetSchool school = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtSchoolName.Text = school.SchoolName;
        txtTheme.Text = school.MagnetTheme;
        txtWebsite.Text = school.Website;
        txtNotes.Content= school.Notes;
		if(school.SchoolStatus!=null)
			ddlStatus.SelectedValue = Convert.ToString(school.SchoolStatus);
        rblShow.SelectedIndex = Convert.ToInt32(school.isActive);
        PopupPanel.Visible = true;
        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetSchool school = new MagnetSchool();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            school = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            int granteeID=0;
            if (Session["SchGranteeid"] != null)
                granteeID = Convert.ToInt32(Session["SchGranteeid"]);
            else
            {
                string Username = HttpContext.Current.User.Identity.Name;
                granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            }
            school.GranteeID = granteeID;
        }
        school.SchoolName = txtSchoolName.Text;
        school.MagnetTheme = txtTheme.Text.Trim();
        school.Website = txtWebsite.Text;
        school.Notes = txtNotes.Content;
        school.SchoolStatus = Convert.ToInt32(ddlStatus.SelectedValue);
        school.ReportYear = reportyear;
        school.isActive = Convert.ToBoolean(rblShow.SelectedIndex);
        school.Save();
        LoadData(0);
    }
    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopupPanel.Visible = false;
        int granteeID = Convert.ToInt32(ddlGrantees.SelectedValue);
        reportyear = Convert.ToInt32(rblActive.SelectedValue);
        GridView1.DataSource = MagnetSchool.Find(x => x.GranteeID == granteeID && x.ReportYear==reportyear );
        Session["SchGranteeid"] = granteeID;
        GridView1.DataBind();
    }
    protected void rbtnlstCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        cohortType = Convert.ToInt32(rbtnlstCohort.SelectedValue);
        setGranteelist();
        LoadData(0);
    }
}