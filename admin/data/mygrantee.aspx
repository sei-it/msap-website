﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="mygrantee.aspx.cs" Inherits="admin_report_mygrantee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    My current Grantee
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script src="../../js/jquery-1.5.1.js" type="text/javascript"></script>
    <script src="../../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../../js/jquery.cookie.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#<%=Button2.ClientID %>, #<%=Button1.ClientID %>, #<%=Button4.ClientID %>, #<%=Button6.ClientID %>").click(function () {
            blockUIForDownload();
        });
    });

    var fileDownloadCheckTimer;
    function blockUIForDownload() {
        var token = new Date().getTime(); //use the current timestamp as the token value
        $('#<%=hfTokenID.ClientID %>').val(token);
        $.blockUI({ message: '<h1><img src="../../img/busy.gif" /> Just a moment...</h1>' }); 
        fileDownloadCheckTimer = window.setInterval(function () {
            var cookieValue = $.cookie('fileDownloadToken');
            if (cookieValue == token)
                finishDownload();
        }, 1000);
    }

    function finishDownload() {
        window.clearInterval(fileDownloadCheckTimer);
        $.cookie('fileDownloadToken', null); //clears this cookie value
        $.unblockUI();
    }

    function MakeSure() {

        var isConfirm = false;
        var granteeId=$("#<%=DropDownList3.ClientID %> option:selected").text();
        if (granteeId != "Please select") {

            isConfirm = confirm('Are you sure you want to submit a report?');
        }
        else
            alert('You have to select a grantee from a drop-down list.');
        return isConfirm;
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div id="GranteeDiv" runat="server" visible="true">
        <h1> <asp:Literal ID="ltlMyGrantee" runat="server" /></h1>
        <h2>
        <asp:Literal ID="ltlGranteeTitle" runat="server" />
         </h2>
        <table style="width: 50%; margin-left: 20px;">
            <tr>
                <td>
                    <asp:DropDownList ID="ddlGrantees" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSave">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; width: 50%; padding-top: 15px;">
                    <asp:Button ID="Button3" runat="server" Text="Enter Grantee Data" CssClass="surveyBtn2"
                        OnClick="OnJump" Visible="true" />
                    <asp:HiddenField ID="hfID" runat="server" />
                </td>
            </tr>
        </table>
    </div>

        <asp:Panel ID="pnlPrints" runat="server" Visible="false">
    <h2 class="space_top">
        Print Final Report</h2>
    <p>
        Please select a Grantee and a report period.</p>
 
    <table style="width: 50%; margin-left: 20px;">
    <tr>
    <td>
    <asp:RadioButtonList ID="rblstPrintCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" 
                onselectedindexchanged="rblstPrintCohort_SelectedIndexChanged">
    <asp:ListItem Value="1"  >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2" >MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
    </td>
    </tr>
    <tr>
        <td>
                <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="true" 
                    onselectedindexchanged="OnGranteeChanged">
                    <asp:ListItem Value="" Text="Please select"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    <tr>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server"  >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right; padding-top: 15px;">
                <asp:Button ID="Button2" runat="server" Text="Print Final Report" CssClass="surveyBtn2"
                    OnClick="OnPrint" />
            </td>
        </tr>
    </table>

    
        </asp:Panel>
    <div id="NotifyDiv" runat="server" visible="false">
        <h2 class="space_top">
            Submit Report</h2>
        <p>
            Press this button to notify the U.S. Department of Education (ED)
            <br />
            that your APR is complete and ready for review.
        </p>
        <table style="width: 50%; margin-left: 20px;">
            <tr>
                <td>
                    <asp:DropDownList ID="DropDownList3" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right; padding-top: 15px;">
                    <asp:Button ID="btnExcel" runat="server" Text="Export Execl " OnClick="ExportExcelReport" />
                    <asp:Button ID="NotifyED" runat="server" Text="Submit Report" CssClass="surveyBtn2" OnClick="OnNotify" OnClientClick=" return MakeSure();" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ReportDiv" runat="server" visible="false">
        <h2 class="space_top">Download MAPS Excel Database</h2>
        <p>
            Please select a report period.</p>

        <table style="width: 50%; margin-left: 20px;">
            <tr>
            <td colspan="3">
            <asp:RadioButtonList ID="rbtnlstDownloadCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" 
                    onselectedindexchanged="rbtnlstDownloadCohort_SelectedIndexChanged" >
    <asp:ListItem Value="1"  >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2" >MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
            </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:DropDownList ID="DropDownList4" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; padding-top: 15px;">
                    <asp:Button ID="Button1" runat="server" Text="Download Report I" CssClass="surveyBtn2"
                        OnClick="OnDownload" />
                </td>
                <td style="text-align: right; padding-top: 15px;">
                    <asp:Button ID="Button4" runat="server" Text="Download Report II" CssClass="surveyBtn2"
                        OnClick="OnDownloadSec" />
                </td>
                <td style="text-align: right; padding-top: 15px;">
                    <asp:Button ID="Button6" runat="server" Text="Download Report III" CssClass="surveyBtn2"
                        OnClick="OnDownloadThird" />
                </td>
            </tr>
        </table>

        <h2 class="space_top">
            Admin Grantee Reports</h2>

         <table style="width: 50%; margin-left: 20px;">
        <tr>
        <td >
          <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" 
                onselectedindexchanged="rbtnlstCohort_SelectedIndexChanged">
    <asp:ListItem Value="1"  >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2" >MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
        </td>
        </tr>
        <tr>
                <td>
                    <asp:DropDownList ID="DropDownList5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnAdminGranteeChanged">
                      
                    </asp:DropDownList>
                </td>
            </tr>
        <tr>
                <td>
                    <asp:DropDownList ID="DropDownList6" runat="server" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right; padding-top: 15px;">
                    <asp:Button ID="Button5" runat="server" Text="Review Data" CssClass="surveyBtn2"
                        OnClick="OnReview" />
            
                </td>
            </tr>
        </table>
     
    </div>

    <asp:HiddenField ID="hfTokenID" runat="server" />
</asp:Content>
