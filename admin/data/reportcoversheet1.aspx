﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="reportcoversheet1.aspx.cs" Inherits="admin_reportcoversheet1"  ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    U.S. Department of Education Grant Performance Report Cover Sheet (ED 524B)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
        function setContentDirty() {
            $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
        }
        function setUpcaseletter(objZip) {

            if (/[^a-z]/gi.test(objZip.value)) {
                objZip.value = objZip.value.replace(/[^a-z]/gi, ""); // strip non-alpha chars
            }
            else
                objZip.value = objZip.value.toUpperCase();

        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Performance Report Cover Sheet (ED 524B)</h4>
<div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfDirectorID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <asp:HiddenField ID="hfControlID" runat="server" ClientIDMode="Static" />
    <br/>
    <div class="titles">Performance Report Cover Sheet (ED 524B) - Page 1 of 4</div>
    <div class="tab_area">
    	<ul class="tabs">
        	<li><a href="reportcoversheet4.aspx">Page 4</a></li>
            <li><a href="reportcoversheet3.aspx">Page 3</a></li>
            <li><a href="reportcoversheet2.aspx">Page 2</a></li>
            <li class="tab_active">Page 1</li>
        </ul>
    </div>
    <br/>
    <p>
        Enter or verify the information in the ED 524B Cover Sheet.
         <img src="../../images/question_mark-thumb.png" title="For detailed instructions on items 2, 5, and 6 through 12, please see the <i>ED 524B Instructions</i>." />
    </p>
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>General
            Information</b></span>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="msapDataTbl">
                <tr>
                    <td class="TDWithTopNoRightBorder subtitles_green">
                        1.
                    </td>
                    <td class="TDWithTopBorder subtitles_green">
                        PR/Award #:
                        <img src="../../images/question_mark-thumb.png" title="Verify the 11 characters from Block 5 of the Grant Award Notification." />
                    </td>
                    <td width="76%" style="border-right: 0px;" colspan="4" class="TDWithTopBorder">
                        <asp:TextBox ID="txtPRAward" runat="server" onchange="setContentDirty();" MaxLength="11" CssClass="msapDataTxt"
                            Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="TDWithTopNoRightBorder subtitles_green">
                        2.
                    </td>
                    <td class="subtitles_green">
                        Grantee NCES ID #:
                        <img src="../../images/question_mark-thumb.png" title="Verify the NCES ID number (up to 12 characters)." />
                    </td>
                    <td style="border-right: 0px;" colspan="4">
                        <asp:TextBox ID="txtNCESID" runat="server" onchange="setContentDirty();" MaxLength="12" CssClass="msapDataTxt"
                            Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="TDWithTopNoRightBorder subtitles_green">
                        3.
                    </td>
                    <td class="subtitles_green">
                        Project Title:
                        <img src="../../images/question_mark-thumb.png" title="Verify the title that appears on the approved application." />
                    </td>
                    <td style="border-right: 0px;" colspan="4">
                        <asp:TextBox ID="txtTitle" runat="server" onchange="setContentDirty();" Enabled="false" Width="600" MaxLength="1000"
                            CssClass="msapDataTxt"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="TDWithTopNoRightBorder subtitles_green">
                        4.
                    </td>
                    <td class="subtitles_green">
                        Grantee Name:
                        <img src="../../images/question_mark-thumb.png" title="Verify grantee name from Block 1 of the Grant Award Notification." />
                    </td>
                    <td style="border-right: 0px;" colspan="4">
                        <asp:TextBox ID="txtGranteeName" runat="server" onchange="setContentDirty();" Enabled="false" Width="600" MaxLength="250"
                            CssClass="msapDataTxt"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="TDWithTopNoRightBorder subtitles_green">
                        5.
                    </td>
                    <td class="subtitles_green">
                        Grantee Address:
                    </td>
                    <td colspan="3" class="TDWithTopNoRightBorder">
                        <asp:CheckBox ID="ckChangeAddress" runat="server" onClick="setContentDirty();"   AutoPostBack="true" OnCheckedChanged="OnChangeAddress"/> 
                        Select checkbox to edit current address.</td>
                    <td>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                        HeaderText="Please complete all items with asterisks." ShowMessageBox="false" 
                        DisplayMode="list" ShowSummary="true"  />
                    
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;
                        
                    </td>
                    <td class="TDWithBottomBorder">
                        Address:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtAddress" runat="server" onchange="setContentDirty();" MaxLength="500" CssClass="msapDataTxt change"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                           ControlToValidate="txtAddress"  
                           EnableClientScript="true"  
                           SetFocusOnError="true"  
                           Text="*"  ></asp:RequiredFieldValidator>
                    </td>
                    <td class="TDWithBottomBorder">
                        City:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtCity" runat="server" onchange="setContentDirty();" MaxLength="250" CssClass="msapDataTxt change"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                          ControlToValidate="txtCity"  
                           EnableClientScript="true"  
                           SetFocusOnError="true"  
                           Text="*"  ></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="TDWithTopNoRightBorder">&nbsp;
                        
                    </td>
                    <td>&nbsp;
                        
                    </td>
                    <td class="TDWithBottomBorder">
                        State:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtState" runat="server" onchange="setContentDirty();" MaxLength="2" CssClass="msapDataTxt change"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                          ControlToValidate="txtState"  
                           EnableClientScript="true"  
                           SetFocusOnError="true"  
                           Text="*"  ></asp:RequiredFieldValidator>
                    </td>
                    <td class="TDWithBottomBorder">
                        Zip code:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtZipcode" runat="server" MaxLength="15" CssClass="msapDataTxt change" onchange="setContentDirty();"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txtZipcode"
                            Mask="99999">
                        </ajax:MaskedEditExtender>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                           ControlToValidate="txtZipcode"  
                           EnableClientScript="true"  
                           SetFocusOnError="true"  
                           Text="*"  ></asp:RequiredFieldValidator>

                       <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                        ErrorMessage="Zipcode is invalid." Text="*" 
                        ForeColor="Red"   ValidationExpression="^\d{5}(-\d{4})?$" 
                        ControlToValidate="txtZipcode" SetFocusOnError="true" EnableClientScript="true"/>
                        
                    </td>
                </tr>
                <tr>
                    <td class="TDWithTopNoRightBorder subtitles_green">
                        6.
                    </td>
                    <td class="subtitles_green">
                        Project Director:
                        
                    </td>
                    <td class="TDWithBottomBorder">
                        Name:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtDirectorName" runat="server" onchange="setContentDirty();" MaxLength="500" CssClass="msapDataTxt change"></asp:TextBox>
                    </td>
                    <td class="TDWithBottomBorder">
                        Title:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtDirectorTitle" runat="server" onchange="setContentDirty();" MaxLength="250" CssClass="msapDataTxt change"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="TDWithTopNoRightBorder">&nbsp;
                        
                    </td>
                    <td>&nbsp;
                        
                    </td>
                    <td class="TDWithBottomBorder">
                        Ph #:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtPhone" runat="server" onchange="setContentDirty();" MaxLength="15" CssClass="msapDataTxt change"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtPhone"
                            Mask="(999)999-9999">
                        </ajax:MaskedEditExtender>
                    </td>
                    <td class="TDWithBottomBorder">
                        Ext:
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:TextBox ID="txtExt" runat="server" onchange="setContentDirty();" MaxLength="15" CssClass="msapDataTxt change"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtenderExt" runat="server" TargetControlID="txtExt"
                         FilterType="Numbers"></ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td class="TDWithoutBorder">&nbsp;
                        
                    </td>
                    <td class="TDWithBorder">&nbsp;
                        
                    </td>
                    <td class="TDWithoutBorder">
                        Fax #:
                    </td>
                    <td class="TDWithoutBorder" >
                        <asp:TextBox ID="txtFax" runat="server" onchange="setContentDirty();" MaxLength="15" CssClass="msapDataTxt change"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtFax"
                            Mask="(999)999-9999">
                        </ajax:MaskedEditExtender>
                    </td>
                    <td class="TDWithoutBorder">
                        Email:
                    </td>
                    <td class="TDWithoutBorder" >
                        <asp:TextBox ID="txtEmail" runat="server" onchange="setContentDirty();" MaxLength="250" Width="320" CssClass="msapDataTxt change"></asp:TextBox>
                        <br/>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="Please enter a valid e-mail address." Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Report Period
            Information</b></span>
    <table class="msapDataTbl">
        <tr>
            <td width="4%"  class="TDWithTopNoRightBorder subtitles_green">
                7.</td>
             <td width="20%" class="TDWithTopBorder subtitles_green">
             	Reporting Period:
                    
                
            </td>
            <td width="76%" class="TDWithTopBorder" style="border-right: 0px !important;">
                From&nbsp;&nbsp;<asp:TextBox ID="txtReportPeriodStart" runat="server" MaxLength="15"
                    Enabled="false" Width="100" CssClass="msapDataTxt"></asp:TextBox>
                &nbsp;&nbsp;To&nbsp;&nbsp;<asp:TextBox ID="txtReportPeriodEnd" runat="server" MaxLength="15"
                    Enabled="false" Width="100" CssClass="msapDataTxt"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table width="100%">
        <tr align="right">
            <td style="padding-top: 10px;">
                <asp:Button ID="Button1" runat="server" Text="Save Record" CausesValidation="true" CssClass="surveyBtn1 postbutton"
                    OnClick="OnSaveData" />
            </td>
            <td width="30">
                <asp:ImageButton Width="29" Height="36" ID="ImageButton2" CssClass="postbutton" runat="server"
                    ImageUrl="button_arrow.jpg" OnClick="OnNext" ToolTip="Next" Visible="false" />
            </td>
        </tr>
    </table>
        <div style="text-align: right; margin-top: 20px;">
           Page 1&nbsp;&nbsp;&nbsp;&nbsp;
           <a href="reportcoversheet2.aspx">Page 2</a>&nbsp;&nbsp;&nbsp;&nbsp;
           <a href="reportcoversheet3.aspx">Page 3</a>&nbsp;&nbsp;&nbsp;&nbsp;
           <a href="reportcoversheet4.aspx">Page 4</a>
        </div>
</asp:Content>
