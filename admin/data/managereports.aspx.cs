﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_data_managereports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        if (DropDownList2.SelectedIndex > 0)
        {
            List<string> TempFiles = new List<string>();
            try
            {
                using (System.IO.MemoryStream output = new MemoryStream())
                {
                    int ReportID = Convert.ToInt32(DropDownList2.SelectedValue);

                    GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
                    MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);

                    Document doc = new Document(PageSize.A4, 40f, 20f, 170f, 30f);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(doc, output);
                    doc.Open();
                    PdfContentByte pdfContentByte = pdfWriter.DirectContent;

                    //Cover Sheet
                    //Cover sheet files
                    string TmpPDF = "";

                    //Add uploaded cover sheet
                    foreach (MagnetUpload upload in MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 1))
                    {
                        PdfReader csuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + upload.PhysicalName);
                        for (int t = 1; t <= csuploadReader.NumberOfPages; t++)
                        {
                            doc.NewPage();
                            PdfImportedPage csuploadPage = pdfWriter.GetImportedPage(csuploadReader, t);
                            pdfContentByte.AddTemplate(csuploadPage, 0, 0);
                        }
                        csuploadReader.Close();
                    }

                    //Project Status Chart
                    var Data = MagnetProjectObjective.Find(x => x.ReportID == ReportID && x.IsActive == true);
                    if (Data.Count > 0)
                    {
                        //Print
                        int ID = 1;
                        foreach (MagnetProjectObjective ObjData in Data)
                        {
                            var PerformanceData = MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == ObjData.ID && x.IsActive == true);
                            if (PerformanceData.Count > 0)
                            {
                                int idx = 1;
                                foreach (MagnetGrantPerformance Performance in PerformanceData)
                                {
                                    TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(TmpPDF))
                                        File.Delete(TmpPDF);
                                    TempFiles.Add(TmpPDF);

                                    PdfStamper pscps = null;

                                    //
                                    PdfReader pscr = null;
                                    pscr = new PdfReader(Server.MapPath("../doc/statuchartp1.pdf"));

                                    pscps = new PdfStamper(pscr, new FileStream(TmpPDF, FileMode.Create));

                                    AcroFields pscaf = pscps.AcroFields;

                                    pscaf.SetField("PRAward", grantee.PRAward);
                                    pscaf.SetField("ID", ID.ToString());
                                    if (ObjData.StatusUpdate != null && (bool)ObjData.StatusUpdate == true) pscaf.SetField("UpdateStatus", "Yes");
                                    pscaf.SetField("ProjectObjective", ObjData.ProjectObjective);

                                    //foreach (MagnetGrantPerformance Performance in PerformanceData)
                                    {
                                        pscaf.SetField("Performance MeasureRow" + idx.ToString(), Performance.PerformanceMeasure);
                                        pscaf.SetField("Measure Type" + idx.ToString(), Performance.MeasureType);
                                        pscaf.SetField("Target Raw Number " + idx.ToString(), Performance.TargetNumber == null ? "" : ManageUtility.FormatInteger((int)Performance.TargetNumber));
                                        if (!string.IsNullOrEmpty(Performance.TargetRatio1))
                                        {
                                            pscaf.SetField("Target Ratio Top " + idx.ToString(), Performance.TargetRatio1);
                                            pscaf.SetField("Target Ratio Bottom " + idx.ToString(), Performance.TargetRatio2);
                                        }
                                        pscaf.SetField("Target Percentage " + idx.ToString(), Performance.TargetPercentage == null ? "" : Convert.ToString(Performance.TargetPercentage));
                                        pscaf.SetField("APD Raw Number " + idx.ToString(), Performance.ActualNumber == null ? "" : ManageUtility.FormatInteger((int)Performance.ActualNumber));
                                        if (!string.IsNullOrEmpty(Performance.AcutalRatio1))
                                        {
                                            pscaf.SetField("APD Ratio Top " + idx.ToString(), Performance.AcutalRatio1);
                                            pscaf.SetField("APD Ratio Bottom " + idx.ToString(), Performance.AcutalRatio2);
                                        }
                                        pscaf.SetField("APD Percent " + idx.ToString(), Performance.ActualPercentage == null ? "" : Convert.ToString(Performance.ActualPercentage));
                                        //idx++;
                                    }

                                    string ExtraContent = "";
                                    if (!string.IsNullOrEmpty(Performance.ExplanationProgress) && Performance.ExplanationProgress.Length > 1932)
                                    {
                                        pscaf.SetField("Explanation", Performance.ExplanationProgress.Substring(0, 1932));
                                        ExtraContent = Performance.ExplanationProgress.Substring(1932, Performance.ExplanationProgress.Length - 1932);
                                    }
                                    else
                                        pscaf.SetField("Explanation", Performance.ExplanationProgress);
                                    pscps.FormFlattening = true;

                                    pscr.Close();
                                    pscps.Close();

                                    //Add to final report
                                    PdfReader pscreader = new PdfReader(TmpPDF);
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                    doc.NewPage();
                                    PdfImportedPage pscimportedPage = pdfWriter.GetImportedPage(pscreader, 1);
                                    pdfContentByte.AddTemplate(pscimportedPage, 0, 0);
                                    pscreader.Close();

                                    //Add page for extra content
                                    if (!string.IsNullOrEmpty(ExtraContent))
                                    {
                                        string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                        string FinalPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                        if (File.Exists(FinalPDF))
                                            File.Delete(FinalPDF);
                                        TempFiles.Add(FinalPDF);
                                        string Stationery = Server.MapPath("../doc/sc") + grantee.PRAward + ".pdf";
                                        if (File.Exists(ContentPDF))
                                            File.Delete(ContentPDF);
                                        TempFiles.Add(ContentPDF);

                                        Document pscContentDocument = new Document(PageSize.A4.Rotate(), 40f, 80f, 120f, 30f);
                                        PdfWriter pscContentWriter = PdfWriter.GetInstance(pscContentDocument, new FileStream(ContentPDF, FileMode.Create));
                                        pscContentDocument.Open();
                                        //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                                        pscContentDocument.Add(new Paragraph(new Chunk(ExtraContent, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                                        pscContentDocument.Close();

                                        //Add stamp
                                        PdfReader psco_reader = new PdfReader(ContentPDF);
                                        PdfReader pscs_reader = new PdfReader(Stationery);
                                        PdfStamper pscstamper = new PdfStamper(psco_reader, new FileStream(FinalPDF, FileMode.Create));

                                        PdfImportedPage pscpage = pscstamper.GetImportedPage(pscs_reader, 1);
                                        int t = psco_reader.NumberOfPages;
                                        PdfContentByte pscbackground;
                                        for (int i = 1; i <= t; i++)
                                        {

                                            pscbackground = pscstamper.GetUnderContent(i);
                                            pscbackground.AddTemplate(pscpage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                        }
                                        pscstamper.Close();

                                        //Add to output
                                        PdfReader pscfreader = new PdfReader(FinalPDF);
                                        for (int j = 1; j <= pscfreader.NumberOfPages; j++)
                                        {
                                            doc.SetPageSize(PageSize.A4.Rotate());
                                            doc.NewPage();
                                            PdfImportedPage pscfimportedPage = pdfWriter.GetImportedPage(pscfreader, j);
                                            pdfContentByte.AddTemplate(pscfimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                        }
                                        pscfreader.Close();
                                    }
                                }
                                ID++;
                            }
                            else
                            {
                                //No active measure, only print out objecitve
                                TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                if (File.Exists(TmpPDF))
                                    File.Delete(TmpPDF);
                                TempFiles.Add(TmpPDF);

                                PdfStamper pscps = null;

                                //
                                PdfReader pscr = null;
                                pscr = new PdfReader(Server.MapPath("../doc/statuchartp1.pdf"));

                                pscps = new PdfStamper(pscr, new FileStream(TmpPDF, FileMode.Create));

                                AcroFields pscaf = pscps.AcroFields;


                                pscaf.SetField("PRAward", grantee.PRAward);
                                pscaf.SetField("ID", ID.ToString());
                                if (ObjData.StatusUpdate != null && (bool)ObjData.StatusUpdate == true) pscaf.SetField("UpdateStatus", "Yes");
                                pscaf.SetField("ProjectObjective", ObjData.ProjectObjective);

                                pscps.FormFlattening = true;

                                pscr.Close();
                                pscps.Close();

                                //Add to final report
                                PdfReader pscreader = new PdfReader(TmpPDF);
                                doc.SetPageSize(PageSize.A4.Rotate());
                                doc.NewPage();
                                PdfImportedPage pscimportedPage = pdfWriter.GetImportedPage(pscreader, 1);
                                pdfContentByte.AddTemplate(pscimportedPage, 0, 0);
                                pscreader.Close();
                            }
                        }
                    }

                    //GPRA 
                    var GPRADatas = MagnetGPRA.Find(x => x.ReportID == ReportID);
                    if (GPRADatas.Count > 0)
                    {
                        foreach (MagnetGPRA GPRAData in GPRADatas)
                        {
                            TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(TmpPDF))
                                File.Delete(TmpPDF);
                            TempFiles.Add(TmpPDF);

                            PdfStamper gpraps = null;

                            // Fill out form
                            PdfReader gprar = new PdfReader(Server.MapPath("../doc/gpra.pdf"));
                            gpraps = new PdfStamper(gprar, new FileStream(TmpPDF, FileMode.Create));

                            AcroFields gpraaf = gpraps.AcroFields;

                            gpraaf.SetField("1 School name", MagnetSchool.SingleOrDefault(x => x.ID == GPRAData.SchoolID).SchoolName);
                            gpraaf.SetField("2 Grantee name", grantee.GranteeName);
                            if (!string.IsNullOrEmpty(GPRAData.SchoolGrade))
                            {
                                foreach (string str in GPRAData.SchoolGrade.Split(';'))
                                {
                                    gpraaf.SetField("Grades" + str, "Yes");
                                }
                            }
                            if (GPRAData.ProgramType != null && (bool)GPRAData.ProgramType == false) gpraaf.SetField("4 whole school magnet", "Yes"); else gpraaf.SetField("4 partial school magnet", "Yes");
                            if (GPRAData.TitleISchoolFunding != null && (bool)GPRAData.TitleISchoolFunding == true)
                            {
                                gpraaf.SetField("5 title 1 funded school", "Yes");
                                if (GPRAData.TitleISchoolFundingImprovement != null)
                                {
                                    if ((bool)GPRAData.TitleISchoolFundingImprovement == true)
                                    {
                                        gpraaf.SetField("6 Title 1 school improvement Yes", "Yes");
                                        if (GPRAData.TitleISchoolFundingImprovementStatus != null)
                                        {
                                            switch ((int)GPRAData.TitleISchoolFundingImprovementStatus)
                                            {

                                                case 1:
                                                    gpraaf.SetField("7 Title 1 status Improvement", "Yes");
                                                    break;
                                                case 2:
                                                    gpraaf.SetField("7 Title 1 status Corrective Action", "Yes");
                                                    break;
                                                case 3:
                                                    gpraaf.SetField("7 Title 1 status restructuring", "Yes");
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                        gpraaf.SetField("6 Title 1 school improvement No", "Yes");
                                }
                            }
                            else gpraaf.SetField("5 non title 1 funded school", "Yes");
                            if (GPRAData.PersistentlyLlowestAchievingSchool != null)
                            {
                                if ((bool)GPRAData.PersistentlyLlowestAchievingSchool == true)
                                {
                                    gpraaf.SetField("8 Persistently low achieving school Yes", "Yes");
                                    if (GPRAData.SchoolImprovementGrant != null)
                                    {
                                        if ((bool)GPRAData.SchoolImprovementGrant == true)
                                            gpraaf.SetField("9 SIG Funds Yes", "Yes");
                                        else
                                            gpraaf.SetField("9 SIG Funds No", "Yes");
                                    }
                                }
                                else
                                    gpraaf.SetField("8 Persistently low achieving school No", "Yes");
                            }

                            //Part II
                            var ApplicationPools = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 1 && x.MagnetGPRAID == GPRAData.ID);
                            if (ApplicationPools.Count > 0)
                            {
                                if (ApplicationPools[0].Total != null) gpraaf.SetField("10", ManageUtility.FormatInteger((int)ApplicationPools[0].Total));
                                if (ApplicationPools[0].Indian != null) gpraaf.SetField("11", ManageUtility.FormatInteger((int)ApplicationPools[0].Indian));
                                if (ApplicationPools[0].Asian != null) gpraaf.SetField("12", ManageUtility.FormatInteger((int)ApplicationPools[0].Asian));
                                if (ApplicationPools[0].Black != null) gpraaf.SetField("13", ManageUtility.FormatInteger((int)ApplicationPools[0].Black));
                                if (ApplicationPools[0].Hispanic != null) gpraaf.SetField("14", ManageUtility.FormatInteger((int)ApplicationPools[0].Hispanic));
                                if (ApplicationPools[0].Hawaiian != null) gpraaf.SetField("15", ManageUtility.FormatInteger((int)ApplicationPools[0].Hawaiian));
                                if (ApplicationPools[0].White != null) gpraaf.SetField("16", ManageUtility.FormatInteger((int)ApplicationPools[0].White));
                                if (ApplicationPools[0].MultiRaces != null) gpraaf.SetField("17", ManageUtility.FormatInteger((int)ApplicationPools[0].MultiRaces));
                            }

                            var EnrollmentDatas = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 2 && x.MagnetGPRAID == GPRAData.ID);
                            if (EnrollmentDatas.Count > 0)
                            {
                                if (EnrollmentDatas[0].ExtraFiled1 != null) gpraaf.SetField("18", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled1));
                                if (EnrollmentDatas[0].Total != null) gpraaf.SetField("19", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Total));
                                if (EnrollmentDatas[0].Indian != null) gpraaf.SetField("20", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Indian));
                                if (EnrollmentDatas[0].Asian != null) gpraaf.SetField("21", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Asian));
                                if (EnrollmentDatas[0].Black != null) gpraaf.SetField("22", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Black));
                                if (EnrollmentDatas[0].Hispanic != null) gpraaf.SetField("23", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hispanic));
                                if (EnrollmentDatas[0].Hawaiian != null) gpraaf.SetField("24", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hawaiian));
                                if (EnrollmentDatas[0].White != null) gpraaf.SetField("25", ManageUtility.FormatInteger((int)EnrollmentDatas[0].White));
                                if (EnrollmentDatas[0].MultiRaces != null) gpraaf.SetField("26", ManageUtility.FormatInteger((int)EnrollmentDatas[0].MultiRaces));
                                if (EnrollmentDatas[0].ExtraFiled2 != null) gpraaf.SetField("27", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled2));
                                if (EnrollmentDatas[0].ExtraFiled1 != null && ApplicationPools[0].ExtraFiled2 != null && ApplicationPools[0].Total != null)
                                    gpraaf.SetField("28", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled1 + (int)EnrollmentDatas[0].ExtraFiled2 + (int)EnrollmentDatas[0].Total));
                            }

                            var ParticipationReading = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 3 && x.MagnetGPRAID == GPRAData.ID);
                            if (ParticipationReading.Count > 0)
                            {
                                if (ParticipationReading[0].Total != null) gpraaf.SetField("29 Reading", ParticipationReading[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Total));
                                if (ParticipationReading[0].Indian != null) gpraaf.SetField("30 Reading", ParticipationReading[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Indian));
                                if (ParticipationReading[0].Asian != null) gpraaf.SetField("31 Reading", ParticipationReading[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Asian));
                                if (ParticipationReading[0].Black != null) gpraaf.SetField("32 Reading", ParticipationReading[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Black));
                                if (ParticipationReading[0].Hispanic != null) gpraaf.SetField("33 Reading", ParticipationReading[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Hispanic));
                                if (ParticipationReading[0].Hawaiian != null) gpraaf.SetField("34 Reading", ParticipationReading[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Hawaiian));
                                if (ParticipationReading[0].White != null) gpraaf.SetField("35 Reading", ParticipationReading[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].White));
                                if (ParticipationReading[0].MultiRaces != null) gpraaf.SetField("47 Reading", ParticipationReading[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].MultiRaces));
                                if (ParticipationReading[0].ExtraFiled2 != null) gpraaf.SetField("36 Reading", ParticipationReading[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].ExtraFiled2));
                                if (ParticipationReading[0].ExtraFiled1 != null) gpraaf.SetField("37 Reading", ParticipationReading[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].ExtraFiled1));
                            }

                            var ParticipationMath = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 4 && x.MagnetGPRAID == GPRAData.ID);
                            if (ParticipationMath.Count > 0)
                            {
                                if (ParticipationMath[0].Total != null) gpraaf.SetField("29 Math", ParticipationMath[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Total));
                                if (ParticipationMath[0].Indian != null) gpraaf.SetField("30 Math", ParticipationMath[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Indian));
                                if (ParticipationMath[0].Asian != null) gpraaf.SetField("31 Math", ParticipationMath[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Asian));
                                if (ParticipationMath[0].Black != null) gpraaf.SetField("32 Math", ParticipationMath[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Black));
                                if (ParticipationMath[0].Hispanic != null) gpraaf.SetField("33 Math", ParticipationMath[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Hispanic));
                                if (ParticipationMath[0].Hawaiian != null) gpraaf.SetField("34 Math", ParticipationMath[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Hawaiian));
                                if (ParticipationMath[0].White != null) gpraaf.SetField("35 Math", ParticipationMath[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].White));
                                if (ParticipationMath[0].MultiRaces != null) gpraaf.SetField("47 Math", ParticipationMath[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].MultiRaces));
                                if (ParticipationMath[0].ExtraFiled2 != null) gpraaf.SetField("36 Math", ParticipationMath[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].ExtraFiled2));
                                if (ParticipationMath[0].ExtraFiled1 != null) gpraaf.SetField("37 Math", ParticipationMath[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].ExtraFiled1));
                            }

                            var ArchievementReading = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 5 && x.MagnetGPRAID == GPRAData.ID);
                            if (ArchievementReading.Count > 0)
                            {
                                if (ArchievementReading[0].Total != null) gpraaf.SetField("38 Reading", ArchievementReading[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Total));
                                if (ArchievementReading[0].Indian != null) gpraaf.SetField("39 Reading", ArchievementReading[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Indian));
                                if (ArchievementReading[0].Asian != null) gpraaf.SetField("40 Reading", ArchievementReading[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Asian));
                                if (ArchievementReading[0].Black != null) gpraaf.SetField("41 Reading", ArchievementReading[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Black));
                                if (ArchievementReading[0].Hispanic != null) gpraaf.SetField("42 Reading", ArchievementReading[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Hispanic));
                                if (ArchievementReading[0].Hawaiian != null) gpraaf.SetField("43 Reading", ArchievementReading[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Hawaiian));
                                if (ArchievementReading[0].White != null) gpraaf.SetField("44 Reading", ArchievementReading[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].White));
                                if (ArchievementReading[0].MultiRaces != null) gpraaf.SetField("48 Reading", ArchievementReading[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].MultiRaces));
                                if (ArchievementReading[0].ExtraFiled2 != null) gpraaf.SetField("45 Reading", ArchievementReading[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].ExtraFiled2));
                                if (ArchievementReading[0].ExtraFiled1 != null) gpraaf.SetField("46 Reading", ArchievementReading[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].ExtraFiled1));
                            }

                            var ArchievementMath = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 6 && x.MagnetGPRAID == GPRAData.ID);
                            if (ArchievementMath.Count > 0)
                            {
                                if (ArchievementMath[0].Total != null) gpraaf.SetField("38 Math", ArchievementMath[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Total));
                                if (ArchievementMath[0].Indian != null) gpraaf.SetField("39 Math", ArchievementMath[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Indian));
                                if (ArchievementMath[0].Asian != null) gpraaf.SetField("40 Math", ArchievementMath[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Asian));
                                if (ArchievementMath[0].Black != null) gpraaf.SetField("41 Math", ArchievementMath[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Black));
                                if (ArchievementMath[0].Hispanic != null) gpraaf.SetField("42 Math", ArchievementMath[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Hispanic));
                                if (ArchievementMath[0].Hawaiian != null) gpraaf.SetField("43 Math", ArchievementMath[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Hawaiian));
                                if (ArchievementMath[0].White != null) gpraaf.SetField("44 Math", ArchievementMath[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].White));
                                if (ArchievementMath[0].MultiRaces != null) gpraaf.SetField("48 Math", ArchievementMath[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].MultiRaces));
                                if (ArchievementMath[0].ExtraFiled2 != null) gpraaf.SetField("45 Math", ArchievementMath[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].ExtraFiled2));
                                if (ArchievementMath[0].ExtraFiled1 != null) gpraaf.SetField("46 Math", ArchievementMath[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].ExtraFiled1));
                            }

                            var PerformanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 7 && x.MagnetGPRAID == GPRAData.ID);
                            if (PerformanceMeasure.Count > 0)
                            {
                                if (PerformanceMeasure[0].Budget != null) gpraaf.SetField("49", "$" + string.Format("{0:#,0.00}", PerformanceMeasure[0].Budget));
                                if (PerformanceMeasure[0].Total != null) gpraaf.SetField("50", ManageUtility.FormatInteger((int)PerformanceMeasure[0].Total));
                            }

                            gpraps.FormFlattening = true;

                            gprar.Close();
                            gpraps.Close();

                            //Add to final report
                            PdfReader gprareader = new PdfReader(TmpPDF);
                            doc.SetPageSize(PageSize.A4);
                            doc.NewPage();
                            PdfImportedPage gpraimportedPage = pdfWriter.GetImportedPage(gprareader, 1);
                            pdfContentByte.AddTemplate(gpraimportedPage, 1.0F, 0, 0, 1.0F, 0, 0);
                            doc.NewPage();
                            gpraimportedPage = pdfWriter.GetImportedPage(gprareader, 2);
                            pdfContentByte.AddTemplate(gpraimportedPage, 1.0F, 0, 0, 1.0F, 0, 0);
                            doc.NewPage();
                            gpraimportedPage = pdfWriter.GetImportedPage(gprareader, 3);
                            pdfContentByte.AddTemplate(gpraimportedPage, 1.0F, 0, 0, 1.0F, 0, 0);
                            gprareader.Close();
                        }
                    }

                    //Budget Summary
                    PdfStamper bsps = null;

                    // read existing PDF document
                    TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                    if (File.Exists(TmpPDF))
                        File.Delete(TmpPDF);
                    TempFiles.Add(TmpPDF);

                    PdfReader bsr = new PdfReader(Server.MapPath("../doc/ed524budget.pdf"));
                    bsps = new PdfStamper(bsr, new FileStream(TmpPDF, FileMode.Create));

                    AcroFields bsaf = bsps.AcroFields;

                    decimal[] BudgetTotal = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    decimal ColumnTotal = 0;
                    decimal ColumnTotalNonFederal = 0;
                    string[] affix = { "a", "b", "c", "d", "e", "f" };

                    int bsidx = 1;
                    int cnn = 1;
                    string summary = "";
                    while (bsidx < 6)
                    {
                        var adhocReports = GranteeReport.Find(x => x.GranteeID == grantee.ID && x.ReportPeriodID == cnn + 1 && x.ReportType == false);
                        var reports = GranteeReport.Find(x => x.GranteeID == grantee.ID && x.ReportPeriodID == cnn && x.ReportType == false);

                        if (reports.Count > 0 || adhocReports.Count > 0)
                        {
                            GranteeReport bsreport;
                            MagnetBudgetInformation MBI;
                            if (adhocReports.Count > 0)
                            {
                                var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == adhocReports[0].ID);
                                MBI = data[0];
                                bsreport = adhocReports[0];
                            }
                            else
                            {
                                var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == reports[0].ID);
                                MBI = data[0];
                                bsreport = reports[0];
                            }
                            if (MBI != null)
                            {
                                ColumnTotal = 0;
                                ColumnTotalNonFederal = 0;
                                decimal dd = 0;

                                summary = MBI.BudgetSummary;

                                //Personnel
                                bsaf.SetField("1" + affix[bsidx - cnn] + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.Personnel));
                                ColumnTotal += (decimal)MBI.Personnel;
                                BudgetTotal[0] += (decimal)MBI.Personnel;
                                bsaf.SetField("B" + bsidx.ToString() + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.PersonnelNF));
                                ColumnTotalNonFederal += (decimal)MBI.PersonnelNF;
                                BudgetTotal[13] += (decimal)MBI.PersonnelNF;

                                //Fringe Benefits
                                dd = (decimal)MBI.Fringe;
                                bsaf.SetField("2" + affix[bsidx - cnn] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[1] += dd;
                                dd = (decimal)MBI.FringeNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[14] += dd;

                                //Travel
                                dd = (decimal)MBI.Travel;
                                bsaf.SetField("3" + affix[bsidx - cnn] + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[2] += dd;
                                dd = (decimal)MBI.TravelNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[15] += dd;

                                //Equipment
                                dd = (decimal)MBI.Equipment;
                                bsaf.SetField("4" + affix[bsidx - cnn] + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[3] += dd;
                                dd = (decimal)MBI.EquipmentNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[16] += dd;

                                //Supplies
                                dd = (decimal)MBI.Supplies;
                                bsaf.SetField("5" + affix[bsidx - cnn] + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[4] += dd;
                                dd = (decimal)MBI.SuppliesNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[17] += dd;

                                //contractual
                                dd = (decimal)MBI.Contractual;
                                bsaf.SetField("6" + affix[bsidx - cnn] + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[5] += dd;
                                dd = (decimal)MBI.ContractualNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[18] += dd;

                                //Construction
                                dd = (decimal)MBI.Construction;
                                bsaf.SetField("7" + affix[bsidx - cnn] + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[6] += dd;
                                dd = (decimal)MBI.ConstructionNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[19] += dd;

                                //Other
                                dd = (decimal)MBI.Other;
                                bsaf.SetField("8" + affix[bsidx - cnn] + " Other", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[7] += dd;
                                dd = (decimal)MBI.OtherNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Other", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[20] += dd;

                                //Total Direct Costs
                                if (ColumnTotal.CompareTo((decimal)0.0) > 0)
                                {
                                    bsaf.SetField("9" + affix[bsidx - cnn] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                    BudgetTotal[8] += ColumnTotal;
                                }

                                if (ColumnTotalNonFederal.CompareTo((decimal)0.0) > 0)
                                {
                                    bsaf.SetField("B" + bsidx.ToString() + " Total Direct Costs", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                    BudgetTotal[21] += ColumnTotalNonFederal;
                                }

                                dd = (decimal)MBI.IndirectCost;
                                bsaf.SetField("10" + affix[bsidx - cnn] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[9] += dd;
                                dd = (decimal)MBI.IndirectCostNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[22] += dd;

                                dd = (decimal)MBI.TrainingStipends;
                                bsaf.SetField("11" + affix[bsidx - cnn] + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotal += dd;
                                BudgetTotal[10] += dd;
                                dd = (decimal)MBI.TrainingStipendsNF;
                                bsaf.SetField("B" + bsidx.ToString() + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                                ColumnTotalNonFederal += dd;
                                BudgetTotal[23] += dd;

                                //Total Costs
                                bsaf.SetField("12" + affix[bsidx - cnn] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                BudgetTotal[11] += ColumnTotal;

                                bsaf.SetField("B" + bsidx.ToString() + " Total Costs Lines", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                BudgetTotal[24] += ColumnTotalNonFederal;

                                //Curent report period?
                                if (cnn == bsreport.ReportPeriodID)
                                {
                                    if (MBI.IndirectCostAgreement != null)
                                    {
                                        if ((bool)MBI.IndirectCostAgreement)
                                            bsaf.SetField("AgreementYes", "Yes");
                                        else
                                            bsaf.SetField("AgreementNo", "Yes");
                                    }

                                    if (!string.IsNullOrEmpty(MBI.AgreementFrom))
                                    {
                                        string[] strs = MBI.AgreementFrom.Split('/');
                                        bsaf.SetField("PeriodFrom1", strs[0]);
                                        bsaf.SetField("PeriodFrom2", strs.Count() >= 2 ? strs[1] : "");
                                        bsaf.SetField("PeriodFrom3", strs.Count() >= 3 ? strs[2] : "");
                                    }
                                    if (!string.IsNullOrEmpty(MBI.AgreementTo))
                                    {
                                        string[] strs = MBI.AgreementTo.Split('/');
                                        bsaf.SetField("PeriodTo1", strs[0]);
                                        bsaf.SetField("PeriodTo2", strs.Count() >= 2 ? strs[1] : "");
                                        bsaf.SetField("PeriodTo3", strs.Count() >= 3 ? strs[2] : "");
                                    }
                                    if (MBI.ApprovalAgency != null)
                                    {
                                        if ((bool)MBI.ApprovalAgency)
                                            bsaf.SetField("Ed", "Yes");
                                        else
                                        {
                                            bsaf.SetField("Other", "Yes");
                                            bsaf.SetField("Other approving agency", MBI.OtherAgency);
                                        }
                                    }
                                    bsaf.SetField("Indirect Cost Rate Percentage", Convert.ToString(MBI.IndirectCostRate));
                                    if (MBI.RestrictedRateProgram != null)
                                    {
                                        if ((bool)MBI.RestrictedRateProgram)
                                            bsaf.SetField("Is it included in ICRA", "Yes");
                                        else
                                            bsaf.SetField("Complies with 34 CFR", "Yes");
                                    }
                                    bsaf.SetField("Restricted Indirect Cost Rate Percentage", Convert.ToString(MBI.RestrictedRate));
                                }
                            }
                        }

                        bsidx+=2;
                        cnn++;
                    }

                    //Total expense
                    bsidx = 6;
                    bsaf.SetField("1" + affix[bsidx - 1] + " Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[0]));
                    bsaf.SetField("2" + affix[bsidx - 1] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[1]));
                    bsaf.SetField("3" + affix[bsidx - 1] + " Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[2]));
                    bsaf.SetField("4" + affix[bsidx - 1] + " Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[3]));
                    bsaf.SetField("5" + affix[bsidx - 1] + " Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[4]));
                    bsaf.SetField("6" + affix[bsidx - 1] + " Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[5]));
                    bsaf.SetField("7" + affix[bsidx - 1] + " Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[6]));
                    bsaf.SetField("8" + affix[bsidx - 1] + " Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[7]));
                    bsaf.SetField("9" + affix[bsidx - 1] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", BudgetTotal[8]));
                    bsaf.SetField("10" + affix[bsidx - 1] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[9]));
                    bsaf.SetField("11" + affix[bsidx - 1] + " Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[10]));
                    bsaf.SetField("12" + affix[bsidx - 1] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", BudgetTotal[11]));

                    //Indirect Cost Information from lastest cover sheet
                    bsaf.SetField("B6 Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[13]));
                    bsaf.SetField("B6 Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[14]));
                    bsaf.SetField("B6 Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[15]));
                    bsaf.SetField("B6 Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[16]));
                    bsaf.SetField("B6 Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[17]));
                    bsaf.SetField("B6 Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[18]));
                    bsaf.SetField("B6 Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[19]));
                    bsaf.SetField("B6 Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[20]));
                    bsaf.SetField("B6 Total Direct Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[21]));
                    bsaf.SetField("B6 Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[22]));
                    bsaf.SetField("B6 Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[23]));
                    bsaf.SetField("B6 Total Costs Lines", "$" + string.Format("{0:#,0.00}", BudgetTotal[24]));

                    bsaf.SetField("InstitutionOrganization", grantee.GranteeName);

                    bsps.FormFlattening = true;

                    bsr.Close();
                    bsps.Close();

                    //Add to final report
                    PdfReader bsreader = new PdfReader(TmpPDF);
                    doc.SetPageSize(PageSize.A4.Rotate());
                    doc.NewPage();
                    PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(bsreader, 1);
                    pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                    doc.NewPage();
                    PdfImportedPage bsimportedPage2 = pdfWriter.GetImportedPage(bsreader, 2);
                    pdfContentByte.AddTemplate(bsimportedPage2, 0, 0);

                    if (!string.IsNullOrEmpty(summary))
                    {
                        //Add budget summary
                        string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(ContentPDF))
                            File.Delete(ContentPDF);
                        TempFiles.Add(ContentPDF);

                        Document bsContentDocument = new Document(PageSize.A4.Rotate(), 40f, 40f, 40f, 40f);
                        PdfWriter bsContentWriter = PdfWriter.GetInstance(bsContentDocument, new FileStream(ContentPDF, FileMode.Create));
                        bsContentDocument.Open();
                        //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                        bsContentDocument.Add(new Paragraph(new Chunk(summary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                        bsContentDocument.Close();

                        //Add to output
                        PdfReader bsfreader2 = new PdfReader(ContentPDF);
                        for (int j = 1; j <= bsfreader2.NumberOfPages; j++)
                        {
                            doc.SetPageSize(PageSize.A4.Rotate());
                            doc.NewPage();
                            PdfImportedPage bsfimportedPage = pdfWriter.GetImportedPage(bsfreader2, j);
                            pdfContentByte.AddTemplate(bsfimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                        }
                        bsfreader2.Close();
                    }

                    doc.NewPage();
                    PdfImportedPage bsimportedPage3 = pdfWriter.GetImportedPage(bsreader, 3);
                    pdfContentByte.AddTemplate(bsimportedPage3, 0, 0);
                    bsreader.Close();

                    //Uploaded budget summary file
                    foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 3))
                    {
                        PdfReader bsuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                        for (int t = 1; t <= bsuploadReader.NumberOfPages; t++)
                        {
                            doc.NewPage();
                            PdfImportedPage bsuploadPage = pdfWriter.GetImportedPage(bsuploadReader, t);
                            pdfContentByte.AddTemplate(bsuploadPage, 0, 0);
                        }
                        bsuploadReader.Close();
                    }

                    //Desegregation
                    doc.SetPageSize(PageSize.A4);

                    var DesegregationData = MagnetDesegregationPlan.Find(x => x.ReportID == ReportID);
                    if (DesegregationData.Count > 0)
                    {
                        PdfStamper desegps = null;

                        // read existing PDF document
                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TempFiles.Add(TmpPDF);

                        PdfReader desegr = new PdfReader(Server.MapPath("../doc/desegregation.pdf"));
                        desegps = new PdfStamper(desegr, new FileStream(TmpPDF, FileMode.Create));
                        AcroFields desegaf = desegps.AcroFields;

                        if (DesegregationData[0].PlanType != null)
                        {
                            if (DesegregationData[0].PlanType == false)
                                desegaf.SetField("RequiredPlan", "Yes");
                            else
                                desegaf.SetField("VoluntaryPlan", "Yes");
                        }
                        desegps.FormFlattening = true;

                        desegr.Close();
                        desegps.Close();

                        //Add to output
                        PdfReader desegfreader2 = new PdfReader(TmpPDF);
                        for (int j = 1; j <= desegfreader2.NumberOfPages; j++)
                        {
                            doc.NewPage();
                            PdfImportedPage desegfimportedPage = pdfWriter.GetImportedPage(desegfreader2, j);
                            pdfContentByte.AddTemplate(desegfimportedPage, 0, 0);
                        }
                        desegfreader2.Close();
                    }

                    //Uploaded Desegregation file
                    foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 4))
                    {
                        PdfReader uploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                        for (int t = 1; t <= uploadReader.NumberOfPages; t++)
                        {
                            doc.NewPage();
                            PdfImportedPage uploadPage = pdfWriter.GetImportedPage(uploadReader, t);
                            pdfContentByte.AddTemplate(uploadPage, 0, 0);
                        }
                        uploadReader.Close();
                    }

                    //Assurance
                    //Uploaded Assurance file
                    foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 5))
                    {
                        PdfReader uploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                        for (int t = 1; t <= uploadReader.NumberOfPages; t++)
                        {
                            doc.NewPage();
                            PdfImportedPage uploadPage = pdfWriter.GetImportedPage(uploadReader, t);
                            pdfContentByte.AddTemplate(uploadPage, 0, 0);
                        }
                        uploadReader.Close();
                    }

                    //Table 8
                    MagnetDBDB db = new MagnetDBDB();
                    var LeaData = from m in db.MagnetSchoolEnrollments
                                  where m.GranteeReportID == ReportID
                                  && m.StageID == 1
                                  orderby m.GradeLevel
                                  select new
                                  {
                                      m.GradeLevel,
                                      AmericanIndian = m.AmericanIndian == null ? 0 : m.AmericanIndian,
                                      Asian = m.Asian == null ? 0 : m.Asian,
                                      AfricanAmerican = m.AfricanAmerican == null ? 0 : m.AfricanAmerican,
                                      Hispanic = m.Hispanic == null ? 0 : m.Hispanic,
                                      Hawaiian = m.Hawaiian == null ? 0 : m.Hawaiian,
                                      White = m.White == null ? 0 : m.White,
                                      MultiRacial = m.MultiRacial == null ? 0 : m.MultiRacial,
                                      GradeTotal = (m.AmericanIndian == null ? 0 : m.AmericanIndian
                                      + m.Asian == null ? 0 : m.Asian
                                      + m.AfricanAmerican == null ? 0 : m.AfricanAmerican
                                      + m.Hispanic == null ? 0 : m.Hispanic
                                      + m.Hawaiian == null ? 0 : m.Hawaiian
                                      + m.White == null ? 0 : m.White
                                      + m.MultiRacial == null ? 0 : m.MultiRacial)
                                  };
                    if (LeaData.Count() > 0)
                    {
                        PdfStamper leaps = null;

                        // read existing PDF document
                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TempFiles.Add(TmpPDF);

                        PdfReader lear = new PdfReader(Server.MapPath("../doc/Tables78.pdf"));
                        leaps = new PdfStamper(lear, new FileStream(TmpPDF, FileMode.Create));
                        AcroFields leaaf = leaps.AcroFields;

                        if (report.FirstTimeMagnetProgram != null && report.FirstTimeMagnetProgram == true)
                            leaaf.SetField("FirstYearProgram", "Yes");

                        int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                        int total = 0;
                        foreach (var Enrollment in LeaData)
                        {
                            total = 0;
                            if (Enrollment.AmericanIndian != null)
                            {
                                total += (int)Enrollment.AmericanIndian;
                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger((int)Enrollment.AmericanIndian));
                            }
                            if (Enrollment.Asian != null)
                            {
                                total += (int)Enrollment.Asian;
                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger((int)Enrollment.Asian));
                            }
                            if (Enrollment.AfricanAmerican != null)
                            {
                                total += (int)Enrollment.AfricanAmerican;
                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican));
                            }
                            if (Enrollment.Hispanic != null)
                            {
                                total += (int)Enrollment.Hispanic;
                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger((int)Enrollment.Hispanic));
                            }
                            if (Enrollment.Hawaiian != null)
                            {
                                total += (int)Enrollment.Hawaiian;
                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger((int)Enrollment.Hawaiian));
                            }
                            if (Enrollment.White != null)
                            {
                                total += (int)Enrollment.White;
                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger((int)Enrollment.White));
                            }
                            if (Enrollment.MultiRacial != null)
                            {
                                total += (int)Enrollment.MultiRacial;
                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger((int)Enrollment.MultiRacial));
                            }
                            leaaf.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger(total));

                            if (total > 0) leaaf.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", (double)Enrollment.AmericanIndian / total * 100));
                            if (total > 0) leaaf.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Asian / total * 100));
                            if (total > 0) leaaf.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", (double)Enrollment.AfricanAmerican / total * 100));
                            if (total > 0) leaaf.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hispanic / total * 100));
                            if (total > 0) leaaf.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hawaiian / total * 100));
                            if (total > 0) leaaf.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", (double)Enrollment.White / total * 100));
                            if (total > 0) leaaf.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", (double)Enrollment.MultiRacial / total * 100));

                            if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                            if (Enrollment.Asian != null) TotalEnrollments[1] += (int)Enrollment.Asian;
                            if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                            if (Enrollment.Hispanic != null) TotalEnrollments[3] += (int)Enrollment.Hispanic;
                            if (Enrollment.Hawaiian != null) TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                            if (Enrollment.White != null) TotalEnrollments[5] += (int)Enrollment.White;
                            if (Enrollment.MultiRacial != null) TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                            TotalEnrollments[7] += total;
                        }

                        leaaf.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                        leaaf.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                        leaaf.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                        leaaf.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                        leaaf.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                        leaaf.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                        leaaf.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                        leaaf.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));

                        leaaf.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                        leaaf.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                        leaaf.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                        leaaf.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                        leaaf.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                        leaaf.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                        leaaf.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));

                        int leaIdx = 1;
                        var SchoolYearData = (from m in db.MagnetSchoolYears
                                              from n in db.MagnetSchools
                                              where m.SchoolID == n.ID
                                              && m.ReportID == ReportID
                                              select new { m.ID, n.SchoolName, m.SchoolYear });
                        foreach (var SchoolYear in SchoolYearData)
                        {
                            leaaf.SetField("School NameRow" + leaIdx.ToString(), SchoolYear.SchoolName);
                            leaaf.SetField("School YearRow" + leaIdx.ToString(), SchoolYear.SchoolYear);
                            leaIdx++;
                        }

                        leaps.FormFlattening = true;

                        lear.Close();
                        leaps.Close();

                        //Add to output
                        PdfReader leafreader2 = new PdfReader(TmpPDF);
                        for (int j = 1; j <= leafreader2.NumberOfPages; j++)
                        {
                            doc.NewPage();
                            PdfImportedPage leaimportedPage = pdfWriter.GetImportedPage(leafreader2, j);
                            pdfContentByte.AddTemplate(leaimportedPage, 1.0F, 0, 0, 1.0F, 0, 0);
                        }
                        leafreader2.Close();
                    }

                    //Table 9
                    foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == grantee.ID).OrderBy(x => x.SchoolName))
                    {
                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TempFiles.Add(TmpPDF);

                        PdfStamper enrollps = null;

                        // Fill out form
                        PdfReader enrollr = new PdfReader(Server.MapPath("../doc/Table9.pdf"));
                        enrollps = new PdfStamper(enrollr, new FileStream(TmpPDF, FileMode.Create));

                        AcroFields enrollaf = enrollps.AcroFields;

                        enrollaf.SetField("SchoolName", School.SchoolName);

                        var EnrollmentData = from m in db.MagnetSchoolEnrollments
                                             where m.GranteeReportID == ReportID
                                             && m.SchoolID == School.ID
                                             && m.StageID == 2//School enrollment data
                                             orderby m.GradeLevel
                                             select new
                                             {
                                                 m.GradeLevel,
                                                 AmericanIndian = m.AmericanIndian == null ? 0 : m.AmericanIndian,
                                                 Asian = m.Asian == null ? 0 : m.Asian,
                                                 AfricanAmerican = m.AfricanAmerican == null ? 0 : m.AfricanAmerican,
                                                 Hispanic = m.Hispanic == null ? 0 : m.Hispanic,
                                                 Hawaiian = m.Hawaiian == null ? 0 : m.Hawaiian,
                                                 White = m.White == null ? 0 : m.White,
                                                 MultiRacial = m.MultiRacial == null ? 0 : m.MultiRacial,
                                                 GradeTotal = (m.AmericanIndian == null ? 0 : m.AmericanIndian
                                                 + m.Asian == null ? 0 : m.Asian
                                                 + m.AfricanAmerican == null ? 0 : m.AfricanAmerican
                                                 + m.Hispanic == null ? 0 : m.Hispanic
                                                 + m.Hawaiian == null ? 0 : m.Hawaiian
                                                 + m.White == null ? 0 : m.White
                                                 + m.MultiRacial == null ? 0 : m.MultiRacial)
                                             };

                        int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                        int total = 0;
                        foreach (var Enrollment in EnrollmentData)
                        {
                            total = 0;
                            if (Enrollment.AmericanIndian != null)
                            {
                                total += (int)Enrollment.AmericanIndian;
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger((int)Enrollment.AmericanIndian));
                            }
                            if (Enrollment.Asian != null)
                            {
                                total += (int)Enrollment.Asian;
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger((int)Enrollment.Asian));
                            }
                            if (Enrollment.AfricanAmerican != null)
                            {
                                total += (int)Enrollment.AfricanAmerican;
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican));
                            }
                            if (Enrollment.Hispanic != null)
                            {
                                total += (int)Enrollment.Hispanic;
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger((int)Enrollment.Hispanic));
                            }
                            if (Enrollment.Hawaiian != null)
                            {
                                total += (int)Enrollment.Hawaiian;
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger((int)Enrollment.Hawaiian));
                            }
                            if (Enrollment.White != null)
                            {
                                total += (int)Enrollment.White;
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger((int)Enrollment.White));
                            }
                            if (Enrollment.MultiRacial != null)
                            {
                                total += (int)Enrollment.MultiRacial;
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger((int)Enrollment.MultiRacial));
                            }
                            enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger(total));

                            if (total > 0) enrollaf.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", (double)Enrollment.AmericanIndian / total * 100));
                            if (total > 0) enrollaf.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Asian / total * 100));
                            if (total > 0) enrollaf.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", (double)Enrollment.AfricanAmerican / total * 100));
                            if (total > 0) enrollaf.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hispanic / total * 100));
                            if (total > 0) enrollaf.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hawaiian / total * 100));
                            if (total > 0) enrollaf.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", (double)Enrollment.White / total * 100));
                            if (total > 0) enrollaf.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", (double)Enrollment.MultiRacial / total * 100));

                            if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                            if (Enrollment.Asian != null) TotalEnrollments[1] += (int)Enrollment.Asian;
                            if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                            if (Enrollment.Hispanic != null) TotalEnrollments[3] += (int)Enrollment.Hispanic;
                            if (Enrollment.Hawaiian != null) TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                            if (Enrollment.White != null) TotalEnrollments[5] += (int)Enrollment.White;
                            if (Enrollment.MultiRacial != null) TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                            if (Enrollment.GradeTotal != null) TotalEnrollments[7] += total;
                        }

                        enrollaf.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                        enrollaf.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                        enrollaf.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                        enrollaf.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                        enrollaf.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                        enrollaf.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                        enrollaf.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                        enrollaf.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));

                        enrollaf.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                        enrollaf.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                        enrollaf.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                        enrollaf.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                        enrollaf.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                        enrollaf.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                        enrollaf.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));

                        enrollps.FormFlattening = true;

                        enrollr.Close();
                        enrollps.Close();

                        //Add to final report
                        PdfReader enrollreader = new PdfReader(TmpPDF);
                        doc.NewPage();
                        PdfImportedPage enrollimportedPage = pdfWriter.GetImportedPage(enrollreader, 1);
                        pdfContentByte.AddTemplate(enrollimportedPage, 0, 0);
                        doc.NewPage();
                        enrollreader.Close();
                    }

                    //Table 11
                    //Create font
                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font Time12Bold = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.BOLD);
                    iTextSharp.text.Font Time10BoldItalic = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLDITALIC);
                    iTextSharp.text.Font Time10Bold = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLD);
                    iTextSharp.text.Font Time10Normal = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font Time8Normal = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL);

                    iTextSharp.text.Rectangle page = new iTextSharp.text.Rectangle(PageSize.A4.Width, PageSize.A4.Height); // doc.PageSize;
                    PdfPTable head = new PdfPTable(2);
                    head.TotalWidth = 100;
                    head.SetWidths(new float[] { 10f, 90f });
                    Phrase phrase = new Phrase("Table 11", Time12Bold);
                    PdfPCell c = new PdfPCell(phrase);
                    c.Colspan = 2;
                    c.MinimumHeight = 40f;
                    c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    c.VerticalAlignment = Element.ALIGN_TOP;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    head.AddCell(c);

                    phrase = new Phrase("Feeder School-(Converted)-Enrollment Data ", Time10Bold);
                    phrase.Add(new Chunk("(LEAs that HAVE converted to new race and ethnic categories)", Time10BoldItalic));
                    c = new PdfPCell(phrase);
                    c.Colspan = 2;
                    c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    c.VerticalAlignment = Element.ALIGN_BOTTOM;
                    c.HorizontalAlignment = Element.ALIGN_LEFT;
                    head.AddCell(c);

                    phrase = new Phrase("•", Time10Bold);
                    c = new PdfPCell(phrase);
                    c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    c.VerticalAlignment = Element.ALIGN_TOP;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    head.AddCell(c);

                    phrase = new Phrase("For each feeder school, identify the magnet school(s) to which the feeder school would send students.  If a feeder school would send students to all magnet schools at a particular grade level (for example, Elementary Feeder School “X” would send students to all of the elementary magnet schools participating in the project, indicate “All” in the “Magnet” column associated with Elementary Feeder School “X”.  ", Time10Normal);
                    c = new PdfPCell(phrase);
                    c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    c.VerticalAlignment = Element.ALIGN_TOP;
                    c.HorizontalAlignment = Element.ALIGN_LEFT;
                    head.AddCell(c);

                    phrase = new Phrase("Use additional sheets, if necessary.", Time10Normal);
                    c = new PdfPCell(phrase);
                    c.Colspan = 2;
                    c.MinimumHeight = 20f;
                    c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    c.VerticalAlignment = Element.ALIGN_BOTTOM;
                    c.HorizontalAlignment = Element.ALIGN_LEFT;
                    head.AddCell(c);
                    doc.Add(head);

                    //Data table
                    PdfPTable DataTable = new PdfPTable(17);
                    DataTable.TotalWidth = 70;
                    DataTable.SetWidths(new float[] { 12f, 13f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f });

                    phrase = new Phrase("Schools", Time10Bold);
                    c = new PdfPCell(phrase);
                    c.Colspan = 2;
                    //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                    DataTable.AddCell(c);

                    phrase = new Phrase("Actual Enrollment as of October 1, 2010 \r\n(Year 1 of Project)", Time10Bold);
                    c = new PdfPCell(phrase);
                    c.Colspan = 15;
                    //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("FEEDER", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("MAGNET(S)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("American Indian /Alaskan Native (Number)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("American Indian /Alaskan Native (%)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Asian (Number)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Asian (%)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Black or African-American (Number) ", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Black or African-American (%) ", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Hispanic/Latino (Number)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Hispanic/Latino (%)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander  (Number)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander (%)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("White (Number)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("White (%)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Two or more races (Number) ", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Two or more races (%)", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    c = new PdfPCell(new Phrase("Total  Students", Time10Bold));
                    c.Rotation = 90;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                    DataTable.AddCell(c);

                    var FeederData = from m in db.MagnetSchoolFeederEnrollments
                                     where m.GranteeReportID == ReportID
                                      && m.StageID == 3
                                     orderby m.GradeLevel
                                     select new
                                     {
                                         m.ID,
                                         m.FeederSchoolID,
                                         m.SchoolID,
                                         m.AmericanIndian,
                                         m.Asian,
                                         m.AfricanAmerican,
                                         m.Hispanic,
                                         m.White,
                                         m.Hawaiian,
                                         m.MultiRacial,
                                         GradeTotal = (m.AmericanIndian + m.Asian + m.Hispanic + m.Hawaiian + m.White + m.MultiRacial + m.AfricanAmerican)
                                     };

                    foreach (var Enrollment in FeederData)
                    {
                        MagnetFeederSchool feederSchool = MagnetFeederSchool.SingleOrDefault(x => x.ID == (int)(Enrollment.FeederSchoolID));
                        c = new PdfPCell(new Phrase(feederSchool.SchoolName, Time10Normal));
                        c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                        DataTable.AddCell(c);

                        if (!string.IsNullOrEmpty(Enrollment.SchoolID))
                        {
                            string strSchoolName = "";
                            if (Enrollment.SchoolID.Equals("999999"))
                            {
                                strSchoolName = "All";
                            }
                            else
                            {
                                foreach (string magnetSchoolID in Enrollment.SchoolID.Split(';'))
                                {
                                    MagnetSchool magnetSchool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(magnetSchoolID));
                                    strSchoolName += string.IsNullOrEmpty(strSchoolName) ? magnetSchool.SchoolName : "\r\n" + magnetSchool.SchoolName;
                                }
                            }
                            c = new PdfPCell(new Phrase(strSchoolName, Time10Normal));
                            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                            DataTable.AddCell(c);
                        }
                        else
                        {
                            c = new PdfPCell(new Phrase("", Time10Normal));
                            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                            DataTable.AddCell(c);
                        }

                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.AmericanIndian), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.AmericanIndian / Enrollment.GradeTotal * 100), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Asian), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.Asian / Enrollment.GradeTotal * 100), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.AfricanAmerican / Enrollment.GradeTotal * 100), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Hispanic), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.Hispanic / Enrollment.GradeTotal * 100), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Hawaiian), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.Hawaiian / Enrollment.GradeTotal * 100), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.White), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.White / Enrollment.GradeTotal * 100), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.MultiRacial), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.MultiRacial / Enrollment.GradeTotal * 100), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.GradeTotal), Time8Normal)));
                    }

                    doc.Add(DataTable);

                    //Ed approved file
                    foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 20))
                    {
                        PdfReader EDApproveduploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                        for (int t = 1; t <= EDApproveduploadReader.NumberOfPages; t++)
                        {
                            doc.NewPage();
                            PdfImportedPage EDApproveduploadPage = pdfWriter.GetImportedPage(EDApproveduploadReader, t);
                            pdfContentByte.AddTemplate(EDApproveduploadPage, 0, 0);
                        }
                        EDApproveduploadReader.Close();
                    }

                    doc.Close();

                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_FinalReport.pdf");
                    Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                    Response.OutputStream.Flush();
                    Response.OutputStream.Close();
                }
            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("Combine report:", ex);
            }
            finally
            {
                foreach (string FileName in TempFiles)
                {
                    if (File.Exists(FileName))
                        File.Delete(FileName);
                }
            }
        }
    }
}