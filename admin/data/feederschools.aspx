﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="feederschools.aspx.cs" Inherits="admin_data_feederschools" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Feeder Schools
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="mainContent">
        <h1>
            Manage Feeder Schools</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
       <%-- <p style="text-align: left">
                   </p>--%>
        <table>
        <tr>
        <td colspan="2">
        <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" onselectedindexchanged="rbtnlstCohort_SelectedIndexChanged" >
                <asp:ListItem Value="1"  >MSAP 2010 Cohort</asp:ListItem>
                <asp:ListItem Value="2" >MSAP 2013 Cohort</asp:ListItem>
        </asp:RadioButtonList>
        </td>
        </tr>
        <tr>
        <td> <asp:Button ID="Newbutton" runat="server" Text="New School" CssClass="msapBtn" Visible="false" OnClick="OnAdd" />
</td>
         <td><asp:DropDownList ID="ddlGrantees" runat="server" AutoPostBack="True" 
                onselectedindexchanged="ddlGrantees_SelectedIndexChanged" EnableViewState="true" >
         </asp:DropDownList>
        </td>
        </tr>
        </table>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            >
            <Columns>
                <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School Name" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:Label ID="lblNorecId" Visible="false" runat="server" Text="Sorry! Requested Records Are Not Found Please select With other grantee from dropdown list."  Font-Bold="true" ForeColor="Brown" Font-Names="Arial" Font-Size="Medium"></asp:Label>
        <asp:HiddenField ID="hfID" runat="server" />
    <%-- Panel --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit School</div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                School Name:
                            </td>
                            <td>
                                <asp:TextBox ID="txtSchoolName" runat="server" MaxLength="250" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                            </td>
                            <td>
                                <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button14" /> 
                    <asp:AsyncPostBackTrigger ControlID="Button15" EventName="Click" /> 
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
