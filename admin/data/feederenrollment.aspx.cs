﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using Telerik.Web.UI;
using System.Data;

public partial class admin_report_feederenrollment : System.Web.UI.Page
{
    int reportyear = 1;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (Session["ReportYear"] != null)
            reportyear = Convert.ToInt32(Session["ReportYear"]);
        Button1.Attributes.Add("onclick", "return Reset();");
        if (!Page.IsPostBack)
        {
            
            hfStageID.Value = "3";
            if (Session["ReportID"] != null)
            {
                hfReportID.Value = Convert.ToString(Session["ReportID"]);
            }
            else
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }

      
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
            var mschools = MagnetSchool.Find(x=>x.GranteeID==report.GranteeID && x.ReportYear==reportyear && x.isActive==false);
            var enrollments = MagnetSchoolFeederEnrollment.Find(m => m.GranteeReportID == Convert.ToInt32(hfReportID.Value) && m.StageID == 3);

            string strIDs = "";
            string[] deactiveshools = mschools.Select(x => x.ID.ToString()).ToArray();
            
            int count = 0;

            foreach (MagnetSchool mgschool in mschools)
            {
            
                foreach (MagnetSchoolFeederEnrollment mfenroll in enrollments)
                {
                    if (mgschool.ID.ToString() == mfenroll.SchoolID)
                        strIDs += mfenroll.ID + ";";
                    else
                    {
                        foreach (string meId in mfenroll.SchoolID.Split(';'))
                        {
                            if (!deactiveshools.Contains(meId))
                                break;
                            count++;
                        }
                        if (mfenroll.SchoolID.Split(';').Count()==count)
                            strIDs += mfenroll.ID + ";";
                        count = 0;
                    }
                }
                
            }
            strIDs = strIDs.TrimEnd(';');

                        
            
            if (string.IsNullOrEmpty(strIDs))
                strIDs = "998899"; //keep sql happy

            hfGranteeID.Value = Convert.ToString(report.GranteeID);
            SqlDataSource1.SelectParameters["GranteeReportID"].DefaultValue = Convert.ToString(Session["ReportID"]);
            SqlDataSource1.SelectParameters["StageID"].DefaultValue = "3";
            SqlDataSource1.SelectParameters["SchoolIDs"].DefaultValue = strIDs;
            SqlDataSource2.SelectParameters["GranteeID"].DefaultValue = Convert.ToString(report.GranteeID);
            SqlDataSource3.SelectParameters["GranteeID"].DefaultValue = Convert.ToString(report.GranteeID);

            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
            lblYear.Text = period.ReportPeriod.Substring(0, 4);
            lblNumberYear.Text = Convert.ToString(period.reportYear);
        }
    }
    protected void OnSchoolSelected(object sender, EventArgs e)
    {
        CheckBoxList cbl = sender as CheckBoxList;
        if (cbl.Items[cbl.Items.Count - 1].Value.Equals("999999") && cbl.Items[cbl.Items.Count - 1].Selected)
        {
            //Last one, all school
            foreach (System.Web.UI.WebControls.ListItem item in cbl.Items)
            {
                if (item.Selected && !item.Value.Equals("999999"))
                    item.Selected = false;
            }
        }
    }
    protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridHeaderItem)
        {
            InitRealMcCoyGridHeader();
        }
    }
    protected void RadGrid1_DeleteCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        //Get the GridDataItem of the RadGrid        
        GridDataItem item = (GridDataItem)e.Item;
        //Get the primary key value using the DataKeyValue.       
        string itemID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ID"].ToString();
        MagnetSchoolFeederEnrollment.Delete(x => x.ID == Convert.ToInt32(itemID));
        UpdateRecord();
        RadGrid1.Rebind();
    }
    protected void RadGrid1_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        GridEditableItem item = (GridEditableItem)e.Item;
        //Get the primary key value using the DataKeyValue.       
        string itemID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ID"].ToString();
        MagnetSchoolFeederEnrollment SchoolData = MagnetSchoolFeederEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(itemID) );
        string FeederSchool = (item.FindControl("ddlFeederSchool") as RadComboBox).SelectedValue;
        SchoolData.FeederSchoolID = Convert.ToInt32(FeederSchool);
        CheckBoxList cbl = item.FindControl("cklMagnetSchool") as CheckBoxList;
        string MagnetSchool = "";
        foreach (System.Web.UI.WebControls.ListItem li in cbl.Items)
        {
            if (li.Selected)
            {
                MagnetSchool += string.IsNullOrEmpty(MagnetSchool) ? li.Value : ";" + li.Value ;
            }
        }
        SchoolData.SchoolID = MagnetSchool;
        string strData = (item.FindControl("txtIndian") as RadNumericTextBox).Text;
        SchoolData.AmericanIndian = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtAsian") as RadNumericTextBox).Text;
        SchoolData.Asian = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtBlack") as RadNumericTextBox).Text;
        SchoolData.AfricanAmerican = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtHispanic") as RadNumericTextBox).Text;
        SchoolData.Hispanic = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtWhite") as RadNumericTextBox).Text;
        SchoolData.White = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtHawaiian") as RadNumericTextBox).Text;
        SchoolData.Hawaiian = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtMultiRaces") as RadNumericTextBox).Text;
        SchoolData.MultiRacial = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        SchoolData.Save();
        UpdateRecord();
        RadGrid1.MasterTableView.ClearEditItems(); 
        RadGrid1.Rebind();
    }
    protected void RadGrid1_InsertCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        GridEditableItem item = (GridEditableItem)e.Item;
        MagnetSchoolFeederEnrollment SchoolData = new MagnetSchoolFeederEnrollment();
        SchoolData.GranteeReportID = Convert.ToInt32(hfReportID.Value);
        SchoolData.StageID = 3;
        string FeederSchool = (item.FindControl("ddlFeederSchool") as RadComboBox).SelectedValue;
        SchoolData.FeederSchoolID = Convert.ToInt32(FeederSchool);
        CheckBoxList cbl = item.FindControl("cklMagnetSchool") as CheckBoxList;
        string MagnetSchool = "";
        foreach (System.Web.UI.WebControls.ListItem li in cbl.Items)
        {
            if (li.Selected)
            {
                MagnetSchool += string.IsNullOrEmpty(MagnetSchool) ? li.Value : ";" + li.Value;
            }
        }
        SchoolData.SchoolID = MagnetSchool;
        string strData = (item.FindControl("txtIndian") as RadNumericTextBox).Text;
        SchoolData.AmericanIndian = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtAsian") as RadNumericTextBox).Text;
        SchoolData.Asian = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtBlack") as RadNumericTextBox).Text;
        SchoolData.AfricanAmerican = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtHispanic") as RadNumericTextBox).Text;
        SchoolData.Hispanic = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtWhite") as RadNumericTextBox).Text;
        SchoolData.White = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtHawaiian") as RadNumericTextBox).Text;
        SchoolData.Hawaiian = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        strData = (item.FindControl("txtMultiRaces") as RadNumericTextBox).Text;
        SchoolData.MultiRacial = string.IsNullOrEmpty(strData) ? (int?)null : Convert.ToInt32(strData);
        SchoolData.Save();
        Session["ItemInserted"] = "True";
        UpdateRecord();
        RadGrid1.Rebind();
    }
    protected void RadGrid1_PreRender(object sender, EventArgs e)
    {
        if (Session["ItemInserted"] != null && Session["ItemInserted"] == "True")
        {
            RadGrid1.EditIndexes.Clear();
            RadGrid1.MasterTableView.IsItemInserted = false;
            RadGrid1.MasterTableView.Rebind();
            Session["ItemInserted"] = "False";
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("Product with ID " + id + " is updated!");
        }
    }

    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("New product is inserted!");
        }
    }

    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("Product with ID " + id + " is deleted!");
        }
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {

        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            GridEditableItem item = (GridEditableItem)e.Item;
            LinkButton editLink = (LinkButton)item.FindControl("btnUpdate");
            LinkButton cancelLink = (LinkButton)item.FindControl("btnCancel");
            if ( editLink != null)
            {
                editLink.Attributes["onclick"] ="return Reset();";
            }
            if (cancelLink != null)
            {
                cancelLink.Attributes["onclick"] = "return Reset();";
            }

            List<string> ids = new List<string>();
            GridEditFormItem editFormItem = e.Item as GridEditFormItem;
            if (editFormItem.OwnerTableView.DataKeyValues.Count > 0 && editFormItem.ItemIndex > -1)
            {
                string itemID = editFormItem.OwnerTableView.DataKeyValues[editFormItem.ItemIndex]["ID"].ToString();
                MagnetSchoolFeederEnrollment SchoolData = MagnetSchoolFeederEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(itemID));
                if (!string.IsNullOrEmpty(SchoolData.SchoolID))
                {
                    foreach (string str in SchoolData.SchoolID.Split(';'))
                        ids.Add(str);
                }
            }
            CheckBoxList cklMagnetSchool = editFormItem.FindControl("cklMagnetSchool") as CheckBoxList;
            int idx = 0;
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == Convert.ToInt32(hfGranteeID.Value) && x.ReportYear==reportyear && x.isActive).OrderBy(x => x.SchoolName))
            {
                idx++;
                System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem(school.SchoolName, school.ID.ToString());
                if (ids.Contains(school.ID.ToString())) li.Selected = true;
                cklMagnetSchool.Items.Add(li);
            }
            if (idx > 1)
            {
                System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem("All", "999999");
                if (ids.Contains("999999")) li.Selected = true;
                cklMagnetSchool.Items.Add(li);
            }
        }
    }
    private void UpdateRecord()
    {
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 10);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 10;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        using (System.IO.MemoryStream Output = new MemoryStream())
        {
            //Print
            Document document = new Document(PageSize.A4, 10, 10, 40, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, Output);
            document.Open();
            PdfContentByte pdfContentByte = pdfWriter.DirectContent;

            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));

            //Create font
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font Time12Bold = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font Time10BoldItalic = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLDITALIC);
            iTextSharp.text.Font Time10Bold = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font Time10Normal = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font Time8Normal = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL);

            iTextSharp.text.Rectangle page = document.PageSize;
            PdfPTable head = new PdfPTable(2);
            head.TotalWidth = PageSize.A4.Width - 50; //100;
            head.LockedWidth = true;
            head.SetWidths(new float[] { 10f, 90f });
            Phrase phrase = new Phrase("Table 4", Time12Bold);
            PdfPCell c = new PdfPCell(phrase);
            c.Colspan = 2;
            c.MinimumHeight = 40f;
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            //head.AddCell(c);

            phrase = new Phrase("Table 4: Feeder School - Enrollment Data ", Time10Bold);
            //phrase.Add(new Chunk("(LEAs that HAVE converted to new race and ethnic categories)", Time10BoldItalic));
            c = new PdfPCell(phrase);
            c.Colspan = 2;
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_BOTTOM;
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            head.AddCell(c);

            phrase = new Phrase("•", Time10Bold);
            c = new PdfPCell(phrase);
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            head.AddCell(c);

            phrase = new Phrase("For each feeder school, identify the magnet school(s) to which the feeder school would send students.  If a feeder school would send students to all magnet schools at a   particular grade level (for example, Elementary Feeder School “X” would send students to all of the elementary magnet schools participating in the project, indicate “All” in the “Magnet” column associated with Elementary Feeder School “X”). ", Time10Normal);
            c = new PdfPCell(phrase);
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            head.AddCell(c);

            phrase = new Phrase("•", Time10Bold);
            c = new PdfPCell(phrase);
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            head.AddCell(c);

            phrase = new Phrase("Use additional sheets, if necessary.", Time10Normal);
            c = new PdfPCell(phrase);
            c.Colspan = 2;
            c.MinimumHeight = 20f;
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            head.AddCell(c);
            document.Add(head);

            //Data table
            PdfPTable DataTable = new PdfPTable(17);
            DataTable.TotalWidth = PageSize.A4.Width - 50; //100;
            DataTable.LockedWidth = true;
            DataTable.SetWidths(new float[] { 12f, 13f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f });

            phrase = new Phrase("Schools", Time10Bold);
            c = new PdfPCell(phrase);
            c.Colspan = 2;
            //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
            DataTable.AddCell(c);

            string ds = "Actual Enrollment as of October 1, ";
            ds += MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID).ReportPeriod.Substring(0, 4);
            ds += "\r\n";
            //ds += "(Year ";
            //ds += MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID).reportYear;
            //ds += " of Project)";
            ds += "(Current School Year)";

            //phrase = new Phrase("Actual Enrollment as of October 1, 2010 \r\n(Year 1 of Project)", Time10Bold);
            phrase = new Phrase(ds, Time10Bold);
            
            c = new PdfPCell(phrase);
            c.Colspan = 15;
            //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("FEEDER", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("MAGNET(S)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("American Indian /Alaskan Native (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("American Indian /Alaskan Native (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Asian (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Asian (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Black or African-American (Number) ", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Black or African-American (%) ", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Hispanic/Latino (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Hispanic/Latino (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander  (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("White (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("White (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Two or more races (Number) ", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Two or more races (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Total  Students", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            MagnetDBDB db = new MagnetDBDB();
            //Reorder data
            db.spp_feederenrollment_order(Convert.ToInt32(hfReportID.Value), 3).Execute();

            var data = from m in db.MagnetSchoolFeederEnrollments
                       where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                        && m.StageID == 3
                       orderby m.TotalEnrollment
                       select new
                       {
                           m.ID,
                           m.FeederSchoolID,
                           m.SchoolID,
                           m.AmericanIndian,
                           m.Asian,
                           m.AfricanAmerican,
                           m.Hispanic,
                           m.White,
                           m.Hawaiian,
                           m.MultiRacial,
                           
                         GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                            + (m.Asian == null ? 0 : m.Asian)
                            + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                            + (m.Hispanic == null ? 0 : m.Hispanic)
                            + (m.Hawaiian == null ? 0 : m.Hawaiian)
                            + (m.White == null ? 0 : m.White)
                            + (m.MultiRacial == null ? 0 : m.MultiRacial))
                       };

            List<int> excludelist = new List<int>();
            int count=0;
            //GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
            var mschools = MagnetSchool.Find(x=>x.GranteeID==report.GranteeID && x.ReportYear==reportyear && x.isActive==false);
            var enrollments = MagnetSchoolFeederEnrollment.Find(m => m.GranteeReportID == Convert.ToInt32(hfReportID.Value) && m.StageID == 3);

            string[] deactiveshools = mschools.Select(x => x.ID.ToString()).ToArray();

            foreach (MagnetSchool school in MagnetSchool.Find(x =>x.GranteeID==report.GranteeID && x.ReportYear == reportyear && !x.isActive))
            {
                foreach (var item in data)
                {

                    if (item.SchoolID == school.ID.ToString() && !excludelist.Exists(element => element == item.ID))
                    {
                        excludelist.Add(item.ID);
                    }
                    else
                    {
                        foreach (string meId in item.SchoolID.Split(';'))
                        {
                            if (!deactiveshools.Contains(meId))
                                break;
                            count++;
                        }
                        if (item.SchoolID.Split(';').Count()==count)
                            excludelist.Add(item.ID);
                        count = 0;
                    }
                }

            }

            foreach (var Enrollment in data)
            {
                if (!excludelist.Exists(element => element == Enrollment.ID))
                {
                    MagnetFeederSchool feederSchool = MagnetFeederSchool.SingleOrDefault(x => x.ID == (int)(Enrollment.FeederSchoolID));
                    c = new PdfPCell(new Phrase(feederSchool==null ? "" : feederSchool.SchoolName, Time10Normal));
                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                    DataTable.AddCell(c);

                    if (!string.IsNullOrEmpty(Enrollment.SchoolID))
                    {
                        string strSchoolName = "";
                        if (Enrollment.SchoolID.Equals("999999"))
                        {
                            strSchoolName = "All";
                        }
                        else
                        {
                            foreach (string magnetSchoolID in Enrollment.SchoolID.Split(';'))
                            {
                                MagnetSchool magnetSchool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(magnetSchoolID) && x.isActive);
                                if(magnetSchool!=null)
                                    strSchoolName += string.IsNullOrEmpty(strSchoolName) ? magnetSchool.SchoolName : "\r\n" + magnetSchool.SchoolName;
                            }
                        }
                        c = new PdfPCell(new Phrase(strSchoolName, Time10Normal));
                        c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                        DataTable.AddCell(c);
                    }
                    else
                    {
                        c = new PdfPCell(new Phrase("", Time10Normal));
                        c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                        DataTable.AddCell(c);
                    }

                    if (Enrollment.AmericanIndian != null)
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.AmericanIndian), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal==0?0:(double)Enrollment.AmericanIndian / Enrollment.GradeTotal * 100), Time8Normal)));
                    }
                    else
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase("")));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                    }

                    if (Enrollment.Asian != null)
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Asian), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : (double)Enrollment.Asian / Enrollment.GradeTotal * 100), Time8Normal)));
                    }
                    else
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase("")));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                    }
                    if (Enrollment.AfricanAmerican != null)
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : (double)Enrollment.AfricanAmerican / Enrollment.GradeTotal * 100), Time8Normal)));
                    }
                    else
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase("")));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                    }
                    if (Enrollment.Hispanic != null)
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Hispanic), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : (double)Enrollment.Hispanic / Enrollment.GradeTotal * 100), Time8Normal)));
                    }
                    else
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase("")));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                    }

                    if (Enrollment.Hawaiian != null)
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Hawaiian), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : (double)Enrollment.Hawaiian / Enrollment.GradeTotal * 100), Time8Normal)));
                    }
                    else
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase("")));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                    }
                    if (Enrollment.White != null)
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.White), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : (double)Enrollment.White / Enrollment.GradeTotal * 100), Time8Normal)));
                    }
                    else
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase("")));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                    }
                    if (Enrollment.MultiRacial != null)
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.MultiRacial), Time8Normal)));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : (double)Enrollment.MultiRacial / Enrollment.GradeTotal * 100), Time8Normal)));
                    }
                    else
                    {
                        DataTable.AddCell(new PdfPCell(new Phrase("")));
                        DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                    }
                    if(Enrollment.GradeTotal!=null)
                        DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.GradeTotal), Time8Normal)));
                    else
                        DataTable.AddCell(new PdfPCell(new Phrase("0",Time8Normal)));
                }
            }

            document.Add(DataTable);

            //Close document
            document.Close();

            int granteeID = Convert.ToInt32(hfGranteeID.Value);
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_Table_4.pdf");
            Response.OutputStream.Write(Output.GetBuffer(), 0, Output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            Output.Close();
        }
    }
    private IWGridItem[] GetRealMcCoyHeaders(int row)
    {
        ArrayList c = new ArrayList();
        IWGridItem hdr;

        string hdrtop = "";
        switch (row)
        {
            case 0:
                hdr = new IWGridItem("Header0L1", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header1L1", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("FeederL1", 0, 1, "Feeder School", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MagnetL1", 0, 1, "Magnet School", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AmericanIndianL1", 0, 2, "American Indian or Alaska Native", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AsianL1", 0, 2, "Asian", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AfricanAmericanL1", 0, 2, "Black or African-American", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HispanicL1", 0, 2, "Hispanic or Latino", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HawaiianL1", 0, 2, "Native Hawaiian or Other Pacific Islander", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("WhiteL1", 0, 2, "White", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MultiRacialL1", 0, 2, "Two or more races", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Total", 0, 1, "Total Students", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header2L1", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                /*hdr = new IWGridItem("Header3L1", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);*/
                break;
            case 1:
                hdrtop = "rgHeader2";
                hdr = new IWGridItem("Header0L2", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header1L2", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("FeederL2", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MagnetL2", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AmericanIndianL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AmericanIndianL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AsianL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AsianL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AfricanAmericanL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AfricanAmericanL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HispanicL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HispanicL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HawaiianL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HawaiianL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("WhiteL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("WhiteL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MultiRacialL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MultiRacialL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Total", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header2L2", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                /*hdr = new IWGridItem("Header3L2", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);*/
                break;
        }
        return (IWGridItem[])c.ToArray(typeof(IWGridItem));
    }

    private void InitRealMcCoyGridHeader()
    {
        IWGridItem[] row1hdrs = GetRealMcCoyHeaders(0);
        IWGridItem[] row2hdrs = GetRealMcCoyHeaders(1);

        //get the current header   
        GridItem[] header = RadGrid1.MasterTableView.GetItems(GridItemType.Header);

        //get the current THead element   
        if (header.Length != 0)
        {
            GridTHead head = ((GridTHead)header[0].Parent.Controls[0].Parent);
            //Clear all GridHeaderItems   
            head.Controls.RemoveAt(1);

            //create a new GridHeaderItem which will be the new row   
            GridHeaderItem hdr1Item = new GridHeaderItem(RadGrid1.MasterTableView, 0, 0);
            GridHeaderItem hdr2Item = new GridHeaderItem(RadGrid1.MasterTableView, 0, 0);

            int hdr1indx = 0; // logical column index  
            int hdr2indx = 0;
            int hdr1colindx = 0; // Physical column index  
            int hdr2colindx = 0;
            int hdr1nextcol = 0; // Next logical column  
            int hdr2nextcol = 0;
            int hdr1totalcols = 0;
            int hdr2totalcols = 0;
            int hdr1colcount = 0;
            int hdr2colcount = 0;

            foreach (IWGridItem hdr in row1hdrs)
            {
                hdr1totalcols += hdr.ColumnSpan;
            }

            foreach (IWGridItem hdr in row2hdrs)
            {
                hdr2totalcols += hdr.ColumnSpan;
            }

            do
            {
                GridTableHeaderCell cell;

                if ((hdr1colcount >= hdr1totalcols) && (hdr2colcount >= hdr2totalcols))
                {
                    break;
                }

                if (hdr1indx >= hdr1nextcol)
                {
                    cell = new GridTableHeaderCell
                    {
                        Text = row1hdrs[hdr1colindx].Text,
                        ColumnSpan = row1hdrs[hdr1colindx].ColumnSpan,
                        HorizontalAlign = row1hdrs[hdr1colindx].HorizontalAlign,
                        CssClass = row1hdrs[hdr1colindx].ClassOverride
                    };

                    hdr1Item.Cells.Add(cell);
                    hdr1nextcol = hdr1indx + row1hdrs[hdr1colindx].ColumnSpan;
                    hdr1colindx += 1;
                }

                hdr1colcount += 1;
                hdr1indx += 1;

                if (hdr2indx >= hdr2nextcol)
                {
                    cell = new GridTableHeaderCell
                    {
                        Text = row2hdrs[hdr2colindx].Text,
                        ColumnSpan = row2hdrs[hdr2colindx].ColumnSpan,
                        HorizontalAlign = row2hdrs[hdr2colindx].HorizontalAlign,
                        CssClass = row2hdrs[hdr2colindx].ClassOverride
                    };

                    hdr2Item.Cells.Add(cell);
                    hdr2nextcol = hdr2indx + row2hdrs[hdr2colindx].ColumnSpan;
                    hdr2colindx += 1;
                }

                hdr2colcount += 1;
                hdr2indx += 1;
            } while (true);

            //Add back the GridHeaderItems in the order you want them to appear   
            head.Controls.Add(hdr1Item);
            head.Controls.Add(hdr2Item);
        }
    }
    protected void RadGrid1_CustomAggregate(object sender, GridCustomAggregateEventArgs e)
    {
        string field = e.Column.UniqueName;


        GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        
        MagnetDBDB db = new MagnetDBDB();


        var Data = from m in db.MagnetSchoolFeederEnrollments
                   where m.GranteeReportID == Convert.ToInt32(Session["ReportID"])
                   && m.StageID == 3 //School enrollment data
                   orderby m.GradeLevel
                   select new
                   {
                       m.SchoolID,
                       m.ID,
                       m.GradeLevel,
                       AmericanIndian = m.AmericanIndian == null ? 0 : m.AmericanIndian,
                       Asian = m.Asian == null ? 0 : m.Asian,
                       AfricanAmerican = m.AfricanAmerican == null ? 0 : m.AfricanAmerican,
                       Hispanic = m.Hispanic == null ? 0 : m.Hispanic,
                       Hawaiian = m.Hawaiian == null ? 0 : m.Hawaiian,
                       White = m.White == null ? 0 : m.White,
                       MultiRacial = m.MultiRacial == null ? 0 : m.MultiRacial,
                       GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                       + (m.Asian == null ? 0 : m.Asian)
                       + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                       + (m.Hispanic == null ? 0 : m.Hispanic)
                       + (m.Hawaiian == null ? 0 : m.Hawaiian)
                       + (m.White == null ? 0 : m.White)
                       + (m.MultiRacial == null ? 0 : m.MultiRacial))
                   };

        List<int> excludelist = new List<int>();
        int count = 0;
        //GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        var mschools = MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive == false);
        var enrollments = MagnetSchoolFeederEnrollment.Find(m => m.GranteeReportID == Convert.ToInt32(hfReportID.Value) && m.StageID == 3);

        string[] deactiveshools = mschools.Select(x => x.ID.ToString()).ToArray();

        foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && !x.isActive))
        {
            foreach (var item in Data)
            {

                if (item.SchoolID == school.ID.ToString() && !excludelist.Exists(element => element == item.ID))
                {
                    excludelist.Add(item.ID);
                }
                else
                {
                    foreach (string meId in item.SchoolID.Split(';'))
                    {
                        if (!deactiveshools.Contains(meId))
                            break;
                        count++;
                    }
                    if (item.SchoolID.Split(';').Count() == count)
                        excludelist.Add(item.ID);
                    count = 0;
                }
            }

        }
        int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
        int total = 0;
        foreach (var Enrollment in Data)
        {
            if (!excludelist.Exists(element => element == Enrollment.ID))
            {
                //total = 0; 
                if (Enrollment.AmericanIndian != null)
                {
                    total += (int)Enrollment.AmericanIndian;
     
                }
                if (Enrollment.Asian != null)
                {
                    total += (int)Enrollment.Asian;
      
                }
                if (Enrollment.AfricanAmerican != null)
                {
                    total += (int)Enrollment.AfricanAmerican;
        
                }
                if (Enrollment.Hispanic != null)
                {
                    total += (int)Enrollment.Hispanic;
                   
                }
                if (Enrollment.Hawaiian != null)
                {
                    total += (int)Enrollment.Hawaiian;
                    
                }
                if (Enrollment.White != null)
                {
                    total += (int)Enrollment.White;
                   
                }
                if (Enrollment.MultiRacial != null)
                {
                    total += (int)Enrollment.MultiRacial;
                    
                }

                if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                else
                    TotalEnrollments[0] += 0;

                if (Enrollment.Asian != null) TotalEnrollments[1] += (int)Enrollment.Asian;
                if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                if (Enrollment.Hispanic != null) TotalEnrollments[3] += (int)Enrollment.Hispanic;
                if (Enrollment.Hawaiian != null) TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                if (Enrollment.White != null) TotalEnrollments[5] += (int)Enrollment.White;
                if (Enrollment.MultiRacial != null) TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                if (Enrollment.GradeTotal != null) TotalEnrollments[7] = total;
            }
        }


        if (total > 0)
        {
            switch (field)
            {

                case "P1":
                    e.Result = ((double)TotalEnrollments[0] / TotalEnrollments[7] * 100); //AmericanIndia
                    break;

                case "P2":
                    e.Result = ((double)TotalEnrollments[1] / TotalEnrollments[7] * 100);//Asian
                    break;

                case "P3":
                    e.Result = ((double)TotalEnrollments[2] / TotalEnrollments[7] * 100);//AfricaAmerica
                    break;

                case "P4":
                    e.Result = ((double)TotalEnrollments[3] / TotalEnrollments[7] * 100); //Hispanic
                    break;

                case "P5":
                    e.Result = ((double)TotalEnrollments[5] / TotalEnrollments[7] * 100); //Hawaiian
                    break;

                case "P6":
                    e.Result = ((double)TotalEnrollments[4] / TotalEnrollments[7] * 100); //White
                    break;

                case "P7":
                    e.Result = ((double)TotalEnrollments[6] / TotalEnrollments[7] * 100); //MultiRace
                    break;

            }
        }
        else
            e.Result = 0;
    }
}