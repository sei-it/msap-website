﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_contractsbudget : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/default.aspx");
            }
            if (Request.QueryString["type"].Equals("1"))
            {
                hfReportType.Value = "13";
                lblBudgetType.Text = "Federal Funds";
            }
            else
            {
                hfReportType.Value = "14";
                lblBudgetType.Text = "Non Federal Funds";
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            //Consultant
            var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == Convert.ToInt32(hfReportType.Value));
            if (data.Count == 1)
            {
                hfSummaryID.Value = data[0].ID.ToString();
                txtSummary.Text = data[0].ReportSummary;
            }
            RegisterHelper();
            LoadData();
        }
    }
    private void RegisterHelper()
    {
        GridViewHelper helper = new GridViewHelper(this.GridView1);
        helper.RegisterSummary("ApprovedFederalFunds", SummaryOperation.Sum);
        helper.RegisterSummary("BudgetExpenditures", SummaryOperation.Sum);
        helper.RegisterSummary("Carryover", SummaryOperation.Sum);
        
        GridViewHelper helper2 = new GridViewHelper(this.GridView2);
        helper2.RegisterSummary("ApprovedFederalFunds", SummaryOperation.Sum);
        helper2.RegisterSummary("BudgetExpenditures", SummaryOperation.Sum);
        helper2.RegisterSummary("Carryover", SummaryOperation.Sum);
    }
    private void LoadData()
    {
        //Consultant
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetConsultantBudgets
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.ReportType == Convert.ToInt32(hfReportType.Value)
                   orderby m.ConsultantName
                   select new
                   {
                       m.ID,
                       m.ConsultantName,
                       m.ServicesProvided,
                       m.HourlyRate,
                       m.NumberOfHours,
                       m.ApprovedFederalFunds,
                       BudgetExpenditures = (m.HourlyRate != null ? Convert.ToInt32(m.HourlyRate * m.NumberOfHours) : 0),
                       Carryover = (m.HourlyRate != null ? Convert.ToInt32(m.ApprovedFederalFunds - m.HourlyRate * m.NumberOfHours) : 0)
                   };
        GridView1.DataSource = data;
        GridView1.DataBind();

        //Contract
        var contracts = from m in db.MagnetContractsBudgets
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.ReportType == Convert.ToInt32(hfReportType.Value)
                   orderby m.Item
                   select new
                   {
                       m.ID,
                       m.Item,
                       m.Location,
                       m.Detail,
                       m.UnitCost,
                       m.NumberOfDays,
                       m.NumberOfPeople,
                       m.ApprovedFederalFunds,
                       BudgetExpenditures = (m.UnitCost != null ? Convert.ToInt32(m.UnitCost * m.NumberOfDays * m.NumberOfPeople) : 0),
                       Carryover = (m.UnitCost != null ? Convert.ToInt32(m.ApprovedFederalFunds - m.UnitCost * m.NumberOfDays * m.NumberOfPeople) : 0)
                   };
        GridView2.DataSource = contracts;
        GridView2.DataBind();
    }
    private void ClearFields()
    {
        txtConsultantName.Text = "";
        txtServicesProvided.Text = "";
        txtHourlyRate.Text = "";
        txtNumberOfHours.Text = "";
        txtConsultantApprovedFunds.Text = "";
        hfID.Value = "";
        txtItem.Text = "";
        txtLocation.Text = "";
        txtDetail.Text = "";
        txtUnitCost.Text = "";
        txtNumberOfDays.Text = "";
        txtNumberOfPeople.Text = "";
        txtApprovedContractFunds.Text = "";
    }
    protected void OnEditConsultant(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetConsultantBudget data = MagnetConsultantBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtConsultantName.Text = data.ConsultantName;
        txtServicesProvided.Text = data.ServicesProvided;
        txtHourlyRate.Text = Convert.ToString(data.HourlyRate);
        txtNumberOfHours.Text = Convert.ToString(data.NumberOfHours);
        txtConsultantApprovedFunds.Text = Convert.ToString(data.ApprovedFederalFunds);
        mpeConsultantWindow.Show();
    }
    protected void OnEditContract(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetContractsBudget data = MagnetContractsBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtItem.Text = data.Item;
        txtLocation.Text = data.Location;
        txtDetail.Text = data.Detail;
        txtUnitCost.Text = Convert.ToString(data.UnitCost);
        txtNumberOfDays.Text = Convert.ToString(data.NumberOfDays);
        txtNumberOfPeople.Text = Convert.ToString(data.NumberOfPeople);
        txtApprovedContractFunds.Text = Convert.ToString(data.ApprovedFederalFunds);
        mpeContractWindow.Show();
    }
    protected void OnNewConsultant(object sender, EventArgs e)
    {
        ClearFields();
        mpeConsultantWindow.Show();
    }
    protected void OnNewContract(object sender, EventArgs e)
    {
        ClearFields();
        mpeContractWindow.Show();
    }
    protected void OnSaveConsultant(object sender, EventArgs e)
    {

        MagnetConsultantBudget data = new MagnetConsultantBudget();
        if (!string.IsNullOrEmpty(hfID.Value))
            data = MagnetConsultantBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
            data.ReportType = Convert.ToInt32(hfReportType.Value);
        }
        data.ApprovedFederalFunds = string.IsNullOrEmpty(txtConsultantApprovedFunds.Text) ? 0 : Convert.ToInt32(txtConsultantApprovedFunds.Text);
        data.ConsultantName = txtConsultantName.Text;
        data.ServicesProvided = txtServicesProvided.Text;
        data.HourlyRate = string.IsNullOrEmpty(txtHourlyRate.Text) ? 0 : Convert.ToDecimal(txtHourlyRate.Text);
        data.NumberOfHours = string.IsNullOrEmpty(txtNumberOfHours.Text) ? 0 : Convert.ToInt32(txtNumberOfHours.Text);
        data.Save();
        RegisterHelper();
        LoadData();
    }
    protected void OnSaveContract(object sender, EventArgs e)
    {

        MagnetContractsBudget data = new MagnetContractsBudget();
        if (!string.IsNullOrEmpty(hfID.Value))
            data = MagnetContractsBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
            data.ReportType = Convert.ToInt32(hfReportType.Value);
        }
        data.ApprovedFederalFunds = string.IsNullOrEmpty(txtApprovedContractFunds.Text) ? 0 : Convert.ToInt32(txtApprovedContractFunds.Text);
        data.Item = txtItem.Text;
        data.Location = txtLocation.Text;
        data.Detail = txtDetail.Text;
        data.UnitCost = string.IsNullOrEmpty(txtUnitCost.Text) ? 0 : Convert.ToDecimal(txtUnitCost.Text);
        data.NumberOfDays = string.IsNullOrEmpty(txtNumberOfDays.Text) ? 0 : Convert.ToInt32(txtNumberOfDays.Text);
        data.NumberOfPeople = string.IsNullOrEmpty(txtNumberOfPeople.Text) ? 0 : Convert.ToInt32(txtNumberOfPeople.Text);
        data.Save();
        RegisterHelper();
        LoadData();
    }
    protected void OnSaveSummary(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSummary.Text))
        {
            //Save to database
            MagnetBudgetSummary summary = new MagnetBudgetSummary();
            if (!string.IsNullOrEmpty(hfSummaryID.Value))
                summary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSummaryID.Value));
            else
            {
                summary.ReportID = Convert.ToInt32(hfReportID.Value);
                summary.ReportType = Convert.ToInt32(hfReportType.Value);
            }
            summary.ReportSummary = txtSummary.Text;
            summary.Save();
        }
    }
}