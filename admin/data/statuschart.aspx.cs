﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Synergy.Magnet;
using Telerik.Web.UI;
using System.Data;

public partial class admin_data_statuschart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            if (Request.QueryString["option"] == null)
                Response.Redirect("managestatuschart.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadObjective(id);
                }
                //else
                //{
                //    MagnetProjectObjective item = new MagnetProjectObjective();
                //    item.ReportID = Convert.ToInt32(hfReportID.Value);
                //    item.IsActive = true;
                //    item.Save();
                //    hfID.Value = item.ID.ToString();
                //}
            }
            GridView1.DataSource=LoadData();
            GridView1.DataBind();

            //Build link
            BuildLink();

            int reportPeriod=1;

             if (Session["ReportPeriodID"] == null)  
                {

                    Session["ReportPeriodID"]=reportPeriod = MagnetReportPeriod.Find(x => x.isActive == true).Last().ID;
                     
                }
                else
                    reportPeriod = Convert.ToInt32(Session["ReportPeriodID"]);

            //Disable textbox and check box for non-msap people
             //if ((Context.User.IsInRole("Administrators") || Context.User.IsInRole("Data"))
             //    || ((reportPeriod % 2 == 0) && (Context.User.IsInRole("projectdirector") || Context.User.IsInRole("evaluator")))
             //    )
             if (Context.User.IsInRole("Administrators") || Context.User.IsInRole("ED") || (reportPeriod % 2 == 0))
             {
                 cbStatusUpdate.ForeColor = System.Drawing.Color.Black;
                 cbStatusUpdate.Enabled = true;
             }
             else
             {
                 //In Ad hoc period, project directors and evaluators roles should enabled a checkbox;
                 txtObjective.ReadOnly = true;
                 cbStatusUpdate.Enabled = false;
                 button1.Visible = false;
                 Button4.Visible = false;
             }

             if (!(Context.User.IsInRole("Administrators")||Context.User.IsInRole("ED")))
                 txtObjective.ReadOnly = true;
        }
    }
    private void BuildLink()
    {
        string links = "<a href='managestatuschart.aspx'>Back to Objectives</a>&nbsp;&nbsp;&nbsp;&nbsp;";
        string tabLinks = "";
        int idx = 1;
        foreach (MagnetProjectObjective po in MagnetProjectObjective.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value)))
        {
            if (hfID.Value.Equals(po.ID.ToString()))
            {
                links += "Objective " + idx.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;";
                lblSSID.Text = idx.ToString();
                lblCurrentObjective.Text = "Project Objective " + idx.ToString();
                tabLinks = "<li  class='tab_active'>" + "Objective " + idx.ToString() + "</li>" + tabLinks;
            }
            else
            {
                links += "<a href='statuschart.aspx?option=" + po.ID.ToString() + "'>Objective " + idx.ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;";
                tabLinks = "<li>" + "<a href='statuschart.aspx?option=" + po.ID.ToString() + "'>Objective " + idx.ToString() + "</a></li>" + tabLinks;
            }
            idx++;
        }
        tabDiv.InnerHtml = tabLinks;
        linkDiv.InnerHtml = links;
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("managestatuschart.aspx");
    }
    protected void OnRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Disable edit button if not active
            if (e.Row.Cells[8].Text.Equals("No"))
            {
                ((LinkButton)e.Row.Cells[9].Controls[1]).Enabled = false;
                e.Row.CssClass = "GreyedRow";
            }

            if (HttpContext.Current.User.IsInRole("Administrators"))
                ((LinkButton)e.Row.Cells[9].Controls[3]).Visible = true;
            else
                ((LinkButton)e.Row.Cells[9].Controls[3]).Visible = false;

        }
    }
    protected void OnSaveObjective(object sender, EventArgs e)
    {
        SaveData();
        BuildLink();
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 2);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 2;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    private void SaveData()
    {
        MagnetProjectObjective obejctive = new MagnetProjectObjective();
        if (!string.IsNullOrEmpty(hfID.Value))
            obejctive = MagnetProjectObjective.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            obejctive.ReportID = Convert.ToInt32(hfReportID.Value);
            obejctive.IsActive = true;
        }
        obejctive.ProjectObjective = txtObjective.Text;
        obejctive.StatusUpdate = cbStatusUpdate.Checked ? true : false;
        //obejctive.ExplanationProgress = txtExplanationProgress.Text;
        obejctive.Save();
        hfID.Value = obejctive.ID.ToString();

        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    protected void OnActivate(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        MagnetGrantPerformance magnetPerformance = MagnetGrantPerformance.SingleOrDefault(x => x.ID == id);
        magnetPerformance.IsActive = !magnetPerformance.IsActive;
        magnetPerformance.Save();
        GridView1.DataSource=LoadData();

        GridView1.DataBind();
    }
    protected void OnAddObjectiveDetail(object sender, EventArgs e)
    {
        SaveData();
        BuildLink();
        Response.Redirect("objectperformance.aspx?option=" + hfID.Value + "&mgp=0&index=" + lblSSID.Text, true);
    }
    private DataTable LoadData()
    {
        DataTable dtb = new DataTable();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            MagnetDBDB db = new MagnetDBDB();
            var data = from m in db.MagnetGrantPerformances
                       where m.ProjectObjectiveID == Convert.ToInt32(hfID.Value)
                       orderby m.OrderIndex
                       select new
                       {
                           m.ID,
                           m.PerformanceMeasure,
                           m.MeasureType,
                           m.TargetNumber,
                           TargetRatio = m.TargetRatio1 + "/" + m.TargetRatio2,
                           m.TargetPercentage,
                           m.ActualNumber,
                           AcutalRatio = m.AcutalRatio1 + "/" + m.AcutalRatio2,
                           m.ActualPercentage,
                           ActiveStatus = (m.IsActive == true ? "Yes" : "No"),
                           ActiveAction = (m.IsActive == true ? "Deactivate" : "Activate")
                       };
            dtb = data.ToDataTable();
        }

        return dtb;
    }
    private void LoadObjective(int ID)
    {
        MagnetProjectObjective objective = MagnetProjectObjective.SingleOrDefault(x => x.ID == ID);
        txtObjective.Text = objective.ProjectObjective;
        cbStatusUpdate.Checked = objective.StatusUpdate == true ? true : false;
        //txtExplanationProgress.Text = objective.ExplanationProgress;
    }
   
    protected void OnEdit(object sender, EventArgs e)
    {
        hfStatusID.Value = (sender as LinkButton).CommandArgument;
        Response.Redirect("objectperformance.aspx?option=" + hfID.Value + "&mgp=" + hfStatusID.Value + "&index=" + lblSSID.Text, true);
    }

    protected void OnDelete(object sender, EventArgs e)
    {
        int Id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        var data = MagnetGrantPerformance.SingleOrDefault(x => x.ID == Id);
        data.Delete();
        GridView1.DataSource = LoadData();

        GridView1.DataBind();
    }
    
}