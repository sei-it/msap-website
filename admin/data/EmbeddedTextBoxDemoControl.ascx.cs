﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.XPath;
using System.Text;
using Synergy.Magnet;

public partial class EmbeddedTextBoxDemoControl : System.Web.UI.UserControl
{
    public int ReportID
    {
        get;
        set;
    }
    class SchoolYearClass
    {
        public int ID { get; set; }
        public string SchoolName { get; set; }
        public string SchoolYear { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        btn.OnClientClick = String.Format("$find('{0}').finishEdits();", ceExtender.ClientID);
        if (!Page.IsPostBack)
        {
            string user = Context.User.Identity.Name;
            int GranteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(user.ToLower()))[0].GranteeID;
            foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == GranteeID))
            {
                if (MagnetSchoolYear.Find(x => x.ReportID == ReportID && x.SchoolID == School.ID).Count == 0)
                {
                    MagnetSchoolYear msy = new MagnetSchoolYear();
                    msy.ReportID = ReportID;
                    msy.SchoolID = School.ID;
                    msy.Save();
                }
            }

            MagnetDBDB db = new MagnetDBDB();
            var data = (from m in db.MagnetSchoolYears
                        from n in db.MagnetSchools
                        where m.SchoolID == n.ID
                        && m.ReportID == ReportID
                        select new SchoolYearClass { ID = m.ID, SchoolName = n.SchoolName, SchoolYear = m.SchoolYear }).ToList();
            gvMembers.DataSource = data;
            gvMembers.DataBind();
        }
    }

    protected void RowReady(object sender, Devarchive.Net.GridViewControlEmbedderRowReadyEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.RowID = ((SchoolYearClass)e.Row.DataItem).ID.ToString();
            e.CellValues[0] = ((SchoolYearClass)e.Row.DataItem).SchoolName;
            e.CellValues[1] = ((SchoolYearClass)e.Row.DataItem).SchoolYear;
        }
    }

    protected void Click(object sender, EventArgs e)
    {
        if (ceExtender.Values.Count > 0)
        {
            foreach (string key in ceExtender.Values.Keys)
            {
                string id = key;
                MagnetSchoolYear SchoolYear = MagnetSchoolYear.SingleOrDefault(x => x.ID == Convert.ToInt32(id));
                foreach (string column in ceExtender.Values[key].Keys)
                {
                    SchoolYear.SchoolYear = ceExtender.Values[key][column];
                }
                SchoolYear.Save();
            }
            // And finally clear whole state 
            ceExtender.Values.Clear();
        }
    }
}
