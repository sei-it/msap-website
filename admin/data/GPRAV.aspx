﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="GPRAV.aspx.cs" Inherits="admin_data_GPRAV" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit GPRA Performance Measure 6 Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript" >
        function percentage_vald1(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtAllcohort.ClientID%>').value;
            var num = document.getElementById('<%=txtAllgraduated.ClientID%>').value;
            if ((deno == '' || round(deno) == 0)) {
                args.IsValid = false;
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should be greater than zero."
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }

        }

        function percentage_vald2(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtAmericanIndiancohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtAmericanIndiangraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald3(sender, args) {
            var round = Math.round;
            var deno  = document.getElementById('<%=txtAsiancohort.ClientID%>').value;
            var num = document.getElementById('<%=txtAsiangraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }


        //////////////////////////////
        function percentage_vald4(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtAfricanAmericancohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtAfricanAmericangraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald5(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtHispanicLatinocohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtHispanicLatinograduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald6(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtNativeHawaiiancohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtNativeHawaiiangraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald7(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtWhitecohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtWhitegraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald8(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtMoreRacescohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtMoreRacesgraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald9(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtEconomicallycohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtEconomicallygraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald10(sender, args) {
            var round = Math.round;
            var deno = document.getElementById('<%=txtEnglishlearnerscohort.ClientID%>').value;
            var num = document.getElementById('<%=txtEnglishlearnersgraduated.ClientID%>').value;
            if (num != '' && deno == '') {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: Four-year adjusted cohort should have a value."
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (round(num) > round(deno))) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }


        function txtpercentage1() {
            ch1 = $("#<%=txtAllcohort.ClientID%>").val() >= 0 ? $("#<%=txtAllcohort.ClientID%>").val().replace(",", "") : $("#<%=txtAllcohort.ClientID%>").val('');
            ch2 = $("#<%=txtAllgraduated.ClientID%>").val() >= 0 ? $("#<%=txtAllgraduated.ClientID%>").val().replace(",", "") : $("#<%=txtAllgraduated.ClientID%>").val('');
            ch3 = '';

            if (ch1 != '' && ch2 != '' && ch1 > 0 && ch2 !='0' ) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtallpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {     //adjusted cohort is zero
                $("#<%=txtallpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage2() {
            ch1 = $("#<%=txtAmericanIndiancohort.ClientID%>").val() >= 0 ? $("#<%=txtAmericanIndiancohort.ClientID%>").val().replace(",", "") : $("#<%=txtAmericanIndiancohort.ClientID%>").val('');
            ch2 = $("#<%=txtAmericanIndiangraduated.ClientID%>").val() >= 0 ? $("#<%=txtAmericanIndiangraduated.ClientID%>").val().replace(",", "") : $("#<%=txtAmericanIndiangraduated.ClientID%>").val('');
            ch3 = '';

            $("#<%=txtAmericanIndiangraduated.ClientID%>").removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtAmericanIndianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtAmericanIndianpercentage.ClientID%>").val('');
                $("#<%=txtAmericanIndiangraduated.ClientID%>").val('');
                $("#<%=txtAmericanIndiangraduated.ClientID%>").attr('disabled', 'disabled');
            }
            else if( ch2 == '0'){
             $("#<%=txtAmericanIndianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage3() {
            ch1 = $("#<%=txtAsiancohort.ClientID%>").val() >= 0 ? $("#<%=txtAsiancohort.ClientID%>").val().replace(",", "") : $("#<%=txtAsiancohort.ClientID%>").val('');
            ch2 = $("#<%=txtAsiangraduated.ClientID%>").val() >= 0 ? $("#<%=txtAsiangraduated.ClientID%>").val().replace(",", "") : $("#<%=txtAsiangraduated.ClientID%>").val('');
            ch3 = '';

            $('#<%=txtAsiangraduated.ClientID%>').removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtAsianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtAsianpercentage.ClientID%>").val('');
                $('#<%=txtAsiangraduated.ClientID%>').val('');
                $('#<%=txtAsiangraduated.ClientID%>').attr('disabled', 'disabled');
            }
            else if (ch2 == '0') {
                $("#<%=txtAsianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage4() {
           ch1 = $("#<%=txtAfricanAmericancohort.ClientID%>").val() >= 0 ? $("#<%=txtAfricanAmericancohort.ClientID%>").val().replace(",", "") : $("#<%=txtAfricanAmericancohort.ClientID%>").val('');
           ch2 = $("#<%=txtAfricanAmericangraduated.ClientID%>").val() >= 0 ? $("#<%=txtAfricanAmericangraduated.ClientID%>").val().replace(",", "") : $("#<%=txtAfricanAmericangraduated.ClientID%>").val('');
           ch3 = '';

            $('#<%=txtAfricanAmericangraduated.ClientID%>').removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtAfricanAmericanpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtAfricanAmericanpercentage.ClientID%>").val('');
                $('#<%=txtAfricanAmericangraduated.ClientID%>').val('');
                $('#<%=txtAfricanAmericangraduated.ClientID%>').attr('disabled', 'disabled');
            }
             else if( ch2 == '0'){
             $("#<%=txtAfricanAmericanpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage5() {
           var ch1 = $("#<%=txtHispanicLatinocohort.ClientID%>").val() >= 0 ? $("#<%=txtHispanicLatinocohort.ClientID%>").val().replace(",", "") : $("#<%=txtHispanicLatinocohort.ClientID%>").val('');
           var ch2 = $("#<%=txtHispanicLatinograduated.ClientID%>").val() >= 0 ? $("#<%=txtHispanicLatinograduated.ClientID%>").val().replace(",", "") : $("#<%=txtHispanicLatinograduated.ClientID%>").val('');
           var ch3 = '';

            $('#<%=txtHispanicLatinograduated.ClientID%>').removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtHispanicLatinopercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtHispanicLatinopercentage.ClientID%>").val('');
                $('#<%=txtHispanicLatinograduated.ClientID%>').val('');
                $('#<%=txtHispanicLatinograduated.ClientID%>').attr('disabled', 'disabled');
            }
             else if( ch2 == '0'){
             $("#<%=txtHispanicLatinopercentage.ClientID%>").val('');
            }
        }

        function txtpercentage6() {
            ch1 = $("#<%=txtNativeHawaiiancohort.ClientID%>").val() >= 0 ? $("#<%=txtNativeHawaiiancohort.ClientID%>").val().replace(",", "") : $("#<%=txtNativeHawaiiancohort.ClientID%>").val('');
            ch2 = $("#<%=txtNativeHawaiiangraduated.ClientID%>").val() >= 0 ? $("#<%=txtNativeHawaiiangraduated.ClientID%>").val().replace(",", "") : $("#<%=txtNativeHawaiiangraduated.ClientID%>").val('');
            ch3 = '';

            $('#<%=txtNativeHawaiiangraduated.ClientID%>').removeAttr('disabled');
            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtNativeHawaiianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtNativeHawaiianpercentage.ClientID%>").val('');
                $('#<%=txtNativeHawaiiangraduated.ClientID%>').val('');
                $('#<%=txtNativeHawaiiangraduated.ClientID%>').attr('disabled', 'disabled');
            }
             else if( ch2 == '0'){
             $("#<%=txtNativeHawaiianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage7() {
            ch1 = $("#<%=txtWhitecohort.ClientID%>").val() >= 0 ? $("#<%=txtWhitecohort.ClientID%>").val().replace(",", "") : $("#<%=txtWhitecohort.ClientID%>").val('');
            ch2 = $("#<%=txtWhitegraduated.ClientID%>").val() >= 0 ? $("#<%=txtWhitegraduated.ClientID%>").val().replace(",", "") : $("#<%=txtWhitegraduated.ClientID%>").val('');
            ch3 = '';

            $('#<%=txtWhitegraduated.ClientID%>').removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtWhitepercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtWhitepercentage.ClientID%>").val('');
                $('#<%=txtWhitegraduated.ClientID%>').val('');
                $('#<%=txtWhitegraduated.ClientID%>').attr('disabled', 'disabled');
            }
             else if( ch2 == '0'){
             $("#<%=txtWhitepercentage.ClientID%>").val('');
            }
        }

        function txtpercentage8() {
            ch1 = $("#<%=txtMoreRacescohort.ClientID%>").val() >= 0 ? $("#<%=txtMoreRacescohort.ClientID%>").val().replace(",", "") : $("#<%=txtMoreRacescohort.ClientID%>").val('');
            ch2 = $("#<%=txtMoreRacesgraduated.ClientID%>").val() >= 0 ? $("#<%=txtMoreRacesgraduated.ClientID%>").val().replace(",", "") : $("#<%=txtMoreRacesgraduated.ClientID%>").val('');
            ch3 = '';

            $('#<%=txtMoreRacesgraduated.ClientID%>').removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtMoreRacespercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtMoreRacespercentage.ClientID%>").val('');
                $('#<%=txtMoreRacesgraduated.ClientID%>').val('');
                $('#<%=txtMoreRacesgraduated.ClientID%>').attr('disabled', 'disabled');
            }
             else if( ch2 == '0'){
             $("#<%=txtMoreRacespercentage.ClientID%>").val('');
            }
        }

        function txtpercentage9() {
            ch1 = $("#<%=txtEconomicallycohort.ClientID%>").val() >= 0 ? $("#<%=txtEconomicallycohort.ClientID%>").val().replace(",", "") : $("#<%=txtEconomicallycohort.ClientID%>").val('');
            ch2 = $("#<%=txtEconomicallygraduated.ClientID%>").val() >= 0 ? $("#<%=txtEconomicallygraduated.ClientID%>").val().replace(",", "") : $("#<%=txtEconomicallygraduated.ClientID%>").val('');
            ch3 = '';

            $('#<%=txtEconomicallygraduated.ClientID%>').removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtEconomicallypercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtEconomicallypercentage.ClientID%>").val('');
                $('#<%=txtEconomicallygraduated.ClientID%>').val('');
                $('#<%=txtEconomicallygraduated.ClientID%>').attr('disabled', 'disabled');
            }
             else if( ch2 == '0'){
             $("#<%=txtEconomicallypercentage.ClientID%>").val('');
            }
        }

        function txtpercentage10() {
            ch1 = $("#<%=txtEnglishlearnerscohort.ClientID%>").val() >= 0 ? $("#<%=txtEnglishlearnerscohort.ClientID%>").val().replace(",", "") : $("#<%=txtEnglishlearnerscohort.ClientID%>").val('');
            ch2 = $("#<%=txtEnglishlearnersgraduated.ClientID%>").val() >= 0 ? $("#<%=txtEnglishlearnersgraduated.ClientID%>").val().replace(",", "") : $("#<%=txtEnglishlearnersgraduated.ClientID%>").val('');
            ch3 = '';

            $('#<%=txtEnglishlearnersgraduated.ClientID%>').removeAttr('disabled');

            if (ch1 != '' && ch2 != '' && ch2 != '0' && ch1 > 0) {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtEnglishlearnerspercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else if (ch1 != '' && ch1 == '0') {
                $("#<%=txtEnglishlearnerspercentage.ClientID%>").val('');
                $('#<%=txtEnglishlearnersgraduated.ClientID%>').val('');
                $('#<%=txtEnglishlearnersgraduated.ClientID%>').attr('disabled', 'disabled');
            }
             else if( ch2 == '0'){
             $("#<%=txtEnglishlearnerspercentage.ClientID%>").val('');
            }
        }
    
    </script>

    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
    </script>
    <style type="text/css">
        .msapDataTbl tr td:first-child
        {
            color: #4e8396 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
            text-align: center;
        }
        .msapDataTbl th
        {
            color: #000;
            text-align: left;
        }
        .TDCenterClass
        {
            text-align: center !important;
        }
        .style1
        {
            width: 290px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managegpra.aspx">GPRA Table</a> <span class="greater_than">&gt;</span>
            Part V. GPRA Performance Measure 6 Data</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <div class="GPRA_titles">
            <asp:Label ID="lblSchoolName" runat="server" Width="380px"></asp:Label></div>
        <div class="tab_area">
            <ul class="tabs">
               <li><a runat="server" id="tabPartVI" href="gpravi.aspx">Part VI</a></li>
                <li class="tab_active">Part V</li>
                <li><a runat="server" id="tabPartIV" href="gpraiv.aspx">Part IV</a></li>
                <li><a runat="server" id="tabPartIII" href="gpraiii.aspx">Part III</a></li>
                <li><a runat="server" id="tabPartII" href="gpraii.aspx">Part II</a></li>
                <li><a runat="server" id="tabPartI" href="gprai.aspx">Part I</a></li>
            </ul>
        </div>
        <br />
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Part V. GPRA Performance Measure 6 Data</b> </span>
        <p>
        Part V of the GPRA Table collects high school graduation rates. High school graduation rate data will be collected annually to establish trend data. However, the annual measurable objective data will be collected 3 years after the grant ends. Follow the instructions in the <i>GPRA Guide</i> to complete the items in each row.
            </p>
      
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfPerformanceMeasure" runat="server" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <br /><br />
        <hr style="color: #A9DEF2;" />
        <table class="msapDataTbl">
            <tr>
                <th colspan="5" style="color: #F58220; text-align:center;" >
                    Magnet school graduation rate
                </th>
            </tr>
            <tr>
            <td style="border-bottom: 2px solid #F58220; text-align:center;" class="style1"></td>
            <td style="border-bottom: 2px solid #F58220 !important; text-align:center;"></td>
            <td colspan="3" style="border-bottom: 2px solid #F58220 !important; text-align:center;">
                <b><asp:Label ID="lblReportYear" runat="server" Text=""></asp:Label></b></td>
            </tr>
            <tr >
            <td style="border-bottom: 2px solid #F58220; color:Black; text-align:center;" 
                    class="style1"><b>Student groups</b></td>
            <td style="border-bottom: 2px solid #F58220 !important; text-align:center;"><b>Four-year<br /> adjusted cohort</b></td>
            <td  style="border-bottom: 2px solid #F58220 !important; text-align:center;">
                <b>Number of students who graduated</b></td>
             <td colspan="2"  style="border-bottom: 2px solid #F58220 !important; text-align:center;">
                <b>Percentage of students who graduated</b></td>
                
            </tr>
            <tr>
                <td class="style1">
                    51. <b>All</b> students</td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtAllcohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                      onkeyup="txtpercentage1()"   CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                TargetControlID="txtAllcohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">

                    <asp:TextBox MinValue="0" ID="txtAllgraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                     onkeyup="txtpercentage1()"    CssClass="change" EnableEmbeddedSkins="false" NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtAllgraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtallpercentage" runat="server" Skin="MyCustomSkin" Enabled="false" ViewStateMode="Enabled"
                        CssClass="change" EnableEmbeddedSkins="false"  Width="100"></telerik:RadTextBox>
                </td>
                <td>
                    <asp:CustomValidator ID="CustomValidator1" runat="server" 
       ClientValidationFunction="percentage_vald1"></asp:CustomValidator> 
                </td>
            </tr>
            <tr>
                <td class="style1">
                    52. <b>American Indian or Alaska Native</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtAmericanIndiancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage2()"   CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                TargetControlID="txtAmericanIndiancohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtAmericanIndiangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage2()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                     <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                                TargetControlID="txtAmericanIndiangraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtAmericanIndianpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false" Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator2" runat="server" 
       ClientValidationFunction="percentage_vald2" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    53. <b>Asian</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtAsiancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage3()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                                TargetControlID="txtAsiancohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtAsiangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage3()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                     <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                                TargetControlID="txtAsiangraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtAsianpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"   Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator3" runat="server" 
       ClientValidationFunction="percentage_vald3" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    54. <b>Black or African-American</b> students
                </td>
               <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtAfricanAmericancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage4()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                                TargetControlID="txtAfricanAmericancohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
               <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtAfricanAmericangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage4()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers"
                                TargetControlID="txtAfricanAmericangraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
               <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtAfricanAmericanpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"
                        Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator4" runat="server" 
       ClientValidationFunction="percentage_vald4" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    55. <b>Hispanic or Latino</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtHispanicLatinocohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage5()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers"
                                TargetControlID="txtHispanicLatinocohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtHispanicLatinograduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage5()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers"
                                TargetControlID="txtHispanicLatinograduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtHispanicLatinopercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"   Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator5" runat="server" 
       ClientValidationFunction="percentage_vald5" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    56. <b>Native Hawaiian or Other Pacific Islander</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtNativeHawaiiancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage6()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                     <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers"
                                TargetControlID="txtNativeHawaiiancohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtNativeHawaiiangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage6()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                 <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers"
                                TargetControlID="txtNativeHawaiiangraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtNativeHawaiianpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false" Enabled="false"  Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator6" runat="server" 
       ClientValidationFunction="percentage_vald6" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    57. <b>White</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtWhitecohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage7()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers"
                                TargetControlID="txtWhitecohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtWhitegraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage7()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Numbers"
                                TargetControlID="txtWhitegraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtWhitepercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false" Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator7" runat="server" 
       ClientValidationFunction="percentage_vald7" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    58. <b>Two or more races </b>students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtMoreRacescohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage8()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                 <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Numbers"
                                TargetControlID="txtMoreRacescohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtMoreRacesgraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage8()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                 <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Numbers"
                                TargetControlID="txtMoreRacesgraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtMoreRacespercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"  Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator8" runat="server" 
       ClientValidationFunction="percentage_vald8" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
              <tr>
                <td class="style1">
                    59. <b>Economically disadvantaged</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtEconomicallycohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage9()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Numbers"
                                TargetControlID="txtEconomicallycohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtEconomicallygraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage9()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Numbers"
                                TargetControlID="txtEconomicallygraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtEconomicallypercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"  Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator9" runat="server" 
       ClientValidationFunction="percentage_vald9" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    60. <b>English language learners</b></td>
             <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtEnglishlearnerscohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage10()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
             <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Numbers"
                                TargetControlID="txtEnglishlearnerscohort">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:TextBox MinValue="0" ID="txtEnglishlearnersgraduated" runat="server" Skin="MyCustomSkin"  MaxLength="6"
                    onkeyup="txtpercentage10()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </asp:TextBox>
                 <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers"
                                TargetControlID="txtEnglishlearnersgraduated">
                            </ajax:FilteredTextBoxExtender>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtEnglishlearnerspercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false" Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator10" runat="server" 
       ClientValidationFunction="percentage_vald10" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
        </table>
        <div style="text-align: right; margin-top: 20px;">
            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn1 postbutton" Text="Save Record"
                OnClick="OnSave" />
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <a href="manageGPRA.aspx">Back to GPRA table</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                id="LinkI" href="GPRAI.aspx">Part I</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                    id="LinkII" href="GPRAII.aspx">Part II</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                        id="LinkIII" href="GPRAIII.aspx">Part III</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server"  id="LinkIV" href="GPRAIV.aspx">Part IV</a>&nbsp;&nbsp;&nbsp;&nbsp;
            Part V &nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"  id="LinkVI" href="GPRAVI.aspx">Part VI</a>
        </div>
    </div>
</asp:Content>



