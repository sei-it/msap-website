﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managecalendar.aspx.cs" Inherits="admin_managecalendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Calendar Entries
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Manage Calendar Entries</h1>
    <cc1:ToolkitScriptManager ID="Manager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New Calendar Entry" CssClass="msapBtn" OnClick="OnNew" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false"
        PageSize="10" DataKeyNames="ID" OnPageIndexChanging="OnGridViewPageIndexChanged"
        CssClass="msapTbl">
        <Columns>
            <asp:BoundField DataField="CalendarTitle" SortExpression="CalendarTitle" HeaderText="Calendar Title" />
            <asp:BoundField DataField="CalendarStart" SortExpression="CalendarStart" HeaderText="Calendar Start" />
            <asp:BoundField DataField="CalendarEnd" SortExpression="CalendarEnd" HeaderText="Calendar End" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID")%>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID")%>'
                        OnClick="OnDelete"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <%-- Calendar --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Calendar Entry</div>
            <table>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <tr>
                            <td>
                                Start:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox3" CssClass="msapTxt" runat="server"></asp:TextBox>
                                <asp:CheckBox ID="CheckBox1" runat="server" Text="All Day" OnCheckedChanged="OnAllDayEvent"
                                    AutoPostBack="true" />
                                <cc2:TimeSelector ID="TimeSelector1" AllowSecondEditing="false" AmPm="am" DisplaySeconds="false"
                                    runat="server" MinuteIncrement="30" />
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox3">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                End:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox4" CssClass="msapTxt" runat="server"></asp:TextBox>
                                <cc2:TimeSelector ID="TimeSelector2" AllowSecondEditing="false" DisplaySeconds="false"
                                    AmPm="am" runat="server" MinuteIncrement="30" />
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TextBox4">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTimeZone" runat="server">
                                    <asp:ListItem>ET</asp:ListItem>
                                    <asp:ListItem>CT</asp:ListItem>
                                    <asp:ListItem>MT</asp:ListItem>
                                    <asp:ListItem>PT</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Event Title:
                            </td>
                            <td colspan="4">
                                <asp:TextBox ID="TextBox1" CssClass="msapTxt" Width="500"  runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Event Description:
                            </td>
                            <td colspan="4">
                                <asp:TextBox ID="TextBox2" CssClass="msapTxt" TextMode="MultiLine" Columns="60" Rows="6" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="button1" runat="server" CssClass="msapBtn" Text="Save" OnClick="OnSave" />
                        &nbsp;
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <cc1:modalpopupextender id="mpeNewsWindow" runat="server" targetcontrolid="LinkButton7"
        popupcontrolid="PopupPanel" dropshadow="true" okcontrolid="Button15" cancelcontrolid="Button15"
        backgroundcssclass="magnetMPE" y="20" />
</asp:Content>
