﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Drawing;

public partial class admin_data_GPRAVI : System.Web.UI.Page
{
    bool isADHOCReportPeriod = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            Session["GPRA6item18"] = 0;
            Session["GPRA6item19"] = 0;
            Session["GPRA6item20"] = 0;
            Session["GPRA6item21"] = 0;
            Session["GPRA6item22"] = 0;
            Session["GPRA6item23"] = 0;

            Session["GPRA6item26a"] = 0;
            Session["GPRA6item26b"] = 0;
            Session["GPRA6item26c"] = 0;
            Session["GPRA6item26d"] = 0;
            Session["GPRA6item26e"] = 0;
            Session["GPRA6item26f"] = 0;

            Session["GPRA6item27a"] = 0;
            Session["GPRA6item27b"] = 0;
            Session["GPRA6item27c"] = 0;
            Session["GPRA6item27d"] = 0;
            Session["GPRA6item27e"] = 0;
            Session["GPRA6item27f"] = 0;

            Session["GPRA6item28"] = 0;

            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
            
            //Report type
            int granteeID = (int)report.GranteeID;

            if (Request.QueryString["id"] == null)
                Response.Redirect("managegpra.aspx");
            else
            {
                LinkI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
                tabPartI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
                LinkII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
                tabPartII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
                LinkIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                tabPartIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                LinkIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                tabPartIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                LinkV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
                tabPartV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];

                int schoolID = Convert.ToInt32(Request.QueryString["id"]);
                lblSchoolName.Text = MagnetSchool.SingleOrDefault(x => x.ID == schoolID).SchoolName;
                MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
                isADHOCReportPeriod = (period.ID % 2) == 0 ? true : false;
                int ReportYear = Convert.ToInt32(period.ReportPeriod.Substring(7, 2)) + 1;
                lblReportPeriod.Text = "Target enrollment percentage for fall 20" + period.ReportPeriod.Substring(7, 2); // +" - " + Convert.ToString(ReportYear);
                lblReportPeriod2.Text = "Actual enrollment percentage for fall 20" + period.ReportPeriod.Substring(7, 2); // +" - " + Convert.ToString(ReportYear);

                var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == Convert.ToInt32(hfReportID.Value));
                if (gpraData.Count > 0)
                {
                    hfID.Value = gpraData[0].ID.ToString();

                    //Enrollment data
                    var enrollmentData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 2);
                    if (enrollmentData.Count > 0)
                    {
                        //18-23
                        Session["GPRA6item18"] = enrollmentData[0].Indian==null? 0  : enrollmentData[0].Indian;
                        Session["GPRA6item19"] = enrollmentData[0].Asian == null ? 0 : (enrollmentData[0].Asian);
                        Session["GPRA6item20"] = enrollmentData[0].Black == null ? 0 : (enrollmentData[0].Black);
                        Session["GPRA6item21"] = enrollmentData[0].Hispanic== null ? 0 : (enrollmentData[0].Hispanic);
                        Session["GPRA6item22"] = enrollmentData[0].Hawaiian==null ? 0 : (enrollmentData[0].Hawaiian);
                        Session["GPRA6item23"] = enrollmentData[0].White==null ? 0 : (enrollmentData[0].White);

                        Session["GPRA6item28"] = enrollmentData[0].ExtraFiled3 ==null ? 1 : (enrollmentData[0].ExtraFiled3);


                        //26(a) - 26(g)
                        Session["GPRA6item26a"] = enrollmentData[0].NewIndian == null ? 0 : (enrollmentData[0].NewIndian);
                        Session["GPRA6item26b"] = enrollmentData[0].NewAsian == null ? 0 : (enrollmentData[0].NewAsian);
                        Session["GPRA6item26c"] = enrollmentData[0].NewBlack == null ? 0 : (enrollmentData[0].NewBlack);
                        Session["GPRA6item26d"] = enrollmentData[0].NewHispanic == null ? 0 : (enrollmentData[0].NewHispanic);
                        Session["GPRA6item26e"] = enrollmentData[0].NewHawaiian == null ? 0 : (enrollmentData[0].NewHawaiian);
                        Session["GPRA6item26f"] = enrollmentData[0].NewWhite == null ? 0 : (enrollmentData[0].NewWhite);
                       
                        //27(a) - 27(g)
                        Session["GPRA6item27a"] = enrollmentData[0].ContIndian == null ? 0 : (enrollmentData[0].ContIndian);
                        Session["GPRA6item27b"] = enrollmentData[0].ContAsian == null ? 0 : (enrollmentData[0].ContAsian);
                        Session["GPRA6item27c"] = enrollmentData[0].ContBlack == null ? 0 : (enrollmentData[0].ContBlack);
                        Session["GPRA6item27d"] = enrollmentData[0].ContHispanic == null ? 0 : (enrollmentData[0].ContHispanic);
                        Session["GPRA6item27e"] = enrollmentData[0].ContHawaiian == null ? 0 : (enrollmentData[0].ContHawaiian);
                        Session["GPRA6item27f"] = enrollmentData[0].ContWhite == null ? 0 : (enrollmentData[0].ContWhite);
                    }

                    MagnetGPRA6 gpra6 = MagnetGPRA6.SingleOrDefault(x => x.MagnetGPRAID == gpraData[0].ID);
                    if (gpra6 != null)
                    {
                        rbtnMsapIndian62.Checked = gpra6.IndianMSAP62;
                        rbtnFeederIndian62.Checked = gpra6.IndianFeeder62;
                        rbtnMSAPAsian62.Checked = gpra6.AsianMSAP62;
                        rbtnFeederAsian62.Checked = gpra6.AsianFeeder62;
                        rbtnMSAPBlack62.Checked = gpra6.BlackMSAP62;
                        rbtnFeederBlack62.Checked = gpra6.BlackFeeder62;
                        rbtnMSAPHispanic62.Checked = gpra6.HispanicMSAP62;
                        rbtnFeederHispanic62.Checked = gpra6.HispanicFeeder62;
                        rbtnMSAPNative62.Checked = gpra6.NativeMSAP62;
                        rbtnFeederNative62.Checked = gpra6.NativeFeeder62;
                        rbtnMSAPWhite62.Checked = gpra6.WhiteMSAP62;
                        rbtnFeederWhite62.Checked = gpra6.WhiteFeeder62;

                        rbtnDecIndian63.Checked = gpra6.IndianDec63;
                        rbtnIncIndian63.Checked = gpra6.IndianInc63;
                        rbtnMtnIndian63.Checked = gpra6.IndianMtn63;
                        rbtnDecAsian63.Checked = gpra6.AsianDec63;
                        rbtnIncAsian63.Checked = gpra6.AsianInc63;
                        rbtnMtnAsian63.Checked = gpra6.AsianMtn63;
                        rbtnDecBlack63.Checked = gpra6.BlackDec63;
                        rbtnIncBlack63.Checked = gpra6.BlackInc63;
                        rbtnMtnBlack63.Checked = gpra6.BlackMtn63;
                        rbtnDecHispanic63.Checked = gpra6.HispanicDec63;
                        rbtnIncHispanic63.Checked = gpra6.HispanicInc63;
                        rbtnMtnHispanic63.Checked = gpra6.HispanicMtn63;
                        rbtnDecNative63.Checked = gpra6.NativeDec63;
                        rbtnIncNative63.Checked = gpra6.NativeInc63;
                        rbtnMtnNative63.Checked = gpra6.NativeMtn63;
                        rbtnDecWhite63.Checked = gpra6.WhiteDec63;
                        rbtnIncWhite63.Checked = gpra6.WhiteInc63;
                        rbtnMtnWhite63.Checked = gpra6.WhiteMtn63;

                        foreach (string itm in gpra6.TargetRacialGroup64.Split(','))
                        {
                            switch (itm)
                            {
                                case "1":       //American Indian or Alaska Native students
                                    cbxlstTargetMinRacialIsolateGroups.Items[0].Selected = true;
                                    break;
                                case "2":       //Asian students
                                    cbxlstTargetMinRacialIsolateGroups.Items[1].Selected = true;
                                    break;
                                case "3":       //Black or African-American students
                                    cbxlstTargetMinRacialIsolateGroups.Items[2].Selected = true;
                                    break;
                                case "4":       //Hispanic or Latino students
                                    cbxlstTargetMinRacialIsolateGroups.Items[3].Selected = true;
                                    break;
                                case "5":       //Native Hawaiian or Other Pacific Islander students
                                    cbxlstTargetMinRacialIsolateGroups.Items[4].Selected = true;
                                    break;
                                case "6":       //White students
                                    cbxlstTargetMinRacialIsolateGroups.Items[5].Selected = true;
                                    break;

                            }
                        }

                        txtTargetIndian65.Text = string.IsNullOrEmpty(gpra6.IndianTargetPercentage65.Trim()) ? "" : gpra6.IndianTargetPercentage65.Trim();
                        txtTargetAsian65.Text = string.IsNullOrEmpty(gpra6.AsianTargetPercentage65.Trim()) ? "" : gpra6.AsianTargetPercentage65.Trim();
                        txtTargetBlack65.Text = string.IsNullOrEmpty(gpra6.BlackTargetPercentage65.Trim()) ? "" : gpra6.BlackTargetPercentage65.Trim();
                        txtTargetHispanic65.Text = string.IsNullOrEmpty(gpra6.HispanicTargetPercentage65.Trim()) ? "" : gpra6.HispanicTargetPercentage65.Trim();
                        txtTargetNative65.Text = string.IsNullOrEmpty(gpra6.NativeTargetPercentage65.Trim()) ? "" : gpra6.NativeTargetPercentage65.Trim();
                        txtTargetWhite65.Text = string.IsNullOrEmpty(gpra6.WhiteTargetPercentage65.Trim()) ? "" : gpra6.WhiteTargetPercentage65.Trim();

                        rbtnMetIndian66.Checked = gpra6.IndianMet66;
                        rbtnNotMetIndian66.Checked = gpra6.IndianNotMet66;
                        rbtnMetAsian66.Checked = gpra6.AsianMet66;
                        rbtnNotMetAsian66.Checked = gpra6.AsianNotMet66;
                        rbtnMetBlack66.Checked = gpra6.BlackMet66;
                        rbtnNotMetBlack66.Checked = gpra6.BlackNotMet66;
                        rbtnMetHispanic66.Checked = gpra6.HispanicMet66;
                        rbtnNotMetHispanic66.Checked = gpra6.HispanicNotMet66;
                        rbtnMetNative66.Checked = gpra6.NativeMet66;
                        rbtnNotMetNative66.Checked = gpra6.NativeNotMet66;
                        rbtnMetWhite66.Checked = gpra6.WhiteMet66;
                        rbtnNotMetWhite66.Checked = gpra6.WhiteNotMet66;

                        rbtnMadeIndian67.Checked = gpra6.IndianMade67;
                        rbtnNotMadeIndian67.Checked = gpra6.IndianNotMade67;
                        rbtnNAIndian67.Checked = gpra6.IndianNA67;
                        rbtnMadeAsian67.Checked = gpra6.AsianMade67;
                        rbtnNotMadeAsian67.Checked = gpra6.AsianNotMade67;
                        rbtnNAAsian67.Checked = gpra6.AsianNA67;
                        rbtnMadeBlack67.Checked = gpra6.BlackMade67;
                        rbtnNotMadeBlack67.Checked = gpra6.BlackNotMade67;
                        rbtnNABlack67.Checked = gpra6.BlackNA67;
                        rbtnMadeHispanic67.Checked = gpra6.HispanicMade67;
                        rbtnNotMadeHispanic67.Checked = gpra6.HispanicNotMade67;
                        rbtnNAHispanic67.Checked = gpra6.HispanicNA67;
                        rbtnMadeNative67.Checked = gpra6.NativeMade67;
                        rbtnNotMadeNative67.Checked = gpra6.NativeNotMade67;
                        rbtnNANative67.Checked = gpra6.NativeNA67;
                        rbtnMadeWhite67.Checked = gpra6.WhiteMade67;
                        rbtnNotMadeWhite67.Checked = gpra6.WhiteNotMade67;
                        rbtnNAWhite67.Checked = gpra6.WhiteNA67;

                        MinRacialIsolateGroupsChecklist(gpra6.MinorityIsolatedGroups);  //initial gpra6 fields
                    }
                    else
                        MinRacialIsolateGroupsChecklist("");  //initial gpra6 fields with unchecked
                }
                else
                {
                    MagnetGPRA item = new MagnetGPRA();
                    item.ReportID = Convert.ToInt32(hfReportID.Value);
                    item.SchoolID = schoolID;
                    item.Save();
                    hfID.Value = item.ID.ToString();

                    MagnetGPRA6 mpm = new MagnetGPRA6();
                    mpm.ReportType = 60;  //gpra6
                    mpm.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    mpm.MinorityIsolatedGroups = "";
                    mpm.TargetRacialGroup64 = "";
                    mpm.IndianTargetPercentage65 = "";
                    mpm.AsianTargetPercentage65 = "";
                    mpm.BlackTargetPercentage65 = "";
                    mpm.HispanicTargetPercentage65 = "";
                    mpm.NativeTargetPercentage65 = "";
                    mpm.WhiteTargetPercentage65 = "";
                    mpm.Save();
                    //hfPerformanceMeasure.Value = mpm.ID.ToString();
                }
               
            }
        }
    }
    private void EnableDisableTable62(bool isEnableIndian, bool isEnableAsian, bool isEnableBlack, bool isEnableHispanic, bool isEnableNative, bool isEnableWhite)
    {
        lblIndian62.ForeColor = isEnableIndian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMsapIndian62.Enabled = isEnableIndian ? true : false;
        rbtnFeederIndian62.Enabled = isEnableIndian ? true : false;
        if (isADHOCReportPeriod)
            cusvalIndian62.Enabled = isEnableIndian ? true : false;
        if (!isEnableIndian)
        {
            rbtnMsapIndian62.Checked = false;
            rbtnFeederIndian62.Checked = false;
        }
       

        lblAsian62.ForeColor = isEnableAsian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMSAPAsian62.Enabled = isEnableAsian ? true : false;
        rbtnFeederAsian62.Enabled = isEnableAsian ? true : false;
        if(isADHOCReportPeriod)
            cusvalAsian62.Enabled = isEnableAsian ? true : false;
        if (!isEnableAsian)
        {
            rbtnMSAPAsian62.Checked = false;
            rbtnFeederAsian62.Checked = false;
        }
       
        lblBlack62.ForeColor = isEnableBlack ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMSAPBlack62.Enabled = isEnableBlack ? true : false;
        rbtnFeederBlack62.Enabled = isEnableBlack ? true : false;
        if (isADHOCReportPeriod)
            cusvalBlack62.Enabled = isEnableBlack ? true : false;
        if (!isEnableBlack)
        {
            rbtnMSAPBlack62.Checked = false;
            rbtnFeederBlack62.Checked = false;
        }
        

        lblHispanic62.ForeColor = isEnableHispanic ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMSAPHispanic62.Enabled = isEnableHispanic ? true : false;
        rbtnFeederHispanic62.Enabled = isEnableHispanic ? true : false;
        if (isADHOCReportPeriod)
            cusvalHispanic62.Enabled = isEnableHispanic ? true : false;
        if (!isEnableHispanic)
        {
            rbtnMSAPHispanic62.Checked = false;
            rbtnFeederHispanic62.Checked = false;
        }

        lblNative62.ForeColor = isEnableNative ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMSAPNative62.Enabled = isEnableNative ? true : false;
        rbtnFeederNative62.Enabled = isEnableNative ? true : false;
        if (isADHOCReportPeriod)
            cusvalNative62.Enabled = isEnableNative ? true : false;
        if (!isEnableNative)
        {
            rbtnMSAPNative62.Checked = false;
            rbtnFeederNative62.Checked = false;
        }

        lblWhite62.ForeColor = isEnableWhite ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMSAPWhite62.Enabled = isEnableWhite ? true : false;
        rbtnFeederWhite62.Enabled = isEnableWhite ? true : false;
        if (isADHOCReportPeriod)
            cusvalWhite62.Enabled = isEnableWhite ? true : false;
        if (!isEnableWhite)
        {
            rbtnMSAPWhite62.Checked = false;
            rbtnFeederWhite62.Checked = false;
        }

    }
    private void EnableDisableTable63(bool isEnableIndian, bool isEnableAsian, bool isEnableBlack, bool isEnableHispanic, bool isEnableNative, bool isEnableWhite)
    {
        lblIndian63.ForeColor = isEnableIndian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnDecIndian63.Enabled = isEnableIndian ? true : false;
        rbtnIncIndian63.Enabled = isEnableIndian ? true : false;
        rbtnMtnIndian63.Enabled = isEnableIndian ? true : false;
        if (isADHOCReportPeriod)
            cusvalIndian63.Enabled = isEnableIndian ? true : false;
        if (!isEnableIndian)
        {
            rbtnDecIndian63.Checked = false;
            rbtnIncIndian63.Checked = false;
            rbtnMtnIndian63.Checked = false;
        }

        lblAsian63.ForeColor = isEnableAsian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnDecAsian63.Enabled = isEnableAsian ? true : false;
        rbtnIncAsian63.Enabled = isEnableAsian ? true : false;
        rbtnMtnAsian63.Enabled = isEnableAsian ? true : false;
        if (isADHOCReportPeriod)
            cusvalAsian63.Enabled = isEnableAsian ? true : false;
        if (!isEnableAsian)
        {
            rbtnDecAsian63.Checked = false;
            rbtnIncAsian63.Checked = false;
            rbtnMtnAsian63.Checked = false;
        }

        lblBlack63.ForeColor = isEnableBlack ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnDecBlack63.Enabled = isEnableBlack ? true : false;
        rbtnIncBlack63.Enabled = isEnableBlack ? true : false;
        rbtnMtnBlack63.Enabled = isEnableBlack ? true : false;
        if (isADHOCReportPeriod)
            cusvalBlack63.Enabled = isEnableBlack ? true : false;
        if (!isEnableBlack)
        {
            rbtnDecBlack63.Checked = false;
            rbtnIncBlack63.Checked = false;
            rbtnMtnBlack63.Checked = false;
        }

        lblHispanic63.ForeColor = isEnableHispanic ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnDecHispanic63.Enabled = isEnableHispanic ? true : false;
        rbtnIncHispanic63.Enabled = isEnableHispanic ? true : false;
        rbtnMtnHispanic63.Enabled = isEnableHispanic ? true : false;
        if (isADHOCReportPeriod)
            cusvalHispanic63.Enabled = isEnableHispanic ? true : false;
        if (!isEnableHispanic)
        {
            rbtnDecHispanic63.Checked = false;
            rbtnIncHispanic63.Checked = false;
            rbtnMtnHispanic63.Checked = false;
        }

        lblNative63.ForeColor = isEnableNative ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnDecNative63.Enabled = isEnableNative ? true : false;
        rbtnIncNative63.Enabled = isEnableNative ? true : false;
        rbtnMtnNative63.Enabled = isEnableNative ? true : false;
        if (isADHOCReportPeriod)
            cusvalNative63.Enabled = isEnableNative ? true : false;
        if (!isEnableNative)
        {
            rbtnDecNative63.Checked = false;
            rbtnIncNative63.Checked = false;
            rbtnMtnNative63.Checked = false;
        }

        lblWhite63.ForeColor = isEnableWhite ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnDecWhite63.Enabled = isEnableWhite ? true : false;
        rbtnIncWhite63.Enabled = isEnableWhite ? true : false;
        rbtnMtnWhite63.Enabled = isEnableWhite ? true : false;
        if (isADHOCReportPeriod)
            cusvalWhite63.Enabled = isEnableWhite ? true : false;
        if (!isEnableWhite)
        {
            rbtnDecWhite63.Checked = false;
            rbtnIncWhite63.Checked = false;
            rbtnMtnWhite63.Checked = false;
        }
    }
    private void EnableDisableTable65(bool isEnableIndian, bool isEnableAsian, bool isEnableBlack, bool isEnableHispanic, bool isEnableNative, bool isEnableWhite)
    {
        lblIndian65.ForeColor = isEnableIndian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        txtTargetIndian65.Enabled = isEnableIndian ? true : false;
        lblActualIndian65.Text = isEnableIndian ? getActualIndian65() : ""; 
        if(isADHOCReportPeriod)
            rqvalIndian65.Enabled = isEnableIndian ? true : false;
        if(!isEnableIndian )
        {
            txtTargetIndian65.Text="";
        }

        lblAsian65.ForeColor = isEnableAsian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        txtTargetAsian65.Enabled = isEnableAsian ? true : false;
        lblActualAsian65.Text = isEnableAsian ? getActualAsian65() : "";
        if (isADHOCReportPeriod)
            rqvalAsian65.Enabled = isEnableAsian ? true : false;
        if(!isEnableAsian )
        {
            txtTargetAsian65.Text="";
        }

        lblBlack65.ForeColor = isEnableBlack ? Color.FromArgb(78, 131, 150) : Color.Gray;
        txtTargetBlack65.Enabled = isEnableBlack ? true : false;
        lblActualBlack65.Text = isEnableBlack ? getActualBlack65() : "";
        if (isADHOCReportPeriod)
            rqvalBlack65.Enabled = isEnableBlack ? true : false;
        if(!isEnableBlack )
        {
            txtTargetBlack65.Text="";
        }

        lblHispanic65.ForeColor = isEnableHispanic ? Color.FromArgb(78, 131, 150) : Color.Gray;
        txtTargetHispanic65.Enabled = isEnableHispanic ? true : false;
        lblActualHispanic65.Text = isEnableHispanic ? getActualHispanic65() : "";
        if (isADHOCReportPeriod)
            rqvalHispanic65.Enabled = isEnableHispanic ? true : false;
        if(!isEnableHispanic )
        {
            txtTargetHispanic65.Text = "";
        }

        lblNative65.ForeColor = isEnableNative ? Color.FromArgb(78, 131, 150) : Color.Gray;
        txtTargetNative65.Enabled = isEnableNative ? true : false;
        lblActualNative65.Text = isEnableNative ? getActualNative65() : "";
        if (isADHOCReportPeriod)
            rqvalNative65.Enabled = isEnableNative ? true : false;
        if(!isEnableNative )
        {
            txtTargetNative65.Text="";
        }

        lblWhite65.ForeColor = isEnableWhite ? Color.FromArgb(78, 131, 150) : Color.Gray;
        txtTargetWhite65.Enabled = isEnableWhite ? true : false;
        lblActualWhite65.Text = isEnableWhite ? getActualWhite65() : "";
        if (isADHOCReportPeriod)
            rqvalWhite65.Enabled = isEnableWhite ? true : false;
        if(!isEnableWhite )
        {
             txtTargetWhite65.Text ="";
        }
    }

    private string getActualIndian65()
    {
        double sum = Convert.ToDouble(Session["GPRA6item18"]) +
            Convert.ToDouble(Session["GPRA6item26a"]) + Convert.ToDouble(Session["GPRA6item27a"]);
        return (int)Session["GPRA6item28"]==0? "" : ((sum / Convert.ToDouble(Session["GPRA6item28"]))*100).ToString("F");

    }

    private string getActualAsian65()
    {
        double sum = Convert.ToDouble(Session["GPRA6item19"]) +
            Convert.ToDouble(Session["GPRA6item26b"]) + Convert.ToDouble(Session["GPRA6item27b"]);
        return (int)Session["GPRA6item28"] == 0 ? "" : ((sum / Convert.ToDouble(Session["GPRA6item28"]))*100).ToString("F");

    }

    private string getActualBlack65()
    {
        double sum = Convert.ToDouble(Session["GPRA6item20"]) +
        Convert.ToDouble(Session["GPRA6item26c"]) + Convert.ToDouble(Session["GPRA6item27c"]);
        return (int)Session["GPRA6item28"] == 0 ? "" : ((sum / Convert.ToDouble(Session["GPRA6item28"]))*100).ToString("F");

    }

    private string getActualHispanic65()
    {
        double sum = Convert.ToDouble(Session["GPRA6item21"]) +
    Convert.ToDouble(Session["GPRA6item26d"]) + Convert.ToDouble(Session["GPRA6item27d"]);
        return (int)Session["GPRA6item28"] == 0 ? "" : ((sum / Convert.ToDouble(Session["GPRA6item28"]))*100).ToString("F");

    }

    private string getActualNative65()
    {
        double sum = Convert.ToDouble(Session["GPRA6item22"]) +
            Convert.ToDouble(Session["GPRA6item26e"]) + Convert.ToDouble(Session["GPRA6item27e"]);
        return (int)Session["GPRA6item28"] == 0 ? "" : ((sum / Convert.ToDouble(Session["GPRA6item28"]))*100).ToString("F");

    }

    private string getActualWhite65()
    {
        double sum = Convert.ToDouble(Session["GPRA6item23"]) +
    Convert.ToDouble(Session["GPRA6item26f"]) + Convert.ToDouble(Session["GPRA6item27f"]);
        return (int)Session["GPRA6item28"] == 0 ? "" : ((sum / Convert.ToDouble(Session["GPRA6item28"]))*100).ToString("F");

    }


    private void EnableDisableTable66(bool isEnableIndian, bool isEnableAsian, bool isEnableBlack, bool isEnableHispanic, bool isEnableNative, bool isEnableWhite)
    {
        lblIndian66.ForeColor = isEnableIndian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMetIndian66.Enabled = isEnableIndian ? true : false;
        rbtnNotMetIndian66.Enabled = isEnableIndian ? true : false;
        if (isADHOCReportPeriod)
            cusvalIndian66.Enabled = isEnableIndian ? true : false;
        if (!isEnableIndian)
        {
            rbtnMetIndian66.Checked = false;
            rbtnNotMetIndian66.Checked = false;
        }

        lblAsian66.ForeColor = isEnableAsian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMetAsian66.Enabled = isEnableAsian ? true : false;
        rbtnNotMetAsian66.Enabled = isEnableAsian ? true : false;
        if (isADHOCReportPeriod)
            cusvalAsian66.Enabled = isEnableAsian ? true : false;
        if (!isEnableAsian)
        {
            rbtnMetAsian66.Checked = false;
            rbtnNotMetAsian66.Checked = false;
        }

        lblBlack66.ForeColor = isEnableBlack ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMetBlack66.Enabled = isEnableBlack ? true : false;
        rbtnNotMetBlack66.Enabled = isEnableBlack ? true : false;
        if (isADHOCReportPeriod)
            cusvalBlack66.Enabled = isEnableBlack ? true : false;
        if (!isEnableBlack)
        {
            rbtnMetBlack66.Checked = false;
            rbtnNotMetBlack66.Checked = false;
        }

        lblHispanic66.ForeColor = isEnableHispanic ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMetHispanic66.Enabled = isEnableHispanic ? true : false;
        rbtnNotMetHispanic66.Enabled = isEnableHispanic ? true : false;
        if (isADHOCReportPeriod)
            cusvalHispanic66.Enabled = isEnableHispanic ? true : false;
        if (!isEnableHispanic)
        {
            rbtnMetHispanic66.Checked = false;
            rbtnNotMetHispanic66.Checked = false;
        }

        lblNative66.ForeColor = isEnableNative ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMetNative66.Enabled = isEnableNative ? true : false;
        rbtnNotMetNative66.Enabled = isEnableNative ? true : false;
        if (isADHOCReportPeriod)
            cusvalNative66.Enabled = isEnableNative ? true : false;
        if (!isEnableNative)
        {
            rbtnMetNative66.Checked = false;
            rbtnNotMetNative66.Checked = false;
        }

        lblWhite66.ForeColor = isEnableWhite ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMetWhite66.Enabled = isEnableWhite ? true : false;
        rbtnNotMetWhite66.Enabled = isEnableWhite ? true : false;
        if (isADHOCReportPeriod)
            cusvalWhite66.Enabled = isEnableWhite ? true : false;
        if (!isEnableWhite)
        {
            rbtnMetWhite66.Checked = false;
            rbtnNotMetWhite66.Checked = false;
        }
    }
    private void EnableDisableTable67(bool isEnableIndian, bool isEnableAsian, bool isEnableBlack, bool isEnableHispanic, bool isEnableNative, bool isEnableWhite)
    {
        lblIndian67.ForeColor = isEnableIndian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMadeIndian67.Enabled = isEnableIndian ? true : false;
        rbtnNotMadeIndian67.Enabled = isEnableIndian ? true : false;
        rbtnNAIndian67.Enabled = isEnableIndian ? true : false;
        if (isADHOCReportPeriod)
            cusvalIndian67.Enabled = isEnableIndian ? true : false;
        if (!isEnableIndian)
        {
            rbtnMadeIndian67.Checked = false;
            rbtnNotMadeIndian67.Checked = false;
            rbtnNAIndian67.Checked = false;
        }

        lblAsian67.ForeColor = isEnableAsian ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMadeAsian67.Enabled = isEnableAsian ? true : false;
        rbtnNotMadeAsian67.Enabled = isEnableAsian ? true : false;
        rbtnNAAsian67.Enabled = isEnableAsian ? true : false;
        if (isADHOCReportPeriod)
            cusvalAsian67.Enabled = isEnableAsian ? true : false;
        if (!isEnableAsian)
        {
            rbtnMadeAsian67.Checked = false;
            rbtnNotMadeAsian67.Checked = false;
            rbtnNAAsian67.Checked = false;
        }

        lblBlack67.ForeColor = isEnableBlack ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMadeBlack67.Enabled = isEnableBlack ? true : false;
        rbtnNotMadeBlack67.Enabled = isEnableBlack ? true : false;
        rbtnNABlack67.Enabled = isEnableBlack ? true : false;
        if (isADHOCReportPeriod)
            cusvalBlack67.Enabled = isEnableBlack ? true : false;
        if (!isEnableBlack)
        {
            rbtnMadeBlack67.Checked = false;
            rbtnNotMadeBlack67.Checked = false;
            rbtnNABlack67.Checked = false;
        }

        lblHispanic67.ForeColor = isEnableHispanic ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMadeHispanic67.Enabled = isEnableHispanic ? true : false;
        rbtnNotMadeHispanic67.Enabled = isEnableHispanic ? true : false;
        rbtnNAHispanic67.Enabled = isEnableHispanic ? true : false;
        if (isADHOCReportPeriod)
            cusvalHispanic67.Enabled = isEnableHispanic ? true : false;
        if (!isEnableHispanic)
        {
            rbtnMadeHispanic67.Checked = false;
            rbtnNotMadeHispanic67.Checked = false;
            rbtnNAHispanic67.Checked = false;
        }

        lblNative67.ForeColor = isEnableNative ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMadeNative67.Enabled = isEnableNative ? true : false;
        rbtnNotMadeNative67.Enabled = isEnableNative ? true : false;
        rbtnNANative67.Enabled = isEnableNative ? true : false;
        if (isADHOCReportPeriod)
            cusvalNative67.Enabled = isEnableNative ? true : false;
        if (!isEnableNative)
        {
            rbtnMadeNative67.Checked = false;
            rbtnNotMadeNative67.Checked = false;
            rbtnNANative67.Checked = false;
        }

        lblWhite67.ForeColor = isEnableWhite ? Color.FromArgb(78, 131, 150) : Color.Gray;
        rbtnMadeWhite67.Enabled = isEnableWhite ? true : false;
        rbtnNotMadeWhite67.Enabled = isEnableWhite ? true : false;
        rbtnNAWhite67.Enabled = isEnableWhite ? true : false;
        if (isADHOCReportPeriod)
            cusvalWhite67.Enabled = isEnableWhite ? true : false;
        if (!isEnableWhite)
        {
            rbtnMadeWhite67.Checked = false;
            rbtnNotMadeWhite67.Checked = false;
            rbtnNAWhite67.Checked = false;
        }
    }

    private void MinRacialIsolateGroupsChecklist(string chklist)
    {
        bool isEnableIndian = false, isEnableAsian = false,
            isEnableBlack = false, isEnableHispanic = false,
            isEnableNative = false, isEnableWhite = false;

        foreach (string itm in chklist.Split(','))
        {
            switch (itm)
            {
                case "1":       //American Indian or Alaska Native students
                    isEnableIndian = true;
                    cbxlstMinRacialIsolateGroups.Items[0].Selected = true;
                    break;
                case "2":       //Asian students
                    isEnableAsian = true;
                    cbxlstMinRacialIsolateGroups.Items[1].Selected = true;
                    break;
                case "3":       //Black or African-American students
                    isEnableBlack = true;
                    cbxlstMinRacialIsolateGroups.Items[2].Selected = true;
                    break;
                case "4":       //Hispanic or Latino students
                    isEnableHispanic = true;
                    cbxlstMinRacialIsolateGroups.Items[3].Selected = true;
                    break;
                case "5":       //Native Hawaiian or Other Pacific Islander students
                    isEnableNative = true;
                    cbxlstMinRacialIsolateGroups.Items[4].Selected = true;
                    break;
                case "6":       //White students
                    isEnableWhite = true;
                    cbxlstMinRacialIsolateGroups.Items[5].Selected = true;
                    break;
            }

        }

        EnableDisableTable62(isEnableIndian, isEnableAsian, isEnableBlack, isEnableHispanic, isEnableNative, isEnableWhite);
        EnableDisableTable63(isEnableIndian, isEnableAsian, isEnableBlack, isEnableHispanic, isEnableNative, isEnableWhite);
        EnableDisableTable65(isEnableIndian, isEnableAsian, isEnableBlack, isEnableHispanic, isEnableNative, isEnableWhite);
        EnableDisableTable66(isEnableIndian, isEnableAsian, isEnableBlack, isEnableHispanic, isEnableNative, isEnableWhite);
        EnableDisableTable67(isEnableIndian, isEnableAsian, isEnableBlack, isEnableHispanic, isEnableNative, isEnableWhite);

    }
    protected void cbxlstMinRacialIsolateGroups_SelectedIndexChanged(object sender, EventArgs e)
    {
        string grouplist = "";
        foreach (ListItem itm in cbxlstMinRacialIsolateGroups.Items)
        {
            if (itm.Selected)
                grouplist += itm.Value + ",";
        }
        MinRacialIsolateGroupsChecklist(grouplist.TrimEnd(','));
    }

    private bool validationAllcheckedfields()
    {
        bool isvalid = true;
        foreach (ListItem li in cbxlstMinRacialIsolateGroups.Items)
        {
            switch (li.Value)
            {
                case "1":       //American Indian or Alaska Native students
                    if (isADHOCReportPeriod)
                    {
                        cusvalIndian62.IsValid = true;
                        cusvalIndian63.IsValid = true;
                        rqvalIndian65.IsValid = true;
                        cusvalIndian66.IsValid = true;
                        cusvalIndian67.IsValid = true;
                    }
                    if (li.Selected)
                    {
                        if (!rbtnMsapIndian62.Checked && !rbtnFeederIndian62.Checked)
                            isvalid = cusvalIndian62.IsValid = false;
                        if (!rbtnDecIndian63.Checked && !rbtnIncIndian63.Checked && !rbtnMtnIndian63.Checked)
                            isvalid = cusvalIndian63.IsValid = false;
                        if (string.IsNullOrEmpty(txtTargetIndian65.Text.Trim()))
                            isvalid = rqvalIndian65.IsValid = false;
                        if (!rbtnMetIndian66.Checked && !rbtnNotMetIndian66.Checked)
                            isvalid = cusvalIndian66.IsValid = false;
                        if (!rbtnMadeIndian67.Checked && !rbtnNotMadeIndian67.Checked && !rbtnNAIndian67.Checked)
                            isvalid = cusvalIndian67.IsValid = false;
                    }
                    break;
                case "2":       //Asian students
                    if (isADHOCReportPeriod)
                    {
                        cusvalAsian62.IsValid = true;
                        cusvalAsian63.IsValid = true;
                        rqvalAsian65.IsValid = true;
                        cusvalAsian66.IsValid = true;
                        cusvalAsian67.IsValid = true;
                    }
                    if (li.Selected)
                    {
                        if (!rbtnMSAPAsian62.Checked && !rbtnFeederAsian62.Checked)
                            isvalid = cusvalAsian62.IsValid = false;
                        if (!rbtnDecAsian63.Checked && !rbtnIncAsian63.Checked && !rbtnMtnAsian63.Checked)
                            isvalid = cusvalAsian63.IsValid = false;
                        if (string.IsNullOrEmpty(txtTargetAsian65.Text.Trim()))
                            isvalid = rqvalAsian65.IsValid = false;
                        if (!rbtnMetAsian66.Checked && !rbtnNotMetAsian66.Checked)
                            isvalid = cusvalAsian66.IsValid = false;
                        if (!rbtnMadeAsian67.Checked && !rbtnNotMadeAsian67.Checked && !rbtnNAAsian67.Checked)
                            isvalid = cusvalAsian67.IsValid = false;
                    }
                    
                    break;
                case "3":       //Black or African-American students
                    if (isADHOCReportPeriod)
                    {
                        cusvalBlack62.IsValid = true;
                        cusvalBlack63.IsValid = true;
                        rqvalBlack65.IsValid = true;
                        cusvalBlack66.IsValid = true;
                        cusvalBlack67.IsValid = true;
                    }
                    if (li.Selected)
                    {
                        if (!rbtnMSAPBlack62.Checked && !rbtnFeederBlack62.Checked)
                            isvalid = cusvalBlack62.IsValid = false;
                        if (!rbtnDecBlack63.Checked && !rbtnIncBlack63.Checked && !rbtnMtnBlack63.Checked)
                            isvalid = cusvalBlack63.IsValid = false;
                        if (string.IsNullOrEmpty(txtTargetBlack65.Text.Trim()))
                            isvalid = rqvalBlack65.IsValid = false;
                        if (!rbtnMetBlack66.Checked && !rbtnNotMetBlack66.Checked)
                            isvalid = cusvalBlack66.IsValid = false;
                        if (!rbtnMadeBlack67.Checked && !rbtnNotMadeBlack67.Checked && !rbtnNABlack67.Checked)
                            isvalid = cusvalBlack67.IsValid = false;
                    }
                    
                    break;
                case "4":       //Hispanic or Latino students
                    if (isADHOCReportPeriod)
                    {
                        cusvalHispanic62.IsValid = true;
                        cusvalHispanic63.IsValid = true;
                        rqvalHispanic65.IsValid = true;
                        cusvalHispanic66.IsValid = true;
                        cusvalHispanic67.IsValid = true;
                    }
                    if (li.Selected)
                    {
                        if (!rbtnMSAPHispanic62.Checked && !rbtnFeederHispanic62.Checked)
                            isvalid = cusvalHispanic62.IsValid = false;
                        if (!rbtnDecHispanic63.Checked && !rbtnIncHispanic63.Checked && !rbtnMtnHispanic63.Checked)
                            isvalid = cusvalHispanic63.IsValid = false;
                        if (string.IsNullOrEmpty(txtTargetHispanic65.Text.Trim()))
                            isvalid = rqvalHispanic65.IsValid = false;
                        if (!rbtnMetHispanic66.Checked && !rbtnNotMetHispanic66.Checked)
                            isvalid = cusvalHispanic66.IsValid = false;
                        if (!rbtnMadeHispanic67.Checked && !rbtnNotMadeHispanic67.Checked && !rbtnNAHispanic67.Checked)
                            isvalid = cusvalHispanic67.IsValid = false;
                    }
                    break;
                case "5":       //Native Hawaiian or Other Pacific Islander students
                    if (isADHOCReportPeriod)
                    {
                        cusvalNative62.IsValid = true;
                        cusvalNative63.IsValid = true;
                        rqvalNative65.IsValid = true;
                        cusvalNative66.IsValid = true;
                        cusvalNative67.IsValid = true;
                    }
                    if (li.Selected)
                    {
                        if (!rbtnMSAPNative62.Checked && !rbtnFeederNative62.Checked)
                            isvalid = cusvalNative62.IsValid = false;
                        if (!rbtnDecNative63.Checked && !rbtnIncNative63.Checked && !rbtnMtnNative63.Checked)
                            isvalid = cusvalNative63.IsValid = false;
                        if (string.IsNullOrEmpty(txtTargetNative65.Text.Trim()))
                            isvalid = rqvalNative65.IsValid = false;
                        if (!rbtnMetNative66.Checked && !rbtnNotMetNative66.Checked)
                            isvalid = cusvalNative66.IsValid = false;
                        if (!rbtnMadeNative67.Checked && !rbtnNotMadeNative67.Checked && !rbtnNANative67.Checked)
                            isvalid = cusvalNative67.IsValid = false;
                    }
                    
                    break;
                case "6":       //White students
                    {
                        cusvalWhite62.IsValid = true;
                        cusvalWhite63.IsValid = true;
                        rqvalWhite65.IsValid = true;
                        cusvalWhite66.IsValid = true;
                        cusvalWhite67.IsValid = true;
                    }
                    if (li.Selected)
                    {
                        if (!rbtnMSAPWhite62.Checked && !rbtnFeederWhite62.Checked)
                            isvalid = cusvalWhite62.IsValid = false;
                        if (!rbtnDecWhite63.Checked && !rbtnIncWhite63.Checked && !rbtnMtnWhite63.Checked)
                            isvalid = cusvalWhite63.IsValid = false;
                        if (string.IsNullOrEmpty(txtTargetWhite65.Text.Trim()))
                            isvalid = rqvalWhite65.IsValid = false;
                        if (!rbtnMetWhite66.Checked && !rbtnNotMetWhite66.Checked)
                            isvalid = cusvalWhite66.IsValid = false;
                        if (!rbtnMadeWhite67.Checked && !rbtnNotMadeWhite67.Checked && !rbtnNAWhite67.Checked)
                            isvalid = cusvalWhite67.IsValid = false;
                    }

                    break;
            }
        }
        return isvalid;
    }

    protected void OnSave(object sender, EventArgs e)
    {
        if(!validationAllcheckedfields())
            return;

        MagnetGPRA6  gpra6 = MagnetGPRA6.SingleOrDefault(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value));

        if (gpra6 == null)
            gpra6 = new MagnetGPRA6();

        gpra6.MagnetGPRAID = Convert.ToInt32(hfID.Value);
        gpra6.ReportType = 60;

        string grouplist = "";
        foreach (ListItem itm in cbxlstMinRacialIsolateGroups.Items)
        {
            if (itm.Selected)
                grouplist += itm.Value + ",";
        }

        gpra6.MinorityIsolatedGroups = grouplist.Trim(',');

        gpra6.IndianMSAP62 = rbtnMsapIndian62.Checked; 
        gpra6.IndianFeeder62 = rbtnFeederIndian62.Checked;
        gpra6.AsianMSAP62 = rbtnMSAPAsian62.Checked;
        gpra6.AsianFeeder62 = rbtnFeederAsian62.Checked;
        gpra6.BlackMSAP62 = rbtnMSAPBlack62.Checked;
        gpra6.BlackFeeder62 = rbtnFeederBlack62.Checked;
        gpra6.HispanicMSAP62 = rbtnMSAPHispanic62.Checked;
        gpra6.HispanicFeeder62 = rbtnFeederHispanic62.Checked;
        gpra6.NativeMSAP62 = rbtnMSAPNative62.Checked;
        gpra6.NativeFeeder62 = rbtnFeederNative62.Checked;
        gpra6.WhiteMSAP62 = rbtnMSAPWhite62.Checked;
        gpra6.WhiteFeeder62 = rbtnFeederWhite62.Checked;

        gpra6.IndianDec63 = rbtnDecIndian63.Checked;
        gpra6.IndianInc63 = rbtnIncIndian63.Checked;
        gpra6.IndianMtn63 = rbtnMtnIndian63.Checked;
        gpra6.AsianDec63 = rbtnDecAsian63.Checked;
        gpra6.AsianInc63 = rbtnIncAsian63.Checked;
        gpra6.AsianMtn63 = rbtnMtnAsian63.Checked;
        gpra6.BlackDec63 = rbtnDecBlack63.Checked;
        gpra6.BlackInc63 = rbtnIncBlack63.Checked;
        gpra6.BlackMtn63 = rbtnMtnBlack63.Checked;
        gpra6.HispanicDec63 = rbtnDecHispanic63.Checked;
        gpra6.HispanicInc63 = rbtnIncHispanic63.Checked;
        gpra6.HispanicMtn63 = rbtnMtnHispanic63.Checked;
        gpra6.NativeDec63 = rbtnDecNative63.Checked;
        gpra6.NativeInc63 = rbtnIncNative63.Checked;
        gpra6.NativeMtn63 = rbtnMtnNative63.Checked;
        gpra6.WhiteDec63 = rbtnDecWhite63.Checked;
        gpra6.WhiteInc63 = rbtnIncWhite63.Checked;
        gpra6.WhiteMtn63 = rbtnMtnWhite63.Checked;

        string Targetgrouplist = "";
        foreach (ListItem itm in cbxlstTargetMinRacialIsolateGroups.Items)
        {
            if (itm.Selected)
                Targetgrouplist += itm.Value + ",";
        }
        gpra6.TargetRacialGroup64 = Targetgrouplist.TrimEnd(',');

        gpra6.IndianTargetPercentage65 = string.IsNullOrEmpty(txtTargetIndian65.Text.Trim()) ? "" : Convert.ToDouble(txtTargetIndian65.Text.Trim()).ToString("F");
        gpra6.AsianTargetPercentage65 = string.IsNullOrEmpty(txtTargetAsian65.Text.Trim()) ? "" : Convert.ToDouble(txtTargetAsian65.Text.Trim()).ToString("F");
        gpra6.BlackTargetPercentage65 = string.IsNullOrEmpty(txtTargetBlack65.Text.Trim()) ? "" : Convert.ToDouble(txtTargetBlack65.Text.Trim()).ToString("F");
        gpra6.HispanicTargetPercentage65 = string.IsNullOrEmpty(txtTargetHispanic65.Text.Trim()) ? "" : Convert.ToDouble(txtTargetHispanic65.Text.Trim()).ToString("F");
        gpra6.NativeTargetPercentage65 = string.IsNullOrEmpty(txtTargetNative65.Text.Trim()) ? "" : Convert.ToDouble(txtTargetNative65.Text.Trim()).ToString("F");
        gpra6.WhiteTargetPercentage65 = string.IsNullOrEmpty(txtTargetWhite65.Text.Trim()) ? "" : Convert.ToDouble(txtTargetWhite65.Text.Trim()).ToString("F");

        gpra6.IndianMet66 = rbtnMetIndian66.Checked;
        gpra6.IndianNotMet66 = rbtnNotMetIndian66.Checked;
        gpra6.AsianMet66 = rbtnMetAsian66.Checked;
        gpra6.AsianNotMet66 = rbtnNotMetAsian66.Checked;
        gpra6.BlackMet66 = rbtnMetBlack66.Checked;
        gpra6.BlackNotMet66 = rbtnNotMetBlack66.Checked;
        gpra6.HispanicMet66 = rbtnMetHispanic66.Checked;
        gpra6.HispanicNotMet66 = rbtnNotMetHispanic66.Checked;
        gpra6.NativeMet66 = rbtnMetNative66.Checked;
        gpra6.NativeNotMet66 = rbtnNotMetNative66.Checked;
        gpra6.WhiteMet66 = rbtnMetWhite66.Checked;
        gpra6.WhiteNotMet66 = rbtnNotMetWhite66.Checked;

        gpra6.IndianMade67 = rbtnMadeIndian67.Checked;
        gpra6.IndianNotMade67 = rbtnNotMadeIndian67.Checked;
        gpra6.IndianNA67 = rbtnNAIndian67.Checked;
        gpra6.AsianMade67 = rbtnMadeAsian67.Checked;
        gpra6.AsianNotMade67 = rbtnNotMadeAsian67.Checked;
        gpra6.AsianNA67 = rbtnNAAsian67.Checked;
        gpra6.BlackMade67 = rbtnMadeBlack67.Checked;
        gpra6.BlackNotMade67 = rbtnNotMadeBlack67.Checked;
        gpra6.BlackNA67 = rbtnNABlack67.Checked;
        gpra6.HispanicMade67 = rbtnMadeHispanic67.Checked;
        gpra6.HispanicNotMade67 = rbtnNotMadeHispanic67.Checked;
        gpra6.HispanicNA67 = rbtnNAHispanic67.Checked;
        gpra6.NativeMade67 = rbtnMadeNative67.Checked;
        gpra6.NativeNotMade67 = rbtnNotMadeNative67.Checked;
        gpra6.NativeNA67 = rbtnNANative67.Checked;
        gpra6.WhiteMade67 = rbtnMadeWhite67.Checked;
        gpra6.WhiteNotMade67 = rbtnNotMadeWhite67.Checked;
        gpra6.WhiteNA67 = rbtnNAWhite67.Checked;

        gpra6.Save();

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 3);  //GPRA tables
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 3;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }

        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
        hfSaved.Value = "1";
    }
   
}