﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="enrollmentschooldata.aspx.cs" Inherits="admin_report_enrollmentschooldata" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Enrollment data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript">
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp != null && tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }

        $(function () {
            window.onbeforeunload = UnloadConfirm;
        });

        function MergeColumn() {
            $('#ctl00_ContentPlaceHolder1_RadGrid1_ctl00').find('th, td').filter(':nth-child(17)').attr('style', 'text-align:left').append(function () {
                if ($(this).next().html() != null)
                    return '<br/>' + $(this).next().html();
                else
                    return '';
            }).next().remove();
			$('a:contains("Cancel")').before('<br/>');
        }
        function GridCreated(sender, eventArgs) {
            MergeColumn();
        }
		
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        <a href="enrollment.aspx">Table 3: Enrollment Data-Magnet Schools</a> <span class="greater_than">
            &gt;</span> School Enrollment Data</h4>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfSchoolID" runat="server" />
    <asp:HiddenField ID="hfStageID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <br />
    <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
    <br />
    <div class="tab_area">
        <div class="titles">
            Table 3: Enrollment Data-Magnet Schools</div>
        <ul class="tabs">
            <li><a href="feederenrollment.aspx?id=3">Table 4</a></li>
            <li class="tab_active">Table 3</li>
            <li><a href="manageschoolyear.aspx">Table 2</a></li>
            <li><a href="leaenrollment.aspx">Table 1</a></li>
        </ul>
    </div>
    <br />
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>
            <asp:Label runat="server" ID="lblSchoolName"></asp:Label></b></span>
    <br />
    <!-- content start -->
    <div class="asofDate">
        Actual Enrollment as of October 1, 2014 (Year 2 of Project)</div>
        <p>
        Provide data for all students in each grade for which the school enrolls students.
        </p>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            <!--
            var hasChanges, inputs, dropdowns, editedRow;
            function RowDblClick(sender, eventArgs) {
                /*editedRow = eventArgs.get_itemIndexHierarchical();
                if (editedRow && hasChanges) {
                hasChanges = false;
                $find("<%= RadGrid1.ClientID %>").get_masterTableView().updateItem(editedRow);
                }*/

                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            }

            function OnKeyPress(Sender, args) {
                if (args.get_keyCode() == 45) {
                    alert("You cannot enter negative numbers in this table.");
                    args.set_cancel(true);
                }
            }


            function setSavedBit(sender, eventArgs) {

                if (eventArgs.get_commandName() == "InitInsert" || eventArgs.get_commandName() == "Edit") {
                    $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
                }
                else if (eventArgs.get_commandName() == "CancelUpdate" || eventArgs.get_commandName() == "CancelInsert" || eventArgs.get_commandName() == "PerformInsert" || eventArgs.get_commandName() == "Update") {
                    $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
                }

            }
            /*function GridCreated(sender, eventArgs) {
            var gridElement = sender.get_element();
            var elementsToUse = [];
            inputs = gridElement.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
            var lowerType = inputs[i].type.toLowerCase();
            if (lowerType == "hidden" || lowerType == "button") {
            continue;
            }

            Array.add(elementsToUse, inputs[i]);
            inputs[i].onchange = TrackChanges;
            }

            dropdowns = gridElement.getElementsByTagName("select");
            for (var i = 0; i < dropdowns.length; i++) {
            dropdowns[i].onchange = TrackChanges;
            }

            setTimeout(function () { if (elementsToUse[0]) elementsToUse[0].focus(); }, 100);
            }
            function GridCommand(sender, args)
            {
            if (args.get_commandName() != "Edit")
            {
            editedRow = null;
            }
            }
            function TrackChanges(e) {
            hasChanges = true;
            }*/
            -->
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="Label1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadGrid ID="RadGrid1" GridLines="None" runat="server" AllowAutomaticDeletes="True" onkeydonw="OnKeyPress"
        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="False"
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnItemUpdated="RadGrid1_ItemUpdated"
        OnItemDeleted="RadGrid1_ItemDeleted" 
        OnItemInserted="RadGrid1_ItemInserted" OnItemCreated="RadGrid1_ItemCreated"
        ShowFooter="true" Skin="MyCustomSkin" EnableEmbeddedSkins="false" 
        oncustomaggregate="RadGrid1_CustomAggregate">
        <PagerStyle Mode="NextPrevAndNumeric" />
        <ClientSettings ClientEvents-OnKeyPress="OnKeyPress">
            <ClientEvents OnGridCreated="GridCreated" />
            <ClientEvents OnCommand="setSavedBit" />
        </ClientSettings>
        <MasterTableView Width="103%" CommandItemDisplay="TopAndBottom" DataKeyNames="ID"
            EditMode="InPlace" DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
            TableLayout="Fixed">
            <CommandItemSettings ShowRefreshButton="false" />
            <Columns>
                <telerik:GridDropDownColumn DataField="GradeLevel" DataSourceID="SqlDataSource2"
                    HeaderText="Grade" ListTextField="TypeName" ListValueField="TypeIndex" UniqueName="GradeLevel"
                    ColumnEditorID="GridDropDownColumnEditor1" FooterText="Total">
                </telerik:GridDropDownColumn>
                <telerik:GridNumericColumn DataField="AmericanIndian" HeaderText="American<br> Indian"
                    UniqueName="AmericanIndian" ColumnEditorID="GridNumericColumnEditor2" Aggregate="Sum"
                    FooterText=" ">
                </telerik:GridNumericColumn>
                <telerik:GridTemplateColumn DataField="P1" HeaderText="%" Aggregate="Custom" UniqueName="P1"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P1") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridNumericColumn DataField="Asian" HeaderText="Asian" UniqueName="Asian"
                    ColumnEditorID="GridNumericColumnEditor3" Aggregate="Sum" FooterText=" ">
                </telerik:GridNumericColumn>
                <telerik:GridTemplateColumn DataField="P2" HeaderText="%" Aggregate="Custom" UniqueName="P2"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P2") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridNumericColumn DataField="AfricanAmerican" HeaderText="African<br> American"
                    UniqueName="AfricanAmerican" ColumnEditorID="GridNumericColumnEditor4" Aggregate="Sum"
                    FooterText=" ">
                </telerik:GridNumericColumn>
                <telerik:GridTemplateColumn DataField="P3" HeaderText="%" Aggregate="Custom" UniqueName="P3"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P3") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridNumericColumn DataField="Hispanic" HeaderText="Hispanic" UniqueName="Hispanic"
                    ColumnEditorID="GridNumericColumnEditor5" Aggregate="Sum" FooterText=" ">
                </telerik:GridNumericColumn>
                <telerik:GridTemplateColumn DataField="P4" HeaderText="%" Aggregate="Custom" UniqueName="P4"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P4") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridNumericColumn DataField="Hawaiian" HeaderText="Hawaiian" UniqueName="Hawaiian"
                    ColumnEditorID="GridNumericColumnEditor7" Aggregate="Sum" FooterText=" ">
                </telerik:GridNumericColumn>
                <telerik:GridTemplateColumn DataField="P6" HeaderText="%" Aggregate="Custom" UniqueName="P6"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P6") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridNumericColumn DataField="White" HeaderText="White" UniqueName="White"
                    ColumnEditorID="GridNumericColumnEditor6" Aggregate="Sum" FooterText=" ">
                </telerik:GridNumericColumn>
                <telerik:GridTemplateColumn DataField="P5" HeaderText="%" Aggregate="Custom" UniqueName="P5"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P5") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridNumericColumn DataField="MultiRacial" HeaderText="Multi Racial" UniqueName="MultiRacial"
                    ColumnEditorID="GridNumericColumnEditor8" Aggregate="Sum" FooterText=" ">
                </telerik:GridNumericColumn>
                <telerik:GridTemplateColumn DataField="P7" HeaderText="%" Aggregate="Custom" UniqueName="P7"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P7") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="total" HeaderText="Total" Aggregate="Sum" UniqueName="total"
                    FooterText=" "  DataType="System.Int32" HeaderStyle-Width="80px">
                    <HeaderTemplate>
                        Total</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("total") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="EditCommandColumn"
                    UpdateText="Update" CancelText="Cancel" EditText="Edit">
                    <ItemStyle CssClass="MyImageButton" />
                </telerik:GridEditCommandColumn>
                <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="Classic"
                    ConfirmTitle="Delete" ButtonType="LinkButton" CommandName="Delete" Text="Delete"
                    UniqueName="DeleteColumn">
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings>
            <ClientEvents OnRowClick="RowDblClick" />
        </ClientSettings>
    </telerik:RadGrid>
    <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditor1" runat="server"
        DropDownStyle-Width="50px" />
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor2" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox1" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
            <ClientEvents OnKeyPress="OnKeyPress" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor3" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox2" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
            <ClientEvents OnKeyPress="OnKeyPress" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor4" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox3" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
            <ClientEvents OnKeyPress="OnKeyPress" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor5" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox4" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
            <ClientEvents OnKeyPress="OnKeyPress" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor6" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox5" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
            <ClientEvents OnKeyPress="OnKeyPress" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor7" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox6" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
            <ClientEvents OnKeyPress="OnKeyPress" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor8" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox7" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
            <ClientEvents OnKeyPress="OnKeyPress" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <br />
    <!-- SelectCommand="SELECT [id],[GradeLevel],[AmericanIndian] as A2, [AmericanIndian], [Asian], [AfricanAmerican], [Hispanic], [White], [Hawaiian], [MultiRacial] FROM [MagnetSchoolEnrollments] where [GranteeReportID]=@GranteeReportID and [StageID]=@StageID order by [GradeLevel]" -->
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        DeleteCommand="DELETE FROM [MagnetSchoolEnrollments] WHERE [ID] = @ID" InsertCommand="INSERT INTO [MagnetSchoolEnrollments] ([GranteeReportID], [StageID], [SchoolID], [GradeLevel], [AmericanIndian], [Asian], [AfricanAmerican], [Hispanic], [White], [Hawaiian], [MultiRacial]) VALUES (@GranteeReportID, @StageID, @SchoolID, @GradeLevel, @AmericanIndian, @Asian, @AfricanAmerican, @Hispanic, @White, @Hawaiian, @MultiRacial)"
        SelectCommand="exec spp_schoolenrollment @GranteeReportID, @StageID, @SchoolID"
        UpdateCommand="UPDATE [MagnetSchoolEnrollments] SET [GradeLevel] = @GradeLevel, [AmericanIndian] = @AmericanIndian, [Asian] = @Asian, [AfricanAmerican] = @AfricanAmerican, [Hispanic] = @Hispanic, [White] = @White, [Hawaiian]=@Hawaiian, [MultiRacial]=@MultiRacial WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:Parameter Name="GranteeReportID" Type="Int32" />
            <asp:Parameter Name="StageID" Type="Int32" />
            <asp:Parameter Name="SchoolID" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="GranteeReportID" Type="Int32" />
            <asp:Parameter Name="StageID" Type="Int32" />
            <asp:Parameter Name="SchoolID" Type="Int32" />
            <asp:Parameter Name="GradeLevel" Type="Int32" />
            <asp:Parameter Name="AmericanIndian" Type="Int32" />
            <asp:Parameter Name="Asian" Type="Int32" />
            <asp:Parameter Name="AfricanAmerican" Type="Int32" />
            <asp:Parameter Name="Hispanic" Type="Int32" />
            <asp:Parameter Name="White" Type="Int32" />
            <asp:Parameter Name="Hawaiian" Type="Int32" />
            <asp:Parameter Name="MultiRacial" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ID" Type="Int32" />
            <asp:Parameter Name="GradeLevel" Type="Int32" />
            <asp:Parameter Name="AmericanIndian" Type="Int32" />
            <asp:Parameter Name="Asian" Type="Int32" />
            <asp:Parameter Name="AfricanAmerican" Type="Int32" />
            <asp:Parameter Name="Hispanic" Type="Int32" />
            <asp:Parameter Name="White" Type="Int32" />
            <asp:Parameter Name="Hawaiian" Type="Int32" />
            <asp:Parameter Name="MultiRacial" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        SelectCommand="SELECT [TypeIndex], [TypeName] FROM [MagnetDLL] where [TypeID]=1 ORDER BY TypeIndex ">
    </asp:SqlDataSource>
    <!-- content end -->
    <div align="right" id="bck_Mag_schools">
        <a href="enrollment.aspx" style="font-size:10px;">Back to Magnet Schools</a>
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a href="leaenrollment.aspx">Table 1 - Enrollment Data-LEA Level</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="manageschoolyear.aspx">Table 2 - Year of Implementation</a>&nbsp;&nbsp;&nbsp;&nbsp;
        Table 3 - Enrollment Data-Magnet Schools&nbsp;&nbsp;&nbsp;&nbsp; <a href="feederenrollment.aspx?id=3">
            Table 4 - Feeder School</a>
    </div>
</asp:Content>
