﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using System.Text;

public partial class admin_data_desegregation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
  
            hfReportID.Value = Convert.ToString(Session["ReportID"]);
            //for preious version, isRequiredUpload and isVoluntaryUpload are undefined. So initial those reports
            bool isRequired = false;
            bool isVolunteer = false;

            var fileReq = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == 9); //required
            if (fileReq.Count > 0)
                isRequired = true;

            var fileVol = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == 10); //voluntary
            if (fileVol.Count > 0)
                isVolunteer = true;

            var data = MagnetDesegregationPlan.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));


         if (data.Count > 0)
         {
             data[0].isRequiredUpload = isRequired;
             data[0].isVoluntaryUpload = isVolunteer;
             data[0].Save();

             if (data[0].PlanType != null)
             {
                 if (data[0].isVoluntaryUpload == null && data[0].isRequiredUpload == null) //old version
                 {
                     if (data[0].PlanType == false)
                     {
                         if (data[0].isRequiredUpload == true)
                             imgRequired.Visible = true;
                     }
                     else
                     {
                         if(data[0].isVoluntaryUpload==true)
                            imgVolunteer.Visible = true;
                     }
                 }
                 else //new version #739 buglist
                 {
                     if (data[0].isRequiredUpload == true)
                         imgRequired.Visible = true;
                     else
                         imgRequired.Visible = false;

                     if (data[0].isVoluntaryUpload == true)
                         imgVolunteer.Visible = true;
                     else
                         imgVolunteer.Visible = false;
                 }
             }
         }
        }
    }
    protected void OnOCR(object sender, EventArgs e)
    {
        Response.Redirect("assurance.aspx", true);
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        var data = MagnetDesegregationPlan.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
        if (data.Count > 0)
        {
            using (System.IO.MemoryStream output = new MemoryStream())
            {
                try
                {
                    PdfStamper ps = null;

                    // read existing PDF document
                    PdfReader r = new PdfReader(Server.MapPath("../doc/desegregation.pdf"));
                    ps = new PdfStamper(r, output);
                    AcroFields af = ps.AcroFields;

                    if (data[0].PlanType != null)
                    {
                        if (data[0].PlanType == false)
                            af.SetField("RequiredPlan", "Yes");
                        else
                            af.SetField("VoluntaryPlan", "Yes");
                    }
                    ps.FormFlattening = true;

                    r.Close();
                    ps.Close();
                }
                catch (System.Exception ex)
                {
                    ILog Log = LogManager.GetLogger("EventLog");
                    Log.Error("Desegregation Plan Report:", ex);
                }
                finally
                {
                }

                int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
                MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_desegregation.pdf");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();

                output.Close();
            }
        }
    }
}