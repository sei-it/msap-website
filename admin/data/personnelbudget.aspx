﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="personnelbudget.aspx.cs" Inherits="admin_data_personnelbudget" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/tooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".screenshot").thumbPopup({
                imgSmallFlag: "../../images/question_mark-thumb.png",
                imgLargeFlag: "../../images/personnel.jpg"
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        
        <h4 style="color:#F58220" >
           <a href="quarterreports.aspx">Main Menu</a> <img src="button_arrow.jpg" alt="" width="29" height="36" />Key Personnel Budget Detail <span style="color:#F58220"><img src="button_arrow.jpg" width="29" height="36" /></span>
<asp:Label runat="server" ID="lblBudgetType"></asp:Label></h4>
        <p>
            List each key personnel position by title and the name of the employee, if available.
            For the computation, show the <b>annual salary rate</b> and the <b>percentage of time
                to be devoted to the MSAP project</b>. Compensation paid for employees engaged
            in grant activities must be consistent with that paid for similar work within the
            organization. If you anticipate carryover funds at the end of this current fiscal
            year, provide a description for those funds encumbered for services received or
            rendered for the current budget period but may have: 1) not been completed; or 2)
            not been reimbursed.
            <img src="../../images/question_mark-thumb.png" class="screenshot" alt="Budget Detail Example" />
    </p>
        <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfReportType" runat="server" />
        <asp:HiddenField ID="hfSummaryID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
        <Columns>
                <asp:BoundField DataField="PersonnelName" SortExpression="" HeaderText="Key Personnel Name" />
                <asp:BoundField DataField="PersonnelPosition" SortExpression="" HeaderText="Position" />
                <asp:BoundField DataField="AnnualSalary" SortExpression="" HeaderText="Annual Salary" />
                <asp:BoundField DataField="MSAPPercentage" SortExpression="" HeaderText="Percentage Time on MSAP" />
                <asp:BoundField DataField="ApprovedFederalFunds" SortExpression="" HeaderText="Approved Federal Funds"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="BudgetExpenditures" SortExpression="" HeaderText="Budget Expenditures"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="Carryover" SortExpression="" HeaderText="Carryover" DataFormatString="{0:D}" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <%-- Budget --%>
        <asp:Panel ID="PopupPanel" runat="server">
          <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Edit Budget Detail
                    <div style="float:right">
                        <asp:ImageButton ID="Button3" ImageUrl="../../images/close.gif" runat="server" />
                    </div>
                </div>
                <table>
                    <tr>
                        <td>
                            Key Personnel Name/Position:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPersonnel" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Key Personnel Position:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPosition" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Annual Salary:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAnnualSalary" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtAnnualSalary"
                                Mask="999,999,999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Percentage Time on MSAP:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtPercentage"
                                Mask="999" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Approved Federal Funds
                        </td>
                        <td>
                            <asp:TextBox ID="txtApprovedFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtApprovedFunds"
                                Mask="999,999,999" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr align="right">
                        <td align="right">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                        </td>
                    </tr>
                </table>
            </div>
          <asp:Button ID="Button4" runat="server" CssClass="msapBtn" Text="Add Budget"
            OnClick="OnNewBudget" />            
          </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button3" CancelControlID="Button3"
            BackgroundCssClass="magnetMPE" Y="20" />
        <p>
            Key Personnel Summary (100 word maximum)
        </p>
        <asp:TextBox ID="txtSummary" runat="server" Columns="100" Rows="4" TextMode="MultiLine"
            CssClass="msapDataTxt"></asp:TextBox>
        <br /><br />
       <div align="right"> <asp:Button ID="Savebutton" runat="server" Text="Save Record" OnClick="OnSaveSummary"
            CssClass="surveyBtn1" /></div>
    </div>
</asp:Content>
