﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using System.Text;

public partial class admin_data_managestatuschart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);
            LoadData();
         
            //Disable add new objective button for non-msap people
            //if (!Context.User.IsInRole("Administrators") && !Context.User.IsInRole("Data"))
            if (!Context.User.IsInRole("Administrators"))
            {
                Newbutton.Visible = false;
            }
            else
                Newbutton.Visible = true;
        }
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        Response.Redirect("statuschart.aspx?option=" + (sender as LinkButton).CommandArgument, true);
    }
    protected void OnAddData(object sender, EventArgs e)
    {
        Response.Redirect("statuschart.aspx?option=0", true);
    }
    private class DataRow
    {
        public int ID { get; set; }
        public string ActiveAction { get; set; }
        public string ActiveStatus { get; set; }
        public string ProjectObjective { get; set; }
    }
    protected void OnRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Disable edit button if not active
            if (DataBinder.Eval(e.Row.DataItem, "ActiveStatus").ToString().Equals("No"))
            {
                ((LinkButton)e.Row.Cells[3].Controls[1]).Enabled = false;
                e.Row.CssClass = "GreyedRow";
            }
        }
    }
    protected void OnDeactivate(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        MagnetProjectObjective magnetProjectObjective = MagnetProjectObjective.SingleOrDefault(x => x.ID == id);
        magnetProjectObjective.IsActive = !magnetProjectObjective.IsActive;
        magnetProjectObjective.Save();
        LoadData();
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetProjectObjectives
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   select new
                   {
                       m.ID,
                       m.ProjectObjective,
                       ActiveStatus = (m.IsActive == true ? "Yes" : "No"),
                       ActiveAction = (m.IsActive == true ? "Deactivate" : "Activate")
                   };
        GridView1.DataSource = data;
        GridView1.DataBind();
        /*if (data.Count() >= 6)
        {
            Newbutton.Visible = false;
            lblMessage.Text = "No more objectives may be added.";
        }
        else
        {
            Newbutton.Visible = true;
            lblMessage.Text = "";
        }*/
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        var Data = MagnetProjectObjective.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value)).OrderBy(x=>x.ID).ToList();
        if (Data.Count > 0)
        {
            int granteeID = (int)Session["GranteeID"];
            MagnetGrantee MGGrantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

            try
            {
                using (System.IO.MemoryStream Output = new MemoryStream())
                {
                    //Print
                    Document document = new Document();
                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, Output);
                    document.Open();
                    PdfContentByte pdfContentByte = pdfWriter.DirectContent;

                    List<string> TmpPDFs = new List<string>();
                    int ID = 1;
                    StringBuilder sb = new StringBuilder();
                    foreach (MagnetProjectObjective ObjData in Data)
                    {
                        sb.Clear();
                        var PerformanceData = MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == ObjData.ID && x.IsActive==true).OrderBy(x=>x.ID).ToList();
                        if (PerformanceData.Count > 0)
                        {
                            //int idx = 1;
                            //foreach (MagnetGrantPerformance Performance in PerformanceData)
                            string TmpPDF = "";
                            PdfStamper ps = null;
                            PdfReader r = null;
                            AcroFields af = null;
                           // int current = 0;
                            //foreach(MagnetGrantPerformance Performance in PerformanceData)
                            for (int idx = 1; idx <= PerformanceData.Count; idx++)
                            {
                                MagnetGrantPerformance Performance = PerformanceData[idx - 1];

                                //if (idx != current && (idx % 2 == 1))
                                //{
                                //    current = idx;
                                    TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(TmpPDF))
                                        File.Delete(TmpPDF);
                                    TmpPDFs.Add(TmpPDF);

                                    r = new PdfReader(Server.MapPath("../doc/statuschart.pdf"));
                                    ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));
                                    af = ps.AcroFields;
                                //}

                                af.SetField("PRAward", MGGrantee.PRAward);
                                af.SetField("ID", ID.ToString());
                                af.SetField("MeasureID", ID.ToString() + "." + (idx < 27 ? Convert.ToString((char)(96 + idx)) : Convert.ToString(((char)(96 + idx - 26)) + Convert.ToString((char)(96 + idx - 26)))));
                                if (!string.IsNullOrEmpty(Performance.ExplanationProgress))
                                {
                                    if (idx > 1)
                                        sb.AppendLine("");
                                    sb.AppendLine(ID.ToString() + "." + (idx < 27 ? Convert.ToString((char)(96 + idx)) : Convert.ToString(((char)(96 + idx - 26)) + Convert.ToString((char)(96 + idx - 26)))));
                                    sb.Append(Performance.ExplanationProgress);
                                }
                                if (ObjData.StatusUpdate != null && (bool)ObjData.StatusUpdate == true) af.SetField("UpdateStatus", "Yes");
                                af.SetField("ProjectObjective", (bool)ObjData.IsActive ? ObjData.ProjectObjective : "DEACTIVATED " + ObjData.ProjectObjective);

                                //foreach (MagnetGrantPerformance Performance in PerformanceData)
                                {
                                    af.SetField("Performance MeasureRow", (bool)Performance.IsActive ? Performance.PerformanceMeasure : "DEACTIVATED " + Performance.PerformanceMeasure);
                                    af.SetField("Measure Type", Performance.MeasureType);
                                    af.SetField("Target Raw Number", Performance.TargetNumber == null ? "" : Convert.ToString(Performance.TargetNumber));
                                    if (!string.IsNullOrEmpty(Performance.TargetRatio1))
                                    {
                                        af.SetField("Target Ratio Top", Performance.TargetRatio1);
                                        af.SetField("Target Ratio Bottom", Performance.TargetRatio2);
                                    }
                                    af.SetField("Target Percentage", Performance.TargetPercentage == null ? "" : Convert.ToString(Performance.TargetPercentage));
                                    af.SetField("APD Raw Number", Performance.ActualNumber == null ? "" : Convert.ToString(Performance.ActualNumber));
                                    if (!string.IsNullOrEmpty(Performance.AcutalRatio1))
                                    {
                                        af.SetField("APD Ratio Top", Performance.AcutalRatio1);
                                        af.SetField("APD Ratio Bottom", Performance.AcutalRatio2);
                                    }
                                    af.SetField("APD Percent", Performance.ActualPercentage == null ? "" : Convert.ToString(Performance.ActualPercentage));
                                }

                                //Close document
                                //if (idx % 2 == 0 || idx == PerformanceData.Count)
                                //{
                                    ps.FormFlattening = true;
                                    r.Close();
                                    ps.Close();

                                    //Add to final report
                                    PdfReader reader = new PdfReader(TmpPDF);
                                    document.SetPageSize(PageSize.A4.Rotate());
                                    document.NewPage();
                                    PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                                    pdfContentByte.AddTemplate(importedPage, 27, -15);
                                    reader.Close();
                                //}
                            }

                            if (sb.Length > 0)
                            {
                                //Add page for explanation of progress
                                string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                string FinalPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                if (File.Exists(FinalPDF))
                                    File.Delete(FinalPDF);
                                TmpPDFs.Add(FinalPDF);
                                string Stationery = Server.MapPath("../doc/sc") + MagnetGrantee.SingleOrDefault(x => x.ID == granteeID).PRAward + ".pdf";
                                if (File.Exists(ContentPDF))
                                    File.Delete(ContentPDF);
                                TmpPDFs.Add(ContentPDF);

                                Document ContentDocument = new Document(PageSize.A4.Rotate(), 40f, 80f, 120f, 30f);

                                PdfWriter ContentWriter = PdfWriter.GetInstance(ContentDocument, new FileStream(ContentPDF, FileMode.Create));
                                ContentDocument.Open();
                                //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                                ContentDocument.Add(new Paragraph(new Chunk(sb.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                                ContentDocument.Close();

                                //Add stamp
                                PdfReader o_reader = new PdfReader(ContentPDF);
                                PdfReader s_reader = new PdfReader(Stationery);
                                PdfStamper stamper = new PdfStamper(o_reader, new FileStream(FinalPDF, FileMode.Create));

                                PdfImportedPage page = stamper.GetImportedPage(s_reader, 1);
                                int n = o_reader.NumberOfPages;
                                PdfContentByte background;
                                for (int i = 1; i <= n; i++)
                                {

                                    background = stamper.GetUnderContent(i);
                                    background.AddTemplate(page, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                }
                                stamper.Close();

                                //Add to output
                                PdfReader freader = new PdfReader(FinalPDF);
                                for (int j = 1; j <= freader.NumberOfPages; j++)
                                {
                                    document.SetPageSize(PageSize.A4.Rotate());
                                    document.NewPage();
                                    PdfImportedPage fimportedPage = pdfWriter.GetImportedPage(freader, j);
                                    pdfContentByte.AddTemplate(fimportedPage, 0, -1.0F, 1.0F, 0, 30, PageSize.A4.Width);
                                }
                                freader.Close();
                            }
                        }
                        else
                        {
                            //No active measure, only print out objecitve
                            string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(TmpPDF))
                                File.Delete(TmpPDF);
                            TmpPDFs.Add(TmpPDF);

                            PdfStamper ps = null;
                            PdfReader r = null;
                            r = new PdfReader(Server.MapPath("../doc/statuchartp1.pdf"));
                            ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));
                            AcroFields af = ps.AcroFields;


                            af.SetField("PRAward", MGGrantee.PRAward);
                            af.SetField("ID", ID.ToString());
                            if (ObjData.StatusUpdate != null && (bool)ObjData.StatusUpdate == true) af.SetField("UpdateStatus", "Yes");
                            af.SetField("ProjectObjective", ObjData.ProjectObjective);

                            ps.FormFlattening = true;

                            r.Close();
                            ps.Close();

                            //Add to final report
                            PdfReader reader = new PdfReader(TmpPDF);
                            document.SetPageSize(PageSize.A4.Rotate());
                            document.NewPage();
                            PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                            pdfContentByte.AddTemplate(importedPage, 27, -15);
                            reader.Close();
                        }
                        ID++;
                    }

                    document.Close();
                    foreach (string str in TmpPDFs)
                    {
                        if (File.Exists(str))
                            File.Delete(str);
                    }

                    MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_statuschart.pdf");
                    Response.OutputStream.Write(Output.GetBuffer(), 0, Output.GetBuffer().Length);
                    Response.OutputStream.Flush();
                    Response.OutputStream.Close();

                    Output.Close();
                }
            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("GPRA report:", ex);
            }
            finally
            {
            }

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Please activate at least one Objective.');</script>", false);
        }
    }
}