﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="budgetreport.aspx.cs" Inherits="admin_data_budgetreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Budget - Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="Manager1" runat="server"></ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfReportType" runat="server" />
    <asp:HiddenField ID="hfSummaryID" runat="server" />
    <br/><br/><table>
        <tr>
            <td>
                Indirect Costs:
            </td>
            <td>
                <asp:TextBox ID="txtIndirectCosts" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:maskededitextender id="MaskedEditExtender1" targetcontrolid="txtIndirectCosts" mask="999,999,999.99"
                    messagevalidatortip="true" runat="server" oninvalidcssclass="MaskedEditError"
                    masktype="Number" inputdirection="RightToLeft" acceptnegative="None" errortooltipenabled="True" />
            </td>
        </tr>
        <tr>
            <td>
                Training Stipends:
            </td>
            <td>
                <asp:TextBox ID="txtTrainingStipends" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                <ajax:maskededitextender id="MaskedEditExtender2" targetcontrolid="txtTrainingStipends" mask="999,999,999.99"
                    messagevalidatortip="true" runat="server" oninvalidcssclass="MaskedEditError"
                    masktype="Number" inputdirection="RightToLeft" acceptnegative="None" errortooltipenabled="True" />
            </td>
        </tr>
        <tr>
            <td>
                Budget Narrative (250 word maximum)
            </td>
            <td>
                <asp:TextBox ID="txtSummary" runat="server" Columns="90" Rows="8" TextMode="MultiLine"
                    CssClass="msapDataTxt"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <div align="right"><asp:Button ID="Savebutton" runat="server" Text="Save Record" OnClick="OnSaveSummary"
        CssClass="surveyBtn1" /></div>
    <p>&nbsp;
        </p>
</asp:Content>
