﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_AYP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //Report type
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 21 && x.TypeIndex > -9))
            {
                ddlAYPType.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 23 && x.TypeIndex > -9))
            {
                ddlReportType.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 22 && x.TypeIndex > -9))
            {
                ddlAmericanIndian.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlBlack.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlHispanic.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlHawaiian.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlWhite.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlMoreRaces.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlEconomically.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlEnglishLearner.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("manageayp.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadData(id);
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetAYP item = new MagnetAYP();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetAYP.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
        }
        item.StageID = Convert.ToInt32(ddlAYPType.SelectedValue);
        item.TestType = Convert.ToInt32(ddlReportType.SelectedValue);
        item.AmericanIndianTested = string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(txtAmericanIndian.Text);
        item.AmericanIndianProficient = string.IsNullOrEmpty(txtAmericanIndianProficient.Text) ? 0 : Convert.ToInt32(txtAmericanIndianProficient.Text);
        item.AmericanIndianPercentage = string.IsNullOrEmpty(txtAmericanIndianPercentage.Text) ? 0 : Convert.ToDecimal(txtAmericanIndianPercentage.Text);
        item.AmericanIndianAYP = Convert.ToInt32(ddlAmericanIndian.SelectedValue);
        item.BlackTested = string.IsNullOrEmpty(txtBlack.Text) ? 0 : Convert.ToInt32(txtBlack.Text);
        item.BlackProficient = string.IsNullOrEmpty(txtBlackProficient.Text) ? 0 : Convert.ToInt32(txtBlackProficient.Text);
        item.BlackPercentage = string.IsNullOrEmpty(txtBlackPercentage.Text) ? 0 : Convert.ToDecimal(txtBlackPercentage.Text);
        item.BlackAYP = Convert.ToInt32(ddlBlack.SelectedValue);
        item.HispanicTested = string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(txtHispanic.Text);
        item.HispanicProficient = string.IsNullOrEmpty(txtHispanicProficient.Text) ? 0 : Convert.ToInt32(txtHispanicProficient.Text);
        item.HispanicPercentage = string.IsNullOrEmpty(txtHispanicPercentage.Text) ? 0 : Convert.ToDecimal(txtHispanicPercentage.Text);
        item.HispanicAYP = Convert.ToInt32(ddlHispanic.SelectedValue);
        item.WhiteTested = string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(txtWhite.Text);
        item.WhiteProficient = string.IsNullOrEmpty(txtWhiteProficient.Text) ? 0 : Convert.ToInt32(txtWhiteProficient.Text);
        item.WhitePercentage = string.IsNullOrEmpty(txtWhitePercentage.Text) ? 0 : Convert.ToDecimal(txtWhitePercentage.Text);
        item.WhiteAYP = Convert.ToInt32(ddlWhite.SelectedValue);
        item.HawaiianTested = string.IsNullOrEmpty(txtHawaiian.Text) ? 0 : Convert.ToInt32(txtHawaiian.Text);
        item.HawaiianProficient = string.IsNullOrEmpty(txtHawaiianProficient.Text) ? 0 : Convert.ToInt32(txtHawaiianProficient.Text);
        item.HawaiianPercentage = string.IsNullOrEmpty(txtHawaiianPercentage.Text) ? 0 : Convert.ToDecimal(txtHawaiianPercentage.Text);
        item.HawaiianAYP = Convert.ToInt32(ddlHawaiian.SelectedValue);
        item.MultiRacialTested = string.IsNullOrEmpty(txtMoreRaces.Text) ? 0 : Convert.ToInt32(txtMoreRaces.Text);
        item.MultiRacialProficient = string.IsNullOrEmpty(txtMoreRacesProficient.Text) ? 0 : Convert.ToInt32(txtMoreRacesProficient.Text);
        item.MultiRacialPercentage = string.IsNullOrEmpty(txtMoreRacesPercentage.Text) ? 0 : Convert.ToDecimal(txtMoreRacesPercentage.Text);
        item.MultiRacialAYP = Convert.ToInt32(ddlMoreRaces.SelectedValue);
        item.EconomicallyDisadvantagedTested = string.IsNullOrEmpty(txtEconomically.Text) ? 0 : Convert.ToInt32(txtEconomically.Text);
        item.EconomicallyDisadvantagedProficient = string.IsNullOrEmpty(txtEconomicallyProficient.Text) ? 0 : Convert.ToInt32(txtEconomicallyProficient.Text);
        item.EconomicallyDisadvantagedPercentage = string.IsNullOrEmpty(txtEconomicallyPercentage.Text) ? 0 : Convert.ToDecimal(txtEconomicallyPercentage.Text);
        item.EconomicallyDisadvantagedAYP = Convert.ToInt32(ddlEconomically.SelectedValue);
        item.EnglishLearnerTested = string.IsNullOrEmpty(txtEnglishLearner.Text) ? 0 : Convert.ToInt32(txtEnglishLearner.Text);
        item.EnglishLearnerProficient = string.IsNullOrEmpty(txtEnglishLearnerProficient.Text) ? 0 : Convert.ToInt32(txtEnglishLearnerProficient.Text);
        item.EnglishLearnerPercentage = string.IsNullOrEmpty(txtEnglishLearnerPercentage.Text) ? 0 : Convert.ToDecimal(txtEnglishLearnerPercentage.Text);
        item.EnglishLearnerAYP = Convert.ToInt32(ddlEnglishLearner.SelectedValue);
        item.TotalStudentsTested = string.IsNullOrEmpty(txtTotalReading.Text) ? 0 : Convert.ToInt32(txtTotalReading.Text);
        item.TotalStudentsProficient = string.IsNullOrEmpty(txtTotalReadingProficient.Text) ? 0 : Convert.ToInt32(txtTotalReadingProficient.Text);
        item.TotalStudentsPercentage = string.IsNullOrEmpty(txtTotalReadingPercentage.Text) ? 0 : Convert.ToDecimal(txtTotalReadingPercentage.Text);
        item.Save();
        hfID.Value = item.ID.ToString();
    }
    private void LoadData(int SIPID)
    {
        MagnetAYP item = MagnetAYP.SingleOrDefault(x => x.ID == SIPID);
        ddlAYPType.SelectedValue = Convert.ToString(item.StageID);
        ddlReportType.SelectedValue = Convert.ToString(item.TestType);
        txtAmericanIndian.Text = Convert.ToString(item.AmericanIndianTested);
        txtAmericanIndianProficient.Text = Convert.ToString(item.AmericanIndianProficient);
        txtAmericanIndianPercentage.Text = Convert.ToString(item.AmericanIndianPercentage);
        ddlAmericanIndian.SelectedValue = Convert.ToString(item.AmericanIndianAYP);
        txtBlack.Text = Convert.ToString(item.BlackTested);
        txtBlackProficient.Text = Convert.ToString(item.BlackProficient);
        txtBlackPercentage.Text = Convert.ToString(item.BlackPercentage);
        ddlBlack.SelectedValue = Convert.ToString(item.BlackAYP);
        txtHispanic.Text = Convert.ToString(item.HispanicTested);
        txtHispanicProficient.Text = Convert.ToString(item.HispanicProficient);
        txtHispanicPercentage.Text = Convert.ToString(item.HispanicPercentage);
        ddlHispanic.SelectedValue = Convert.ToString(item.HispanicAYP);
        txtWhite.Text = Convert.ToString(item.WhiteTested);
        txtWhiteProficient.Text = Convert.ToString(item.WhiteProficient);
        txtWhitePercentage.Text = Convert.ToString(item.WhitePercentage);
        ddlWhite.SelectedValue = Convert.ToString(item.WhiteAYP);
        txtHawaiian.Text = Convert.ToString(item.HawaiianTested);
        txtHawaiianProficient.Text = Convert.ToString(item.HawaiianProficient);
        txtHawaiianPercentage.Text = Convert.ToString(item.HawaiianPercentage);
        ddlHawaiian.SelectedValue = Convert.ToString(item.HawaiianAYP);
        txtMoreRaces.Text = Convert.ToString(item.MultiRacialTested);
        txtMoreRacesProficient.Text = Convert.ToString(item.MultiRacialProficient);
        txtMoreRacesPercentage.Text = Convert.ToString(item.MultiRacialPercentage);
        ddlMoreRaces.SelectedValue = Convert.ToString(item.MultiRacialAYP);
        txtEconomically.Text = Convert.ToString(item.EconomicallyDisadvantagedTested);
        txtEconomicallyProficient.Text = Convert.ToString(item.EconomicallyDisadvantagedProficient);
        txtEconomicallyPercentage.Text = Convert.ToString(item.EconomicallyDisadvantagedPercentage);
        ddlEconomically.SelectedValue = Convert.ToString(item.EconomicallyDisadvantagedAYP);
        txtEnglishLearner.Text = Convert.ToString(item.EnglishLearnerTested);
        txtEnglishLearnerProficient.Text = Convert.ToString(item.EnglishLearnerProficient);
        txtEnglishLearnerPercentage.Text = Convert.ToString(item.EnglishLearnerPercentage);
        ddlEnglishLearner.SelectedValue = Convert.ToString(item.EnglishLearnerAYP);
        txtTotalReading.Text = Convert.ToString(item.TotalStudentsTested);
        txtTotalReadingProficient.Text = Convert.ToString(item.TotalStudentsProficient);
        txtTotalReadingPercentage.Text = Convert.ToString(item.TotalStudentsPercentage);
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("manageayp.aspx");
    }
}