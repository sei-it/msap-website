﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_renovationbudget : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/default.aspx");
            }
            if (Request.QueryString["type"].Equals("1"))
            {
                hfReportType.Value = "11";
                lblBudgetType.Text = "Federal Funds";
            }
            else
            {
                hfReportType.Value = "12";
                lblBudgetType.Text = "Non Federal Funds";
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == Convert.ToInt32(hfReportType.Value));
            if (data.Count == 1)
            {
                hfSummaryID.Value = data[0].ID.ToString();
                txtSummary.Text = data[0].ReportSummary;
            }
            RegisterHelper();
            LoadData();
        }
    }
    private void RegisterHelper()
    {
        GridViewHelper helper = new GridViewHelper(this.GridView1);
        helper.RegisterSummary("ApprovedFederalFunds", SummaryOperation.Sum);
        helper.RegisterSummary("BudgetExpenditures", SummaryOperation.Sum);
        helper.RegisterSummary("Carryover", SummaryOperation.Sum);
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetRenovationBudgets
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.ReportType == Convert.ToInt32(hfReportType.Value)
                   orderby m.Purpose
                   select new
                   {
                       m.ID,
                       m.Purpose,
                       m.DescriptionOfWork,
                       m.ApprovedFederalFunds,
                       BudgetExpenditures=m.BudgetExpenditures==null?0:m.BudgetExpenditures,
                       Carryover = ((m.ApprovedFederalFunds != null && m.BudgetExpenditures != null) ? m.ApprovedFederalFunds - m.BudgetExpenditures : 0)
                   };
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtPurpose.Text = "";
        txtDescriptionWork.Text = "";
        txtApprovedFunds.Text = "";
        txtExpdenditures.Text = "";
        hfID.Value = "";
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetRenovationBudget data = MagnetRenovationBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtPurpose.Text = data.Purpose;
        txtDescriptionWork.Text = data.DescriptionOfWork;
        if (data.ApprovedFederalFunds != null) txtApprovedFunds.Text = Convert.ToString(data.ApprovedFederalFunds);
        if (data.BudgetExpenditures != null) txtExpdenditures.Text = Convert.ToString(data.BudgetExpenditures);
        mpeWindow.Show();
    }
    protected void OnNewBudget(object sender, EventArgs e)
    {
        ClearFields();
        mpeWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetRenovationBudget data = new MagnetRenovationBudget();
        if (!string.IsNullOrEmpty(hfID.Value))
            data = MagnetRenovationBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
            data.ReportType = Convert.ToInt32(hfReportType.Value);
        }
        data.ApprovedFederalFunds = string.IsNullOrEmpty(txtApprovedFunds.Text) ? 0 : Convert.ToInt32(txtApprovedFunds.Text);
        data.Purpose = txtPurpose.Text;
        data.Purpose = txtPurpose.Text;
        data.DescriptionOfWork = txtDescriptionWork.Text;
        data.BudgetExpenditures = string.IsNullOrEmpty(txtExpdenditures.Text) ? 0 : Convert.ToDecimal(txtExpdenditures.Text);
        data.Save();
        RegisterHelper();
        LoadData();
    }
    protected void OnSaveSummary(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSummary.Text))
        {
            //Save to database
            MagnetBudgetSummary summary = new MagnetBudgetSummary();
            if (!string.IsNullOrEmpty(hfSummaryID.Value))
                summary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSummaryID.Value));
            else
            {
                summary.ReportID = Convert.ToInt32(hfReportID.Value);
                summary.ReportType = Convert.ToInt32(hfReportType.Value);
            }
            summary.ReportSummary = txtSummary.Text;
            summary.Save();
        }
    }
}