﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="Copy of budgetform.aspx.cs" Inherits="admin_data_budgetform" ErrorPage="~/Error_page.aspx"
    ViewStateMode="Disabled" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Budget Summary
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .msapDataTxtSmall
        {
            width: 150px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            //if(<% =(Page.IsPostBack).ToString().ToLower() %> = false)
            {
                var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
                var tmp2 = $('input[name=ctl00$ContentPlaceHolder1$hfFileID]').val();
                $('input[name=ctl00$ContentPlaceHolder1$hfFileID]').val('0');
                if (tmp == "0" && tmp2 == "0") {
                    return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
                }
            }
        }
        function PreventConfirmation() {
            $('input[name=ctl00$ContentPlaceHolder1$hfFileID]').val('1');
        }
        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
            $('.Federal').keyup(function () {
                var total;
                total = 0;
                $('.Federal1').each(function (index) {
                    //$(this).val((!$(this).val() ? "0" : $(this).val()));
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });

                $('#txtDirectCosts').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
                $('.Federal2').each(function (index) {
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });
                $('#txtTotalCosts').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
            });
            $('.NonFederal').keyup(function () {
                var total;
                total = 0;
                $('.NonFederal1').each(function (index) {
                    //$(this).val((!$(this).val() ? "0" : $(this).val()));
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });
                $('#txtDirectCostsNF').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
                $('.NonFederal2').each(function (index) {
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });
                $('#txtTotalCostsNF').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
            });
        });
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {
                var who = document.getElementsByName('<%= FileUpload1.UniqueID %>')[0];
                who.value = "";

                var who2 = who.cloneNode(false);
                who2.onchange = who.onchange;
                who.parentNode.replaceChild(who2, who);

                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }
        function checkWordLen(obj, wordLen, cid) {
            var len = obj.value.split(/[\s]+/);
            $("#" + cid).val(len.length.toString());
            if (len.length > wordLen) {
                alert("You've exceeded the " + wordLen + " word limit for this field!");
                obj.value = obj.SavedValue;
                //obj.value = obj.value.substring(0, wordLen - 1);
                len = obj.value.split(/[\s]+/);
                $("#" + cid).val(len.length.toString());
                return false;
            } else {
                obj.SavedValue = obj.value;
            }
            return true;
        }
</script>
    <style>
        .msapDataTbl tr td:first-child
        {
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            Budget Summary</h4>
        <div class="titles_noTabs">
            Budget Summary</div>
        <br />
        <p class="clear">
            This form applies to individual U.S. Department of Education (ED) discretionary
            grant programs. Unless directed otherwise, provide the same budget information for
            each year of the multiyear funding request. Pay attention to specific instructions
            in the Dear Colleague Letter. You may access the General Administrative Regulations,
            34 CFR 74 - 86 and 97-99, on ED's website at: <a href="http://www.ed.gov/policy/fund/reg/edgarReg/edgar.html"
                target="_blank">http://www.ed.gov/policy/fund/reg/edgarReg/edgar.html</a>.
            <br />
            <br />
            <b>You must consult with your Business Office prior to submitting this form.<img
                src="../../images/question_mark-thumb.png" title="All applicants must complete all items listed Federal Funds column.<br /><br />If you are required to provide or volunteer to provide cost-sharing or matching funds or other non-Federal resources to the project, these should be shown for each applicable budget category on items 1-11 of Non-Federal Funds." /></b></p>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfFileID" runat="server" Value="0" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <div>
            <table class="msapDataTbl">
                <tr>
                    <th>
                        &nbsp;
                    </th>
                    <th class="TDWithBottomBorder">
                        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;Federal Funds
                        <img src="../../images/question_mark-thumb.png" title="For the current project year, of which funding is requested, show the total amount requested for each applicable budget category." /></b>
                    </th>
                    <th class="TDWithBottomBorder">
                        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;Non-Federal Funds
                        <img src="../../images/question_mark-thumb.png" title="For the current project year, if matching funds or other contributions are provided, show the total contribution for each applicable budget category." /></b>
                    </th>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        1. Personnel:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtPersonnel" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="1" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtPersonnelNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="13" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        2. Fringe Benefits:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtFringe" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="2" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtFringeNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="14" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        3. Travel:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTravel" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="3" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTravelNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="15" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        4. Equipment:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtEquipment" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="4" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtEquipmentNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="16" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        5. Supplies:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtSupplies" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="5" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtSuppliesNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="17" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        6. Contractual:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtContractual" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="6" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtContractualNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="18" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        7. Construction:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtConstruction" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="7" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtConstructionNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="19" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        8. Other:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtOther" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="8" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtOtherNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="20" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="color: #4e8396;">
                        9. Total Direct Costs (lines 1-8):
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 1-8." /></b>
                    </td>
                    <td align="center">
                        <asp:TextBox ID="txtDirectCosts" Enabled="false" ClientIDMode="Static" runat="server"
                            CssClass="msapDataTxtSmall" TabIndex="9" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtDirectCostsNF" Enabled="false" ClientIDMode="Static" runat="server"
                            CssClass="msapDataTxtSmall" TabIndex="21" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        10. Indirect Costs:*
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtIndirectCosts" runat="server" CssClass="msapDataTxtSmall change Federal Federal2"
                            TabIndex="10" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtIndirectCostsNF" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal2"
                            TabIndex="22" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="color: #4e8396;">
                        11. Training Stipends:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTrainingStipends" runat="server" CssClass="msapDataTxtSmall change Federal Federal2"
                            TabIndex="11" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTrainingStipendsNF" runat="server"
                            CssClass="msapDataTxtSmall change NonFederal NonFederal2" TabIndex="23" Type="Currency"
                            NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline" style="color: #4e8396;">
                        12. Total Costs (lines 9-11):
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 9-11." /></b>
                    </td>
                    <td align="center">
                        <asp:TextBox ID="txtTotalCosts" ClientIDMode="Static" Enabled="false" runat="server"
                            CssClass="msapDataTxtSmall" TabIndex="12" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtTotalCostsNF" ClientIDMode="Static" Enabled="false" runat="server"
                            CssClass="msapDataTxtSmall" TabIndex="24" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table class="msapDataTbl">
                        <tr>
                            <td colspan="4" class="TDWithBottomNoRightBorder" style="color: #4e8396;">
                                <strong>*Indirect Cost Information (To Be Completed by Your Business Office):</strong>
                                <img src="../../images/question_mark-thumb.png" title="For federal funds only." /><br />
                                <br />
                                If you are requesting reimbursement for indirect costs on line 10, please answer
                                the following questions:
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #4e8396;" class="TDWithTopNoRightBorder" valign="top">
                                (1)
                            </td>
                            <td style="color: #4e8396; width: 50%;" class="TDWithTopBorder">
                                Do you have an Indirect Cost Rate Agreement approved by the Federal government?
                            </td>
                            <td class="TDWithTopNoRightBorder" style="padding-left: 0px; width: 120px;">
                                <asp:RadioButtonList ID="rblRageAggrement" CssClass="DotnetTbl change" runat="server"
                                    RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="OnAgreementYes">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="This field is required!"
                                    ControlToValidate="rblRageAggrement" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #4e8396;" class="TDWithTopNoRightBorder">
                                (2)
                            </td>
                            <td style="color: #4e8396;">
                                If yes, please provide the following information:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">
                                &nbsp;
                            </td>
                            <td style="color: #4e8396; vertical-align: top;">
                                Period covered by the Indirect Cost Rate Agreement:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2">
                                From:
                                <asp:TextBox CssClass="msapDataTxtSmall change" Width="80" ID="txtAgreementFrom"
                                    runat="server"></asp:TextBox>
                                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtAgreementFrom">
                                </ajax:CalendarExtender>
                                To:
                                <asp:TextBox CssClass="msapDataTxtSmall change" Width="80" ID="txtAgreementTo" runat="server">
                        </asp:TextBox>
                                <br />
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="<strong>From</strong> date is required!"
                                    ControlToValidate="txtAgreementFrom" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="<strong>To</strong> date is required!"
                                    ControlToValidate="txtAgreementTo" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                                <br />
                                <asp:CompareValidator ID="CompareValidatorTextBox1" runat="server" ControlToValidate="txtAgreementFrom"
                                    Type="Date" Operator="DataTypeCheck" ErrorMessage="<strong>From</strong> date is not in the correct format."
                                    Display="Dynamic" />
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtAgreementTo"
                                    Type="Date" Operator="DataTypeCheck" ErrorMessage="<strong>To</strong> date is not in correct format."
                                    Display="Dynamic" />
                                <br />
                                <asp:CompareValidator runat="server" ID="cvEndTime" ControlToValidate="txtAgreementTo"
                                    ControlToCompare="txtAgreementFrom" ErrorMessage="<b>To</b> date must be later than From Date."
                                    SetFocusOnError="True" Operator="GreaterThan" Type="Date" Display="Dynamic" />
                                <ajax:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtAgreementTo">
                                </ajax:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">
                                &nbsp;
                            </td>
                            <td style="color: #4e8396;">
                                Approving Federal agency:
                            </td>
                            <td class="TDWithTopNoRightBorder" style="padding-left: 0px;">
                                <asp:RadioButtonList ID="rblAgency" CssClass="DotnetTbl change" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="true" OnSelectedIndexChanged="OnAgencyChanged">
                                    <asp:ListItem Value="1">ED</asp:ListItem>
                                    <asp:ListItem Value="0">Other</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="This field is required!"
                                    ControlToValidate="rblAgency" Display="Dynamic"></asp:RequiredFieldValidator>
                                <br />
                                <asp:Panel ID="agencyPanel" runat="server" Visible="false">
                                    (please specify):
                                    <asp:TextBox CssClass="msapDataTxtSmall change" ID="txtOtherAgency" runat="server"></asp:TextBox></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">
                                &nbsp;
                            </td>
                            <td style="color: #4e8396;">
                                Indirect Cost Rate:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2" style="padding-left: 10px; margin-left: 10px;">
                                <telerik:RadNumericTextBox MinValue="0" CssClass="msapDataTxtSmall change" ID="txtRate"
                                    runat="server" Width="100">
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Text="This field is required!"
                                    ControlToValidate="txtRate" Enabled="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #4e8396;" class="TDWithTopNoRightBorder" valign="top">
                                (3)
                            </td>
                            <td style="color: #4e8396;" valign="top">
                                For Restricted Rate Programs (check one) -- Are you using a restricted indirect
                                cost rate that:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2">
                                <asp:RadioButton ID="rbIncludedInAgreement" runat="server" GroupName="includedgroup"
                                    CssClass="change" Text="Is Included in your approved Indirect Cost Rate Agreement?" />
                                <br />
                                <asp:RadioButton ID="rb34CFR" CssClass="change" GroupName="includedgroup" runat="server"
                                    Text="Complies with 34 CFR 76.564(c)(2)?" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">
                                &nbsp;
                            </td>
                            <td style="color: #4e8396;">
                                Indirect Cost Rate
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2" style="padding-left: 10px; margin-left: 10px;">
                                <telerik:RadNumericTextBox MinValue="0" CssClass="msapDataTxtSmall change" ID="txtRICRate"
                                    runat="server" Width="100">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <table class="msapDataTbl">
                <tr>
                    <td colspan="3" class="TDWithBottomNoRightBorder" style="color: #4e8396;">
                        <strong>Budget Narrative</strong><%--<img src="../../images/question_mark-thumb.png" title="Budget summary......" />--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: #4e8396;">
                        Is there any change to the budget?
                    </td>
                    <td colspan="2" class="TDWithTopNoRightBorder">
                        <asp:RadioButtonList ID="rblBudgetChange" runat="server" CssClass="DotnetTbl change"
                            RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="OnBudgeChanged">
                            <asp:ListItem Value="0">No</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: #4e8396; vertical-align: top;">
                        If yes, what was the reason?
                    </td>
                    <td class="TDWithTopNoRightBorder">
                        <asp:TextBox ID="txtBudgetReason" runat="server" CssClass="msapDataTxtSmall_nowidth change"
                            TextMode="MultiLine" onkeyup="checkWordLen(this, 250, 'reasonlength');" Rows="3"
                            Columns="50"></asp:TextBox><br />
                        <input type="text" disabled="disabled" size="3" id="reasonlength" style="margin-top: 2px;" />
                    </td>
                </tr>
                <tr id="fileRow" runat="server" visible="false">
                    <td style="border: 0px !important;" colspan="2">
                        <table class="msapDataTbl" style="width: 300px;">
                            <tr>
                                <th colspan="2">
                                    Uploaded File
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <a id="UploadedFile" target="_blank" runat="server">
                                        <asp:Label ID="lblFile" runat="server"></asp:Label></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="Button3" runat="server" CssClass="postbutton" Text="Delete" OnClick="OnDelete" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="border: 0px !important;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan='3' class="TDWithTopNoRightBorder" style="border: 0px !important;">
                        <div style="color: #4e8396; padding-top: 10px;">
                            Please upload your budget narrative or type in the space provided.</div>
                        <asp:TextBox ID="txtBudgetSummary" runat="server" Font-Size="10" Columns="97" Rows="10"
                            TextMode="MultiLine" CssClass="msapDataTxt change"></asp:TextBox>
                        <!--  onkeyup="checkWordLen(this, 2500, 'summarylength');"-->
                        <input type="text" disabled="disabled" style="display: none;" size="2" id="summarylength" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border: 0px !important; text-align: right;">
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="msapDataTxt" />&nbsp;&nbsp;
                        <asp:Button ID="UploadBtn" CssClass="surveyBtn postbutton" runat="server" Text="Upload"
                            OnClick="OnUpload" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <asp:Button ID="Button10" runat="server" CssClass="surveyBtn1 postbutton" OnClick="OnSave"
                Text="Save Record" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn1 postbutton" OnClick="OnPrint"
                Text="Print" />
        </div>
    </div>
</asp:Content>
