﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_travelbudget : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/default.aspx");
            }
            if (Request.QueryString["type"].Equals("1"))
            {
                hfReportType.Value = "5";
                lblBudgetType.Text = "Federal Funds";
            }
            else
            {
                hfReportType.Value = "6";
                lblBudgetType.Text = "Non Federal Funds";
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == Convert.ToInt32(hfReportType.Value));
            if (data.Count == 1)
            {
                hfSummaryID.Value = data[0].ID.ToString();
                txtSummary.Text = data[0].ReportSummary;
            }
            RegisterHelper();
            LoadData();
        }
    }
    private void RegisterHelper()
    {
        GridViewHelper helper = new GridViewHelper(this.GridView1);
        helper.RegisterSummary("ApprovedFederalFunds", SummaryOperation.Sum);
        helper.RegisterSummary("BudgetExpenditures", SummaryOperation.Sum);
        helper.RegisterSummary("Carryover", SummaryOperation.Sum);
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetTravelBudgets
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.ReportType == Convert.ToInt32(hfReportType.Value)
                   orderby m.PurposeOfTravel
                   select new
                   {
                       m.ID,
                       m.PurposeOfTravel,
                       m.Location,
                       m.Item,
                       m.UnitCost,
                       m.NumberOfDays,
                       m.NumberOfPeople,
                       m.ApprovedFederalFunds,
                       BudgetExpenditures = (m.UnitCost != null ? Convert.ToInt32(m.UnitCost * m.NumberOfDays * m.NumberOfPeople) : 0),
                       Carryover = (m.UnitCost != null ? Convert.ToInt32(m.ApprovedFederalFunds - m.NumberOfDays * m.UnitCost * m.NumberOfPeople) : 0)
                   };
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtPurpose.Text = "";
        txtLocation.Text = "";
        txtItem.Text = "";
        txtUnitCost.Text = "";
        txtDays.Text = "";
        txtPeople.Text = "";
        txtApprovedFunds.Text = "";
        hfID.Value = "";
    }
    protected void OnNewBudget(object sender, EventArgs e)
    {
        ClearFields();
        mpeWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetTravelBudget data = MagnetTravelBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtPurpose.Text = data.PurposeOfTravel;
        txtLocation.Text = data.Location;
        txtItem.Text = data.Item;
        if (data.ApprovedFederalFunds != null) txtApprovedFunds.Text = Convert.ToString(data.ApprovedFederalFunds);
        if (data.UnitCost != null) txtUnitCost.Text = Convert.ToString(data.UnitCost);
        if (data.NumberOfDays != null) txtDays.Text = Convert.ToString(data.NumberOfDays);
        if (data.NumberOfPeople != null) txtPeople.Text = Convert.ToString(data.NumberOfPeople);
        mpeWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetTravelBudget data = new MagnetTravelBudget();
        if (!string.IsNullOrEmpty(hfID.Value))
            data = MagnetTravelBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
            data.ReportType = Convert.ToInt32(hfReportType.Value);
        }
        data.ApprovedFederalFunds = string.IsNullOrEmpty(txtApprovedFunds.Text) ? 0 : Convert.ToInt32(txtApprovedFunds.Text); data.PurposeOfTravel = txtPurpose.Text;
        data.Location = txtLocation.Text;
        data.Item = txtItem.Text;
        data.UnitCost = string.IsNullOrEmpty(txtUnitCost.Text) ? 0 : Convert.ToDecimal(txtUnitCost.Text);
        data.NumberOfDays = string.IsNullOrEmpty(txtDays.Text) ? 0 : Convert.ToInt32(txtDays.Text);
        data.NumberOfPeople = string.IsNullOrEmpty(txtPeople.Text) ? 0 : Convert.ToInt32(txtPeople.Text);
        data.Save();
        RegisterHelper();
        LoadData();
    }
    protected void OnSaveSummary(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSummary.Text))
        {
            //Save to database
            MagnetBudgetSummary summary = new MagnetBudgetSummary();
            if (!string.IsNullOrEmpty(hfSummaryID.Value))
                summary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSummaryID.Value));
            else
            {
                summary.ReportID = Convert.ToInt32(hfReportID.Value);
                summary.ReportType = Convert.ToInt32(hfReportType.Value);
            }
            summary.ReportSummary = txtSummary.Text;
            summary.Save();
        }
    }
}