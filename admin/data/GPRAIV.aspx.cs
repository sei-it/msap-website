﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_gpraiv : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] == null)
            Response.Redirect("managegpra.aspx");
        else
        {
            LinkI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
            tabPartI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
            LinkII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
            tabPartII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
            LinkIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
            tabPartIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
            LinkV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
            tabPartV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
            LinkVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];
            tabPartVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];
        }
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);
            
            //Report type
            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;

            if (Request.QueryString["id"] == null)
                Response.Redirect("managegpra.aspx");
            else
            {
                int schoolID = Convert.ToInt32(Request.QueryString["id"]);
                lblSchoolName.Text = MagnetSchool.SingleOrDefault(x => x.ID == schoolID).SchoolName;

                lblReportPeriod.Text = MagnetReportPeriod.SingleOrDefault(x => x.ID == (Convert.ToInt32(Session["ReportPeriodID"]))).ReportPeriod.Replace(" ", "");
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == Convert.ToInt32(hfReportID.Value));
                if (gpraData.Count > 0)
                {
                    hfID.Value = gpraData[0].ID.ToString();

                    //Cost per student 
                    var performanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 7);
                    if (performanceMeasure.Count > 0)
                    {
                        MagnetGPRAPerformanceMeasure Item = performanceMeasure[0];
                        hfPerformanceMeasure.Value = Item.ID.ToString();
                        txtTotalSutdents.Text = Convert.ToString(Item.Total);
                        txtAnnualBudget.Text = Convert.ToString(Item.Budget);
                    }

                }
                else
                {
                    MagnetGPRA item = new MagnetGPRA();
                    item.ReportID = Convert.ToInt32(hfReportID.Value);
                    item.SchoolID = schoolID;
                    item.Save();
                    hfID.Value = item.ID.ToString();

                    MagnetGPRAPerformanceMeasure mpm = new MagnetGPRAPerformanceMeasure();
                    mpm.ReportType = 7;
                    mpm.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    mpm.Save();
                    hfPerformanceMeasure.Value = mpm.ID.ToString();
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        int schoolID = Convert.ToInt32(Request.QueryString["id"]);
        if (string.IsNullOrEmpty(hfID.Value))
        {
            MagnetGPRA Newitem = new MagnetGPRA();
            Newitem.ReportID = Convert.ToInt32(hfReportID.Value);
            Newitem.SchoolID = schoolID;
            Newitem.Save();
            hfID.Value = Newitem.ID.ToString();
        }

        if (string.IsNullOrEmpty(hfPerformanceMeasure.Value))
        {
            MagnetGPRAPerformanceMeasure mpm = new MagnetGPRAPerformanceMeasure();
            mpm.ReportType = 7;
            mpm.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            mpm.Save();
            hfPerformanceMeasure.Value = mpm.ID.ToString();
        }


        MagnetGPRAPerformanceMeasure item = new MagnetGPRAPerformanceMeasure();
        if (!string.IsNullOrEmpty(hfPerformanceMeasure.Value))
            item =  MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfPerformanceMeasure.Value));

        item.ReportType = 7;
        item.MagnetGPRAID = Convert.ToInt32(hfID.Value);

        
        //item.Total = string.IsNullOrEmpty(txtTotalSutdents.Text) ? (int?)null : Convert.ToInt32(txtTotalSutdents.Text);
        txtTotalSutdents.Text = item.Total.ToString();
        item.Budget = string.IsNullOrEmpty(txtAnnualBudget.Text) ? (decimal?)null : Convert.ToDecimal(txtAnnualBudget.Text);
        item.Save();
        hfPerformanceMeasure.Value = item.ID.ToString();

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 3);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 3;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
}