﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using log4net;

public partial class admin_data_manageschoolyear : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
        {
            ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("../../js/MaskedEditFix.js")));
        }
    }

    int reportyear = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack)
        {
            if(Session["ReportYear"]!=null)
                reportyear = Convert.ToInt32(Session["ReportYear"]);
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = ManageUtility.FormatInteger((int)Session["ReportID"]);
            LoadData();

            ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
            //lblYear.Text = period.ReportPeriod.Substring(0, 4);
            //lblNumberYear.Text = Convert.ToString(period.reportYear);
        }
    }
    private void LoadData()
    {
        int ReportID = (int)Session["ReportID"];
        string user = Context.User.Identity.Name;
        int GranteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(user.ToLower()))[0].GranteeID;

        Table table = new Table();
        table.ID = "schoolTbl1";
        table.ClientIDMode = System.Web.UI.ClientIDMode.Static;
        table.CssClass = "msapDataTbl CenterContent";
        Page.Form.Controls.Add(table);

        TableHeaderRow thr = new TableHeaderRow();
        TableHeaderCell thc = new TableHeaderCell();
        thc.Text = "<img src='../../images/head_button.jpg' align='ABSMIDDLE' /> School Name";
        thr.Cells.Add(thc);
        thc = new TableHeaderCell();
        thc.Text = "<img src='../../images/head_button.jpg' align='ABSMIDDLE' /> First School Year as a Magnet School";
        thr.Cells.Add(thc);
        table.Rows.Add(thr);

        contentPanel.Controls.Add(table);
        string ids = "";
        int idx = 1;
        foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == GranteeID && x.ReportYear==reportyear && x.isActive).OrderBy(x=>x.SchoolName))
        {
            var data = MagnetSchoolYear.Find(x => x.ReportID == ReportID && x.SchoolID == School.ID);
            MagnetSchoolYear msy = new MagnetSchoolYear();
            if (data.Count == 0)
            {
                msy.ReportID = ReportID;
                msy.SchoolID = School.ID;
                msy.Save();
            }
            else
            {
                msy = data[0];
            }

            ids += string.IsNullOrEmpty(ids) ? msy.ID.ToString() : ";" + msy.ID.ToString();
            //Add to UI
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            tc = new TableCell();
            tc.Text = School.SchoolName;

            tr.Cells.Add(tc);

            tc = new TableCell();
            //RadTextBox tb = new RadTextBox();
            TextBox tb = new TextBox();
            tb.ClientIDMode = System.Web.UI.ClientIDMode.Static;
            tb.ID = "txtYear" + idx.ToString();

            tb.CssClass = "msapDataTxt change";
            
            tb.Text = msy.SchoolYear;
            tb.MaxLength = 4;


            AjaxControlToolkit.MaskedEditExtender extender = new AjaxControlToolkit.MaskedEditExtender();
            extender.ID = "MaskedEditExtender" + idx.ToString();
            extender.TargetControlID = "txtYear" + idx.ToString();
         
            extender.Mask = "9999";
            
            extender.MaskType = AjaxControlToolkit.MaskedEditType.Number;
            tc.Controls.Add(tb);
            tc.Controls.Add(extender);
            tc.CssClass = "TDWithBottomNoRightBorder";

            tr.Cells.Add(tc);
            table.Rows.Add(tr);
            idx++;
        }
        hfIDs.Value = ids;
        //contentPanel.Controls.Add(table);
    }
    protected void OnSave(object sender, EventArgs e)
    {
        SaveData();
        UpdateRecord();
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    private void UpdateRecord()
    {
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 8);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 8;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
    }
    private void SaveData()
    {
        Table table = (Table)contentPanel.FindControl("schoolTbl1");
        //Table table = FindControlRecursive(this.Master, "schoolTbl1") as Table;   
        string[] ids = hfIDs.Value.Split(';');
        if (ids.Count() > 0)
        {
            int idx = 1;
            foreach (string id in ids)
            {
                TextBox tb = (TextBox)table.FindControl("txtYear" + idx.ToString());
                if (tb != null)
                {
                    MagnetSchoolYear SchoolYear = MagnetSchoolYear.SingleOrDefault(x => x.ID == Convert.ToInt32(id));
                    SchoolYear.SchoolYear = tb.Text;
                    SchoolYear.Save();
                }
                idx++;
            }
        }
        //LoadData();
    }
    private Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;
        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }
        return null;
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        SaveData();
        MagnetDBDB db = new MagnetDBDB();
        var Data = from m in db.MagnetSchoolEnrollments
                   where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                   && m.StageID == 1
                 
                   orderby m.GradeLevel
                   select new
                   {
                       m.GradeLevel,
                       AmericanIndian = m.AmericanIndian == null ? (int?)null : m.AmericanIndian,
                       Asian = m.Asian == null ? (int?)null : m.Asian,
                       AfricanAmerican = m.AfricanAmerican == null ? (int?)null : m.AfricanAmerican,
                       Hispanic = m.Hispanic == null ? (int?)null : m.Hispanic,
                       Hawaiian = m.Hawaiian == null ? (int?)null : m.Hawaiian,
                       White = m.White == null ? (int?)null : m.White,
                       MultiRacial = m.MultiRacial == null ? (int?)null : m.MultiRacial,
                       GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                       + (m.Asian == null ? 0 : m.Asian)
                       + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                       + (m.Hispanic == null ? 0 : m.Hispanic)
                       + (m.Hawaiian == null ? 0 : m.Hawaiian)
                       + (m.White == null ? 0 : m.White)
                       + (m.MultiRacial == null ? 0 : m.MultiRacial))
                   };
        //var Data = MagnetSchoolEnrollment.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value) && x.StageID == 1).OrderBy(x=>x.GradeLevel);
        {
            using (System.IO.MemoryStream output = new MemoryStream())
            {
                try
                {
                    PdfStamper ps = null;

                    // read existing PDF document
                    PdfReader r = new PdfReader(Server.MapPath("../doc/Tables12.pdf"));
                    ps = new PdfStamper(r, output);
                    AcroFields af = ps.AcroFields;

                    GranteeReport Report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
                    if (Report.FirstTimeMagnetProgram != null && Report.FirstTimeMagnetProgram == true)
                        af.SetField("FirstYearProgram", "Yes");

                    //Date (Year 1 of Project—Oct 1, 2010)
                    string DateString = "(Year ";
                    DateString += MagnetReportPeriod.SingleOrDefault(x=>x.ID == Report.ReportPeriodID).reportYear;
                    DateString += " of Project—October 1, ";
                    DateString += MagnetReportPeriod.SingleOrDefault(x=>x.ID == Report.ReportPeriodID).ReportPeriod.Substring(0,4);
                    DateString += ")";
                    af.SetField("Date", DateString);

                    int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                    int total = 0;
                    foreach (var Enrollment in Data)
                    {
                        total = 0;
                        if (Enrollment.AmericanIndian != null)
                        {
                            total += (int)Enrollment.AmericanIndian;
                            af.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger((int)Enrollment.AmericanIndian));
                        }
                        if (Enrollment.Asian != null)
                        {
                            total += (int)Enrollment.Asian;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger((int)Enrollment.Asian));
                        }
                        if (Enrollment.AfricanAmerican != null)
                        {
                            total += (int)Enrollment.AfricanAmerican;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican));
                        }
                        if (Enrollment.Hispanic != null)
                        {
                            total += (int)Enrollment.Hispanic;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger((int)Enrollment.Hispanic));
                        }
                        if (Enrollment.Hawaiian != null)
                        {
                            total += (int)Enrollment.Hawaiian;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger((int)Enrollment.Hawaiian));
                        }
                        if (Enrollment.White != null)
                        {
                            total += (int)Enrollment.White;
                            af.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger((int)Enrollment.White));
                        }
                        if (Enrollment.MultiRacial != null)
                        {
                            total += (int)Enrollment.MultiRacial;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger((int)Enrollment.MultiRacial));
                        }
                        af.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger(total));

                        if (Enrollment.AmericanIndian != null) af.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", total == 0 ? 0 : (double)Enrollment.AmericanIndian / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}",0));
                        if (Enrollment.Asian != null) af.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", total == 0 ? 0 : (double)Enrollment.Asian / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", 0));
                        if (Enrollment.AfricanAmerican != null) af.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", total == 0 ? 0 : (double)Enrollment.AfricanAmerican / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", 0));
                        if (Enrollment.Hispanic != null) af.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", total == 0 ? 0 : (double)Enrollment.Hispanic / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", 0));
                        if (Enrollment.Hawaiian != null) af.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", total == 0 ? 0 : (double)Enrollment.Hawaiian / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", 0));
                        if (Enrollment.White != null) af.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", total == 0 ? 0 : (double)Enrollment.White / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", 0));
                        if (Enrollment.MultiRacial != null) af.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", total == 0 ? 0 : (double)Enrollment.MultiRacial / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", 0));

                        if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                        if (Enrollment.Asian != null) TotalEnrollments[1] += (int)Enrollment.Asian;
                        if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                        if (Enrollment.Hispanic != null) TotalEnrollments[3] += (int)Enrollment.Hispanic;
                        if (Enrollment.Hawaiian != null) TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                        if (Enrollment.White != null) TotalEnrollments[5] += (int)Enrollment.White;
                        if (Enrollment.MultiRacial != null) TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                        TotalEnrollments[7] += total;
                    }

                    af.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                    af.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                    af.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                    af.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                    af.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                    af.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                    af.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                    af.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));
                    if (TotalEnrollments[7] > 0)
                    {
                        af.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                        af.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                        af.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                        af.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                        af.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                        af.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                        af.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));
                    }

                    int Idx = 1;
                    string user = Context.User.Identity.Name;
                    int GranteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(user.ToLower()))[0].GranteeID;
                    var SchoolYearData = (from m in db.MagnetSchoolYears
                                          from n in db.MagnetSchools
                                          where m.SchoolID == n.ID
                                          && m.ReportID == Convert.ToInt32(hfReportID.Value)
                                          && n.ReportYear==reportyear && n.isActive && n.GranteeID==GranteeID
                                          select new { m.ID, n.SchoolName, m.SchoolYear });
                    foreach (var SchoolYear in SchoolYearData)
                    {
                        //if (SchoolYear.SchoolName.Length > 68)
                        //{
                        //    af.SetFieldProperty("School NameRow" + Idx.ToString(), "textsize", 0f, null);
                        //    af.SetField("School NameRow" + Idx.ToString(), SchoolYear.SchoolName);
                        //    af.SetField("School YearRow" + Idx.ToString(), SchoolYear.SchoolYear);
                        //}
                        //else
                        {
                            af.SetField("School NameRow" + Idx.ToString(), SchoolYear.SchoolName);
                            af.SetField("School YearRow" + Idx.ToString(), SchoolYear.SchoolYear);
                        }
                        Idx++;
                    }

                    ps.FormFlattening = true;
                    //ps.FormFlattening = false;

                    r.Close();
                    ps.Close();
                }
                catch (System.Exception ex)
                {
                    ILog Log = LogManager.GetLogger("EventLog");
                    Log.Error("District Enrollment Report:", ex);
                }
                finally
                {
                }

                int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
                MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_Tables_1_and_2.pdf");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();

                output.Close();
            }
        }
    }
}