﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="required.aspx.cs" Inherits="admin_data_required" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Desegregation Required Plan
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });

       

        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {
                
                var who = document.getElementsByName('<%= CourtOrder.UniqueID %>')[0];
                who.value = "";

                var who2 = who.cloneNode(false);
                who2.onchange = who.onchange;
                who.parentNode.replaceChild(who2, who);
   

                var who = document.getElementsByName('<%= CourtOrder.UniqueID %>')[0];
                who.value = "";

                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="desegregation.aspx">Desegregation Plan</a> <span class="greater_than">&gt;</span>
            Required Plan Upload</h4>
            <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />

        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <div class="titles_noTabs">
            Required Plan Upload</div>
        <br />
        <br />
        
        <span class="Orange_headers">
	     <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; A Required Plan<br /></span>
		<div class="Desg_content_div">
            <p>
                If you selected “A Required Plan”, you must attach <b>ONE</b> of the following documents
</p>
                        <ul id="Desg_plan_list_Req">
                            <li>A copy of the court order that demonstrates that the magnet school(s) for which
                                assistance is sought under the grant are a part of the approved plan.</li>
                            <li>A copy of the order of a State Agency or other official of competent jurisdiction
                                that demonstrates that the magnet school(s) for which assistance is sought under
                                the grant are a part of the approved plan.</li>
                            <li>A copy of the OCR order that demonstrates that the magnet school(s) for which assistance
                                is sought under the grant are a part of the approved plan.</li>
                        </ul>
                       	
                        <asp:FileUpload ID="CourtOrder" runat="server" 
                CssClass="msapDataTxt"  />
                        
                   <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
                        AutoGenerateColumns="false" DataKeyNames="ID" GridLines="None" Width="300" CssClass="msapDataTbl">
                        <Columns>
                            <asp:BoundField HeaderText="Uploaded Files" DataField="FilePath" HtmlEncode="false" />
                            <asp:TemplateField>
                                <ItemStyle CssClass="TDWithBottomBorder" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete"
                                        CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you sure you want to delete this uploaded file?');"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                         
			</div>
                    <p>
                        <u><b>Note</b></u>: If the applicant is implementing a previously approved plan
                        that does not include the magnet school(s) for which assistance is requested, the
                        plan must be modified to include the new magnet school(s). The applicant must obtain
                        approval of the new magnet schools, or any other modification to its desegregation
                        plan, from the court, agency or official that originally approved the plan. The
                        date by which proof of approval of any desegregation plan modification must be submitted
                        to the US Department of Education is identified in the closing date notice.</p>

          	<p>
                Any desegregation plan modification should be mailed to:</p>
            <div id="desg_address">
                Anna Hinton<br />
                US Department of Education<br />
                Office of Innovation &amp; Improvement<br />
                400 Maryland Avenue SW, Rm 4W229<br />
                Washington, DC 20202-5970</div>
    		</div>
            <div class="right_align">
            	<asp:Button ID="Button10" runat="server" CssClass="surveyBtn2" Text="Save & Upload"
                        OnClick="OnSave" />
            </div>
</asp:Content>
