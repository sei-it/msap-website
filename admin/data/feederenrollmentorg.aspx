﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="feederenrollmentorg.aspx.cs" Inherits="admin_report_feederenrollmentorg" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP center - Feeder School Enrollment data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        <a href="quarterreports.aspx">Home</a> --> <asp:Label ID="lblFormTitle" runat="server"></asp:Label></h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfSchoolID" runat="server" />
    <asp:HiddenField ID="hfStageID" runat="server" />
    <div style="text-align:left;">
        <asp:Button ID="Button32" runat="server" Text="Add Enrollment Data" CssClass="msapBtn" OnClick="OnAdd" />
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
        <Columns>
            <asp:BoundField DataField="FeederSchool" SortExpression="" HeaderText="Feeder School" />
            <asp:BoundField DataField="MagnetSchool" SortExpression="" HeaderText="Magnet School" />
            <asp:BoundField DataField="AmericanIndian" SortExpression="" HeaderText="American Indian" />
            <asp:BoundField DataField="Asian" SortExpression="" HeaderText="Asian/Pacific Islander" />
            <asp:BoundField DataField="AfricanAmerican" SortExpression="" HeaderText="Black" />
            <asp:BoundField DataField="Hispanic" SortExpression="" HeaderText="Hispanic" />
            <asp:BoundField DataField="White" SortExpression="" HeaderText="White" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <%-- Resource --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit <asp:Label ID="lblLabel" runat="server"></asp:Label> Data</div>
            <table>
                <tr>
                    <td>
                        Feeder School:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlFeederSchool" runat="server" CssClass="msapDataTxt">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Magnet School:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMagnetSchool" runat="server" CssClass="msapDataTxt">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        American Indian /Alaskan Native:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmericanIndian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtAmericanIndian"
                            Mask="999,999,999" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Asian/Pacific Islander:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAsian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtAsian" Mask="999,999,999"
                            MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Black or African American:
                    </td>
                    <td>
                        <asp:TextBox ID="txtBlack" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtBlack" Mask="999,999,999"
                            MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Hispanic/Latino:
                    </td>
                    <td>
                        <asp:TextBox ID="txtHispanic" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender4" TargetControlID="txtHispanic" Mask="999,999,999"
                            MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        White:
                    </td>
                    <td>
                        <asp:TextBox ID="txtWhite" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:MaskedEditExtender ID="MaskedEditExtender5" TargetControlID="txtWhite" Mask="999,999,999"
                            MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                            MaskType="Number" InputDirection="RightToLeft" ErrorTooltipEnabled="True" />
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        &nbsp;<asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveData" />&nbsp;&nbsp;
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>
