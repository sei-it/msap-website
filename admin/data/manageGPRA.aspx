﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageGPRA.aspx.cs" Inherits="admin_data_managegpra"  ErrorPage="~/Error_page.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage GPRA Table
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a>
        <span class="greater_than">&gt;</span>
        GPRA Table</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <div class="titles_noTabs">GPRA Table</div><br/>
    <p class="clear">
       Complete a separate Government Performance and Results Act (GPRA) Table for each school participating in your grant project. The reporting table consists of six parts: (1) Part I - School demographic data; (2) Part II - GPRA Performance Measure 1 data; (3) Part III - GPRA Performance Measures 2 and 3 data; (4) Part IV - GPRA Performance Measure 4 data; (5)
        Part V - GPRA Performance Measure 6 data; and (6) Part VI – Minority Group Isolation Data. See the relevant sections of the <i>GPRA Guide</i> for instructions on completing each part.
        <img src="../../images/question_mark-thumb.png" title="Click Add/Edit for each magnet school to enter data in the GPRA Table." class="Q_mark"/></p>
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl CenterContent" GridLines="None">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
              		<img src="../../images/head_button.jpg" align="ABSMIDDLE" />Magnet School
                </HeaderTemplate>
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Eval("SchoolName") %></font>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle CssClass="TDWithBottomBorder" />
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Eval("PartI")%></font>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <p style="text-align: right;">
        <asp:Button ID="Button2" runat="server" CssClass="surveyBtn" Text="Review" OnClick="OnPrint" />
    </p>
</asp:Content>
