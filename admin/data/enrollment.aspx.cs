﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
public partial class admin_report_enrollment : System.Web.UI.Page
{
    int reportyear = 1;
    protected void Page_Load(object sender, EventArgs e)
    {

        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if(Session["ReportYear"]!=null)
            reportyear = Convert.ToInt32(Session["ReportYear"]);
        if (!Page.IsPostBack)
        {
            
            int reportid = 1;
            if (Session["ReportID"] != null)
            {
                reportid = (int)Session["ReportID"];
                hfReportID.Value = reportid.ToString();
            }
            else
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }

            lblFormTitle.Text = "Enrollment Data-Magnet Schools";
            hfStageID.Value = "2";

            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == reportid).GranteeID;
            hfGranteeID.Value = granteeID.ToString();

            //Schools
            GridView2.DataSource = MagnetSchool.Find(x => x.GranteeID == granteeID && x.ReportYear==reportyear && x.isActive).OrderBy(x => x.SchoolName);
            GridView2.DataBind();

            GranteeReport Report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == Report.ReportPeriodID);
            lblYear.Text = period.ReportPeriod.Substring(0, 4);
            lblNumberYear.Text = Convert.ToString(period.reportYear);
        }
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        using (System.IO.MemoryStream Output = new MemoryStream())
        {
            //Print
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, Output);
            document.Open();
            PdfContentByte pdfContentByte = pdfWriter.DirectContent;
            GranteeReport Report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));

            MagnetDBDB db = new MagnetDBDB();
            List<string> TmpPDFs = new List<string>();
            var magnetSchools = MagnetSchool.Find(x => x.GranteeID == Convert.ToInt32(hfGranteeID.Value) && x.ReportYear==reportyear && x.isActive).OrderBy(x => x.SchoolName);
            if (magnetSchools.Count() > 0)
            {
                foreach (MagnetSchool School in magnetSchools)
                {
                    try
                    {
                        string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TmpPDFs.Add(TmpPDF);

                        PdfStamper ps = null;

                        // Fill out form
                        PdfReader r = new PdfReader(Server.MapPath("../doc/Table3.pdf"));
                        ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                        AcroFields af = ps.AcroFields;

                        af.SetField("SchoolName", School.SchoolName);

                        //Date (Year 1 of Project—Oct 1, 2010)
                        string DateString = "(Year ";
                        DateString += MagnetReportPeriod.SingleOrDefault(x => x.ID == Report.ReportPeriodID).reportYear;
                        DateString += " of Project—Oct 1, ";
                        DateString += MagnetReportPeriod.SingleOrDefault(x => x.ID == Report.ReportPeriodID).ReportPeriod.Substring(0, 4);
                        DateString += ")";
                        af.SetField("Date", DateString);

                        var Data = from m in db.MagnetSchoolEnrollments
                                   where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                                   && m.SchoolID == School.ID
                                   && m.StageID == 2//School enrollment data
                                   orderby m.GradeLevel
                                   select new
                                   {
                                       m.GradeLevel,
                                       AmericanIndian = m.AmericanIndian == null ? (int?)null : m.AmericanIndian,
                                       Asian = m.Asian == null ? (int?)null : m.Asian,
                                       AfricanAmerican = m.AfricanAmerican == null ? (int?)null : m.AfricanAmerican,
                                       Hispanic = m.Hispanic == null ? (int?)null : m.Hispanic,
                                       Hawaiian = m.Hawaiian == null ? (int?)null : m.Hawaiian,
                                       White = m.White == null ? (int?)null : m.White,
                                       MultiRacial = m.MultiRacial == null ? (int?)null : m.MultiRacial,
                                       GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                                       + (m.Asian == null ? 0 : m.Asian)
                                       + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                                       + (m.Hispanic == null ? 0 : m.Hispanic)
                                       + (m.Hawaiian == null ? 0 : m.Hawaiian)
                                       + (m.White == null ? 0 : m.White)
                                       + (m.MultiRacial == null ? 0 : m.MultiRacial))
                                   };

                        int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                        int total = 0; bool isTotalHasData = false;
                        foreach (var Enrollment in Data)
                        {
                            total = 0; 
                            if (Enrollment.AmericanIndian != null)
                            {
                                isTotalHasData = true;
                                total += (int)Enrollment.AmericanIndian;
                                af.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger((int)Enrollment.AmericanIndian));
                            }
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "India", "");

                            if (Enrollment.Asian != null)
                            {
                                isTotalHasData = true;
                                total += (int)Enrollment.Asian;
                                af.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger((int)Enrollment.Asian));
                            }
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "Asian", "");

                            if (Enrollment.AfricanAmerican != null)
                            {
                                isTotalHasData = true;
                                total += (int)Enrollment.AfricanAmerican;
                                af.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican));
                            }
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "Black", "");

                            if (Enrollment.Hispanic != null)
                            {
                                isTotalHasData = true;
                                total += (int)Enrollment.Hispanic;
                                af.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger((int)Enrollment.Hispanic));
                            }
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", "");

                            if (Enrollment.Hawaiian != null)
                            {
                                isTotalHasData = true;
                                total += (int)Enrollment.Hawaiian;
                                af.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger((int)Enrollment.Hawaiian));
                            }
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", "");

                            if (Enrollment.White != null)
                            {
                                isTotalHasData = true;
                                total += (int)Enrollment.White;
                                af.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger((int)Enrollment.White));
                            }
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "White", "");

                            if (Enrollment.MultiRacial != null)
                            {
                                isTotalHasData = true;
                                total += (int)Enrollment.MultiRacial;
                                af.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger((int)Enrollment.MultiRacial));
                            }
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "White", "");

                            if (isTotalHasData)
                                af.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger(total));
                            else
                                af.SetField(Enrollment.GradeLevel.ToString() + "Total", "");

                            if (total > 0)
                            {
                                af.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", Enrollment.AmericanIndian == null ? 0 : (double)Enrollment.AmericanIndian / total * 100));

                                af.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", Enrollment.Asian == null ? 0 : (double)Enrollment.Asian / total * 100));

                                af.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", Enrollment.AfricanAmerican == null ? 0 : (double)Enrollment.AfricanAmerican / total * 100));

                                af.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", Enrollment.Hispanic == null ? 0 : (double)Enrollment.Hispanic / total * 100));

                                af.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", Enrollment.Hawaiian == null ? 0 : (double)Enrollment.Hawaiian / total * 100));

                                af.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", Enrollment.White == null ? 0 : (double)Enrollment.White / total * 100));

                                af.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", Enrollment.MultiRacial == null ? 0 : (double)Enrollment.MultiRacial / total * 100));
                            }
                            else
                            {
                                af.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", 0));

                                af.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", 0));

                                af.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", 0));

                                af.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", 0));

                                af.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", 0));

                                af.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", 0));

                                af.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", 0));

                            }
                            

                            if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                            if (Enrollment.Asian != null) TotalEnrollments[1] += (int)Enrollment.Asian;
                            if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                            if (Enrollment.Hispanic != null) TotalEnrollments[3] += (int)Enrollment.Hispanic;
                            if (Enrollment.Hawaiian != null) TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                            if (Enrollment.White != null) TotalEnrollments[5] += (int)Enrollment.White;
                            if (Enrollment.MultiRacial != null) TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                            if (Enrollment.GradeTotal != null) TotalEnrollments[7] += total;
                        }

                        af.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                        af.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                        af.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                        af.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                        af.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                        af.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                        af.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                        af.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));

                        if (TotalEnrollments[7] > 0)
                        {
                            af.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                            af.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                            af.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                            af.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                            af.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                            af.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                            af.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));
                        }
                        else
                        {
                            af.SetField("TotalIndiaPercentage", "0");
                            af.SetField("TotalAsianPercentage", "0");
                            af.SetField("TotalBlackPercentage", "0");
                            af.SetField("TotalHispanicPercentage", "0");
                            af.SetField("TotalHawaiianPercentage", "0");
                            af.SetField("TotalWhitePercentage", "0");
                            af.SetField("TotalMultiPercentage", "0");
                        }

                        ps.FormFlattening = true;

                        r.Close();
                        ps.Close();

                        //Add to final report
                        PdfReader reader = new PdfReader(TmpPDF);
                        document.NewPage();
                        PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                        pdfContentByte.AddTemplate(importedPage, 0, 0);
                        document.NewPage();
                        reader.Close();
                    }
                    catch (System.Exception ex)
                    {
                        ILog Log = LogManager.GetLogger("EventLog");
                        Log.Error("School Enrollment report:", ex);
                    }
                    finally
                    {
                    }
                }
            }
            else
            {
                try
                {
                    string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                    if (File.Exists(TmpPDF))
                        File.Delete(TmpPDF);
                    TmpPDFs.Add(TmpPDF);

                    PdfStamper ps = null;

                    // Fill out form
                    PdfReader r = new PdfReader(Server.MapPath("../doc/Table9.pdf"));
                    ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                    AcroFields af = ps.AcroFields;

                    af.SetField("SchoolName", " ");

                    //Date (Year 1 of Project—Oct 1, 2010)
                    string DateString = "(Year ";
                    DateString += Report.ReportPeriodID % 2 == 0 ?
                        Convert.ToString(Report.ReportPeriodID / 2) :
                        Convert.ToString(Report.ReportPeriodID / 2 + 1);
                    DateString += " of Project—Oct 1, ";
                    DateString += MagnetReportPeriod.SingleOrDefault(x => x.ID == Report.ReportPeriodID).ReportPeriod.Substring(0, 4);
                    DateString += ")";
                    af.SetField("Date", DateString);

                    var Data = from m in db.MagnetSchoolEnrollments
                               where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                               && m.SchoolID == null
                               && m.StageID == 2//School enrollment data
                               orderby m.GradeLevel
                               select new
                               {
                                   m.GradeLevel,
                                   AmericanIndian = m.AmericanIndian == null ? (int?)null : m.AmericanIndian,
                                   Asian = m.Asian == null ? (int?)null : m.Asian,
                                   AfricanAmerican = m.AfricanAmerican == null ? (int?)null : m.AfricanAmerican,
                                   Hispanic = m.Hispanic == null ? (int?)null : m.Hispanic,
                                   Hawaiian = m.Hawaiian == null ? (int?)null : m.Hawaiian,
                                   White = m.White == null ? (int?)null : m.White,
                                   MultiRacial = m.MultiRacial == null ? (int?)null : m.MultiRacial,
                                   GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                                   + (m.Asian == null ? 0 : m.Asian)
                                   + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                                   + (m.Hispanic == null ? 0 : m.Hispanic)
                                   + (m.Hawaiian == null ? 0 : m.Hawaiian)
                                   + (m.White == null ? 0 : m.White)
                                   + (m.MultiRacial == null ? 0 : m.MultiRacial))
                               };

                    int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                    int total = 0;
                    foreach (var Enrollment in Data)
                    {
                        total = 0;
                        if (Enrollment.AmericanIndian != null)
                        {
                            total += (int)Enrollment.AmericanIndian;
                            af.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger((int)Enrollment.AmericanIndian));
                        }
                        if (Enrollment.Asian != null)
                        {
                            total += (int)Enrollment.Asian;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger((int)Enrollment.Asian));
                        }
                        if (Enrollment.AfricanAmerican != null)
                        {
                            total += (int)Enrollment.AfricanAmerican;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican));
                        }
                        if (Enrollment.Hispanic != null)
                        {
                            total += (int)Enrollment.Hispanic;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger((int)Enrollment.Hispanic));
                        }
                        if (Enrollment.Hawaiian != null)
                        {
                            total += (int)Enrollment.Hawaiian;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger((int)Enrollment.Hawaiian));
                        }
                        if (Enrollment.White != null)
                        {
                            total += (int)Enrollment.White;
                            af.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger((int)Enrollment.White));
                        }
                        if (Enrollment.MultiRacial != null)
                        {
                            total += (int)Enrollment.MultiRacial;
                            af.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger((int)Enrollment.MultiRacial));
                        }
                        af.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger(total));

                        if (total > 0) af.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", (double)Enrollment.AmericanIndian / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", (double)0));
                        if (total > 0) af.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Asian / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", (double)0));
                        if (total > 0) af.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", (double)Enrollment.AfricanAmerican / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", (double)0));
                        if (total > 0) af.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hispanic / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", 0));
                        if (total > 0) af.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", (double)Enrollment.Hawaiian / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", 0));
                        if (total > 0) af.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", (double)Enrollment.White / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", 0));
                        if (total > 0) af.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", (double)Enrollment.MultiRacial / total * 100));
                        else
                            af.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", 0));

                        if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                        if (Enrollment.Asian != null) TotalEnrollments[1] += (int)Enrollment.Asian;
                        if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                        if (Enrollment.Hispanic != null) TotalEnrollments[3] += (int)Enrollment.Hispanic;
                        if (Enrollment.Hawaiian != null) TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                        if (Enrollment.White != null) TotalEnrollments[5] += (int)Enrollment.White;
                        if (Enrollment.MultiRacial != null) TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                        if (Enrollment.GradeTotal != null) TotalEnrollments[7] += total;
                    }

                    af.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                    af.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                    af.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                    af.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                    af.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                    af.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                    af.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                    af.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));

                    if (TotalEnrollments[7] > 0)
                    {
                        af.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                        af.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                        af.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                        af.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                        af.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                        af.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                        af.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));
                    }
                    else
                    {
                        af.SetField("TotalIndiaPercentage", "0");
                        af.SetField("TotalAsianPercentage", "0");
                        af.SetField("TotalBlackPercentage", "0");
                        af.SetField("TotalHispanicPercentage", "0");
                        af.SetField("TotalHawaiianPercentage", "0");
                        af.SetField("TotalWhitePercentage", "0");
                        af.SetField("TotalMultiPercentage", "0");
                    }

                    ps.FormFlattening = true;

                    r.Close();
                    ps.Close();

                    //Add to final report
                    PdfReader reader = new PdfReader(TmpPDF);
                    document.NewPage();
                    PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                    pdfContentByte.AddTemplate(importedPage, 0, 0);
                    document.NewPage();
                    reader.Close();
                }
                catch (System.Exception ex)
                {
                    ILog Log = LogManager.GetLogger("EventLog");
                    Log.Error("School Enrollment report:", ex);
                }
                finally
                {
                }
            }

            

            document.Close();
            foreach (string str in TmpPDFs)
            {
                if (File.Exists(str))
                    File.Delete(str);
            }

            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_Table_3.pdf");
            Response.OutputStream.Write(Output.GetBuffer(), 0, Output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            Output.Close();
        }
    }
}