﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text.RegularExpressions;

public partial class admin_data_gprai : System.Web.UI.Page
{
    int reportyear =1;
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if(Session["ReportYear"]!=null)
            reportyear = Convert.ToInt32(Session["ReportYear"]);


        if (!Page.IsPostBack)
        {
            Session["isMagnetHighSchool"] = null;

            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);
         
            //Report type
            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
            lblGranteeName.Text = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID).GranteeName;
            int ReportPeriodID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).ReportPeriodID;
            foreach (MagnetDLL Item in MagnetDLL.Find(x => x.TypeID == 29).OrderBy(x => x.TypeIndex))
            {
                cblGrades.Items.Add(new ListItem(Item.TypeName, Item.TypeIndex.ToString()));
            }

            if (ReportPeriodID < 6)
            {
                //Disable pre-k
                cblGrades.Items[0].Enabled = false;
                cblGrades.Items[0].Attributes.Add("color", "gray");
            }
           
                

            //item 7: school Improvement Status
            foreach(MagnetGPRAImprovementStatus itm in MagnetGPRAImprovementStatus.Find(x=>x.id >0).OrderBy(x=>x.orderby))
            {
                rblImprovementStatus.Items.Add(new ListItem(itm.statusitem, itm.statusvalue));
            }


            if (Request.QueryString["id"] == null)
                Response.Redirect("managegpra.aspx");
            else
            {
                LinkII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
                tabPartII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
                LinkIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                tabPartIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                LinkIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                tabPartIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                LinkV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
                tabPartV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
                LinkVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];
                tabPartVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];

                int schoolID = Convert.ToInt32(Request.QueryString["id"]);
                MagnetSchool magnetSchool = MagnetSchool.SingleOrDefault(x => x.ID == schoolID);
                lblSchoolName.Text = magnetSchool.SchoolName;
                lblSchoolName2.Text = magnetSchool.SchoolName;

                var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == Convert.ToInt32(hfReportID.Value));
                if (gpraData.Count > 0)
                {
                    hfID.Value = gpraData[0].ID.ToString();
                    MagnetGPRA item = gpraData[0];
                    if (!string.IsNullOrEmpty(item.SchoolGrade))
                    {
                        foreach (string str in item.SchoolGrade.Split(';'))
                        {
                            foreach (ListItem li in cblGrades.Items)
                            {
                                if (li.Value.Equals(str))
                                {
                                    li.Selected = true;
                                    if (!string.IsNullOrEmpty(li.Value) && Convert.ToInt32(li.Value) >=11)
                                        Session["isMagnetHighSchool"] = true;
                                }
                            }
                        }
                    }
                    if(item.ProgramType != null) ddlProgramType.SelectedValue = Convert.ToString(Convert.ToInt32(item.ProgramType));  //item 4
                    if (item.TitleISchoolFunding != null) ddlTitleISchoolFunding.SelectedValue = Convert.ToString(Convert.ToInt32(item.TitleISchoolFunding));  //item 5



                    if (item.TitleISchoolFundingImprovement != null)  //item 6
                    {
                        ddlTitleISchoolImprovement.SelectedValue = Convert.ToString(Convert.ToInt32(item.TitleISchoolFundingImprovement));
                    }

                    if (item.TitleISchoolFundingImprovementStatus != null)
                    {

                        foreach (ListItem li in rblImprovementStatus.Items)
                        {
                            if (li.Value.Equals(item.TitleISchoolFundingImprovementStatus.ToString()))
                                li.Selected = true;
                        }


                        if (item.TitleISchoolFundingImprovementStatus == 28) //Other option
                        {
                            txtOther.Enabled = true;
                            txtOther.Text = item.StatusOtherDes;
                        }
                        else
                            txtOther.Enabled = false;
                        

                        if (item.TitleISchoolFundingImprovementStatus == 29) //Not Applicable option
                            chkNA.Checked = true;


                    }
                    if (item.PersistentlyLlowestAchievingSchool != null) //item 8
                    {
                        ddlLowestArchieving.SelectedValue = Convert.ToString(Convert.ToInt32(item.PersistentlyLlowestAchievingSchool));
                        //ImprovementChange();
                    }
                    if (item.SchoolImprovementGrant != null) ddlSIGFunds.SelectedValue = Convert.ToString(Convert.ToInt32(item.SchoolImprovementGrant));  //item 9
                    if (item.FRPMPpercentage != null) txtFRPMPpercentage.Text = item.FRPMPpercentage;  //itme 9a
                }
                //else
                //{
                //    MagnetGPRA item = new MagnetGPRA();
                //    item.ReportID = Convert.ToInt32(hfReportID.Value);
                //    item.SchoolID = schoolID;
                //    //item.Save();
                //    hfID.Value = item.ID.ToString();
                //}
            }
            //javascript
            ddlProgramType.Attributes.Add("onchange", "setContentDirty();");
            ddlTitleISchoolFunding.Attributes.Add("onchange", "setContentDirty();");
            ddlTitleISchoolImprovement.Attributes.Add("onchange", "setContentDirty();");
            //ddlImprovementStatus.Attributes.Add("onchange", "setContentDirty();");
            ddlLowestArchieving.Attributes.Add("onchange", "setContentDirty();");
            ddlSIGFunds.Attributes.Add("onchange", "setContentDirty();");
        }
    }
    protected void OnTitleI(object sender, EventArgs e)
    {
        hfValidate.Value = "0";
    }
  
    protected void OnTitleIImprovement(object sender, EventArgs e)
    {
        hfValidate.Value = "0";
    }

    protected void OnLowAchieving(object sender, EventArgs e)
    {
        hfValidate.Value = "0";
    }
   
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetGPRA item = new MagnetGPRA();
        int schoolID = Convert.ToInt32(Request.QueryString["id"]);
        if (string.IsNullOrEmpty(hfID.Value))
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
            item.SchoolID = schoolID;
            item.Save();
            hfID.Value = item.ID.ToString();
        }
        Session["isMagnetHighSchool"] = null;
        item = MagnetGPRA.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        string str = "";
        foreach (ListItem li in cblGrades.Items)
        {
            if (li.Selected)
            {
                if (!string.IsNullOrEmpty(str))
                    str += ";";
                str += li.Value;
                if (!string.IsNullOrEmpty(li.Value) && Convert.ToInt32(li.Value) >= 11)
                    Session["isMagnetHighSchool"] = true;
            }
        }
        item.SchoolGrade = str;

        //item 7: school improvement status 
        int ImpvStatus = chkNA.Checked ? 29 : Convert.ToInt32(rblImprovementStatus.SelectedValue);

        item.TitleISchoolFundingImprovementStatus = ImpvStatus;

        if (ImpvStatus == 28)
            item.StatusOtherDes = txtOther.Text.Trim();
        else
        {
            item.StatusOtherDes = ""; 
            txtOther.Text = "";
        }

        item.ProgramType = (ddlProgramType.SelectedIndex > 0 ? Convert.ToBoolean(Convert.ToInt32(ddlProgramType.SelectedValue)) : (bool?)null);
        item.TitleISchoolFunding = (ddlTitleISchoolFunding.SelectedIndex > 0 ? Convert.ToBoolean(Convert.ToInt32(ddlTitleISchoolFunding.SelectedValue)) : (bool?)null);
        item.TitleISchoolFundingImprovement = (ddlTitleISchoolImprovement.SelectedIndex > 0 ? Convert.ToBoolean(Convert.ToInt32(ddlTitleISchoolImprovement.SelectedValue)) : (bool?)null);
        //item.TitleISchoolFundingImprovementStatus = (rblImprovementStatus.SelectedIndex > 0 ? Convert.ToInt32(rblImprovementStatus.SelectedValue) : (int?)null);
        item.PersistentlyLlowestAchievingSchool = (ddlLowestArchieving.SelectedIndex > 0 ? Convert.ToBoolean(Convert.ToInt32(ddlLowestArchieving.SelectedValue)) : (bool?)null);
        item.SchoolImprovementGrant = (ddlSIGFunds.SelectedIndex > 0 ? Convert.ToBoolean(Convert.ToInt32(ddlSIGFunds.SelectedValue)) : (bool?)null);
        if (!string.IsNullOrEmpty(txtFRPMPpercentage.Text.Trim()))
            item.FRPMPpercentage = Convert.ToDouble(txtFRPMPpercentage.Text.Trim()).ToString("F");
        else
            item.FRPMPpercentage = "";

        item.Save();

        
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 3);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 3;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
        hfSaved.Value = "1";
    }
}