﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text.RegularExpressions;
using System.Net.Mail;

public partial class admin_data_contacts : System.Web.UI.Page
{
    int cohortType = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        cohortType = Convert.ToInt32(rbtnlstCohort.SelectedValue);

        if (!Page.IsPostBack)
        {
            
            string Username = HttpContext.Current.User.Identity.Name;
            var users = MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()));

            if (users != null && users.Count == 0)
            {
                rbtnlstCohort.SelectedIndex = 0;
                rbtnlstCohort.Items[1].Enabled = false;
            }
            
            //States
            foreach (MagnetState state in MagnetState.All())
            {
                ddlState.Items.Add(new ListItem( state.StateFullName, state.StateAbb));
            }
            foreach (MagnetGrantee grantee in MagnetGrantee.Find(x => x.CohortType == cohortType && x.isActive).OrderBy(x=>x.GranteeName))
            {
                ddlGrantee.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
            }
            ChangeContactType();
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetGranteeContact.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(Convert.ToInt32(ddlContactType.SelectedValue),0);
    }
    private void LoadData(int ContactType, int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        if (ddlContactType.SelectedValue.Equals("6"))
        {
            //Principal
            var data = (from m in db.MagnetGranteeContacts
                        from n in db.MagnetSchools
                        from l in db.MagnetGrantees
                        where m.SchoolID == n.ID
                        && m.ContactType == ContactType
                        && n.GranteeID == l.ID
                        && l.CohortType == cohortType
                        orderby m.State, l.GranteeName
                        select new { m.ID, l.GranteeName, n.SchoolName, m.ContactFirstName, m.ContactLastName, m.Address, m.City, m.State, m.Zipcode });

            GridView1.DataSource = data;
            GridView1.PageIndex = PageNumber;
            GridView1.DataBind();
        }
        else
        {
            var data = (from m in db.MagnetGranteeContacts
                       from n in db.MagnetGrantees
                        where m.LinkID == n.ID
                        && m.ContactType == ContactType
                        && n.CohortType == cohortType
                        select new { m.ID, n.GranteeName, SchoolName="", m.ContactFirstName, m.ContactLastName, m.Address, m.City, m.State, m.Zipcode });
            GridView1.DataSource = data;
            GridView1.PageIndex = PageNumber;
            GridView1.DataBind();
        }
    }
    protected void OnIndexChanged(object sender, EventArgs e)
    {
        ChangeContactType();
    }
    private void LoadSchoolForGrantee(int GranteeID)
    {
        ddlOrganization.Items.Clear();
        foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == GranteeID))
        {
            ddlOrganization.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
        }
    }
    private void ChangeContactType()
    {
        if (ddlContactType.SelectedValue.Equals("6"))
        {
            schoolRow.Visible = true;
            ddlGrantee.SelectedIndex = 0;
            LoadSchoolForGrantee(Convert.ToInt32(ddlGrantee.Items[0].Value));
        }
        LoadData(Convert.ToInt32(ddlContactType.SelectedValue), 0);
    }
    private void ClearFields()
    {
        txtTitle.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtAddress.Text = "";
        txtCity.Text = "";
        txtZipcode.Text = "";
        txtTelephone.Text = "";
        txtExtension.Text = "";
        txtFax.Text = "";
        txtEmail.Text = "";
        txtNotes.Content = "";
        ddlState.SelectedIndex = 0;
        ddlOrganization.Items.Clear();
        ddlGrantee.SelectedIndex = -1;
        hfID.Value = "";
    }
    protected void OnGranteeChanged(object sender, EventArgs e)
    {
        LoadSchoolForGrantee(Convert.ToInt32(ddlGrantee.SelectedValue));
        mpeResourceWindow.Show();
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        ddlGrantee.Items.Clear();
        foreach (MagnetGrantee grantee in MagnetGrantee.Find(x => x.CohortType == cohortType && x.isActive).OrderBy(x => x.GranteeName))
        {
            ddlGrantee.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
        }

        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetGranteeContact item = MagnetGranteeContact.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtTitle.Text = item.ContactTitle;
        txtFirstName.Text = item.ContactFirstName;
        txtLastName.Text = item.ContactLastName;
        txtAddress.Text = item.Address;
        txtCity.Text = item.City;
        txtZipcode.Text = item.Zipcode;
        txtTelephone.Text = item.Phone;
        txtExtension.Text = item.Ext;
        txtFax.Text = item.Fax;
        txtEmail.Text = item.Email;
        txtNotes.Content = item.Notes;
        ddlState.SelectedValue = item.State;
        ddlGrantee.SelectedValue = item.LinkID.ToString();
        LoadSchoolForGrantee((int)item.LinkID);
        if (item.SchoolID != null) ddlOrganization.SelectedValue = item.SchoolID.ToString();
        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        string msg="";
        if (string.IsNullOrEmpty(txtEmail.Text))
            msg = "Email address is required.";
        else
        {
            try
            {
                MailAddress m = new MailAddress(txtEmail.Text);
            }
            catch (FormatException)
            {
                msg = "Email address is not proper format.";
            }
        }

        if(!string.IsNullOrEmpty(msg))
        {
             ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('" + msg +  "');</script>", false);
             return;
        }

        MagnetGranteeContact item = new MagnetGranteeContact();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetGranteeContact.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ContactType = Convert.ToInt32(ddlContactType.SelectedValue);
            item.LinkID = Convert.ToInt32(ddlGrantee.SelectedValue);
            if (ddlOrganization.Items.Count > 0)
                item.SchoolID = Convert.ToInt32(ddlOrganization.SelectedValue);
        }
        item.ContactTitle = txtTitle.Text;
        item.ContactFirstName = txtFirstName.Text;
        item.ContactLastName = txtLastName.Text;
        item.Address = txtAddress.Text;
        item.City = txtCity.Text;
        item.Zipcode = txtZipcode.Text;
        item.Phone = txtTelephone.Text;
        item.Ext = txtExtension.Text;
        item.Fax = txtFax.Text;
        item.Email = txtEmail.Text;
        item.Notes = txtNotes.Content;
        item.State = ddlState.SelectedValue;
        item.Save();
        LoadData(Convert.ToInt32(ddlContactType.SelectedValue), 0);
    }
    protected void rbtnlstCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadData(Convert.ToInt32(ddlContactType.SelectedValue), 0);
    }
}