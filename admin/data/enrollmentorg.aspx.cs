﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using Telerik.Web.UI;

public partial class admin_report_enrollmentorg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ReportID"] == null)
        {
            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/login.aspx");
        }

        if (!Page.IsPostBack)
        {
            SqlDataSource1.SelectParameters["GranteeReportID"].DefaultValue = Convert.ToString(Session["ReportID"]);
            SqlDataSource1.SelectParameters["StageID"].DefaultValue = "1";
            SqlDataSource1.InsertParameters["GranteeReportID"].DefaultValue = Convert.ToString(Session["ReportID"]);
            SqlDataSource1.InsertParameters["StageID"].DefaultValue = "1";
        }
    }
    protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            GridNumericColumnEditor editor = item.EditManager.GetColumnEditor("AmericanIndian") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 60;

            editor = item.EditManager.GetColumnEditor("Asian") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 60;
            
            editor = item.EditManager.GetColumnEditor("AfricanAmerican") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 60;

            editor = item.EditManager.GetColumnEditor("Hispanic") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 60;

            editor = item.EditManager.GetColumnEditor("White") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 60;

            editor = item.EditManager.GetColumnEditor("Hawaiian") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 60;

            editor = item.EditManager.GetColumnEditor("MultiRacial") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 60;
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
    {
        GridEditableItem item = (GridEditableItem)e.Item;
        String id = item.GetDataKeyValue("ID").ToString();

        if (e.Exception != null)
        {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("Product with ID " + id + " is updated!");
        }
    }

    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("New product is inserted!");
        }
    }

    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
    {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("ID").ToString();

        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("Product with ID " + id + " is deleted!");
        }
    }
    private void UpdateRecord()
    {
    }
}