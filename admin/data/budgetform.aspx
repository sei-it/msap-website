﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="budgetform.aspx.cs" Inherits="admin_data_budgetform" ErrorPage="~/Error_page.aspx"
    ViewStateMode="Disabled" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Budget Summary
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .msapDataTxtSmall
        {
            width: 150px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            //if(<% =(Page.IsPostBack).ToString().ToLower() %> = false)
            {
                var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
                var validate = $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val();
                var tmp2 = $('input[name=ctl00$ContentPlaceHolder1$hfFileID]').val();
                $('input[name=ctl00$ContentPlaceHolder1$hfFileID]').val('0');
                if ( validate=="0" && tmp=="0") {
                    return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
                }
            }
        }
        function PreventConfirmation() {
           $('input[name=ctl00$ContentPlaceHolder1$hfFileID]').val('1');
        }

        function ContentIsDirty() {
            $('input[name=ctl00$ContentPlaceHolder1$hfFileID]').val('0');
            $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
        }

        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
        $(function () {
            $(".postbutton").click(function () {
               // $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
            $('.Federal').keyup(function () {
                var total;
                total = 0;
                $('.Federal1').each(function (index) {
                    //$(this).val((!$(this).val() ? "0" : $(this).val()));
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });

                $('#txtDirectCosts').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
                $('.Federal2').each(function (index) {
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });
                $('#txtTotalCosts').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
            });
            $('.NonFederal').keyup(function () {
                var total;
                total = 0;
                $('.NonFederal1').each(function (index) {
                    //$(this).val((!$(this).val() ? "0" : $(this).val()));
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });
                $('#txtDirectCostsNF').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
                $('.NonFederal2').each(function (index) {
                    if ($(this).val()) {
                        var str = $(this).val();
                        if (str.startsWith('$')) str = str.substring(1, str.length);
                        str = str.replace(/,/gi, "");
                        total = total + parseFloat(str);
                    }
                });
                $('#txtTotalCostsNF').val('$' + addCommas((Math.round(total * 100) / 100).toString()));
            });
        });


        function DateValidate(source, args) {

            var dateFrom = document.getElementById('<%=txtAgreementFrom.ClientID%>');
            var dateTo = document.getElementById('<%=txtAgreementTo.ClientID%>');

            var difference = 0;
            var date1 = 0;
            var date2 = 0;

            date1 = new Date(dateFrom.value);
            date2 = new Date(dateTo.value);
            difference = date2 - date1;

            if (difference < 0) {
                $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('0');
            }

           else if (isValidDate(dateFrom.value))
                $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('0');

            else if (isValidDate(dateTo.value))
                $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('0');
            else
            $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('1');
        }

        function isValidDate(sText) {
            var reDate = /(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/;
            return reDate.test(sText);
        }
</script>
    <style>
        .msapDataTbl tr td:first-child
        {
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            Budget Summary</h4>
            <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <div class="titles_noTabs">
            </div>
        
         <div class="tab_area">
        <div class="titles">
            Budget Summary - Page 1 of 2</div>
        <ul class="tabs">
            <li><a href="BNIBSpreadsheet.aspx">Page 2</a></li>
            <li class="tab_active">Page 1</li>
        </ul>
    </div>
    <br />
        <p class="clear">
            This form applies to individual U.S. Department of Education (ED) discretionary grant programs. Pay attention to specific instructions in the <i>Dear Colleague Letter.</i> You may access the General Administrative Regulations, 34 CFR 74 - 86 and 97-99, on ED's website at: <a href="http://www.ed.gov/policy/fund/reg/edgarReg/edgar.html"
                target="_blank">http://www.ed.gov/policy/fund/reg/edgarReg/edgar.html</a>. <img
                src="../../images/question_mark-thumb.png" title="If you are required to provide or volunteer to provide cost-sharing or matching funds or other non-Federal resources to the project, these should be shown for each applicable budget category on items 1-11 of Non-Federal Funds." class="Q_mark" />
            </p>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfFileID" runat="server" Value="0" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
        <asp:HiddenField ID="HiddenField2" runat="server" Value="0" />
        <asp:HiddenField ID="hfValidate" runat="server" Value="0" />
        <div>
            <table class="msapDataTbl">
                <tr>
                    <th>&nbsp;
                        
                    </th>
                    <th class="TDWithBottomBorder">
                        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;Federal Funds
                        </b>
                    </th>
                    <th class="TDWithBottomBorder">
                        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;Non-Federal Funds
                        </b>
                    </th>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        1. Personnel:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtPersonnel" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="1" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;" >
                        <telerik:RadNumericTextBox MinValue="0"  ID="txtPersonnelNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="13" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        2. Fringe Benefits:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtFringe" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="2" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtFringeNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="14" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        3. Travel:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTravel" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="3" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTravelNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="15" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        4. Equipment:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtEquipment" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="4" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtEquipmentNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="16" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        5. Supplies:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtSupplies" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="5" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtSuppliesNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="17" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        6. Contractual:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtContractual" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="6" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtContractualNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="18" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        7. Construction:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtConstruction" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="7" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtConstructionNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="19" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        8. Other:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtOther" runat="server" CssClass="msapDataTxtSmall change Federal Federal1"
                            TabIndex="8" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtOtherNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal1"
                            TabIndex="20" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="color: #4e8396;">
                        9. Total Direct Costs (lines 1-8):
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 1-8." /></b>
                    </td>
                    <td align="center">
                        <asp:TextBox ID="txtDirectCosts" Enabled="false" ClientIDMode="Static" runat="server" BackColor="LightGray"
                            CssClass="msapDataTxtSmall" TabIndex="9" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtDirectCostsNF" Enabled="false" ClientIDMode="Static" runat="server" BackColor="LightGray"
                            CssClass="msapDataTxtSmall" TabIndex="21" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                </tr>
                <tr>
                    <td style="color: #4e8396;">
                        10. Indirect Costs:*
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtIndirectCosts" runat="server" CssClass="msapDataTxtSmall change Federal Federal2"
                            TabIndex="10" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtIndirectCostsNF" Enabled="false" runat="server" CssClass="msapDataTxtSmall change NonFederal NonFederal2"
                            TabIndex="22" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="color: #4e8396;">
                        11. Training Stipends:
                    </td>
                    <td align="center">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTrainingStipends" runat="server" CssClass="msapDataTxtSmall change Federal Federal2"
                            TabIndex="11" Type="Currency" NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <telerik:RadNumericTextBox MinValue="0" ID="txtTrainingStipendsNF" Enabled="false" runat="server"
                            CssClass="msapDataTxtSmall change NonFederal NonFederal2" TabIndex="23" Type="Currency"
                            NumberFormat-DecimalDigits="2" Width="151">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline" style="color: #4e8396;">
                        12. Total Costs (lines 9-11):
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 9-11." />
                    </td>
                    <td align="center">
                        <asp:TextBox ID="txtTotalCosts" ClientIDMode="Static" Enabled="false" runat="server" BackColor="LightGray"
                            CssClass="msapDataTxtSmall" TabIndex="12" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                    <td align="center" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtTotalCostsNF" ClientIDMode="Static" Enabled="false" runat="server" BackColor="LightGray"
                            CssClass="msapDataTxtSmall" TabIndex="24" Type="Currency" NumberFormat-DecimalDigits="2"
                            Width="151" />
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table class="msapDataTbl">
                        <tr>
                            <td colspan="4" class="TDWithBottomNoRightBorder" style="color: #4e8396;">
                                <strong>*Indirect Cost Information (To Be Completed by Your Business Office):</strong>
                                <img src="../../images/question_mark-thumb.png" title="For federal funds only." /><br />
                                <br />
                                If you are requesting reimbursement for indirect costs on line 10, please answer
                                the following questions:
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #4e8396;" class="TDWithTopNoRightBorder" valign="top">
                                (1)
                            </td>
                            <td style="color: #4e8396; width: 50%;" class="TDWithTopBorder">
                                Do you have an Indirect Cost Rate Agreement approved by the Federal government?
                            </td>
                            <td class="TDWithTopNoRightBorder" style="padding-left: 0px; width: 120px;">
                                <asp:RadioButtonList ID="rblRageAggrement" CssClass="DotnetTbl change" runat="server"
                                    RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="OnAgreementYes">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="TDWithTopNoRightBorder">
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #4e8396;" class="TDWithTopNoRightBorder">
                                (2)
                            </td>
                            <td style="color: #4e8396;">
                                If yes, please provide the following information:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2">&nbsp;
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">&nbsp;
                                
                            </td>
                            <td style="color: #4e8396; vertical-align: top;">
                                Period covered by the Indirect Cost Rate Agreement:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2">
                                From:
                                <asp:TextBox CssClass="msapDataTxtSmall change" Width="80" ID="txtAgreementFrom"
                                    runat="server"></asp:TextBox>
                                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtAgreementFrom">
                                </ajax:CalendarExtender>
                                To:
                                <asp:TextBox CssClass="msapDataTxtSmall change" Width="80" ID="txtAgreementTo" runat="server">
                        </asp:TextBox>
                                <br />
                                <asp:CompareValidator ID="CompareValidatorTextBox1" runat="server" ControlToValidate="txtAgreementFrom"
                                    Type="Date" Operator="DataTypeCheck" ErrorMessage="<strong>From</strong> date is not in the correct format."
                                    Display="Dynamic"  />
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtAgreementTo"
                                    Type="Date" Operator="DataTypeCheck" ErrorMessage="<strong>To</strong> date is not in correct format."
                                    Display="Dynamic" />
                                <br />
                                <asp:CompareValidator runat="server" ID="cvEndTime" ControlToValidate="txtAgreementTo"
                                    ControlToCompare="txtAgreementFrom" ErrorMessage="<b>To</b> date must be later than From Date."
                                    SetFocusOnError="True" Operator="GreaterThan" Type="Date" Display="Dynamic" />
                                
                                <ajax:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtAgreementTo">
                                </ajax:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">&nbsp;
                                
                            </td>
                            <td style="color: #4e8396;">
                                Approving Federal agency:
                            </td>
                            <td class="TDWithTopNoRightBorder" style="padding-left: 0px;">
                                <asp:RadioButtonList ID="rblAgency" CssClass="DotnetTbl change" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="true" OnSelectedIndexChanged="OnAgencyChanged">
                                    <asp:ListItem Value="1">ED</asp:ListItem>
                                    <asp:ListItem Value="0">Other</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <br />
                                <asp:Panel ID="agencyPanel" runat="server" Visible="false">
                                    (please specify):
                                    <asp:TextBox CssClass="msapDataTxtSmall change" ID="txtOtherAgency" runat="server"></asp:TextBox></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">&nbsp;
                                
                            </td>
                            <td style="color: #4e8396;">
                                Indirect Cost Rate:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2" style="padding-left: 10px; margin-left: 10px;">
                                <telerik:RadNumericTextBox MinValue="0" CssClass="msapDataTxtSmall change" ID="txtRate"
                                    runat="server" Width="50">
                                </telerik:RadNumericTextBox>%
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #4e8396;" class="TDWithTopNoRightBorder" valign="top">
                                (3)
                            </td>
                            <td style="color: #4e8396;" valign="top">
                                For Restricted Rate Programs (check one) -- Are you using a restricted indirect
                                cost rate that:
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2">
                                <asp:RadioButton ID="rbIncludedInAgreement" runat="server" GroupName="includedgroup"
                                    CssClass="change" Text="Is Included in your approved Indirect Cost Rate Agreement?" />
                                <br />
                                <asp:RadioButton ID="rb34CFR" CssClass="change" GroupName="includedgroup" runat="server"
                                    Text="Complies with 34 CFR 76.564(c)(2)?" /><br />
                                <asp:RadioButton ID="rbtnNA" CssClass="change" GroupName="includedgroup" runat="server"
                                Text="Not Applicable" />
                            </td>
                        </tr>
                        <tr>
                            <td class="TDWithTopNoRightBorder">&nbsp;
                                
                            </td>
                            <td style="color: #4e8396;">
                                Indirect Cost Rate
                            </td>
                            <td class="TDWithTopNoRightBorder" colspan="2" style="padding-left: 10px; margin-left: 10px;">
                                <telerik:RadNumericTextBox MinValue="0" CssClass="msapDataTxtSmall change" ID="txtRICRate"
                                    runat="server" Width="50">
                                </telerik:RadNumericTextBox>%
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
             <asp:Panel ID="tmpID" runat="server" Visible="false">
            <table class="msapDataTbl" style="visibility:hidden;">
                <tr id="ChangeRow" runat="server" visible="false">
                    <td colspan="2" style="color: #4e8396;">
                        Is there any change to the budget?
                    </td>
                    <td colspan="2" class="TDWithTopNoRightBorder">
                        <asp:RadioButtonList ID="rblBudgetChange" runat="server" CssClass="DotnetTbl change" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="OnBudgeChanged">
                            <asp:ListItem Value="0">No</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr id="ChangeRowExplanation" runat="server" visible="false">
                    <td colspan="2" style="color: #4e8396; vertical-align: top;">
                        If yes, what was the reason?
                    </td>
                    <td class="TDWithTopNoRightBorder">
                        <asp:TextBox ID="txtBudgetReason" runat="server" CssClass="msapDataTxtSmall_nowidth change"
                            TextMode="MultiLine" onkeyup="checkWordLen(this, 250, 'reasonlength');" Rows="3"
                            Columns="50"></asp:TextBox><br />
                        <input type="text" disabled="disabled" size="3" id="reasonlength" style="margin-top: 2px;" />
                    </td>
                </tr>
               
            </table>
            </asp:Panel>
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <asp:Button ID="Button10" runat="server" CssClass="surveyBtn1 postbutton" OnClick="OnSave" OnClientClick="DateValidate();"
                Text="Save Record" />
           
        </div>
        <div style="text-align: right; margin-top: 20px;">
           Page 1&nbsp;&nbsp;&nbsp;&nbsp;
           <a href="BNIBSpreadsheet.aspx">Page 2</a>&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
