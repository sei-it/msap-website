﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.IO;

public partial class admin_data_additional : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = ManageUtility.FormatInteger(Convert.ToInt32(Session["ReportID"]));
            LoadData();
           
            FileUpload1.Attributes.Add("onchange", "return validateFileUpload(this);");
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        int muploadID = Convert.ToInt32((sender as LinkButton).CommandArgument);

        var mupload = MagnetUpload.SingleOrDefault(x => x.ID == muploadID);

        if (System.IO.File.Exists(Server.MapPath("../upload/") + mupload.PhysicalName))
            System.IO.File.Delete(Server.MapPath("../upload/") + mupload.PhysicalName);


        MagnetUpload.Delete(x => x.ID == muploadID);
        LoadData();
    }
    protected void OnUpload(object sender, EventArgs e)
    {

        if (FileUpload1.HasFile)
        {
             int ReportID = Convert.ToInt32(hfReportID.Value);
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

            string strReportPeriod = period.ReportPeriod;
            strReportPeriod += Convert.ToBoolean(period.PeriodType) ? "-Ad hoc" : "-APR";
            string strGrantee = grantee.GranteeName.Trim();
            
            string strFileroot = Server.MapPath("") + "/../upload/";
            string strPath = strReportPeriod + "/" + strGrantee.Replace("#", "") +"/Additional Information/";
            string strFileName = FileUpload1.FileName.Replace("#", "");
            string strPathandFile = strPath + strFileName;

            int projectid = Convert.ToInt32(hfReportID.Value);
            MagnetUpload mupload = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == strFileName);

            if (mupload == null)
            {
                MagnetUpload upload = new MagnetUpload();
                upload.ProjectID = Convert.ToInt32(hfReportID.Value);
                upload.FormID = 20;
                upload.FileName = FileUpload1.FileName.Replace("#","");
                //string guid = System.Guid.NewGuid().ToString() + "-" + upload.FileName.Replace('#', ' ');

                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strFileroot + strPath);
                }

                FileUpload1.SaveAs(strFileroot + strPathandFile);
                upload.PhysicalName = strPathandFile;
                //FileUpload1.SaveAs(Server.MapPath("") + "/../upload/" + guid);
                upload.UploadDate = DateTime.Now;
                upload.Save();


                var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 13);
                if (muhs.Count > 0)
                {
                    muhs[0].UpdateUser = Context.User.Identity.Name;
                    muhs[0].TimeStamp = DateTime.Now;
                    muhs[0].Save();
                }
                else
                {
                    MagnetUpdateHistory muh = new MagnetUpdateHistory();
                    muh.ReportID = Convert.ToInt32(hfReportID.Value);
                    muh.FormIndex = 13;
                    muh.UpdateUser = Context.User.Identity.Name;
                    muh.TimeStamp = DateTime.Now;
                    muh.Save();
                }

                //ddlFileType.SelectedIndex = 0;
                LoadData();
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('This file name already exists. Please rename the file.');</script>", false);

        }
    }
    private void LoadData()
    {
        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        MagnetDBDB db = new MagnetDBDB();
        var data = from n in db.MagnetUploads
                   where n.ProjectID == Convert.ToInt32(hfReportID.Value)
                   & n.FormID == 20
                   orderby n.FormID
                   select new
                   {
                       n.ID,
                       FileNames = "<a target='_blank' href='../upload/" + n.PhysicalName + "'>" + n.FileName + "</a>",
                       FileType =
                       n.FormID == -1 ? "Executive Summary" :
                       n.FormID == 0 ? "Cover Sheet" :
                       n.FormID == 1 ? "Project Status Chart" :
                       n.FormID == 2 ? "GPRA Table" :
                       n.FormID == 3 ? "ED 524B Budget Summary" :
                       n.FormID == 4 ? "Required Desegregation Plan" :
                       n.FormID == 5 ? "Table 7: Enrollment Data-LEA Level" :
                       n.FormID == 6 ? "Table 8: Year of Implementation for Existing Magnet Schools" :
                       n.FormID == 7 ? "Table 9: Enrollment Data-Magnet Schools" :
                       n.FormID == 8 ? "Table 11: Feeder School-Enrollment Data" :
                       n.FormID == 10 ? "Desegregation Plan Information Forms" :
                       n.FormID == 11 ? "Desegregation Plan Information Forms" :
                       n.FormID == 12 ? "Assurance and Certifications" :
                       n.FormID == 13 ? "ED 524B Budget Narrative" :
                       n.FormID == 20 ? "Additional Information" :
                       n.FormID == 21 ? "Voluntary Desegregation Plan" :
                       "Undefined"
                   };
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}