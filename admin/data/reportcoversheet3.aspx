﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="reportcoversheet3.aspx.cs" Inherits="admin_reportcoversheet3" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    U.S. Department of Education Grant Performance Report Cover Sheet (ED 524B)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
    </script>
    <style>
        .msapDataTbl tr td:first-child
        {
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Performance Report Cover Sheet (ED 524B)</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
    <br />

    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfFileID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <asp:HiddenField ID="hfControlID" runat="server" ClientIDMode="Static" />
    <br />
    <div class="tab_area">
        <div class="titles">
            Performance Report Cover Sheet (ED 524B) - Page 3 of 4</div>
        <ul class="tabs">
            <li><a href="reportcoversheet4.aspx">Page 4</a></li>
            <li class="tab_active">Page 3</li>
            <li><a href="reportcoversheet2.aspx">Page 2</a></li>
            <li><a href="reportcoversheet1.aspx">Page 1</a></li>
        </ul>
    </div>
    <br />
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Human Subjects
            (Annual Institutional Review Board (IRB) Certification)</b></span>
    <table class="msapDataTbl">
        <tr>
            <td class="TDWithTopBorder" width="71%" style="color: #4e8396; font-weight: bold;
                vertical-align: top;">
                10. &nbsp;&nbsp;Is the Annual Certification of Institutional Review Board (IRB) approval attached?
                <img src="../../images/question_mark-thumb.png" title="See <i>ED 524B Instructions</i>. If yes, upload the IRB approval document on the Additional Information page in Section C." />
            </td>
            <td class="TDWithTopBorder" style="border-right: 0px !important;" colspan="2">
                <asp:RadioButtonList ID="ddlIRBApproval" runat="server" CssClass="DotnetTbl change"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="2">No</asp:ListItem>
                    <asp:ListItem Value="3">Not Applicable</asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="This field is required!"
                    ControlToValidate="ddlIRBApproval" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <br />
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Performance
            Measures Status and Certification</b></span>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="msapDataTbl">
                <tr>
                    <td class="TDWithBottomBorder" colspan="2" width="71%" style="color: #4e8396; font-weight: bold;">
                        11. &nbsp;&nbsp;Performance Measures Status
                        <img src="../../images/question_mark-thumb.png" title="See <i>ED 524B Instructions.</i>" />
                    </td>
                    <td class="TDWithBottomBorder">&nbsp;
                        
                    </td>
                    <td class="TDWithBottomBorder">&nbsp;
                        
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border-right: 0px; padding-top: 12px; color: #4e8396;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.
                    </td>
                    <td style="color: #4e8396;">
                        Are complete data on performance measures for the current budget period included
                        in the Project Status Chart?
                    </td>
                    <td class="TDWithBottomBorder" colspan="2">
                        <asp:RadioButtonList ID="ddlProjectStatusChart" runat="server" CssClass="DotnetTbl change"
                            AutoPostBack="true" OnSelectedIndexChanged="OnStatuschartChanged" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                        </asp:RadioButtonList>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="This field is required!"
                            ControlToValidate="ddlProjectStatusChart" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 0px; color: #4e8396;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.
                    </td>
                    <td style="color: #4e8396;">
                        If no, when will the data be available and submitted to the Department?
                    </td>
                    <td class="TDWithBottomBorder" colspan="2">
                        &nbsp;&nbsp;<asp:TextBox ID="txtDataAvailable" runat="server" CssClass="msapDataTxt change"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidatorTextBox1" runat="server" ControlToValidate="txtDataAvailable"
                            Type="Date" Operator="DataTypeCheck" ErrorMessage="Date is not in correct format." />
                        <ajax:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtDataAvailable">
                        </ajax:CalendarExtender>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <table width="100%">
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton Width="29" Height="36" ID="ImageButton1" runat="server" ImageUrl="button_arrow2.jpg"
                                OnClick="OnPrevious" CssClass="postbutton" ToolTip="Previous" Visible="false" />
                        </td>
                        <td style="padding-top: 18px;">
                            <asp:Button ID="Button1" runat="server" Text="Save Record" CssClass="surveyBtn1 postbutton"
                                OnClick="OnSaveData" />
                        </td>
                        <td>
                            <asp:ImageButton Width="29" Height="36" ID="ImageButton2" runat="server" ImageUrl="button_arrow.jpg"
                                OnClick="OnNext" CssClass="postbutton" ToolTip="Next" Visible="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="text-align: right; margin-top: 20px;">
        <a href="reportcoversheet1.aspx">Page 1</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="reportcoversheet2.aspx">
            Page 2</a>&nbsp;&nbsp;&nbsp;&nbsp; Page 3&nbsp;&nbsp;&nbsp;&nbsp; <a href="reportcoversheet4.aspx">
                Page 4</a>
    </div>
</asp:Content>
