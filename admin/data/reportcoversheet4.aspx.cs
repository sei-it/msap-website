﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_reportcoversheet4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }

            hfReportID.Value = Convert.ToString((int)Session["ReportID"]);
            GranteePerformanceCoverSheet sheet;
            var CoverSheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == (int)Session["ReportID"]);
            if (CoverSheets.Count == 0)
            {
                sheet = new GranteePerformanceCoverSheet();
                sheet.GranteeReportID = (int)Session["ReportID"];
                sheet.Save();
                Session["CoverSheetID"] = sheet.ID;
                hfID.Value = Convert.ToString(sheet.ID);
            }
            else
            {
                sheet = CoverSheets[0];
                hfID.Value = sheet.ID.ToString();
            }

            LoadFiles();
            FileUpload1.Attributes.Add("onchange", "return validateFileUpload(this);");
            UploadBtn.Attributes.Add("onclick", "return validateFileUpload(document.getElementById('" + FileUpload1.ClientID + "'));");
        }
    }
    private void LoadFiles()
    {
        //Uploaded file
        var data = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == 0);
        if (data.Count > 0)
        {
            fileRow.Visible = true;
            hfFileID.Value = data[0].ID.ToString();
            lblFile.Text = data[0].FileName;
            UploadedFile.HRef = "../upload/" + data[0].PhysicalName;
            UploadBtn.Enabled = false;
            FileUpload1.Enabled = false;
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfFileID.Value))
        {
            try
            {
                fileRow.Visible = false;
                MagnetUpload ExecutiveSummary = MagnetUpload.SingleOrDefault(x => x.ID == Convert.ToInt32(hfFileID.Value));
                if(System.IO.File.Exists(Server.MapPath("") + "/../upload/" + ExecutiveSummary.PhysicalName))
                    System.IO.File.Delete(Server.MapPath("") + "/../upload/" + ExecutiveSummary.PhysicalName);

                ExecutiveSummary.Delete();
                hfFileID.Value = "";
                UploadBtn.Enabled = true;
                FileUpload1.Enabled = true;
            }
            catch (Exception ex) { }
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("reportcoversheet3.aspx", true);
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        SaveData();
        LoadFiles();
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data has been saved!');</script>", false);
    }
    protected void OnUpload(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            int ReportID = Convert.ToInt32(hfReportID.Value);
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

            string strReportPeriod = period.ReportPeriod;
            strReportPeriod += Convert.ToBoolean(period.PeriodType) ? "-Ad hoc" : "-APR";
            string strGrantee = grantee.GranteeName.Trim();

            string strFileroot = Server.MapPath("") + "/../upload/";
            string strPath = strReportPeriod + "/" + strGrantee.Replace("#", "") +"/ReportCoversheet4/";
            string strFileName = FileUpload1.FileName.Replace("#", "");
            string strPathandFile = strPath + strFileName;

            int projectid = Convert.ToInt32(hfReportID.Value);
            MagnetUpload mupload = MagnetUpload.SingleOrDefault(x=>x.ProjectID ==projectid && x.FileName== strFileName);

            if (mupload == null)
            {
                MagnetUpload ExecutiveSummary = new MagnetUpload();
                ExecutiveSummary.ProjectID = projectid;
                ExecutiveSummary.FormID = 0; //Cover Sheet 
                ExecutiveSummary.FileName = strFileName;
                //string tmp = System.Guid.NewGuid().ToString() + "-" + FileUpload1.FileName.Replace('#', ' ');

                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strFileroot + strPath);
                }

                FileUpload1.SaveAs(strFileroot + strPathandFile);

                ExecutiveSummary.PhysicalName = strPathandFile;
                //FileUpload1.SaveAs(Server.MapPath("") + "/../upload/" + tmp);
                ExecutiveSummary.UploadDate = DateTime.Now;
                ExecutiveSummary.Save();

                fileRow.Visible = true;
                hfFileID.Value = ExecutiveSummary.ID.ToString();
                lblFile.Text = FileUpload1.FileName;
                UploadedFile.HRef = "../upload/" + strPathandFile;

                UploadBtn.Enabled = false;
                FileUpload1.Enabled = false;

                var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 1);
                if (muhs.Count > 0)
                {
                    muhs[0].UpdateUser = Context.User.Identity.Name;
                    muhs[0].TimeStamp = DateTime.Now;
                    muhs[0].Save();
                }
                else
                {
                    MagnetUpdateHistory muh = new MagnetUpdateHistory();
                    muh.ReportID = Convert.ToInt32(hfReportID.Value);
                    muh.FormIndex = 1;
                    muh.UpdateUser = Context.User.Identity.Name;
                    muh.TimeStamp = DateTime.Now;
                    muh.Save();
                }
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('This file name already exists. Please rename the file.');</script>", false);

        }
        LoadFiles();
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        //LoadFiles();
        SaveData();
        //string Username = HttpContext.Current.User.Identity.Name;
        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

        GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        var sheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));
        GranteePerformanceCoverSheet sheet = sheets[0];
        //var directors = MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(granteeID) && x.ContactType == 1);
        var directors = MagnetGranteeDatum.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));

        //Print
        string TmpPDF = Server.MapPath("../doc/") +"tmp\\" + Guid.NewGuid().ToString() + ".pdf";
        if (File.Exists(TmpPDF))
            File.Delete(TmpPDF);
        string SummaryPDF = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
        if (File.Exists(SummaryPDF))
            File.Delete(SummaryPDF);
        string AddressChangePDF = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
        if (File.Exists(AddressChangePDF))
            File.Delete(AddressChangePDF);

        string Stationery = Server.MapPath("../doc/") + "tmp\\" + grantee.PRAward + ".pdf";

        using (System.IO.MemoryStream output = new MemoryStream())
        {
            Document document = new Document(PageSize.A4, 40f, 20f, 170f, 30f);
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, output);
            document.Open();
            try
            {
               
                PdfStamper ps = null;
                PdfContentByte pdfContentByte = pdfWriter.DirectContent;

                // read existing PDF document
                PdfReader r = new PdfReader(Server.MapPath("../doc/coversheet.pdf"));
                ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));
                //ps = new PdfStamper(r, output);

                AcroFields af = ps.AcroFields;
                af.SetField("1 PRAward #", grantee.PRAward);
                af.SetField("2 Grantee NCES ID #", grantee.NCESID);
                af.SetField("3 Project Title", grantee.ProjectTitle);
                af.SetField("4 Grantee Name Block 1 of the Grant Award Notification", grantee.GranteeName);

                if ( report.ReportPeriodID == -1)
                {
                    af.SetField("Final Performance Report", "Yes");
                }
                else
                {
                    af.SetField("Annual Performance Report", "Yes");
                }

                if (directors.Count > 0)
                {
                    af.SetField("6 Project Director See instructions Name", directors[0].ProjectDirector);
                    af.SetField("Project Director Title", directors[0].Title);
                    if (!string.IsNullOrEmpty(directors[0].Phone))
                    {
                        string strPhone, strPhoneHolder = directors[0].Phone;

                        strPhone = strPhoneHolder.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");

                        if (strPhone.Length >= 3)
                            af.SetField("Project Phone Area", strPhone.Substring(0, 3));
                        if (strPhone.Length >= 6)
                            af.SetField("Project Phone A", strPhone.Substring(3, 3));
                        if (strPhone.Length >= 10)
                            af.SetField("Project Phone B", strPhone.Substring(6, 4));

                    }
                    if (!string.IsNullOrEmpty(directors[0].Fax))
                    {
                        string strFax, strFaxHolder = directors[0].Fax;

                        strFax = strFaxHolder.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
                        if (strFax.Length >= 3)
                            af.SetField("Project Fax Area", strFax.Substring(0, 3));
                        if (strFax.Length >= 6)
                            af.SetField("Project Fax A", strFax.Substring(3, 3));
                        if (strFax.Length >= 10)
                            af.SetField("Project Fax B", strFax.Substring(6, 4));

                    }
                    af.SetField("Project Phone Ext", directors[0].Ext);
                    af.SetField("Email Address", directors[0].Email);
                }

                MagnetReportPeriod reportPeriod = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
                if (!string.IsNullOrEmpty(reportPeriod.PeriodFrom))
                {
                    string[] strs = reportPeriod.PeriodFrom.Split('/');
                    af.SetField("Reporting From Month", ManageUtility.PaddingString(strs[0]));
                    af.SetField("Reporting From Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("Reporting From Year", strs[2]);
                }
                if (!string.IsNullOrEmpty(reportPeriod.PeriodTo))
                {
                    string[] strs = reportPeriod.PeriodTo.Split('/');
                    af.SetField("Reporting To Month", ManageUtility.PaddingString(strs[0]));
                    af.SetField("Reporting To Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("Reporting To Year", strs[2]);
                }

                af.SetField("Federal Grant Fundsa Previous Budget Period", "$" + (sheet.PreviousFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.PreviousFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Sharea Previous Budget Period", "$" + (sheet.PreviousNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.PreviousNonFederalFunds)));
                af.SetField("Federal Grant Fundsb Current Budget Period", "$" + (sheet.CurrentFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.CurrentFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Shareb Current Budget Period", "$" + (sheet.CurrentNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.CurrentNonFederalFunds)));
                af.SetField("Federal Grant Fundsc Entire Project Period For Final Performance Reports only", "$" + (sheet.EntireFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.EntireFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Sharec Entire Project Period For Final Performance Reports only", "$" + (sheet.EntireNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.EntireNonFederalFunds)));

                if (sheet.IndirectCost != null)
                {
                    if ((bool)sheet.IndirectCost)
                    {
                        af.SetField("9a Yes", "Yes");
                        if (sheet.IndirectCostApproved != null)
                        {
                            if ((bool)sheet.IndirectCostApproved)
                            {
                                af.SetField("9b Yes", "Yes");
                                if (!string.IsNullOrEmpty(sheet.IndirectCostStart))
                                {
                                    string[] strs = sheet.IndirectCostStart.Split('/');
                                    af.SetField("9c Period From", ManageUtility.PaddingString(strs[0]));
                                    af.SetField("9c Period From Day", ManageUtility.PaddingString(strs[1]));
                                    af.SetField("9c Period From Year", strs[2]);
                                }

                                if (!string.IsNullOrEmpty(sheet.IndirectCostEnd))
                                {
                                    string[] strs = sheet.IndirectCostEnd.Split('/');
                                    af.SetField("9c Period To", ManageUtility.PaddingString(strs[0]));
                                    af.SetField("9c Period To Day", ManageUtility.PaddingString(strs[1]));
                                    af.SetField("9c Period To Year", strs[2]);
                                }

                                if (sheet.EDApproved != null)
                                {
                                    if ((bool)sheet.EDApproved)
                                        af.SetField("9c Yes", "Yes");
                                    else
                                    {
                                        af.SetField("9c No", "Yes");
                                        af.SetField("Approving agency other", sheet.OtherAgency);
                                    }
                                }

                                if (sheet.RateType != null)
                                {
                                    switch ((int)sheet.RateType)
                                    {
                                        case 1:
                                            af.SetField("Type of rate provisional", "Yes");
                                            break;
                                        case 2:
                                            af.SetField("Type of rate final", "Yes");
                                            break;
                                        case 3:
                                            af.SetField("Type of rate other", "Yes");
                                            if (!string.IsNullOrEmpty(sheet.OtherRateType))
                                                af.SetField("Type of rate other specify", sheet.OtherRateType);
                                            break;
                                    }
                                }
                            }
                            else
                                af.SetField("9b No", "Yes");
                        }

                        if (sheet.RestrictedRateProgram != null && (bool)sheet.RestrictedRateProgram)
                            af.SetField("9d Is included in approved indirect cost rate agreement", "Yes");

                        if (sheet.Comply34CFR != null && (bool)sheet.Comply34CFR)
                            af.SetField("9d complies with 34 CFR 76.564", "Yes");
                    }
                    else
                        af.SetField("9a No", "Yes");
                }

                if (sheet.AnnualCertificationApproval != null)
                {
                    switch ((int)sheet.AnnualCertificationApproval)
                    {
                        case 1:
                            af.SetField("IRB attached Yes", "Yes");
                            break;
                        case 2:
                            af.SetField("IRB attached No", "Yes");
                            break;
                        case 3:
                            af.SetField("IRB attached N/A", "Yes");
                            break;
                    }
                }

                if (sheet.CompletionDate != null)
                {
                    if ((bool)sheet.CompletionDate)
                        af.SetField("11a Performance Measures Status Yes", "Yes");
                    else
                        af.SetField("11a Performance Measures Status No", "Yes");
                }

                if (!string.IsNullOrEmpty(sheet.DataAvailableDate))
                {
                    string[] strs = sheet.DataAvailableDate.Split('/');
                    af.SetField("b If no when will the data be available", strs[0]);
                    af.SetField("b If no when will the data be available Month", strs[1]);
                    af.SetField("b If no when will the data be available Year", strs[2]);
                }

                //af.SetField("ExecutiveSummary", sheet.ExecutiveSummary);

                ps.FormFlattening = true;

                r.Close();
                ps.Close();
              
                // add to output
                PdfReader bsreader = new PdfReader(TmpPDF);
                for (int j = 1; j <= bsreader.NumberOfPages; j++)
                {
                    document.NewPage();

                    PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(bsreader, j);
                    pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                }
                bsreader.Close();


                //change address
                if (directors[0].AddressChanged == true)
                {
                    string pdfTemplate = Server.MapPath("../doc/addressinfo.pdf");

                  
                    PdfReader pdfReader = new PdfReader(pdfTemplate);
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                        AddressChangePDF, FileMode.Create));

                    AcroFields pdfFormFields = pdfStamper.AcroFields;

                    pdfFormFields.SetField("address", directors[0].Address);
                    pdfFormFields.SetField("city", directors[0].City);
                    pdfFormFields.SetField("state", directors[0].State);
                    pdfFormFields.SetField("zipcode", directors[0].Zipcode);


                    // flatten the form to remove editting options, set it to false
                    // to leave the form open to subsequent manual edits
                    pdfStamper.FormFlattening = true;

                    // close the pdf
                    pdfReader.Close();
                    pdfStamper.Close();

                    //Add to final report

                    PdfReader addrsreader = new PdfReader(AddressChangePDF);

                    document.NewPage();

                    PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                    pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                    addrsreader.Close();

                }

                document.Close();
               
                Response.Clear();                             
                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_coversheet.pdf");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("Cover Sheet report:", ex);

                if (File.Exists(TmpPDF))
                    File.Delete(TmpPDF);
                if (File.Exists(SummaryPDF))
                    File.Delete(SummaryPDF);
                if (File.Exists(AddressChangePDF))
                    File.Delete(AddressChangePDF);
            }
            finally
            {
                if (File.Exists(TmpPDF))
                    File.Delete(TmpPDF);
                if (File.Exists(SummaryPDF))
                    File.Delete(SummaryPDF);
                if (File.Exists(AddressChangePDF))
                    File.Delete(AddressChangePDF);
                output.Close();
            }
        }
    }
    private void SaveData()
    {
        /*GranteePerformanceCoverSheet sheet = GranteePerformanceCoverSheet.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        sheet.ExecutiveSummary = txtExecutiveSummary.Text;
        sheet.Save();

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 1);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 1;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }*/
    }
}