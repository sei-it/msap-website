﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="assurance.aspx.cs" Inherits="admin_data_assurance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Edit Desegregation Plan Assurance
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="quarterreports.aspx">Main Menu</a><img src="button_arrow.jpg" width="29"
                height="36" />
            Office for Civil Rights Review</h4>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField3" runat="server" />
        <asp:HiddenField ID="HiddenField4" runat="server" />
        <asp:HiddenField ID="HiddenField5" runat="server" />
        <p>
            Additionally, there is specific information regarding each grantee’s approved desegregation
            plan and MSAP non-discrimination assurances that the Office for Civil Rights reviews.
            To facilitate this review, please make sure that you explain all of the following
            points, as appropriate, for a grantee implementing a voluntary plan or a required
            plan.
        </p>
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Type of
                Desegregation Plan</b>:</span>
        <div class="clear">
        </div>
        <asp:Panel ID="Panel1" runat="server" GroupingText="For Grantees Implementing Voluntary Plans:">
            <table>
                <tr>
                    <td valign="top">
                        <asp:RadioButton ID="RadioButton1" runat="server" GroupName="plangroup" Enabled="false" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblPeriod1"></asp:Label> and <asp:Label runat="server" ID="lblPeriod2"></asp:Label> Student Selection Plans—describe how your district assigned
                        students to MSAP-funded schools for <asp:Label runat="server" ID="lblPeriod3"></asp:Label> , and how your district plans to assign
                        students to MSAP-funded schools for <asp:Label runat="server" ID="lblPeriod4"></asp:Label> . These selection plan descriptions
                        should include information addressing each of the following plan details:
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding-left: 20px;">
                        What factors did your district use when assigning students to schools (e.g., race,
                        national origin, sex, religion, disability, geography, limited English proficiency
                        (LEP), sibling preference, parent education level, free/reduced lunch, etc.)
                        <asp:Label CssClass="redFont" runat="server" Text="Required!" Visible="false" ID="lblFactor"></asp:Label><br />
                        <asp:TextBox ID="txtFactor" runat="server" TextMode="MultiLine" Columns="90" Rows="6"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding-left: 20px;">
                        In addition to explaining the factors you considered, your Student Selection Plan
                        should also describe in detail the process, or set of steps or procedures, by which
                        you selected students, including when and how your selection factors (if any) were
                        employed as part of your selection process.
                        <asp:Label CssClass="redFont" runat="server" Text="Required!" Visible="false" ID="lblProcess"></asp:Label><br />
                        <asp:TextBox ID="txtProcess" runat="server" TextMode="MultiLine" Columns="90" Rows="6"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding-left: 20px;">
                        Did the district make any changes to the student selection plan for <asp:Label runat="server" ID="lblPeriod5"></asp:Label>,
                        as compared to the student selection plan for <asp:Label runat="server" ID="lblPeriod6"></asp:Label>?
                        <asp:Label CssClass="redFont" runat="server" Text="Required!" Visible="false" ID="lblChange"></asp:Label><br />
                        <asp:TextBox ID="txtChange" runat="server" TextMode="MultiLine" Columns="90" Rows="6"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding-left: 20px;">
                        If applicable , when implementing the student selection plan for <asp:Label runat="server" ID="lblPeriod7"></asp:Label> , did
                        the district implement the actual plan that OCR reviewed and approved last year,
                        or did the district, while implementing the plan, make changes to the plan that
                        OCR approved?<br />
                        <asp:TextBox ID="txtAcutalPlan" runat="server" TextMode="MultiLine" Columns="90"
                            Rows="6"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>
            <asp:Panel ID="Panel2" runat="server" GroupingText="For Grantees Implementing Required Plans <img src='../../images/question_mark-thumb.png' title='A grant of unitary status does not make an LEA ineligible for MSAP funding, but the district must re-qualify for funding as a voluntary plan.  If the district has achieved unitary status since its previous MSAP submission, please submit the documents discussed above in the “Voluntary Plan” section of this handout.' />:"
             >
                <table>
                    <tr>
                        <td valign="top">
                            <asp:RadioButton ID="RadioButton2" runat="server" GroupName="plangroup" Enabled="false" />
                        </td>
                        <td>
                            Upload a letter from the school board’s attorney or the appropriate State Agency
                            or official of competent jurisdiction verifying that the plan is still required.
                            This letter should describe, in detail, any changes to the required plan that have
                            occurred since the district’s previous MSAP submission.<br />
                            <asp:FileUpload ID="LetterFile" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="padding-left: 20px;">
                            If there is a possibility that unitary status will be granted prior to the end
                            of the MSAP grant cycle, this should be explained in detail.
                            <br />
                            <asp:TextBox ID="txtUnitary" runat="server" TextMode="MultiLine" Columns="90" Rows="6"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AllowSorting="false"
                                AutoGenerateColumns="false" DataKeyNames="ID" CssClass="dotnettbl" GridLines="None"
                                Width="300">
                                <Columns>
                                    <asp:BoundField HeaderText="" DataField="FilePath" HtmlEncode="false" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete"
                                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you sure you want delete this uploaded file?');"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        <div class="clear">
        </div>
        <br />
        <p align="right">
            <asp:Button ID="Button10" runat="server" CssClass="surveyBtn2" Text="Save Record"
                OnClick="OnSave" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button3" runat="server" CssClass="surveyBtn" Text="Print" CausesValidation="false"
                OnClick="OnPrint" Visible="false" /></p>
    </div>
</asp:Content>
