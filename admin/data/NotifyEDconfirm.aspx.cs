﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Configuration;
using System.Web.Security;


public partial class admin_data_NotifyEDconfirm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bool isProductionSite = false;
            string[] dbcnnParams = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString.Split(';');
            string dbname = "";

            foreach (string dbtmp in dbcnnParams)
            {
                if (dbtmp.Contains("Initial Catalog"))
                {
                    dbname = dbtmp.Substring(dbtmp.IndexOf('=') + 1);
                    break;
                }

            }

            if (dbname.Trim().ToLower() == "magnet")
                isProductionSite = true;

            int GranteeID = Convert.ToInt32(Session["NdfyEDgranteeid"]);
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == GranteeID);
            string LMCisubject = grantee.GranteeName + " has completed its annual performance report.";
            string confirmNumber = GenerateKey().ToUpper();
            string LMCibody = "<p><strong>" + grantee.GranteeName + "</strong> has completed its annual performance report. To access a PDF of the completed report, go to MAPS on the MSAP Center private workspace and then select the grantee name and report period from the “Print Final Report” dropdown menu.</p><p>The confirmation number for this grantee is <b>" + confirmNumber + "</b></p>";

            string granteeBody = "<p>This confirms we received your Annual Performance Report on " + DateTime.Now.ToString("MMMM dd, yyyy")
                            + " at " + DateTime.Now.ToShortTimeString() + ". <br/> Your confirmation number is: <b>" + confirmNumber + "</b>. Save this page for your records. </p> <p>Please mail the ED 524B Cover Sheet with the original signature of the current authorized representative of your grant to:<br/><br/> </p> <p> Your Program Officer<br/> MSAP Team<br/> Parental Options and Improvement <br/> Office of Innovation and Improvement <br/> U.S. Department of Education<br/> 400 Maryland Avenue, SW <br/> Washington, DC 20202 </p>";
            string granteeSubject = " The submitted report has been received. ";
            
            if (isProductionSite)
            {
                if (Session["NdfyEDgranteeid"] != null && !string.IsNullOrEmpty(Session["NdfyEDgranteeid"].ToString()))
                {
                    
                    var data = MagnetGranteeContact.SingleOrDefault(x => x.LinkID == GranteeID && x.ContactType == 1);  //1: Project Director
                    string message = "";
                    if (string.IsNullOrEmpty(data.Email))
                        message = "This step cannot be completed unless you contact the MSAP Center to update your missing email information.";
                    else
                    {
                        try
                        {
                            MailAddress m = new MailAddress(data.Email);
                        }
                        catch (FormatException)
                        {
                            message = "This step cannot be completed unless you contact the MSAP Center to update your proper email information.";
                        }
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('" + message + "');</script>", false);
                        return;
                    }

                   
                    if (data != null)
                    {
                        //send email to LMCi
                        //string strTo = "bbrown@leedmanagement.onmicrosoft.com";
                        string strTo = "msapcenter@leedmanagement.onmicrosoft.com";
                        string strFrom = "msapcenter@leedmanagement.onmicrosoft.com";
                        MailMessage objMailMsg = new MailMessage(strFrom, strTo);


                       // objMailMsg.CC.Add("msapcenter@leedmanagement.onmicrosoft.com");

                        objMailMsg.CC.Add("msap.team@ed.gov"); 

                        objMailMsg.BodyEncoding = Encoding.UTF8;
                        objMailMsg.Subject = LMCisubject;
                        objMailMsg.Body = LMCibody;
                        objMailMsg.Priority = MailPriority.High;
                        objMailMsg.IsBodyHtml = true;

                        //prepare to send mail via SMTP transport
                        SmtpClient objSMTPClient = new SmtpClient();
                        objSMTPClient.Host = "mail2.seiservices.com";
                        NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                        objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                        try
                        {
                            objSMTPClient.Send(objMailMsg);
                        }
                        catch (Exception exc)
                        {
                            Response.Write("Send failure: " + exc.ToString());
                        }


                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Thank you for submitting your data. ED will review your submission and contact you should anything need to be changed.');</script>", false);


                        //send to grantee
                        MailMessage MailMsg = new MailMessage(strFrom, data.Email);
                        MailMsg.BodyEncoding = Encoding.UTF8;

                        MailMsg.Subject = granteeSubject;
                        MailMsg.Body = granteeBody;
                        MailMsg.Priority = MailPriority.High;
                        MailMsg.IsBodyHtml = true;

                        //prepare to send mail via SMTP transport
                        SmtpClient SMTPClient = new SmtpClient();
                        // SMTPClient.Host = "sei-exch01.synergyentinc.local";
                        SMTPClient.Host = "mail2.seiservices.com";
                        userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                        SMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                        SMTPClient.Send(MailMsg);

                        ltlmessage.Text = granteeBody;

                        Session["NdfyEDgranteeid"] = null;

                        //write to db

                        var confirm = new EDConfirmation();
                        confirm.GranteeName = grantee.GranteeName;
                        confirm.UserName = HttpContext.Current.User.Identity.Name;
                        confirm.ConfirmNo = confirmNumber;
                        confirm.Created = DateTime.Now;
                        confirm.Save();
                    }

                }
            }
            else  //dev/stg
            {
                string loginUserEmail = Membership.GetUser(User.Identity.Name).Email;

                //send email to QA tester
                string strTo = "NSayal@seiservices.com";
                //string strFrom = "msapcenter@leedmanagement.onmicrosoft.com";
                string strFrom = "msapcenter@leedmanagement.onmicrosoft.com";
                MailMessage objMailMsg = new MailMessage(strFrom, strTo);

                //objMailMsg.CC.Add("msapcenter@leedmanagement.onmicrosoft.com");

                //objMailMsg.CC.Add("NSayal@seiservices.com");
                objMailMsg.CC.Add("jwang@seiservices.com"); 

                objMailMsg.BodyEncoding = Encoding.UTF8;
                objMailMsg.Subject = LMCisubject;
                objMailMsg.Body = LMCibody;
                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

                //prepare to send mail via SMTP transport
                SmtpClient objSMTPClient = new SmtpClient();
                objSMTPClient.Host = "mail2.seiservices.com";
                NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                //NetworkCredential userCredential = new NetworkCredential("mail2.seiservices.com", "");
                objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                objSMTPClient.Send(objMailMsg);

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Thank you for submitting your data. ED will review your submission and contact you should anything need to be changed.');</script>", false);


                //send to Grantee!
                loginUserEmail = "jwang@seiservices.com";
                MailMessage MailMsg = new MailMessage(strFrom, loginUserEmail);
                MailMsg.BodyEncoding = Encoding.UTF8;

                MailMsg.Subject = granteeSubject;
                MailMsg.Body = granteeBody;
                MailMsg.Priority = MailPriority.High;
                MailMsg.IsBodyHtml = true;

                //prepare to send mail via SMTP transport
                SmtpClient SMTPClient = new SmtpClient();
                // SMTPClient.Host = "mail2.seiservices.com.synergyentinc.local";
                SMTPClient.Host = "mail2.seiservices.com";
                userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                SMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                SMTPClient.Send(MailMsg);

                ltlmessage.Text = granteeBody;

                Session["NdfyEDgranteeid"] = null;

                //write to db

                var confirm = new EDConfirmation();
                confirm.GranteeName = grantee.GranteeName;
                confirm.UserName = HttpContext.Current.User.Identity.Name;
                confirm.ConfirmNo = confirmNumber;
                confirm.Created = DateTime.Now;
                confirm.Save();
                
            }
        }
        else
            ltlmessage.Text = "Invalid request to notification. Please go back to <a href='mygrantee.aspx'>Notify ED</a>.";


    }

    private string GenerateKey()
    {
        long i = 1;
        foreach (byte b in Guid.NewGuid().ToByteArray())
        {
            i *= ((int)b + 1);
        }
        return string.Format("{0:x}", i - DateTime.Now.Ticks).Substring(0,5);
    }


}