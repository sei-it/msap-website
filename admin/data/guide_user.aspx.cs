﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_guide_user : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int periodID = Session["ReportPeriodID"]!=null?Convert.ToInt32(Session["ReportPeriodID"]):Convert.ToInt32(MagnetReportPeriod.Find(x=>x.isActive).Last().ID);

        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        var guides = MagnetUserGuide.Find(x => x.reportPeriodID == periodID);
        if (guides != null)
        {
            dlstUguide.DataSource = guides.ToDataTable();
            dlstUguide.DataBind();
        }

    }
}