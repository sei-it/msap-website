﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="manageenrollment.aspx.cs" Inherits="admin_data_manageenrollment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage Enrollment Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h1>
            <a href="datareport.aspx">Home</a> --> Manage Enrollment Data</h1>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New Enrollment Data" CssClass="msapBtn" OnClick="OnAddData" />
        </p>
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="TypeName" SortExpression="" HeaderText="Report Type" />
                <asp:BoundField DataField="AmericanIndian" SortExpression="" HeaderText="American Indian" />
                <asp:BoundField DataField="Asian" SortExpression="" HeaderText="Asian" />
                <asp:BoundField DataField="AfircanAmerican" SortExpression="" HeaderText="Afircan American" />
                <asp:BoundField DataField="Hispanic" SortExpression="" HeaderText="Hispanic" />
                <asp:BoundField DataField="White" SortExpression="" HeaderText="White" />
                <asp:BoundField DataField="MultiRacial" SortExpression="" HeaderText="Multi Racial" />
                <asp:BoundField DataField="UnknownRacial" SortExpression="" HeaderText="Unknown Racial" />
                <asp:BoundField DataField="TotalApplicants" SortExpression="" HeaderText="Total Applicants" />                
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
</asp:Content>

