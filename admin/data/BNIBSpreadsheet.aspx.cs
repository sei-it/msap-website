﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using System.Data;


public partial class admin_data_BNIBSpreadsheet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            LoadData();

        
            //FileUpload1.Attributes.Add("onchange", "return validateFileUpload(this);");
            uploadNarrative.Attributes.Add("onchange", "return validateFileUpload(this);");
            uploadItemize.Attributes.Add("onchange", "return validateFileUpload(this);");

            int rptPeriodID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(Session["ReportID"])).ReportPeriodID;

            //if (rptPeriodID == 7) //cohort 2013 apr
            //    ltlItemiedDes.Text = "please upload your itemized budget.";
            //else
            ltlItemiedDes.Text = "Please upload your itemized budget or type in the space provided.";
        }

    }

    protected void UploadFile(string strName)
    {
        FileUpload FileUpload1 = new FileUpload();
        int intFormID = 0;
        string filepath = "";
        if (strName == "Narrative")
            FileUpload1 = uploadNarrative;
        else
            FileUpload1 = uploadItemize;



        //13: budget narrative; 14: budget itemized
        if (FileUpload1.ID == "uploadNarrative")
        {
            intFormID = 13;
            filepath = "/budget narrative/";
        }
        else
        {
            intFormID = 14;
            filepath = "/budget itemized/";
        }

        if (FileUpload1.HasFile && FileUpload1.FileName.ToLower().EndsWith(".pdf"))
        {
            //SaveData();
            int ReportID = Convert.ToInt32(hfReportID.Value);
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

            string strReportPeriod = period.ReportPeriod;
            strReportPeriod += Convert.ToBoolean(period.PeriodType) ? "-Ad hoc" : "-APR";
            string strGrantee = grantee.GranteeName.Trim().Replace("#","");

            string strFileroot = Server.MapPath("") + "/../upload/";

            string strPath = strReportPeriod + "/" + strGrantee + filepath;
            string strFilename = FileUpload1.FileName.Replace("#", "");
            string strPathandFile = strPath + strFilename.ToLower();

            int projectid = Convert.ToInt32(hfReportID.Value);
            MagnetUpload mupload = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == strFilename.ToLower());

            if (mupload == null)
            {
                MagnetUpload ExecutiveSummary = new MagnetUpload();
                ExecutiveSummary.ProjectID = ReportID;
                ExecutiveSummary.FormID = intFormID; //Budge narrative/itemized
                ExecutiveSummary.FileName = strFilename;


                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strFileroot + strPath);
                }

                FileUpload1.SaveAs(strFileroot + strPathandFile);

                ExecutiveSummary.PhysicalName = strPathandFile;

                ExecutiveSummary.UploadDate = DateTime.Now;
                ExecutiveSummary.Save();


                var muhs = MagnetUpdateHistory.Find(x => x.ReportID == ReportID && x.FormIndex == 4); //4: budget narrative/itemized
                if (muhs.Count > 0)
                {
                    muhs[0].UpdateUser = Context.User.Identity.Name;
                    muhs[0].TimeStamp = DateTime.Now;
                    muhs[0].Save();
                }
                else
                {
                    MagnetUpdateHistory muh = new MagnetUpdateHistory();
                    muh.ReportID = ReportID;
                    muh.FormIndex = 4; //4: budget narrative/itemized
                    muh.UpdateUser = Context.User.Identity.Name;
                    muh.TimeStamp = DateTime.Now;
                    muh.Save();
                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('This file name already exists. Please rename the file.');</script>", false);
                LoadData();
            }
        }


    }
    protected void OnDelete1(object sender, EventArgs e)
    {
        try
        {
            MagnetUpload ExecutiveSummary = MagnetUpload.SingleOrDefault(x => x.ID == Convert.ToInt32(HiddenField1.Value) && x.FormID == 13);
            if (System.IO.File.Exists(Server.MapPath("") + "/../upload/" + ExecutiveSummary.PhysicalName))
                System.IO.File.Delete(Server.MapPath("") + "/../upload/" + ExecutiveSummary.PhysicalName);
            ExecutiveSummary.Delete();
        }
        catch (Exception ex) { }
        LoadData();
    }
    protected void OnDelete2(object sender, EventArgs e)
    {
        try
        {
            MagnetUpload ExecutiveSummary = MagnetUpload.SingleOrDefault(x => x.ID == Convert.ToInt32(HiddenField2.Value) && x.FormID == 14);
            System.IO.File.Delete(Server.MapPath("") + "/../upload/" + ExecutiveSummary.PhysicalName);
            ExecutiveSummary.Delete();
        }
        catch (Exception ex) { }
        LoadData();
    }

    private void LoadData()
    {
        var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));
        if (data.Count > 0)
        {
            hfID.Value = data[0].ID.ToString();
            txtBudgetSummary.Text = data[0].BudgetSummary;
            //summaryLength.Value = wordCount(data[0].BudgetSummary);

            txtItemize.Text = data[0].BudgetItemized;
            //txtItemizeLen.Value = wordCount(data[0].BudgetItemized);
        }


        var files = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value)
                   && x.FormID == 13);
        if (files.Count > 0)
        {
            FileTableRow1.Visible = true;

            //First file
            FileTableRow1.Visible = true;
            UploadedFile1.HRef = "../upload/" + files[0].PhysicalName;
            lblFile1.Text = files[0].FileName;
            HiddenField1.Value = Convert.ToString(files[0].ID);
            uploadNarrative.Enabled = false;
            UploadBtn.Enabled = false;
        }
        else
        {
            FileTableRow1.Visible = false;
            UploadBtn.Enabled = true;
            uploadNarrative.Enabled = true;
        }

        files = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value)
                   && x.FormID == 14);
        if (files.Count > 0)
        {
            //Second file
            FileTableRow2.Visible = true;
            UploadedFile2.HRef = "../upload/" + files[0].PhysicalName;
            lblFile2.Text = files[0].FileName;
            HiddenField2.Value = Convert.ToString(files[0].ID);
            uploadItemize.Enabled = false;
            btnItemizeUpload.Enabled = false;
        }
        else
        {
            FileTableRow2.Visible = false;
            btnItemizeUpload.Enabled = true;
            uploadItemize.Enabled = true;
        }

    }

    private string wordCount(string words)
    {
        string[] a = words.Split(new char[] { ' ', ',', ';', '.', '!', '"', '(', ')', '?' }, StringSplitOptions.RemoveEmptyEntries);
        return a.Count().ToString();
    }

    protected void OnSave(object sender, EventArgs e)
    {
        string uploadNarrativeFileName = uploadNarrative.FileName, uploadItemizeFileName = uploadItemize.FileName, alertmsg = "";
        int projectid = Convert.ToInt32(hfReportID.Value);

        MagnetUpload mupload = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && (x.FileName == uploadNarrativeFileName || x.FileName == uploadItemizeFileName));
        MagnetUpload mupPlanCopy = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == uploadNarrativeFileName);
        MagnetUpload mupSchoolBoard = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == uploadItemizeFileName);


        if (mupPlanCopy != null && !string.IsNullOrEmpty(uploadNarrativeFileName.Trim()))
        {
            alertmsg = "<script>alert('The file name " + uploadNarrativeFileName + " already exists. Please rename the file.');</script>";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmationVPlan", alertmsg, false);
            LoadData();
            return;
        }
        if (mupSchoolBoard != null && !string.IsNullOrEmpty(uploadItemizeFileName.Trim()))
        {
            alertmsg = "<script>alert('The file name " + uploadItemizeFileName + " already exists. Please rename the file.');</script>";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmationVPlan", alertmsg, false);
            LoadData();
            return;
        }
        if (uploadNarrativeFileName.Trim() == uploadItemizeFileName.Trim() && !string.IsNullOrEmpty(uploadNarrativeFileName.Trim()))
        {
            alertmsg = "<script>alert('The file name " + uploadItemizeFileName + " already exists. Please rename the file.');</script>";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmationVPlan", alertmsg, false);
            LoadData();
            return;
        }
        if (uploadNarrative.HasFile && uploadNarrative.FileName.ToLower().EndsWith(".pdf"))
            UploadFile("Narrative");

        if (uploadItemize.HasFile && uploadItemize.FileName.ToLower().EndsWith(".pdf"))
            UploadFile("Itemized");

        SaveData();
        LoadData();
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    private void SaveData()
    {
        MagnetBudgetInformation Budget = new MagnetBudgetInformation();
        if (!string.IsNullOrEmpty(hfID.Value))
            Budget = MagnetBudgetInformation.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));

        Budget.GranteeReportID = Convert.ToInt32(hfReportID.Value);
        Budget.BudgetSummary = txtBudgetSummary.Text;
        Budget.BudgetItemized = txtItemize.Text;
        Budget.Save();
        if (string.IsNullOrEmpty(hfID.Value))
            hfID.Value = Budget.ID.ToString();

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 4); //ED 524B Budget Summary
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 4; //ED 524B Budget Summary
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
    }

    protected void OnPrint(object sender, EventArgs e)
    {

        SaveData();
        LoadData();
        //List of grantee report
        GranteeReport currentReport = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        int granteeID = (int)currentReport.GranteeID;
        MagnetGrantee Grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);
        using (System.IO.MemoryStream output = new MemoryStream())
        {
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, output);
            document.Open();
            PdfContentByte pdfContentByte = pdfWriter.DirectContent;

            List<string> TmpPDFs = new List<string>();

            try
            {
                PdfStamper ps = null;

                // read existing PDF document
                string TmpPDF = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                if (File.Exists(TmpPDF))
                    File.Delete(TmpPDF);
                TmpPDFs.Add(TmpPDF);

                PdfReader r = new PdfReader(Server.MapPath("../doc/ed524budget.pdf"));
                ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                AcroFields af = ps.AcroFields;

                decimal[] BudgetTotal = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                decimal ColumnTotal = 0;
                decimal ColumnTotalNonFederal = 0;
                string[] affix = { "a", "b", "c", "d", "e", "f" };

                int idx = 1;
                int cnn = 1;
                int ReportID = 0;
                int cohortBoundary = 6;
               
                string summary = "", itemized = "";
                if (currentReport.ReportPeriodID > 6) //cohort 2013
                {
                    idx = 7;
                    cohortBoundary = 12;
                    cnn = 7;
                }



                while (idx < cohortBoundary && idx <= currentReport.ReportPeriodID)
                {
                    
                    //Each grant year has two reports, use ad hoc report if it's ready
                    var adhocReports = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == idx + 1 && x.ReportType == false);
                    var reports = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == idx && x.ReportType == false);

                    if (reports.Count > 0 || adhocReports.Count > 0)
                    {
                        GranteeReport report;
                        MagnetBudgetInformation MBI = null;
                        if (adhocReports.Count > 0)
                        {
                            var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == adhocReports[0].ID);

                            if (data.Count > 0)
                                MBI = data[0];
                            report = adhocReports[0];
                        }
                        else
                        {
                            var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == reports[0].ID);
                            if (data.Count > 0)
                                MBI = data[0];

                            report = reports[0];
                            ReportID = Convert.ToInt32(report.ID);
                        }
                        if (MBI != null)
                        {
                            //ReportID = report.ID;
                            ColumnTotal = 0;
                            ColumnTotalNonFederal = 0;
                            decimal dd = 0;

                            //summary = MBI.BudgetSummary;
                            //itemized = MBI.BudgetItemized;
                            //Personnel
                            af.SetField("1" + affix[idx - cnn] + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.Personnel == null ? 0 : MBI.Personnel));
                            ColumnTotal += (decimal)(MBI.Personnel==null?0:MBI.Personnel);
                            BudgetTotal[0] += (decimal)(MBI.Personnel == null ? 0 : MBI.Personnel);
                            af.SetField("B" + cnn.ToString() + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.PersonnelNF == null ? 0 : MBI.PersonnelNF));
                            ColumnTotalNonFederal += (decimal)(MBI.PersonnelNF==null?0:MBI.PersonnelNF);
                            BudgetTotal[13] += (decimal)(MBI.PersonnelNF == null ? 0 : MBI.PersonnelNF);

                            //Fringe Benefits
                            dd = (decimal)(MBI.Fringe==null?0:MBI.Fringe);
                            af.SetField("2" + affix[idx - cnn] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[1] += dd;
                            dd = (decimal)(MBI.FringeNF==null?0:MBI.FringeNF);
                            af.SetField("B" + cnn.ToString() + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[14] += dd;

                            //Travel
                            dd = (decimal)(MBI.Travel==null?0:MBI.Travel);
                            af.SetField("3" + affix[idx - cnn] + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[2] += dd;
                            dd = (decimal)(MBI.TravelNF==null?0:MBI.TravelNF);
                            af.SetField("B" + cnn.ToString() + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[15] += dd;

                            //Equipment
                            dd = (decimal)(MBI.Equipment==null?0:MBI.Equipment);
                            af.SetField("4" + affix[idx - cnn] + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[3] += dd;
                            dd = (decimal)(MBI.EquipmentNF==null?0:MBI.EquipmentNF);
                            af.SetField("B" + cnn.ToString() + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[16] += dd;

                            //Supplies
                            dd = (decimal)(MBI.Supplies==null?0:MBI.Supplies);
                            af.SetField("5" + affix[idx - cnn] + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[4] += dd;
                            dd = (decimal)(MBI.SuppliesNF==null?0:MBI.SuppliesNF);
                            af.SetField("B" + cnn.ToString() + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[17] += dd;

                            //contractual
                            dd = (decimal)(MBI.Contractual==null?0:MBI.Contractual);
                            af.SetField("6" + affix[idx - cnn] + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[5] += dd;
                            dd = (decimal)(MBI.ContractualNF==null?0:MBI.ContractualNF);
                            af.SetField("B" + cnn.ToString() + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[18] += dd;

                            //Construction
                            dd = (decimal)(MBI.Construction==null?0:MBI.Construction);
                            af.SetField("7" + affix[idx - cnn] + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[6] += dd;
                            dd = (decimal)(MBI.ConstructionNF==null?0:MBI.ConstructionNF);
                            af.SetField("B" + cnn.ToString() + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[19] += dd;

                            //Other
                            dd = (decimal)(MBI.Other==null?0:MBI.Other);
                            af.SetField("8" + affix[idx - cnn] + " Other", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[7] += dd;
                            dd = (decimal)(MBI.OtherNF==null?0:MBI.OtherNF);
                            af.SetField("B" + cnn.ToString() + " Other", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[20] += dd;

                            //Total Direct Costs
                            if (ColumnTotal.CompareTo((decimal)0.0) >= 0)
                            {
                                af.SetField("9" + affix[idx - cnn] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                BudgetTotal[8] += ColumnTotal;
                            }

                            if (ColumnTotalNonFederal.CompareTo((decimal)0.0) >= 0)
                            {
                                af.SetField("B" + cnn.ToString() + " Total Direct Costs", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                BudgetTotal[21] += ColumnTotalNonFederal;
                            }

                            dd = (decimal)(MBI.IndirectCost==null?0:MBI.IndirectCost);
                            af.SetField("10" + affix[idx - cnn] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[9] += dd;
                            dd = (decimal)(MBI.IndirectCostNF==null?0:MBI.IndirectCostNF);
                            af.SetField("B" + cnn.ToString() + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[22] += dd;

                            dd = (decimal)(MBI.TrainingStipends==null?0:MBI.TrainingStipends);
                            af.SetField("11" + affix[idx - cnn] + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[10] += dd;
                            dd = (decimal)(MBI.TrainingStipendsNF==null?0:MBI.TrainingStipendsNF);
                            af.SetField("B" + cnn.ToString() + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[23] += dd;

                            //Total Costs
                            af.SetField("12" + affix[idx - cnn] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                            BudgetTotal[11] += ColumnTotal;

                            af.SetField("B" + cnn.ToString() + " Total Costs Lines", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                            BudgetTotal[24] += ColumnTotalNonFederal;
                        }
                    }

                    idx += 2;
                    cnn++;
                }

                //Total expense
                idx = 6;
                af.SetField("1" + affix[idx - 1] + " Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[0]));
                af.SetField("2" + affix[idx - 1] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[1]));
                af.SetField("3" + affix[idx - 1] + " Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[2]));
                af.SetField("4" + affix[idx - 1] + " Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[3]));
                af.SetField("5" + affix[idx - 1] + " Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[4]));
                af.SetField("6" + affix[idx - 1] + " Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[5]));
                af.SetField("7" + affix[idx - 1] + " Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[6]));
                af.SetField("8" + affix[idx - 1] + " Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[7]));
                af.SetField("9" + affix[idx - 1] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", BudgetTotal[8]));
                af.SetField("10" + affix[idx - 1] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[9]));
                af.SetField("11" + affix[idx - 1] + " Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[10]));
                af.SetField("12" + affix[idx - 1] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", BudgetTotal[11]));

                //Indirect Cost Information from lastest cover sheet
                af.SetField("B6 Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[13]));
                af.SetField("B6 Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[14]));
                af.SetField("B6 Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[15]));
                af.SetField("B6 Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[16]));
                af.SetField("B6 Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[17]));
                af.SetField("B6 Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[18]));
                af.SetField("B6 Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[19]));
                af.SetField("B6 Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[20]));
                af.SetField("B6 Total Direct Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[21]));
                af.SetField("B6 Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[22]));
                af.SetField("B6 Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[23]));
                af.SetField("B6 Total Costs Lines", "$" + string.Format("{0:#,0.00}", BudgetTotal[24]));

                af.SetField("InstitutionOrganization", Grantee.GranteeName);

                //Curent report period?
                var bddata = MagnetBudgetInformation.Find(x => x.GranteeReportID == currentReport.ID);
                if (bddata.Count > 0)
                {
                    MagnetBudgetInformation bdMBI = bddata[0];
                    if (bdMBI.IndirectCostAgreement != null)
                    {
                        if ((bool)bdMBI.IndirectCostAgreement)
                        {
                            af.SetField("AgreementYes", "Yes");
                            if (!string.IsNullOrEmpty(bdMBI.AgreementFrom))
                            {
                                string[] strs = bdMBI.AgreementFrom.Split('/');
                                af.SetField("PeriodFrom1", strs[0]);
                                af.SetField("PeriodFrom2", strs.Count() >= 2 ? strs[1] : "");
                                af.SetField("PeriodFrom3", strs.Count() >= 3 ? strs[2] : "");
                            }
                            if (!string.IsNullOrEmpty(bdMBI.AgreementTo))
                            {
                                string[] strs = bdMBI.AgreementTo.Split('/');
                                af.SetField("PeriodTo1", strs[0]);
                                af.SetField("PeriodTo2", strs.Count() >= 2 ? strs[1] : "");
                                af.SetField("PeriodTo3", strs.Count() >= 3 ? strs[2] : "");
                            }
                            if (bdMBI.ApprovalAgency != null)
                            {
                                if ((bool)bdMBI.ApprovalAgency)
                                    af.SetField("Ed", "Yes");
                                else
                                {
                                    af.SetField("Other", "Yes");
                                    af.SetField("Other approving agency", bdMBI.OtherAgency);
                                }
                            }
                            af.SetField("Indirect Cost Rate Percentage", Convert.ToString(bdMBI.IndirectCostRate));
                        }
                        else
                            af.SetField("AgreementNo", "Yes");
                    }

                    if (bdMBI.RestrictedRateProgram != null)
                    {
                        if (!(bool)bdMBI.na)
                        {
                            if ((bool)bdMBI.RestrictedRateProgram)
                                af.SetField("Is it included in ICRA", "Yes");
                            else
                                af.SetField("Complies with 34 CFR", "Yes");
                        }
                    }
                    af.SetField("Restricted Indirect Cost Rate Percentage", Convert.ToString(bdMBI.RestrictedRate));
                }

                ps.FormFlattening = true;

                r.Close();
                ps.Close();

                //Add to final report
                PdfReader reader = new PdfReader(TmpPDF);
                document.SetPageSize(PageSize.A4.Rotate());
                document.NewPage();
                PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                pdfContentByte.AddTemplate(importedPage, 24, -10);
                document.NewPage();
                PdfImportedPage importedPage2 = pdfWriter.GetImportedPage(reader, 2);
                pdfContentByte.AddTemplate(importedPage2, 24, -20);

                document.NewPage();
                PdfImportedPage importedPage3 = pdfWriter.GetImportedPage(reader, 3);
                pdfContentByte.AddTemplate(importedPage3, 24, -10);
                reader.Close();

                MagnetBudgetInformation MBIf = null;
                var reportsf = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == currentReport.ReportPeriodID );
                var dataf = MagnetBudgetInformation.Find(x => x.GranteeReportID == reportsf[0].ID);
                if (dataf.Count > 0)
                    MBIf = dataf[0];

                ReportID = Convert.ToInt32(reportsf[0].ID);
                summary = MBIf.BudgetSummary;
                itemized = MBIf.BudgetItemized;

                if (!string.IsNullOrEmpty(summary))
                {
                    //Add budget summary
                    string ContentPDF = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                    if (File.Exists(ContentPDF))
                        File.Delete(ContentPDF);
                    TmpPDFs.Add(ContentPDF);

                    Document ContentDocument = new Document(PageSize.A4.Rotate(), 10f, 10f, 20f, 20f);
                    PdfWriter ContentWriter = PdfWriter.GetInstance(ContentDocument, new FileStream(ContentPDF, FileMode.Create));
                    ContentDocument.Open();
                    //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font Time12Bold = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.BOLD);
                    ContentDocument.Add(new Paragraph(new Chunk("Budget Narrative", Time12Bold)));
                    ContentDocument.Add(new Paragraph(new Chunk(summary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                    ContentDocument.Close();

                    //Add to output
                    PdfReader freader = new PdfReader(ContentPDF);
                    for (int j = 1; j <= freader.NumberOfPages; j++)
                    {
                        document.SetPageSize(PageSize.A4.Rotate());
                        document.NewPage();
                        PdfImportedPage fimportedPage = pdfWriter.GetImportedPage(freader, j);
                        pdfContentByte.AddTemplate(fimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                    }
                    freader.Close();
                }

                float width = 0;
                float height = 0;


                document.SetPageSize(PageSize.A4);
                foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && (x.FormID == 13)))
                {
                    PdfReader bsuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                    for (int t = 1; t <= bsuploadReader.NumberOfPages; t++)
                    {
                        width = bsuploadReader.GetPageSize(t).Width;
                        height = bsuploadReader.GetPageSize(t).Height;
                        if (width > height)
                            document.SetPageSize(PageSize.A4.Rotate());
                        else
                            document.SetPageSize(PageSize.A4);
                        document.NewPage();
                        PdfImportedPage bsuploadPage = pdfWriter.GetImportedPage(bsuploadReader, t);
                        pdfContentByte.AddTemplate(bsuploadPage, 0, 0);
                    }
                    bsuploadReader.Close();
                }

                if (!string.IsNullOrEmpty(itemized))
                {
                    //Add budget summary
                    string ContentPDF = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                    if (File.Exists(ContentPDF))
                        File.Delete(ContentPDF);
                    TmpPDFs.Add(ContentPDF);

                    Document ContentDocument = new Document(PageSize.A4.Rotate(), 10f, 10f, 20f, 20f);
                    PdfWriter ContentWriter = PdfWriter.GetInstance(ContentDocument, new FileStream(ContentPDF, FileMode.Create));
                    ContentDocument.Open();
                    //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font Time12Bold = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.BOLD);
                    ContentDocument.Add(new Paragraph(new Chunk("Itemized Budget Spreadsheet", Time12Bold)));
                    ContentDocument.Add(new Paragraph(new Chunk(itemized, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                    ContentDocument.Close();

                    //Add to output
                    PdfReader freader = new PdfReader(ContentPDF);
                    for (int j = 1; j <= freader.NumberOfPages; j++)
                    {
                        document.SetPageSize(PageSize.A4.Rotate());
                        document.NewPage();
                        PdfImportedPage fimportedPage = pdfWriter.GetImportedPage(freader, j);
                        pdfContentByte.AddTemplate(fimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                    }
                    freader.Close();
                }

                //Uploaded budget summary file
                document.SetPageSize(PageSize.A4);
                foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && (x.FormID == 14)))
                {
                    PdfReader bsuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                    for (int t = 1; t <= bsuploadReader.NumberOfPages; t++)
                    {
                        width = bsuploadReader.GetPageSize(t).Width;
                        height = bsuploadReader.GetPageSize(t).Height;
                        if (width > height)
                            document.SetPageSize(PageSize.A4.Rotate());
                        else
                            document.SetPageSize(PageSize.A4);
                        document.NewPage();
                        PdfImportedPage bsuploadPage = pdfWriter.GetImportedPage(bsuploadReader, t);
                        pdfContentByte.AddTemplate(bsuploadPage, 0, 0);
                    }
                    bsuploadReader.Close();
                }

            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("Budget report:", ex);
            }
            finally
            {
            }
            if (document.IsOpen())
                document.CloseDocument();
            foreach (string str in TmpPDFs)
            {
                if (File.Exists(str))
                    File.Delete(str);
            }

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Grantee.GranteeName.Replace(' ', '_') + "_Budget_Summary.pdf");
            Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            output.Close();
        }
    }
}