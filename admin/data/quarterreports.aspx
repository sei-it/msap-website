﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="quarterreports.aspx.cs" Inherits="admin_report_quarterreports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Grantee reports
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!--[if IE]>
        <style type="text/css">
        table { border-collapse: collapse; }
        </style>
    <![endif]-->
    <style type="text/css">
        legend
        {
            font-size: 1.0em;
            font-weight: bold;
        }
        img
        {
            color: none;
            border: none;
            background: none;
            background-color: none;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        $(function () {
            $("input:submit").click(function () {
                $("#hfControlID").val($(this).attr("id"));
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <div class="redFont">
        <noscript>
            It appears that Javascript is disabled in your web browser. This report system requires
            Javascript enabled. Please consult IT personel in your organization with how to
            enable Javascript in your system.
        </noscript>
    </div>
    <div class="mainContent">
        <h1>
            <!-- U.S. Department of Education Grant Performance Report (ED 524B)-->
            MSAP Annual Performance System
            <!--<br/><span style="font-style:italic;font-size:14px;">Cohort 2010 | Year 1</span>-->
        </h1>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
        <p>
        Before you begin, please read the <i>Dear Colleague Letter, Instructions for Grant Performance Report (ED 524B Instructions)</i>, and <i>Magnet Schools Assistance Program: Guidance for Reporting GPRA Modernization Act of 2010 Data (GPRA Guide)</i>. To access these documents and to find the <i>MAPS User Guide</i>, which has information about how to use the system’s features, click on the icon in the Resources box below.
        </p>
    </div>
    <div class="reportContent">
            <asp:Panel ID="Panel0" runat="server" GroupingText="Resources">
            <table class="msapQuarterReport">
                <tr class="msapDataTblLastRow">
                    <td width="670">
                        Resources
                    </td>
                    <td width="80" align="center">
                        <a href="guide_user.aspx">
                        <img src="files_icon.jpg" style="vertical-align: top; border: 0px;" title="Click here to view or download the guidance documents."
                    alt="Files icons" /></a>
                    </td>
                    <td width="74">
            
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server" GroupingText="Cover Sheet">
            <table class="msapQuarterReport">
                <tr>
                    <td width="670">
                        Cover Sheet
                    </td>
                    <td width="80" align="center">
                        <a href="reportcoversheet1.aspx">
                            <asp:Image ID="imgCoverSheet" runat="server" ImageUrl="~/images/pencil.png" alt="cover sheet" /></a>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblCoversheet" runat="server"></asp:Label>&nbsp;
                    </td>
                </tr>
                <tr class="msapDataTblLastRow">
                    <td width="670">
                        Executive Summary
                    </td>
                    <td width="80" align="center">
                        <a href="uploadcs.aspx">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/pencil.png" alt="executive summary" /></a>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblExecutiveSummary" runat="server"></asp:Label>&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <br />
        <asp:Panel ID="Panel2" runat="server" GroupingText="SECTION A - Performance Objectives Information and Related Performance Measures Data">
            <table class="msapQuarterReport">
                <tr>
                    <td width="670">
                        Project Status Chart
                    </td>
                    <td width="80" align="center">
                        <a href="statuschart.aspx">
                            <asp:Image ID="imgStatusChart" runat="server" ImageUrl="~/images/pencil.png" alt="Project Status Chart" /></a>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblStatusChart" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr class="msapDataTblLastRow">
                    <td>
                        GPRA Table
                    </td>
                    <td align="center">
                        <a href="managegpra.aspx">
                            <asp:Image ID="imgGPRA" runat="server" ImageUrl="~/images/pencil.png" alt="GPRA Table" /></a>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblGPRA" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <br />
        <asp:Panel ID="Panel3" runat="server" GroupingText="SECTION B - Budget Information">
            <table class="msapQuarterReport">
                <tr class="msapDataTblLastRow">
                    <td width="670">
                        <asp:Label ID="hdBudgetSummary" runat="server" Text="ED 524B Budget Summary"></asp:Label>
                    </td>
                    <td width="80" align="center">
                        <asp:HyperLink ImageUrl="~/images/pencil.png" ID="hlBudgetSummary" runat="server"
                            Text="Ed 524B Form Budget Summary" NavigateUrl="budgetform.aspx"></asp:HyperLink>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblBudgetSummary" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <br />
        <asp:Panel ID="Panel4" runat="server" GroupingText="SECTION C - Additional Information" >
            <table class="msapQuarterReport">
                <tr>
                    <td width="670">
                        <asp:Label ID="hdDesegregation" runat="server" Text="Desegregation Plan Information Forms"></asp:Label>
                    </td>
                    <td width="80" align="center">
                        <asp:HyperLink ID="hlDesegregation" ImageUrl="~/images/pencil.png" runat="server"
                            Text="Desegregation Plan Information Forms" NavigateUrl="desegregation.aspx"></asp:HyperLink>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblDesegregation" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="670">
                        <asp:Label ID="hdAssurance" runat="server" Text="Assurances and Certifications"></asp:Label>
                    </td>
                    <td width="80" align="center">
                        <asp:HyperLink ID="hlAssurance" ImageUrl="~/images/pencil.png" runat="server" Text="Assurance and Certifications"
                            NavigateUrl="ac.aspx"></asp:HyperLink>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblAssurance" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="hdLeaEnrollment" runat="server" Text="Table 1: Enrollment Data-LEA Level"></asp:Label>
                    </td>
                    <td align="center" width="80">
                        <asp:HyperLink ID="hlLeaEnrollment" ImageUrl="~/images/pencil.png" runat="server"
                            Text="Table 7: Enrollment Data-Lea level" NavigateUrl="leaenrollment.aspx"></asp:HyperLink>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblLEAEnrollment" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="hdSchoolYear" runat="server" Text="Table 2: Year of Implementation for Existing Magnet Schools"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:HyperLink ID="hlSchollYear" ImageUrl="~/images/pencil.png" runat="server" Text="Table 8: Year of implementation for existing magnet schools"
                            NavigateUrl="manageschoolyear.aspx"></asp:HyperLink>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblSchoolYear" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="hdMagnetSchool" runat="server" Text="Table 3: Enrollment Data-Magnet Schools"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:HyperLink ID="hlMagnetSchool" ImageUrl="~/images/pencil.png" runat="server"
                            Text="Table 9: Enrollment Data-magnet schools" NavigateUrl="enrollment.aspx?datatype=1&id=2"></asp:HyperLink>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblEnrollment" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="hdFeeder" runat="server" Text="Table 4: Feeder School-Enrollment Data"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:HyperLink ID="hlFeeder" ImageUrl="~/images/pencil.png" runat="server" Text="Table 11: Feeder School enrollment data"
                            NavigateUrl="feederenrollment.aspx?id=3"></asp:HyperLink>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblFeederEnrollment" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr class="msapDataTblLastRow">
                    <td>
                        Additional Information
                    </td>
                    <td align="center">
                        <a href="additional.aspx">
                            <asp:Image ID="imgFeederSchool" runat="server" ImageUrl="~/images/pencil.png" alt="additional information" /></a>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblAdditional" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <br />
        <asp:Panel ID="Panel5" runat="server" GroupingText="Uploaded Documents">
            <table>
                <tr class="msapDataTblLastRow">
                    <td width="670">
                        Uploaded Documents
                    </td>
                    <td width="80" align="center">
                        <a href="manageupload.aspx">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/pencil.png" alt="Upload Documents" />
                        </a>
                    </td>
                    <td width="74">
                        <asp:Label ID="lblUploadDocs" runat="server"></asp:Label>&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
