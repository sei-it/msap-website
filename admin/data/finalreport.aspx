﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="finalreport.aspx.cs" Inherits="admin_data_finalreport" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="EmbeddedTextBoxDemoControl.ascx" TagName="EmbeddedTextBoxDemoControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Magnet School Year
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            Final report</h4>
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:Button runat="server" ID="btn" OnClick="Print" CssClass="surveyBtn1" Text="Print" />
</asp:Content>
