﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="applicationpool.aspx.cs" Inherits="admin_data_applicationpool" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit Application Pool
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h1>
            Add/Edit Applicant Pool</h1>
        <%-- SIP --%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfReportID" runat="server" />
                <table>
                    <tr>
                        <td>
                            School:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                                <asp:ListItem Value="">Please select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlSchool"
                                ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of American Indian/Alaskan Native Students:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAmericanIndian" runat="server" MaxLength="4" CssClass="msapDataTxt" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtAmericanIndian"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of Asian Students:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAsian" runat="server" MaxLength="4" CssClass="msapDataTxt" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAsian"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of Black or African American Students:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAfricanAmerican" runat="server" MaxLength="4" CssClass="msapDataTxt" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtAfricanAmerican"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of Hispanic/Latino Students:
                        </td>
                        <td>
                            <asp:TextBox ID="txtHispanic" runat="server" CssClass="msapDataTxt" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtHispanic"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of White Students:
                        </td>
                        <td>
                            <asp:TextBox ID="txtWhite" runat="server" MaxLength="4" CssClass="msapDataTxt" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtWhite"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Two or more races:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMultiRacial" runat="server" MaxLength="4" CssClass="msapDataTxt" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtMultiRacial"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr style="display:none;">
                        <td>
                            Number of Students of Unknown racial/ethnic groups:
                        </td>
                        <td>
                            <asp:TextBox ID="txtUnknown" runat="server" CssClass="msapDataTxt" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtUnknown"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total Number of Applicants:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTotal" Enabled="false" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                            <asp:Button ID="Button3" runat="server" CssClass="msapBtn" Text="Return" CausesValidation="false"
                                OnClick="OnReturn" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
