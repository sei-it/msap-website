﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="statuschart.aspx.cs" Inherits="admin_data_statuschart"  ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Edit Project Objective
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
        $(document).ready(function () {
            /*$('tr').find('td:contains("No")').each(function () {
                $(this).parent().css("background-color", "#eee");
                $(this).parent().css("color", "#ccc");
            });*/
				$(":input[readonly]").css({"background-color":"#fff","border":"solid 1px #fff","color":"#4e8396","font-weight":"bold","font-family":"Helvetica, San-serif"});
        });
    </script>
    <style>
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managestatuschart.aspx">Project Status Chart</a> <span class="greater_than">
                &gt;</span> Edit Project Objective
            <asp:Label ID="lblSSID" runat="server"></asp:Label></h4>
            <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfStatusID" runat="server" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <br/>
        <div class="titles"><asp:Label ID="lblCurrentObjective" runat="server"></asp:Label></div>
    	<div class="tab_area">
    		<ul class="tabs">
                <div id="tabDiv" runat="server" style="width: 600px; float: right;"></div>
        	</ul>
    	</div>
    	<br/>
        <p class="clear" style="padding-top:5px;">
            Provide quantitative and/or qualitative data for each performance measure and describe findings or outcomes to demonstrate that you have met or are making progress toward meeting the performance measure. You will also explain how your performance measure data demonstrate that you have met or are making progress toward meeting the project objective.
            <img src="../../images/question_mark-thumb.png" title="To report on an existing project objective, click Edit in the last column that corresponds to the objective and enter the appropriate information. When you have finished entering data for this page, click on Save Record before proceeding." class="Q_mark"/></p>
        <table width="835">
        	<tr>
                <td style="color:#f58220;font-weight:bold;">
                    <img align="ABSMIDDLE" src="../../images/head_button.jpg"> Project Objective
                </td>
            </tr>
            <tr>
                <td id="Objective_disabled">
                    <asp:TextBox TextMode="Multiline" ID="txtObjective" runat="server" Font-Size="10pt" MaxLength="2000" 
                    Width="730" CssClass="msapDataTxt change" Wrap="true" ></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtObjective" ErrorMessage="*This field is required."></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td colspan="2" align="right">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>    
        </table>
        <span id="obj_checkbox">
        	<asp:CheckBox ID="cbStatusUpdate" ForeColor="Gray" CssClass="change" runat="server" Text="Check this box if this is a status update from the previous budget period." />
        </span>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            GridLines="None" PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID"
            CssClass="msapDataTbl Obj_tbl" OnRowDataBound="OnRowDataBound">
            <EmptyDataTemplate>
                <table class="emptyTbl">
                    <tr>
                        <th>
                            Performance Measure
                        </th>
                        <th>
                            Measure<br />
                            Type
                        </th>
                        <th>
                            Target Raw<br />
                            Number
                        </th>
                        <th>
                            Target<br />
                            Ratio
                        </th>
                        <th>
                            Target<br />
                            %
                        </th>
                        <th>
                            Actual Raw<br />
                            Number
                        </th>
                        <th>
                            Actual<br />
                            Ratio
                        </th>
                        <th>
                            Actual<br />
                            %
                        </th>
                        <th class="TDWithBottomBorder" style="border-right: 0px !important;">
                            Active
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4" class="TDWithBottomBorder">&nbsp;
                            
                        </td>
                    </tr>
                </table>
                <style>
                    .msapDataTbl td, .msapDataTbl th
                    {
                        padding: 0px;
                        border-right: 0px !important;
                    }
                </style>
            </EmptyDataTemplate>
            <Columns>
                <asp:BoundField DataField="PerformanceMeasure" HtmlEncode="false" ItemStyle-CssClass="PM_align" 
                ItemStyle-Width="30%" HeaderText="<br />Performance Measure" />
                <asp:BoundField DataField="MeasureType" SortExpression="" HeaderText="Measure Type" />
                <asp:BoundField DataField="TargetNumber" SortExpression="" HeaderText="Target Raw Number" />
                <asp:BoundField DataField="TargetRatio" SortExpression="" HeaderText="Target Ratio" />
                <asp:BoundField DataField="TargetPercentage" SortExpression="" HeaderText="Target %" />
                <asp:BoundField DataField="ActualNumber" SortExpression="" HeaderText="Actual Raw Number" />
                <asp:BoundField DataField="AcutalRatio" SortExpression="" HeaderText="Actual Ratio" />
                <asp:BoundField DataField="ActualPercentage" SortExpression="" HeaderText="Actual %" />
                <asp:BoundField DataField="ActiveStatus" SortExpression="" HtmlEncode="false" HeaderText="<br />Active" />
                <asp:TemplateField>
                    <ItemStyle CssClass="TDWithBottomBorder LeftAlignCell_LinksCol" />
                    <ItemTemplate>
                        <asp:LinkButton ID="edit" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                         class="edit" OnClick="OnEdit"></asp:LinkButton><br/>
                        <asp:LinkButton ID="LinkButton2"  ClientIDMode="Static" runat="server" Text='<%# Eval("ActiveAction") %>'
                            CommandArgument='<%# Eval("ID") %>' OnClick="OnActivate" Enabled="true" Visible="false"></asp:LinkButton>
                             <asp:LinkButton ID="Delete" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>' Visible="false"
                         class="edit" OnClick="OnDelete"></asp:LinkButton><br/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="button1" runat="server" Text="Add New Performance Measure" CssClass="surveyBtn3 postbutton"
                        OnClick="OnAddObjectiveDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button4"
                            runat="server" Text="Save Record" CssClass="surveyBtn1 postbutton" OnClick="OnSaveObjective" />
                </td>
            </tr>
        </table>
        <div id="linkDiv" runat="server" style="text-align: right; margin-top: 10px;">
        </div>
    </div>
</asp:Content>
