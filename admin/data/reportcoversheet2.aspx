﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="reportcoversheet2.aspx.cs" Inherits="admin_reportcoversheet2" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    U.S. Department of Education Grant Performance Report Cover Sheet (ED 524B)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });

        function setDirty(sender, args) {
            $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
        }
    </script>
    <style>
        .msapDataTbl tr td:first-child
        {
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Performance Report Cover Sheet (ED 524B)</h4>
<div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <asp:HiddenField ID="hfControlID" runat="server" ClientIDMode="Static" />
    <br />
    <div class="tab_area">
        <div class="titles">
            Performance Report Cover Sheet (ED 524B) - Page 2 of 4</div>
        <ul class="tabs">
            <li><a href="reportcoversheet4.aspx">Page 4</a></li>
            <li><a href="reportcoversheet3.aspx">Page 3</a></li>
            <li class="tab_active">Page 2</li>
            <li><a href="reportcoversheet1.aspx">Page 1</a></li>
        </ul>
    </div>
    <br />
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Budget Expenditures</b></span>
    <table class="msapDataTbl">
        <tr>
            <td style="font-weight: bold; color: #4e8396 !important;" colspan="3" class="TDWithBottomBorder">
                8. &nbsp;&nbsp;Budget Expenditures
                <img src="../../images/question_mark-thumb.png" title="See <i>Dear Colleague Letter and ED 524B Instructions.</i>" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
            <td align="center" class="TDWithBottomBorder" style="color: #4e8396; font-weight: bold;">
                Federal Grant Funds
            </td>
            <td align="center" class="TDWithBottomBorder" style="color: #4e8396; font-weight: bold;">
                Non-Federal Funds
            </td>
        </tr>
        <tr>
            <td style="color: #4e8396 !important;">
                &nbsp;&nbsp;&nbsp;&nbsp;a. &nbsp;&nbsp;Previous Budget Period
            </td>
            <td class="TDWithBottomBorder">
                <telerik:RadNumericTextBox MinValue="0" ID="txtPreviousFederalFunds" runat="server"
                    CssClass="msapDataTxt change" TabIndex="1" Type="Currency" NumberFormat-DecimalDigits="2"
                    Width="151">
                </telerik:RadNumericTextBox>
            </td>
            <td class="TDWithBottomBorder">
                <telerik:RadNumericTextBox MinValue="0" ID="txtPreviousNonFederalFunds" runat="server"
                    CssClass="msapDataTxt change" TabIndex="4" Type="Currency" NumberFormat-DecimalDigits="2"
                    Width="151" Enabled="false">
                </telerik:RadNumericTextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #4e8396 !important;">
                &nbsp;&nbsp;&nbsp;&nbsp;b. &nbsp;&nbsp;Current Budget Period
            </td>
            <td class="TDWithBottomBorder">
                <telerik:RadNumericTextBox MinValue="0" ID="txtCurrentFederalFunds" runat="server"
                    CssClass="msapDataTxt change" TabIndex="2" Type="Currency" NumberFormat-DecimalDigits="2"
                    Width="151">
                </telerik:RadNumericTextBox>
            </td>
            <td class="TDWithBottomBorder">
                <telerik:RadNumericTextBox MinValue="0" ID="txtCurrentNonFederalFunds" runat="server"
                    CssClass="msapDataTxt change" TabIndex="5" Type="Currency" NumberFormat-DecimalDigits="2"
                    Width="151" Enabled="false">
                </telerik:RadNumericTextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #4e8396 !important;">
                &nbsp;&nbsp;&nbsp;&nbsp;c. &nbsp;&nbsp;Entire Project Period (<i>For Final Performance
                    Reports only</i>)
            </td>
            <td class="TDWithBottomBorder">
                <telerik:RadNumericTextBox MinValue="0" ID="txtEntireFederalFunds" runat="server" Enabled="false"
                    CssClass="msapDataTxt change" TabIndex="3" Type="Currency" NumberFormat-DecimalDigits="2"
                    Width="151">
                </telerik:RadNumericTextBox>
            </td>
            <td class="TDWithoutBorder">
                <telerik:RadNumericTextBox MinValue="0" ID="txtEntireNonFederalFunds" runat="server"
                    CssClass="msapDataTxt change" Enabled="false" TabIndex="6" Type="Currency" NumberFormat-DecimalDigits="2"
                    Width="151">
                </telerik:RadNumericTextBox>
            </td>
        </tr>
    </table>
    <br />
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Indirect
            Costs</b></span>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="msapDataTbl">
                <tr>
                    <td style="background-color: #FFF; font-weight: bold; color: #4e8396 !important;"
                        colspan="4" class="TDWithBottomBorder">
                        9. &nbsp;&nbsp;Indirect Costs
                        <img src="../../images/question_mark-thumb.png" title="See <i>Dear Colleague Letter and ED 524B Instructions.</i>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" width="49%" style="color: #4e8396 !important;">
                        &nbsp;&nbsp;&nbsp;&nbsp;a. &nbsp;&nbsp;Are you claiming indirect costs under this
                        grant?<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (If “no” is selected, skip
                        question b, c, and d)
                    </td>
                    <td class="TDWithBottomBorder" style="padding-left: 0px;">
                        <asp:RadioButtonList ID="rblClaimingIndrectCost" CssClass="DotnetTbl change test"
                            runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="OnClaimingIndirect">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="This field is required!"
                            ControlToValidate="rblClaimingIndrectCost"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border-right: 0px; color: #4e8396 !important;">
                        &nbsp;&nbsp;&nbsp;&nbsp;b.
                    </td>
                    <td style="color: #4e8396 !important;">
                        If yes, do you have an Indirect Cost Rate Agreement approved by the Federal Government?<br />
                        (If “no” is selected, skip question c and move on to question d)
                    </td>
                    <td class="TDWithBottomBorder" style="padding-left: 0px; width: 120px;">
                        <asp:RadioButtonList ID="rblRageAggrement" CssClass="DotnetTbl change" runat="server"
                            RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="OnRateAgreementChanged">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="This field is required!"
                            ControlToValidate="rblRageAggrement"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: #4e8396 !important;">
                        &nbsp;&nbsp;&nbsp;&nbsp;c. &nbsp;&nbsp;If yes, provide the following information:
                    </td>
                    <td colspan="2" class="TDWithBottomBorder">
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: #4e8396 !important; vertical-align: top;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Period Covered
                        by the Indirect Cost Rate Agreement:
                    </td>
                    <td colspan="2" class="TDWithBottomBorder">
                        From
                        <asp:TextBox ID="txtAggrementStart" Width="100" runat="server" CssClass="msapDataTxt change"></asp:TextBox>
                        &nbsp;&nbsp;To&nbsp;&nbsp;
                        <asp:TextBox ID="txtAggrementEnd" Width="100" runat="server" CssClass="msapDataTxt change"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="<strong>From</strong> date is required!"
                            ControlToValidate="txtAggrementStart" Display="dynamic"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="<strong>To</strong> date is required!"
                            ControlToValidate="txtAggrementEnd"></asp:RequiredFieldValidator><br />
                        <asp:CompareValidator ID="CompareValidatorTextBox1" runat="server" ControlToValidate="txtAggrementStart"
                            Type="Date" Operator="DataTypeCheck" ErrorMessage="<strong>From</strong> date is not in correct format."
                            Display="dynamic" />
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtAggrementEnd"
                            Type="Date" Operator="DataTypeCheck" ErrorMessage="<strong>To</strong> date is not in correct format."
                            Display="dynamic" />
                        <br />
                        <asp:CompareValidator runat="server" ID="cvEndTime" ControlToValidate="txtAggrementEnd"
                            ControlToCompare="txtAggrementStart" ErrorMessage="<b>To</b> date must be later than From Date"
                            SetFocusOnError="True" Operator="GreaterThan" Type="Date" Display="dynamic" />
                        <ajax:CalendarExtender ID="CalendarExtender3" runat="server" OnClientDateSelectionChanged="setDirty" TargetControlID="txtAggrementStart">
                        </ajax:CalendarExtender>
                        <ajax:CalendarExtender ID="CalendarExtender4" runat="server" OnClientDateSelectionChanged="setDirty" TargetControlID="txtAggrementEnd">
                        </ajax:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: #4e8396 !important;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Approving
                        Federal Agency:
                    </td>
                    <td class="TDWithBottomBorder" style="padding-left: 0px;">
                        <asp:RadioButtonList ID="rblAgency" CssClass="DotnetTbl change" runat="server" RepeatDirection="Horizontal"
                            AutoPostBack="true" OnSelectedIndexChanged="OnOtherAgencyChanged">
                            <asp:ListItem Value="1">ED</asp:ListItem>
                            <asp:ListItem Value="0">Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td class="TDWithBottomBorder">
                        <asp:Panel ID="panelOtherAgency" runat="server" Visible="false" Style="display: inline;">
                            (<i>Please specify</i>):
                            <asp:TextBox ID="txtOtherApprovalAgnecy" runat="server" CssClass="msapDataTxt"></asp:TextBox></asp:Panel>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Text="This field is required!"
                            ControlToValidate="rblAgency" Display="static"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: #4e8396 !important;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type of
                        Rate (<i>For Final Performance Reports Only</i>):
                    </td>
                    <td colspan="2" class="TDWithBottomBorder">
                        <asp:RadioButton ID="rbProvisional" Enabled="false" CssClass="change" Text="Provisional"
                            runat="server" GroupName="typerate" AutoPostBack="true" OnCheckedChanged="OnOtherRateTypeChanged" />
                        &nbsp;&nbsp;
                        <asp:RadioButton ID="rbFinal" Enabled="false" Text="Final" CssClass="change" runat="server"
                            GroupName="typerate" AutoPostBack="true" OnCheckedChanged="OnOtherRateTypeChanged" />
                        <br />
                        <asp:RadioButton Text="Other" Enabled="false" runat="server" CssClass="change" ID="rbOtherRateType"
                            GroupName="typerate" AutoPostBack="true" OnCheckedChanged="OnOtherRateTypeChanged" />
                        <asp:Panel ID="panelOtherRateType" runat="server" Visible="false">
                            (<i>Please specify</i>):
                            <asp:TextBox ID="txtOtherRateType" runat="server" CssClass="msapDataTxt change"></asp:TextBox></asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 0px; color: #4e8396 !important;" valign="top">
                        &nbsp;&nbsp;&nbsp;&nbsp;d.
                    </td>
                    <td style="color: #4e8396 !important;vertical-align:top;">
                        For Restricted Rate Programs (<i>Check one</i>) -- Are you using a restricted indirect
                        cost rate that:
                    </td>
                    <td colspan="2" class="TDWithBottomBorder">
                        <asp:RadioButton GroupName="cfr" ID="cbIncludedApprovedAgreement" runat="server"
                            CssClass="change" Text="Is included in your approved Indirect Cost Rate Agreement?" />
                        <br />
                        <asp:RadioButton GroupName="cfr" ID="cbComplyCFR" runat="server" CssClass="change"
                            Text="Complies with 34 CFR 76.564(c)(2)?" />
                             <br />
                        <asp:RadioButton GroupName="cfr" ID="cbNA" runat="server" CssClass="change"
                            Text="Not Applicable" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <p align="right">
        <table width="100%">
            <tr align="right">
                <td>
                    <asp:ImageButton Width="29" Height="36" ID="ImageButton1" CssClass="postbutton" runat="server"
                        ImageUrl="button_arrow2.jpg" OnClick="OnPrevious" ToolTip="Previous" Visible="false" />
                </td>
                <td style="padding-top: 10px; width: 50px;">
                    <asp:Button ID="Button1" runat="server" Text="Save Record" CssClass="surveyBtn1 postbutton"
                        OnClick="OnSaveData" />
                </td>
                <td width="30">
                    <asp:ImageButton Width="29" Height="36" ID="ImageButton2" CssClass="postbutton" runat="server"
                        ImageUrl="button_arrow.jpg" OnClick="OnNext" ToolTip="Next" Visible="false" />
                </td>
            </tr>
        </table>
    </p>
    <div style="text-align: right; margin-top: 20px;">
        <a href="reportcoversheet1.aspx">Page 1</a>&nbsp;&nbsp;&nbsp;&nbsp; Page 2&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="reportcoversheet3.aspx">Page 3</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="reportcoversheet4.aspx">
            Page 4</a>
    </div>
</asp:Content>
