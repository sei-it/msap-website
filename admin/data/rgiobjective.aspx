﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="rgiobjective.aspx.cs" Inherits="admin_data_rgiobjective" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit MGI Objective
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Add/Edit MGI Objective</h1>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <table>
            <tr>
                <td>
                    Magnet School:
                </td>
                <td>
                    <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    MGI objective:
                </td>
                <td>
                    <asp:DropDownList ID="ddlRGIObjective" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 1:
                </td>
                <td>
                    <asp:DropDownList ID="ddlRacialGroup1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 2:
                </td>
                <td>
                    <asp:DropDownList ID="ddlRacialGroup2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 3:
                </td>
                <td>
                    <asp:DropDownList ID="ddlRacialGroup3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 4:
                </td>
                <td>
                    <asp:DropDownList ID="ddlRacialGroup4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 1:
                </td>
                <td>
                    <asp:DropDownList ID="ddlTargetedGroup1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 2:
                </td>
                <td>
                    <asp:DropDownList ID="ddlTargetedGroup2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 3:
                </td>
                <td>
                    <asp:DropDownList ID="ddlTargetedGroup3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 4:
                </td>
                <td>
                    <asp:DropDownList ID="ddlTargetedGroup4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Met objective?
                </td>
                <td>
                    <asp:DropDownList ID="ddlMeetObjective" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 1:
                </td>
                <td>
                    <asp:DropDownList ID="ddlIncresedTargetedGroup1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 2:
                </td>
                <td>
                    <asp:DropDownList ID="ddlIncresedTargetedGroup2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 3:
                </td>
                <td>
                    <asp:DropDownList ID="ddlIncresedTargetedGroup3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 4:
                </td>
                <td>
                    <asp:DropDownList ID="ddlIncresedTargetedGroup4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Met GPRA measure 1?
                </td>
                <td>
                    <asp:DropDownList ID="ddlMetGPRAMeasure" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />&nbsp;&nbsp;
                    <asp:Button ID="Button3" runat="server" CssClass="msapBtn" Text="Return" OnClick="OnReturn" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
