﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="managereports.aspx.cs" Inherits="admin_data_managereports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    MSAP Center - Manage quarter reports
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>Print final quarter report.</p>
    <table>
        <tr>
            <td>
                Please select grantee
            </td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem Value="" Text="Please select"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Grantee 1"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Please select grantee
            </td>
            <td>
                <asp:DropDownList ID="DropDownList2" runat="server">
                    <asp:ListItem Value="" Text="Please select"></asp:ListItem>
                    <asp:ListItem Value="58" Text="1st quarter"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="Button2" runat="server" Text="Print Finale Report" CssClass="surveyBtn2"
                    OnClick="OnPrint" />
            </td>
        </tr>
    </table>
</asp:Content>

