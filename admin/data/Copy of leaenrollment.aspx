﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="Copy of leaenrollment.aspx.cs" Inherits="admin_report_leaenrollment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP center - Enrollment data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Table 7: Enrollment Data-LEA Level</h4>
    <p>
        For each applicable grade level, enter the number of students belonging to each
        category.</p>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfSchoolID" runat="server" />
    <asp:HiddenField ID="hfStageID" runat="server" />
    <br />
    <div class="tab_area">
        <div class="titles">
            Table 7: Enrollment Data-LEA Level</div>
        <ul class="tabs">
            <li><a href="feederenrollment.aspx?id=3">Table 11</a></li>
            <li><a href="enrollment.aspx">Table 9</a></li>
            <li><a href="manageschoolyear.aspx">Table 8</a></li>
            <li class="tab_active">Table 7</li>
        </ul>
    </div>
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <p>
                <asp:CheckBox runat="server" ID="ckbFirstTimeProgram" Text="Check this box if all the magnet schools included in the program are implementing a magnet program for the first time."
                    AutoPostBack="true" OnCheckedChanged="OnFirstTimeProgramChanged" /></p>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        GridLines="None" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl CenterContent">
        <Columns>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    &nbsp;<br />
                    <br />
                    <br />
                    Grade Level
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("GradeLevel")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    American Indian/Alaskan Native
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("AmericanIndian"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    Asian/Pacific Islander
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("Asian"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    Black or African American
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("AfricanAmerican"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    <br />
                    Hispanic/Latin
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("Hispanic"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    <br />
                    White
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("White"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    Native Hawaiian or Other Pacific Islander
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("Hawaiian"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="12%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    Two or more races
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("MultiRacial"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle CssClass="LeftAlignLeft_noRightBorder" />
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <%-- Resource --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit
                <asp:Label ID="lblLabel" runat="server"></asp:Label>
                Data</div>
            <table class="mpeModalPopup_Tbl">
                <tr>
                    <td colspan="2">
                        <b>
                            <asp:Label ID="lblSchoolNameSec" runat="server"></asp:Label>&nbsp;</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Grade Level:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGradeLevel" runat="server" CssClass="msapDataTxt">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        American Indian/Alaskan Native:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmericanIndian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtAmericanIndian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Asian/Pacific Islander:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAsian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                            TargetControlID="txtAsian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Black or African American:
                    </td>
                    <td>
                        <asp:TextBox ID="txtBlack" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                            TargetControlID="txtBlack">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Hispanic/Latino:
                    </td>
                    <td>
                        <asp:TextBox ID="txtHispanic" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                            TargetControlID="txtHispanic">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        White:
                    </td>
                    <td>
                        <asp:TextBox ID="txtWhite" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                            TargetControlID="txtWhite">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Native Hawaiian or Other Pacific Islander:
                    </td>
                    <td>
                        <asp:TextBox ID="txtHawaiian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                            TargetControlID="txtHawaiian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Two or more races:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMultiRaces" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                            TargetControlID="txtMultiRaces">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        &nbsp;<asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="Button14" runat="server" CssClass="surveyBtn1" Text="Save Record"
                            OnClick="OnSaveData" />&nbsp;&nbsp;
                        <asp:Button ID="Button15" runat="server" CssClass="surveyBtn1" Text="Close Window" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
    <div style="margin-top: 20px; margin-bottom: 20px; text-align: right;">
        &nbsp;&nbsp;<asp:Button ID="Button1" runat="server" CssClass="surveyBtn1" Text="New Data Entry"
            OnClick="OnAdd" />&nbsp;&nbsp;
    </div>
    <div style="text-align: right; margin-top: 20px;">
        Table 7 - Enrollment Data-LEA Level&nbsp;&nbsp;&nbsp;&nbsp; <a href="manageschoolyear.aspx">
            Table 8 - Year of Implementation</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="enrollment.aspx">
                Table 9 - Enrollment Data-Magnet Schools</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="feederenrollment.aspx?id=3">
                    Table 11 - Feeder School</a>
    </div>
</asp:Content>
