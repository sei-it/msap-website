﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="desegregation.aspx.cs" Inherits="admin_data_desegregation" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Edit Desegregation Plan
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            Desegregation Plan</h4>
<div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <div class="titles_noTabs">
            Desegregation Plan</div>
        <br />
        <p class="clear">
            Please choose the type of desegregation plan and upload the appropriate documents as noted for each plan.
        </p>
      
        <table class="msapDataTbl">
        	<tr>
            	<th id="Desg_tbl_headers">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; Type of
                Desegregation Plan<br />
                </th>
                <th id="Desg_tbl_headers">
                	<img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp;Uploaded<br />
                </th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td class="TDWithTopBottomRightBorder tealtxt" width="75%">
                    <strong>A Required Plan</strong><br />
                    A plan that is (1) implemented pursuant to a final order of a court of the United
                    States, or a court of any State, or any other state agency or official of competent
                    jurisdiction and (2) the order requires the desegregation of minority group segregated
                    children or faculty in the elementary and secondary schools of that agency or those
                    agencies.
                </td>
                <td width="15%" class="tbl_align_center">
                	<asp:Image ID="imgRequired" runat="server" ImageUrl="~/images/checkmark.jpg" Visible="false" alt="required plan" />
                </td>
                <td class="TDWithTopBorder TDWithTopOnlyBorder">
                    <a href="required.aspx">Select</a>
                </td>

            </tr>
            <tr>
            	
          <td class="tealtxt">
                    <Strong>A Voluntary Plan</Strong><br />
                    A plan to reduce, eliminate or prevent minority group isolation that is being implemented
                    (or would be implemented if assistance under the Magnet Schools Assistance Program
                    is made available) on either a voluntary basis or as required under Title VI of
                    the Civil Rights Act of 1964.
                </td>
               	<td class="tbl_align_center">
               		<asp:Image ID="imgVolunteer" runat="server" ImageUrl="~/images/checkmark.jpg" Visible="false" alt="volunteer plan" />
                </td>
                <td class="TDWithTopNoRightBorder" >
               		<a href="volunteer.aspx">Select</a>
           	  </td>
            </tr>
        </table>
              <div class="clear">
        </div>
     <!--   <div class="clear">
         
                <p style="float:right;">
                <asp:Button ID="Button3" runat="server" CssClass="surveyBtn" Text="Revie" CausesValidation="false"
                    OnClick="OnPrint" />
            </p>
        </div>-->
    </div>
</asp:Content>
