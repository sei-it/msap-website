﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="GPRAIV.aspx.cs" Inherits="admin_data_gpraiv" ErrorPage="~/Error_page.aspx"
    ViewStateMode="Disabled" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit GPRA Performance Measure 6 Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>

    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });

    </script>
    <style type="text/css">
        .msapDataTbl tr td:first-child
        {
            color: #4e8396 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
            text-align: center;
        }
        .msapDataTbl th
        {
            color: #000;
            text-align: left;
        }
        .TDCenterClass
        {
            text-align: center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managegpra.aspx">GPRA Table</a> <span class="greater_than">&gt;</span>
            Part IV. GPRA Performance Measure 4 Data</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <div class="GPRA_titles">
            <asp:Label ID="lblSchoolName" runat="server" Width="380px"></asp:Label></div>
        <div class="tab_area">
            <ul class="tabs">
                <li><a runat="server" id="tabPartVI" href="gpravi.aspx">Part VI</a></li>
                <li><a runat="server" id="tabPartV" href="gprav.aspx">Part V</a></li>
                <li class="tab_active">Part IV</li>
                <li><a runat="server" id="tabPartIII" href="gpraiii.aspx">Part III</a></li>
                <li><a runat="server" id="tabPartII" href="gpraii.aspx">Part II</a></li>
                <li><a runat="server" id="tabPartI" href="gprai.aspx">Part I</a></li>
            </ul>
        </div>
        <br />
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Part IV.
                GPRA Performance Measure 4 Data</b> </span>
        <p>
           Part IV of the GPRA Table collects data for the GPRA Performance Measure in which the budget data and number of students served by the MSAP grant are reported. See the <i>GPRA Guide</i> for instructions about reporting these
            data. When you finish entering data for this page, click on Save Record before proceeding.
        </p>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfPerformanceMeasure" runat="server" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <table class="msapDataTbl">
            <tr>
                <th style="color: #F58220; text-align:center;" >
                    Cost per student
                </th>
                <th align="center" class="TDCenterClass" style="color: #F58220;">
                    <asp:Label ID="lblReportPeriod" runat="server"></asp:Label>
                </th>
            </tr>
            <tr>
                <td>
                    49. Total annual MSAP funds expended at this school</td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAnnualBudget" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false" Type="Currency" NumberFormat-DecimalDigits="2"
                        Width="181">
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    50. Total number of students served by this school’s magnet program
                    <img src="../../images/question_mark-thumb.png" title="See the <i>GPRA Guide</i> for detailed instructions." />
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <asp:Label ID="txtTotalSutdents" runat="server" style="text-align:left;" CssClass="msapDataTxt change" Width="178" Height="19" MaxLength="18">
                    </asp:Label>
                   <%-- <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                        TargetControlID="txtTotalSutdents">
                    </ajax:FilteredTextBoxExtender>--%>
                </td>
            </tr>
        </table>
        <div style="text-align: right; margin-top: 20px;">
            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn1 postbutton" Text="Save Record"
                OnClick="OnSave" />
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <a href="manageGPRA.aspx">Back to GPRA table</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                id="LinkI" href="GPRAII.aspx">Part I</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                    id="LinkII" href="GPRAIII.aspx">Part II</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                        id="LinkIII" href="GPRAIV.aspx">Part III</a>&nbsp;&nbsp;&nbsp;&nbsp;
            Part IV&nbsp;&nbsp;&nbsp;&nbsp;<a runat="server" id="LinkV" href="GPRAV.aspx">Part V</a>
            &nbsp;&nbsp;&nbsp;&nbsp;<a runat="server" id="LinkVI" href="GPRAVI.aspx">Part VI</a>
        </div>
    </div>
</asp:Content>
