﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_data_schoolstatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID; 
            
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 28 && x.TypeIndex > -9))
            {
                ddlSchoolStatus.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == granteeID))
            {
                ddlSchool.Items.Add(new ListItem(School.SchoolName, School.ID.ToString()));
            }
            LoadData();
        }
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetSchools
                   from n in db.MagnetSchoolStatuses
                   from l in db.MagnetDLLs
                   where m.ID == n.SchoolID
                   && n.ReportID == Convert.ToInt32(hfReportID.Value)
                   && n.SchoolStatus == l.TypeIndex
                   && l.TypeID == 28
                   select new { n.ID, m.SchoolName, l.TypeName, n.Notes};
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    protected void OnNew(object sender, EventArgs e)
    {
        ddlSchool.SelectedIndex = 0;
        ddlSchoolStatus.SelectedIndex = 0;
        txtNotes.Text = "";
        mpeResourceWindow.Show();
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        string id =  (sender as LinkButton).CommandArgument;
        MagnetSchoolStatus.Delete(x => x.ID == Convert.ToInt32(id));
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetSchoolStatus School = MagnetSchoolStatus.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        ddlSchool.SelectedValue = Convert.ToString(School.SchoolID);
        ddlSchoolStatus.SelectedValue = Convert.ToString(School.SchoolStatus);
        txtNotes.Text = School.Notes;
        mpeResourceWindow.Show();
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        MagnetSchoolStatus Item = new MagnetSchoolStatus();
        if(!string.IsNullOrEmpty(hfID.Value))
            Item = MagnetSchoolStatus.SingleOrDefault(x=>x.ID == Convert.ToInt32(hfID.Value));
        else
            Item.ReportID = Convert.ToInt32(hfReportID.Value);
        Item.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        Item.SchoolStatus = Convert.ToInt32(ddlSchoolStatus.SelectedValue);
        Item.Notes = txtNotes.Text;
        Item.Save();
        LoadData();
    }
}