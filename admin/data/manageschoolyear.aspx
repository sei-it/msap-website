﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageschoolyear.aspx.cs" Inherits="admin_data_manageschoolyear" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="EmbeddedTextBoxDemoControl.ascx" TagName="EmbeddedTextBoxDemoControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Magnet School Year
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

    <script type="text/javascript">
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });

        onload = function () {
            $(".msapDataTxt").onfocus = function () {
                setTimeout(function () {
                    $(".msapDataTxt").setSelectionRange(0, 0);
                }, 0);
            };
        };
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <asp:HiddenField ID="hfControlID" runat="server" ClientIDMode="Static" />
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>Table
            2: Year of Implementation for Existing Magnet Schools</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br /> <div class="titles">
                Table 2: Year of Implementation for Existing Magnet Schools <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;included in the project</div>
        <div class="tab_area">
           
            <ul class="tabs">
                <li><a href="feederenrollment.aspx?id=3">Table 4</a></li>
                <li><a href="enrollment.aspx">Table 3</a></li>
                <li class="tab_active">Table 2</li>
                <li><a href="leaenrollment.aspx">Table 1</a></li>
            </ul>
        </div>
        <br />
        
        <p>
            For each participating school, enter the first school year as a magnet school.</p>
        <asp:Panel ID="contentPanel" runat="server" ClientIDMode="Static">
        </asp:Panel>
        <br />
        <table width="100%">
            <tr>
                <td align='right'>
                    <asp:Button runat="server" ID="btn" OnClick="OnSave" CssClass="surveyBtn1 postbutton" Text="Save Record" />
                    &nbsp;&nbsp;
                    <asp:Button ID="Button2" runat="server" CssClass="surveyBtn postbutton" Text="Review" OnClick="OnPrint" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hfIDs" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a href="leaenrollment.aspx">Table 1 - Enrollment Data-LEA Level</a>&nbsp;&nbsp;&nbsp;&nbsp;
        Table 2 - Year of Implementation&nbsp;&nbsp;&nbsp;&nbsp; <a href="enrollment.aspx">Table
            3 - Enrollment Data-Magnet Schools</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="feederenrollment.aspx?id=3">
                Table 4 - Feeder School</a>
    </div>
</asp:Content>
