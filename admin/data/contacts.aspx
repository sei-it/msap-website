﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="contacts.aspx.cs" Inherits="admin_data_contacts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Contacts
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Manage Contacts</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <div>
        <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" 
                onselectedindexchanged="rbtnlstCohort_SelectedIndexChanged">
    <asp:ListItem Value="1" >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2" Selected="True">MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
        </div>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New Contact" CssClass="msapBtn" OnClick="OnAdd" />
        </p>
        <asp:DropDownList ID="ddlContactType" runat="server" OnSelectedIndexChanged="OnIndexChanged"
            AutoPostBack="true">
            <asp:ListItem Value="1">Project Director</asp:ListItem>
            <asp:ListItem Value="2">Superintendent</asp:ListItem>
            <asp:ListItem Value="3">Evaluator</asp:ListItem>
            <asp:ListItem Value="4">Parent Coordinator</asp:ListItem>
            <asp:ListItem Value="5">Title I Coordinator</asp:ListItem>
            <asp:ListItem Value="6">Principal</asp:ListItem>
			<asp:ListItem Value="7">Grant Manager</asp:ListItem>
        </asp:DropDownList>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="GranteeName" SortExpression="" HeaderText="Grantee Name" />
                <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School Name" />
                <asp:BoundField DataField="ContactFirstName" SortExpression="" HeaderText="First Name" />
                <asp:BoundField DataField="ContactLastName" SortExpression="" HeaderText="Last Name" />
                <asp:BoundField DataField="Address" SortExpression="" HeaderText="Address" />
                <asp:BoundField DataField="City" SortExpression="" HeaderText="City" />
                <asp:BoundField DataField="State" SortExpression="" HeaderText="State" />
                <asp:BoundField DataField="Zipcode" SortExpression="" HeaderText="Zipcode" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hfID" runat="server" />
        <%-- Panel --%>
        <asp:Panel ID="PopupPanel" runat="server" >
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit Contact</div>
                <table>
                    <tr>
                        <td>
                            Grantee:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGrantee" runat="server" CssClass="msapDataTxt" AutoPostBack="false"
                             OnSelectedIndexChanged="OnGranteeChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="schoolRow" runat="server" visible="false">
                        <td>
                            <asp:Label ID="lblOrganization" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="msapDataTxt">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="250" Width="450" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            First Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server" MaxLength="250" Width="450" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="250" Width="450" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server" MaxLength="500" Width="450" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            City:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCity" runat="server" MaxLength="50" Width="450" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            State:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlState" runat="server" CssClass="msapDataTxt">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Zipcode:
                        </td>
                        <td>
                            <asp:TextBox ID="txtZipcode" runat="server" MaxLength="5" Width="50" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender7" runat="server" TargetControlID="txtZipcode"
                                Mask="99999">
                            </ajax:MaskedEditExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTelephone" runat="server" MaxLength="15"  CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtTelephone"
                                Mask="(999)999-9999">
                            </ajax:MaskedEditExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Extension:
                        </td>
                        <td>
                            <asp:TextBox ID="txtExtension" runat="server" MaxLength="15"  CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fax:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFax" runat="server" MaxLength="15" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtFax"
                                Mask="(999)999-9999">
                            </ajax:MaskedEditExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" Width="450" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                   <%-- <tr>
                    <td></td>
                    <td><asp:RequiredFieldValidator ID="rqvalidator" runat="server" ControlToValidate="txtEmail" 
                            ErrorMessage="The Email address is required." SetFocusOnError="true"  />
                            <asp:RegularExpressionValidator
                                ID="regExpValidate" runat="server" SetFocusOnError="true" ControlToValidate="txtEmail"
                                ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" 
                                ErrorMessage="Email is not in proper format." />

                    </td>
                    </tr>--%>
                    <tr>
                        <td>
                            Notes:
                        </td>
                        <td>
                            <cc1:Editor ID="txtNotes" runat="server" Width="470" Height="200" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                        </td>
                        <td>
                            <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
            BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
