﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="enrollmentorg.aspx.cs" Inherits="admin_report_enrollmentorg" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    `</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        <a href="quarterreports.aspx">Home</a> --> Enrollment</h1>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <!-- content start -->
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            <!--
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            -->
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadGrid ID="RadGrid1" GridLines="None" runat="server" AllowAutomaticDeletes="True"
        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="False"
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnItemUpdated="RadGrid1_ItemUpdated"
        OnItemDeleted="RadGrid1_ItemDeleted" OnItemInserted="RadGrid1_ItemInserted" 
        OnItemCreated="RadGrid1_ItemCreated" ShowFooter="True">
        <PagerStyle Mode="NextPrevAndNumeric" />
        <MasterTableView Width="100%" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" EditMode="InPlace"
            DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False">
            <Columns>
                <telerik:GridDropDownColumn DataField="GradeLevel" DataSourceID="SqlDataSource2"
                    HeaderText="Grade" ListTextField="TypeName" ListValueField="TypeIndex" UniqueName="GradeLevel"
                    ColumnEditorID="GridDropDownColumnEditor1">
                </telerik:GridDropDownColumn>
                <telerik:GridNumericColumn DataField="AmericanIndian" HeaderText="American Indian"
                    UniqueName="AmericanIndian" ColumnEditorID="GridNumericColumnEditor2" Aggregate="Sum" >
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="Asian" HeaderText="Asian" UniqueName="Asian"
                    ColumnEditorID="GridNumericColumnEditor3" Aggregate="Sum">
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="AfricanAmerican" HeaderText="African American"
                    UniqueName="AfricanAmerican" ColumnEditorID="GridNumericColumnEditor4" Aggregate="Sum">
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="Hispanic" HeaderText="Hispanic" UniqueName="Hispanic"
                    ColumnEditorID="GridNumericColumnEditor5" Aggregate="Sum">
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="White" HeaderText="White" UniqueName="White"
                    ColumnEditorID="GridNumericColumnEditor6" Aggregate="Sum">
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="Hawaiian" HeaderText="Hawaiian" UniqueName="Hawaiian"
                    ColumnEditorID="GridNumericColumnEditor7" Aggregate="Sum">
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="MultiRacial" HeaderText="Multi Racial" UniqueName="MultiRacial"
                    ColumnEditorID="GridNumericColumnEditor8" Aggregate="Sum">
                </telerik:GridNumericColumn>
                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                    <ItemStyle CssClass="MyImageButton" />
                </telerik:GridEditCommandColumn> 
                <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                    ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                    UniqueName="DeleteColumn">
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings>
            <ClientEvents OnRowClick="RowDblClick" />
        </ClientSettings>
    </telerik:RadGrid>
    <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditor1" runat="server"
        DropDownStyle-Width="50px" />
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor2" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox1" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" " />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor3" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox2" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0"  GroupSeparator="" DecimalSeparator=" " />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor4" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox3" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0"  GroupSeparator="" DecimalSeparator=" " />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor5" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox4" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0"  GroupSeparator="" DecimalSeparator=" " />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor6" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox5" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0"  GroupSeparator="" DecimalSeparator=" " />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor7" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox6" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0"  GroupSeparator="" DecimalSeparator=" " />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:GridNumericColumnEditor ID="GridNumericColumnEditor8" runat="server" NumericTextBox-Width="60px">
        <NumericTextBox ID="NumericTextBox7" DataType="System.Int32" runat="Server">
            <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator="" DecimalSeparator=" "  />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        DeleteCommand="DELETE FROM [MagnetSchoolEnrollments] WHERE [ID] = @ID" InsertCommand="INSERT INTO [MagnetSchoolEnrollments] ([GranteeReportID], [StageID], [GradeLevel], [AmericanIndian], [Asian], [AfricanAmerican], [Hispanic], [White], [Hawaiian], [MultiRacial]) VALUES (@GranteeReportID, @StageID, @GradeLevel, @AmericanIndian, @Asian, @AfricanAmerican, @Hispanic, @White, @Hawaiian, @MultiRacial)"
        SelectCommand="SELECT [id],[GradeLevel], [AmericanIndian], [Asian], [AfricanAmerican], [Hispanic], [White], [Hawaiian], [MultiRacial] FROM [MagnetSchoolEnrollments] where [GranteeReportID]=@GranteeReportID and [StageID]=@StageID order by [GradeLevel]"
        UpdateCommand="UPDATE [MagnetSchoolEnrollments] SET [GradeLevel] = @GradeLevel, [AmericanIndian] = @AmericanIndian, [Asian] = @Asian, [AfricanAmerican] = @AfricanAmerican, [Hispanic] = @Hispanic, [White] = @White, [Hawaiian]=@Hawaiian, [MultiRacial]=@MultiRacial WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:Parameter Name="GranteeReportID" Type="Int32" />
            <asp:Parameter Name="StageID" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="GranteeReportID" Type="Int32" />
            <asp:Parameter Name="StageID" Type="Int32" />
            <asp:Parameter Name="GradeLevel" Type="Int32" />
            <asp:Parameter Name="AmericanIndian" Type="Int32" />
            <asp:Parameter Name="Asian" Type="Int32" />
            <asp:Parameter Name="AfricanAmerican" Type="Int32" />
            <asp:Parameter Name="Hispanic" Type="Int32" />
            <asp:Parameter Name="White" Type="Int32" />
            <asp:Parameter Name="Hawaiian" Type="Int32" />
            <asp:Parameter Name="MultiRacial" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ID" Type="Int32" />
            <asp:Parameter Name="GradeLevel" Type="Int32" />
            <asp:Parameter Name="AmericanIndian" Type="Int32" />
            <asp:Parameter Name="Asian" Type="Int32" />
            <asp:Parameter Name="AfricanAmerican" Type="Int32" />
            <asp:Parameter Name="Hispanic" Type="Int32" />
            <asp:Parameter Name="White" Type="Int32" />
            <asp:Parameter Name="Hawaiian" Type="Int32" />
            <asp:Parameter Name="MultiRacial" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        SelectCommand="SELECT [TypeIndex], [TypeName] FROM [MagnetDLL] where [TypeID]=1">
    </asp:SqlDataSource>
    <!-- content end -->
</asp:Content>
