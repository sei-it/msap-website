﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_adhocreview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //Report type
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 17 && x.TypeIndex > -9))
            {
                ddlGPRAMeasure1.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlGPRAMeasure2.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlGPRAMeasure3.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlGPRAMeasure4.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlGPRAMeasure5.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlGPRAMeasure6.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 24 && x.TypeIndex > -9))
            {
                ddlReportStatus.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlAdHocStatus.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("manageadhocreview.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadData(id);
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetAdHocReview item = new MagnetAdHocReview();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetAdHocReview.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
        }
        item.APRDataSufficientMeasure1 = Convert.ToInt32(ddlGPRAMeasure1.SelectedValue);
        item.Notes1 = txtGPRAMeasure1.Text;
        item.APRDataSufficientMeasure2 = Convert.ToInt32(ddlGPRAMeasure2.SelectedValue);
        item.Notes2 = txtGPRAMeasure2.Text;
        item.APRDataSufficientMeasure3 = Convert.ToInt32(ddlGPRAMeasure3.SelectedValue);
        item.Notes3 = txtGPRAMeasure3.Text;
        item.APRDataSufficientMeasure4 = Convert.ToInt32(ddlGPRAMeasure4.SelectedValue);
        item.Notes4 = txtGPRAMeasure4.Text;
        item.APRDataSufficientMeasure5 = Convert.ToInt32(ddlGPRAMeasure5.SelectedValue);
        item.Notes5 = txtGPRAMeasure5.Text;
        item.APRDataSufficientMeasure6 = Convert.ToInt32(ddlGPRAMeasure6.SelectedValue);
        item.Notes6 = txtGPRAMeasure6.Text;
        item.GeneralNotes = txtGeneralNotes.Text;
        item.ReportStatus = Convert.ToInt32(ddlReportStatus.SelectedValue);
        item.ExpectedDate = txtDateExpected.Text;
        item.Datereceived = txtDateReceived.Text;
        item.Notes = txtDateNotes.Text;
        item.AdHocStatus = Convert.ToInt32(ddlAdHocStatus.SelectedValue);
        item.Save();
        hfID.Value = item.ID.ToString();
    }
    private void LoadData(int SIPID)
    {
        MagnetAdHocReview item = MagnetAdHocReview.SingleOrDefault(x => x.ID == SIPID);
        ddlGPRAMeasure1.SelectedValue = Convert.ToString(item.APRDataSufficientMeasure1);
        txtGPRAMeasure1.Text = item.Notes1;
        ddlGPRAMeasure2.SelectedValue = Convert.ToString(item.APRDataSufficientMeasure2);
        txtGPRAMeasure2.Text = item.Notes2;
        ddlGPRAMeasure3.SelectedValue = Convert.ToString(item.APRDataSufficientMeasure3);
        txtGPRAMeasure3.Text = item.Notes3;
        ddlGPRAMeasure4.SelectedValue = Convert.ToString(item.APRDataSufficientMeasure4);
        txtGPRAMeasure4.Text = item.Notes4;
        ddlGPRAMeasure5.SelectedValue = Convert.ToString(item.APRDataSufficientMeasure5);
        txtGPRAMeasure5.Text = item.Notes5;
        ddlGPRAMeasure6.SelectedValue = Convert.ToString(item.APRDataSufficientMeasure6);
        txtGPRAMeasure6.Text = item.Notes6;
        txtGeneralNotes.Text = item.GeneralNotes;
        ddlReportStatus.SelectedValue = Convert.ToString(item.ReportStatus);
        txtDateExpected.Text = item.ExpectedDate;
        txtDateReceived.Text = item.Datereceived;
        txtDateNotes.Text = item.Notes;
        ddlAdHocStatus.SelectedValue = Convert.ToString(item.AdHocStatus);
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("manageadhocreview.aspx");
    }
}