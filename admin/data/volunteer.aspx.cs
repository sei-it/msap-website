﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.IO;

public partial class admin_data_volunteer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

      
            //Report type
            var data = MagnetDesegregationPlan.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
            if (data.Count > 0)
            {
                hfID.Value = data[0].ID.ToString();
                LoadData();
            }
            else
            {
                MagnetDesegregationPlan Desegregation = new MagnetDesegregationPlan();
                Desegregation.ReportID = Convert.ToInt32(hfReportID.Value);
                Desegregation.Save();
                hfID.Value = Desegregation.ID.ToString();
            }

            PlanCopy.Attributes.Add("onchange", "return validateFileUpload(this);");
            SchoolBoard.Attributes.Add("onchange", "return validateFileUpload(this);");

           // MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlGrantees2.SelectedValue));

        }

    }
    private void LoadData()
    {
        MagnetDesegregationPlan plan = MagnetDesegregationPlan.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        if (plan.PlanType != null)
        {
            //Button1.Visible = true;

            MagnetDBDB db = new MagnetDBDB();
            if (plan.PlanType == true)
            {
                //Voluntory
                //RadioButton2.Checked = true;
                var Files = from m in db.MagnetUploads
                            where m.ProjectID == Convert.ToInt32(hfReportID.Value)
                            && (m.FormID == 10 ||m.FormID == 11)
                            select new { m.ID, FilePath = "<a target='_blank' href='../upload/" + m.PhysicalName + "'>" + m.FileName + "</a>" };
                GridView2.DataSource = Files;
                GridView2.DataBind();

                bool hasUpload = false;
                var mups = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value)
                             && (x.FormID == 10 || x.FormID == 11)).OrderBy(y=>y.FormID);
                
                if(mups.Count()>0)
                {
                    hasUpload = true;
                    if (mups.Count() == 2)
                    {
                        PlanCopy.Enabled = false;
                        SchoolBoard.Enabled = false;
                    } 
                    else
                    {
                        foreach(var mup in mups)
                        {
                            if (mup.FormID == 10)  //copy plan
                            {
                                PlanCopy.Enabled = false;
                                SchoolBoard.Enabled = true;
                            }
                            else
                            {
                                PlanCopy.Enabled = true;
                                SchoolBoard.Enabled = false;
                            }

                        }
                    }
                    
                }
                
                if(!hasUpload)
                {
                    PlanCopy.Enabled = true;
                    SchoolBoard.Enabled = true;
                }
            }

        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        //Delete volunary files
        foreach (MagnetUpload file in MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && (x.FormID == 10 ||x.FormID ==11 )))
        {
            if (System.IO.File.Exists(Server.MapPath("../upload/") + file.PhysicalName))
                System.IO.File.Delete(Server.MapPath("../upload/") + file.PhysicalName);
            file.Delete();
        }

        int muploadId = Convert.ToInt32((sender as LinkButton).CommandArgument);
        var voluntaryRecords = MagnetUpload.Find(m => m.ProjectID == Convert.ToInt32(hfReportID.Value)
                            && (m.FormID == 10 || m.FormID == 11));

        foreach (MagnetUpload mupload in voluntaryRecords)
        {
            MagnetUpload.Delete(x => x.ID == mupload.ID);
        }

        MagnetDesegregationPlan Desegregation = MagnetDesegregationPlan.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        
        int count = MagnetUpload.Find(x =>x.ProjectID == Convert.ToInt32(hfReportID.Value) && (x.FormID == 10 ||x.FormID == 11)).Count;

        if (count == 0)
        {
            Desegregation.isVoluntaryUpload = false;
            Desegregation.Save();
        }

        LoadData();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        string PlanCopyFileName = PlanCopy.FileName, SchoolBoardFileName = SchoolBoard.FileName, alertmsg="";
        int projectid = Convert.ToInt32(hfReportID.Value);

         MagnetUpload mupload = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && (x.FileName == PlanCopyFileName || x.FileName==SchoolBoardFileName));
         MagnetUpload mupPlanCopy = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == PlanCopyFileName );
         MagnetUpload mupSchoolBoard = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == SchoolBoardFileName);

        
        if (mupPlanCopy != null && !string.IsNullOrEmpty(PlanCopyFileName.Trim()))
        {
            alertmsg = "<script>alert('The file name " + PlanCopyFileName + " already exists. Please rename the file.');</script>";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmationVPlan", alertmsg, false);
            LoadData();
            return;
        }
        if (mupSchoolBoard != null && !string.IsNullOrEmpty(SchoolBoardFileName.Trim()))
        {
            alertmsg = "<script>alert('The file name " + SchoolBoardFileName + " already exists. Please rename the file.');</script>";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmationVPlan", alertmsg, false);
            LoadData();
            return;
        }
        if (PlanCopyFileName.Trim() == SchoolBoardFileName.Trim() && !string.IsNullOrEmpty(PlanCopyFileName.Trim()))
        {
            alertmsg = "<script>alert('The file name " + SchoolBoardFileName + " already exists. Please rename the file.');</script>";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmationVPlan", alertmsg, false);
            LoadData();
            return;
        }
        //Allow one uploaded documentation
        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);
        bool bDesegregation = grantee.Desegregation == null ? false : (bool)grantee.Desegregation;

        bool ret = false;
        string msg = "";

        //if (RadioButton2.Checked)
        {
            if (bDesegregation)
            {
                dvCopyschool.Visible = false;
                ltlBothDoc.Visible = false;
                //If approved by ED only one file is required

                if (PlanCopy.HasFile || SchoolBoard.HasFile)
                {
                    ret = false;
                }
                else
                {
                    if (GridView2.Rows.Count <= 0)
                    {
                        ret = true;
                        msg = "Please upload at least one required document!";
                    }
                }
            }
            else
            {
                //By default 2 files are required
                if (PlanCopy.HasFile && SchoolBoard.HasFile)
                {
                    ret = false;
                }
                else
                {
                    if (GridView2.Rows.Count <= 0)
                    {
                        ret = true;
                        msg = "Please upload two required documents!";
                    }
                }
            }
        }

        if (ret)
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('" + msg + "');</script>", false);
        }
        else
        {
            //Delete required files
            foreach(MagnetUpload file in MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == 9))
            {
                if(System.IO.File.Exists(Server.MapPath("../upload/") + file.PhysicalName))
                    System.IO.File.Delete(Server.MapPath("../upload/") + file.PhysicalName);
                file.Delete();
            }

            //if (RadioButton2.Checked)
            {
                MagnetDesegregationPlan Desegregation = MagnetDesegregationPlan.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
                Desegregation.PlanType = true;
                Desegregation.isVoluntaryUpload = true;
                Desegregation.isRequiredUpload = false;
                Desegregation.Save();

                int ReportID = Convert.ToInt32(hfReportID.Value);
                GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);

                MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

                string strReportPeriod = period.ReportPeriod;
                strReportPeriod += Convert.ToBoolean(period.PeriodType) ? "-Ad hoc" : "-APR";
                string strGrantee = grantee.GranteeName.Trim();

                string strFileroot = Server.MapPath("") + "/../upload/";
                string strPath = "";
                string strFileName = PlanCopy.FileName.Replace('#', ' ');
                string strPathandFile = "";

                //if (RadioButton2.Checked)
                {
                    string guid = "";
                    if (PlanCopy.HasFile)
                    {
                        strPath = strReportPeriod + "/" + strGrantee.Replace("#","") + "/Voluntary Plan/";
                        strPathandFile = strPath + strFileName;


                        MagnetUpload upload1 = new MagnetUpload();
                        upload1.ProjectID = Convert.ToInt32(hfReportID.Value);
                        upload1.FormID = 10;  //copy of the plan
                        upload1.FileName = PlanCopy.FileName.Replace("#","");

                        if (!Directory.Exists(strPath))
                        {
                            Directory.CreateDirectory(strFileroot + strPath);
                        }

                        upload1.PhysicalName = strPathandFile;
                        PlanCopy.SaveAs(strFileroot + strPathandFile);
                        upload1.UploadDate = DateTime.Now;
                        upload1.Save();
                    }

                    if (SchoolBoard.HasFile)
                    {
                        strFileName = SchoolBoard.FileName.Replace("#", "");
                        strPath = strReportPeriod + "/" + strGrantee.Replace("#","") + "/Voluntary Plan/";
                        strPathandFile = strPath + strFileName;

                        MagnetUpload upload2 = new MagnetUpload();
                        upload2.ProjectID = Convert.ToInt32(hfReportID.Value);
                        upload2.FormID = 11; 

                        if (!Directory.Exists(strPath))
                        {
                            Directory.CreateDirectory(strFileroot + strPath);
                        }

                        upload2.FileName = strFileName;
                        //guid = System.Guid.NewGuid().ToString() + "-" + SchoolBoard.FileName.Replace('#', ' ');
                        upload2.PhysicalName = strPathandFile;
                        SchoolBoard.SaveAs(strFileroot + strPathandFile);
                        upload2.UploadDate = DateTime.Now;
                        upload2.Save();
                    }
                }

                LoadData();

                var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 5);
                if (muhs.Count > 0)
                {
                    muhs[0].UpdateUser = Context.User.Identity.Name;
                    muhs[0].TimeStamp = DateTime.Now;
                    muhs[0].Save();
                }
                else
                {
                    MagnetUpdateHistory muh = new MagnetUpdateHistory();
                    muh.ReportID = Convert.ToInt32(hfReportID.Value);
                    muh.FormIndex = 5;
                    muh.UpdateUser = Context.User.Identity.Name;
                    muh.TimeStamp = DateTime.Now;
                    muh.Save();
                }

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
            }
        }
    }
}