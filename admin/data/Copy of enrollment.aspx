﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="Copy of enrollment.aspx.cs" Inherits="admin_report_enrollment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP center - Enrollment data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Table 9: <asp:Label ID="lblFormTitle" runat="server"></asp:Label></h4>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfSchoolID" runat="server" />
    <asp:HiddenField ID="hfStageID" runat="server" />
        <br/>
        <div class="tab_area">
            <div class="titles">Table 9 - Enrollment Data-Magnet Schools</div>
            <ul class="tabs">
                <li><a href="feederenrollment.aspx?id=3">Table 11</a></li>
                <li class="tab_active">Table 9</li>
                <li><a href="manageschoolyear.aspx">Table 8</a></li>
                <li><a href="leaenrollment.aspx">Table 7</a></li>
            </ul>
        </div>
        <br/>    
    <p>
        Provide data for all students in each grade for which the school enrolls students.</p>
    <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AllowSorting="false"
        GridLines="None" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl CenterContent">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                    School Name
                </HeaderTemplate>
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Eval("SchoolName")%></font>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle CssClass="TDWithBottomNoRightBorder" />
                <ItemTemplate>
                    <a href="enrollmentschooldata.aspx?schoolid=<%# Eval("ID") %>">Add Edit School Data</a>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <p align="right">
        <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" Text="Print" OnClick="OnPrint" /></p>
    <div style="text-align: right; margin-top: 20px;">
        <a href="leaenrollment.aspx">Table 7 - Enrollment Data-LEA Level</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="manageschoolyear.aspx">Table 8 - Year of Implementation</a>&nbsp;&nbsp;&nbsp;&nbsp;
        Table 9 - Enrollment Data-Magnet Schools&nbsp;&nbsp;&nbsp;&nbsp; <a href="feederenrollment.aspx?id=3">
            Table 11 - Feeder School</a>
    </div>
</asp:Content>
