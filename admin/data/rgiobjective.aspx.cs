﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_rgiobjective : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //School
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == granteeID).OrderBy(x => x.SchoolName))
            {
                ddlSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
            }

            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 11 && x.TypeIndex > -9))
            {
                ddlRGIObjective.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 12 && x.TypeIndex > -9))
            {
                ddlRacialGroup1.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlRacialGroup2.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlRacialGroup3.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlRacialGroup4.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 13 && x.TypeIndex > -9))
            {
                ddlTargetedGroup1.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 14 && x.TypeIndex > -9))
            {
                ddlTargetedGroup2.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 15 && x.TypeIndex > -9))
            {
                ddlTargetedGroup3.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 16 && x.TypeIndex > -9))
            {
                ddlTargetedGroup4.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 17 && x.TypeIndex > -9))
            {
                ddlMeetObjective.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlIncresedTargetedGroup1.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlMetGPRAMeasure.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 18 && x.TypeIndex > -9))
            {
                ddlIncresedTargetedGroup2.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 19 && x.TypeIndex > -9))
            {
                ddlIncresedTargetedGroup3.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 20 && x.TypeIndex > -9))
            {
                ddlIncresedTargetedGroup4.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("managergiobj.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadData(id);
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetRGIObjective item = new MagnetRGIObjective();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetRGIObjective.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
            item.RGIStage = 0;
        }
        item.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        item.RGIObjective = Convert.ToInt32(ddlRGIObjective.SelectedValue);
        item.RaciallyIsolatedGroup = Convert.ToInt32(ddlRacialGroup1.SelectedValue);
        item.RaciallyIsolatedGroup1 = Convert.ToInt32(ddlRacialGroup2.SelectedValue);
        item.RaciallyIsolatedGroup2 = Convert.ToInt32(ddlRacialGroup3.SelectedValue);
        item.RaciallyIsolatedGroup3 = Convert.ToInt32(ddlRacialGroup4.SelectedValue);
        item.TargetedRacialGroup1 = Convert.ToInt32(ddlTargetedGroup1.SelectedValue);
        item.TargetedRacialGroup2 = Convert.ToInt32(ddlTargetedGroup2.SelectedValue);
        item.TargetedRacialGroup3 = Convert.ToInt32(ddlTargetedGroup3.SelectedValue);
        item.TargetedRacialGroup4 = Convert.ToInt32(ddlTargetedGroup4.SelectedValue);
        item.MetObjective = Convert.ToInt32(ddlMeetObjective.SelectedValue);
        item.IncreasedTargetedRacialGroup1 = Convert.ToInt32(ddlIncresedTargetedGroup1.SelectedValue);
        item.IncreasedTargetedRacialGroup2 = Convert.ToInt32(ddlIncresedTargetedGroup2.SelectedValue);
        item.IncreasedTargetedRacialGroup3 = Convert.ToInt32(ddlIncresedTargetedGroup3.SelectedValue);
        item.IncreasedTargetedRacialGroup4 = Convert.ToInt32(ddlIncresedTargetedGroup4.SelectedValue);
        item.MetGPRAMeasure = Convert.ToInt32(ddlMetGPRAMeasure.SelectedValue);
        item.Save();
        hfID.Value = item.ID.ToString();
    }
    private void LoadData(int SIPID)
    {
        MagnetRGIObjective item = MagnetRGIObjective.SingleOrDefault(x => x.ID == SIPID);
        ddlSchool.SelectedValue = item.SchoolID.ToString();
        ddlRGIObjective.SelectedValue = Convert.ToString(item.RGIObjective);
        ddlRacialGroup1.SelectedValue = Convert.ToString(item.RaciallyIsolatedGroup);
        ddlRacialGroup2.SelectedValue = Convert.ToString(item.RaciallyIsolatedGroup1);
        ddlRacialGroup3.SelectedValue = Convert.ToString(item.RaciallyIsolatedGroup2);
        ddlRacialGroup4.SelectedValue = Convert.ToString(item.RaciallyIsolatedGroup3);
        ddlTargetedGroup1.SelectedValue = Convert.ToString(item.TargetedRacialGroup1);
        ddlTargetedGroup2.SelectedValue = Convert.ToString(item.TargetedRacialGroup2);
        ddlTargetedGroup3.SelectedValue = Convert.ToString(item.TargetedRacialGroup3);
        ddlTargetedGroup4.SelectedValue = Convert.ToString(item.TargetedRacialGroup4);
        ddlMeetObjective.SelectedValue = Convert.ToString(item.MetObjective);
        ddlIncresedTargetedGroup1.SelectedValue = Convert.ToString(item.IncreasedTargetedRacialGroup1);
        ddlIncresedTargetedGroup2.SelectedValue = Convert.ToString(item.IncreasedTargetedRacialGroup2);
        ddlIncresedTargetedGroup3.SelectedValue = Convert.ToString(item.IncreasedTargetedRacialGroup3);
        ddlIncresedTargetedGroup4.SelectedValue = Convert.ToString(item.IncreasedTargetedRacialGroup4);
        ddlMetGPRAMeasure.SelectedValue = Convert.ToString(item.MetGPRAMeasure);
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("managergiobj.aspx");
    }
}