﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_data_budgetform : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            LoadData();

            if (GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).ReportPeriodID % 2 == 0)
            {
                ChangeRow.Visible = true;
                ChangeRowExplanation.Visible = true;
            }

            //FileUpload1.Attributes.Add("onchange", "return validateFileUpload(this);");
            //UploadBtn.Attributes.Add("onclick", "return validateFileUpload(document.getElementById('" + FileUpload1.ClientID + "'));");
            foreach(System.Web.UI.WebControls.ListItem li in rblRageAggrement.Items)
                li.Attributes.Add("onClick", "Javascript:ContentIsDirty();");
            foreach (System.Web.UI.WebControls.ListItem li in rblAgency.Items)
                li.Attributes.Add("onClick", "Javascript:ContentIsDirty();");
            foreach (System.Web.UI.WebControls.ListItem li in rblBudgetChange.Items)
                li.Attributes.Add("onClick", "Javascript:ContentIsDirty();");
            rbIncludedInAgreement.Attributes.Add("onClick", "Javascript:ContentIsDirty();");
            rb34CFR.Attributes.Add("onClick", "Javascript:ContentIsDirty();");
            txtAgreementFrom.Attributes.Add("onClick", "Javascript:ContentIsDirty();");
            txtAgreementTo.Attributes.Add("onClick", "Javascript:ContentIsDirty();");
            rbtnNA.Attributes.Add("onClick", "Javascript:ContentIsDirty();");

            txtAgreementFrom.Attributes.Add("onchange", "Javascript:ContentIsDirty();");
            txtAgreementTo.Attributes.Add("onchange", "Javascript:ContentIsDirty();");
        }
    }
    private void LoadData()
    {
        var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));
        if (data.Count > 0)
        {
            hfID.Value = data[0].ID.ToString();
            txtPersonnel.Text = Convert.ToString(data[0].Personnel);
            txtPersonnelNF.Text = data[0].PersonnelNF==0 ? "" : Convert.ToString(data[0].PersonnelNF);
            txtFringe.Text = Convert.ToString(data[0].Fringe);
            txtFringeNF.Text = data[0].FringeNF==0 ? "" : Convert.ToString(data[0].FringeNF);
            txtTravel.Text = Convert.ToString(data[0].Travel);
            txtTravelNF.Text = data[0].TravelNF==0?"":Convert.ToString(data[0].TravelNF);
            txtEquipment.Text = Convert.ToString(data[0].Equipment);
            txtEquipmentNF.Text = data[0].EquipmentNF==0?"":Convert.ToString(data[0].EquipmentNF);
            txtSupplies.Text = Convert.ToString(data[0].Supplies);
            txtSuppliesNF.Text = data[0].SuppliesNF==0?"": Convert.ToString(data[0].SuppliesNF);
            txtContractual.Text = Convert.ToString(data[0].Contractual);
            txtContractualNF.Text = data[0].ContractualNF==0?"":Convert.ToString(data[0].ContractualNF);
            txtConstruction.Text = Convert.ToString(data[0].Construction);
            txtConstructionNF.Text = data[0].ConstructionNF==0?"":Convert.ToString(data[0].ConstructionNF);
            txtOther.Text = Convert.ToString(data[0].Other);
            txtOtherNF.Text = data[0].OtherNF==0?"":Convert.ToString(data[0].OtherNF);
            txtIndirectCosts.Text = Convert.ToString(data[0].IndirectCost);
            txtIndirectCostsNF.Text = data[0].IndirectCostNF==0?"":Convert.ToString(data[0].IndirectCostNF);
            txtTrainingStipends.Text = Convert.ToString(data[0].TrainingStipends);
            txtTrainingStipendsNF.Text = data[0].TrainingStipendsNF==0?"":Convert.ToString(data[0].TrainingStipendsNF);
            decimal Total = 0;
            Total = (decimal)((data[0].Personnel == null ? 0 : data[0].Personnel) +
                (data[0].Fringe == null ? 0 : data[0].Fringe) +
                (data[0].Travel == null ? 0 : data[0].Travel) +
                (data[0].Equipment == null ? 0 : data[0].Equipment) +
                (data[0].Supplies == null ? 0 : data[0].Supplies) +
                (data[0].Contractual == null ? 0 : data[0].Contractual) +
                (data[0].Construction == null ? 0 : data[0].Construction) +
                (data[0].Other == null ? 0 : data[0].Other));
            decimal TotalNF = 0;
            TotalNF = (decimal)((data[0].PersonnelNF == null ? 0 : data[0].PersonnelNF) +
                (data[0].FringeNF == null ? 0 : data[0].FringeNF) +
                (data[0].TravelNF == null ? 0 : data[0].TravelNF) +
                (data[0].EquipmentNF == null ? 0 : data[0].EquipmentNF) +
                (data[0].SuppliesNF == null ? 0 : data[0].SuppliesNF) +
                (data[0].ContractualNF == null ? 0 : data[0].ContractualNF) +
                (data[0].ConstructionNF == null ? 0 : data[0].ConstructionNF) +
                (data[0].OtherNF == null ? 0 : data[0].OtherNF));
            txtDirectCosts.Text = "$" + Total.ToString("#,#.00#"); //Convert.ToString(Total);
            txtDirectCostsNF.Text = TotalNF==0 ? "" : "$" + TotalNF.ToString("#,#.00#");
            Total += (data[0].IndirectCost == null ? 0 : (decimal)data[0].IndirectCost) +
                (data[0].TrainingStipends == null ? 0 : (decimal)data[0].TrainingStipends);
            TotalNF += (data[0].IndirectCostNF == null ? 0 : (decimal)data[0].IndirectCostNF) +
                (data[0].TrainingStipendsNF == null ? 0 : (decimal)data[0].TrainingStipendsNF);
            txtTotalCosts.Text = "$" + Total.ToString("#,#.00#");
            txtTotalCostsNF.Text = TotalNF==0 ? "" : "$" + TotalNF.ToString("#,#.00#");

            if (data[0].IndirectCostAgreement != null)
            {
                if ((bool)data[0].IndirectCostAgreement)
                {
                    rblRageAggrement.SelectedIndex = 0;
                    if (!string.IsNullOrEmpty(data[0].AgreementFrom))
                        //txtAgreementFrom.SelectedDate = Convert.ToDateTime(data[0].AgreementFrom);
                        txtAgreementFrom.Text = data[0].AgreementFrom;
                    if (!string.IsNullOrEmpty(data[0].AgreementTo))
                        //txtAgreementTo.SelectedDate = Convert.ToDateTime(data[0].AgreementTo);
                        txtAgreementTo.Text = data[0].AgreementTo;
                    if (data[0].ApprovalAgency != null)
                    {
                        if ((bool)data[0].ApprovalAgency)
                        {
                            rblAgency.SelectedIndex = 0;
                            agencyPanel.Visible = false;
                        }
                        else
                        {
                            rblAgency.SelectedIndex = 1;
                            agencyPanel.Visible = true;
                            txtOtherAgency.Text = data[0].OtherAgency;
                        }
                    }
                    else
                        agencyPanel.Visible = false;
                    txtRate.Text = Convert.ToString(data[0].IndirectCostRate);
                }
                else
                {
                    SetAgreement(false);
                    rblRageAggrement.SelectedIndex = 1;
                    txtAgreementFrom.Enabled = false;
                    txtAgreementTo.Enabled = false;
                    rblAgency.ClearSelection();
                    txtOtherAgency.Enabled = false;
                    txtRate.Enabled = false;
                }
            }
            if (data[0].RestrictedRateProgram != null)
            {
                if ((bool)data[0].RestrictedRateProgram)
                    rbIncludedInAgreement.Checked = true;
                else
                    rb34CFR.Checked = true;
            }
            if (data[0].na != null)
                rbtnNA.Checked = Convert.ToBoolean(data[0].na);

            if (data[0].BudgetChange != null)
            {
                if (data[0].BudgetChange == true)
                {
                    rblBudgetChange.SelectedIndex = 1;
                    txtBudgetReason.Text = data[0].BudgetReason;
                }
                else
                {
                    rblBudgetChange.SelectedIndex = 0;
                    txtBudgetReason.Enabled = false;
                }
            }
            txtRICRate.Text = Convert.ToString(data[0].RestrictedRate);
            //txtBudgetSummary.Text = data[0].BudgetSummary;
        }

        //Uploaded file

        /*MagnetDBDB db = new MagnetDBDB();
        var files = from n in db.MagnetUploads
                   where n.ProjectID == Convert.ToInt32(hfReportID.Value)
                   && n.FormID == 13
                   select new
                   {
                       n.ID,
                       FileNames = "<a target='_blank' href='../upload/" + n.PhysicalName + "'>" + n.FileName + "</a>"
                   };
        GridView1.DataSource = files;
        GridView1.DataBind();*/

        //var files = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value)
        //           && x.FormID == 13); //13: budget narrative
        //if (files.Count > 0)
        //{
        //    //FileTableRow.Visible = true;

        //    ////First file
        //    //FileRow1.Visible = true;
        //    //UploadedFile1.HRef = "../upload/" + files[0].PhysicalName;
        //    //lblFile1.Text = files[0].FileName;
        //    HiddenField1.Value = Convert.ToString(files[0].ID);
        //}
        //else
        //{
        //    FileRow1.Visible = false;
        //}
        //if (files.Count > 1)
        //{
        //    //Second file
        //    FileRow2.Visible = true;
        //    UploadedFile2.HRef = "../upload/" + files[1].PhysicalName;
        //    lblFile2.Text = files[1].FileName;
        //    HiddenField2.Value = Convert.ToString(files[1].ID);
        //}
        //else
        //{
        //    FileRow2.Visible = false;
        //}

        //if (files.Count() > 1)
        //{
        //    FileUpload1.Enabled = false;
        //    UploadBtn.Enabled = false;
        //}
        //else
        //{
        //    UploadBtn.Enabled = true;
        //    FileUpload1.Enabled = true;
        //}
    }
    protected void OnSave(object sender, EventArgs e)
    {
        SaveData();
        LoadData();
        hfSaved.Value = "1";
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    private void SaveData()
    {
        MagnetBudgetInformation Budget = new MagnetBudgetInformation();
        if (!string.IsNullOrEmpty(hfID.Value))
            Budget = MagnetBudgetInformation.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            Budget.GranteeReportID = Convert.ToInt32(hfReportID.Value);
        }
        Budget.Personnel = string.IsNullOrEmpty(txtPersonnel.Text) ? (decimal?)null : Convert.ToDecimal(txtPersonnel.Text);
        Budget.PersonnelNF = string.IsNullOrEmpty(txtPersonnelNF.Text) ? (decimal?)null : Convert.ToDecimal(txtPersonnelNF.Text);
        Budget.Fringe = string.IsNullOrEmpty(txtFringe.Text) ? (decimal?)null : Convert.ToDecimal(txtFringe.Text);
        Budget.FringeNF = string.IsNullOrEmpty(txtFringeNF.Text) ? (decimal?)null : Convert.ToDecimal(txtFringeNF.Text);
        Budget.Travel = string.IsNullOrEmpty(txtTravel.Text) ? (decimal?)null : Convert.ToDecimal(txtTravel.Text);
        Budget.TravelNF = string.IsNullOrEmpty(txtTravelNF.Text) ? (decimal?)null : Convert.ToDecimal(txtTravelNF.Text);
        Budget.Equipment = string.IsNullOrEmpty(txtEquipment.Text) ? (decimal?)null : Convert.ToDecimal(txtEquipment.Text);
        Budget.EquipmentNF = string.IsNullOrEmpty(txtEquipmentNF.Text) ? (decimal?)null : Convert.ToDecimal(txtEquipmentNF.Text);
        Budget.Supplies = string.IsNullOrEmpty(txtSupplies.Text) ? (decimal?)null : Convert.ToDecimal(txtSupplies.Text);
        Budget.SuppliesNF = string.IsNullOrEmpty(txtSuppliesNF.Text) ? (decimal?)null : Convert.ToDecimal(txtSuppliesNF.Text);
        Budget.Contractual = string.IsNullOrEmpty(txtContractual.Text) ? (decimal?)null : Convert.ToDecimal(txtContractual.Text);
        Budget.ContractualNF = string.IsNullOrEmpty(txtContractualNF.Text) ? (decimal?)null : Convert.ToDecimal(txtContractualNF.Text);
        Budget.Construction = string.IsNullOrEmpty(txtConstruction.Text) ? (decimal?)null : Convert.ToDecimal(txtConstruction.Text);
        Budget.ConstructionNF = string.IsNullOrEmpty(txtConstructionNF.Text) ? (decimal?)null : Convert.ToDecimal(txtConstructionNF.Text);
        Budget.Other = string.IsNullOrEmpty(txtOther.Text) ? (decimal?)null : Convert.ToDecimal(txtOther.Text);
        Budget.OtherNF = string.IsNullOrEmpty(txtOtherNF.Text) ? (decimal?)null : Convert.ToDecimal(txtOtherNF.Text);
        decimal dFederal = (decimal)(Budget.Personnel == null ? 0 : Budget.Personnel
            + Budget.Fringe == null ? 0 : Budget.Fringe + Budget.Travel == null ? 0 : Budget.Travel
            + Budget.Equipment == null ? 0 : Budget.Equipment + Budget.Supplies == null ? 0 : Budget.Supplies
            + Budget.Contractual == null ? 0 : Budget.Contractual + Budget.Construction + Budget.Other == null ? 0 : Budget.Other);

        decimal dFederalNF = (decimal)(Budget.PersonnelNF == null ? 0 : Budget.PersonnelNF + Budget.FringeNF == null ? 0 : Budget.FringeNF
            + Budget.TravelNF == null ? 0 : Budget.TravelNF + Budget.EquipmentNF == null ? 0 : Budget.EquipmentNF +
            Budget.SuppliesNF == null ? 0 : Budget.SuppliesNF + Budget.ContractualNF == null ? 0 : Budget.ContractualNF
            + Budget.ConstructionNF == null ? 0 : Budget.ConstructionNF + Budget.OtherNF == null ? 0 : Budget.OtherNF);

        txtDirectCosts.Text = dFederal > 0 ? Convert.ToString(dFederal) : "";
        txtDirectCostsNF.Text = dFederalNF == 0 ? "" : Convert.ToString(dFederalNF);
        Budget.IndirectCost = string.IsNullOrEmpty(txtIndirectCosts.Text) ? (decimal?)null : Convert.ToDecimal(txtIndirectCosts.Text);
        Budget.IndirectCostNF = string.IsNullOrEmpty(txtIndirectCostsNF.Text) ? (decimal?)null : Convert.ToDecimal(txtIndirectCostsNF.Text);
        Budget.TrainingStipends = string.IsNullOrEmpty(txtTrainingStipends.Text) ? (decimal?)null : Convert.ToDecimal(txtTrainingStipends.Text);
        Budget.TrainingStipendsNF = string.IsNullOrEmpty(txtTrainingStipendsNF.Text) ? (decimal?)null : Convert.ToDecimal(txtTrainingStipendsNF.Text);
        dFederal += (decimal)(Budget.IndirectCost == null ? 0 : Budget.IndirectCost + Budget.TrainingStipends == null ? 0 : Budget.TrainingStipends);
        dFederalNF += (decimal)(Budget.IndirectCostNF == null ? 0 : Budget.IndirectCostNF + Budget.TrainingStipendsNF == null ? 0 : Budget.TrainingStipendsNF);
        txtTotalCosts.Text =dFederal>0? Convert.ToString(dFederal):"";


        txtTotalCostsNF.Text = dFederalNF == 0 ? "" : Convert.ToString(dFederalNF);
        if(rblRageAggrement.SelectedIndex == 0)
        {
            //yes
            Budget.IndirectCostAgreement = true;
            Budget.AgreementFrom = txtAgreementFrom.Text;
            Budget.AgreementTo = txtAgreementTo.Text;
            if (rblAgency.SelectedIndex == 0)
            {
                Budget.ApprovalAgency = true;
            }
            else if (rblAgency.SelectedIndex == 1)
            {
                Budget.ApprovalAgency = false;
                Budget.OtherAgency = txtOtherAgency.Text;
            }
            else
            {
                Budget.ApprovalAgency = null;
            }
            if (!string.IsNullOrEmpty(txtRate.Text))
                Budget.IndirectCostRate = Convert.ToDecimal(txtRate.Text);
            else
                Budget.IndirectCostRate = null;
        }
        else
        {
            Budget.IndirectCostAgreement = false;
        }
        if (rbIncludedInAgreement.Checked) Budget.RestrictedRateProgram = true;
        if (rb34CFR.Checked) Budget.RestrictedRateProgram = false;
        if (!string.IsNullOrEmpty(txtRICRate.Text))
            Budget.RestrictedRate = Convert.ToDecimal(txtRICRate.Text);
        else
            Budget.RestrictedRate = null;

        if (!string.IsNullOrEmpty(rblBudgetChange.SelectedValue))
        {
            if (rblBudgetChange.SelectedIndex == 0)
            {
                Budget.BudgetChange = false;
            }
            else
            {
                Budget.BudgetChange = true;
                Budget.BudgetReason = txtBudgetReason.Text;
            }
        }
        Budget.na = rbtnNA.Checked;

        //Budget.BudgetSummary = txtBudgetSummary.Text;
        Budget.Save();
        if (string.IsNullOrEmpty(hfID.Value))
            hfID.Value = Budget.ID.ToString();

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 4);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 4;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
    }
    protected void OnBudgeChanged(object sender, EventArgs e)
    {
        if (rblBudgetChange.SelectedIndex == 0)
        {
            txtBudgetReason.Enabled = false;
            txtBudgetReason.Text = "";
        }
        else
            txtBudgetReason.Enabled = true;
    }
    protected void OnAgencyChanged(object sender, EventArgs e)
    {
        if (rblAgency.SelectedIndex == 0)
            agencyPanel.Visible = false;
        else
            agencyPanel.Visible = true;
    }
    protected void OnAgreementYes(object sender, EventArgs e)
    {
        if(rblRageAggrement.SelectedIndex == 0)
            SetAgreement(true);
        else
            SetAgreement(false);
    }
    private void SetAgreement(bool value)
    {
        txtAgreementFrom.Enabled = value;
        if (!value) txtAgreementFrom.Text = "";//txtAgreementFrom.Clear();
        txtAgreementTo.Enabled = value;
        if (!value) txtAgreementTo.Text = ""; //txtAgreementTo.Clear();
        rblAgency.Enabled = value;
        if (!value) rblAgency.ClearSelection();
        txtOtherAgency.Enabled = value;
        if (!value) txtOtherAgency.Text = "";
        txtRate.Enabled = value;
        //if (!string.IsNullOrEmpty(rblAgency.SelectedValue) && rblAgency.SelectedValue.Equals("0"))
        //    agencyPanel.Visible = true;
        //else
            agencyPanel.Visible = false;
        if (!value) txtRate.Text = "";
        //RequiredFieldValidator2.Enabled = value;
        //RequiredFieldValidator3.Enabled = value;
        //RequiredFieldValidator4.Enabled = value;
        //RequiredFieldValidator5.Enabled = value;
    }
   
  
}