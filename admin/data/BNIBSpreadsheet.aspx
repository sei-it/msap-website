﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="BNIBSpreadsheet.aspx.cs" Inherits="admin_data_BNIBSpreadsheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Budget Summary
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
     <style type="text/css">
        .msapDataTxtSmall
        {
            width: 150px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
       <script type="text/javascript">
           $(function () {
               $("[title]").mbTooltip({
                   opacity: .97,       //opacity
                   wait: 800,           //before show
                   cssClass: "default",  // default = default
                   timePerWord: 70,      //time to show in milliseconds per word
                   hasArrow: true, 		// if you whant a little arrow on the corner
                   hasShadow: true,
                   imgPath: "../../css/images/",
                   anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                   shadowColor: "black", //the color of the shadow
                   mb_fade: 200 //the time to fade-in
               });
           });

        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
        function checkWordLen(obj, wordLen, cid) {
            var len = obj.value.split(/[\s]+/);
            $("#" + cid).val(len.length.toString());
            if (len.length > wordLen) {
                alert("You've exceeded the " + wordLen + " word limit for this field!");
                obj.value = obj.SavedValue;
                //obj.value = obj.value.substring(0, wordLen - 1);
                len = obj.value.split(/[\s]+/);
                $("#" + cid).val(len.length.toString());
                return false;
            } else {
                obj.SavedValue = obj.value;
            }
            return true;
        }
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();
            
            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {

                if (obj.id == "ctl00_ContentPlaceHolder1_uploadNarrative") {
                    var who3 = document.getElementsByName('<%= uploadNarrative.UniqueID %>')[0];
                    who3.value = "";

                    var who4 = who3.cloneNode(false);
                    who4.onchange = who3.onchange;
                    who3.parentNode.replaceChild(who4, who3);
                }

                else {
                    var who5 = document.getElementsByName('<%= uploadItemize.UniqueID %>')[0];
                    who5.value = "";

                    var who6 = who5.cloneNode(false);
                    who6.onchange = who5.onchange;
                    who5.parentNode.replaceChild(who6, who5);
                }

                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }

        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Budget Summary</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfFileID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <asp:HiddenField ID="hfControlID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenField2" runat="server" Value="0" />
    <div class="titles_noTabs">
            </div>
        
         <div class="tab_area">
        <div class="titles">
            Budget Summary - Page 2 of 2</div>
        <ul class="tabs">
            <li class="tab_active">Page 2</li>
            <li ><a href="budgetform.aspx">Page 1</a></li>
        </ul>
    </div>
    <br />

    <p class="clear">
    This form applies to individual U.S. Department of Education (ED) discretionary grant programs. Pay attention to specific instructions in the <i>Dear Colleague Letter.</i> You may access the General Administrative Regulations, 34 CFR 74 - 86 and 97-99, on ED's website at: <a href="http://www.ed.gov/policy/fund/reg/edgarReg/edgar.html"
                target="_blank">http://www.ed.gov/policy/fund/reg/edgarReg/edgar.html</a>. <img
                src="../../images/question_mark-thumb.png" title="If you are required to provide or volunteer to provide cost-sharing or matching funds or other non-Federal resources to the project, these should be shown for each applicable budget category on items 1-11 of Non-Federal Funds." class="Q_mark" />
    </p>
   

    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Budget Narrative</b> </span>
       <!-- <p style="color: #4e8396;">
        	Please upload your budget narrative or type in the space provided.
        </p>-->
    <div class="MAPS_Qs">
        <p>
        Provide a narrative summary of your budget for the current budget reporting period (October 1, 2014, to April 15, 2015). Provide any additional information related to the categories in the Budget Summary (items 1-12 above), and also respond to these guiding questions and prompts:
         </p>
        <strong>Explanation of Carryover Funds:</strong>
         <ul>
            <li>Do you expect to have any unexpended funds at the end of the current budget period (this means September 30, 2015)?</li>
            <li>If you do, explain why, provide an estimate of how much, and indicate how you plan to use the unexpended funds (carryover) in the next budget period.</li>
         </ul>
         <strong>Also respond to the following (if applicable):</strong>
         <ul>
             <li>Provide an explanation if you did not expend funds at the expected rate during the reporting period.</li>
             <li>Provide an explanation if funds have not been drawn down from the G5 system to pay for the budget expenditure amounts reported in items 8a.-8c of the ED 524B Cover Sheet.</li>
             <li>Describe any significant changes to your budget resulting from modification of project activities.</li>
             <li>Describe any changes to your budget that affected your ability to achieve your approved project activities and/or project objectives.</li>
         </ul>
	</div>
    <table width="100%">
        <tr id="FileTableRow1" runat="server" visible="false">
            <td>
                <table class="msapDataTbl" style="width: 300px;">
                    <tr>
                        <th colspan="2">
                            Uploaded File
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <a id="UploadedFile1" target="_blank" runat="server">
                                <asp:Label ID="lblFile1" runat="server"></asp:Label></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="btnDeleteNarrative" runat="server" CssClass="postbutton" Text="Delete" OnClick="OnDelete1" 
                            OnClientClick="return confirm('Are you sure you want to delete this uploaded file?');" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtBudgetSummary" runat="server" Font-Size="10" Columns="97"
                    Rows="10" TextMode="MultiLine" CssClass="msapDataTxt change" onkeyup="checkWordLen(this, 1000, 'summaryLength');"></asp:TextBox>
                <!---->
                <input type="text"  disabled="disabled" size="4" id="summaryLength" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:FileUpload ID="uploadNarrative" runat="server" CssClass="msapDataTxt" />&nbsp;&nbsp;
                <asp:Button ID="UploadBtn" CssClass="surveyBtn postbutton" runat="server" Text="Upload"
                    visible="false" />
            </td>
        </tr>
       
    </table>
    <hr style="color:Red;"/>
    <br />
     <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Itemized Budget Spreadsheet</b> </span>
    <p style="color: #4e8396;">
    <asp:Literal ID="ltlItemiedDes" runat="server"></asp:Literal>
    </p>
    <table width="100%">
        <tr id="FileTableRow2" runat="server" visible="false">
            <td>
                <table class="msapDataTbl" style="width: 300px;">
                    <tr >
                        <th colspan="2">
                            Uploaded File
                        </th>
                    </tr>
                    <tr >
                        <td>
                            <a id="UploadedFile2" target="_blank" runat="server">
                                <asp:Label ID="lblFile2" runat="server"></asp:Label></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="FileButton2" runat="server" CssClass="postbutton" Text="Delete" OnClick="OnDelete2"
                            OnClientClick="return confirm('Are you sure you want to delete this uploaded file?');" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtItemize" runat="server" Font-Size="10" Columns="97"
                    Rows="10" TextMode="MultiLine" CssClass="msapDataTxt change" onkeyup="checkWordLen(this, 1000, 'txtItemizeLen');"></asp:TextBox>
                <!---->
                <input type="text" disabled="disabled" size="4" id="txtItemizeLen" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:FileUpload ID="uploadItemize" runat="server" CssClass="msapDataTxt" />&nbsp;&nbsp;
                <asp:Button ID="btnItemizeUpload" CssClass="surveyBtn postbutton" 
                    runat="server" Text="Upload" Visible="false" />
            </td>
        </tr>
       
    </table>
    <hr style="color:Red;"/>
    <br />
    <table width="100%">
     <tr>
            <td style="text-align: right; padding-top: 20px;">
                <asp:Button ID="btnSaveRecord" CssClass="surveyBtn2 postbutton" runat="server" Text="Save & Upload"
                  OnClick="OnSave"  />
                   &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn1 postbutton" OnClick="OnPrint"
                Text="Review" />
            </td>
        </tr>
    </table>
     <div style="text-align: right; margin-top: 20px;">
           <a href="budgetform.aspx">Page 1</a>&nbsp;&nbsp;&nbsp;&nbsp;
           Page 2&nbsp;&nbsp;&nbsp;&nbsp;
     </div>
</asp:Content>

