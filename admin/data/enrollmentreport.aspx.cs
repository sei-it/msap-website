﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_enrollmentreport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //Report type
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 25 && x.TypeIndex > -9))
            {
                ddlReportType.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("manageenrollment.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadData(id);
                }
            }
        }
    }
    protected void OnTextChanged(object sender, EventArgs e)
    {
        int Total = 0;
        Total += string.IsNullOrEmpty(txtAfricanAmerican.Text) ? 0 : Convert.ToInt32(txtAfricanAmerican.Text);
        Total += string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(txtAmericanIndian.Text);
        Total += string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(txtAsian.Text);
        Total += string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(txtHispanic.Text);
        Total += string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(txtWhite.Text);
        Total += string.IsNullOrEmpty(txtMultiRacial.Text) ? 0 : Convert.ToInt32(txtMultiRacial.Text);
        Total += string.IsNullOrEmpty(txtUnknown.Text) ? 0 : Convert.ToInt32(txtUnknown.Text);
        txtTotal.Text = Convert.ToString(Total);
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetEnrollment item = new MagnetEnrollment();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
        }
        item.StageID = Convert.ToInt32(ddlReportType.SelectedValue);
        item.AmericanIndian = string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(txtAmericanIndian.Text);
        item.Asian = string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(txtAsian.Text);
        item.AfircanAmerican = string.IsNullOrEmpty(txtAfricanAmerican.Text) ? 0 : Convert.ToInt32(txtAfricanAmerican.Text);
        item.Hispanic = string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(txtHispanic.Text);
        item.White = string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(txtWhite.Text);
        item.MultiRacial = string.IsNullOrEmpty(txtMultiRacial.Text) ? 0 : Convert.ToInt32(txtMultiRacial.Text);
        item.UnknownRacial = string.IsNullOrEmpty(txtUnknown.Text) ? 0 : Convert.ToInt32(txtUnknown.Text);
        item.TotalApplicants= string.IsNullOrEmpty(txtTotal.Text) ? 0 : Convert.ToInt32(txtTotal.Text);
        item.Save();
        hfID.Value = item.ID.ToString();
    }
    private void LoadData(int SIPID)
    {
        MagnetEnrollment item = MagnetEnrollment.SingleOrDefault(x => x.ID == SIPID);
        ddlReportType.SelectedValue = Convert.ToString(item.StageID);
        txtAmericanIndian.Text = Convert.ToString(item.AmericanIndian);
        txtAfricanAmerican.Text = Convert.ToString(item.AfircanAmerican);
        txtAsian.Text = Convert.ToString(item.Asian);
        txtHispanic.Text = Convert.ToString(item.Hispanic);
        txtWhite.Text = Convert.ToString(item.White);
        txtMultiRacial.Text = Convert.ToString(item.MultiRacial);
        txtUnknown.Text = Convert.ToString(item.UnknownRacial);
        txtTotal.Text = Convert.ToString(item.TotalApplicants);
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("manageenrollment.aspx");
    }
}