﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="managergioutcome.aspx.cs" Inherits="admin_data_managergioutcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage MGI Outcome
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h1>
            <a href="datareport.aspx">Home</a> --> Manage MGI Outcome</h1>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New MGI Outcome" CssClass="msapBtn" OnClick="OnAddData" />
        </p>
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School Name" />
                <asp:BoundField DataField="RGIObjective" SortExpression="" HeaderText="MGI objective" />
                <asp:BoundField DataField="RaciallyIsolatedGroup" SortExpression="" HeaderText="Racial isolated group 1" />
                <asp:BoundField DataField="RaciallyIsolatedGroup1" SortExpression="" HeaderText="Racially isolated group 2" />
                <asp:BoundField DataField="RaciallyIsolatedGroup2" SortExpression="" HeaderText="Racially isolated  group 3" />
                <asp:BoundField DataField="RaciallyIsolatedGroup3" SortExpression="" HeaderText="Racially isolated  group 4" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
</asp:Content>

