﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_feederschools: System.Web.UI.Page
{
    int cohortType = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            cohortType = (int)MagnetReportPeriod.Find(x => x.isActive).Last().CohortType;
            rbtnlstCohort.SelectedValue = cohortType.ToString();
            LoadData(0);
            getGranteelist();
        }
    }

    private void getGranteelist()
    {
        ddlGrantees.Items.Clear();
        string Username = HttpContext.Current.User.Identity.Name;
        var users = MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()));
        if (users.Count > 0)
        {
            hfID.Value = users[0].ID.ToString();
            var reportPeriods = MagnetReportPeriod.Find(x => x.isReportPeriod == true);
            if (reportPeriods.Count == 0)
            {
                reportPeriods = MagnetReportPeriod.Find(x => x.isActive == true);
            }

            foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == users[0].ID))
            {
                MagnetGrantee grantee = null;
                if (reportPeriods.Last().ID > 8)
                    grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == cohortType && x.ID != 69 && x.ID != 70);  //Lee county and CHampaign
                else
                    grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == cohortType && x.ID != 71);  //Wake County


                var contacts = MagnetGranteeContact.Find(x => x.LinkID == district.GranteeID && x.ContactType == 1);
                if (grantee != null)
                {
                    String GranteeName = "";
                    if (HttpContext.Current.User.IsInRole("Administrators") || Context.User.IsInRole("Data"))
                    {
                        GranteeName = contacts.Count > 0 ? contacts[0].State + " " + grantee.GranteeName : grantee.GranteeName;
                        //ReportDiv.Visible = true;
                        //GranteeDiv.Visible = true;
                        //NotifyDiv.Visible = true;
                    }
                    else
                        GranteeName = grantee.GranteeName;
                    System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem();
                    item.Value = district.GranteeID.ToString();
                    item.Text = GranteeName;
                    ddlGrantees.Items.Add(item);
                }
            }

            List<System.Web.UI.WebControls.ListItem> li = new List<System.Web.UI.WebControls.ListItem>();
            foreach (System.Web.UI.WebControls.ListItem list in ddlGrantees.Items)
            {
                li.Add(list);
            }
            li.Sort((x, y) => string.Compare(x.Text, y.Text));
            ddlGrantees.Items.Clear();
            ddlGrantees.DataSource = li;
            ddlGrantees.DataTextField = "Text";
            ddlGrantees.DataValueField = "Value";

            ddlGrantees.DataBind();
            ddlGrantees.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Please select", ""));
            //if (ddlGrantees.Items.FindByValue(users[0].GranteeID.ToString()) != null)
            //    ddlGrantees.SelectedValue = users[0].GranteeID.ToString();
            //else
           ddlGrantees.SelectedValue = "";
           Session["SchGranteeid"] = ddlGrantees.SelectedValue;
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetFeederSchool.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(0);
    }
    private void LoadData(int PageNumber)
    {
        int granteeID = 0;
        if (Session["SchGranteeid"] != null && !string.IsNullOrEmpty(Session["SchGranteeid"].ToString()))
        {
            granteeID = Convert.ToInt32(Session["SchGranteeid"]);
        }
        else
        {
            string Username = HttpContext.Current.User.Identity.Name;
            //int userid = (int)MagnetGranteeUser.Find()[0].ID;
            //var data = MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == userid);
            //var grantees = MagnetGrantee.Find(x => x.CohortType == cohortType);
            //foreach (var item in data)
            //{
            //    if
            //}
            //granteeID = (int)MagnetGranteeUser.SingleOrDefault(x => x.UserName.ToLower().Equals(Username.ToLower())).GranteeID;
            granteeID = ddlGrantees.SelectedValue==""? 0: Convert.ToInt32(ddlGrantees.SelectedValue);
            if (Session["ReportPeriodID"] != null)
            {
                var reports = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == Convert.ToInt32(Session["ReportPeriodID"]) && x.ReportType == false);
                if (reports.Count > 0)
                {
                    GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == (int)reports[0].ID);
                    granteeID = (int)report.GranteeID;
                }
                else
                    granteeID = 0; //not find

                Session["SchGranteeid"] = granteeID;
            }
            else
                Response.Redirect("mygrantee.aspx");

        }
        var fdschools = MagnetFeederSchool.Find(x => x.GranteeID == granteeID).OrderBy(x => x.SchoolName).OrderBy(x => x.SchoolName);
        if (fdschools.Count() == 0 && !string.IsNullOrEmpty(ddlGrantees.SelectedValue))
           // lblNorecId.Visible = true;

        GridView1.DataSource = fdschools;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtSchoolName.Text = "";
        hfID.Value = "";
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetFeederSchool school = MagnetFeederSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtSchoolName.Text = school.SchoolName;
        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetFeederSchool school = new MagnetFeederSchool();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            school = MagnetFeederSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            int granteeID = 0;
            if (Session["SchGranteeid"] != null)
            {
                granteeID = Convert.ToInt32(Session["SchGranteeid"]);
            }
            else
            {
                string Username = HttpContext.Current.User.Identity.Name;
                granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            } 
            school.GranteeID = granteeID;
        }
        school.SchoolName = txtSchoolName.Text;
        school.Save();
        LoadData(0);
    }

    protected void ddlGrantees_SelectedIndexChanged(object sender, EventArgs e)
    {
        int granteeID = ddlGrantees.SelectedValue==""? 0:Convert.ToInt32(ddlGrantees.SelectedValue);
        var fdschools = MagnetFeederSchool.Find(x => x.GranteeID == granteeID).OrderBy(x => x.SchoolName).OrderBy(x => x.SchoolName);
        //if (fdschools.Count() == 0 && !string.IsNullOrEmpty(ddlGrantees.SelectedValue))
            //lblNorecId.Visible = true;
        if (!string.IsNullOrEmpty(ddlGrantees.SelectedValue))
        {
           // lblNorecId.Visible = false;
            Newbutton.Visible = true;
        }
        else
            Newbutton.Visible = false;

        GridView1.DataSource = fdschools;
        Session["SchGranteeid"] = granteeID;
        GridView1.DataBind();
    }
    protected void rbtnlstCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        cohortType = Convert.ToInt32(rbtnlstCohort.SelectedValue);

        getGranteelist();
        LoadData(0);
    }
}