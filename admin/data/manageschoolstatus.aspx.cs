﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_manageschoolstatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);
            LoadData();
        }
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        Response.Redirect("schoolstatus.aspx?option=" + (sender as LinkButton).CommandArgument);
    }
    protected void OnAddData(object sender, EventArgs e)
    {
        Response.Redirect("schoolstatus.aspx?option=0");
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        MagnetSchoolStatus.Delete(x => x.ID == id);
        LoadData();
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetSchoolStatuses
                   from n in db.MagnetSchools
                   from l in db.MagnetDLLs
                   where m.SchoolID == n.ID
                   && m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.SchoolStatus == l.TypeIndex
                   && l.TypeID == 28
                   orderby n.SchoolName
                   select new { m.ID, n.SchoolName, m.SchoolStatus, l.TypeName, m.Notes};
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}