﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.IO;

public partial class admin_data_required : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            //Report type
            var data = MagnetDesegregationPlan.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
            if (data.Count > 0)
            {
                hfID.Value = data[0].ID.ToString();
                LoadData();
            }
            else
            {
                MagnetDesegregationPlan Desegregation = new MagnetDesegregationPlan();
                Desegregation.ReportID = Convert.ToInt32(hfReportID.Value);
                Desegregation.Save();
                hfID.Value = Desegregation.ID.ToString();
            }

            //PlanCopy.Attributes.Add("onchange", "return validateFileUpload(this);");
            //SchoolBoard.Attributes.Add("onchange", "return validateFileUpload(this);");
            CourtOrder.Attributes.Add("onchange", "return validateFileUpload(this);");
            //Button10.Attributes.Add("onclick", "return validateFileUpload(document.getElementById('" + FileUpload1.ClientID + "'));");
        }

    }
    private void LoadData()
    {
        MagnetDesegregationPlan plan = MagnetDesegregationPlan.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        if (plan.PlanType != null)
        {
            //Button1.Visible = true;

            MagnetDBDB db = new MagnetDBDB();
            if (plan.PlanType == false)
            {
                //Required
                //RadioButton1.Checked = true;
                var Files = from m in db.MagnetUploads
                            where m.ProjectID == Convert.ToInt32(hfReportID.Value)
                            && m.FormID == 9
                            select new { m.ID, FilePath = "<a target='_blank' href='../upload/" + m.PhysicalName + "'>" + m.FileName + "</a>" };
                GridView1.DataSource = Files;
                GridView1.DataBind();
                if (Files.Count() > 0)
                {
                    CourtOrder.Enabled = false;
                }
                else
                {
                    CourtOrder.Enabled = true;
                }
            }
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        //Delete required files
        foreach (MagnetUpload file in MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == 9))
        {
            if (System.IO.File.Exists(Server.MapPath("../upload/") + file.PhysicalName))
                System.IO.File.Delete(Server.MapPath("../upload/") + file.PhysicalName);
            file.Delete();
        }

        MagnetUpload.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        MagnetDesegregationPlan Desegregation = MagnetDesegregationPlan.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        int count = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == 9).Count;
        if (!(count > 0))
        {
            Desegregation.isRequiredUpload = false;
            Desegregation.Save();
        }
        LoadData();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        //Allow one uploaded documentation
        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);
        bool bDesegregation = grantee.Desegregation == null ? false : (bool)grantee.Desegregation;

        bool ret = false;
        string msg = "";

        //if (RadioButton1.Checked)
        {
            if (CourtOrder.HasFile)
            {
                ret = false;
            }
            else
            {
                if (GridView1.Rows.Count <= 0)
                {
                    ret = true;
                    msg = "Please upload at least one required document!";
                }
            }
        }

        if (ret)
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('" + msg + "');</script>", false);
        }
        else
        {
            //Delete volunteer files
            foreach (MagnetUpload file in MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && (x.FormID == 10 ||x.FormID==11)))
            {
                if (System.IO.File.Exists(Server.MapPath("../upload/") + file.PhysicalName))
                    System.IO.File.Delete(Server.MapPath("../upload/") + file.PhysicalName);
                file.Delete();
            }

            //if (RadioButton1.Checked)
            {
                MagnetDesegregationPlan Desegregation = MagnetDesegregationPlan.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
                Desegregation.PlanType = false;
                Desegregation.isRequiredUpload = true;
                Desegregation.isVoluntaryUpload = false;
                Desegregation.Save();

                if (CourtOrder.HasFile)
                {
                    int ReportID = Convert.ToInt32(hfReportID.Value);
                    GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
                   
                    MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

                    string strReportPeriod = period.ReportPeriod;
                    strReportPeriod += Convert.ToBoolean(period.PeriodType) ? "-Ad hoc" : "-APR";
                    string strGrantee = grantee.GranteeName.Trim().Replace("#","");

                    string strFileroot = Server.MapPath("") + "/../upload/";
                    string strPath = strReportPeriod + "/" + strGrantee.Replace('#', ' ') +"/Required Plan/";
                    string strFilename = CourtOrder.FileName.Replace('#', ' ');
                    string strPathandFile = strPath + strFilename;

                    int projectid = Convert.ToInt32(hfReportID.Value);
                    MagnetUpload mupload = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == strFilename);

                    if (mupload == null)
                    {
                        MagnetUpload upload1 = new MagnetUpload();
                        upload1.ProjectID = Convert.ToInt32(hfReportID.Value);
                        upload1.FormID = 9;
                        string temp1 = CourtOrder.FileName.Replace("#", "");
                        upload1.FileName = temp1;
                        //string guid = System.Guid.NewGuid().ToString() + "-" + temp1;

                        if (!Directory.Exists(strPath))
                        {
                            Directory.CreateDirectory(strFileroot + strPath);
                        }

                        upload1.PhysicalName = strPathandFile;

                        CourtOrder.SaveAs(strFileroot + strPathandFile);
                        upload1.UploadDate = DateTime.Now;
                        upload1.Save();
                    }
                    else
                    {
                        LoadData();
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('This file name already exists. Please rename the file.');</script>", false);
                        return;
                    }
                }
                LoadData();

                var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 5);
                if (muhs.Count > 0)
                {
                    muhs[0].UpdateUser = Context.User.Identity.Name;
                    muhs[0].TimeStamp = DateTime.Now;
                    muhs[0].Save();
                }
                else
                {
                    MagnetUpdateHistory muh = new MagnetUpdateHistory();
                    muh.ReportID = Convert.ToInt32(hfReportID.Value);
                    muh.FormIndex = 5;
                    muh.UpdateUser = Context.User.Identity.Name;
                    muh.TimeStamp = DateTime.Now;
                    muh.Save();
                }

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
            }
        }
    }
    
}