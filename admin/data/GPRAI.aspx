﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="GPRAI.aspx.cs" Inherits="admin_data_gprai" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit School Demographic Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });

        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            var validate = $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val();

            if (validate == "0" && tmp == "0") {
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
            }
            $("#<%=hfValidate.ClientID%>").val('0');
        }
        function setContentDirty() {

            $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            $("#<%=hfValidate.ClientID%>").val('0');
        }

        $(function () {

            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
                $("#<%=hfValidate.ClientID%>").val('0');

                var value = $(':radio:checked').val();
                if (value != null && value == "28") {
                    $("#<%=txtOther.ClientID%>").removeAttr('disabled');
                }
                else {
                    $("#<%=txtOther.ClientID%>").attr('disabled', 'disabled');
                    $("#<%=txtOther.ClientID%>").val(" ");

                }

            });


            $("#<%=chkNA.ClientID%>").click(function () {
                setContentDirty();

            });

            window.onbeforeunload = UnloadConfirm;
        });



        $(document).ready(function () {

            //$('label[for=ctl00_ContentPlaceHolder1_cblGrades_0]').css("color", "gray");

        });


        function cvFieldRequireed(sender, args) {
            args.IsValid = false;
            var chkControlId = sender.id.replace("cv_", "");

            var ddlobj = document.getElementById(chkControlId);
            var selectVal = ddlobj.options[ddlobj.selectedIndex].value;
            var count = $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val();
            var isvalid = true;

            if (selectVal != "") {
                args.IsValid = true;
                if(isvalid)
                  $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('1');
            }
            else {
                $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('0');
                isvalid = false;
            }


        }

        function cvTextBoxValdate(sender, args) {
            args.IsValid = true;

            var value = $(':radio:checked').val();

            if (value!=null && value == "28" && $("#<%=txtOther.ClientID%>").val() == "") {
                args.IsValid = false;
            }
            if (!args.IsValid)
                $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('0');
        }


        function ValidateRadioButtonList7(sender, args) {
          args.IsValid = false;

          var radChecked = $(':radio:checked').val();

          if (radChecked != null)
              args.IsValid = true;

              if(!args.IsValid)
                  $('input[name=ctl00$ContentPlaceHolder1$hfValidate]').val('0');
          }

          function ValidateCBList(source, args) {
              var chkListModules = document.getElementById('<%= cblGrades.ClientID %>');
              var chkListinputs = chkListModules.getElementsByTagName("input");
              for (var i = 0; i < chkListinputs.length; i++) {
                  if (chkListinputs[i].checked) {
                      args.IsValid = true;
                      return;
                  }
              }
              args.IsValid = false;
          }

    </script>

    <style type="text/css">
     
        .msapDataTbl tr td:first-child
        {
            border-right: 0px !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
        .msapDataTbl .DotnetTbl tr td:first-child
        {
            color: #000 !important;
        }
        .RadInput_Default table
        {
            width: 20px  !important ;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managegpra.aspx">GPRA Table</a> <span class="greater_than">&gt;</span>
            Part I. School Demographic Data</h4>
            <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <%-- SIP --%>
        <br />
        <div class="GPRA_titles">
            <asp:Label ID="lblSchoolName" runat="server" Width="380px" ></asp:Label></div>
        <div class="tab_area">
            <ul class="tabs">
                <li><a runat="server" id="tabPartVI" href="">Part VI</a></li>
                <li><a runat="server" id="tabPartV" href="">Part V</a></li>
                <li><a runat="server" id="tabPartIV" href="">Part IV</a></li>
                <li><a runat="server" id="tabPartIII" href="">Part III</a></li>
                <li><a runat="server" id="tabPartII" href="">Part II</a></li>
                <li class="tab_active">Part I</li>
            </ul>
        </div>
        <br />
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Part I.
                School Demographic Data</b> </span>
        <p>
           Part I of the GPRA Table collects MSAP school demographic data. Follow the instructions in the <i>GPRA Guide</i> to complete each item. When you finish entering data for this page, click on Save Record before proceeding.
        </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfReportID" runat="server" />
                <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
                <asp:HiddenField ID="hfValidate" runat="server" Value="0" />
                <table class="msapDataTbl GPRA_tbl">
                    <tr>
                        <th colspan="2" align="center">
                            School demographic data
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                    <tr>
                        <td class="subtitles_green">
                            1.
                        </td>
                        <td width="36%" class="subtitles_green">
                            School name
                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblSchoolName2" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtitles_green">
                            2.
                        </td>
                        <td width="36%" class="subtitles_green">
                            Grantee name
                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblGranteeName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtitles_green">
                            3.
                        </td>
                        <td width="36%" class="subtitles_green">
                            Check all grades served by your magnet program.
                            <asp:CustomValidator runat="server" ID="cvCBList"
  ClientValidationFunction="ValidateCBList"
  ErrorMessage="This field is Required!" ></asp:CustomValidator>
                        </td>
                        <td style="border-right: 0px !important;" width="70%">
                            <asp:CheckBoxList ID="cblGrades" runat="server" RepeatColumns="7" CssClass="DotnetTbl change">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtitles_green">
                            4.
                        </td>
                        <td class="subtitles_green">
                            What is the school’s magnet program type?
                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlProgramType" runat="server" CssClass="msapDataTxt"
                                Width="130">
                                <asp:ListItem Value="" Selected="True">Please select</asp:ListItem>
                                <asp:ListItem Value="0">Whole-school</asp:ListItem>
                                <asp:ListItem Value="1">Partial</asp:ListItem>
                            </asp:DropDownList>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlProgramType"
                                ErrorMessage="This field is required!"></asp:RequiredFieldValidator>--%>
                                <asp:CustomValidator ClientValidationFunction="cvFieldRequireed"
                            ID="cv_ddlProgramType" runat="server" Display="Dynamic" 
                            ErrorMessage="This field is required!"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtitles_green">
                            5.
                        </td>
                        <td class="subtitles_green">
                            Is this a Title I funded school?<br />
                            <%--<span class="normal_txt">(If “no” is selected, skip 6 & 7 and move on to 8)</span>--%>
                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlTitleISchoolFunding" runat="server"
                                CssClass="msapDataTxt" Width="130"  >
                                <asp:ListItem Value="" Selected="True">Please select</asp:ListItem>
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlTitleISchoolFunding"
                                ErrorMessage="This field is required!"></asp:RequiredFieldValidator>--%>
                                 <asp:CustomValidator ClientValidationFunction="cvFieldRequireed"
                            ID="cv_ddlTitleISchoolFunding" runat="server" Display="Dynamic" 
                            ErrorMessage="This field is required!"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtitles_green">
                            6.
                        </td>
                        <td class="subtitles_green">
                            Is this school in Title I improvement?<br />

                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlTitleISchoolImprovement" runat="server"
                                CssClass="msapDataTxt" Width="130"  >
                                <asp:ListItem Value="" Selected="True">Please select</asp:ListItem>
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlTitleISchoolImprovement"
                                ErrorMessage="This field is required!" ></asp:RequiredFieldValidator>--%>
                                 <asp:CustomValidator ClientValidationFunction="cvFieldRequireed"
                            ID="cv_ddlTitleISchoolImprovement" runat="server" Display="Dynamic" 
                            ErrorMessage="This field is required!"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtitles_green"  style="vertical-align: top">
                            7.
                        </td>
                        <td class="subtitles_green"  style="vertical-align: top">
                            How has this school been identified? (Select only one.)</td>
                        <td style="border-right: 0px !important;" width="70%">
                        <div>
                        <asp:RadioButtonList ID="rblImprovementStatus" runat="server" RepeatColumns="2" RepeatLayout="table"   CssClass="DotnetTbl RadioButtonList change"> 
                            </asp:RadioButtonList>
                        </div>
                        <div style="position:relative;top:-24px;left:220px;">
                        <asp:TextBox ID="txtOther" runat="server"  MaxLength="30"  ></asp:TextBox>
                         <asp:CustomValidator ClientValidationFunction="cvTextBoxValdate"
                            ID="CustomValidator1" runat="server" Display="Dynamic" 
                            ErrorMessage="This textbox can not be empty!"></asp:CustomValidator>
                        </div>
                        <br />
                        <div style="padding-left:120px" >
                        <input type="radio" id="chkNA" runat="server" class="change" name="rblImprovementStatus" value="29" />Not Applicable
                        </div>

                            

                            <br />
                            &nbsp;<asp:CustomValidator ClientValidationFunction="ValidateRadioButtonList7"
                            ID="cv_rblImprovementStatus" runat="server" Display="Dynamic" 
                            ErrorMessage="At least one checkbox has to be checked!"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="subtitles_green">
                            8.
                        </td>
                        <td class="subtitles_green">
                            Has this school been identified by your state as a persistently lowest-achieving
                            school?<br />
                            <%--<span class="normal_txt">(If “no” is selected, skip 9 and move to Part II)</span>--%>
                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlLowestArchieving" runat="server"
                                CssClass="msapDataTxt" Width="130"  >
                                <asp:ListItem Value="" Selected="True">Please select</asp:ListItem>
                                 <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlLowestArchieving"
                                ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                        <%--        <asp:CustomValidator ClientValidationFunction="cvFieldRequireed"
                            ID="cv_ddlLowestArchieving" runat="server" Display="Dynamic" 
                            ErrorMessage="This field is required!"></asp:CustomValidator>--%>
                        </td>
                    </tr>
                    <tr class="msapDataTblLastRowWithBorder">
                        <td valign="top" class="subtitles_green">
                            9.
                        </td>
                        <td class="subtitles_green">
                            Does this school receive School Improvement Grant (SIG) funds?
                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlSIGFunds" runat="server" CssClass="msapDataTxt"
                                Width="130">
                                <asp:ListItem Value="" Selected="True">Please select</asp:ListItem>
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlSIGFunds"
                                ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                            <%--<asp:CustomValidator ClientValidationFunction="cvSIGFunds"
                            ID="CustomValidator1" runat="server" Display="Dynamic" 
                            ErrorMessage="This field is required!"></asp:CustomValidator>--%>
                        </td>
                    </tr>
                    <tr class="msapDataTblLastRowWithBorder">
                        <td valign="top" class="subtitles_green">
                            9a.
                        </td>
                        <td class="subtitles_green">
                            What percentage of students enrolled in this school are eligible for the Free and Reduced-Priced Meal Program for the 2014-15 school year?
                        </td>
                        <td style="border-right: 0px !important;">
                            &nbsp;&nbsp;&nbsp;&nbsp; 

					   <telerik:radnumerictextbox ID="txtFRPMPpercentage" runat="server" CssClass="change" Width="80"
                                  MaxLength="4" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="2" 
                                  Type="Number"  /><span style="padding-left:15px;">percent</span>


                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div align="right" style="margin-top: 10px;">
            <asp:Button ID="Button2" runat="server" CssClass="surveyBtn1" Text="Save Record"
                OnClick="OnSave" />
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <a href="manageGPRA.aspx">Back to GPRA table</a>&nbsp;&nbsp;&nbsp;&nbsp; Part I&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server" id="LinkII" href="GPRAII.aspx">Part II</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server" id="LinkIII" href="GPRAIII.aspx">Part
                III</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server" id="LinkIV" href="GPRAIV.aspx">Part IV</a>
                &nbsp;&nbsp;&nbsp;&nbsp;<a runat="server" id="LinkV" href="GPRAV.aspx">Part V</a>
                 &nbsp;&nbsp;&nbsp;&nbsp;<a runat="server" id="LinkVI" href="GPRAVI.aspx">Part VI</a>
        </div>
    </div>
    <asp:SqlDataSource ID="sqlDSImprovementStatus" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MagnetServer %>" 
        SelectCommand="SELECT * FROM [MagnetGPRAImprovementStatus] ORDER BY [id]"></asp:SqlDataSource>
</asp:Content>
