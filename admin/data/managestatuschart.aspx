﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managestatuschart.aspx.cs" Inherits="admin_data_managestatuschart"
    ErrorPage="~/Error_page.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Project Objectives
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        $(document).ready(function () {
            /*$('tr').find('td:contains("No")').each(function () {
                $(this).parent().css("background-color", "#eee");
                $(this).parent().css("color", "#ccc");
            });*/
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Project Status Chart</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <div class="titles_noTabs">
        Project Status Chart</div>
    <br />
    <p class="clear">
        Report on the results to date of your project evaluation as required under EDGAR, 34 CFR 75.590. For each approved project objective, provide quantitative and/or qualitative data for each performance measure. Also include a description of findings or outcomes to demonstrate that you have met or are making progress toward meeting the performance measure. 
        <img src="../../images/question_mark-thumb.png" title="To report on an existing project objective, click Edit in the last column that corresponds to the objective and enter the appropriate information." class="Q_mark"/>
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        GridLines="None" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl CenterContent"
        OnRowDataBound="OnRowDataBound">
        <EmptyDataTemplate>
            <table class="emptyTbl">
                <tr>
                    <th>&nbsp;
                        
                    </th>
                    <th>
                        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                        Objective
                    </th>
                    <th>
                        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                        Active
                    </th>
                    <th class="TDWithBottomBorder" style="border-right: 0px !important;">&nbsp;
                        
                    </th>
                </tr>
                <tr>
                    <td colspan="4" class="TDWithBottomBorder">&nbsp;
                        
                    </td>
                </tr>
            </table>
            <style>
                .msapDataTbl td, .msapDataTbl th
                {
                    padding: 0px;
                    border-right: 0px !important;
                }
            </style>
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Container.DataItemIndex + 1 %></font>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                    Objective
                </HeaderTemplate>
                <ItemStyle CssClass="txtAlignLeft" />
                <ItemTemplate>
                    <%# Eval("ProjectObjective")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                    Active
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("ActiveStatus")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle CssClass="TDWithBottomBorder LeftAlignCell_LinksCol" />
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton><br />
                    <asp:LinkButton ID="LinkButton2" runat="server" Text='<%# Eval("ActiveAction") %>'
                        CommandArgument='<%# Eval("ID") %>' OnClick="OnDeactivate" Enabled="true" Visible="false"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <div style="margin-top: 12px;">
        <table width="100%" id="Obj_print_rw">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblMessage"></asp:Label>
                </td>
                <td align="right">
                    <asp:Button ID="Newbutton" runat="server" Text="New Project Objective" CssClass="surveyBtn2"
                        OnClick="OnAddData" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button1" runat="server" Text="Review" CssClass="surveyBtn" OnClick="OnPrint" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
