﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="grantees.aspx.cs" Inherits="admin_data_grantees" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Grantees
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Manage Grantees</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New Grantees" CssClass="msapBtn" OnClick="OnAddGrantee" />
        </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            >
            <Columns>
                <asp:BoundField DataField="GranteeName" SortExpression="" HeaderText="Grantee Name" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>' Visible="false"
                            OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hfID" runat="server" />
    <%-- Event --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Grantee</div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                Grantee Name:
                            </td>
                            <td>
                                <asp:TextBox ID="txtGranteeName" runat="server" MaxLength="250" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                            </td>
                            <td>
                                <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button14" /> 
                    <asp:AsyncPostBackTrigger ControlID="Button15" EventName="Click" /> 
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
