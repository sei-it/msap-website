﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="GPRAIII.aspx.cs" Inherits="admin_data_gpraiii" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit GPRA Performance Measure 2 & 3 Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
    </script>
    <style>
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
        .msapDataTbl th
        {
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managegpra.aspx">GPRA Table</a> <span class="greater_than">&gt;</span>
            Part III. GPRA Performance Measures 2 and 3 Data</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <div class="GPRA_titles">
            <asp:Label ID="lblSchoolName" runat="server" Width="380px"></asp:Label></div>
        <div class="tab_area">
            <ul class="tabs">
                <li><a runat="server" id="tabPartVI" href="">Part VI</a></li>
                <li><a runat="server" id="tabPartV" href="">Part V</a></li>
                <li><a runat="server" id="tabPartIV" href="">Part IV</a></li>
                <li class="tab_active">Part III</li>
                <li><a runat="server" id="tabPartII" href="">Part II</a></li>
                <li><a runat="server" id="tabPartI" href="">Part I</a></li>
            </ul>
        </div>
        <br />
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Part III.
                GPRA Performance Measures 2 and 3 Data</b> </span>
        <p>
            <asp:Literal ID="ltlTitleDes" runat="server"></asp:Literal>
        </p>
        <div style="padding-left:60px;">
        <%--<p><b>GPRA Performance Measure 2: </b>The percentage of students from major racial and ethnic groups in magnet schools receiving assistance who score<br />
proficient or above on State assessments in reading/language arts.
        </p>
        <p><b>GPRA Performance Measure 3: </b>The percentage of students from major racial and ethnic groups in magnet schools receiving assistance who score<br />
proficient or above on State assessments in mathematics.
        </p>--%>
        </div>
        <div>
            <%-- SIP --%>
            <asp:HiddenField ID="hfID" runat="server" />
            <asp:HiddenField ID="hfReportID" runat="server" />
            <asp:HiddenField ID="hfParticipationReading" runat="server" />
            <asp:HiddenField ID="hfParticipationMath" runat="server" />
            <asp:HiddenField ID="hfAchievementReading" runat="server" />
            <asp:HiddenField ID="hfAchievementMath" runat="server" />
            <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
            <div style="margin-top: 20px;">
                <table class="msapDataTbl">
                    <tr>
                        <td colspan="4" align="center" class="TDWithBottomBorder" style="color: #F58220 !important; font-weight: bold;">
                            <asp:Literal ID="ltlHead1" runat="server" />
                        </td>
                        <td colspan="4" class="TDWithBottomBorder" align="center" style="color: #F58220 !important; font-weight: bold;">
                            <asp:Literal ID="ltlHead2" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <th class="TDWithBorder LeftAlignCell" colspan="2" style="color: #4E8396; font-weight: normal;">
                            Number of students that participated in the state<asp:Literal ID="ltlHeadCol1" runat="server" />
                            assessment in reading/language arts and mathematics
                        </th>
                        <th style="color: #4E8396; font-weight: normal;">
                            <br />
                            Reading<br />
                            <asp:Label ID="lblReportPeriod" runat="server"></asp:Label>
                        </th>
                        <th class="TDWithBorder" style="color: #4E8396; font-weight: normal;">
                            <br />
                            Math<br />
                            <asp:Label ID="lblReportPeriod2" runat="server"></asp:Label>
                        </th>
                        <th class="TDWithBorder LeftAlignCell" colspan="2" style="color: #4E8396; font-weight: normal;">
                            <asp:Literal ID="ltlHeadCol2" runat="server" />
                        </th>
                        <th style="color: #4E8396; font-weight: normal;">
                            <br />
                            Reading<br />
                            <asp:Label ID="lblReportPeriod3" runat="server"></asp:Label>
                        </th>
                        <th style="color: #4E8396; font-weight: normal;">
                            <br />
                            Math<br />
                            <asp:Label ID="lblReportPeriod4" runat="server"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            29.
                        </td>
                        <td class="TDWithBottomRightBorder">
                            All students<%--<img src="../../images/question_mark-thumb.png" title="The total number of students in each major racial and ethnic group (items 30-35) should tally in this field. " />--%></td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtTotalParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="200" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" FilterType="Numbers"
                                TargetControlID="txtTotalParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomRightBorder">
                            <asp:TextBox ID="txtTotalParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="201" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" FilterType="Numbers"
                                TargetControlID="txtTotalParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            39.
                        </td>
                        <td class="TDWithBottomRightBorder">
                            All students
                            <%--<img src="../../images/question_mark-thumb.png" title="The total number of students in each major racial and ethnic group (items 39-44) should tally in this field. " />--%>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtTotalAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="220" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" FilterType="Numbers"
                                TargetControlID="txtTotalAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtTotalAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="221" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" FilterType="Numbers"
                                TargetControlID="txtTotalAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            30.
                        </td>
                        <td>
                            <b>American Indian or Alaska Native</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtIndianParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="202" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers"
                                TargetControlID="txtIndianParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtIndianParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="203" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers"
                                TargetControlID="txtIndianParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder" valign="top">
                            40.
                        </td>
                        <td>
                            <b>American Indian or Alaska Native</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtIndianAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="222" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers"
                                TargetControlID="txtIndianAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtIndianAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="223" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers"
                                TargetControlID="txtIndianAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            31.
                        </td>
                        <td>
                            <b>Asian</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtAsianParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="204" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                TargetControlID="txtAsianParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAsianParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="205" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers"
                                TargetControlID="txtAsianParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            41.
                        </td>
                        <td>
                            <b>Asian</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtAsianAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="224" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers"
                                TargetControlID="txtAsianAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtAsianAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="225" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Numbers"
                                TargetControlID="txtAsianAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            32.
                        </td>
                        <td>
                            <b>Black or African-American</b> students</td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtBlackParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="206" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtBlackParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtBlackParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="207" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Numbers"
                                TargetControlID="txtBlackParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            42.
                        </td>
                        <td>
                            <b>Black or African-American</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtBlackAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="226" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterType="Numbers"
                                TargetControlID="txtBlackAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtBlackAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="227" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" FilterType="Numbers"
                                TargetControlID="txtBlackAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            33.
                        </td>
                        <td>
                            <b>Hispanic or Latino</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtHispanicParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="208" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                TargetControlID="txtHispanicParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtHispanicParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="209" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Numbers"
                                TargetControlID="txtHispanicParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            43.
                        </td>
                        <td>
                            <b>Hispanic or Latino</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtHispanicAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="228" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" FilterType="Numbers"
                                TargetControlID="txtHispanicAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtHispanicAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="229" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtHispanicAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder" valign="top">
                            34.
                        </td>
                        <td>
                            <b>Native Hawaiian or Other Pacific Islander</b> students</td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtHawaiianParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="210" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                                TargetControlID="txtHawaiianParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtHawaiianParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="211" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Numbers"
                                TargetControlID="txtHawaiianParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder" valign="top">
                            44.
                        </td>
                        <td>
                            <b>Native Hawaiian or Other Pacific Islander</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtHawaiianAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="230" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" FilterType="Numbers"
                                TargetControlID="txtHawaiianAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtHawaiianAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="231" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" FilterType="Numbers"
                                TargetControlID="txtHawaiianAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            35.
                        </td>
                        <td>
                            <b>White</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtWhiteParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="212" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                                TargetControlID="txtWhiteParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtWhiteParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="213" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Numbers"
                                TargetControlID="txtWhiteParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            45.
                        </td>
                        <td>
                            <b>White</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtWhiteAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="232" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterType="Numbers"
                                TargetControlID="txtWhiteAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtWhiteAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="233" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterType="Numbers"
                                TargetControlID="txtWhiteAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            36.
                        </td>
                        <td>
                            <b>Two or more races students</b>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtMultiRaceParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="214" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers"
                                TargetControlID="txtMultiRaceParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMultiRaceParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="215" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers"
                                TargetControlID="txtMultiRaceParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            46.
                        </td>
                        <td>
                            <b>Two or more races students</b>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtMultiRaceAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="234" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers"
                                TargetControlID="txtMultiRaceAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtMultiRaceAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="235" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers"
                                TargetControlID="txtMultiRaceAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            37.
                        </td>
                        <td>
                            <b>Economically disadvantaged</b> students
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtDisadvantagedParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="216" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                                TargetControlID="txtDisadvantagedParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDisadvantagedParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="217" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Numbers"
                                TargetControlID="txtDisadvantagedParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            47.
                        </td>
                        <td>
                            <b>Economically disadvantaged</b> students</td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtDisadvantagedAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="236" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterType="Numbers"
                                TargetControlID="txtDisadvantagedAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtDisadvantagedAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="237" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Numbers"
                                TargetControlID="txtDisadvantagedAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="TDWithBottomNoRightBorder">
                            38.
                        </td>
                        <td>
                            <b>English language learners</b></td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtEnglishLearnerParticipationReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="218" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                                TargetControlID="txtEnglishLearnerParticipationReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEnglishLearnerParticipationMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="219" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Numbers"
                                TargetControlID="txtEnglishLearnerParticipationMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            48.
                        </td>
                        <td>
                            <b>English language learners</b>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtEnglishLearnerAchievementReading" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="238" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" FilterType="Numbers"
                                TargetControlID="txtEnglishLearnerAchievementReading">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <asp:TextBox ID="txtEnglishLearnerAchievementMath" runat="server" CssClass="msapDataTxtSmall change"
                                TabIndex="239" MaxLength="6"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" FilterType="Numbers"
                                TargetControlID="txtEnglishLearnerAchievementMath">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align: right; margin-top: 20px;">
                <asp:Button ID="Button10" runat="server" CssClass="surveyBtn1 postbutton" Text="Save Record"
                    OnClick="OnSave" />
            </div>
            <div style="text-align: right; margin-top: 20px;">
                <a href="manageGPRA.aspx">Back to GPRA table</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                    id="LinkI" href="GPRAII.aspx">Part I</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                        id="LinkII" href="GPRAIII.aspx">Part II</a>&nbsp;&nbsp;&nbsp;&nbsp; Part
                III&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server" id="LinkIV" href="GPRAIV.aspx">Part IV</a>
                &nbsp;&nbsp;&nbsp;&nbsp; <a runat="server" id="LinkV" href="GPRAV.aspx">Part V</a>
                &nbsp;&nbsp;&nbsp;&nbsp; <a runat="server" id="LinkVI" href="GPRAV.aspx">Part VI</a>
            </div>
        </div>
</asp:Content>
