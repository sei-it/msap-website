﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using System.Net;
using System.Net.Mail;
using System.Web.Security;
using System.Text;

public partial class admin_report_quarterreports : System.Web.UI.Page
{
    int reportyear = 1;
    int reportPeriod = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        reportPeriod = Convert.ToInt32(Session["ReportPeriodID"]);
        MagnetReportPeriod MrptPeriod = MagnetReportPeriod.SingleOrDefault(x => x.ID == reportPeriod);
        if (MrptPeriod != null)
        {
            string rptPeriod, strTmp = MrptPeriod.Des;
            rptPeriod = strTmp.Replace("|", " - ");
            ltlSubTitle.Text = rptPeriod;
            reportyear = (int)MrptPeriod.reportYear;
        }
        Session["ReportPeriodTitle"] = ltlSubTitle.Text;


        if (!Page.IsPostBack)
        {
            string Username = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrEmpty(Username))
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/Default.aspx");
            }

            //int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            int granteeID = Convert.ToInt32(Session["GranteeID"]);

            hfGranteeID.Value = granteeID.ToString();

           
            if (Session["ReportPeriodID"] == null)
            {

                Session["ReportPeriodID"] = MagnetReportPeriod.Find(x => x.isActive == true).Last().ID;

            }

            Session["ReportYear"] = reportyear;

            if (reportPeriod % 2 == 0 && !Context.User.IsInRole("Administrators") && !Context.User.IsInRole("Data"))
            {
                //Ad hoc report
                hdAssurance.CssClass = "adhoc_disable";
                //hdBudgetSummary.CssClass = "adhoc_disable";
                hdDesegregation.CssClass = "adhoc_disable";
                hdFeeder.CssClass = "adhoc_disable";
                hdLeaEnrollment.CssClass = "adhoc_disable";
                hdMagnetSchool.CssClass = "adhoc_disable";
                hdSchoolYear.CssClass = "adhoc_disable";
                hlAssurance.Enabled = false;
                hlAssurance.ImageUrl = "~/images/pencil_deactive.png";
                //hlBudgetSummary.Enabled = false;
                //hlBudgetSummary.ImageUrl = "~/images/pencil_deactive.png";
                hlDesegregation.Enabled = false;
                hlDesegregation.ImageUrl = "~/images/pencil_deactive.png";
                hlFeeder.Enabled = false;
                hlFeeder.ImageUrl = "~/images/pencil_deactive.png";
                hlLeaEnrollment.Enabled = false;
                hlLeaEnrollment.ImageUrl = "~/images/pencil_deactive.png";
                hlMagnetSchool.Enabled = false;
                hlMagnetSchool.ImageUrl = "~/images/pencil_deactive.png";
                hlSchollYear.Enabled = false;
                hlSchollYear.ImageUrl = "~/images/pencil_deactive.png";
            }

            var reports = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == reportPeriod && x.ReportType == false);
            if (reports.Count > 0)
            {
                Session["ReportID"] = reports[0].ID;
                hfReportID.Value = reports[0].ID.ToString();
                //if (reports[0].Desegregation != null && reports[0].Desegregation == true)
                //    Button1.Enabled = false;
            }
            else if (granteeID != 0)
            {
                GranteeReport report = new GranteeReport();
                report.Preparer = Username;
                report.ReportPeriodID = reportPeriod;
                report.GranteeID = granteeID;
                report.ReportType = false;
                report.CreateDate = DateTime.Now;

                MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == reportPeriod);
                report.ReportStart = period.PeriodFrom;
                report.ReportEnd = period.PeriodTo;

                report.Save();
                hfReportID.Value = Convert.ToString(report.ID);
                Session["ReportID"] = report.ID;


                GranteePerformanceCoverSheet sheet = new GranteePerformanceCoverSheet();
                sheet.GranteeReportID = report.ID;
                sheet.Save();
                Session["CoverSheetID"] = sheet.ID;
            }
            else
                Response.Redirect("mygrantee.aspx");

            foreach (MagnetUpdateHistory muhs in MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value)))
            {
                switch (muhs.FormIndex)
                {
                    case 1:
                        if (muhs.TimeStamp != null)
                        {
                            lblCoversheet.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblCoversheet.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 2:
                        if (muhs.TimeStamp != null)
                        {
                            lblStatusChart.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblStatusChart.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 3:
                        if (muhs.TimeStamp != null)
                        {
                            lblGPRA.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblGPRA.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 4:
                        if (muhs.TimeStamp != null)
                        {
                            lblBudgetSummary.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblBudgetSummary.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 5:
                        if (muhs.TimeStamp != null)
                        {
                            lblDesegregation.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblDesegregation.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 6:
                        if (muhs.TimeStamp != null)
                        {
                            lblAssurance.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblAssurance.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 7:
                        if (muhs.TimeStamp != null)
                        {
                            lblLEAEnrollment.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblLEAEnrollment.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 8:
                        if (muhs.TimeStamp != null)
                        {
                            lblSchoolYear.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblSchoolYear.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 9:
                        if (muhs.TimeStamp != null)
                        {
                            lblEnrollment.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblEnrollment.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 10:
                        if (muhs.TimeStamp != null)
                        {
                            lblFeederEnrollment.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblFeederEnrollment.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 11:
                        if (muhs.TimeStamp != null)
                        {
                            lblUploadDocs.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblUploadDocs.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 12:
                        if (muhs.TimeStamp != null)
                        {
                            lblExecutiveSummary.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblExecutiveSummary.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                    case 13:
                        if (muhs.TimeStamp != null)
                        {
                            lblAdditional.Text = ((DateTime)muhs.TimeStamp).ToShortDateString();
                            lblAdditional.ToolTip = "Last updated on " + ((DateTime)muhs.TimeStamp).ToShortDateString();
                        }
                        break;
                }
            }

        }
    }
}
