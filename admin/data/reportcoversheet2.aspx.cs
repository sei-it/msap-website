﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_reportcoversheet2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }

            int rptPeriodID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(Session["ReportID"])).ReportPeriodID;
            if (rptPeriodID % 6 == 0) //only be open and display data for Year 3 Ad Hoc. Required 3/26/15 eamil
            {
                txtEntireFederalFunds.Enabled = true;

            }

            hfReportID.Value = Convert.ToString((int)Session["ReportID"]);
            var CoverSheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == (int)Session["ReportID"]);
            if (CoverSheets.Count > 0)
            {
                Session["CoverSheetID"] = CoverSheets[0].ID;
                hfID.Value = CoverSheets[0].ID.ToString();
            }
            else
            {
                GranteePerformanceCoverSheet st = new GranteePerformanceCoverSheet();
                st.GranteeReportID = (int)Session["ReportID"];
                st.Save();
                Session["CoverSheetID"] = st.ID;
                hfID.Value = Convert.ToString(st.ID);
            }

            GranteePerformanceCoverSheet sheet = GranteePerformanceCoverSheet.SingleOrDefault(x => x.ID == (int)Session["CoverSheetID"]);
            hfID.Value = sheet.ID.ToString();
            if (sheet.PreviousFederalFunds != null) txtPreviousFederalFunds.Text = sheet.PreviousFederalFunds.ToString();
            if (sheet.PreviousNonFederalFunds != null && sheet.PreviousNonFederalFunds!=0) txtPreviousNonFederalFunds.Text = sheet.PreviousNonFederalFunds.ToString();
            if (sheet.CurrentFederalFunds != null) txtCurrentFederalFunds.Text = sheet.CurrentFederalFunds.ToString();
            if (sheet.CurrentNonFederalFunds != null && sheet.CurrentNonFederalFunds!=0) txtCurrentNonFederalFunds.Text = sheet.CurrentNonFederalFunds.ToString();
            if (sheet.EntireFederalFunds != null) txtEntireFederalFunds.Text = sheet.EntireFederalFunds.ToString();
            if (sheet.EntireNonFederalFunds != null && sheet.EntireNonFederalFunds!=0) txtEntireNonFederalFunds.Text = sheet.EntireNonFederalFunds.ToString();
            if (sheet.IndirectCost != null)
            {
                rblClaimingIndrectCost.SelectedValue = Convert.ToInt32(sheet.IndirectCost).ToString();
                if (rblClaimingIndrectCost.SelectedIndex == 0)
                {
                    if (sheet.IndirectCostApproved != null)
                    {
                        rblRageAggrement.SelectedValue = Convert.ToInt32(sheet.IndirectCostApproved).ToString();
                        if (rblClaimingIndrectCost.SelectedIndex == 0)
                        {
                            if (sheet.IndirectCostApproved != null)
                            {
                                rblRageAggrement.SelectedValue = Convert.ToInt32(sheet.IndirectCostApproved).ToString();
                                if (rblRageAggrement.SelectedIndex == 0)
                                {
                                    txtAggrementStart.Text = sheet.IndirectCostStart;
                                    txtAggrementEnd.Text = sheet.IndirectCostEnd;
                                    if (sheet.EDApproved != null) rblAgency.SelectedValue = Convert.ToString(Convert.ToInt32(sheet.EDApproved));
                                    if (!(bool)sheet.EDApproved)
                                    {
                                        panelOtherAgency.Visible = true;
                                    }
                                    txtOtherApprovalAgnecy.Text = sheet.OtherAgency;
                                    if (sheet.RateType != null)
                                    {
                                        switch (sheet.RateType)
                                        {
                                            case 1:
                                                rbProvisional.Checked = true;
                                                break;
                                            case 2:
                                                rbFinal.Checked = true;
                                                break;
                                            case 3:
                                                panelOtherRateType.Visible = true;
                                                rbOtherRateType.Checked = true;
                                                txtOtherRateType.Text = sheet.OtherRateType;
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    EnableDisableC(false);
                                }
                            }
                        }
                        else
                        {
                            //EnableDisableBC(false);
                        }
                    }
                }
                else
                {
                    EnableDisableBC(false);
                }
            }
            if (sheet.RestrictedRateProgram != null)
                cbIncludedApprovedAgreement.Checked = (bool)sheet.RestrictedRateProgram;
            if (sheet.Comply34CFR != null)
                cbComplyCFR.Checked = (bool)sheet.Comply34CFR;
            if (sheet.na != null)
                cbNA.Checked = (bool)sheet.na;
        }
    }
    protected void OnOtherAgencyChanged(object sender, EventArgs e)
    {
        if (rblAgency.SelectedIndex == 1)
            panelOtherAgency.Visible = true;
        else
            panelOtherAgency.Visible = false;
    }
    protected void OnOtherRateTypeChanged(object sender, EventArgs e)
    {
        panelOtherRateType.Visible = rbOtherRateType.Checked;
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("reportcoversheet1.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("reportcoversheet3.aspx", true);
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        SaveData();
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    private void SaveData()
    {
        GranteePerformanceCoverSheet sheet = GranteePerformanceCoverSheet.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        sheet.GranteeReportID = Convert.ToInt32(hfReportID.Value);
        sheet.PreviousFederalFunds = string.IsNullOrEmpty(txtPreviousFederalFunds.Text)?(decimal?)null : Convert.ToDecimal(txtPreviousFederalFunds.Text);
        sheet.PreviousNonFederalFunds = string.IsNullOrEmpty(txtPreviousNonFederalFunds.Text) ? (decimal?)null : Convert.ToDecimal(txtPreviousNonFederalFunds.Text);
        sheet.CurrentFederalFunds = string.IsNullOrEmpty(txtCurrentFederalFunds.Text)?0: Convert.ToDecimal(txtCurrentFederalFunds.Text);
        sheet.CurrentNonFederalFunds = string.IsNullOrEmpty(txtCurrentNonFederalFunds.Text) ? (decimal?)null : Convert.ToDecimal(txtCurrentNonFederalFunds.Text);
        sheet.EntireFederalFunds = string.IsNullOrEmpty(txtEntireFederalFunds.Text) ? (decimal?)null : Convert.ToDecimal(txtEntireFederalFunds.Text);
        sheet.EntireNonFederalFunds = string.IsNullOrEmpty(txtEntireNonFederalFunds.Text) ? (decimal?)null : Convert.ToDecimal(txtEntireNonFederalFunds.Text);
        sheet.IndirectCost = rblClaimingIndrectCost.SelectedIndex == 0 ? true : false;
        if (rblClaimingIndrectCost.SelectedIndex == 0)
        {
            sheet.IndirectCostApproved = rblRageAggrement.SelectedIndex == 0 ? true : false;
            if (rblRageAggrement.SelectedIndex == 0)
            {
                sheet.IndirectCostStart = txtAggrementStart.Text;
                sheet.IndirectCostEnd = txtAggrementEnd.Text;
                sheet.EDApproved = rblAgency.SelectedIndex == 0? true : false;
                if(rblAgency.SelectedIndex == 1) sheet.OtherAgency = txtOtherApprovalAgnecy.Text;
                sheet.RateType = rbProvisional.Checked ? 1 : rbFinal.Checked ? 2 : (rbOtherRateType.Checked || !string.IsNullOrEmpty(txtOtherRateType.Text)) ? 3 : 4;
                sheet.OtherRateType = txtOtherRateType.Text;
            }
            sheet.RestrictedRateProgram = cbIncludedApprovedAgreement.Checked ? true : false;
            sheet.Comply34CFR = cbComplyCFR.Checked ? true : false;
            sheet.na = cbNA.Checked ? true : false;
        }
        sheet.Save();
        
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 1);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 1;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
    }
    private void EnableDisableBC(bool val)
    {
        rblRageAggrement.Enabled = val;
        txtAggrementStart.Enabled = val;
        txtAggrementEnd.Enabled = val;
        rblAgency.Enabled = val;
        txtOtherApprovalAgnecy.Enabled = val;
        txtOtherRateType.Enabled = val;
        RequiredFieldValidator1.Enabled = val;
        cbIncludedApprovedAgreement.Enabled = val;
        cbComplyCFR.Enabled = val;
        cbNA.Enabled = val;
        RequiredFieldValidator2.Enabled = val;
        RequiredFieldValidator4.Enabled = val;
        CompareValidatorTextBox1.Enabled = val;
        CompareValidator1.Enabled = val;
        cvEndTime.Enabled = val;
        RequiredFieldValidator5.Enabled = val;
        if (!val)
        {
            rblAgency.Enabled = val;
            rblRageAggrement.ClearSelection();
            txtAggrementStart.Text = "";
            txtAggrementEnd.Text = "";
            txtOtherApprovalAgnecy.Text = "";
            txtOtherRateType.Text = "";
            cbIncludedApprovedAgreement.Checked = val;
            cbComplyCFR.Checked = val;
            cbNA.Checked = val;
        }
    }
    private void EnableDisableC(bool val)
    {
        txtAggrementStart.Enabled = val;
        txtAggrementEnd.Enabled = val;
        rblAgency.Enabled = val;
        txtOtherApprovalAgnecy.Enabled = val;
        RequiredFieldValidator2.Enabled = val;
        RequiredFieldValidator4.Enabled = val;
        RequiredFieldValidator5.Enabled = val;
        txtOtherRateType.Enabled = val;
        RequiredFieldValidator2.Enabled = val;
        RequiredFieldValidator4.Enabled = val;
        CompareValidatorTextBox1.Enabled = val;
        CompareValidator1.Enabled = val;
        cvEndTime.Enabled = val;
        RequiredFieldValidator5.Enabled = val;
        if (!val)
        {
            rblAgency.ClearSelection();
            txtAggrementStart.Text = "";
            txtAggrementEnd.Text = "";
            txtOtherApprovalAgnecy.Text = "";
            txtOtherRateType.Text = "";
        }
    }
    protected void OnClaimingIndirect(object sender, EventArgs e)
    {
        if (rblClaimingIndrectCost.SelectedIndex == 1)
        {
            EnableDisableBC(false);
        }
        else
        {
            EnableDisableBC(true);
        }
    }
    protected void OnRateAgreementChanged(object sender, EventArgs e)
    {
        if (rblRageAggrement.SelectedIndex == 1)
        {
            EnableDisableC(false);
        }
        else
        {
            EnableDisableC(true);
        }
    }
}