﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="objectperformance.aspx.cs" Inherits="admin_data_objectperformance" 
    ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Edit Project Objective
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });          
            $('.number1').keyup(function () {
                var bvalue1 = false;
                $('.number1').each(function (index) {
                    if ($(this).val())
                        bvalue1 = true;
                });
                if (bvalue1) {
                    $('.target').attr("disabled", "disabled");
                    $('.target').each(function (index) {
                        $(this).css("background-color", '#ccc');
                    });
                }
                else {
                    $('.target').removeAttr("disabled");
                    $('.target').each(function (index) {
                        $(this).css("background-color", 'rgb(255, 255, 255)');
                    });
                }
            });
            $('.target').keyup(function () {
                var bvalue2 = false;
                $('.target').each(function (index) {
                    if ($(this).val())
                        bvalue2 = true;
                });
                if (bvalue2) {
                    $('.number1').attr("disabled", "disabled");
                    $('.number1').each(function (index) {
                        $(this).css("background-color", '#ccc');
                    });
                    $('.target').each(function (index) {
                        $(this).css("background-color", 'rgb(255, 255, 255)');
                    });
                }
                else {
                    $('.number1').removeAttr("disabled");
                    $('.number1').each(function (index) {
                        $(this).css("background-color", 'rgb(255, 255, 255)');
                    });
                }
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        function setTextBoxEnable() {
            var bRawvalue = false, bRatiovalue = false;
            $('.number1').each(function (index) {
                if ($(this).val())
                    bRawvalue = true;
            });
            $('.target').each(function (index) {
                if ($(this).val())
                    bRatiovalue = true;
            });
            if (bRawvalue == bRatiovalue && !bRawvalue) {
                $('.number1').removeAttr("disabled");
                $('.target').removeAttr("disabled");
                $('.number1').each(function (index) {
                    $(this).css("background-color", 'rgb(255, 255, 255)');
                });
                $('.target').each(function (index) {
                    $(this).css("background-color", 'rgb(255, 255, 255)');
                });
            }
            else if (bRawvalue) {
                $('.target').attr("disabled", "disabled");
                 $('.target').each(function (index) {
                     $(this).css("background-color", '#ccc');
                 });
                 $('.number1').each(function (index) {
                     $(this).css("background-color", 'rgb(255, 255, 255)');
                 });
            }
            else {
                $('.target').removeAttr("disabled");
                 $('.target').each(function (index) {
                    $(this).css("background-color", 'rgb(255, 255, 255)');
                });
                $('.number1').each(function (index) {
                    $(this).css("background-color", '#ccc');
                });
            }
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
            window.onload = setTextBoxEnable;
        });
        function checkWordLen(obj, wordLen, cid) {
            var len = obj.value.split(/[\s]+/);
            $("#" + cid).val(len.length.toString());
            if (len.length > wordLen) {
                alert("You've exceeded the " + wordLen + " word limit for this field!");
                obj.value = obj.SavedValue;
                //obj.value = obj.value.substring(0, wordLen - 1);
                len = obj.value.split(/[\s]+/);
                $("#" + cid).val(len.length.toString());
                return false;
            } else {
                obj.SavedValue = obj.value;
            }
            return true;
        }

        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <!--<img src="button_arrow.jpg" width="29" height="36" />-->
            <a href="managestatuschart.aspx">Project Status Chart</a> <span class="greater_than">
                &gt;</span>
            <!--<img src="button_arrow.jpg" width="29" height="36" />-->
            <span id="objectiveDiv" runat="server"></span>
            <!--<img src="button_arrow.jpg" width="29" height="36" />-->
            <span class="greater_than">&gt;</span> Edit Performance Measure
            <asp:Label ID="lblSSID" runat="server"></asp:Label>
        </h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfStatusID" runat="server" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <br />
        <div class="titles">
            <asp:Label ID="lblObjective" runat="server"></asp:Label>
        </div>
        <div class="tab_area">
            <ul class="tabs">
                <div id="tabDiv" runat="server" style="width: 600px; float: right;">
                </div>
            </ul>
        </div>
        <br />
        <p style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>
                <asp:Label ID="lblMeasure" runat="server"></asp:Label></b> </p>
        <p class="clear" style="padding-top:5px;">
            Enter your performance measure data. When you finish entering data, click on Save Record before proceeding. <img src="../../images/question_mark-thumb.png" title="See Dear Colleague Letter and ED 524 B Instructions." class="Q_mark" /></p>
        <%-- Budget --%>
        <table width="100%" id="PSC_measure_tbl">
            <tr>
                <td class="subtitles_green">
                    Performance Measure:
                </td>
                <td>
                    <asp:TextBox ID="txtPerformanceMeasure" MaxLength="250" Font-Size="10pt" runat="server" Columns="90"
                        Rows="4" TextMode="MultiLine" CssClass="msapDataTxt change"></asp:TextBox><br />
                </td>
            </tr>
            <tr>
                <td class="subtitles_green">
                    Measure Type:
                </td>
                <td>
                    <asp:DropDownList ID="ddlMeasureType" runat="server" Font-Size="10" CssClass="msapDataTxt change">
                        <asp:ListItem Value="">Please select</asp:ListItem>
                        <asp:ListItem Value="Desegregation and Choice">Desegregation and Choice</asp:ListItem>
                        <asp:ListItem Value="Building Capacity">Building Capacity</asp:ListItem>
                        <asp:ListItem Value="Academic Achievement">Academic Achievement</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="subtitles_green">
                    Quantitative Data:
                    
                </td>
                <td>
                    <table class="msapChartTbl">
                        <tr>
                            <th colspan="3" align="center" style="border-bottom: 2px solid #F58220; border-left: 0px;
                                border-top: 0px; border-right: 0px;">
                                Target
                            </th>
                            <th colspan="3" align="center" style="border-bottom: 2px solid #F58220; border-right: 0px;
                                border-top: 0px">
                                Actual Performance Data
                            </th>
                        </tr>
                        <tr>
                            <td align="center" width="20%" style="border-left: 0px; border-right: 1px solid black;">
                                Raw number
                            </td>
                            <td align="center" width="20%" style="border-right: 1px solid black;">
                                Ratio
                            </td>
                            <td align="center" width="10%" style="border-right: 2px solid #F58220;">
                                %
                            </td>
                            <td align="center" width="20%" style="border-right: 1px solid black;">
                                Raw number
                            </td>
                            <td align="center" width="20%" style="border-right: 1px solid black;">
                                Ratio
                            </td>
                            <td align="center" width="10%" style="border-right: 0px;">
                                %
                            </td>
                        </tr>
                        <tr>
                            <td style="border-left: 0px; border-right: 1px solid black; text-align: center;">
                                <telerik:RadNumericTextBox MinValue="0" ID="txtTargetNumber" ClientIDMode="Static" ViewStateMode="Disabled" AutoCompleteType="None"
                                    runat="server" CssClass="msapDataTxtSmall change number1" Skin="MyCustomSkin" EnableEmbeddedSkins="false"
                                    Type="Number" NumberFormat-DecimalDigits="2" Width="80"  autocomplete="off">
                                </telerik:RadNumericTextBox>
                            </td>
                            <td style="border-right: 1px solid black; text-align: center;">
                                <asp:TextBox ID="txtTargetRatio1" ClientIDMode="Static" Width="40" runat="server" ViewStateMode="Disabled"
                                    CssClass="msapDataTxtSmall change target"   autocomplete="off"></asp:TextBox>
                                /
                                <asp:TextBox ID="txtTargetRatio2" ClientIDMode="Static" Width="40" runat="server" ViewStateMode="Disabled"
                                    CssClass="msapDataTxtSmall change target"  autocomplete="off"></asp:TextBox>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtTargetRatio1"
                                    runat="server" FilterType="Numbers">
                                </ajax:FilteredTextBoxExtender>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtTargetRatio2"
                                    runat="server" FilterType="Numbers">
                                </ajax:FilteredTextBoxExtender>
                            </td>
                            <td style="border-right: 2px solid #F58220; text-align: center;">
                                <telerik:RadNumericTextBox MinValue="0" ID="txtTargetPercentage" ClientIDMode="Static" ViewStateMode="Disabled"
                                    runat="server" CssClass="change target" Skin="MyCustomSkin" EnableEmbeddedSkins="false"
                                    Type="Number" NumberFormat-DecimalDigits="2" Width="40"   autocomplete="off">
                                </telerik:RadNumericTextBox>
                            </td>
                            <td style="border-right: 1px solid black; text-align: center;">
                                <telerik:RadNumericTextBox MinValue="0" ID="txtActualNumber" ClientIDMode="Static" ViewStateMode="Disabled"
                                    runat="server" Enabled="true" CssClass="change number1" Skin="MyCustomSkin" EnableEmbeddedSkins="false"
                                    Type="Number" NumberFormat-DecimalDigits="2" Width="80"   autocomplete="off">
                                </telerik:RadNumericTextBox>
                            </td>
                            <td style="border-right: 1px solid black; text-align: center;">
                                <asp:TextBox ID="txtActualRatio1" ClientIDMode="Static" Width="40" runat="server" ViewStateMode="Disabled"
                                    Enabled="true" CssClass="msapDataTxtSmall change target"   autocomplete="off"></asp:TextBox>
                                /
                                <asp:TextBox ID="txtActualRatio2" ClientIDMode="Static" Width="40" runat="server" ViewStateMode="Disabled"
                                    CssClass="msapDataTxtSmall change target"   autocomplete="off"></asp:TextBox>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="txtActualRatio1"
                                    runat="server" FilterType="Numbers">
                                </ajax:FilteredTextBoxExtender>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtActualRatio2"
                                    runat="server" FilterType="Numbers">
                                </ajax:FilteredTextBoxExtender>
                            </td>
                            <td style="border-right: 0px; text-align: center;">
                                <telerik:RadNumericTextBox MinValue="0" ID="txtActualPercentage" ClientIDMode="Static" ViewStateMode="Disabled"
                                    runat="server" CssClass="change target" Skin="MyCustomSkin" EnableEmbeddedSkins="false"
                                    Type="Number" NumberFormat-DecimalDigits="2" Width="40"   autocomplete="off"></telerik:RadNumericTextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="subtitles_green">
                    Explanation of Progress:
                </td>
                <td>
                    <asp:TextBox ID="txtExplanationProgress" Font-Size="10pt" runat="server" Columns="90" 
                        Rows="8" TextMode="MultiLine" CssClass="msapDataTxt change" onkeyup="checkWordLen(this, 500, 'explanationLength');"></asp:TextBox>
                    <input type="text" disabled="disabled" size="4" id="explanationLength" /> 
                </td>
            </tr>
        </table>
        <p style="text-align: right;">
            <asp:Button ID="button1" runat="server" Text="Save Record" CssClass="surveyBtn2 postbutton"
                OnClick="OnSave" />
        </p>
        <div id="linkDiv" runat="server" style="text-align: right;">
        </div>
    </div>
</asp:Content>
