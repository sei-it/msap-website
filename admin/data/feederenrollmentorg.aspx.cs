﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_report_feederenrollmentorg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lblFormTitle.Text = "Feeder School enrollment for LEAs that HAVE converted (OPTION 1)";
            lblLabel.Text = "Enrollment";
            hfStageID.Value = "5";

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            hfGranteeID.Value = granteeID.ToString();

            int reportPeriod = 1;
            if (Session["ReportID"] != null)
            {
                reportPeriod = (int)Session["ReportID"];
                hfReportID.Value = reportPeriod.ToString();
            }
            else
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }

            //Feeder School
            foreach (MagnetFeederSchool school in MagnetFeederSchool.Find(x=>x.GranteeID == Convert.ToInt32(hfGranteeID.Value)).OrderBy(x=>x.SchoolName))
            {
                ddlFeederSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
            }

            //Magnet School
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == Convert.ToInt32(hfGranteeID.Value)).OrderBy(x => x.SchoolName))
            {
                ddlMagnetSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
            }

            LoadEnrollmentData();
        }
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetSchoolEnrollment data = MagnetSchoolEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        ddlFeederSchool.SelectedValue = data.FeederSchoolID.ToString();
        ddlMagnetSchool.SelectedValue = data.SchoolID.ToString();
        txtAmericanIndian.Text = data.AmericanIndian.ToString();
        txtAsian.Text = data.Asian.ToString();
        txtBlack.Text = data.AfricanAmerican.ToString();
        txtHispanic.Text = data.Hispanic.ToString();
        txtWhite.Text = data.White.ToString();
        mpeResourceWindow.Show();
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetSchoolEnrollment.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadEnrollmentData();
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        MagnetSchoolEnrollment data = new MagnetSchoolEnrollment();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetSchoolEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            data.GranteeReportID = Convert.ToInt32(hfReportID.Value);
            data.StageID = Convert.ToInt32(hfStageID.Value);
        }
        data.FeederSchoolID = Convert.ToInt32(ddlFeederSchool.SelectedValue);
        data.SchoolID = Convert.ToInt32(ddlMagnetSchool.SelectedValue);
        data.AmericanIndian = string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtAmericanIndian.Text));
        data.Asian = string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtAsian.Text));
        data.AfricanAmerican = string.IsNullOrEmpty(txtBlack.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtBlack.Text));
        data.Hispanic = string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtHispanic.Text));
        data.White = string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtWhite.Text));
        data.Hawaiian = 0;
        data.MultiRacial = 0;
        data.Save();
        LoadEnrollmentData();
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeResourceWindow.Show();
    }
    private void ClearFields()
    {
        hfID.Value = "";
        ddlFeederSchool.SelectedIndex = 0;
        ddlMagnetSchool.SelectedIndex = 0;
        txtAmericanIndian.Text = "";
        txtAsian.Text = "";
        txtBlack.Text = "";
        txtHispanic.Text = "";
        txtWhite.Text = "";
        lblMessage.Text = "";
    }
    private void LoadEnrollmentData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetSchoolEnrollments
                   from n in db.MagnetSchools
                   from p in db.MagnetFeederSchools
                   where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                    && m.StageID == Convert.ToInt32(hfStageID.Value)
                    && m.FeederSchoolID == p.ID
                    && m.SchoolID == n.ID
                   orderby m.GradeLevel
                   select new { m.ID, FeederSchool=p.SchoolName, MagnetSchool=n.SchoolName, m.AmericanIndian, m.Asian, m.AfricanAmerican, m.Hispanic, m.White};
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}