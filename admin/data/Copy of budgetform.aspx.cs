﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_data_budgetform : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            LoadData();

            FileUpload1.Attributes.Add("onchange", "return validateFileUpload(this);");
            UploadBtn.Attributes.Add("onclick", "return validateFileUpload(document.getElementById('" + FileUpload1.ClientID + "'));");
            foreach(System.Web.UI.WebControls.ListItem li in rblRageAggrement.Items)
                li.Attributes.Add("onClick", "Javascript:PreventConfirmation();");
            foreach (System.Web.UI.WebControls.ListItem li in rblAgency.Items)
                li.Attributes.Add("onClick", "Javascript:PreventConfirmation();");
            foreach (System.Web.UI.WebControls.ListItem li in rblRageAggrement.Items)
                li.Attributes.Add("onClick", "Javascript:PreventConfirmation();");
            foreach (System.Web.UI.WebControls.ListItem li in rblBudgetChange.Items)
                li.Attributes.Add("onClick", "Javascript:PreventConfirmation();");
        }
    }
    private void LoadData()
    {
        var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));
        if (data.Count > 0)
        {
            hfID.Value = data[0].ID.ToString();
            txtPersonnel.Text = Convert.ToString(data[0].Personnel);
            txtPersonnelNF.Text = Convert.ToString(data[0].PersonnelNF);
            txtFringe.Text = Convert.ToString(data[0].Fringe);
            txtFringeNF.Text = Convert.ToString(data[0].FringeNF);
            txtTravel.Text = Convert.ToString(data[0].Travel);
            txtTravelNF.Text = Convert.ToString(data[0].TravelNF);
            txtEquipment.Text = Convert.ToString(data[0].Equipment);
            txtEquipmentNF.Text = Convert.ToString(data[0].EquipmentNF);
            txtSupplies.Text = Convert.ToString(data[0].Supplies);
            txtSuppliesNF.Text = Convert.ToString(data[0].SuppliesNF);
            txtContractual.Text = Convert.ToString(data[0].Contractual);
            txtContractualNF.Text = Convert.ToString(data[0].ContractualNF);
            txtConstruction.Text = Convert.ToString(data[0].Construction);
            txtConstructionNF.Text = Convert.ToString(data[0].ConstructionNF);
            txtOther.Text = Convert.ToString(data[0].Other);
            txtOtherNF.Text = Convert.ToString(data[0].OtherNF);
            txtIndirectCosts.Text = Convert.ToString(data[0].IndirectCost);
            txtIndirectCostsNF.Text = Convert.ToString(data[0].IndirectCostNF);
            txtTrainingStipends.Text = Convert.ToString(data[0].TrainingStipends);
            txtTrainingStipendsNF.Text = Convert.ToString(data[0].TrainingStipendsNF);
            decimal Total = 0;
            Total = (decimal)(data[0].Personnel + data[0].Fringe + data[0].Travel + data[0].Equipment + data[0].Supplies + data[0].Contractual + data[0].Construction + data[0].Other);
            decimal TotalNF = 0;
            TotalNF = (decimal)(data[0].PersonnelNF + data[0].FringeNF + data[0].TravelNF + data[0].EquipmentNF + data[0].SuppliesNF + data[0].ContractualNF + data[0].ConstructionNF + data[0].OtherNF);
            txtDirectCosts.Text = "$" + Total.ToString("#,#.00#"); //Convert.ToString(Total);
            txtDirectCostsNF.Text = "$" + TotalNF.ToString("#,#.00#");
            Total += (decimal)data[0].IndirectCost + (decimal)data[0].TrainingStipends;
            TotalNF += (decimal)data[0].IndirectCostNF + (decimal)data[0].TrainingStipendsNF;
            txtTotalCosts.Text = "$" + Total.ToString("#,#.00#");
            txtTotalCostsNF.Text = "$" + TotalNF.ToString("#,#.00#");

            if (data[0].IndirectCostAgreement != null)
            {
                if ((bool)data[0].IndirectCostAgreement)
                {
                    rblRageAggrement.SelectedIndex = 0;
                    if (!string.IsNullOrEmpty(data[0].AgreementFrom))
                        //txtAgreementFrom.SelectedDate = Convert.ToDateTime(data[0].AgreementFrom);
                        txtAgreementFrom.Text = data[0].AgreementFrom;
                    if (!string.IsNullOrEmpty(data[0].AgreementTo))
                        //txtAgreementTo.SelectedDate = Convert.ToDateTime(data[0].AgreementTo);
                        txtAgreementTo.Text = data[0].AgreementTo;
                    if (data[0].ApprovalAgency != null)
                    {
                        if ((bool)data[0].ApprovalAgency)
                        {
                            rblAgency.SelectedIndex = 0;
                            agencyPanel.Visible = false;
                        }
                        else
                        {
                            rblAgency.SelectedIndex = 1;
                            agencyPanel.Visible = true;
                            txtOtherAgency.Text = data[0].OtherAgency;
                        }
                    }
                    else
                        agencyPanel.Visible = false;
                    txtRate.Text = Convert.ToString(data[0].IndirectCostRate);
                }
                else
                {
                    SetAgreement(false);
                    rblRageAggrement.SelectedIndex = 1;
                    txtAgreementFrom.Enabled = false;
                    txtAgreementTo.Enabled = false;
                    rblAgency.ClearSelection();
                    txtOtherAgency.Enabled = false;
                    txtRate.Enabled = false;
                }
            }
            if (data[0].RestrictedRateProgram != null)
            {
                if ((bool)data[0].RestrictedRateProgram)
                    rbIncludedInAgreement.Checked = true;
                else
                    rb34CFR.Checked = true;
            }
            if (data[0].BudgetChange != null)
            {
                if (data[0].BudgetChange == true)
                {
                    rblBudgetChange.SelectedIndex = 1;
                    txtBudgetReason.Text = data[0].BudgetReason;
                }
                else
                {
                    rblBudgetChange.SelectedIndex = 0;
                    txtBudgetReason.Enabled = false;
                }
            }
            txtRICRate.Text = Convert.ToString(data[0].RestrictedRate);
            txtBudgetSummary.Text = data[0].BudgetSummary;
        }

        //Uploaded file
        var files = MagnetUpload.Find(x => x.ProjectID == (int)Session["ReportID"] && x.FormID == 13);
        if (files.Count > 0)
        {
            fileRow.Visible = true;
            hfFileID.Value = files[0].ID.ToString();
            lblFile.Text = files[0].FileName;
            UploadedFile.HRef = "../upload/" + files[0].PhysicalName;
            UploadBtn.Enabled = false;
            FileUpload1.Enabled = false;
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        SaveData();
        LoadData();
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Data has been saved.');</script>", false);
    }
    private void SaveData()
    {
        MagnetBudgetInformation Budget = new MagnetBudgetInformation();
        if (!string.IsNullOrEmpty(hfID.Value))
            Budget = MagnetBudgetInformation.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            Budget.GranteeReportID = Convert.ToInt32(hfReportID.Value);
        }
        Budget.Personnel = string.IsNullOrEmpty(txtPersonnel.Text) ? 0 : Convert.ToDecimal(txtPersonnel.Text);
        Budget.PersonnelNF = string.IsNullOrEmpty(txtPersonnelNF.Text) ? 0 : Convert.ToDecimal(txtPersonnelNF.Text);
        Budget.Fringe = string.IsNullOrEmpty(txtFringe.Text) ? 0 : Convert.ToDecimal(txtFringe.Text);
        Budget.FringeNF = string.IsNullOrEmpty(txtFringeNF.Text) ? 0 : Convert.ToDecimal(txtFringeNF.Text);
        Budget.Travel = string.IsNullOrEmpty(txtTravel.Text) ? 0 : Convert.ToDecimal(txtTravel.Text);
        Budget.TravelNF = string.IsNullOrEmpty(txtTravelNF.Text) ? 0 : Convert.ToDecimal(txtTravelNF.Text);
        Budget.Equipment = string.IsNullOrEmpty(txtEquipment.Text) ? 0 : Convert.ToDecimal(txtEquipment.Text);
        Budget.EquipmentNF = string.IsNullOrEmpty(txtEquipmentNF.Text) ? 0 : Convert.ToDecimal(txtEquipmentNF.Text);
        Budget.Supplies = string.IsNullOrEmpty(txtSupplies.Text) ? 0 : Convert.ToDecimal(txtSupplies.Text);
        Budget.SuppliesNF = string.IsNullOrEmpty(txtSuppliesNF.Text) ? 0 : Convert.ToDecimal(txtSuppliesNF.Text);
        Budget.Contractual = string.IsNullOrEmpty(txtContractual.Text) ? 0 : Convert.ToDecimal(txtContractual.Text);
        Budget.ContractualNF = string.IsNullOrEmpty(txtContractualNF.Text) ? 0 : Convert.ToDecimal(txtContractualNF.Text);
        Budget.Construction = string.IsNullOrEmpty(txtConstruction.Text) ? 0 : Convert.ToDecimal(txtConstruction.Text);
        Budget.ConstructionNF = string.IsNullOrEmpty(txtConstructionNF.Text) ? 0 : Convert.ToDecimal(txtConstructionNF.Text);
        Budget.Other = string.IsNullOrEmpty(txtOther.Text) ? 0 : Convert.ToDecimal(txtOther.Text);
        Budget.OtherNF = string.IsNullOrEmpty(txtOtherNF.Text) ? 0 : Convert.ToDecimal(txtOtherNF.Text);
        decimal dFederal = (decimal)(Budget.Personnel + Budget.Fringe + Budget.Travel + Budget.Equipment +
            Budget.Supplies + Budget.Contractual + Budget.Construction + Budget.Other);
        decimal dFederalNF = (decimal)(Budget.PersonnelNF + Budget.FringeNF + Budget.TravelNF + Budget.EquipmentNF +
            Budget.SuppliesNF + Budget.ContractualNF + Budget.ConstructionNF + Budget.OtherNF);
        txtDirectCosts.Text = Convert.ToString(dFederal);
        txtDirectCostsNF.Text = Convert.ToString(dFederalNF);
        Budget.IndirectCost = string.IsNullOrEmpty(txtIndirectCosts.Text) ? 0 : Convert.ToDecimal(txtIndirectCosts.Text);
        Budget.IndirectCostNF = string.IsNullOrEmpty(txtIndirectCostsNF.Text) ? 0 : Convert.ToDecimal(txtIndirectCostsNF.Text);
        Budget.TrainingStipends = string.IsNullOrEmpty(txtTrainingStipends.Text) ? 0 : Convert.ToDecimal(txtTrainingStipends.Text);
        Budget.TrainingStipendsNF = string.IsNullOrEmpty(txtTrainingStipendsNF.Text) ? 0 : Convert.ToDecimal(txtTrainingStipendsNF.Text);
        dFederal +=  (decimal)(Budget.IndirectCost + Budget.TrainingStipends);
        dFederalNF +=  (decimal)(Budget.IndirectCostNF+Budget.TrainingStipendsNF);
        txtTotalCosts.Text = Convert.ToString(dFederal);
        txtTotalCostsNF.Text = Convert.ToString(dFederalNF);
        if(rblRageAggrement.SelectedIndex == 0)
        {
            //yes
            Budget.IndirectCostAgreement = true;
            Budget.AgreementFrom = txtAgreementFrom.Text;
            Budget.AgreementTo = txtAgreementTo.Text;
            if (rblAgency.SelectedIndex == 0)
            {
                Budget.ApprovalAgency = true;
            }
            if (rblAgency.SelectedIndex == 1)
            {
                Budget.ApprovalAgency = false;
                Budget.OtherAgency = txtOtherAgency.Text;
            }
            if (!string.IsNullOrEmpty(txtRate.Text)) Budget.IndirectCostRate = Convert.ToDecimal(txtRate.Text);
        }
        else
        {
            Budget.IndirectCostAgreement = false;
        }
        if (rbIncludedInAgreement.Checked) Budget.RestrictedRateProgram = true;
        if (rb34CFR.Checked) Budget.RestrictedRateProgram = false;
        if (!string.IsNullOrEmpty(txtRICRate.Text)) Budget.RestrictedRate = Convert.ToDecimal(txtRICRate.Text);

        if (!string.IsNullOrEmpty(rblBudgetChange.SelectedValue))
        {
            if (rblBudgetChange.SelectedIndex == 0)
            {
                Budget.BudgetChange = false;
            }
            else
            {
                Budget.BudgetChange = true;
                Budget.BudgetReason = txtBudgetReason.Text;
            }
        }

        Budget.BudgetSummary = txtBudgetSummary.Text;
        Budget.Save();
        if (string.IsNullOrEmpty(hfID.Value))
            hfID.Value = Budget.ID.ToString();

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 4);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 4;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
    }
    protected void OnBudgeChanged(object sender, EventArgs e)
    {
        if (rblBudgetChange.SelectedIndex == 0)
        {
            txtBudgetReason.Enabled = false;
            txtBudgetReason.Text = "";
        }
        else
            txtBudgetReason.Enabled = true;
    }
    protected void OnAgencyChanged(object sender, EventArgs e)
    {
        if (rblAgency.SelectedIndex == 0)
            agencyPanel.Visible = false;
        else
            agencyPanel.Visible = true;
    }
    protected void OnAgreementYes(object sender, EventArgs e)
    {
        if(rblRageAggrement.SelectedIndex == 0)
            SetAgreement(true);
        else
            SetAgreement(false);
    }
    private void SetAgreement(bool value)
    {
        txtAgreementFrom.Enabled = value;
        if (!value) txtAgreementFrom.Text = "";//txtAgreementFrom.Clear();
        txtAgreementTo.Enabled = value;
        if (!value) txtAgreementTo.Text = ""; //txtAgreementTo.Clear();
        rblAgency.Enabled = value;
        if (!value) rblAgency.ClearSelection();
        txtOtherAgency.Enabled = value;
        if (!value) txtOtherAgency.Text = "";
        txtRate.Enabled = value;
        agencyPanel.Visible = value;
        if (!value) txtRate.Text = "";
        RequiredFieldValidator2.Enabled = value;
        RequiredFieldValidator3.Enabled = value;
        RequiredFieldValidator4.Enabled = value;
        //RequiredFieldValidator5.Enabled = value;
    }
    protected void OnUpload(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile && FileUpload1.FileName.EndsWith(".pdf"))
        {
            MagnetUpload ExecutiveSummary = new MagnetUpload();
            ExecutiveSummary.ProjectID = Convert.ToInt32(hfReportID.Value);
            ExecutiveSummary.FormID = 13; //Budge narrative
            ExecutiveSummary.FileName = FileUpload1.FileName.Replace('#', ' ');
            string tmp = System.Guid.NewGuid().ToString() + "-" + FileUpload1.FileName.Replace('#',' ');
            ExecutiveSummary.PhysicalName = tmp;
            FileUpload1.SaveAs(Server.MapPath("") + "/../upload/" + tmp);
            ExecutiveSummary.UploadDate = DateTime.Now;
            ExecutiveSummary.Save();

            fileRow.Visible = true;
            hfFileID.Value = ExecutiveSummary.ID.ToString();
            lblFile.Text = FileUpload1.FileName;
            UploadedFile.HRef = "../upload/" + tmp;

            UploadBtn.Enabled = false;
            FileUpload1.Enabled = false;

            var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 4);
            if (muhs.Count > 0)
            {
                muhs[0].UpdateUser = Context.User.Identity.Name;
                muhs[0].TimeStamp = DateTime.Now;
                muhs[0].Save();
            }
            else
            {
                MagnetUpdateHistory muh = new MagnetUpdateHistory();
                muh.ReportID = Convert.ToInt32(hfReportID.Value);
                muh.FormIndex = 4;
                muh.UpdateUser = Context.User.Identity.Name;
                muh.TimeStamp = DateTime.Now;
                muh.Save();
            }
        }
        else
        {
            //FileUpload1.
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfFileID.Value))
        {
            try
            {
                fileRow.Visible = false;
                MagnetUpload ExecutiveSummary = MagnetUpload.SingleOrDefault(x => x.ID == Convert.ToInt32(hfFileID.Value));
                System.IO.File.Delete(Server.MapPath("") + "/../upload/" + ExecutiveSummary.PhysicalName);
                ExecutiveSummary.Delete();
                hfFileID.Value = "";
                UploadBtn.Enabled = true;
                FileUpload1.Enabled = true;
            }
            catch (Exception ex) { }
        }
        LoadData();
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        SaveData();
        LoadData();
        //List of grantee report
        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        MagnetGrantee Grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);
        using (System.IO.MemoryStream output = new MemoryStream())
        {
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, output);
            document.Open();
            PdfContentByte pdfContentByte = pdfWriter.DirectContent;

            List<string> TmpPDFs = new List<string>();

            try
            {
                PdfStamper ps = null;

                // read existing PDF document
                string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                if (File.Exists(TmpPDF))
                    File.Delete(TmpPDF);
                TmpPDFs.Add(TmpPDF);

                PdfReader r = new PdfReader(Server.MapPath("../doc/ed524budget.pdf"));
                ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                AcroFields af = ps.AcroFields;

                decimal[] BudgetTotal = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                decimal ColumnTotal = 0;
                decimal ColumnTotalNonFederal = 0;
                string[] affix = { "a", "b", "c", "d", "e", "f" };

                int idx = 1;
                int cnn = 1;
                int ReportID = 0;
                string summary = "";
                while (idx < 6)
                {
                    //Each grant year has two reports, use ad hoc report if it's ready
                    var adhocReports = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == cnn + 1 && x.ReportType == false);
                    var reports = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == cnn && x.ReportType == false);

                    if (reports.Count > 0 || adhocReports.Count > 0)
                    {
                        GranteeReport report;
                        MagnetBudgetInformation MBI;
                        if (adhocReports.Count > 0)
                        {
                            var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == adhocReports[0].ID);
                            MBI = data[0];
                            report = adhocReports[0];
                        }
                        else
                        {
                            var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == reports[0].ID);
                            MBI = data[0];
                            report = reports[0];
                        }
                        if (MBI != null)
                        {
                            //ReportID = report.ID;
                            ColumnTotal = 0;
                            ColumnTotalNonFederal = 0;
                            decimal dd = 0;

                            summary = MBI.BudgetSummary;                            
                            //Personnel
                            af.SetField("1" + affix[idx - cnn] + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.Personnel));
                            ColumnTotal += (decimal)MBI.Personnel;
                            BudgetTotal[0] += (decimal)MBI.Personnel;
                            af.SetField("B" + cnn.ToString() + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.PersonnelNF));
                            ColumnTotalNonFederal += (decimal)MBI.PersonnelNF;
                            BudgetTotal[13] += (decimal)MBI.PersonnelNF;

                            //Fringe Benefits
                            dd = (decimal)MBI.Fringe;
                            af.SetField("2" + affix[idx - cnn] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[1] += dd;
                            dd = (decimal)MBI.FringeNF;
                            af.SetField("B" + cnn.ToString() + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[14] += dd;

                            //Travel
                            dd = (decimal)MBI.Travel;
                            af.SetField("3" + affix[idx - cnn] + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[2] += dd;
                            dd = (decimal)MBI.TravelNF;
                            af.SetField("B" + cnn.ToString() + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[15] += dd;

                            //Equipment
                            dd = (decimal)MBI.Equipment;
                            af.SetField("4" + affix[idx - cnn] + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[3] += dd;
                            dd = (decimal)MBI.EquipmentNF;
                            af.SetField("B" + cnn.ToString() + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[16] += dd;

                            //Supplies
                            dd = (decimal)MBI.Supplies;
                            af.SetField("5" + affix[idx - cnn] + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[4] += dd;
                            dd = (decimal)MBI.SuppliesNF;
                            af.SetField("B" + cnn.ToString() + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[17] += dd;

                            //contractual
                            dd = (decimal)MBI.Contractual;
                            af.SetField("6" + affix[idx - cnn] + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[5] += dd;
                            dd = (decimal)MBI.ContractualNF;
                            af.SetField("B" + cnn.ToString() + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[18] += dd;

                            //Construction
                            dd = (decimal)MBI.Construction;
                            af.SetField("7" + affix[idx - cnn] + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[6] += dd;
                            dd = (decimal)MBI.ConstructionNF;
                            af.SetField("B" + cnn.ToString() + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[19] += dd;

                            //Other
                            dd = (decimal)MBI.Other;
                            af.SetField("8" + affix[idx - cnn] + " Other", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[7] += dd;
                            dd = (decimal)MBI.OtherNF;
                            af.SetField("B" + cnn.ToString() + " Other", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[20] += dd;

                            //Total Direct Costs
                            if (ColumnTotal.CompareTo((decimal)0.0) >= 0)
                            {
                                af.SetField("9" + affix[idx - cnn] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                BudgetTotal[8] += ColumnTotal;
                            }

                            if (ColumnTotalNonFederal.CompareTo((decimal)0.0) >= 0)
                            {
                                af.SetField("B" + cnn.ToString() + " Total Direct Costs", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                BudgetTotal[21] += ColumnTotalNonFederal;
                            }

                            dd = (decimal)MBI.IndirectCost;
                            af.SetField("10" + affix[idx - cnn] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[9] += dd;
                            dd = (decimal)MBI.IndirectCostNF;
                            af.SetField("B" + cnn.ToString() + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[22] += dd;

                            dd = (decimal)MBI.TrainingStipends;
                            af.SetField("11" + affix[idx - cnn] + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotal += dd;
                            BudgetTotal[10] += dd;
                            dd = (decimal)MBI.TrainingStipendsNF;
                            af.SetField("B" + cnn.ToString() + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                            ColumnTotalNonFederal += dd;
                            BudgetTotal[23] += dd;

                            //Total Costs
                            af.SetField("12" + affix[idx - cnn] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                            BudgetTotal[11] += ColumnTotal;

                            af.SetField("B" + cnn.ToString() + " Total Costs Lines", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                            BudgetTotal[24] += ColumnTotalNonFederal;

                            //Curent report period?
                            if (cnn == GranteeReport.SingleOrDefault(x=>x.ID == Convert.ToInt32(hfReportID.Value)).ReportPeriodID)
                            {
                                if (MBI.IndirectCostAgreement != null)
                                {
                                    if ((bool)MBI.IndirectCostAgreement)
                                    {
                                        af.SetField("AgreementYes", "Yes");
                                        if (!string.IsNullOrEmpty(MBI.AgreementFrom))
                                        {
                                            string[] strs = MBI.AgreementFrom.Split('/');
                                            af.SetField("PeriodFrom1", strs[0]);
                                            af.SetField("PeriodFrom2", strs.Count() >= 2 ? strs[1] : "");
                                            af.SetField("PeriodFrom3", strs.Count() >= 3 ? strs[2] : "");
                                        }
                                        if (!string.IsNullOrEmpty(MBI.AgreementTo))
                                        {
                                            string[] strs = MBI.AgreementTo.Split('/');
                                            af.SetField("PeriodTo1", strs[0]);
                                            af.SetField("PeriodTo2", strs.Count() >= 2 ? strs[1] : "");
                                            af.SetField("PeriodTo3", strs.Count() >= 3 ? strs[2] : "");
                                        }
                                        if (MBI.ApprovalAgency != null)
                                        {
                                            if ((bool)MBI.ApprovalAgency)
                                                af.SetField("Ed", "Yes");
                                            else
                                            {
                                                af.SetField("Other", "Yes");
                                                af.SetField("Other approving agency", MBI.OtherAgency);
                                            }
                                        }
                                        af.SetField("Indirect Cost Rate Percentage", Convert.ToString(MBI.IndirectCostRate));
                                    }
                                    else
                                        af.SetField("AgreementNo", "Yes");
                                }

                                if (MBI.RestrictedRateProgram != null)
                                {
                                    if ((bool)MBI.RestrictedRateProgram)
                                        af.SetField("Is it included in ICRA", "Yes");
                                    else
                                        af.SetField("Complies with 34 CFR", "Yes");
                                }
                                af.SetField("Restricted Indirect Cost Rate Percentage", Convert.ToString(MBI.RestrictedRate));
                            }
                        }
                    }

                    idx += 2;
                    cnn++;
                }

                //Total expense
                idx = 6;
                af.SetField("1" + affix[idx - 1] + " Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[0]));
                af.SetField("2" + affix[idx - 1] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[1]));
                af.SetField("3" + affix[idx - 1] + " Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[2]));
                af.SetField("4" + affix[idx - 1] + " Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[3]));
                af.SetField("5" + affix[idx - 1] + " Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[4]));
                af.SetField("6" + affix[idx - 1] + " Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[5]));
                af.SetField("7" + affix[idx - 1] + " Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[6]));
                af.SetField("8" + affix[idx - 1] + " Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[7]));
                af.SetField("9" + affix[idx - 1] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", BudgetTotal[8]));
                af.SetField("10" + affix[idx - 1] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[9]));
                af.SetField("11" + affix[idx - 1] + " Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[10]));
                af.SetField("12" + affix[idx - 1] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", BudgetTotal[11]));

                //Indirect Cost Information from lastest cover sheet
                af.SetField("B6 Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[13]));
                af.SetField("B6 Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[14]));
                af.SetField("B6 Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[15]));
                af.SetField("B6 Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[16]));
                af.SetField("B6 Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[17]));
                af.SetField("B6 Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[18]));
                af.SetField("B6 Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[19]));
                af.SetField("B6 Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[20]));
                af.SetField("B6 Total Direct Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[21]));
                af.SetField("B6 Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[22]));
                af.SetField("B6 Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[23]));
                af.SetField("B6 Total Costs Lines", "$" + string.Format("{0:#,0.00}", BudgetTotal[24]));

                af.SetField("InstitutionOrganization", Grantee.GranteeName);

                ps.FormFlattening = true;

                r.Close();
                ps.Close();

                //Add to final report
                PdfReader reader = new PdfReader(TmpPDF);
                document.SetPageSize(PageSize.A4.Rotate());
                document.NewPage();
                PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                pdfContentByte.AddTemplate(importedPage, 0, 0);
                document.NewPage();
                PdfImportedPage importedPage2 = pdfWriter.GetImportedPage(reader, 2);
                pdfContentByte.AddTemplate(importedPage2, 0, 0);

                if (!string.IsNullOrEmpty(summary))
                {
                    //Add budget summary
                    string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                    if (File.Exists(ContentPDF))
                        File.Delete(ContentPDF);
                    TmpPDFs.Add(ContentPDF);

                    Document ContentDocument = new Document(PageSize.A4.Rotate(), 40f, 40f, 40f, 40f);
                    PdfWriter ContentWriter = PdfWriter.GetInstance(ContentDocument, new FileStream(ContentPDF, FileMode.Create));
                    ContentDocument.Open();
                    //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                    ContentDocument.Add(new Paragraph(new Chunk(summary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                    ContentDocument.Close();

                    //Add to output
                    PdfReader freader = new PdfReader(ContentPDF);
                    for (int j = 1; j <= freader.NumberOfPages; j++)
                    {
                        document.SetPageSize(PageSize.A4.Rotate());
                        document.NewPage();
                        PdfImportedPage fimportedPage = pdfWriter.GetImportedPage(freader, j);
                        pdfContentByte.AddTemplate(fimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                    }
                    freader.Close();
                }

                document.NewPage();
                PdfImportedPage importedPage3 = pdfWriter.GetImportedPage(reader, 3);
                pdfContentByte.AddTemplate(importedPage3, 0, 0);
                reader.Close();


            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("Budget report:", ex);
            }
            finally
            {
            }

            document.Close();
            foreach (string str in TmpPDFs)
            {
                if (File.Exists(str))
                    File.Delete(str);
            }

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Grantee.GranteeName.Replace(' ','_') + "_Budget_Summary.pdf");
            Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            output.Close();
        }
    }
}