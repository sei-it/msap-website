﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_otherbudget : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/default.aspx");
            }
            if (Request.QueryString["type"].Equals("1"))
            {
                hfReportType.Value = "15";
                lblBudgetType.Text = "Federal Funds";
            }
            else
            {
                hfReportType.Value = "16";
                lblBudgetType.Text = "Non Federal Funds";
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == Convert.ToInt32(hfReportType.Value));
            if (data.Count == 1)
            {
                hfSummaryID.Value = data[0].ID.ToString();
                txtSummary.Text = data[0].ReportSummary;
            }
            RegisterHelper();
            LoadData();
        }
    }
    private void RegisterHelper()
    {
        GridViewHelper helper = new GridViewHelper(this.GridView1);
        helper.RegisterSummary("ApprovedFederalFunds", SummaryOperation.Sum);
        helper.RegisterSummary("BudgetExpenditures", SummaryOperation.Sum);
        helper.RegisterSummary("Carryover", SummaryOperation.Sum);
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetOtherBudgets
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.ReportType == Convert.ToInt32(hfReportType.Value)
                   orderby m.Item
                   select new
                   {
                       m.ID,
                       m.Item,
                       UnitPrice = m.RateType == null?0:(bool)m.RateType == false?m.Rate:0,
                       NumberOfUnits = m.RateType == null ? 0 : (bool)m.RateType == false ? m.Quantity : 0,
                       MonthlyRate = m.RateType == null ? 0 : (bool)m.RateType == true ? m.Rate : 0,
                       NumberOfMonths = m.RateType == null ? 0 : (bool)m.RateType == true ? m.Quantity : 0,
                       m.ApprovedFederalFunds,
                       BudgetExpenditures = (m.Rate != null ? Convert.ToInt32(m.Rate * m.Quantity) : 0),
                       Carryover = (m.Rate != null ? Convert.ToInt32(m.ApprovedFederalFunds - m.Rate * m.Quantity) : 0)
                   };
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        rblRateType.SelectedIndex = 0;
        txtRate.Text = "";
        txtQuantity.Text = "";
        txtItem.Text = "";
        txtApprovedFunds.Text = "";
        hfID.Value = "";
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetOtherBudget data = MagnetOtherBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        rblRateType.SelectedIndex = Convert.ToInt32(data.RateType);
        txtRate.Text = data.Rate.ToString();
        txtQuantity.Text = data.Quantity.ToString();
        txtItem.Text = data.Item;
        if (data.ApprovedFederalFunds != null) txtApprovedFunds.Text = Convert.ToString(data.ApprovedFederalFunds);
        mpeWindow.Show();
    }
    protected void OnNewBudget(object sender, EventArgs e)
    {
        ClearFields();
        mpeWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetOtherBudget data = new MagnetOtherBudget();
        if (!string.IsNullOrEmpty(hfID.Value))
            data = MagnetOtherBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
            data.ReportType = Convert.ToInt32(hfReportType.Value);
        }
        data.ApprovedFederalFunds = string.IsNullOrEmpty(txtApprovedFunds.Text) ? 0 : Convert.ToInt32(txtApprovedFunds.Text);
        data.Item = txtItem.Text;
        data.Rate = string.IsNullOrEmpty(txtRate.Text) ? 0 : Convert.ToDecimal(txtRate.Text);
        data.Quantity = string.IsNullOrEmpty(txtQuantity.Text) ? 0 : Convert.ToInt32(txtQuantity.Text);
        data.RateType = rblRateType.SelectedIndex == 0 ? false : true;
        data.Save();
        RegisterHelper();
        LoadData();
    }
    protected void OnSaveSummary(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSummary.Text))
        {
            //Save to database
            MagnetBudgetSummary summary = new MagnetBudgetSummary();
            if (!string.IsNullOrEmpty(hfSummaryID.Value))
                summary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSummaryID.Value));
            else
            {
                summary.ReportID = Convert.ToInt32(hfReportID.Value);
                summary.ReportType = Convert.ToInt32(hfReportType.Value);
            }
            summary.ReportSummary = txtSummary.Text;
            summary.Save();
        }
    }
}