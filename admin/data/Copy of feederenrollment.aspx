﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="Copy of feederenrollment.aspx.cs" Inherits="admin_report_feederenrollment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP center - Feeder School Enrollment data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Table 11: Feeder School</h4>
    <br />
    <div class="tab_area">
        <div class="titles">
            Table 11: Feeder School
        </div>
        <ul class="tabs">
            <li class="tab_active">Table 11</li>
            <li><a href="enrollment.aspx">Table 9</a></li>
            <li><a href="manageschoolyear.aspx">Table 8</a></li>
            <li><a href="leaenrollment.aspx">Table 7</a></li>
        </ul>
    </div>
    <br />
    <p>
        For each feeder school, identify the magnet school(s) to which the feeder school
        would send students. If a feeder school would send students to all magnet schools
        at a particular grade level (for example, Elementary Feeder School “X” would send
        students to all of the elementary magnet schools participating in the project, indicate
        “All” in the “Magnet” column associated with Elementary Feeder School “X”).</p>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfSchoolID" runat="server" />
    <asp:HiddenField ID="hfStageID" runat="server" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        GridLines="None" PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID"
        CssClass="msapDataTbl CenterContent">
        <Columns>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    &nbsp;<br />
                    <br />
                    Feeder School
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("FeederSchool")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    &nbsp;<br />
                    <br />
                    Magnet School
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval( "MagnetSchool" )  %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    American Indian/Alaskan Native
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("AmericanIndian"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    Asian/Pacific Islander
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("Asian"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    Black or African American
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("AfricanAmerican"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    <br />
                    Hispanic/Latin
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("Hispanic"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    <br />
                    White
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("White"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" />
                    Native Hawaiian or Other Pacific Islander
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("Hawaiian"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="11%">
                <HeaderTemplate>
                    <img src="../../images/head_button.jpg" /><br />
                    <br />
                    <br />
                    Two or more races
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Synergy.Magnet.ManageUtility.FormatInteger((int)Eval("MultiRacial"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle CssClass="TDWithBottomNoRightBorder" />
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <%-- Resource --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Enrollment Data</div>
            <table class="mpeModalPopup_Tbl" style="padding-top: 15px;">
                <tr>
                    <td>
                        Feeder School:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlFeederSchool" runat="server" CssClass="msapDataTxt">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Magnet School:
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="cklMagnetSchool" runat="server" CssClass="msapDataTxt" AutoPostBack="true"
                                    OnSelectedIndexChanged="OnMagnetSchoolChanged" Height="100">
                                </asp:CheckBoxList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        American Indian/Alaskan Native:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmericanIndian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                            TargetControlID="txtAmericanIndian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Asian/Pacific Islander:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAsian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtAsian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Black or African American:
                    </td>
                    <td>
                        <asp:TextBox ID="txtBlack" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                            TargetControlID="txtBlack">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Hispanic/Latino:
                    </td>
                    <td>
                        <asp:TextBox ID="txtHispanic" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                            TargetControlID="txtHispanic">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        White:
                    </td>
                    <td>
                        <asp:TextBox ID="txtWhite" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                            TargetControlID="txtWhite">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Native Hawaiian or Other Pacific Islander:
                    </td>
                    <td>
                        <asp:TextBox ID="txtHawaiian" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                            TargetControlID="txtHawaiian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        Two or more races:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMultiRaces" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                            TargetControlID="txtMultiRaces">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        &nbsp;<asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="Button14" runat="server" CssClass="surveyBtn2" Text="Save Record"
                            OnClick="OnSaveData" />&nbsp;&nbsp;
                        <asp:Button ID="Button15" runat="server" CssClass="surveyBtn2" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
    <br />
    <br />
    <div style="text-align: right;">
        <asp:Button ID="Button32" runat="server" Text="Add Enrollment Data" CssClass="surveyBtn2"
            OnClick="OnAdd" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Print" CssClass="surveyBtn" OnClick="OnPrint" />
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a href="leaenrollment.aspx">Table 7 - Enrollment Data-LEA Level</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="manageschoolyear.aspx">Table 8 - Year of Implementation</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="enrollment.aspx">Table 9 - Enrollment Data-Magnet Schools</a>&nbsp;&nbsp;&nbsp;&nbsp;
        Table 11 - Feeder School
    </div>
</asp:Content>
