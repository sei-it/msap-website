﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using System.Data;
using System.Collections;

public partial class admin_data_managegpra : System.Web.UI.Page
{
    int reportyear = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["ReportYear"]!=null)
            reportyear = Convert.ToInt32(Session["ReportYear"]);
        if (!Page.IsPostBack)
        {
            
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = ManageUtility.FormatInteger((int)Session["ReportID"]);
            LoadData();

            ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();
        }
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        int item18_Indian = 0, item19_Asian = 0,
            item20_Black = 0, item21_Hispanic = 0,
            item22_Hawaiian = 0, item23_White = 0,

            item26a_newIndian = 0, item26b_newAsian = 0,
            item26c_newBlack = 0, item26d_newHispanic = 0,
            item26e_newHawaiian = 0, item26f_newWhite = 0,

            item27a_ContIndian = 0, item27b_ContAsian = 0,
            item27c_ContBlack = 0, item27d_ContHispanic = 0,
            item27e_ContHawaiian = 0, item27f_ContWhite = 0,
            item28_total = 0;

        Hashtable htblImpStatus = new Hashtable();
        htblImpStatus.Add("8", "7_1");
        htblImpStatus.Add("4", "7_2");
        htblImpStatus.Add("23", "7_3");
        htblImpStatus.Add("2", "7_4");
        htblImpStatus.Add("5", "7_5");
        htblImpStatus.Add("6", "7_6");
        htblImpStatus.Add("7", "7_7");
        htblImpStatus.Add("1", "7_8");
        htblImpStatus.Add("9", "7_9");
        htblImpStatus.Add("10", "7_10");
        htblImpStatus.Add("11", "7_11");
        htblImpStatus.Add("12", "7_12");
        htblImpStatus.Add("13", "7_13");
        htblImpStatus.Add("14", "7_14");
        htblImpStatus.Add("15", "7_15");
        htblImpStatus.Add("16", "7_16");
        htblImpStatus.Add("17", "7_17");
        htblImpStatus.Add("18", "7_18");
        htblImpStatus.Add("19", "7_19");
        htblImpStatus.Add("20", "7_20");
        htblImpStatus.Add("21", "7_21");
        htblImpStatus.Add("22", "7_22");
        htblImpStatus.Add("3", "7_23");
        htblImpStatus.Add("24", "7_24");
        htblImpStatus.Add("25", "7_25");
        htblImpStatus.Add("26", "7_26");
        htblImpStatus.Add("27", "7_27");
        htblImpStatus.Add("28", "7_28");
        htblImpStatus.Add("29", "7_29");
        htblImpStatus.Add("30", "7_30");
        htblImpStatus.Add("31", "7_31");

        GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
        MagnetDBDB db = new MagnetDBDB();

        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        var schooldata = from n in db.MagnetSchools
                         where n.GranteeID == granteeID
                         && n.ReportYear == reportyear
                         && n.isActive
                         orderby n.SchoolName
                         select n;
        if (schooldata.Count() > 0)
        {

            using (System.IO.MemoryStream Output = new MemoryStream())
            {
                //Print
                Document document = new Document();
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, Output);
                document.Open();
                PdfContentByte pdfContentByte = pdfWriter.DirectContent;
                List<string> TmpPDFs = new List<string>();

                foreach (MagnetSchool itm in schooldata)
                {
                    var data = from rows in db.MagnetGPRAs
                               join school in db.MagnetSchools on
                               rows.SchoolID equals school.ID
                               where rows.ReportID == Convert.ToInt32(hfReportID.Value)
                               && school.ReportYear == reportyear
                               && school.isActive
                               && rows.SchoolID == itm.ID
                               orderby school.SchoolName
                               select rows;

                    item18_Indian = 0; item19_Asian = 0;
                    item20_Black = 0; item21_Hispanic = 0;
                    item22_Hawaiian = 0; item23_White = 0;

                    item26a_newIndian = 0; item26b_newAsian = 0;
                    item26c_newBlack = 0; item26d_newHispanic = 0;
                    item26e_newHawaiian = 0; item26f_newWhite = 0;

                    item27a_ContIndian = 0; item27b_ContAsian = 0;
                    item27c_ContBlack = 0; item27d_ContHispanic = 0;
                    item27e_ContHawaiian = 0; item27f_ContWhite = 0;
                    item28_total = 0;

                    if (data.Count() > 0)
                    {

                        foreach (MagnetGPRA GPRAData in data)
                        {
                            try
                            {
                                bool isHighSchool = false;
                                string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                if (File.Exists(TmpPDF))
                                    File.Delete(TmpPDF);
                                TmpPDFs.Add(TmpPDF);

                                PdfStamper ps = null;

                                // Fill out form
                                PdfReader r = null;
                                if (report.ReportPeriodID > 5)
                                {
                                    if (report.ReportPeriodID > 8)
                                        r = new PdfReader(Server.MapPath("../doc/newGPRA.pdf"));
                                    else
                                        r = new PdfReader(Server.MapPath("../doc/gpranew.pdf"));
                                }
                                else
                                {
                                    r = new PdfReader(Server.MapPath("../doc/gpra.pdf"));
                                }

                                ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                                AcroFields af = ps.AcroFields;

                                af.SetField("1 School name", MagnetSchool.SingleOrDefault(x => x.ID == GPRAData.SchoolID).SchoolName);
                                af.SetField("2 Grantee name", MagnetGrantee.SingleOrDefault(x => x.ID == (int)Session["GranteeID"]).GranteeName);
                                if (!string.IsNullOrEmpty(GPRAData.SchoolGrade))
                                {
                                    foreach (string str in GPRAData.SchoolGrade.Split(';'))
                                    {
                                        af.SetField("Grades" + str, "Yes");
                                        if (Convert.ToInt32(str) >= 11) //high school: 9 - 12 offset 2
                                            isHighSchool = true;
                                    }
                                }
                                if (GPRAData.ProgramType != null)
                                {
                                    if ((bool)GPRAData.ProgramType == false)
                                        af.SetField("4 whole school magnet", "Yes");
                                    else af.SetField("4 partial school magnet", "Yes");
                                }
                                if (GPRAData.TitleISchoolFunding != null)
                                {
                                    if ((bool)GPRAData.TitleISchoolFunding == true)
                                    {
                                        af.SetField("5 title 1 funded school", "Yes");
                                    }
                                    else af.SetField("5 non title 1 funded school", "Yes");
                                }
                                if (GPRAData.TitleISchoolFundingImprovement != null)
                                {
                                    if ((bool)GPRAData.TitleISchoolFundingImprovement == true)
                                    {
                                        af.SetField("6 Title 1 school improvement Yes", "Yes");
                                    }
                                    else
                                        af.SetField("6 Title 1 school improvement No", "Yes");
                                }

                                if (GPRAData.TitleISchoolFundingImprovementStatus != null)
                                {
                                    string skey = GPRAData.TitleISchoolFundingImprovementStatus.ToString();

                                    af.SetField(htblImpStatus[skey].ToString(), "Yes");


                                    if (GPRAData.StatusOtherDes != null)
                                        af.SetField("Other", GPRAData.StatusOtherDes);
                                }

                                if (GPRAData.PersistentlyLlowestAchievingSchool != null)
                                {
                                    if ((bool)GPRAData.PersistentlyLlowestAchievingSchool == true)
                                    {
                                        af.SetField("8 Persistently low achieving school Yes", "Yes");
                                    }
                                    else
                                        af.SetField("8 Persistently low achieving school No", "Yes");
                                }
                                if (GPRAData.SchoolImprovementGrant != null)
                                {
                                    if ((bool)GPRAData.SchoolImprovementGrant == true)
                                        af.SetField("9 SIG Funds Yes", "Yes");
                                    else
                                        af.SetField("9 SIG Funds No", "Yes");
                                }
                                if (GPRAData.FRPMPpercentage != null && !string.IsNullOrEmpty(GPRAData.FRPMPpercentage.Trim()))
                                    af.SetField("9apercent", GPRAData.FRPMPpercentage);

                                //Part II
                                int ddd = Convert.ToInt32(period.ReportPeriod.Substring(7, 2));
                                //af.SetField("Applicant_pool_date", "20" + period.ReportPeriod.Substring(7, 2) + " - " + Convert.ToString(ddd + 1));
                                //af.SetField("Enrollment_date", "20" + period.ReportPeriod.Substring(7, 2) + " - " + Convert.ToString(ddd + 1));
                                af.SetField("Applicant_pool_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));
                                af.SetField("Enrollment_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));

                                var ApplicationPools = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 1 && x.MagnetGPRAID == GPRAData.ID);
                                if (ApplicationPools.Count > 0)
                                {
                                    if (ApplicationPools[0].Total != null) af.SetField("10", ManageUtility.FormatInteger((int)ApplicationPools[0].Total));
                                    if (ApplicationPools[0].Indian != null && ApplicationPools[0].Indian >= 0) af.SetField("11", ManageUtility.FormatInteger((int)ApplicationPools[0].Indian));
                                    if (ApplicationPools[0].Asian != null && ApplicationPools[0].Asian >= 0) af.SetField("12", ManageUtility.FormatInteger((int)ApplicationPools[0].Asian));
                                    if (ApplicationPools[0].Black != null && ApplicationPools[0].Black >= 0) af.SetField("13", ManageUtility.FormatInteger((int)ApplicationPools[0].Black));
                                    if (ApplicationPools[0].Hispanic != null && ApplicationPools[0].Hispanic >= 0) af.SetField("14", ManageUtility.FormatInteger((int)ApplicationPools[0].Hispanic));
                                    if (ApplicationPools[0].Hawaiian != null && ApplicationPools[0].Hawaiian >= 0) af.SetField("15", ManageUtility.FormatInteger((int)ApplicationPools[0].Hawaiian));
                                    if (ApplicationPools[0].White != null || ApplicationPools[0].White >= 0) af.SetField("16", ManageUtility.FormatInteger((int)ApplicationPools[0].White));
                                    if (ApplicationPools[0].Undeclared != null && ApplicationPools[0].Undeclared >= 0) af.SetField("16a", ManageUtility.FormatInteger((int)ApplicationPools[0].Undeclared));
                                    if (ApplicationPools[0].MultiRaces != null && ApplicationPools[0].MultiRaces >= 0) af.SetField("17", ManageUtility.FormatInteger((int)ApplicationPools[0].MultiRaces));
                                }

                                //var EnrollmentDatas = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 2 && x.MagnetGPRAID == GPRAData.ID);
                                //if (EnrollmentDatas.Count > 0)
                                //{
                                //    int btotal = 0; bool isBtotalHasData = false;
                                //    if (EnrollmentDatas[0].ExtraFiled1 != null)
                                //    {
                                //        isBtotalHasData = true;
                                //        btotal += (int)EnrollmentDatas[0].ExtraFiled1;
                                //        af.SetField("18", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled1));
                                //    }
                                //    if (EnrollmentDatas[0].Total != null)
                                //    {
                                //        isBtotalHasData = true;
                                //        btotal += (int)EnrollmentDatas[0].Total;
                                //        af.SetField("19", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Total));
                                //    }
                                //    if (EnrollmentDatas[0].Indian != null && EnrollmentDatas[0].Indian >= 0) af.SetField("20", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Indian));
                                //    if (EnrollmentDatas[0].Asian != null && EnrollmentDatas[0].Asian >= 0) af.SetField("21", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Asian));
                                //    if (EnrollmentDatas[0].Black != null && EnrollmentDatas[0].Black >= 0) af.SetField("22", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Black));
                                //    if (EnrollmentDatas[0].Hispanic != null && EnrollmentDatas[0].Hispanic >= 0) af.SetField("23", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hispanic));
                                //    if (EnrollmentDatas[0].Hawaiian != null && EnrollmentDatas[0].Hawaiian >= 0) af.SetField("24", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hawaiian));
                                //    if (EnrollmentDatas[0].White != null && EnrollmentDatas[0].White >= 0) af.SetField("25", ManageUtility.FormatInteger((int)EnrollmentDatas[0].White));
                                //    if (EnrollmentDatas[0].MultiRaces != null) af.SetField("26", ManageUtility.FormatInteger((int)EnrollmentDatas[0].MultiRaces));

                                //    //26a - 26g
                                //    if (EnrollmentDatas[0].NewIndian != null && EnrollmentDatas[0].NewIndian >= 0) af.SetField("26a", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewIndian));
                                //    if (EnrollmentDatas[0].NewAsian != null && EnrollmentDatas[0].NewAsian >= 0) af.SetField("26b", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewAsian));
                                //    if (EnrollmentDatas[0].NewBlack != null && EnrollmentDatas[0].NewBlack >= 0) af.SetField("26c", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewBlack));
                                //    if (EnrollmentDatas[0].NewHispanic != null && EnrollmentDatas[0].NewHispanic >= 0) af.SetField("26d", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewHispanic));
                                //    if (EnrollmentDatas[0].NewHawaiian != null && EnrollmentDatas[0].NewHawaiian >= 0) af.SetField("26e", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewHawaiian));
                                //    if (EnrollmentDatas[0].NewWhite != null && EnrollmentDatas[0].NewWhite >= 0) af.SetField("26f", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewWhite));
                                //    if (EnrollmentDatas[0].NewMultiRaces != null && EnrollmentDatas[0].NewMultiRaces >= 0) af.SetField("26g", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewMultiRaces));
                                //    /////

                                //    //27a - 27g
                                //    if (EnrollmentDatas[0].ContIndian != null && EnrollmentDatas[0].ContIndian >= 0) af.SetField("27a", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContIndian));
                                //    if (EnrollmentDatas[0].ContAsian != null && EnrollmentDatas[0].ContAsian >= 0) af.SetField("27b", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContAsian));
                                //    if (EnrollmentDatas[0].ContBlack != null && EnrollmentDatas[0].ContBlack >= 0) af.SetField("27c", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContBlack));
                                //    if (EnrollmentDatas[0].ContHispanic != null && EnrollmentDatas[0].ContHispanic >= 0) af.SetField("27d", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContHispanic));
                                //    if (EnrollmentDatas[0].ContHawaiian != null && EnrollmentDatas[0].ContHawaiian >= 0) af.SetField("27e", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContHawaiian));
                                //    if (EnrollmentDatas[0].ContWhite != null && EnrollmentDatas[0].ContWhite >= 0) af.SetField("27f", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContWhite));

                                //    if (EnrollmentDatas[0].ContMultiRaces != null && EnrollmentDatas[0].ContMultiRaces >= 0) af.SetField("27g", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContMultiRaces));

                                //    //
                                //    if (EnrollmentDatas[0].ExtraFiled2 != null && EnrollmentDatas[0].ExtraFiled2 >= 0)
                                //    {
                                //        isBtotalHasData = true;
                                //        btotal += (int)EnrollmentDatas[0].ExtraFiled2;
                                //        af.SetField("27", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled2));
                                //    }
                                //    if (isBtotalHasData)
                                //        af.SetField("28", ManageUtility.FormatInteger(btotal));
                                //}
                                var EnrollmentDatas = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 2 && x.MagnetGPRAID == GPRAData.ID);
                                if (EnrollmentDatas.Count > 0)
                                {
                                    int btotal = 0; bool isBtotalHasData = false;
                                    if (EnrollmentDatas[0].ExtraFiled1 != null)
                                    {
                                        isBtotalHasData = true;
                                        btotal += (int)EnrollmentDatas[0].ExtraFiled1;
                                        af.SetField("18", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled1));
                                    }
                                    if (EnrollmentDatas[0].Total != null)
                                    {
                                        isBtotalHasData = true;
                                        btotal += (int)EnrollmentDatas[0].Total;
                                        af.SetField("19", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Total));
                                    }
                                    if (EnrollmentDatas[0].Indian != null && EnrollmentDatas[0].Indian >= 0)
                                    {
                                        af.SetField("20", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Indian));
                                        item18_Indian = (int)EnrollmentDatas[0].Indian;
                                    }
                                    if (EnrollmentDatas[0].Asian != null && EnrollmentDatas[0].Asian >= 0)
                                    {
                                        af.SetField("21", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Asian));
                                        item19_Asian = (int)EnrollmentDatas[0].Asian;
                                    }
                                    if (EnrollmentDatas[0].Black != null && EnrollmentDatas[0].Black >= 0)
                                    {
                                        af.SetField("22", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Black));
                                        item20_Black = (int)EnrollmentDatas[0].Black;
                                    }
                                    if (EnrollmentDatas[0].Hispanic != null && EnrollmentDatas[0].Hispanic >= 0)
                                    {
                                        af.SetField("23", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hispanic));
                                        item21_Hispanic = (int)EnrollmentDatas[0].Hispanic;
                                    }
                                    if (EnrollmentDatas[0].Hawaiian != null && EnrollmentDatas[0].Hawaiian >= 0)
                                    {
                                        af.SetField("24", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hawaiian));
                                        item22_Hawaiian = (int)EnrollmentDatas[0].Hawaiian;
                                    }
                                    if (EnrollmentDatas[0].White != null && EnrollmentDatas[0].White >= 0)
                                    {
                                        af.SetField("25", ManageUtility.FormatInteger((int)EnrollmentDatas[0].White));
                                        item23_White = (int)EnrollmentDatas[0].White;
                                    }
                                    if (EnrollmentDatas[0].MultiRaces != null) af.SetField("26", ManageUtility.FormatInteger((int)EnrollmentDatas[0].MultiRaces));

                                    //26a - 26g
                                    if (EnrollmentDatas[0].NewIndian != null && EnrollmentDatas[0].NewIndian >= 0)
                                    {
                                        af.SetField("26a", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewIndian));
                                        item26a_newIndian = (int)EnrollmentDatas[0].NewIndian;
                                    }
                                    if (EnrollmentDatas[0].NewAsian != null && EnrollmentDatas[0].NewAsian >= 0)
                                    {
                                        af.SetField("26b", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewAsian));
                                        item26b_newAsian = (int)EnrollmentDatas[0].NewAsian;
                                    }
                                    if (EnrollmentDatas[0].NewBlack != null && EnrollmentDatas[0].NewBlack >= 0)
                                    {
                                        af.SetField("26c", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewBlack));
                                        item26c_newBlack = (int)EnrollmentDatas[0].NewBlack;
                                    }
                                    if (EnrollmentDatas[0].NewHispanic != null && EnrollmentDatas[0].NewHispanic >= 0)
                                    {
                                        af.SetField("26d", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewHispanic));
                                        item26d_newHispanic = (int)EnrollmentDatas[0].NewHispanic;
                                    }
                                    if (EnrollmentDatas[0].NewHawaiian != null && EnrollmentDatas[0].NewHawaiian >= 0)
                                    {
                                        af.SetField("26e", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewHawaiian));
                                        item26e_newHawaiian = (int)EnrollmentDatas[0].NewHawaiian;
                                    }
                                    if (EnrollmentDatas[0].NewWhite != null && EnrollmentDatas[0].NewWhite >= 0)
                                    {
                                        af.SetField("26f", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewWhite));
                                        item26f_newWhite = (int)EnrollmentDatas[0].NewWhite;
                                    }
                                    if (EnrollmentDatas[0].NewMultiRaces != null && EnrollmentDatas[0].NewMultiRaces >= 0) af.SetField("26g", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewMultiRaces));
                                    /////

                                    //27a - 27g
                                    if (EnrollmentDatas[0].ContIndian != null && EnrollmentDatas[0].ContIndian >= 0)
                                    {
                                        af.SetField("27a", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContIndian));
                                        item27a_ContIndian = (int)EnrollmentDatas[0].ContIndian;
                                    }
                                    if (EnrollmentDatas[0].ContAsian != null && EnrollmentDatas[0].ContAsian >= 0)
                                    {
                                        af.SetField("27b", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContAsian));
                                        item27b_ContAsian = (int)EnrollmentDatas[0].ContAsian;
                                    }
                                    if (EnrollmentDatas[0].ContBlack != null && EnrollmentDatas[0].ContBlack >= 0)
                                    {
                                        af.SetField("27c", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContBlack));
                                        item27c_ContBlack = (int)EnrollmentDatas[0].ContBlack;
                                    }
                                    if (EnrollmentDatas[0].ContHispanic != null && EnrollmentDatas[0].ContHispanic >= 0)
                                    {
                                        af.SetField("27d", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContHispanic));
                                        item27d_ContHispanic = (int)EnrollmentDatas[0].ContHispanic;
                                    }
                                    if (EnrollmentDatas[0].ContHawaiian != null && EnrollmentDatas[0].ContHawaiian >= 0)
                                    {
                                        af.SetField("27e", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContHawaiian));
                                        item27e_ContHawaiian = (int)EnrollmentDatas[0].ContHawaiian;
                                    }
                                    if (EnrollmentDatas[0].ContWhite != null && EnrollmentDatas[0].ContWhite >= 0)
                                    {
                                        af.SetField("27f", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContWhite));
                                        item27f_ContWhite = (int)EnrollmentDatas[0].ContWhite;
                                    }

                                    if (EnrollmentDatas[0].ContMultiRaces != null && EnrollmentDatas[0].ContMultiRaces >= 0) af.SetField("27g", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContMultiRaces));

                                    //
                                    if (EnrollmentDatas[0].ExtraFiled2 != null && EnrollmentDatas[0].ExtraFiled2 >= 0)
                                    {
                                        isBtotalHasData = true;
                                        btotal += (int)EnrollmentDatas[0].ExtraFiled2;
                                        af.SetField("27", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled2));
                                    }
                                    if (isBtotalHasData)
                                    {
                                        af.SetField("28", ManageUtility.FormatInteger(btotal));
                                        item28_total = (int)EnrollmentDatas[0].ExtraFiled3;
                                    }
                                }

                                af.SetField("AYP_Participation_date_read", period.ReportPeriod);
                                af.SetField("AYP_Participation_date_math", period.ReportPeriod);
                                af.SetField("AYP_achievement_date_read", period.ReportPeriod);
                                af.SetField("AYP_achievement_date_math", period.ReportPeriod);

                                var ParticipationReading = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 3 && x.MagnetGPRAID == GPRAData.ID);
                                if (ParticipationReading.Count > 0)
                                {
                                    if (ParticipationReading[0].Total != null) af.SetField("29 Reading", ParticipationReading[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Total));
                                    if (ParticipationReading[0].Indian != null) af.SetField("30 Reading", ParticipationReading[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Indian));
                                    if (ParticipationReading[0].Asian != null) af.SetField("31 Reading", ParticipationReading[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Asian));
                                    if (ParticipationReading[0].Black != null) af.SetField("32 Reading", ParticipationReading[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Black));
                                    if (ParticipationReading[0].Hispanic != null) af.SetField("33 Reading", ParticipationReading[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Hispanic));
                                    if (ParticipationReading[0].Hawaiian != null) af.SetField("34 Reading", ParticipationReading[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Hawaiian));
                                    if (ParticipationReading[0].White != null) af.SetField("35 Reading", ParticipationReading[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].White));
                                    if (ParticipationReading[0].MultiRaces != null) af.SetField("47 Reading", ParticipationReading[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].MultiRaces));
                                    if (ParticipationReading[0].ExtraFiled2 != null) af.SetField("36 Reading", ParticipationReading[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].ExtraFiled2));
                                    if (ParticipationReading[0].ExtraFiled1 != null) af.SetField("37 Reading", ParticipationReading[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].ExtraFiled1));
                                }

                                var ParticipationMath = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 4 && x.MagnetGPRAID == GPRAData.ID);
                                if (ParticipationMath.Count > 0)
                                {
                                    if (ParticipationMath[0].Total != null) af.SetField("29 Math", ParticipationMath[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Total));
                                    if (ParticipationMath[0].Indian != null) af.SetField("30 Math", ParticipationMath[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Indian));
                                    if (ParticipationMath[0].Asian != null) af.SetField("31 Math", ParticipationMath[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Asian));
                                    if (ParticipationMath[0].Black != null) af.SetField("32 Math", ParticipationMath[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Black));
                                    if (ParticipationMath[0].Hispanic != null) af.SetField("33 Math", ParticipationMath[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Hispanic));
                                    if (ParticipationMath[0].Hawaiian != null) af.SetField("34 Math", ParticipationMath[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Hawaiian));
                                    if (ParticipationMath[0].White != null) af.SetField("35 Math", ParticipationMath[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].White));
                                    if (ParticipationMath[0].MultiRaces != null) af.SetField("47 Math", ParticipationMath[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].MultiRaces));
                                    if (ParticipationMath[0].ExtraFiled2 != null) af.SetField("36 Math", ParticipationMath[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].ExtraFiled2));
                                    if (ParticipationMath[0].ExtraFiled1 != null) af.SetField("37 Math", ParticipationMath[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].ExtraFiled1));
                                }

                                var ArchievementReading = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 5 && x.MagnetGPRAID == GPRAData.ID);
                                if (ArchievementReading.Count > 0)
                                {
                                    if (ArchievementReading[0].Total != null) af.SetField("38 Reading", ArchievementReading[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Total));
                                    if (ArchievementReading[0].Indian != null) af.SetField("39 Reading", ArchievementReading[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Indian));
                                    if (ArchievementReading[0].Asian != null) af.SetField("40 Reading", ArchievementReading[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Asian));
                                    if (ArchievementReading[0].Black != null) af.SetField("41 Reading", ArchievementReading[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Black));
                                    if (ArchievementReading[0].Hispanic != null) af.SetField("42 Reading", ArchievementReading[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Hispanic));
                                    if (ArchievementReading[0].Hawaiian != null) af.SetField("43 Reading", ArchievementReading[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Hawaiian));
                                    if (ArchievementReading[0].White != null) af.SetField("44 Reading", ArchievementReading[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].White));
                                    if (ArchievementReading[0].MultiRaces != null) af.SetField("48 Reading", ArchievementReading[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].MultiRaces));
                                    if (ArchievementReading[0].ExtraFiled2 != null) af.SetField("45 Reading", ArchievementReading[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].ExtraFiled2));
                                    if (ArchievementReading[0].ExtraFiled1 != null) af.SetField("46 Reading", ArchievementReading[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].ExtraFiled1));
                                }

                                var ArchievementMath = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 6 && x.MagnetGPRAID == GPRAData.ID);
                                if (ArchievementMath.Count > 0)
                                {
                                    if (ArchievementMath[0].Total != null) af.SetField("38 Math", ArchievementMath[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Total));
                                    if (ArchievementMath[0].Indian != null) af.SetField("39 Math", ArchievementMath[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Indian));
                                    if (ArchievementMath[0].Asian != null) af.SetField("40 Math", ArchievementMath[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Asian));
                                    if (ArchievementMath[0].Black != null) af.SetField("41 Math", ArchievementMath[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Black));
                                    if (ArchievementMath[0].Hispanic != null) af.SetField("42 Math", ArchievementMath[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Hispanic));
                                    if (ArchievementMath[0].Hawaiian != null) af.SetField("43 Math", ArchievementMath[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Hawaiian));
                                    if (ArchievementMath[0].White != null) af.SetField("44 Math", ArchievementMath[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].White));
                                    if (ArchievementMath[0].MultiRaces != null) af.SetField("48 Math", ArchievementMath[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].MultiRaces));
                                    if (ArchievementMath[0].ExtraFiled2 != null) af.SetField("45 Math", ArchievementMath[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].ExtraFiled2));
                                    if (ArchievementMath[0].ExtraFiled1 != null) af.SetField("46 Math", ArchievementMath[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].ExtraFiled1));
                                }

                                af.SetField("cost_perstudent_date", period.ReportPeriod);

                                var PerformanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 7 && x.MagnetGPRAID == GPRAData.ID);
                                if (PerformanceMeasure.Count > 0)
                                {
                                    if (PerformanceMeasure[0].Budget != null) af.SetField("49", "$" + string.Format("{0:#,0.00}", PerformanceMeasure[0].Budget));
                                    if (PerformanceMeasure[0].Total != null) af.SetField("50", ManageUtility.FormatInteger((int)PerformanceMeasure[0].Total));
                                }

                                ps.FormFlattening = true;

                                r.Close();
                                ps.Close();

                                //Add to final report
                                PdfReader reader = new PdfReader(TmpPDF);
                                document.NewPage();
                                PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                                pdfContentByte.AddTemplate(importedPage, -3, 0);
                                document.NewPage();
                                importedPage = pdfWriter.GetImportedPage(reader, 2);
                                pdfContentByte.AddTemplate(importedPage, -3, 0);
                                document.NewPage();
                                importedPage = pdfWriter.GetImportedPage(reader, 3);
                                pdfContentByte.AddTemplate(importedPage, -3, 0);
                                reader.Close();

                                if (isHighSchool)
                                {
                                    //gpra v
                                    var gpraVdata = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 8 && x.MagnetGPRAID == GPRAData.ID);
                                    if (gpraVdata.Count > 0)
                                    {
                                        string pdfTemplate = Server.MapPath("../doc/gprav.pdf");
                                        string gpraVpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                        if (File.Exists(gpraVpdf))
                                            File.Delete(gpraVpdf);
                                        TmpPDFs.Add(gpraVpdf);

                                        PdfReader pdfReader = new PdfReader(pdfTemplate);
                                        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                            gpraVpdf, FileMode.Create));

                                        AcroFields pdfFormFields = pdfStamper.AcroFields;
                                        pdfFormFields.SetField("titleyear", period.ReportPeriod);
                                        pdfFormFields.SetField("Allcohort", gpraVdata[0].Allcohort == null ? "" : gpraVdata[0].Allcohort.ToString());
                                        pdfFormFields.SetField("Allgraduated", gpraVdata[0].Allgraduated == null ? "" : gpraVdata[0].Allgraduated.ToString());
                                        pdfFormFields.SetField("allpercentage", gpraVdata[0].allpercentage == null ? "" : gpraVdata[0].allpercentage);

                                        pdfFormFields.SetField("AmericanIndiancohort", gpraVdata[0].AmericanIndiancohort == null ? "" : gpraVdata[0].AmericanIndiancohort.ToString());
                                        pdfFormFields.SetField("AmericanIndiangraduated", gpraVdata[0].AmericanIndiangraduated == null ? "" : gpraVdata[0].AmericanIndiangraduated.ToString());
                                        pdfFormFields.SetField("AmericanIndianpercentage", gpraVdata[0].AmericanIndianpercentage == null ? "" : gpraVdata[0].AmericanIndianpercentage);

                                        pdfFormFields.SetField("Asiancohort", gpraVdata[0].Asiancohort == null ? "" : gpraVdata[0].Asiancohort.ToString());
                                        pdfFormFields.SetField("Asiangraduated", gpraVdata[0].Asiangraduated == null ? "" : gpraVdata[0].Asiangraduated.ToString());
                                        pdfFormFields.SetField("Asianpercentage", gpraVdata[0].Asianpercentage == null ? "" : gpraVdata[0].Asianpercentage);

                                        pdfFormFields.SetField("AfricanAmericancohort", gpraVdata[0].AfricanAmericancohort == null ? "" : gpraVdata[0].AfricanAmericancohort.ToString());
                                        pdfFormFields.SetField("AfricanAmericangraduated", gpraVdata[0].AfricanAmericangraduated == null ? "" : gpraVdata[0].AfricanAmericangraduated.ToString());
                                        pdfFormFields.SetField("AfricanAmericanpercentage", gpraVdata[0].AfricanAmericanpercentage == null ? "" : gpraVdata[0].AfricanAmericanpercentage);

                                        pdfFormFields.SetField("HispanicLatinocohort", gpraVdata[0].HispanicLatinocohort == null ? "" : gpraVdata[0].HispanicLatinocohort.ToString());
                                        pdfFormFields.SetField("HispanicLatinograduated", gpraVdata[0].HispanicLatinograduated == null ? "" : gpraVdata[0].HispanicLatinograduated.ToString());
                                        pdfFormFields.SetField("HispanicLatinopercentage", gpraVdata[0].HispanicLatinopercentage == null ? "" : gpraVdata[0].HispanicLatinopercentage);

                                        pdfFormFields.SetField("NativeHawaiiancohort", gpraVdata[0].NativeHawaiiancohort == null ? "" : gpraVdata[0].NativeHawaiiancohort.ToString());
                                        pdfFormFields.SetField("NativeHawaiiangraduated", gpraVdata[0].NativeHawaiiangraduated == null ? "" : gpraVdata[0].NativeHawaiiangraduated.ToString());
                                        pdfFormFields.SetField("NativeHawaiianpercentage", gpraVdata[0].NativeHawaiianpercentage == null ? "" : gpraVdata[0].NativeHawaiianpercentage);

                                        pdfFormFields.SetField("Whitecohort", gpraVdata[0].Whitecohort == null ? "" : gpraVdata[0].Whitecohort.ToString());
                                        pdfFormFields.SetField("Whitegraduated", gpraVdata[0].Whitegraduated == null ? "" : gpraVdata[0].Whitegraduated.ToString());
                                        pdfFormFields.SetField("Whitepercentage", gpraVdata[0].Whitepercentage == null ? "" : gpraVdata[0].Whitepercentage);

                                        pdfFormFields.SetField("MoreRacescohort", gpraVdata[0].MoreRacescohort == null ? "" : gpraVdata[0].MoreRacescohort.ToString());
                                        pdfFormFields.SetField("MoreRacesgraduated", gpraVdata[0].MoreRacesgraduated == null ? "" : gpraVdata[0].MoreRacesgraduated.ToString());
                                        pdfFormFields.SetField("MoreRacespercentage", gpraVdata[0].MoreRacespercentage == null ? "" : gpraVdata[0].MoreRacespercentage);

                                        pdfFormFields.SetField("Economicallycohort", gpraVdata[0].Economicallycohort == null ? "" : gpraVdata[0].Economicallycohort.ToString());
                                        pdfFormFields.SetField("Economicallygraduated", gpraVdata[0].Economicallygraduated == null ? "" : gpraVdata[0].Economicallygraduated.ToString());
                                        pdfFormFields.SetField("Economicallypercentage", gpraVdata[0].Economicallypercentage == null ? "" : gpraVdata[0].Economicallypercentage);

                                        pdfFormFields.SetField("Englishlearnerscohort", gpraVdata[0].Englishlearnerscohort == null ? "" : gpraVdata[0].Englishlearnerscohort.ToString());
                                        pdfFormFields.SetField("Englishlearnersgraduated", gpraVdata[0].Englishlearnersgraduated == null ? "" : gpraVdata[0].Englishlearnersgraduated.ToString());
                                        pdfFormFields.SetField("Englishlearnerspercentage", gpraVdata[0].Englishlearnerspercentage == null ? "" : gpraVdata[0].Englishlearnerspercentage);



                                        // flatten the form to remove editting options, set it to false
                                        // to leave the form open to subsequent manual edits
                                        pdfStamper.FormFlattening = true;

                                        // close the pdf
                                        pdfReader.Close();
                                        pdfStamper.Close();

                                        //Add to final report

                                        PdfReader addrsreader = new PdfReader(gpraVpdf);

                                        document.NewPage();

                                        PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                        pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                        addrsreader.Close();
                                    }
                                    else
                                    {
                                        //empty gpra v
                                        string pdfTemplate = Server.MapPath("../doc/gprav.pdf");
                                        string gpraVpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                        if (File.Exists(gpraVpdf))
                                            File.Delete(gpraVpdf);
                                        TmpPDFs.Add(gpraVpdf);

                                        PdfReader pdfReader = new PdfReader(pdfTemplate);
                                        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                            gpraVpdf, FileMode.Create));

                                        // close the pdf
                                        pdfReader.Close();
                                        pdfStamper.Close();

                                        //Add to final report

                                        PdfReader addrsreader = new PdfReader(gpraVpdf);

                                        document.NewPage();

                                        PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                        pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                        addrsreader.Close();
                                    }
                                } //end isHighSchool
                                ///////////////GPRA 6  ///////////////////////////////

                                MagnetGPRA6 gpra6Record = MagnetGPRA6.SingleOrDefault(x => x.MagnetGPRAID == GPRAData.ID);
                                if (gpra6Record != null)
                                {
                                    string pdfTemplate = Server.MapPath("../doc/GPRAVI.pdf");
                                    string gpraVIpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(gpraVIpdf))
                                        File.Delete(gpraVIpdf);
                                    TmpPDFs.Add(gpraVIpdf);

                                    PdfReader pdfReader = new PdfReader(pdfTemplate);
                                    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                        gpraVIpdf, FileMode.Create));

                                    AcroFields pdfFormFields = pdfStamper.AcroFields;
                                    //items 61
                                    foreach (string str in gpra6Record.MinorityIsolatedGroups.Split(','))
                                    {
                                        pdfFormFields.SetField("61_" + str, "Yes");
                                    }
                                    //items 62
                                    if (gpra6Record.IndianMSAP62)
                                        pdfFormFields.SetField("IndianMSAP62", "Yes");
                                    if (gpra6Record.IndianFeeder62)
                                        pdfFormFields.SetField("IndianFeeder62", "Yes");
                                    if (gpra6Record.AsianMSAP62)
                                        pdfFormFields.SetField("AsianMSAP62", "Yes");
                                    if (gpra6Record.AsianFeeder62)
                                        pdfFormFields.SetField("AsianFeeder62", "Yes");
                                    if (gpra6Record.BlackMSAP62)
                                        pdfFormFields.SetField("BlackMSAP62", "Yes");
                                    if (gpra6Record.BlackFeeder62)
                                        pdfFormFields.SetField("BlackFeeder62", "Yes");
                                    if (gpra6Record.HispanicMSAP62)
                                        pdfFormFields.SetField("HispanicMSAP62", "Yes");
                                    if (gpra6Record.HispanicFeeder62)
                                        pdfFormFields.SetField("HispanicFeeder62", "Yes");
                                    if (gpra6Record.NativeMSAP62)
                                        pdfFormFields.SetField("NativeMSAP62", "Yes");
                                    if (gpra6Record.NativeFeeder62)
                                        pdfFormFields.SetField("NativeFeeder62", "Yes");
                                    if (gpra6Record.WhiteMSAP62)
                                        pdfFormFields.SetField("WhiteMSAP62", "Yes");
                                    if (gpra6Record.WhiteFeeder62)
                                        pdfFormFields.SetField("WhiteFeeder62", "Yes");
                                    //items 63
                                    if (gpra6Record.IndianDec63)
                                        pdfFormFields.SetField("IndianDec63", "Yes");
                                    if (gpra6Record.IndianInc63)
                                        pdfFormFields.SetField("IndianInc63", "Yes");
                                    if (gpra6Record.IndianMtn63)
                                        pdfFormFields.SetField("IndianMtn63", "Yes");
                                    if (gpra6Record.AsianDec63)
                                        pdfFormFields.SetField("AsianDec63", "Yes");
                                    if (gpra6Record.AsianInc63)
                                        pdfFormFields.SetField("AsianInc63", "Yes");
                                    if (gpra6Record.AsianMtn63)
                                        pdfFormFields.SetField("AsianMtn63", "Yes");
                                    if (gpra6Record.BlackDec63)
                                        pdfFormFields.SetField("BlackDec63", "Yes");
                                    if (gpra6Record.BlackInc63)
                                        pdfFormFields.SetField("BlackInc63", "Yes");
                                    if (gpra6Record.BlackMtn63)
                                        pdfFormFields.SetField("BlackMtn63", "Yes");
                                    if (gpra6Record.HispanicDec63)
                                        pdfFormFields.SetField("HispanicDec63", "Yes");
                                    if (gpra6Record.HispanicInc63)
                                        pdfFormFields.SetField("HispanicInc63", "Yes");
                                    if (gpra6Record.HispanicMtn63)
                                        pdfFormFields.SetField("HispanicMtn63", "Yes");
                                    if (gpra6Record.NativeDec63)
                                        pdfFormFields.SetField("NativeDec63", "Yes");
                                    if (gpra6Record.NativeInc63)
                                        pdfFormFields.SetField("NativeInc63", "Yes");
                                    if (gpra6Record.NativeMtn63)
                                        pdfFormFields.SetField("NativeMtn63", "Yes");
                                    if (gpra6Record.WhiteDec63)
                                        pdfFormFields.SetField("WhiteDec63", "Yes");
                                    if (gpra6Record.WhiteInc63)
                                        pdfFormFields.SetField("WhiteInc63", "Yes");
                                    if (gpra6Record.WhiteMtn63)
                                        pdfFormFields.SetField("WhiteMtn63", "Yes");
                                    //items 64
                                    foreach (string str in gpra6Record.TargetRacialGroup64.Split(','))
                                    {
                                        pdfFormFields.SetField("64_" + str, "Yes");
                                    }
                                    //items 65
                                    string IndianActualPercentage65 = "", AsianActualPercentage65 = "",
                                           BlackActualPercentage65 = "", HispanicActualPercentage65 = "",
                                           NativeActualPercentage65 = "", WhiteActualPercentage65 = "";

                                    IndianActualPercentage65 = item28_total == 0 ? "" : ((double)(item18_Indian + item26a_newIndian + item27a_ContIndian)*100 /
                                                                (item28_total)).ToString("F");
                                    AsianActualPercentage65 = item28_total == 0 ? "" : ((double)(item19_Asian + item26b_newAsian + item27b_ContAsian) * 100 /
                                                                (item28_total)).ToString("F");
                                    BlackActualPercentage65 = item28_total == 0 ? "" : ((double)(item20_Black + item26c_newBlack + item27c_ContBlack) * 100 / (item28_total)).ToString("F");
                                    HispanicActualPercentage65 = item28_total == 0 ? "" : ((double)(item21_Hispanic + item26d_newHispanic + item27d_ContHispanic) * 100 / (item28_total)).ToString("F");
                                    NativeActualPercentage65 = item28_total == 0 ? "" : ((double)(item22_Hawaiian + item26e_newHawaiian + item27e_ContHawaiian) * 100 / (item28_total)).ToString("F");
                                    WhiteActualPercentage65 = item28_total == 0 ? "" : ((double)(item23_White + item26f_newWhite + item27f_ContWhite) * 100 / (item28_total)).ToString("F");

                                    foreach (string str in gpra6Record.MinorityIsolatedGroups.Split(','))
                                    {
                                        switch (str)
                                        {
                                            case "1":
                                                pdfFormFields.SetField("IndianTargetPercentage65", gpra6Record.IndianTargetPercentage65);
                                                pdfFormFields.SetField("ActIndianTargetPercentage65", IndianActualPercentage65);
                                                break;
                                            case "2":
                                                pdfFormFields.SetField("AsianTargetPercentage65", gpra6Record.AsianTargetPercentage65);
                                                pdfFormFields.SetField("ActAsianTargetPercentage65", AsianActualPercentage65);
                                                break;

                                            case "3":
                                                pdfFormFields.SetField("BlackTargetPercentage65", gpra6Record.BlackTargetPercentage65);
                                                pdfFormFields.SetField("ActBlackTargetPercentage65", BlackActualPercentage65);
                                                break;
                                            case "4":
                                                pdfFormFields.SetField("HispanicTargetPercentage65", gpra6Record.HispanicTargetPercentage65);
                                                pdfFormFields.SetField("ActHispanicTargetPercentage65", HispanicActualPercentage65);
                                                break;
                                            case "5":
                                                pdfFormFields.SetField("NativeTargetPercentage65", gpra6Record.NativeTargetPercentage65);
                                                pdfFormFields.SetField("ActNativeTargetPercentage65", NativeActualPercentage65);
                                                break;
                                            case "6":
                                                pdfFormFields.SetField("WhiteTargetPercentage65", gpra6Record.WhiteTargetPercentage65);
                                                pdfFormFields.SetField("ActWhiteTargetPercentage65", WhiteActualPercentage65);
                                                break;
                                        }
                                    }

                                    //items 66
                                    if (gpra6Record.IndianMet66)
                                        pdfFormFields.SetField("IndianMet66", "Yes");
                                    if (gpra6Record.IndianNotMet66)
                                        pdfFormFields.SetField("IndianNotMet66", "Yes");
                                    if (gpra6Record.AsianMet66)
                                        pdfFormFields.SetField("AsianMet66", "Yes");
                                    if (gpra6Record.AsianNotMet66)
                                        pdfFormFields.SetField("AsianNotMet66", "Yes");
                                    if (gpra6Record.BlackMet66)
                                        pdfFormFields.SetField("BlackMet66", "Yes");
                                    if (gpra6Record.BlackNotMet66)
                                        pdfFormFields.SetField("BlackNotMet66", "Yes");
                                    if (gpra6Record.HispanicMet66)
                                        pdfFormFields.SetField("HispanicMet66", "Yes");
                                    if (gpra6Record.HispanicNotMet66)
                                        pdfFormFields.SetField("HispanicNotMet66", "Yes");
                                    if (gpra6Record.NativeMet66)
                                        pdfFormFields.SetField("NativeMet66", "Yes");
                                    if (gpra6Record.NativeNotMet66)
                                        pdfFormFields.SetField("NativeNotMet66", "Yes");
                                    if (gpra6Record.WhiteMet66)
                                        pdfFormFields.SetField("WhiteMet66", "Yes");
                                    if (gpra6Record.WhiteNotMet66)
                                        pdfFormFields.SetField("WhiteNotMet66", "Yes");

                                    //items 67
                                    if (gpra6Record.IndianMade67)
                                        pdfFormFields.SetField("IndianMade67", "Yes");
                                    if (gpra6Record.IndianNotMade67)
                                        pdfFormFields.SetField("IndianNotMade67", "Yes");
                                    if (gpra6Record.IndianNA67)
                                        pdfFormFields.SetField("IndianNA67", "Yes");
                                    if (gpra6Record.AsianMade67)
                                        pdfFormFields.SetField("AsianMade67", "Yes");
                                    if (gpra6Record.AsianNotMade67)
                                        pdfFormFields.SetField("AsianNotMade67", "Yes");
                                    if (gpra6Record.AsianNA67)
                                        pdfFormFields.SetField("AsianNA67", "Yes");
                                    if (gpra6Record.BlackMade67)
                                        pdfFormFields.SetField("BlackMade67", "Yes");
                                    if (gpra6Record.BlackNotMade67)
                                        pdfFormFields.SetField("BlackNotMade67", "Yes");
                                    if (gpra6Record.BlackNA67)
                                        pdfFormFields.SetField("BlackNA67", "Yes");
                                    if (gpra6Record.HispanicMade67)
                                        pdfFormFields.SetField("HispanicMade67", "Yes");
                                    if (gpra6Record.HispanicNotMade67)
                                        pdfFormFields.SetField("HispanicNotMade67", "Yes");
                                    if (gpra6Record.HispanicNA67)
                                        pdfFormFields.SetField("HispanicNA67", "Yes");
                                    if (gpra6Record.NativeMade67)
                                        pdfFormFields.SetField("NativeMade67", "Yes");
                                    if (gpra6Record.NativeNotMade67)
                                        pdfFormFields.SetField("NativeNotMade67", "Yes");
                                    if (gpra6Record.NativeNA67)
                                        pdfFormFields.SetField("NativeNA67", "Yes");
                                    if (gpra6Record.WhiteMade67)
                                        pdfFormFields.SetField("WhiteMade67", "Yes");
                                    if (gpra6Record.WhiteNotMade67)
                                        pdfFormFields.SetField("WhiteNotMade67", "Yes");
                                    if (gpra6Record.WhiteNA67)
                                        pdfFormFields.SetField("WhiteNA67", "Yes");

                                    // flatten the form to remove editting options, set it to false
                                    // to leave the form open to subsequent manual edits
                                    pdfStamper.FormFlattening = true;

                                    // close the pdf
                                    pdfReader.Close();
                                    pdfStamper.Close();

                                    //Add to final report

                                    PdfReader addrsreader = new PdfReader(gpraVIpdf);
                                    document.SetPageSize(PageSize.A4);
                                    document.NewPage();
                                    PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                    pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                    document.NewPage();
                                    bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 2);
                                    pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                    addrsreader.Close();
                                }
                                else
                                {
                                    //empty gpra vi
                                    string pdfTemplate = Server.MapPath("../doc/GPRAVI.pdf");
                                    string gpraVIpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(gpraVIpdf))
                                        File.Delete(gpraVIpdf);
                                    TmpPDFs.Add(gpraVIpdf);

                                    PdfReader pdfReader = new PdfReader(pdfTemplate);
                                    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                        gpraVIpdf, FileMode.Create));

                                    // close the pdf
                                    pdfReader.Close();
                                    pdfStamper.Close();

                                    //Add to final report

                                    PdfReader addrsreader = new PdfReader(gpraVIpdf);

                                    document.NewPage();

                                    PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                    pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                    document.NewPage();
                                    bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 2);
                                    pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                    addrsreader.Close();

                                }

                                /////////////END GPRA 6 ////////////////////
                            }
                            catch (System.Exception ex)
                            {
                                ILog Log = LogManager.GetLogger("EventLog");
                                Log.Error("GPRA report:", ex);
                            }
                            finally
                            {
                            }
                        }

                    } //end if data
                    else  //empty school info
                    {

                        try
                        {
                            string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(TmpPDF))
                                File.Delete(TmpPDF);
                            TmpPDFs.Add(TmpPDF);

                            PdfStamper ps = null;

                            // Fill out form
                            PdfReader r = null;
                            if (report.ReportPeriodID > 5)
                            {
                                if (report.ReportPeriodID > 8)
                                    r = new PdfReader(Server.MapPath("../doc/newGPRA.pdf"));
                                else
                                    r = new PdfReader(Server.MapPath("../doc/gpranew.pdf"));
                            }
                            else
                            {
                                r = new PdfReader(Server.MapPath("../doc/gpra.pdf"));
                            }

                            ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                            AcroFields af = ps.AcroFields;

                            af.SetField("1 School name", itm.SchoolName);
                            af.SetField("2 Grantee name", MagnetGrantee.SingleOrDefault(x => x.ID == granteeID).GranteeName);
                            af.SetField("cost_perstudent_date", period.ReportPeriod);
                            af.SetField("Applicant_pool_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));
                            af.SetField("Enrollment_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));

                            af.SetField("AYP_Participation_date_read", period.ReportPeriod);
                            af.SetField("AYP_Participation_date_math", period.ReportPeriod);
                            af.SetField("AYP_achievement_date_read", period.ReportPeriod);
                            af.SetField("AYP_achievement_date_math", period.ReportPeriod);
                            af.SetField("cost_perstudent_date", period.ReportPeriod);

                            ps.FormFlattening = true;

                            r.Close();
                            ps.Close();
                            float width = 0;
                            float height = 0;

                            //Add to final report
                            PdfReader reader = new PdfReader(TmpPDF);
                            for (int t = 1; t <= reader.NumberOfPages; t++)
                            {
                                width = reader.GetPageSize(t).Width;
                                height = reader.GetPageSize(t).Height;
                                if (width > height)
                                    document.SetPageSize(PageSize.A4.Rotate());
                                else
                                    document.SetPageSize(PageSize.A4);
                                document.NewPage();

                                PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, t);
                                //pdfContentByte.AddTemplate(importedPage, -3, 0);
                                pdfContentByte.AddTemplate(importedPage, 0, 0);
                            }
                            reader.Close();
                            //document.NewPage();
                            //PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                            //pdfContentByte.AddTemplate(importedPage, -3, 0);
                            //document.NewPage();
                            //importedPage = pdfWriter.GetImportedPage(reader, 2);
                            //pdfContentByte.AddTemplate(importedPage, -3, 0);
                            //document.NewPage();
                            //importedPage = pdfWriter.GetImportedPage(reader, 3);
                            //pdfContentByte.AddTemplate(importedPage, -3, 0);


                            //empty gpra v
                            string pdfTemplate = Server.MapPath("../doc/gprav.pdf");
                            string gpraVpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(gpraVpdf))
                                File.Delete(gpraVpdf);
                            TmpPDFs.Add(gpraVpdf);

                            PdfReader pdfReader = new PdfReader(pdfTemplate);
                            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                gpraVpdf, FileMode.Create));

                            AcroFields af5 = pdfStamper.AcroFields;
                            af5.SetField("titleyear", period.ReportPeriod);

                            pdfStamper.FormFlattening = true;

                            // close the pdf
                            pdfReader.Close();
                            pdfStamper.Close();

                            //Add to final report

                            PdfReader addrsreader = new PdfReader(gpraVpdf);

                            document.NewPage();

                            PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                            //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                            pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                            addrsreader.Close();

                            //empty gpra 6
                            string pdfTemplate6 = Server.MapPath("../doc/GPRAVI.pdf");
                            string gpraVpdf6 = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(gpraVpdf6))
                                File.Delete(gpraVpdf6);
                            TmpPDFs.Add(gpraVpdf6);

                            PdfReader pdfReader6 = new PdfReader(pdfTemplate6);
                            PdfStamper pdfStamper6 = new PdfStamper(pdfReader6, new FileStream(
                                gpraVpdf6, FileMode.Create));

                            // close the pdf
                            pdfReader6.Close();
                            pdfStamper6.Close();

                            //Add to final report

                            PdfReader addrsreader6 = new PdfReader(gpraVpdf6);

                            document.NewPage();

                            PdfImportedPage bsimportedPage6 = pdfWriter.GetImportedPage(addrsreader6, 1);
                            //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                            pdfContentByte.AddTemplate(bsimportedPage6, 1.0F, 0, 0, 1.0F, -3, 0);
                            document.NewPage();
                            bsimportedPage6 = pdfWriter.GetImportedPage(addrsreader6, 2);
                            //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                            pdfContentByte.AddTemplate(bsimportedPage6, 1.0F, 0, 0, 1.0F, -3, 0);
                            addrsreader6.Close();

                        }
                        catch (System.Exception ex)
                        {
                            ILog Log = LogManager.GetLogger("EventLog");
                            Log.Error("GPRA report:", ex);
                        }


                    }  //empty school info

                } //foreach (MagnetSchool

                document.Close();
                foreach (string str in TmpPDFs)
                {
                    if (File.Exists(str))
                        File.Delete(str);
                }

                granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
                MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_gpra.pdf");
                Response.OutputStream.Write(Output.GetBuffer(), 0, Output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();

                Output.Close();

            } //using (System.IO

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('There is no School in GPRA.');</script>", false);
        }
    }
    private void LoadData()
    {
        int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
        MagnetDBDB db = new MagnetDBDB();
        var data = from n in db.MagnetSchools
                   where n.GranteeID == granteeID
                   && n.ReportYear==reportyear
                   && n.isActive
                   orderby n.SchoolName
                   select new
                   {
                       n.ID,
                       n.SchoolName,
                       PartI = "<a href='GPRAI.aspx?id=" + n.ID.ToString() + "'>Add/Edit</a>",
                       PartII = "<a href='GPRAII.aspx?id=" + n.ID.ToString() + "'>Add/Edit</a>",
                       PartIII = "<a href='GPRAIII.aspx?id=" + n.ID.ToString() + "'>Add/Edit</a>",
                       PartIV = "<a href='GPRAIV.aspx?id=" + n.ID.ToString() + "'>Add/Edit</a>"
                   };
        if (granteeID != 62 && granteeID != 63) //New York City Community School Districts 13 and 15 (62)
            data.OrderBy(x => x.SchoolName);        //New York City Community School District 28 (63)
        

        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}