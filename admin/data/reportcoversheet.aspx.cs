﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_reportcoversheet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            hfGranteeID.Value = granteeID.ToString();
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);
            lblGranteeName.Text = "Grantee Name:<b> " + grantee.GranteeName + "</b>";
            txtPRAward.Text = grantee.PRAward;
            txtNCESID.Text = grantee.NCESID;

            int reportid = 0;
            if (Session["ReportID"] != null)
            {
                reportid = (int)Session["ReportID"];
                hfReportID.Value = reportid.ToString();
            }
            else
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            } 
            
            //int reportPeriod = 1;
            //if (Session["ReportPeriodID"] != null)
            //    reportPeriod = (int)Session["ReportPeriodID"];
            //var reports = GranteeReport.Find(x => x.GranteeID == granteeID && x.ReportPeriodID == reportPeriod && x.ReportType == false);
            //if (reports.Count > 0)
            {
                //Session["ReportID"] = reports[0].ID;
                //hfReportID.Value = reports[0].ID.ToString();
                GranteeReport report = GranteeReport.SingleOrDefault(x=>x.ID == Convert.ToInt32(hfReportID.Value));
                var sheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == reportid );
                if (sheets.Count > 0)
                {
                    hfID.Value = sheets[0].ID.ToString();
                    txtReportPeriodStart.Text = report.ReportStart;
                    txtReportPeriodEnd.Text = report.ReportEnd;
                    if (sheets[0].PreviousFederalFunds != null) txtPreviousFederalFunds.Text = sheets[0].PreviousFederalFunds.ToString();
                    if (sheets[0].PreviousNonFederalFunds != null) txtPreviousNonFederalFunds.Text = sheets[0].PreviousNonFederalFunds.ToString();
                    if (sheets[0].CurrentFederalFunds != null) txtCurrentFederalFunds.Text = sheets[0].CurrentFederalFunds.ToString();
                    if (sheets[0].CurrentNonFederalFunds != null) txtCurrentNonFederalFunds.Text = sheets[0].CurrentNonFederalFunds.ToString();
                    if (sheets[0].EntireFederalFunds != null) txtEntireFederalFunds.Text = sheets[0].EntireFederalFunds.ToString();
                    if (sheets[0].EntireNonFederalFunds != null) txtEntireNonFederalFunds.Text = sheets[0].EntireNonFederalFunds.ToString();
                    if (sheets[0].IndirectCost != null)
                    {
                        rblClaimingIndrectCost.SelectedValue = Convert.ToInt32(sheets[0].IndirectCost).ToString();
                        if (rblClaimingIndrectCost.SelectedIndex == 0)
                        {
                            if (sheets[0].IndirectCostApproved != null)
                            {
                                rblRageAggrement.SelectedValue = Convert.ToInt32(sheets[0].IndirectCostApproved).ToString();
                                if (rblRageAggrement.SelectedIndex == 0)
                                {
                                    txtAggrementStart.Text = sheets[0].IndirectCostStart;
                                    txtAggrementEnd.Text = sheets[0].IndirectCostEnd;
                                    if (sheets[0].EDApproved != null) rbED.Checked = (bool)sheets[0].EDApproved;
                                    if (!string.IsNullOrEmpty(sheets[0].OtherAgency))
                                        rbOtherAgency.Checked = true;
                                    txtOtherApprovalAgnecy.Text = sheets[0].OtherAgency;
                                    if (sheets[0].RateType != null)
                                    {
                                        switch (sheets[0].RateType)
                                        {
                                            case 1:
                                                rbProvisional.Checked = true;
                                                break;
                                            case 2:
                                                rbFinal.Checked = true;
                                                break;
                                            case 3:
                                                rbOtherRateType.Checked = true;
                                                txtOtherRateType.Text = sheets[0].OtherRateType;
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    EnableDisableC(false);
                                }
                            }
                        }
                        else
                        {
                            EnableDisableBC(false);
                        }
                    }
                    if (sheets[0].RestrictedRateProgram != null)
                        cbIncludedApprovedAgreement.Checked = (bool)sheets[0].RestrictedRateProgram;
                    if (sheets[0].Comply34CFR != null)
                        cbComplyCFR.Checked = (bool)sheets[0].Comply34CFR;
                    if (sheets[0].AnnualCertificationApproval != null)
                        ddlIRBApproval.SelectedIndex = (int)sheets[0].AnnualCertificationApproval;
                    if (sheets[0].CompletionDate != null)
                        ddlProjectStatusChart.SelectedIndex = (bool)sheets[0].CompletionDate == true ? 1 : 2;
                    if (sheets[0].IndirectCostRate != null)
                        txtIndirectCostRate.Text = Convert.ToString(sheets[0].IndirectCostRate);
                    if (sheets[0].IndirectCosts != null)
                        txtIndirectCost.Text = Convert.ToString(sheets[0].IndirectCost);
                    if (sheets[0].RestrictedIndirectCostRate != null)
                        txtRestrictedIndirectCostRate.Text = Convert.ToString(sheets[0].RestrictedIndirectCostRate);
                    if (sheets[0].TrainingStipends != null)
                        txtTrainingStipends.Text = Convert.ToString(sheets[0].TrainingStipends);
                    txtDataAvailable.Text = sheets[0].DataAvailableDate;
                    txtExecutiveSummary.Text = sheets[0].ExecutiveSummary;
                }
            }
            /*else
            {
                GranteeReport report = new GranteeReport();
                report.Preparer = Username;
                report.ReportPeriodID = reportPeriod;
                report.GranteeID = granteeID;
                report.ReportType = false;
                report.CreateDate = DateTime.Now;
                report.Save();

                hfReportID.Value = report.ID.ToString();
                Session["ReportID"] = report.ID;

                GranteePerformanceCoverSheet sheet = new GranteePerformanceCoverSheet();
                sheet.GranteeReportID = report.ID;
                sheet.Save();

                hfID.Value = sheet.ID.ToString();
            }*/
        }
    }
    private void EnableDisableBC(bool val)
    {
        rblRageAggrement.Enabled = val;
        txtAggrementStart.Enabled = val;
        txtAggrementEnd.Enabled = val;
        rbED.Enabled = val;
        rbOtherAgency.Enabled = val;
        txtOtherApprovalAgnecy.Enabled = val;
        rbFinal.Enabled = val;
        rbOtherRateType.Enabled = val;
        rbProvisional.Enabled = val;
        txtOtherRateType.Enabled = val;
    }
    private void EnableDisableC(bool val)
    {
        txtAggrementStart.Enabled = val;
        txtAggrementEnd.Enabled = val;
        rbED.Enabled = val;
        rbOtherAgency.Enabled = val;
        txtOtherApprovalAgnecy.Enabled = val;
        rbFinal.Enabled = val;
        rbOtherRateType.Enabled = val;
        rbProvisional.Enabled = val;
        txtOtherRateType.Enabled = val;
    }
    protected void OnClaimingIndirect(object sender, EventArgs e)
    {
        if (rblClaimingIndrectCost.SelectedIndex == 1)
        {
            EnableDisableBC(false);
        }
        else
        {
            EnableDisableBC(true);
        }
    }
    protected void OnRateAgreementChanged(object sender, EventArgs e)
    {
        if (rblRageAggrement.SelectedIndex == 1)
        {
            EnableDisableC(false);
        }
        else
        {
            EnableDisableC(true);
        }
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        SaveData();
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        SaveData();
        int granteeID = Convert.ToInt32(hfGranteeID.Value);
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

        GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        var sheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));
        GranteePerformanceCoverSheet sheet = sheets[0];
        var directors = MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(granteeID));

        //Print
        string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
        if (File.Exists(TmpPDF))
            File.Delete(TmpPDF);
        string SummaryPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
        if (File.Exists(SummaryPDF))
            File.Delete(SummaryPDF);
        string Stationery = Server.MapPath("../doc/") + grantee.PRAward + ".pdf";
        using (System.IO.MemoryStream output = new MemoryStream())
        {
            Document document = new Document(PageSize.A4, 40f, 20f, 170f, 30f);
            try
            {
                PdfStamper ps = null;

                // read existing PDF document
                PdfReader r = new PdfReader(Server.MapPath("../doc/coversheet.pdf"));
                ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                AcroFields af = ps.AcroFields;
                af.SetField("1 PRAward #", grantee.PRAward);
                af.SetField("2 Grantee NCES ID #", grantee.NCESID);
                af.SetField("3 Project Title", grantee.ProjectTitle);
                af.SetField("4 Grantee Name Block 1 of the Grant Award Notification", grantee.GranteeName);

                if (report.ReportPeriodID == 4)
                {
                    af.SetField("Final Performance Report", "Yes");
                }
                else
                {
                    af.SetField("Annual Performance Report", "Yes");
                }

                if (directors.Count > 0)
                {
                    af.SetField("6 Project Director See instructions Name", directors[0].ContactFirstName + " " + directors[0].ContactLastName);
                    af.SetField("Project Director Title", directors[0].ContactTitle);
                    if (!string.IsNullOrEmpty(directors[0].Phone))
                    {
                        if (directors[0].Phone.Length >= 4)
                            af.SetField("Project Phone Area", directors[0].Phone.Substring(1, 3));
                        if (directors[0].Phone.Length >= 8)
                            af.SetField("Project Phone A", directors[0].Phone.Substring(6, 3));
                        if (directors[0].Phone.Length >= 13)
                            af.SetField("Project Phone B", directors[0].Phone.Substring(10, 4));

                    }
                    if (!string.IsNullOrEmpty(directors[0].Fax))
                    {
                        if (directors[0].Fax.Length >= 4)
                            af.SetField("Project Fax Area", directors[0].Fax.Substring(1, 3));
                        if (directors[0].Fax.Length >= 8)
                            af.SetField("Project Fax A", directors[0].Fax.Substring(6, 3));
                        if (directors[0].Fax.Length >= 13)
                            af.SetField("Project Fax B", directors[0].Fax.Substring(10, 4));

                    } af.SetField("Project Phone Ext", "");
                    af.SetField("Email Address", directors[0].Email);
                }

                if (!string.IsNullOrEmpty(report.ReportStart))
                {
                    string[] strs = report.ReportStart.Split('/');
                    af.SetField("Reporting From Month", ManageUtility.PaddingString(strs[0]));
                    af.SetField("Reporting From Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("Reporting From Year", strs[2]);
                }
                if (!string.IsNullOrEmpty(report.ReportEnd))
                {
                    string[] strs = report.ReportEnd.Split('/');
                    af.SetField("Reporting To Month", ManageUtility.PaddingString(strs[0]));
                    af.SetField("Reporting To Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("Reporting To Year", strs[2]);
                }

                af.SetField("Federal Grant Fundsa Previous Budget Period", "$" + (sheet.PreviousFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.PreviousFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Sharea Previous Budget Period", "$" + (sheet.PreviousNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.PreviousNonFederalFunds)));
                af.SetField("Federal Grant Fundsb Current Budget Period", "$" + (sheet.CurrentFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.CurrentFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Shareb Current Budget Period", "$" + (sheet.CurrentNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.CurrentNonFederalFunds)));
                af.SetField("Federal Grant Fundsc Entire Project Period For Final Performance Reports only", "$" + (sheet.EntireFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.EntireFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Sharec Entire Project Period For Final Performance Reports only", "$" + (sheet.EntireNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.EntireNonFederalFunds)));

                if ((bool)sheet.IndirectCost)
                    af.SetField("9a Yes", "Yes");
                else
                    af.SetField("9a No", "Yes");

                if ((bool)sheet.IndirectCostApproved)
                    af.SetField("9b Yes", "Yes");
                else
                    af.SetField("9b No", "Yes");

                if (!string.IsNullOrEmpty(sheet.IndirectCostStart))
                {
                    string[] strs = sheet.IndirectCostStart.Split('/');
                    af.SetField("9c Period From", ManageUtility.PaddingString(strs[0]));
                    af.SetField("9c Period From Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("9c Period From Year", strs[2]);
                }

                if (!string.IsNullOrEmpty(sheet.IndirectCostEnd))
                {
                    string[] strs = sheet.IndirectCostEnd.Split('/');
                    af.SetField("9c Period To", ManageUtility.PaddingString(strs[0]));
                    af.SetField("9c Period To Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("9c Period To Year", strs[2]);
                }

                if ((bool)sheet.EDApproved)
                    af.SetField("9c Yes", "Yes");
                if (!string.IsNullOrEmpty(sheet.OtherAgency))
                {
                    af.SetField("9c No", "Yes");
                    af.SetField("Approving agency other", sheet.OtherAgency);
                }

                switch ((int)sheet.RateType)
                {
                    case 1:
                        af.SetField("Type of rate provisional", "Yes");
                        break;
                    case 2:
                        af.SetField("Type of rate final", "Yes");
                        break;
                    case 3:
                        af.SetField("Type of rate other", "Yes");
                        break;
                }
                if (!string.IsNullOrEmpty(sheet.OtherRateType))
                    af.SetField("Type of rate other specify", sheet.OtherRateType);

                if ((bool)sheet.RestrictedRateProgram)
                    af.SetField("9d Is included in approved indirect cost rate agreement", "Yes");

                if ((bool)sheet.Comply34CFR)
                    af.SetField("9d complies with 34 CFR 76.564", "Yes");

                switch ((int)sheet.AnnualCertificationApproval)
                {
                    case 1:
                        af.SetField("IRB attached Yes", "Yes");
                        break;
                    case 2:
                        af.SetField("IRB attached No", "Yes");
                        break;
                    case 3:
                        af.SetField("IRB attached N/A", "Yes");
                        break;
                }

                if ((bool)sheet.CompletionDate)
                    af.SetField("11a Performance Measures Status Yes", "Yes");
                else
                    af.SetField("11a Performance Measures Status No", "Yes");

                if (!string.IsNullOrEmpty(sheet.DataAvailableDate))
                {
                    string[] strs = sheet.DataAvailableDate.Split('/');
                    af.SetField("b If no when will the data be available", strs[0]);
                    af.SetField("b If no when will the data be available Month", strs[1]);
                    af.SetField("b If no when will the data be available Year", strs[2]);
                }

                //af.SetField("ExecutiveSummary", sheet.ExecutiveSummary);

                ps.FormFlattening = true;

                r.Close();
                ps.Close();

                PdfReader reader = new PdfReader(TmpPDF);
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, new FileStream(SummaryPDF, FileMode.Create));
                document.Open();
                PdfContentByte pdfContentByte = pdfWriter.DirectContent;

                document.NewPage();
                PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                pdfContentByte.AddTemplate(importedPage, 0, 0);

                document.NewPage();
                document.Add(new Paragraph(new Chunk(sheet.ExecutiveSummary)));

                document.Close();

                //Add stamp
                PdfReader o_reader = new PdfReader(SummaryPDF);
                PdfReader s_reader = new PdfReader(Stationery);
                PdfStamper stamper = new PdfStamper(o_reader, output);

                PdfImportedPage page = stamper.GetImportedPage(s_reader, 1);
                int n = o_reader.NumberOfPages;
                PdfContentByte background;
                for (int i = 2; i <= n; i++)
                {
                    background = stamper.GetUnderContent(i);
                    background.AddTemplate(page, 0, 0);
                }
                stamper.Close();

                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment;filename=Coversheet " + grantee.GranteeName + ".pdf");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("Cover Sheet report:", ex);
            }
            finally
            {
                if (File.Exists(TmpPDF))
                    File.Delete(TmpPDF);
                if (File.Exists(SummaryPDF))
                    File.Delete(SummaryPDF);
                output.Close();
            }
        }
    }
    private void SaveData()
    {
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(hfGranteeID.Value));
        grantee.PRAward = txtPRAward.Text;
        grantee.NCESID = txtNCESID.Text;
        grantee.Save();

        GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        report.ReportStart = txtReportPeriodStart.Text;
        report.ReportEnd = txtReportPeriodEnd.Text;
        report.Preparer = HttpContext.Current.User.Identity.Name;
        report.SubmitDate = DateTime.Now;
        report.Save();

        GranteePerformanceCoverSheet sheet = GranteePerformanceCoverSheet.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        if (!string.IsNullOrEmpty(txtPreviousFederalFunds.Text)) sheet.PreviousFederalFunds = Convert.ToDecimal(txtPreviousFederalFunds.Text);
        if (!string.IsNullOrEmpty(txtPreviousNonFederalFunds.Text)) sheet.PreviousNonFederalFunds = Convert.ToDecimal(txtPreviousNonFederalFunds.Text);
        if (!string.IsNullOrEmpty(txtCurrentFederalFunds.Text)) sheet.CurrentFederalFunds = Convert.ToDecimal(txtCurrentFederalFunds.Text);
        if (!string.IsNullOrEmpty(txtCurrentNonFederalFunds.Text)) sheet.CurrentNonFederalFunds = Convert.ToDecimal(txtCurrentNonFederalFunds.Text);
        if (!string.IsNullOrEmpty(txtEntireFederalFunds.Text)) sheet.EntireFederalFunds = Convert.ToDecimal(txtEntireFederalFunds.Text);
        if (!string.IsNullOrEmpty(txtEntireNonFederalFunds.Text)) sheet.EntireNonFederalFunds = Convert.ToDecimal(txtEntireNonFederalFunds.Text);
        sheet.IndirectCost = rblClaimingIndrectCost.SelectedIndex == 0 ? true : false;
        sheet.IndirectCostApproved = rblRageAggrement.SelectedIndex == 0 ? true : false;
        sheet.IndirectCostStart = txtAggrementStart.Text;
        sheet.IndirectCostEnd = txtAggrementEnd.Text;
        sheet.EDApproved = rbED.Checked ? true : false;
        sheet.OtherAgency = txtOtherApprovalAgnecy.Text;
        sheet.RateType = rbProvisional.Checked ? 1 : rbFinal.Checked ? 2 : (rbOtherRateType.Checked || !string.IsNullOrEmpty(txtOtherRateType.Text)) ? 3 : 4;
        sheet.OtherRateType = txtOtherRateType.Text;
        sheet.RestrictedRateProgram = cbIncludedApprovedAgreement.Checked ? true : false;
        sheet.Comply34CFR = cbComplyCFR.Checked ? true : false;
        sheet.AnnualCertificationApproval = ddlIRBApproval.SelectedIndex;
        sheet.CompletionDate = ddlProjectStatusChart.SelectedIndex == 1 ? true : false;
        sheet.DataAvailableDate = txtDataAvailable.Text;
        sheet.ExecutiveSummary = txtExecutiveSummary.Text;
        sheet.IndirectCostRate = string.IsNullOrEmpty(txtIndirectCostRate.Text) ? 0 : Convert.ToDecimal(txtIndirectCostRate.Text);
        sheet.IndirectCosts = string.IsNullOrEmpty(txtIndirectCost.Text) ? 0 : Convert.ToDecimal(txtIndirectCost.Text);
        sheet.RestrictedIndirectCostRate = string.IsNullOrEmpty(txtRestrictedIndirectCostRate.Text) ? 0 : Convert.ToDecimal(txtRestrictedIndirectCostRate.Text);
        sheet.TrainingStipends = string.IsNullOrEmpty(txtTrainingStipends.Text) ? 0 : Convert.ToDecimal(txtTrainingStipends.Text);
        sheet.Save();

        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Record saved.');</script>", false);
    }
}