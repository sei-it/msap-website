﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_rgidefinition : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //School
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == granteeID).OrderBy(x => x.SchoolName))
            {
                ddlSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
            }

            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 20 && x.TypeIndex > -9))
            {
                ddlIndian.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlAsian.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlBlack.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlHispanic.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
                ddlHawaiian.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("managergidefinition.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadData(id);
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetRGIDefinition item = new MagnetRGIDefinition();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetRGIDefinition.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
            item.RGIStage = 0;
        }
        item.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        item.AmericanIndian = Convert.ToInt32(ddlIndian.SelectedValue);
        item.Asian = Convert.ToInt32(ddlAsian.SelectedValue);
        item.BlackAfricanAmerican = Convert.ToInt32(ddlBlack.SelectedValue);
        item.NativeHawaiian = Convert.ToInt32(ddlHawaiian.SelectedValue);
        item.Hispanic = Convert.ToInt32(ddlHispanic.SelectedValue);
        item.RGIDefinition = txtRGIDefinition.Text;
        item.Notes = txtNotes.Text;
        item.Save();
        hfID.Value = item.ID.ToString();
    }
    private void LoadData(int SIPID)
    {
        MagnetRGIDefinition item = MagnetRGIDefinition.SingleOrDefault(x => x.ID == SIPID);
        ddlSchool.SelectedValue = item.SchoolID.ToString();
        ddlIndian.SelectedValue = Convert.ToString(item.AmericanIndian);
        ddlAsian.SelectedValue = Convert.ToString(item.Asian);
        ddlBlack.SelectedValue = Convert.ToString(item.BlackAfricanAmerican);
        ddlHawaiian.SelectedValue = Convert.ToString(item.NativeHawaiian);
        ddlHispanic.SelectedValue = Convert.ToString(item.Hispanic);
        txtRGIDefinition.Text = item.RGIDefinition;
        txtNotes.Text = item.Notes;
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("managergidefinition.aspx");
    }
}