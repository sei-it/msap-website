﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="GPRAV.aspx.cs" Inherits="admin_data_GPRAV" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit GPRA Performance Measure 6 Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript" >
        function percentage_vald1(sender, args) {
            var deno = document.getElementById('<%=txtAllcohort.ClientID%>').value;
            var num = document.getElementById('<%=txtAllgraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
                sender.textContent = sender.innerText = sender.innerHTML = "numerator or denominator is required."
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }

        }

        function percentage_vald2(sender, args) {
            var deno = document.getElementById('<%=txtAmericanIndiancohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtAmericanIndiangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald3(sender, args) {
            var deno  = document.getElementById('<%=txtAsiancohort.ClientID%>').value;
            var num = document.getElementById('<%=txtAsiangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }


        //////////////////////////////
        function percentage_vald4(sender, args) {
            var deno = document.getElementById('<%=txtAfricanAmericancohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtAfricanAmericangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald5(sender, args) {
            var deno = document.getElementById('<%=txtHispanicLatinocohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtHispanicLatinograduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald6(sender, args) {
            var deno = document.getElementById('<%=txtNativeHawaiiancohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtNativeHawaiiangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald7(sender, args) {
            var deno = document.getElementById('<%=txtWhitecohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtWhitegraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald8(sender, args) {
            var deno = document.getElementById('<%=txtMoreRacescohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtMoreRacesgraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald9(sender, args) {
            var deno = document.getElementById('<%=txtEconomicallycohort.ClientID%>').value;
            var  num = document.getElementById('<%=txtEconomicallygraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }

        function percentage_vald10(sender, args) {
            var deno = document.getElementById('<%=txtEnglishlearnerscohort.ClientID%>').value;
            var num = document.getElementById('<%=txtEnglishlearnersgraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
            else if ((num != '' && deno != '') && (num > deno)) {
                sender.textContent = sender.innerText = sender.innerHTML = "Error: A numerator is greater than a denominator."
                args.IsValid = false;
            }
        }


        function txtpercentage1() {
            ch1 = $("#<%=txtAllcohort.ClientID%>").val().replace(",","");
            ch2 = $("#<%=txtAllgraduated.ClientID%>").val().replace(",", "") ;
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtallpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtallpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage2() {
            ch1 = $("#<%=txtAmericanIndiancohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtAmericanIndiangraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtAmericanIndianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtAmericanIndianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage3() {
            ch1 = $("#<%=txtAsiancohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtAsiangraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtAsianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtAsianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage4() {
            ch1 = $("#<%=txtAfricanAmericancohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtAfricanAmericangraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtAfricanAmericanpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtAfricanAmericanpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage5() {
            ch1 = $("#<%=txtHispanicLatinocohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtHispanicLatinograduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtHispanicLatinopercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtHispanicLatinopercentage.ClientID%>").val('');
            }
        }

        function txtpercentage6() {
            ch1 = $("#<%=txtNativeHawaiiancohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtNativeHawaiiangraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtNativeHawaiianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtNativeHawaiianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage7() {
            ch1 = $("#<%=txtWhitecohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtWhitegraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtWhitepercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtWhitepercentage.ClientID%>").val('');
            }
        }

        function txtpercentage8() {
            ch1 = $("#<%=txtMoreRacescohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtMoreRacesgraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtMoreRacespercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtMoreRacespercentage.ClientID%>").val('');
            }
        }

        function txtpercentage9() {
            ch1 = $("#<%=txtEconomicallycohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtEconomicallygraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtEconomicallypercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtEconomicallypercentage.ClientID%>").val('');
            }
        }

        function txtpercentage10() {
            ch1 = $("#<%=txtEnglishlearnerscohort.ClientID%>").val().replace(",", "");
            ch2 = $("#<%=txtEnglishlearnersgraduated.ClientID%>").val().replace(",", "");
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch2 / ch1) * 100;
                $("#<%=txtEnglishlearnerspercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtEnglishlearnerspercentage.ClientID%>").val('');
            }
        }
    
    </script>

    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
    </script>
    <style type="text/css">
        .msapDataTbl tr td:first-child
        {
            color: #4e8396 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
            text-align: center;
        }
        .msapDataTbl th
        {
            color: #000;
            text-align: left;
        }
        .TDCenterClass
        {
            text-align: center !important;
        }
        .style1
        {
            width: 290px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managegpra.aspx">GPRA Table</a> <span class="greater_than">&gt;</span>
            Part V. GPRA Performance Measure 6 Data</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <div class="GPRA_titles">
            <asp:Label ID="lblSchoolName" runat="server"></asp:Label></div>
        <div class="tab_area">
            <ul class="tabs">
                <li class="tab_active">Part V</li>
                <li><a runat="server" id="tabPartIV" href="gpraiv.aspx">Part IV</a></li>
                <li><a runat="server" id="tabPartIII" href="gpraiii.aspx">Part III</a></li>
                <li><a runat="server" id="tabPartII" href="gpraii.aspx">Part II</a></li>
                <li><a runat="server" id="tabPartI" href="gprai.aspx">Part I</a></li>
            </ul>
        </div>
        <br />
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Part V. GPRA Performance Measure 6 Data</b> </span>
        <p>
            Part V of the GPRA Table collects data for the percentage of magnet schools that received assistance that meet the State’s annual
measurable objectives and, for high schools, graduation rate targets at least three years after Federal funding ends. Follow the
instructions in the GPRA Guide to complete the items in each row. When you have finished entering data for this page, click on Save
Record before proceeding.
        </p>
      
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfPerformanceMeasure" runat="server" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <br /><br />
        <hr style="color: #A9DEF2;" />
        <table class="msapDataTbl">
            <tr>
                <th colspan="5" style="color: #F58220; text-align:center;" >
                    Magnet school graduation rate
                </th>
            </tr>
            <tr>
            <td style="border-bottom: 2px solid #F58220; text-align:center;" class="style1"></td>
            <td style="border-bottom: 2px solid #F58220 !important; text-align:center;"></td>
            <td colspan="3" style="border-bottom: 2px solid #F58220 !important; text-align:center;">
                <b>2013-14</b></td>
            </tr>
            <tr >
            <td style="border-bottom: 2px solid #F58220; color:Black; text-align:center;" 
                    class="style1"><b>Student groups</b></td>
            <td style="border-bottom: 2px solid #F58220 !important; text-align:center;"><b>Adjusted 4-year cohort</b></td>
            <td  style="border-bottom: 2px solid #F58220 !important; text-align:center;">
                <b>Number of students who graduated</b></td>
             <td colspan="2"  style="border-bottom: 2px solid #F58220 !important; text-align:center;">
                <b>Percentage of students who graduated</b></td>
             
            </tr>
            <tr>
                <td class="style1">
                    51. <b>All</b> students</td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAllcohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                      onkeyup="txtpercentage1()"   CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">

                    <telerik:RadNumericTextBox MinValue="0" ID="txtAllgraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                     onkeyup="txtpercentage1()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtallpercentage" runat="server" Skin="MyCustomSkin" Enabled="false" ViewStateMode="Enabled"
                        CssClass="change" EnableEmbeddedSkins="false"  Width="100"></telerik:RadTextBox>
                </td>
                <td>
                    <asp:CustomValidator ID="CustomValidator1" runat="server" 
       ClientValidationFunction="percentage_vald1"></asp:CustomValidator> 
                </td>
            </tr>
            <tr>
                <td class="style1">
                    52. <b>American Indian or Alaska Native</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAmericanIndiancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage2()"   CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAmericanIndiangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage2()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtAmericanIndianpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false" Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator2" runat="server" 
       ClientValidationFunction="percentage_vald2" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    53. <b>Asian</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAsiancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage3()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAsiangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage3()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtAsianpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"   Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator3" runat="server" 
       ClientValidationFunction="percentage_vald3" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    54. <b>Black or African-American</b> students
                </td>
               <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAfricanAmericancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage4()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
               <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtAfricanAmericangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage4()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
               <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtAfricanAmericanpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"
                        Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator4" runat="server" 
       ClientValidationFunction="percentage_vald4" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    55. <b>Hispanic or Latino</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtHispanicLatinocohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage5()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtHispanicLatinograduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage5()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtHispanicLatinopercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"   Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator5" runat="server" 
       ClientValidationFunction="percentage_vald5" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    56. <b>Native Hawaiian or Other Pacific Islander</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtNativeHawaiiancohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage6()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtNativeHawaiiangraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage6()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtNativeHawaiianpercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false" Enabled="false"  Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator6" runat="server" 
       ClientValidationFunction="percentage_vald6" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    57. <b>White</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtWhitecohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage7()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtWhitegraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage7()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtWhitepercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false" Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator7" runat="server" 
       ClientValidationFunction="percentage_vald7" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    58. <b>Two or more races</b>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtMoreRacescohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage8()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtMoreRacesgraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage8()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtMoreRacespercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"  Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator8" runat="server" 
       ClientValidationFunction="percentage_vald8" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
              <tr>
                <td class="style1">
                    59. <b>Economically disadvantaged</b> students
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtEconomicallycohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage9()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtEconomicallygraduated" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage9()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtEconomicallypercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false"  Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator9" runat="server" 
       ClientValidationFunction="percentage_vald9" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    60. <b>English learners</b></td>
             <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtEnglishlearnerscohort" runat="server" Skin="MyCustomSkin" MaxLength="6"
                    onkeyup="txtpercentage10()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadNumericTextBox MinValue="0" ID="txtEnglishlearnersgraduated" runat="server" Skin="MyCustomSkin"  MaxLength="6"
                    onkeyup="txtpercentage10()"    CssClass="change" EnableEmbeddedSkins="false"  NumberFormat-DecimalDigits="0"  Width="100">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="TDWithBottomNoRightBorder">
                    <telerik:RadTextBox MinValue="0" ID="txtEnglishlearnerspercentage" runat="server" Skin="MyCustomSkin"
                        CssClass="change" EnableEmbeddedSkins="false"  Enabled="false" Width="100">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <asp:CustomValidator ID="CustomValidator10" runat="server" 
       ClientValidationFunction="percentage_vald10" ErrorMessage="numerator or denominator is required."></asp:CustomValidator>
                </td>
            </tr>
        </table>
        <div style="text-align: right; margin-top: 20px;">
            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn1 postbutton" Text="Save Record"
                OnClick="OnSave" />
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <a href="manageGPRA.aspx">Back to GPRA table</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                id="LinkI" href="GPRAI.aspx">Part I</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                    id="LinkII" href="GPRAII.aspx">Part II</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                        id="LinkIII" href="GPRAIII.aspx">Part III</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server"  id="LinkIV" href="GPRAIV.aspx">Part IV</a>&nbsp;&nbsp;&nbsp;&nbsp;
            Part V
        </div>
    </div>
</asp:Content>



