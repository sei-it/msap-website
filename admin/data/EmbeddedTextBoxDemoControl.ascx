﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmbeddedTextBoxDemoControl.ascx.cs"
    Inherits="EmbeddedTextBoxDemoControl" %>
<%@ Register TagPrefix="dn" Assembly="Devarchive.Net" Namespace="Devarchive.Net" %>
<dn:gridviewfixedheaderextender runat="server" id="fhExtender" targetcontrolid="gvMembers" />
<dn:gridviewcontrolembedder runat="server" id="ceExtender" hovercssclass="hover"
    rowidattributename="rowID" targetcontrolid="gvMembers" ongridviewcontrolembedderrowready="RowReady">
    <Columns>
        <dn:GridViewControlEmbedderColumn ColumnIndex="1" ControlType="TextBox" CssClass="msapDataTxt" />
    </Columns>
</dn:gridviewcontrolembedder>
<asp:GridView SkinID="Simple" ID="gvMembers" AllowPaging="false" runat="server" AutoGenerateColumns="False"
    CssClass="msapGridView2">
    <Columns>
        <asp:TemplateField HeaderText="School Name" ItemStyle-Width="350px" HeaderStyle-Width="350px"
            ControlStyle-BorderWidth="0">
            <ItemStyle CssClass="rightBorderTD"/>
            <ItemTemplate>
                <%#Eval("SchoolName")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="First School Year as a Magnet School" ItemStyle-Width="350px" HeaderStyle-Width="350px"
            >
            <ItemStyle BorderWidth="0" />
            <ItemTemplate>
                <%#Eval("SchoolYear")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<asp:UpdatePanel runat="server" ID="up" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Button runat="server" ID="btn" OnClick="Click" CssClass="surveyBtn1" Text="Save Changes" />
        <pre runat="server" id="pre">
        </pre>
    </ContentTemplate>
</asp:UpdatePanel>
