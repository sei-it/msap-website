﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_applicationpool : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //Report type
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == granteeID).OrderBy(x => x.SchoolName))
            {
                ddlSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("manageapplicationpool.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadData(id);
                }
            }
        }
    }
    protected void OnTextChanged(object sender, EventArgs e)
    {
        int Total = 0;
        Total += string.IsNullOrEmpty(txtAfricanAmerican.Text) ? 0 : Convert.ToInt32(txtAfricanAmerican.Text);
        Total += string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(txtAmericanIndian.Text);
        Total += string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(txtAsian.Text);
        Total += string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(txtHispanic.Text);
        Total += string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(txtWhite.Text);
        Total += string.IsNullOrEmpty(txtMultiRacial.Text) ? 0 : Convert.ToInt32(txtMultiRacial.Text);
        Total += string.IsNullOrEmpty(txtUnknown.Text) ? 0 : Convert.ToInt32(txtUnknown.Text);
        txtTotal.Text = Convert.ToString(Total);
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetApplicationPool item = new MagnetApplicationPool();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetApplicationPool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
        }
        item.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        item.AmericanIndian = string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(txtAmericanIndian.Text);
        item.Asian = string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(txtAsian.Text);
        item.AfircanAmerican = string.IsNullOrEmpty(txtAfricanAmerican.Text) ? 0 : Convert.ToInt32(txtAfricanAmerican.Text);
        item.Hispanic = string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(txtHispanic.Text);
        item.White = string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(txtWhite.Text);
        item.MultiRacial = string.IsNullOrEmpty(txtMultiRacial.Text) ? 0 : Convert.ToInt32(txtMultiRacial.Text);
        item.UnknownRacial = string.IsNullOrEmpty(txtUnknown.Text) ? 0 : Convert.ToInt32(txtUnknown.Text);
        item.TotalApplicants= string.IsNullOrEmpty(txtTotal.Text) ? 0 : Convert.ToInt32(txtTotal.Text);
        item.Save();
        hfID.Value = item.ID.ToString();
    }
    private void LoadData(int SIPID)
    {
        MagnetApplicationPool item = MagnetApplicationPool.SingleOrDefault(x => x.ID == SIPID);
        ddlSchool.SelectedValue = Convert.ToString(item.SchoolID);
        txtAmericanIndian.Text = Convert.ToString(item.AmericanIndian);
        txtAfricanAmerican.Text = Convert.ToString(item.AfircanAmerican);
        txtAsian.Text = Convert.ToString(item.Asian);
        txtHispanic.Text = Convert.ToString(item.Hispanic);
        txtWhite.Text = Convert.ToString(item.White);
        txtMultiRacial.Text = Convert.ToString(item.MultiRacial);
        txtUnknown.Text = Convert.ToString(item.UnknownRacial);
        txtTotal.Text = Convert.ToString(item.TotalApplicants);
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("manageapplicationpool.aspx");
    }
}