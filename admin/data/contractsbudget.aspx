﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="contractsbudget.aspx.cs" Inherits="admin_data_contractsbudget" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Budget - Consultants/Contracts
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/tooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".screenshot").thumbPopup({
                imgSmallFlag: "../../images/question_mark-thumb.png",
                imgLargeFlag: "../../images/consultant.jpg"
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
       
         <h4 style="color:#F58220" >
           <a href="quarterreports.aspx">Main Menu</a> <img src="button_arrow.jpg" alt="" width="29" height="36" />Consultants/Contracts Budget Detail<span style="color:#F58220"><img src="button_arrow.jpg" alt="" width="29" height="36" /></span>
<asp:Label runat="server" ID="lblBudgetType"></asp:Label></h4>
        
        
        <p>
            For each consultant, enter the <b>name</b>, if known, <b>service to be provided</b>,
            <b>hourly rate</b>, and <b>estimated time on the project in hours</b>. Consultant
            fees in excess of $500 per day require additional justification and prior approval
            from ED. If you anticipate carryover funds at the end of this current fiscal year,
            provide a description for those funds encumbered for services received or rendered
            for the current budget period but may have: 1) not been completed; or 2) not been
            reimbursed.
            <img src="../../images/question_mark-thumb.png" class="screenshot" alt="Budget Detail Example" />
        </p>
        <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfReportType" runat="server" />
        <asp:HiddenField ID="hfSummaryID" runat="server" />
        <p>
            <strong>Consultant</strong></p>
        <asp:Button ID="Button5" runat="server" CssClass="surveyBtn1" Text="Add Budget Item"
            OnClick="OnNewConsultant" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="ConsultantName" SortExpression="" HeaderText="Name of Consultant" />
                <asp:BoundField DataField="ServicesProvided" SortExpression="" HeaderText="Services Provided" />
                <asp:BoundField DataField="HourlyRate" SortExpression="" HeaderText="Hourly Rate" />
                <asp:BoundField DataField="NumberOfHours" SortExpression="" HeaderText="Number of Hours"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="ApprovedFederalFunds" SortExpression="" HeaderText="Approved Federal Funds"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="BudgetExpenditures" SortExpression="" HeaderText="Budget Expenditures"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="Carryover" SortExpression="" HeaderText="Carryover" DataFormatString="{0:D}" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEditConsultant"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <p>
            <strong>Contracts</strong></p>
       <div > <asp:Button ID="Button6" runat="server" CssClass="surveyBtn1" Text="Add Budget Item"
            OnClick="OnNewContract" />
        <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="Item" SortExpression="" HeaderText="Item" />
                <asp:BoundField DataField="Location" SortExpression="" HeaderText="Location" />
                <asp:BoundField DataField="Detail" SortExpression="" HeaderText="Detail" />
                <asp:BoundField DataField="UnitCost" SortExpression="" HeaderText="Unit Cost" />
                <asp:BoundField DataField="NumberOfDays" SortExpression="" HeaderText="Number of Days"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="NumberOfPeople" SortExpression="" HeaderText="Number of People"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="ApprovedFederalFunds" SortExpression="" HeaderText="Approved Federal Funds"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="BudgetExpenditures" SortExpression="" HeaderText="Budget Expenditures"
                    DataFormatString="{0:D}" />
                <asp:BoundField DataField="Carryover" SortExpression="" HeaderText="Carryover" DataFormatString="{0:D}" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEditContract"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <%--Consultant Budget --%>
        <asp:Panel ID="Panel1" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                     Edit Budget Detail
                    <div style="float:right">
                        <asp:ImageButton ID="ImageButton1" ImageUrl="../../images/close.gif" runat="server" />
                    </div>
                </div>
                <table>
                    <tr>
                        <td>
                            Name of Consultant:
                        </td>
                        <td>
                            <asp:TextBox ID="txtConsultantName" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Services Provided:
                        </td>
                        <td>
                            <asp:TextBox ID="txtServicesProvided" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Hourly Rate:
                        </td>
                        <td>
                            <asp:TextBox ID="txtHourlyRate" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender4" TargetControlID="txtHourlyRate" Mask="999,999,999.99"
                                MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of Hours:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNumberOfHours" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender5" TargetControlID="txtNumberOfHours" Mask="999999"
                                MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Approved Federal Funds
                        </td>
                        <td>
                            <asp:TextBox ID="txtConsultantApprovedFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender6" TargetControlID="txtConsultantApprovedFunds"
                                Mask="999,999,999" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button1" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveConsultant" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton2" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeConsultantWindow" runat="server" TargetControlID="LinkButton2"
            PopupControlID="Panel1" DropShadow="true" OkControlID="ImageButton1" CancelControlID="ImageButton1"
            BackgroundCssClass="magnetMPE" Y="20" />
        <%--Contracts Budget --%>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                     Edit Budget Detail
                    <div style="float:right">
                        <asp:ImageButton ID="ImageButton2" ImageUrl="../../images/close.gif" runat="server" />
                    </div>
                </div>
                <table>
                    <tr>
                        <td>
                            Item:
                        </td>
                        <td>
                            <asp:TextBox ID="txtItem" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Location:
                        </td>
                        <td>
                            <asp:TextBox ID="txtLocation" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Detail:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDetail" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Unit Cost:
                        </td>
                        <td>
                            <asp:TextBox ID="txtUnitCost" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtUnitCost" Mask="999,999,999.99"
                                MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of Days:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNumberOfDays" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtNumberOfDays" Mask="999999"
                                MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of People:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNumberOfPeople" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtNumberOfPeople" Mask="999999"
                                MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Approved Federal Funds
                        </td>
                        <td>
                            <asp:TextBox ID="txtApprovedContractFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:MaskedEditExtender ID="MaskedEditExtender7" TargetControlID="txtApprovedContractFunds"
                                Mask="999,999,999" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveContract" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeContractWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="ImageButton2" CancelControlID="ImageButton2"
            BackgroundCssClass="magnetMPE" Y="20" />
        <p>
            Supplies Summary (100 word maximum)
        </p>
        <asp:TextBox ID="txtSummary" runat="server" Columns="100" Rows="4" TextMode="MultiLine"
            CssClass="msapDataTxt"></asp:TextBox><br/>
        <br /><div align="right"><asp:Button ID="Savebutton" runat="server" Text="Save Summary" OnClick="OnSaveSummary" CssClass="surveyBtn1" />
   </div> </div>
</asp:Content>
