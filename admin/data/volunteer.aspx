﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="volunteer.aspx.cs" Inherits="admin_data_volunteer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Desegregation Volunteer Plan
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {

                if (obj.id == "ctl00_ContentPlaceHolder1_PlanCopy") {
                    var who3 = document.getElementsByName('<%= PlanCopy.UniqueID %>')[0];
                    who3.value = "";

                    var who4 = who3.cloneNode(false);
                    who4.onchange = who3.onchange;
                    who3.parentNode.replaceChild(who4, who3);
                }

                else {
                    var who5 = document.getElementsByName('<%= SchoolBoard.UniqueID %>')[0];
                    who5.value = "";

                    var who6 = who5.cloneNode(false);
                    who6.onchange = who5.onchange;
                    who5.parentNode.replaceChild(who6, who5);
                }

                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="desegregation.aspx">Desegregation Plan</a> <span class="greater_than">&gt;</span> Voluntary
            Plan Upload</h4>
            <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <div class="titles_noTabs">
            Voluntary Plan Upload</div>
        <br /><br />
        
         <span class="Orange_headers">
	     	<img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; A Voluntary Plan<br />
         </span>
      
		<div class="Desg_content_div">
        	<p>
               <asp:Literal ID="ltlBothDoc" runat="server">If you selected “A Voluntary Plan”, you must attach <b>BOTH</b> of the following
                documents</asp:Literal>
                
      </p>
              <%--  <ol id="Desg_plan_list_Vol" runat="server">
                    <li>A copy of the plan<br />
                        <asp:FileUpload ID="PlanCopy1" runat="server" CssClass="msapDataTxt" style="margin-top:5px;margin-bottom:20px;" /></li>
                    <li>A copy of the school board resolution adopting and implementing the plan, or<br/> agreeing
                        to adopt and implement the plan upon the award of assistance.<br />
                        <asp:FileUpload ID="SchoolBoard" runat="server" CssClass="msapDataTxt" style="margin-top:5px;"/></li>
                </ol>--%>
                <table id="tblCopyPlan">
                <tr>
                <td>1.&nbsp;&nbsp;&nbsp; </td>
                <td> A copy of the plan</td>
                </tr>
                <tr>
                <td></td>
                <td><asp:FileUpload ID="PlanCopy" runat="server" CssClass="msapDataTxt" style="margin-top:5px;margin-bottom:20px;" /></td>
                </tr>
                </table>

                <div id="dvCopyschool" runat="server">
                <table id="tblcopySchool" runat="server">
                <tr>
                <td>2.&nbsp;&nbsp;&nbsp; </td>
                <td> A copy of the school board resolution adopting and implementing the plan, or<br/> agreeing
                        to adopt and implement the plan upon the award of assistance.</td>
                </tr>
                <tr>
                <td></td>
                <td>
                 <asp:FileUpload ID="SchoolBoard" runat="server" CssClass="msapDataTxt" style="margin-top:5px;"/></td>
                </tr>
                </table>
                </div>

                <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl" GridLines="None"
                    Width="300">
                    <Columns>
                        <asp:BoundField HeaderText="Uploaded Files" DataField="FilePath" HtmlEncode="false" />
                        <asp:TemplateField>
                            <ItemStyle CssClass="TDWithBottomBorder" />
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Both documents will be removed. Are you sure you want to delete these uploaded files?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>            
  </div> 
          <p>
                Any desegregation plan modification should be mailed to:</p>
            <div id="desg_address">
                Anna Hinton<br />
                US Department of Education<br />
                Office of Innovation &amp; Improvement<br />
                400 Maryland Avenue SW, Rm 4W229<br />
                Washington, DC 20202-5970</div>
    		 
                <div class="right_align">
                	<asp:Button ID="Button10" runat="server" CssClass="surveyBtn2" Text="Save & Upload"
                    OnClick="OnSave" />
                </div>  
</asp:Content>
