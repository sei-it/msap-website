﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managecontext.aspx.cs" Inherits="admin_data_managecontext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Context
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            <a href="datareport.aspx">Home</a> --> Context</h1>
        <asp:HiddenField ID="hfReportID" runat="server" />
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New Context" CssClass="msapBtn" OnClick="OnAddGrantee" />
        </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School Name" />
                <asp:BoundField DataField="SchoolType" SortExpression="" HeaderText="School Type" />
                <asp:BoundField DataField="HistoricalContext" SortExpression="" HeaderText="Historical Context" />
                <asp:BoundField DataField="TitleFunding" SortExpression="" HeaderText="Title I Funding" />
                <asp:BoundField DataField="PercentFRL" SortExpression="" HeaderText="Percent FRL" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hfID" runat="server" />
    </div>
</asp:Content>
