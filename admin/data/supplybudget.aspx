﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="supplybudget.aspx.cs" Inherits="admin_data_supplybudget" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Budget - Supplies
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/tooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".screenshot").thumbPopup({
                imgSmallFlag: "../../images/question_mark-thumb.png",
                imgLargeFlag: "../../images/supplies.jpg"
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
       
       
    <h4 style="color:#F58220" ><a href="quarterreports.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" /> Supplies Budget Detail <img src="button_arrow.jpg" width="29" height="36" /> 
      <asp:Label runat="server" ID="lblBudgetType"></asp:Label></h4>
       
        
            
        <p>
            List items by type (office supplies, postage, training material, copying paper,
            and expendable items such as books, etc.) and show the basis for the computation.
            For the computation, show the <b>unit price</b> and the number of <b>units purchased</b>,
            or show the <b>monthly rate</b> and the <b>number of months</b>. Generally, supplies
            include any materials that are expendable or consumed during the course of the project.
            If you anticipate carryover funds at the end of this current fiscal year, provide
            a description for those funds encumbered for services received or rendered for the
            current budget period but may have: 1) not been completed; or 2) not been reimbursed.
            <img src="../../images/question_mark-thumb.png" class="screenshot" alt="Budget Detail Example" />
        </p>
        <div align="right"><asp:Button ID="Button4" runat="server" CssClass="surveyBtn1" Text="Add Budget Item"
            OnClick="OnNewBudget" /></div>
        <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfReportType" runat="server" />
        <asp:HiddenField ID="hfSummaryID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false"
                AllowSorting="false" PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID"
                CssClass="msapTbl">
                <Columns>
                    <asp:BoundField DataField="Item" SortExpression="" HeaderText="Item" />
                    <asp:BoundField DataField="UnitPrice" SortExpression="" HeaderText="Unit Price" />
                    <asp:BoundField DataField="NumberOfUnits" SortExpression="" HeaderText="Number of Units"
                        DataFormatString="{0:D}" />
                    <asp:BoundField DataField="MonthlyRate" SortExpression="" HeaderText="Monthly Rate" />
                    <asp:BoundField DataField="NumberOfMonths" SortExpression="" HeaderText="Number of Months"
                        DataFormatString="{0:D}" />
                    <asp:BoundField DataField="ApprovedFederalFunds" SortExpression="" HeaderText="Approved Federal Funds"
                        DataFormatString="{0:D}" />
                    <asp:BoundField DataField="BudgetExpenditures" SortExpression="" HeaderText="Budget Expenditures"
                        DataFormatString="{0:D}" />
                    <asp:BoundField DataField="Carryover" SortExpression="" HeaderText="Carryover" DataFormatString="{0:D}" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                                OnClick="OnEdit"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        <%-- Budget --%>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Edit Budget Detail
                    <div style="float: right">
                        <asp:ImageButton ID="Button3" ImageUrl="../../images/close.gif" runat="server" />
                    </div>
                    <table>
                        <tr>
                            <td>
                                Item:
                            </td>
                            <td>
                                <asp:TextBox ID="txtItem" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Rate Type:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblRateType" runat="server">
                                    <asp:ListItem>Unit Price</asp:ListItem>
                                    <asp:ListItem>Monthly Rate</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Rate:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRate" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                                <ajax:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtRate" Mask="999,999,999.99"
                                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Quantity:
                            </td>
                            <td>
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                                <ajax:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtQuantity" Mask="999999"
                                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Approved Federal Funds
                            </td>
                            <td>
                                <asp:TextBox ID="txtApprovedFunds" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                                <ajax:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtApprovedFunds"
                                    Mask="999,999,999" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ErrorTooltipEnabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                            </td>
                        </tr>
                    </table>
                </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button3" CancelControlID="Button3"
            BackgroundCssClass="magnetMPE" Y="20" />
        <p>
            Supplies Summary (100 word maximum)
        </p>
        <asp:TextBox ID="txtSummary" runat="server" Columns="100" Rows="4" TextMode="MultiLine"
            CssClass="msapDataTxt"></asp:TextBox>
        <br />
        <asp:Button ID="Savebutton" runat="server" Text="Save Summary" OnClick="OnSaveSummary"
            CssClass="msapBtn
            " />
    </div>
</asp:Content>
