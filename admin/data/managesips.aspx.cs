﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_managesips : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);
            LoadData();
        }
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        Response.Redirect("sip.aspx?option=" + (sender as LinkButton).CommandArgument);
    }
    protected void OnAddSIP(object sender, EventArgs e)
    {
        Response.Redirect("sip.aspx?option=0");
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        MagnetSIP.Delete(x => x.ID == id);
        LoadData();
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetSchools
                   from n in db.MagnetSIPs
                   where m.ID == n.SchoolID
                   && n.ReportID == Convert.ToInt32(hfReportID.Value)
                   orderby m.SchoolName
                   select new { m.SchoolName, n.CurrentStudentAttendance, n.ExpectedStudentAttendance, n.NumberInSchoolSuspensions, n.ExpectedInSchoolSuspensions, 
                       n.NumberOutSchoolSuspensions, n.ExpectedOutSchoolSuspensions, n.CurrentDropoutRate, n.ExpectedDropoutRate, n.CurrentparentalInvolvementRate,
                       n.ExpectedParentalInvolvementRate, n.ID};
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}