﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using System.Text;

public partial class admin_data_assurance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            //Report period
            MagnetReportPeriod Period1 = MagnetReportPeriod.SingleOrDefault(x=>x.ID == Convert.ToInt32(Session["ReportPeriodID"]));
            MagnetReportPeriod Period2 = MagnetReportPeriod.SingleOrDefault(x => x.ID == Convert.ToInt32(Session["ReportPeriodID"]) + 2);
            lblPeriod1.Text = Period1.ReportPeriod;
            lblPeriod2.Text = Period2.ReportPeriod;
            lblPeriod3.Text = Period1.ReportPeriod;
            lblPeriod4.Text = Period2.ReportPeriod;
            lblPeriod5.Text = Period2.ReportPeriod;
            lblPeriod6.Text = Period1.ReportPeriod;
            lblPeriod7.Text = Period1.ReportPeriod;

            //Report type
            var data = MagnetDesegregationPlan.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
            if (data.Count > 0)
            {
                hfID.Value = data[0].ID.ToString();
                LoadData();
            }
            else
            {
                Response.Redirect("desegregation.aspx", true);
            }
        }
    }
    private void LoadData()
    {
        MagnetDesegregationPlan plan = MagnetDesegregationPlan.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        if (plan.PlanType != null)
        {
            MagnetDBDB db = new MagnetDBDB();
            if (plan.PlanType == false)
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
                //Required
                RadioButton1.Checked = true;
                var data = MagnetBudgetSummary.Find(x=>x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == 50);
                foreach (MagnetBudgetSummary item in data)
                {
                    switch (item.ItemIndex)
                    {
                        case 1:
                            txtFactor.Text = item.ReportSummary;
                            HiddenField1.Value = item.ID.ToString();
                            break;
                        case 2:
                            txtProcess.Text = item.ReportSummary;
                            HiddenField2.Value = item.ID.ToString();
                            break;
                        case 3:
                            txtChange.Text = item.ReportSummary;
                            HiddenField3.Value = item.ID.ToString();
                            break;
                        case 4:
                            txtAcutalPlan.Text = item.ReportSummary;
                            HiddenField4.Value = item.ID.ToString();
                            break;
                    }
                }
            }
            else
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
                //Voluntory
                RadioButton2.Checked = true;
                var Files = from m in db.MagnetUploads
                            where m.ProjectID == Convert.ToInt32(hfReportID.Value)
                            && m.FormID == 12
                            select new { m.ID, FilePath = "<a target='_blank' href='../upload/" + m.PhysicalName + "'>" + m.FileName + "</a>" };
                GridView2.DataSource = Files;
                GridView2.DataBind();
                var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == 51);
                if (data.Count > 0)
                {
                    txtUnitary.Text = data[0].ReportSummary;
                    HiddenField5.Value = data[0].ID.ToString();
                }
            }
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetUpload.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        bool ret = false;
        string msg = "";

        if (RadioButton1.Checked)
        {
            if (string.IsNullOrEmpty(txtChange.Text))
            {
                lblChange.Visible = true;
                ret = true;
                if (string.IsNullOrEmpty(msg)) msg = "Please fill out required fields!";
            }
            else
            {
                lblChange.Visible = false;
            }
            if (string.IsNullOrEmpty(txtFactor.Text))
            {
                lblFactor.Visible = true;
                ret = true;
                if (string.IsNullOrEmpty(msg)) msg = "Please fill out required fields!";
            }
            else
                lblFactor.Visible = false;

            if (string.IsNullOrEmpty(txtProcess.Text))
            {
                lblProcess.Visible = true;
                ret = true;
                if (string.IsNullOrEmpty(msg)) msg = "Please fill out required fields!";
            }
            else
            {
                lblProcess.Visible = false;
            }
        }
        else if (RadioButton2.Checked)
        {
            if (!LetterFile.HasFile)
            {
                if (GridView2.Rows.Count <= 0)
                {
                    ret = true;
                    msg = "Please upload required letter!";
                }
            }
        }

        if (ret)
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('" + msg + "');</script>", false);
        }
        else
        {
            if (RadioButton1.Checked || RadioButton2.Checked)
            {
                if (RadioButton1.Checked)
                {
                    MagnetBudgetSummary FactorSummary = new MagnetBudgetSummary();
                    if (!string.IsNullOrEmpty(HiddenField1.Value)) 
                        FactorSummary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(HiddenField1.Value));
                    else
                    {
                        FactorSummary.ReportID = Convert.ToInt32(hfReportID.Value);
                        FactorSummary.ReportType = 50;
                        FactorSummary.ItemIndex = 1;
                    }
                    FactorSummary.ReportSummary = txtFactor.Text;
                    FactorSummary.Save();

                    MagnetBudgetSummary ProcessSummary = new MagnetBudgetSummary();
                    if (!string.IsNullOrEmpty(HiddenField2.Value))
                        ProcessSummary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(HiddenField2.Value));
                    else
                    {
                        ProcessSummary.ReportID = Convert.ToInt32(hfReportID.Value);
                        ProcessSummary.ReportType = 50;
                        ProcessSummary.ItemIndex = 2;
                    }
                    ProcessSummary.ReportSummary = txtProcess.Text;
                    ProcessSummary.Save();

                    MagnetBudgetSummary ChangeSummary = new MagnetBudgetSummary();
                    if (!string.IsNullOrEmpty(HiddenField3.Value))
                        ChangeSummary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(HiddenField3.Value));
                    else
                    {
                        ChangeSummary.ReportID = Convert.ToInt32(hfReportID.Value);
                        ChangeSummary.ReportType = 50;
                        ChangeSummary.ItemIndex = 3;
                    }
                    ChangeSummary.ReportSummary = txtChange.Text;
                    ChangeSummary.Save();

                    if (!string.IsNullOrEmpty(txtAcutalPlan.Text))
                    {
                        MagnetBudgetSummary ActualPlanSummary = new MagnetBudgetSummary();
                        if (!string.IsNullOrEmpty(HiddenField4.Value))
                            ActualPlanSummary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(HiddenField4.Value));
                        else
                        {
                            ActualPlanSummary.ReportID = Convert.ToInt32(hfReportID.Value);
                            ActualPlanSummary.ReportType = 50;
                            ActualPlanSummary.ItemIndex = 4;
                        }
                        ActualPlanSummary.ReportSummary = txtAcutalPlan.Text;
                        ActualPlanSummary.Save();
                    }
                }
                else if (RadioButton2.Checked && LetterFile.HasFile)
                {
                    MagnetUpload upload1 = new MagnetUpload();
                    upload1.ProjectID = Convert.ToInt32(hfReportID.Value);
                    upload1.FormID = 12;
                    upload1.FileName = LetterFile.FileName;
                    string guid = System.Guid.NewGuid().ToString() + "-" + LetterFile.FileName;
                    upload1.PhysicalName = guid;
                    LetterFile.SaveAs(Server.MapPath("../upload/") + guid);
                    upload1.UploadDate = DateTime.Now;
                    upload1.Save();

                    if (!string.IsNullOrEmpty(txtUnitary.Text))
                    {
                        MagnetBudgetSummary UnitarySummary = new MagnetBudgetSummary();
                        if (!string.IsNullOrEmpty(HiddenField5.Value))
                            UnitarySummary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(HiddenField5.Value));
                        else
                        {
                            UnitarySummary.ReportID = Convert.ToInt32(hfReportID.Value);
                            UnitarySummary.ReportType = 51;
                            UnitarySummary.ItemIndex = 1;
                        }
                        UnitarySummary.ReportSummary = txtUnitary.Text;
                        UnitarySummary.Save();
                    }
                }
                LoadData();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Record saved.');</script>", false);
            }
        }
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        var data = MagnetDesegregationPlan.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
        if (data.Count > 0)
        {
            using (System.IO.MemoryStream output = new MemoryStream())
            {
                try
                {
                    PdfStamper ps = null;

                    // read existing PDF document
                    PdfReader r = new PdfReader(Server.MapPath("../doc/desegregation.pdf"));
                    ps = new PdfStamper(r, output);
                    AcroFields af = ps.AcroFields;

                    if (data[0].PlanType != null)
                    {
                        if (data[0].PlanType == false)
                            af.SetField("RequiredPlan", "Yes");
                        else
                            af.SetField("VoluntaryPlan", "Yes");
                    }
                    ps.FormFlattening = true;

                    r.Close();
                    ps.Close();
                }
                catch (System.Exception ex)
                {
                    ILog Log = LogManager.GetLogger("EventLog");
                    Log.Error("Desegregation Plan Report:", ex);
                }
                finally
                {
                }

                int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
                MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName + " desegregation.pdf");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();

                output.Close();
            }
        }
    }
}