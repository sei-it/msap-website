﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.IO;

public partial class admin_data_ac : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            LoadData();

            Assurance.Attributes.Add("onchange", "return validateFileUpload(this);");
            Button10.Attributes.Add("onclick", "return validateFileUpload(document.getElementById('" + Assurance.ClientID + "'));");
        }
    }
    private void LoadData()
    {
        //Assurance
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetUploads
                   where m.ProjectID == Convert.ToInt32(hfReportID.Value)
                   && m.FormID == 12
                   select new { m.ID, FilePath = "<a target='_blank' href='../upload/" + m.PhysicalName + "'>" + m.FileName + "</a>" };
        GridView3.DataSource = data;
        GridView3.DataBind();
        if (data.Count() > 0)
        {
            Assurance.Enabled = false;
        }
        else
        {
            Assurance.Enabled = true;
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        int muploadID = Convert.ToInt32((sender as LinkButton).CommandArgument);

        var mupload = MagnetUpload.SingleOrDefault(x => x.ID == muploadID);

        if (System.IO.File.Exists(Server.MapPath("../upload/") + mupload.PhysicalName))
            System.IO.File.Delete(Server.MapPath("../upload/") + mupload.PhysicalName);

        MagnetUpload.Delete(x => x.ID == muploadID);
        LoadData();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        bool ret = false;
        string msg = "";
        if (!Assurance.HasFile && GridView3.Rows.Count <= 0)
        {
            ret = true;
            msg = "Please upload signed assurance and certification!";
        }

        if (ret)
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('" + msg + "');</script>", false);
        }
        else
        {
            //Assurance document
            if (Assurance.HasFile)
            {
                int ReportID = Convert.ToInt32(hfReportID.Value);
                GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
                MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
                MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

                string strReportPeriod = period.ReportPeriod;
                strReportPeriod += Convert.ToBoolean(period.PeriodType) ? "-Ad hoc" : "-APR";
                string strGrantee = grantee.GranteeName.Trim().Replace("#","");

                string strFileroot = Server.MapPath("") + "/../upload/";
                string strPath = strReportPeriod + "/" + strGrantee + "/Assurances and Certifications/";
                string strFileName = Assurance.FileName.Replace("#","");
                string strPathandFile = strPath + strFileName;

                int projectid = Convert.ToInt32(hfReportID.Value);
                MagnetUpload mupload = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == strFileName);

                   
                if (mupload == null)
                {
                    MagnetUpload upload3 = new MagnetUpload();
                    upload3.ProjectID = Convert.ToInt32(hfReportID.Value);
                    upload3.FormID = 12;  //Assurances and Certifications
                    upload3.FileName = strFileName;


                    if (!Directory.Exists(strPath))
                    {
                        Directory.CreateDirectory(strFileroot + strPath);
                    }

                    upload3.PhysicalName = strPathandFile;

                    Assurance.SaveAs(strFileroot + strPathandFile);
                    upload3.UploadDate = DateTime.Now;
                    upload3.Save();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation22", "<script>alert('Your data have been saved.');</script>", false);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmationfilename", "<script>alert('This file name already exists. Please rename the file.');</script>", false);
            }

            var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 6);
            if (muhs.Count > 0)
            {
                muhs[0].UpdateUser = Context.User.Identity.Name;
                muhs[0].TimeStamp = DateTime.Now;
                muhs[0].Save();
            }
            else
            {
                MagnetUpdateHistory muh = new MagnetUpdateHistory();
                muh.ReportID = Convert.ToInt32(hfReportID.Value);
                muh.FormIndex = 6;
                muh.UpdateUser = Context.User.Identity.Name;
                muh.TimeStamp = DateTime.Now;
                muh.Save();
            }

            LoadData();
        }
    }
}