﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_uploadcs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();
        int rptPeriodID = Session["ReportPeriodID"] == null ? 1 : Convert.ToInt32(Session["ReportPeriodID"]);
        if (rptPeriodID > 6) //cohort 2013
        {
            pnlCohort2013.Visible = true;
            pnlCohort2010.Visible = false;
        }
        else
        {
            //pnlCohort2013.Visible = false;
            //pnlCohort2010.Visible = true;
        }
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString((int)Session["ReportID"]);
            int ReportID = Convert.ToInt32(hfReportID.Value);
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);

            if (report.ReportPeriodID % 2 == 0 && report.ReportPeriodID !=2) //Ad hoc
            {
                ltlDes.Text = "Provide a brief summary of your overall results for the year. You may include important project updates in this section, although it is not required. Please keep the summary concise—no more than two pages or about 1,000 words.";
                ltlEval3.Text = "Did you meet each of your project-specific measures? If not, why not?";
            }
            else  //APR
            {
                ltlEval3.Text = "Discuss the likelihood that you will meet each of your project-specific measures by September 30, 2015.";
                ltlExeTitle.Text = "<b>Executive Summary</b><br />"; 
                ltlDes.Text = "<strong>District Administration</strong>" +
                                "<ol>" +
                                "<li>Describe the role and efforts of the Project Director at the state, district, and school level in implementing the MSAP project.</li>" +
                                "<li>What types of activities are being implemented to facilitate sustainability of your project? (MSAP Statute 5303(b)(2)(D))</li>" +
                                "</ol>" +
                                "<strong>Project Implementation</strong>" +
                                "<ol>" +
                                "<li>How have MSAP funds been used to develop the capacity of district staff and staff at each school to support the continuation of magnet-themed education and promote ongoing student achievement? (If your school is in improvement, how are these activities linked to your school improvement plan?) [MSAP Statute 5307(a)(5)]</li>" +
                                "<li>How has the grant been used to leverage community partnerships and resources to enhance theme integration and improve academic achievement at each school?</li>" +
                                "<li>How have MSAP funds been used to increase systemic family and community engagement? [MSAP Statute 5305(b)(2)(D)]</li>" +
                                "</ol>" +
                                "<strong>Evaluation</strong>" +
                                "<ol>" +
                                "<li>How are you evaluating your program?  What is the design and methodology of your evaluation?</li>" +
                                "<li>What are the results of your evaluations?</li>" +
                                "<li>How have you used the evaluation results to improve your programs?</li>" +
                                "</ol>" +
                                "<strong>Proposed Changes</strong>" +
                                "<ol>" +
                                "<li>Describe any changes that you wish to make in the grant’s activities for the next budget period that are consistent with the scope and objectives of your approved application.</li>" +
                                "<li>Attach a resume if you are requesting changes to key personnel.</li>" +
                                "</ol>";
            
            }

            GranteePerformanceCoverSheet sheet;
            var CoverSheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == (int)Session["ReportID"]);
            if (CoverSheets.Count == 0)
            {
                sheet = new GranteePerformanceCoverSheet();
                sheet.GranteeReportID = (int)Session["ReportID"];
                sheet.Save();
                Session["CoverSheetID"] = sheet.ID;
                hfID.Value = Convert.ToString(sheet.ID);
            }
            else
            {
                sheet = CoverSheets[0];
                hfID.Value = sheet.ID.ToString();
            }
            txtExecutiveSummary.Text = sheet.ExecutiveSummary;

            LoadFiles();
            FileUpload1.Attributes.Add("onchange", "return validateFileUpload(this);");
            UploadBtn.Attributes.Add("onclick", "return validateFileUpload(document.getElementById('" + FileUpload1.ClientID + "'));");
        }
    }
    private void LoadFiles()
    {
        //Uploaded file
        var data = MagnetUpload.Find(x => x.ProjectID == Convert.ToInt32(hfReportID.Value) && x.FormID == -1);
        if (data.Count > 0)
        {
            fileRow.Visible = true;
            hfFileID.Value = data[0].ID.ToString();
            lblFile.Text = data[0].FileName;
            UploadedFile.HRef = "../upload/" + data[0].PhysicalName;
            UploadBtn.Enabled = false;
            FileUpload1.Enabled = false;
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        OnUpload(sender, e);
        //if (!string.IsNullOrEmpty(txtExecutiveSummary.Text))
        {
            GranteePerformanceCoverSheet sheet = GranteePerformanceCoverSheet.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
            sheet.ExecutiveSummary = txtExecutiveSummary.Text;
            sheet.Save();
        }
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 12);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 12;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    protected void OnUpload(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            int ReportID = Convert.ToInt32(hfReportID.Value);
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

            string strReportPeriod = period.ReportPeriod;
            strReportPeriod += Convert.ToBoolean(period.PeriodType) ? "-Ad hoc" : "-APR";
            string strGrantee = grantee.GranteeName.Trim().Replace("#","");
            
            string strFileroot = Server.MapPath("") + "/../upload/";
            string strPath = strReportPeriod + "/" + strGrantee + "/ExecutiveSummary/";
            string strFileName = FileUpload1.FileName.Replace("#", "");
            string strPathandFile = strPath + strFileName;

            int projectid = Convert.ToInt32(hfReportID.Value);
            MagnetUpload mupload = MagnetUpload.SingleOrDefault(x => x.ProjectID == projectid && x.FileName == strFileName);

            if(mupload ==null)
            {
                MagnetUpload ExecutiveSummary = new MagnetUpload();
                ExecutiveSummary.ProjectID = projectid;
                ExecutiveSummary.FormID = -1; //Cover Sheet executive summary
                ExecutiveSummary.FileName = strFileName;
                //string tmp = System.Guid.NewGuid().ToString() + "-" + FileUpload1.FileName.Replace('#', ' ');
                        
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strFileroot + strPath);
                }

                FileUpload1.SaveAs(strFileroot + strPathandFile);

                ExecutiveSummary.PhysicalName = strPathandFile;
                ExecutiveSummary.UploadDate = DateTime.Now;
                ExecutiveSummary.Save();

                fileRow.Visible = true;
                hfFileID.Value = ExecutiveSummary.ID.ToString();
                lblFile.Text = strFileName;
                UploadedFile.HRef = "../upload/" + strPathandFile;

                UploadBtn.Enabled = false;
                FileUpload1.Enabled = false;

                var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 12);
                if (muhs.Count > 0)
                {
                    muhs[0].UpdateUser = Context.User.Identity.Name;
                    muhs[0].TimeStamp = DateTime.Now;
                    muhs[0].Save();
                }
                else
                {
                    MagnetUpdateHistory muh = new MagnetUpdateHistory();
                    muh.ReportID = Convert.ToInt32(hfReportID.Value);
                    muh.FormIndex = 12;
                    muh.UpdateUser = Context.User.Identity.Name;
                    muh.TimeStamp = DateTime.Now;
                    muh.Save();
                }
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('That file name already exists. Please rename your file and try again.');</script>", false);
        }
        
    }
    
    protected void OnDelete(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfFileID.Value))
        {
            try
            {
                fileRow.Visible = false;
                MagnetUpload ExecutiveSummary = MagnetUpload.SingleOrDefault(x => x.ID == Convert.ToInt32(hfFileID.Value));
                System.IO.File.Delete(Server.MapPath("") + "/../upload/" + ExecutiveSummary.PhysicalName);
                ExecutiveSummary.Delete();
                hfFileID.Value = "";
                UploadBtn.Enabled = true;
                FileUpload1.Enabled = true;
            }
            catch (Exception ex) { }
        }
    }
}