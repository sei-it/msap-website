﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_objectperformance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();
            if (!Page.IsPostBack)
            {
            if (Request.QueryString["option"] == null)
                Response.Redirect("managestatuschart.aspx");
            else
            {
                hfID.Value = Request.QueryString["option"];
                hfReportID.Value = Convert.ToString(Session["ReportID"]);

                //Performance measure
                int id = Convert.ToInt32(Request.QueryString["mgp"]);
                if (id > 0)
                {
                    hfStatusID.Value = id.ToString();
                    LoadPerformanceMeasure(id);
                }
                //else
                //{
                //    MagnetGrantPerformance item = new MagnetGrantPerformance();
                //    item.ProjectObjectiveID = Convert.ToInt32(hfID.Value);
                //    item.IsActive = true;
                //    item.Save();
                //    hfStatusID.Value = item.ID.ToString();
                //}
                
                //Build link
                BuildLink();

                if (!(Context.User.IsInRole("Administrators") || Context.User.IsInRole("ED")))
                {
                    txtPerformanceMeasure.Enabled = false;
                }
                else
                    txtPerformanceMeasure.Enabled = true;
            }
        }
    }
    private void BuildLink()
    {
        string objLink = "<a href='statuschart.aspx?option=" + Request.QueryString["option"] + "'>Project Objective " + Request.QueryString["index"] + "</a>";
        objectiveDiv.InnerHtml = objLink;
        lblObjective.Text = "Project Objective " + Request.QueryString["index"];

        string links = "<a href='statuschart.aspx?option=" + Request.QueryString["option"] + "'>Back to Objective " + Request.QueryString["index"] + "</a>&nbsp;&nbsp;&nbsp;&nbsp;";
        string tabLinks = "";
        int idx = 1;
        foreach (MagnetGrantPerformance mgp in MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == Convert.ToInt32(hfID.Value)).OrderBy(x=>x.ID))
        {
            if (hfStatusID.Value.Equals(mgp.ID.ToString()))
            {
                links += "Measure " + idx.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;";
                lblSSID.Text = idx.ToString();
                lblMeasure.Text = "Performance Measure " + idx.ToString();
                tabLinks = "<li class='tab_active'>Measure " + idx.ToString() + "</li>" + tabLinks;
            }
            else
            {
                links += "<a href='objectperformance.aspx?option=" + hfID.Value + "&mgp=" + mgp.ID.ToString() + "&index=" + Request.QueryString["index"] + "'>Measure " + idx.ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;";
                tabLinks = "<li><a href='objectperformance.aspx?option=" + hfID.Value + "&mgp=" + mgp.ID.ToString() + "&index=" + Request.QueryString["index"] + "'>Measure " + idx.ToString() + "</a></li>" + tabLinks;
            }
            idx++;
        }
        tabDiv.InnerHtml = tabLinks;
        linkDiv.InnerHtml = links;
    }
    private void ClearFields()
    {
        txtPerformanceMeasure.Text = "";
        ddlMeasureType.SelectedIndex = 0;
        txtTargetNumber.Text = "";
        txtTargetRatio1.Text = "";
        txtTargetRatio2.Text = "";
        txtTargetPercentage.Text = "";
        txtActualNumber.Text = "";
        txtActualRatio1.Text = "";
        txtActualRatio2.Text = "";
        txtActualPercentage.Text = "";
        txtExplanationProgress.Text = "";
    }
    private void LoadPerformanceMeasure(int ID)
    {
        MagnetGrantPerformance data = MagnetGrantPerformance.SingleOrDefault(x => x.ID == Convert.ToInt32(hfStatusID.Value));
        txtPerformanceMeasure.Text = data.PerformanceMeasure;
        ddlMeasureType.SelectedValue = data.MeasureType;
        txtTargetNumber.Text = Convert.ToString(data.TargetNumber);
        txtTargetRatio1.Text = data.TargetRatio1;
        txtTargetRatio2.Text = data.TargetRatio2;
        txtTargetPercentage.Text = Convert.ToString(data.TargetPercentage);
        txtActualNumber.Text = Convert.ToString(data.ActualNumber);
        txtActualRatio1.Text = data.AcutalRatio1;
        txtActualRatio2.Text = data.AcutalRatio2;
        txtActualPercentage.Text = Convert.ToString(data.ActualPercentage);
        txtExplanationProgress.Text = data.ExplanationProgress;

        //Disable text boxes
        if (!string.IsNullOrEmpty(txtTargetNumber.Text) || !string.IsNullOrEmpty(txtActualNumber.Text))
        {
            txtTargetRatio1.Attributes.Add("disabled", "disabled");
            txtTargetRatio2.Attributes.Add("disabled", "disabled");
            txtTargetPercentage.Attributes.Add("disabled", "disabled");
            txtActualRatio1.Attributes.Add("disabled", "disabled");
            txtActualRatio2.Attributes.Add("disabled", "disabled");
            txtActualPercentage.Attributes.Add("disabled", "disabled");
        }
        else if (!string.IsNullOrEmpty(txtTargetRatio1.Text) ||
            !string.IsNullOrEmpty(txtTargetRatio2.Text) ||
                !string.IsNullOrEmpty(txtTargetPercentage.Text) ||
                    !string.IsNullOrEmpty(txtActualRatio1.Text) ||
                        !string.IsNullOrEmpty(txtActualRatio2.Text) ||
                            !string.IsNullOrEmpty(txtActualPercentage.Text))
        {
            txtTargetNumber.Attributes.Add("disabled", "disabled");
            txtActualNumber.Attributes.Add("disabled", "disabled");
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetGrantPerformance data = new MagnetGrantPerformance();
        if (!string.IsNullOrEmpty(hfStatusID.Value))
        {
            data = MagnetGrantPerformance.SingleOrDefault(x => x.ID == Convert.ToInt32(hfStatusID.Value));
        }
        else
        {
            data.ProjectObjectiveID = Convert.ToInt32(hfID.Value);
            data.IsActive = true;
        }
        data.PerformanceMeasure = txtPerformanceMeasure.Text;
        data.MeasureType = ddlMeasureType.SelectedValue;
        if (!string.IsNullOrEmpty(txtTargetNumber.Text))
            data.TargetNumber = Convert.ToDecimal(txtTargetNumber.Text);
        else
            data.TargetNumber = null;

        data.TargetRatio1 = txtTargetRatio1.Text;
        data.TargetRatio2 = txtTargetRatio2.Text;
        if (!string.IsNullOrEmpty(txtTargetPercentage.Text))
            data.TargetPercentage = Convert.ToDecimal(txtTargetPercentage.Text);
        else
            data.TargetPercentage = null;
        if (!string.IsNullOrEmpty(txtActualNumber.Text))
            data.ActualNumber = Convert.ToDecimal(txtActualNumber.Text);
        else
            data.ActualNumber = null;
        data.AcutalRatio1 = txtActualRatio1.Text;
        data.AcutalRatio2 = txtActualRatio2.Text;
        if (!string.IsNullOrEmpty(txtActualPercentage.Text))
            data.ActualPercentage = Convert.ToDecimal(txtActualPercentage.Text);
        else
            data.ActualPercentage = null;
        data.ExplanationProgress = txtExplanationProgress.Text;
        data.Save();
        BuildLink();
        hfStatusID.Value = data.ID.ToString();
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 2);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 2;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }


        ScriptManager.RegisterClientScriptBlock(this, typeof(Page),
        "confirmation", "<script>alert('Your data have been saved.')</script>", false);

        if (string.IsNullOrEmpty(txtActualNumber.Text) && string.IsNullOrEmpty(txtTargetNumber.Text)
            && string.IsNullOrEmpty(txtTargetRatio1.Text)
            && string.IsNullOrEmpty(txtTargetRatio2.Text)
            && string.IsNullOrEmpty(txtTargetPercentage.Text)
            && string.IsNullOrEmpty(txtActualRatio1.Text)
            && string.IsNullOrEmpty(txtActualRatio2.Text)
            && string.IsNullOrEmpty(txtActualPercentage.Text)
            )
        {
            txtTargetNumber.Attributes.Remove("disabled");
            txtTargetRatio1.Attributes.Remove("disabled");
            txtTargetRatio2.Attributes.Remove("disabled");
            txtTargetPercentage.Attributes.Remove("disabled");

            txtActualNumber.Attributes.Remove("disabled");

            txtActualRatio1.Attributes.Remove("disabled");
            txtActualRatio2.Attributes.Remove("disabled");
            txtActualPercentage.Attributes.Remove("disabled");



        }
        else if (!string.IsNullOrEmpty(txtActualNumber.Text) || !string.IsNullOrEmpty(txtTargetNumber.Text))
        {
            txtTargetRatio1.Attributes.Add("disabled", "disabled");
            txtTargetRatio2.Attributes.Add("disabled", "disabled");


            txtTargetPercentage.Attributes.Add("disabled", "disabled");

            txtActualRatio1.Attributes.Add("disabled", "disabled");
            txtActualRatio2.Attributes.Add("disabled", "disabled");

            txtActualPercentage.Attributes.Add("disabled", "disabled");
        }
        else
        {
            txtActualNumber.Attributes.Add("disabled", "disabled");
            txtTargetNumber.Attributes.Add("disabled", "disabled");
        }


    }
}