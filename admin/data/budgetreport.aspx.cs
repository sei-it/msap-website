﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_data_budgetreport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == 17);
            if (data.Count == 1)
            {
                hfSummaryID.Value = data[0].ID.ToString();
                txtSummary.Text = data[0].ReportSummary;
            }
        }

    }
    protected void OnSaveSummary(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSummary.Text))
        {
            //Save to database
            MagnetBudgetSummary summary = new MagnetBudgetSummary();
            if (!string.IsNullOrEmpty(hfSummaryID.Value))
                summary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSummaryID.Value));
            else
            {
                summary.ReportID = Convert.ToInt32(hfReportID.Value);
                summary.ReportType = 17;
            }
            summary.ReportSummary = txtSummary.Text;
            summary.Save();
        }

    }
}