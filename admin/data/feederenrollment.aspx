﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="feederenrollment.aspx.cs" Inherits="admin_report_feederenrollment"
    ErrorPage="~/Error_page.aspx" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Feeder School Enrollment data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript">
        function Reset() {
            $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
        }

        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp != null && tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }

        $(function () {
            window.onbeforeunload = UnloadConfirm;
        });

        function Reset() {
            $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
        }
        function MergeColumn() {
            $('#ctl00_ContentPlaceHolder1_RadGrid1_ctl00').find('th, td').filter(':nth-child(18)').attr('style', 'text-align:left').append(function () {
                if ($(this).next().html() != null)
                    return '<br/>' + $(this).next().html();
                else
                    return '';
            }).next().remove();
            $('.rgEditForm').parent().attr('colspan', 18);
        }
        function GridCreated(sender, eventArgs) {
            MergeColumn();
        }

        function KeyPress(sender, args) {
            if (args.get_keyCode() == 45) {

                alert("You cannot enter negative numbers in this table.");

                args.set_cancel(true);

            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Table 4: Feeder School</h4>
   <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <div class="tab_area">
        <div class="titles">
            Table 4: Feeder School - Enrollment Data
        </div>
        <ul class="tabs">
            <li class="tab_active">Table 4</li>
            <li><a href="enrollment.aspx">Table 3</a></li>
            <li><a href="manageschoolyear.aspx">Table 2</a></li>
            <li><a href="leaenrollment.aspx">Table 1</a></li>
        </ul>
    </div>
    <br />
    <div class="asofDate">
        Actual Enrollment as of October 1,
        <asp:Label ID="lblYear" runat="server"></asp:Label>
        (Year
        <asp:Label ID="lblNumberYear" runat="server"></asp:Label>
        of Project)</div>
    <p>
        For each feeder school, identify the magnet school(s) to which the feeder school
        would send students. If a feeder school would send students to all magnet schools
        at a particular grade level (for example, Elementary Feeder School “X” would send
        students to all of the elementary magnet schools participating in the project, indicate
        “All” in the “Magnet School” column associated with Elementary Feeder School “X”).</p>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfSchoolID" runat="server" />
    <asp:HiddenField ID="hfStageID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <!-- content start -->
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            <!--
            var hasChanges, inputs, dropdowns, editedRow;
            function RowDblClick(sender, eventArgs) {
                /*editedRow = eventArgs.get_itemIndexHierarchical();
                if (editedRow && hasChanges) {
                hasChanges = false;
                $find("<%= RadGrid1.ClientID %>").get_masterTableView().updateItem(editedRow);
                }*/
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            }



            function setSavedBit(sender, eventArgs) {

                if (eventArgs.get_commandName() == "InitInsert" || eventArgs.get_commandName() == "Edit") {
                    $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
                }
                else if (eventArgs.get_commandName() == "CancelUpdate" || eventArgs.get_commandName() == "CancelInsert" || eventArgs.get_commandName() == "PerformInsert" || eventArgs.get_commandName() == "Update") {
                    $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
                }

            }

            -->
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadGrid ID="RadGrid1" GridLines="None" runat="server" AllowAutomaticDeletes="True" onkeydonw="KeyPress"
        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="False" 
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnItemUpdated="RadGrid1_ItemUpdated"
        OnItemDeleted="RadGrid1_ItemDeleted" 
        OnItemInserted="RadGrid1_ItemInserted" OnItemCreated="RadGrid1_ItemCreated"
        ShowFooter="True" Skin="MyCustomSkin" EnableEmbeddedSkins="false" OnDeleteCommand="RadGrid1_DeleteCommand"
        OnInsertCommand="RadGrid1_InsertCommand" OnUpdateCommand="RadGrid1_UpdateCommand"
        OnItemDataBound="RadGrid1_ItemDataBound" OnPreRender="RadGrid1_PreRender" 
        oncustomaggregate="RadGrid1_CustomAggregate">
        <ClientSettings ClientEvents-OnKeyPress="KeyPress">
            <ClientEvents OnGridCreated="GridCreated" />
            <ClientEvents OnCommand="setSavedBit" />
        </ClientSettings>
        <MasterTableView Width="103%" CommandItemDisplay="TopAndBottom" DataKeyNames="ID"
            EditMode="EditForms" DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
            TableLayout="Fixed" InsertItemDisplay="top">
            <CommandItemSettings ShowRefreshButton="false" />
            <Columns>
                <telerik:GridBoundColumn DataField="FeederSchool" HeaderText="Feeder<br>School" UniqueName="FeederSchool"
                    FooterText="" HeaderStyle-Width="90px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MagnetSchool" HeaderText="Magnet<br>School" UniqueName="MagnetSchool"
                    FooterText="Total" HeaderStyle-Width="90px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AmericanIndian" HeaderText="American<br> Indian"
                    UniqueName="AmericanIndian" Aggregate="Sum" FooterText=" ">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="P1" HeaderText="%" Aggregate="Custom" UniqueName="P1"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P1") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="Asian" HeaderText="Asian" UniqueName="Asian"
                    Aggregate="Sum" FooterText=" ">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="P2" HeaderText="%" Aggregate="Custom" UniqueName="P2"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P2") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="AfricanAmerican" HeaderText="African<br> American"
                    UniqueName="AfricanAmerican" Aggregate="Sum" FooterText=" ">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="P3" HeaderText="%" Aggregate="Custom" UniqueName="P3"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P3") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="Hispanic" HeaderText="Hispanic" UniqueName="Hispanic"
                    Aggregate="Sum" FooterText=" ">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="P4" HeaderText="%" Aggregate="Custom" UniqueName="P4"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P4") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="Hawaiian" HeaderText="Hawaiian" UniqueName="Hawaiian"
                    Aggregate="Sum" FooterText=" ">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="P6" HeaderText="%" Aggregate="Custom" UniqueName="P6"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P6") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="White" HeaderText="White" UniqueName="White"
                    Aggregate="Sum" FooterText=" ">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="P5" HeaderText="%" Aggregate="Custom" UniqueName="P5"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P5") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="MultiRacial" HeaderText="Multi Racial" UniqueName="MultiRacial"
                    Aggregate="Sum" FooterText=" ">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="P7" HeaderText="%" Aggregate="Custom" UniqueName="P7"
                    FooterText=" " FooterAggregateFormatString="{0: #,#.00}" DataType="System.Decimal">
                    <HeaderTemplate>
                        %</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("P7") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="total" HeaderText="%" Aggregate="Sum" UniqueName="total"
                    FooterText=" " DataType="System.Int32" HeaderStyle-Width="60px">
                    <HeaderTemplate>
                        Total</HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("total") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="EditCommandColumn"
                    UpdateText="Update" CancelText="Cancel" EditText="Edit" HeaderStyle-Width="50px">
                    <ItemStyle CssClass="MyImageButton" />
                </telerik:GridEditCommandColumn>
                <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                    ConfirmTitle="Delete" ButtonType="LinkButton" CommandName="Delete" Text="Delete"
                    UniqueName="DeleteColumn">
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings EditFormType="Template" >
             
                <FormTemplate>
                    <table id="Table2" cellspacing="2" cellpadding="1" width="100%" border="0" rules="none"
                        style="border-collapse: collapse; background: white;" class="Table_11_edit">
                        <tr class="EditFormHeader">
                            <td style="font-size: small; padding-top: 10px; margin-top: 10px;">
                                <b style="color: #F58220; font-size: 16px;">Record Details</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table3" cellspacing="1" cellpadding="1" width="100%" border="0" class="module">
                                    <tr>
                                        <td colspan="3" style="padding-bottom: 15px; margin-bottom: 15px; vertical-align: top;"
                                            class="subheader_txt">
                                            Feeder School:
                                            <telerik:RadComboBox ID="ddlFeederSchool" runat="server" SelectedValue='<%# Bind("FeederSchoolID") %>'
                                                DataSourceID="SqlDataSource2" TabIndex="1" DataTextField="SchoolName" DataValueField="ID"
                                                AppendDataBoundItems="False" EnableScreenBoundaryDetection="false">
                                            </telerik:RadComboBox>
                                        </td>
                                        <td style="vertical-align: top; padding-top: 6px;" class="subheader_txt">
                                            Magnet School:
                                        </td>
                                        <td colspan="3" style="padding-bottom: 15px; margin-bottom: 15px; vertical-align: top;">
                                            <asp:CheckBoxList ID="cklMagnetSchool" runat="server" CssClass="msapDataTxt" AutoPostBack="true"
                                                Height="80" TabIndex="2" RepeatColumns="2" OnSelectedIndexChanged="OnSchoolSelected">
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr style="text-align: center;" class="subheader_txt Table11_edit2_tbl_header">
                                        <td style="width: 15%; border-left: 0px;">
                                            American Indian or Alaska Native
                                        </td>
                                        <td style="width: 14%;">
                                            Asian
                                        </td>
                                        <td style="width: 14%;">
                                            Black or African-American
                                        </td>
                                        <td style="width: 14%;">
                                            Hispanic or Latino
                                        </td>
                                        <td style="width: 15%;">
                                            Native Hawaiian or Other Pacific Islander
                                        </td>
                                        <td style="width: 14%;">
                                            White
                                        </td>
                                        <td style="width: 14%;">
                                            Two or more races
                                        </td>
                                    </tr>
                                    <tr style="text-align: center;" class="Table11_edit2_tbl">
                                        <td style="border: 0px;">
                                            <telerik:RadNumericTextBox ID="txtIndian" runat="server" DbValue='<%# Bind("AmericanIndian") %>'
                                                TabIndex="3" Type="Number" NumberFormat-DecimalDigits="0" Width="80"  >
                                                <ClientEvents OnKeyPress="KeyPress" />
                                                </telerik:RadNumericTextBox>
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="txtAsian" runat="server" DbValue='<%# Bind("Asian") %>'
                                                TabIndex="4" Type="Number" NumberFormat-DecimalDigits="0" Width="80" >
                                                <ClientEvents OnKeyPress="KeyPress" />
                                                </telerik:RadNumericTextBox>
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="txtBlack" runat="server" DbValue='<%# Bind("AfricanAmerican") %>'
                                                TabIndex="5" Type="Number" NumberFormat-DecimalDigits="0" Width="80" >
                                                <ClientEvents OnKeyPress="KeyPress" />
                                                </telerik:RadNumericTextBox>
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="txtHispanic" runat="server" DbValue='<%# Bind("Hispanic") %>'
                                                TabIndex="6" Type="Number" NumberFormat-DecimalDigits="0" Width="80" >
                                                <ClientEvents OnKeyPress="KeyPress" />
                                                </telerik:RadNumericTextBox>
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="txtHawaiian" runat="server" DbValue='<%# Bind("Hawaiian") %>'
                                                TabIndex="7" Type="Number" NumberFormat-DecimalDigits="0" Width="80" >
                                                <ClientEvents OnKeyPress="KeyPress" />
                                                </telerik:RadNumericTextBox>
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="txtWhite" runat="server" DbValue='<%# Bind("White") %>'
                                                TabIndex="8" Type="Number" NumberFormat-DecimalDigits="0" Width="80" >
                                                <ClientEvents OnKeyPress="KeyPress" />
                                                </telerik:RadNumericTextBox>
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="txtMultiRaces" runat="server" DbValue='<%# Bind("MultiRacial") %>'
                                                TabIndex="9" Type="Number" NumberFormat-DecimalDigits="0" Width="80" >
                                                <ClientEvents OnKeyPress="KeyPress" />
                                                </telerik:RadNumericTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="padding-bottom: 10px; padding-top: 15px;">
                                <asp:LinkButton ID="btnUpdate" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' 
                                 runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                                </asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                    CommandName="Cancel"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
        <ClientSettings>
            <ClientEvents OnRowClick="RowDblClick" />
        </ClientSettings>
    </telerik:RadGrid>
    <br />
    <!-- SelectCommand="SELECT [id],[GradeLevel],[AmericanIndian] as A2, [AmericanIndian], [Asian], [AfricanAmerican], [Hispanic], [White], [Hawaiian], [MultiRacial] FROM [MagnetSchoolEnrollments] where [GranteeReportID]=@GranteeReportID and [StageID]=@StageID order by [GradeLevel]" -->
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        SelectCommand="exec spp_feederenrollment @GranteeReportID, @StageID, @SchoolIDs">
        <SelectParameters>
            <asp:Parameter Name="GranteeReportID" Type="Int32" />
            <asp:Parameter Name="StageID" Type="Int32" />
            <asp:Parameter Name="SchoolIDs" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        SelectCommand="SELECT [ID], [SchoolName] FROM [MagnetFeederSchools] where [GranteeID]=@GranteeID order by SchoolName">
        <SelectParameters>
            <asp:Parameter Name="GranteeID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        SelectCommand="SELECT [ID], [SchoolName] FROM [MagnetSchools] where [GranteeID]=@GranteeID order by SchoolName">
        <SelectParameters>
            <asp:Parameter Name="GranteeID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!-- content end -->
    <br />
    <br />
    <div style="text-align: right;">
        <asp:Button ID="Button1" runat="server" Text="Review" CssClass="surveyBtn" OnClick="OnPrint" />
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a href="leaenrollment.aspx">Table 1 - Enrollment Data-LEA Level</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="manageschoolyear.aspx">Table 2 - Year of Implementation</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="enrollment.aspx">Table 3 - Enrollment Data-Magnet Schools</a>&nbsp;&nbsp;&nbsp;&nbsp;
        Table 4 - Feeder School
    </div>
</asp:Content>
