﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="schoolstatus.aspx.cs" Inherits="admin_data_schoolstatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    School Status
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            <a href="datareport.aspx">Home</a> --> School Status</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfGranteeID" runat="server" />
         <asp:Button ID="Button3" runat="server" Text="New School Status" CssClass="msapBtn" OnClick="OnNew" CausesValidation="false" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School Name" />
                <asp:BoundField DataField="TypeName" SortExpression="" HeaderText="School Status" />
                <asp:BoundField DataField="Notes" SortExpression="" HeaderText="Notes" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>' CausesValidation="false"
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'  CausesValidation="false"
                            Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit School Status</div>
                <table>
                    <tr>
                        <td>
                            School:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                                <asp:ListItem Value="">Please select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="ddlSchool" ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            School Status:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSchoolStatus" runat="server" CssClass="msapDataTxt">
                                <asp:ListItem Value="-9">Please select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Notes:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNotes" runat="server" CssClass="msapDataTxt" MaxLength="5000" Width="450"
                                TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' align="center">
                            <asp:Button ID="Button1" runat="server" Text="Save" CssClass="msapBtn" OnClick="OnSaveData" />
                            <asp:Button ID="Button2" runat="server" Text="Close" CssClass="msapBtn" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button2" CancelControlID="Button2"
            BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
