﻿using System;
using System.Web.UI;
using Synergy.Magnet;

public partial class admin_data_gpraii : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
           
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
            if (report.UndeclaredField != null && report.UndeclaredField == true)
            {
                txtUndeclared.Enabled = true;
            }
            if (report.ReportPeriodID == 7) //cohort 2013 apr
                pnlMeasure1Des.Visible = false;

            //Report type
            int granteeID = (int)report.GranteeID;

            if (Request.QueryString["id"] == null)
                Response.Redirect("managegpra.aspx");
            else
            {
                LinkI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
                tabPartI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
                LinkIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                tabPartIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                LinkIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                tabPartIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                LinkV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
                tabPartV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
                LinkVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];
                tabPartVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];

                int schoolID = Convert.ToInt32(Request.QueryString["id"]);
                lblSchoolName.Text = MagnetSchool.SingleOrDefault(x => x.ID == schoolID).SchoolName;
                MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);
                int ReportYear = Convert.ToInt32(period.ReportPeriod.Substring(7, 2)) + 1;
                lblReportPeriod.Text = "Fall 20" + period.ReportPeriod.Substring(7, 2) ; // +" - " + Convert.ToString(ReportYear);
                lblReportPeriod2.Text = "Fall 20" + period.ReportPeriod.Substring(7, 2) ; // +" - " + Convert.ToString(ReportYear);

                var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == Convert.ToInt32(hfReportID.Value));
                if (gpraData.Count > 0)
                {
                    hfID.Value = gpraData[0].ID.ToString();

                    //Applicant pool
                    var applicantPoolData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 1);
                    if (applicantPoolData.Count > 0)
                    {
                        hfApplicationPool.Value = applicantPoolData[0].ID.ToString();
                        txtIndian.Text = applicantPoolData[0].Indian < 0 ? "" : Convert.ToString(applicantPoolData[0].Indian);
                        txtAsian.Text = applicantPoolData[0].Asian < 0 ?"":Convert.ToString(applicantPoolData[0].Asian);
                        txtBlack.Text = applicantPoolData[0].Black < 0 ?"":Convert.ToString(applicantPoolData[0].Black);
                        txtHispanic.Text = applicantPoolData[0].Hispanic < 0 ?"":Convert.ToString(applicantPoolData[0].Hispanic);
                        txtHawaiian.Text = applicantPoolData[0].Hawaiian < 0 ?"":Convert.ToString(applicantPoolData[0].Hawaiian);
                        txtWhite.Text = applicantPoolData[0].White < 0 ?"":Convert.ToString(applicantPoolData[0].White);
                        txtMultiRaces.Text = applicantPoolData[0].MultiRaces < 0 ?"":Convert.ToString(applicantPoolData[0].MultiRaces);
                        txtTotalSutdents.Text = applicantPoolData[0].Total < 0 ?"":Convert.ToString(applicantPoolData[0].Total);
                        txtNewStudents.Text = Convert.ToString(applicantPoolData[0].ExtraFiled1);
                        txtUndeclared.Text = applicantPoolData[0].Undeclared < 0 ?"":Convert.ToString(applicantPoolData[0].Undeclared);
                    }
                    else
                    {
                        //MagnetGPRAPerformanceMeasure applicantPool = new MagnetGPRAPerformanceMeasure();
                        //applicantPool.ReportType = 1;
                        //applicantPool.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                        ////applicantPool.Save();
                        //hfApplicationPool.Value = applicantPool.ID.ToString();
                    }
                    //Enrollment data
                    var enrollmentData = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 2);
                    if (enrollmentData.Count > 0)
                    {
                        hfEnrollment.Value = enrollmentData[0].ID.ToString();
                        txtNewStudentEnrollment.Text = enrollmentData[0].Total < 0 ?"":Convert.ToString(enrollmentData[0].Total);
                        hidNewStudentEnrollment.Value = txtNewStudentEnrollment.Text; 

                        txtIndianEnrollment.Text = enrollmentData[0].Indian < 0 ?"":Convert.ToString(enrollmentData[0].Indian);
                        txtAsianEnrollment.Text = enrollmentData[0].Asian < 0 ?"":Convert.ToString(enrollmentData[0].Asian);
                        txtBlackEnrollment.Text = enrollmentData[0].Black < 0 ?"":Convert.ToString(enrollmentData[0].Black);
                        txtHispanicEnrollment.Text = enrollmentData[0].Hispanic < 0 ?"":Convert.ToString(enrollmentData[0].Hispanic);
                        txtHawaiianEnrollment.Text = enrollmentData[0].Hawaiian < 0 ?"":Convert.ToString(enrollmentData[0].Hawaiian);
                        txtWhiteEnrollment.Text = enrollmentData[0].White < 0 ?"":Convert.ToString(enrollmentData[0].White);
                        txtMultiRacesEnrollment.Text = enrollmentData[0].MultiRaces < 0 ?"":Convert.ToString(enrollmentData[0].MultiRaces);

                        txtNewStudents.Text = enrollmentData[0].ExtraFiled1 < 0 ?"":Convert.ToString(enrollmentData[0].ExtraFiled1);
                        hidNewStudents.Value = txtNewStudents.Text;

                        txtContinuingEnrollment.Text = enrollmentData[0].ExtraFiled2 < 0 ?"":Convert.ToString(enrollmentData[0].ExtraFiled2);
                        hidContinuingEnrollment.Value = txtContinuingEnrollment.Text;

                        txtTotalSutdentsEnrollment.Text = enrollmentData[0].ExtraFiled3 < 0 ? "" : Convert.ToString(enrollmentData[0].ExtraFiled3);
                      

                        //26(a) - 26(g)
                        TxtNewIndianEnrollment.Text = enrollmentData[0].NewIndian < 0 ? "" : Convert.ToString(enrollmentData[0].NewIndian);
                        TxtNewAsianEnrollment.Text = enrollmentData[0].NewAsian < 0 ? "" : Convert.ToString(enrollmentData[0].NewAsian);
                        txtNewBlackEnrollment.Text = enrollmentData[0].NewBlack < 0 ? "" : Convert.ToString(enrollmentData[0].NewBlack);
                        txtNewHispanicEnrollment.Text = enrollmentData[0].NewHispanic < 0 ? "" : Convert.ToString(enrollmentData[0].NewHispanic);
                        txtNewHawaiianEnrollment.Text = enrollmentData[0].NewHawaiian < 0 ? "" : Convert.ToString(enrollmentData[0].NewHawaiian);
                        txtNewWhiteEnrollment.Text = enrollmentData[0].NewWhite < 0 ? "" : Convert.ToString(enrollmentData[0].NewWhite);
                        txtNewMultiRacesEnrollment.Text = enrollmentData[0].NewMultiRaces < 0 ? "" : Convert.ToString(enrollmentData[0].NewMultiRaces);

                        //27(a) - 27(g)
                        TxtContIndianEnrollment.Text = enrollmentData[0].ContIndian < 0 ? "" : Convert.ToString(enrollmentData[0].ContIndian);
                        TxtContAsianEnrollment.Text = enrollmentData[0].ContAsian < 0 ? "" : Convert.ToString(enrollmentData[0].ContAsian);
                        txtContBlackEnrollment.Text = enrollmentData[0].ContBlack < 0 ? "" : Convert.ToString(enrollmentData[0].ContBlack);
                        txtContHispanicEnrollment.Text = enrollmentData[0].ContHispanic < 0 ? "" : Convert.ToString(enrollmentData[0].ContHispanic);
                        txtContHawaiianEnrollment.Text = enrollmentData[0].ContHawaiian < 0 ? "" : Convert.ToString(enrollmentData[0].ContHawaiian);
                        txtContWhiteEnrollment.Text = enrollmentData[0].ContWhite < 0 ? "" : Convert.ToString(enrollmentData[0].ContWhite);
                        txtContMultiRacesEnrollment.Text = enrollmentData[0].ContMultiRaces < 0 ? "" : Convert.ToString(enrollmentData[0].ContMultiRaces);
                    }
                    else
                    {
                        //MagnetGPRAPerformanceMeasure enrollment = new MagnetGPRAPerformanceMeasure();
                        //enrollment.ReportType = 2;
                        //enrollment.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                        ////enrollment.Save();
                        //hfEnrollment.Value = enrollment.ID.ToString();
                    }
                }
                else
                {
                    //MagnetGPRA item = new MagnetGPRA();
                    //item.ReportID = Convert.ToInt32(hfReportID.Value);
                    //item.SchoolID = schoolID;
                    ////item.Save();
                    //hfID.Value = item.ID.ToString();

                    //MagnetGPRAPerformanceMeasure applicantPool = new MagnetGPRAPerformanceMeasure();
                    //applicantPool.ReportType = 1;
                    //applicantPool.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    //applicantPool.Save();
                    //hfApplicationPool.Value = applicantPool.ID.ToString();

                    //MagnetGPRAPerformanceMeasure enrollment = new MagnetGPRAPerformanceMeasure();
                    //enrollment.ReportType = 2;
                    //enrollment.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    ////enrollment.Save();
                    //hfEnrollment.Value = enrollment.ID.ToString();
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetGPRA item = new MagnetGPRA();
        bool isNullValue = false;
        int schoolID = Convert.ToInt32(Request.QueryString["id"]);
        var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == Convert.ToInt32(hfReportID.Value));
        if (gpraData.Count == 0)
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
            item.SchoolID = schoolID;
            item.Save();
            hfID.Value = item.ID.ToString();
        }

        if (string.IsNullOrEmpty(hfApplicationPool.Value))
        {
            MagnetGPRAPerformanceMeasure newapplicantPool = new MagnetGPRAPerformanceMeasure();
            newapplicantPool.ReportType = 1;
            newapplicantPool.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            newapplicantPool.Save();
            hfApplicationPool.Value = newapplicantPool.ID.ToString();
        }

        MagnetGPRAPerformanceMeasure applicantPool = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfApplicationPool.Value));

        if (string.IsNullOrEmpty(txtIndian.Text) &&
            string.IsNullOrEmpty(txtAsian.Text) &&
           string.IsNullOrEmpty(txtBlack.Text) &&
           string.IsNullOrEmpty(txtHispanic.Text) &&
           string.IsNullOrEmpty(txtHawaiian.Text) &&
           string.IsNullOrEmpty(txtWhite.Text) &&
           string.IsNullOrEmpty(txtMultiRaces.Text) &&
           string.IsNullOrEmpty(txtUndeclared.Text))
            isNullValue = true;

        applicantPool.Indian = string.IsNullOrEmpty(txtIndian.Text) ? (int?)null : Convert.ToInt32(txtIndian.Text);
        applicantPool.Asian = string.IsNullOrEmpty(txtAsian.Text) ? (int?)null : Convert.ToInt32(txtAsian.Text);
        applicantPool.Black = string.IsNullOrEmpty(txtBlack.Text) ? (int?)null : Convert.ToInt32(txtBlack.Text);
        applicantPool.Hispanic = string.IsNullOrEmpty(txtHispanic.Text) ? (int?)null : Convert.ToInt32(txtHispanic.Text);
        applicantPool.Hawaiian = string.IsNullOrEmpty(txtHawaiian.Text) ? (int?)null : Convert.ToInt32(txtHawaiian.Text);
        applicantPool.White = string.IsNullOrEmpty(txtWhite.Text) ? (int?)null : Convert.ToInt32(txtWhite.Text);
        applicantPool.MultiRaces = string.IsNullOrEmpty(txtMultiRaces.Text) ? (int?)null : Convert.ToInt32(txtMultiRaces.Text);
        //applicantPool.Total = string.IsNullOrEmpty(txtTotalSutdents.Text) ? 0 : Convert.ToInt32(txtTotalSutdents.Text);
        applicantPool.Undeclared = string.IsNullOrEmpty(txtUndeclared.Text) ? (int?)null : Convert.ToInt32(txtUndeclared.Text);
        //17 total
        if (!isNullValue)
        {
            applicantPool.Total = (applicantPool.Indian == null ? 0 : applicantPool.Indian)
                + (applicantPool.Asian == null ? 0 : applicantPool.Asian)
                + (applicantPool.Black == null ? 0 : applicantPool.Black)
                + (applicantPool.Hispanic == null ? 0 : applicantPool.Hispanic)
                + (applicantPool.Hawaiian == null ? 0 : applicantPool.Hawaiian)
                + (applicantPool.White == null ? 0 : applicantPool.White)
                + (applicantPool.MultiRaces == null ? 0 : applicantPool.MultiRaces)
                + (applicantPool.Undeclared == null ? 0 : applicantPool.Undeclared);
        }
        else
            applicantPool.Total = (int?)null;

        applicantPool.ReportType = 1;
        //applicantPool.ExtraFiled1 = string.IsNullOrEmpty(txtNewStudents.Text) ? 0 : Convert.ToInt32(txtNewStudents.Text);
        applicantPool.Save();
        txtTotalSutdents.Text = Convert.ToString(applicantPool.Total);


        if (string.IsNullOrEmpty(hfEnrollment.Value))
        {
            MagnetGPRAPerformanceMeasure Newenrollment = new MagnetGPRAPerformanceMeasure();
            Newenrollment.ReportType = 2;
            Newenrollment.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            Newenrollment.Save();
            hfEnrollment.Value = Newenrollment.ID.ToString();
        }

        //Enrollment
        MagnetGPRAPerformanceMeasure enrollmentData = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfEnrollment.Value));

        //18 - 24
        isNullValue = false;
        if (string.IsNullOrEmpty(txtIndianEnrollment.Text) &&
          string.IsNullOrEmpty(txtAsianEnrollment.Text) &&
         string.IsNullOrEmpty(txtBlackEnrollment.Text) &&
         string.IsNullOrEmpty(txtHispanicEnrollment.Text) &&
         string.IsNullOrEmpty(txtHawaiianEnrollment.Text) &&
         string.IsNullOrEmpty(txtWhiteEnrollment.Text) &&
         string.IsNullOrEmpty(txtMultiRacesEnrollment.Text))
            isNullValue = true;

        enrollmentData.Indian = string.IsNullOrEmpty(txtIndianEnrollment.Text) ? (int?)null : Convert.ToInt32(txtIndianEnrollment.Text);
        enrollmentData.Asian = string.IsNullOrEmpty(txtAsianEnrollment.Text) ? (int?)null : Convert.ToInt32(txtAsianEnrollment.Text);
        enrollmentData.Black = string.IsNullOrEmpty(txtBlackEnrollment.Text) ? (int?)null : Convert.ToInt32(txtBlackEnrollment.Text);
        enrollmentData.Hispanic = string.IsNullOrEmpty(txtHispanicEnrollment.Text) ? (int?)null : Convert.ToInt32(txtHispanicEnrollment.Text);
        enrollmentData.Hawaiian = string.IsNullOrEmpty(txtHawaiianEnrollment.Text) ? (int?)null : Convert.ToInt32(txtHawaiianEnrollment.Text);
        enrollmentData.White = string.IsNullOrEmpty(txtWhiteEnrollment.Text) ? (int?)null : Convert.ToInt32(txtWhiteEnrollment.Text);
        enrollmentData.MultiRaces = string.IsNullOrEmpty(txtMultiRacesEnrollment.Text) ? (int?)null : Convert.ToInt32(txtMultiRacesEnrollment.Text);
        //25 total
        if (!isNullValue)
        {
            enrollmentData.Total = (enrollmentData.Indian == null ? 0 : enrollmentData.Indian)
               + (enrollmentData.Asian == null ? 0 : enrollmentData.Asian)
               + (enrollmentData.Black == null ? 0 : enrollmentData.Black)
               + (enrollmentData.Hispanic == null ? 0 : enrollmentData.Hispanic)
               + (enrollmentData.Hawaiian == null ? 0 : enrollmentData.Hawaiian)
               + (enrollmentData.White == null ? 0 : enrollmentData.White)
               + (enrollmentData.MultiRaces == null ? 0 : enrollmentData.MultiRaces);
        }
        else
            enrollmentData.Total = (int?)null;

        

        //26(a) - 26(g)
        isNullValue = false;
        if (string.IsNullOrEmpty(TxtNewIndianEnrollment.Text) &&
         string.IsNullOrEmpty(TxtNewAsianEnrollment.Text) &&
        string.IsNullOrEmpty(txtNewBlackEnrollment.Text) &&
        string.IsNullOrEmpty(txtNewHispanicEnrollment.Text) &&
        string.IsNullOrEmpty(txtNewHawaiianEnrollment.Text) &&
        string.IsNullOrEmpty(txtNewWhiteEnrollment.Text) &&
        string.IsNullOrEmpty(txtNewMultiRacesEnrollment.Text))
            isNullValue = true;
        
        enrollmentData.NewIndian = string.IsNullOrEmpty(TxtNewIndianEnrollment.Text) ? (int?)null : Convert.ToInt32(TxtNewIndianEnrollment.Text);
        enrollmentData.NewAsian = string.IsNullOrEmpty(TxtNewAsianEnrollment.Text) ? (int?)null : Convert.ToInt32(TxtNewAsianEnrollment.Text);
        enrollmentData.NewBlack = string.IsNullOrEmpty(txtNewBlackEnrollment.Text) ? (int?)null : Convert.ToInt32(txtNewBlackEnrollment.Text);
        enrollmentData.NewHispanic = string.IsNullOrEmpty(txtNewHispanicEnrollment.Text) ? (int?)null : Convert.ToInt32(txtNewHispanicEnrollment.Text);
        enrollmentData.NewHawaiian = string.IsNullOrEmpty(txtNewHawaiianEnrollment.Text) ? (int?)null : Convert.ToInt32(txtNewHawaiianEnrollment.Text);
        enrollmentData.NewWhite = string.IsNullOrEmpty(txtNewWhiteEnrollment.Text) ? (int?)null : Convert.ToInt32(txtNewWhiteEnrollment.Text);
        enrollmentData.NewMultiRaces = string.IsNullOrEmpty(txtNewMultiRacesEnrollment.Text) ? (int?)null : Convert.ToInt32(txtNewMultiRacesEnrollment.Text);
        //enrollmentData.ExtraFiled1 = string.IsNullOrEmpty(hidNewStudents.Value) ? (int?)null : Convert.ToInt32(hidNewStudents.Value); 
       
        //26 total
        if (!isNullValue)
        {
            enrollmentData.ExtraFiled1 = (enrollmentData.NewIndian == null ? 0 : enrollmentData.NewIndian)
               + (enrollmentData.NewAsian == null ? 0 : enrollmentData.NewAsian)
               + (enrollmentData.NewBlack == null ? 0 : enrollmentData.NewBlack)
               + (enrollmentData.NewHispanic == null ? 0 : enrollmentData.NewHispanic)
               + (enrollmentData.NewHawaiian == null ? 0 : enrollmentData.NewHawaiian)
               + (enrollmentData.NewWhite == null ? 0 : enrollmentData.NewWhite)
               + (enrollmentData.NewMultiRaces == null ? 0 : enrollmentData.NewMultiRaces);
        }
        else
            enrollmentData.ExtraFiled1 = (int?)null;

        //27(a) - 27(g)
        isNullValue = false;
        if (string.IsNullOrEmpty(TxtContIndianEnrollment.Text) &&
         string.IsNullOrEmpty(TxtContAsianEnrollment.Text) &&
        string.IsNullOrEmpty(txtContBlackEnrollment.Text) &&
        string.IsNullOrEmpty(txtContHispanicEnrollment.Text) &&
        string.IsNullOrEmpty(txtContHawaiianEnrollment.Text) &&
        string.IsNullOrEmpty(txtContWhiteEnrollment.Text) &&
        string.IsNullOrEmpty(txtContMultiRacesEnrollment.Text))
            isNullValue = true;

        enrollmentData.ContIndian = string.IsNullOrEmpty(TxtContIndianEnrollment.Text) ? (int?)null : Convert.ToInt32(TxtContIndianEnrollment.Text);
        enrollmentData.ContAsian = string.IsNullOrEmpty(TxtContAsianEnrollment.Text) ? (int?)null : Convert.ToInt32(TxtContAsianEnrollment.Text);
        enrollmentData.ContBlack = string.IsNullOrEmpty(txtContBlackEnrollment.Text) ? (int?)null : Convert.ToInt32(txtContBlackEnrollment.Text);
        enrollmentData.ContHispanic = string.IsNullOrEmpty(txtContHispanicEnrollment.Text) ? (int?)null : Convert.ToInt32(txtContHispanicEnrollment.Text);
        enrollmentData.ContHawaiian = string.IsNullOrEmpty(txtContHawaiianEnrollment.Text) ? (int?)null : Convert.ToInt32(txtContHawaiianEnrollment.Text);
        enrollmentData.ContWhite = string.IsNullOrEmpty(txtContWhiteEnrollment.Text) ? (int?)null : Convert.ToInt32(txtContWhiteEnrollment.Text);
        enrollmentData.ContMultiRaces = string.IsNullOrEmpty(txtContMultiRacesEnrollment.Text) ? (int?)null : Convert.ToInt32(txtContMultiRacesEnrollment.Text);

        //enrollmentData.ExtraFiled2 = string.IsNullOrEmpty(hidContinuingEnrollment.Value) ? (int?)null : Convert.ToInt32(hidContinuingEnrollment.Value); 
        //27 total
        if (!isNullValue)
        {
            enrollmentData.ExtraFiled2 = (enrollmentData.ContIndian == null ? 0 : enrollmentData.ContIndian)
               + (enrollmentData.ContAsian == null ? 0 : enrollmentData.ContAsian)
               + (enrollmentData.ContBlack == null ? 0 : enrollmentData.ContBlack)
               + (enrollmentData.ContHispanic == null ? 0 : enrollmentData.ContHispanic)
               + (enrollmentData.ContHawaiian == null ? 0 : enrollmentData.ContHawaiian)
               + (enrollmentData.ContWhite == null ? 0 : enrollmentData.ContWhite)
               + (enrollmentData.ContMultiRaces == null ? 0 : enrollmentData.ContMultiRaces);
        }
        else
            enrollmentData.ExtraFiled2 = (int?)null;

        isNullValue = false;

        if (enrollmentData.Total == null &&
            enrollmentData.ExtraFiled1 == null &&
            enrollmentData.ExtraFiled2 == null )
             isNullValue = true;

        if (!isNullValue)
        {
            enrollmentData.ExtraFiled3 = (enrollmentData.Total == null ? 0 : enrollmentData.Total) + (enrollmentData.ExtraFiled1 == null ? 0 : enrollmentData.ExtraFiled1) + (enrollmentData.ExtraFiled2 == null ? 0 : enrollmentData.ExtraFiled2);
            enrollmentData.ReportType = 2;
        }
        else
            enrollmentData.ExtraFiled3 = (int?)null;

        enrollmentData.Save();

        txtTotalSutdentsEnrollment.Text = Convert.ToString(enrollmentData.ExtraFiled3);
        txtNewStudentEnrollment.Text = Convert.ToString(enrollmentData.Total); //25 total

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 3);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 3;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }

        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);

        txtNewStudents.Text = hidNewStudents.Value;
        txtContinuingEnrollment.Text = hidContinuingEnrollment.Value;

        
    }
}