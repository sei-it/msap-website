﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using Telerik.Web.UI;

public partial class admin_report_enrollmentschooldata : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            hfSchoolID.Value = Request.QueryString["schoolid"];
            MagnetSchool school = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSchoolID.Value));
            lblSchoolName.Text = school.SchoolName;
        }
        
        hfStageID.Value = "2";
        if (Session["ReportID"] != null)
        {
            hfReportID.Value = Convert.ToString(Session["ReportID"]);
        }
        else
        {
            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/login.aspx");
        }

        SqlDataSource1.SelectParameters["GranteeReportID"].DefaultValue = Convert.ToString(Session["ReportID"]);
        SqlDataSource1.SelectParameters["StageID"].DefaultValue = "2";
        SqlDataSource1.SelectParameters["SchoolID"].DefaultValue = hfSchoolID.Value;
        SqlDataSource1.InsertParameters["GranteeReportID"].DefaultValue = Convert.ToString(Session["ReportID"]);
        SqlDataSource1.InsertParameters["StageID"].DefaultValue = "2";
        SqlDataSource1.InsertParameters["SchoolID"].DefaultValue = hfSchoolID.Value;
    }
    protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridHeaderItem)
        {
            InitRealMcCoyGridHeader();
        }
        
        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            GridNumericColumnEditor editor = item.EditManager.GetColumnEditor("AmericanIndian") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 40;

            editor = item.EditManager.GetColumnEditor("Asian") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 40;

            editor = item.EditManager.GetColumnEditor("AfricanAmerican") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 40;

            editor = item.EditManager.GetColumnEditor("Hispanic") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 40;

            editor = item.EditManager.GetColumnEditor("White") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 40;

            editor = item.EditManager.GetColumnEditor("Hawaiian") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 40;

            editor = item.EditManager.GetColumnEditor("MultiRacial") as GridNumericColumnEditor;
            editor.NumericTextBox.NumberFormat.AllowRounding = true;
            editor.NumericTextBox.NumberFormat.DecimalDigits = 0;
            editor.NumericTextBox.Width = 40;
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
    {
        UpdateRecord();
        if (e.Exception != null)
        {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("Product with ID " + id + " is updated!");
        }
    }

    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
    {
        UpdateRecord();
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("New product is inserted!");
        }
    }

    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
    {
        UpdateRecord();
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            UpdateRecord();
        }
        else
        {
            //SetMessage("Product with ID " + id + " is deleted!");
        }
    }
    private void UpdateRecord()
    {
        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 9);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 9;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
    }
    private IWGridItem[] GetRealMcCoyHeaders(int row)
    {
        ArrayList c = new ArrayList();
        IWGridItem hdr;

        string hdrtop = "rgHeader";
        switch (row)
        {
            case 0:
                hdr = new IWGridItem("Header0L1", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header1L1", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("GradeL1", 0, 1, "Grade", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AmericanIndianL1", 0, 2, "American Indian or Alaska Native", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AsianL1", 0, 2, "Asian", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AfricanAmericanL1", 0, 2, "Black or African-American", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HispanicL1", 0, 2, "Hispanic or Latino", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HawaiianL1", 0, 2, "Native Hawaiian or Other Pacific Islander", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("WhiteL1", 0, 2, "White", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MultiRacialL1", 0, 2, "Two or more races", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("total", 0, 1, "Total Students", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header2L1", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                /*hdr = new IWGridItem("Header3L1", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);*/
                break;
            case 1:
                hdrtop = "rgHeader2";
                hdr = new IWGridItem("Header0L2", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header1L2", 0, 1, "", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("GradeL2", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AmericanIndianL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AmericanIndianL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AsianL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AsianL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AfricanAmericanL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("AfricanAmericanL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HispanicL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HispanicL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HawaiianL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("HawaiianL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("WhiteL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("WhiteL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MultiRacialL2", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("MultiRacialL2P", 0, 1, "%", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("total", 0, 1, "#", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                hdr = new IWGridItem("Header2L2", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);
                /*hdr = new IWGridItem("Header3L2", 0, 1, "&nbsp;", true, HorizontalAlign.Center, hdrtop, null, null);
                c.Add(hdr);*/
            break;
        }
        return (IWGridItem[])c.ToArray(typeof(IWGridItem));
    }

    private void InitRealMcCoyGridHeader()
    {
        IWGridItem[] row1hdrs = GetRealMcCoyHeaders(0);
        IWGridItem[] row2hdrs = GetRealMcCoyHeaders(1);

        //get the current header   
        GridItem[] header = RadGrid1.MasterTableView.GetItems(GridItemType.Header);

        //get the current THead element   
        if (header.Length != 0)
        {
            GridTHead head = ((GridTHead)header[0].Parent.Controls[0].Parent);
            //Clear all GridHeaderItems   
            head.Controls.RemoveAt(1);

            //create a new GridHeaderItem which will be the new row   
            GridHeaderItem hdr1Item = new GridHeaderItem(RadGrid1.MasterTableView, 0, 0);
            GridHeaderItem hdr2Item = new GridHeaderItem(RadGrid1.MasterTableView, 0, 0);

            int hdr1indx = 0; // logical column index  
            int hdr2indx = 0;
            int hdr1colindx = 0; // Physical column index  
            int hdr2colindx = 0;
            int hdr1nextcol = 0; // Next logical column  
            int hdr2nextcol = 0;
            int hdr1totalcols = 0;
            int hdr2totalcols = 0;
            int hdr1colcount = 0;
            int hdr2colcount = 0;
            
            foreach (IWGridItem hdr in row1hdrs)
            {
                hdr1totalcols += hdr.ColumnSpan;
            }

            foreach (IWGridItem hdr in row2hdrs)
            {
                hdr2totalcols += hdr.ColumnSpan;
            }

            do
            {
                GridTableHeaderCell cell;
                
                if ((hdr1colcount >= hdr1totalcols) && (hdr2colcount >= hdr2totalcols))
                {
                    break;
                }

                if (hdr1indx >= hdr1nextcol)
                {
                    cell = new GridTableHeaderCell
                    {
                        Text = row1hdrs[hdr1colindx].Text,
                        ColumnSpan = row1hdrs[hdr1colindx].ColumnSpan,
                        HorizontalAlign = row1hdrs[hdr1colindx].HorizontalAlign,
                        CssClass = row1hdrs[hdr1colindx].ClassOverride
                    };

                    hdr1Item.Cells.Add(cell);
                    hdr1nextcol = hdr1indx + row1hdrs[hdr1colindx].ColumnSpan;
                    hdr1colindx += 1;
                }

                hdr1colcount += 1;
                hdr1indx += 1;

                if (hdr2indx >= hdr2nextcol)
                {
                    cell = new GridTableHeaderCell
                    {
                        Text = row2hdrs[hdr2colindx].Text,
                        ColumnSpan = row2hdrs[hdr2colindx].ColumnSpan,
                        HorizontalAlign = row2hdrs[hdr2colindx].HorizontalAlign,
                        CssClass = row2hdrs[hdr2colindx].ClassOverride
                    };

                    hdr2Item.Cells.Add(cell);
                    hdr2nextcol = hdr2indx + row2hdrs[hdr2colindx].ColumnSpan;
                    hdr2colindx += 1;
                }

                hdr2colcount += 1;
                hdr2indx += 1;
            } while (true);

            //Add back the GridHeaderItems in the order you want them to appear   
            head.Controls.Add(hdr1Item);
            head.Controls.Add(hdr2Item);
        }
    }
    protected void RadGrid1_CustomAggregate(object sender, GridCustomAggregateEventArgs e)
    {
        string field = e.Column.UniqueName;

        MagnetDBDB db = new MagnetDBDB();
        var Data = from m in db.MagnetSchoolEnrollments
                               where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                               && m.SchoolID == Convert.ToInt32(hfSchoolID.Value)
                               && m.StageID == 2 //School enrollment data
                               orderby m.GradeLevel
                               select new { m.GradeLevel,
                                            AmericanIndian = m.AmericanIndian == null ? 0 : m.AmericanIndian,
                                            Asian = m.Asian == null ? 0 : m.Asian,
                                            AfricanAmerican = m.AfricanAmerican == null ? 0 : m.AfricanAmerican,
                                            Hispanic = m.Hispanic == null ? 0 : m.Hispanic,
                                            Hawaiian = m.Hawaiian == null ? 0 : m.Hawaiian,
                                            White = m.White == null ? 0 : m.White,
                                            MultiRacial = m.MultiRacial == null ? 0 : m.MultiRacial,
                                            GradeTotal = (m.AmericanIndian == null ? 0 : m.AmericanIndian
                                            + m.Asian == null ? 0 : m.Asian
                                            + m.AfricanAmerican == null ? 0 : m.AfricanAmerican
                                            + m.Hispanic == null ? 0 : m.Hispanic
                                            + m.Hawaiian == null ? 0 : m.Hawaiian
                                            + m.White == null ? 0 : m.White
                                            + m.MultiRacial == null ? 0 : m.MultiRacial)
                               };

                    int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                    int total = 0;
                    foreach (var Enrollment in Data)
                    {
                        total = 0;
                        if (Enrollment.AmericanIndian != null)
                        {
                            total += (int)Enrollment.AmericanIndian;
                        }
                        if (Enrollment.Asian != null)
                        {
                            total += (int)Enrollment.Asian;
                        }
                        if (Enrollment.AfricanAmerican != null)
                        {
                            total += (int)Enrollment.AfricanAmerican;
                        }
                        if (Enrollment.Hispanic != null)
                        {
                            total += (int)Enrollment.Hispanic;
                        }
                        if (Enrollment.Hawaiian != null)
                        {
                            total += (int)Enrollment.Hawaiian;
                        }
                        if (Enrollment.White != null)
                        {
                            total += (int)Enrollment.White;
                        }
                        if (Enrollment.MultiRacial != null)
                        {
                            total += (int)Enrollment.MultiRacial;
                        }
                        
                        if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += (int)Enrollment.AmericanIndian;
                        if (Enrollment.Asian != null) TotalEnrollments[1] += (int)Enrollment.Asian;
                        if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += (int)Enrollment.AfricanAmerican;
                        if (Enrollment.Hispanic != null) TotalEnrollments[3] += (int)Enrollment.Hispanic;
                        if (Enrollment.Hawaiian != null) TotalEnrollments[4] += (int)Enrollment.Hawaiian;
                        if (Enrollment.White != null) TotalEnrollments[5] += (int)Enrollment.White;
                        if (Enrollment.MultiRacial != null) TotalEnrollments[6] += (int)Enrollment.MultiRacial;
                        if (Enrollment.GradeTotal != null) TotalEnrollments[7] += total;
                    }


                    if (total > 0)
                    {
                        switch (field)
                        {

                            case "P1":
                                e.Result= ((double)TotalEnrollments[0] / TotalEnrollments[7] * 100); //AmericanIndia
                                break;

                            case "P2":
                                e.Result= ((double)TotalEnrollments[1] / TotalEnrollments[7] * 100);//Asian
                                break;

                            case "P3":
                                e.Result = ((double)TotalEnrollments[2] / TotalEnrollments[7] * 100);//AfricaAmerica
                                break;
                                
                            case "P4":
                                e.Result = ((double)TotalEnrollments[3] / TotalEnrollments[7] * 100); //Hispanic
                                break;

                            case "P5":
                                e.Result = ((double)TotalEnrollments[5] / TotalEnrollments[7] * 100); //Hawaiian
                                break;

                            case "P6":
                                e.Result = ((double)TotalEnrollments[4] / TotalEnrollments[7] * 100); //White
                                break;

                            case "P7":
                                e.Result = ((double)TotalEnrollments[6] / TotalEnrollments[7] * 100); //MultiRace
                                break;

                        }
                    }
                    else
                        e.Result = 0;
    }
}
