﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="Context.aspx.cs" Inherits="admin_data_Context" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit Context
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfID" runat="server" />
        <div>
            <h1>
                Add/Edit Context</h1>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                School:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                School Type:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSchoolType" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Historical Context:
                            </td>
                            <td>
                                <asp:TextBox ID="txtHistoricalContext" runat="server" MaxLength="5000" Width="450" CssClass="msapDataTxt"
                                    TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Title I funding:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTitleFunding" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Percent FRL:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPercentFRL" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPercentFRL"
                                    FilterType="Numbers">
                                </ajax:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Improvement stage:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlImprovementStage" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Years since made AYP:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlYearsAYP" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Desegregation Plan Type:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDesegregation" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Program Type:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProgramType" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Attendance Type:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAttendanceType" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Program Status:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProgramStatus" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Missing:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAdmMMissing" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Planning Year:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAdmMPlanning" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Open Lottery:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAdmMOpenLottery" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAdmMWeightedLottery" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Title I Status:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAdmMWeightedLotteryTitleIStatus" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Siblings:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlWeightedLotterySiblings" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Race/Ethnicity:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlWeightedLotteryRace" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Other:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlWeightedOther" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Other_Specify:
                            </td>
                            <td>
                                <asp:TextBox ID="txtWeightedLottery" runat="server" MaxLength="5000" Width="450" CssClass="msapDataTxt"
                                    TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Selective Admissions:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAdmMSelectiveAdmissions" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Notes:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNotes" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine" CssClass="msapDataTxt"
                                    Rows="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />&nbsp;&nbsp;
                                <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Return" OnClick="OnReturn" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button14" />
                    <asp:AsyncPostBackTrigger ControlID="Button15" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
