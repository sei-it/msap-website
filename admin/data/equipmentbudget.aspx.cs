﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_equipmentbudget : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/default.aspx");
            }
            if(Request.QueryString["type"].Equals("1"))
            {
                hfReportType.Value = "7";
                lblBudgetType.Text = "Federal Funds";
            }else
            {
                hfReportType.Value = "8";
                lblBudgetType.Text = "Non Federal Funds";
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == Convert.ToInt32(hfReportType.Value));
            if (data.Count == 1)
            {
                hfSummaryID.Value = data[0].ID.ToString();
                txtSummary.Text = data[0].ReportSummary;
            }
            RegisterHelper();
            LoadData();
        }
    }
    private void RegisterHelper()
    {
        GridViewHelper helper = new GridViewHelper(this.GridView1);
        helper.RegisterSummary("ApprovedFederalFunds", SummaryOperation.Sum);
        helper.RegisterSummary("BudgetExpenditures", SummaryOperation.Sum);
        helper.RegisterSummary("Carryover", SummaryOperation.Sum);
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetEquipmentBudgets
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.ReportType == Convert.ToInt32(hfReportType.Value)
                   orderby m.Item
                   select new { m.ID, m.Item, m.UnitCost, m.NumberOfUnites, m.ApprovedFederalFunds,
                                BudgetExpenditures = (m.UnitCost != null ? Convert.ToInt32(m.UnitCost * m.NumberOfUnites) : 0),
                                Carryover = (m.UnitCost != null ? Convert.ToInt32(m.ApprovedFederalFunds - m.UnitCost * m.NumberOfUnites) : 0)
                   };
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtPersonnel.Text = "";
        txtAnnualSalary.Text = "";
        txtPercentage.Text = "";
        txtApprovedFunds.Text = "";
        hfID.Value = "";
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetEquipmentBudget data = MagnetEquipmentBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtPersonnel.Text = data.Item;
        if (data.ApprovedFederalFunds != null) txtApprovedFunds.Text = Convert.ToString(data.ApprovedFederalFunds);
        if (data.UnitCost != null) txtAnnualSalary.Text = Convert.ToString(data.UnitCost);
        if (data.NumberOfUnites != null) txtPercentage.Text = Convert.ToString(data.NumberOfUnites);
        mpeWindow.Show();
    }
    protected void OnNewBudget(object sender, EventArgs e)
    {
        ClearFields();
        mpeWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetEquipmentBudget data = new MagnetEquipmentBudget();
        if (!string.IsNullOrEmpty(hfID.Value))
            data = MagnetEquipmentBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
            data.ReportType = Convert.ToInt32(hfReportType.Value);
        }
        data.ApprovedFederalFunds = string.IsNullOrEmpty(txtApprovedFunds.Text) ? 0 : Convert.ToInt32(txtApprovedFunds.Text); data.Item = txtPersonnel.Text;
        data.UnitCost = string.IsNullOrEmpty(txtAnnualSalary.Text) ? 0 : Convert.ToDecimal(txtAnnualSalary.Text);
        data.NumberOfUnites = string.IsNullOrEmpty(txtPercentage.Text) ? 0 : Convert.ToInt32(txtPercentage.Text);
        data.Save();
        RegisterHelper();
        LoadData();
    }
    protected void OnSaveSummary(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSummary.Text))
        {
            //Save to database
            MagnetBudgetSummary summary = new MagnetBudgetSummary();
            if (!string.IsNullOrEmpty(hfSummaryID.Value))
                summary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSummaryID.Value));
            else
            {
                summary.ReportID = Convert.ToInt32(hfReportID.Value);
                summary.ReportType = Convert.ToInt32(hfReportType.Value);
            }
            summary.ReportSummary = txtSummary.Text;
            summary.Save();
        }
    }
}