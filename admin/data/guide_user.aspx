﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="guide_user.aspx.cs" Inherits="admin_data_guide_user" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    User Guide
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        $(function () {
            $("input:submit").click(function () {
                $("#hfControlID").val($(this).attr("id"));
            });
        });

    </script>
       <style>
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
            text-align: center !important;
        }
        .msapDataTbl th
        {
            color: #000;
            text-align: left;
        }
		.msapDataTbl TDWithBottomNoRightBorder{
			text-align: center;
		}
        .TDCenterClass
        {
            text-align: center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        User Guide</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p>
    Please read all guidance documents and the worksheets below. The documents are intended to help users fill out the forms found in MAPS.
    </p>
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" alt="" align="middle" />&nbsp;&nbsp; <b>User Guide Documents</b></span>
        <div id="user_guide_pg">
         <table class="msapDataTbl">
                    <tr>
                        <th>
                            Document Name
                        </th>
                        <th class="TDCenterClass">
                            File Type
                        </th>
                    </tr>
	   <asp:Repeater ID="dlstUguide"   runat="server">
      
       <ItemTemplate>
            <tr >
                <td><%# DataBinder.Eval(Container.DataItem, "fileType")%></td>
                        
                <td class="TDWithBottomNoRightBorder">
                    <a href='<%# DataBinder.Eval(Container.DataItem, "fileURI")%>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "fileExt")%></a>
                </td>
            </tr>
       </ItemTemplate>
       </asp:Repeater>
       </table>
            
        </div>
        
        
        <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>APR Data Resources</b></span>
        <ContentTemplate>
        <div id="user_guide_pg">
	   
            <table class="msapDataTbl">
                    <tr>
                        <th colspan="2">
                           Federal Government Sponsored Websites
                        </th>
                    </tr>
                    <tr>
                        <td>Common Core of Data (CCD)</td>
                        
                        <td class="TDWithBottomNoRightBorder">
                           <a href="http://nces.ed.gov/ccd/ccseas.asp" target="_blank">http://nces.ed.gov/ccd/ccseas.asp</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Education Resource Organization Directory <br/>(State Department of Education Listing)</td>
                        
                        <td class="TDWithBottomNoRightBorder">
                           <a href="http://wdcrobcolp01.ed.gov/Programs/EROD/org_list.cfm?category_ID=SEA" target="_blank">http://wdcrobcolp01.ed.gov/Programs/EROD/org_list.cfm?category_ID=SEA</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                         State Profiles (NAEP)
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                           <a href="http://nces.ed.gov/nationsreportcard/states/" target="_blank">http://nces.ed.gov/nationsreportcard/states/</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           National Center for Education Statistics (NCES)
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <a href="http://nces.ed.gov/nationsreportcard/studies/statemapping/faq.asp" target="_blank">http://nces.ed.gov/nationsreportcard/studies/statemapping/faq.asp</a>
                        </td>
                    </tr>           
                </table>
        </div>
        </ContentTemplate>

   
</asp:Content>


