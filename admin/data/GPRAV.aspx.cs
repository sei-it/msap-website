﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_GPRAV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Session["isMagnetHighSchool"] != null && (bool)Session["isMagnetHighSchool"]))
            setPageUnable();
      
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            //Report type
            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;

            if (Request.QueryString["id"] == null)
                Response.Redirect("managegpra.aspx");
            else
            {
                LinkI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
                tabPartI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
                LinkII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
                tabPartII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
                LinkIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                tabPartIII.HRef = "gpraiii.aspx?id=" + Request.QueryString["id"];
                LinkIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                tabPartIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
                LinkVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];
                tabPartVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];

                int schoolID = Convert.ToInt32(Request.QueryString["id"]);
                lblSchoolName.Text = MagnetSchool.SingleOrDefault(x => x.ID == schoolID).SchoolName;

                lblReportYear.Text = MagnetReportPeriod.SingleOrDefault(x => x.ID == (Convert.ToInt32(Session["ReportPeriodID"]))).ReportPeriod;
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == Convert.ToInt32(hfReportID.Value));
                if (gpraData.Count > 0)
                {
                    hfID.Value = gpraData[0].ID.ToString();

                    //gpra v
                    var performanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 8);
                    if (performanceMeasure.Count > 0)
                    {
                        setFields(performanceMeasure[0].ID);
                    }
                    else
                    {
                        setFields(0);
                    }
                }
                else
                {
                    //MagnetGPRA item = new MagnetGPRA();
                    //item.ReportID = Convert.ToInt32(hfReportID.Value);
                    //item.SchoolID = schoolID;
                    //item.Save();
                    //hfID.Value = item.ID.ToString();

                    //MagnetGPRAPerformanceMeasure mpm = new MagnetGPRAPerformanceMeasure();
                    //mpm.ReportType = 8;
                    //mpm.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    //mpm.Save();
                    //hfPerformanceMeasure.Value = mpm.ID.ToString();
                }
            }
        }
    }

    private void setPageUnable()
    {
        txtAllcohort.Enabled = false;
        txtAllgraduated.Enabled = false;
        txtAmericanIndiancohort.Enabled = false;
        txtAmericanIndiangraduated.Enabled = false;
        txtAsiancohort.Enabled = false;
        txtAsiangraduated.Enabled = false;
        txtAfricanAmericancohort.Enabled = false;
        txtAfricanAmericangraduated.Enabled = false;
        txtHispanicLatinocohort.Enabled = false;
        txtHispanicLatinograduated.Enabled = false;
        txtNativeHawaiiancohort.Enabled = false;
        txtNativeHawaiiangraduated.Enabled = false;
        txtWhitecohort.Enabled = false;
        txtWhitegraduated.Enabled = false;
        txtMoreRacescohort.Enabled = false;
        txtMoreRacesgraduated.Enabled = false;
        txtEconomicallycohort.Enabled = false;
        txtEconomicallygraduated.Enabled = false;
        txtEnglishlearnerscohort.Enabled = false;
        txtEnglishlearnersgraduated.Enabled = false;

        Button1.Enabled = false;
    }

    private void setFields(int PerfMeasureID)
    {
        if (PerfMeasureID == 0)
        {
            txtAllcohort.Text = "";
            txtAllgraduated.Text = "";
            txtallpercentage.Text = "";

            txtAmericanIndiancohort.Text = "";
            txtAmericanIndiangraduated.Text = "";
            txtAfricanAmericanpercentage.Text = "";

            txtAsiancohort.Text = "";
            txtAsiangraduated.Text = "";
            txtAsianpercentage.Text = "";

            txtAfricanAmericancohort.Text = "";
            txtAfricanAmericangraduated.Text = "";
            txtAfricanAmericanpercentage.Text = "";

            txtHispanicLatinocohort.Text = "";
            txtHispanicLatinograduated.Text = "";
            txtHispanicLatinopercentage.Text = "";

            txtNativeHawaiiancohort.Text = "";
            txtNativeHawaiiangraduated.Text = "";
            txtNativeHawaiianpercentage.Text = "";

            txtWhitecohort.Text = "";
            txtWhitegraduated.Text = "";
            txtWhitepercentage.Text = "";

            txtMoreRacescohort.Text = "";
            txtMoreRacesgraduated.Text = "";
            txtMoreRacespercentage.Text = "";

            txtEconomicallycohort.Text = "";
            txtEconomicallygraduated.Text = "";
            txtEconomicallypercentage.Text = "";

            txtEnglishlearnerscohort.Text = "";
            txtEnglishlearnersgraduated.Text = "";
            txtEnglishlearnerspercentage.Text ="";
        }
        else
        {
            MagnetGPRAPerformanceMeasure Item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == PerfMeasureID);
            hfPerformanceMeasure.Value = Item.ID.ToString();
            txtAllcohort.Text = Item.Allcohort == null ? "" : Item.Allcohort.ToString();
            txtAllgraduated.Text = Item.Allgraduated == null ? "" : Item.Allgraduated.ToString();
            txtallpercentage.Text = Item.allpercentage == null ? "" : Item.allpercentage;

            txtAmericanIndiancohort.Text = Item.AmericanIndiancohort == null ? "" : Item.AmericanIndiancohort.ToString();
            txtAmericanIndiangraduated.Enabled = txtAmericanIndiancohort.Text == "0" ? false : true;
            txtAmericanIndiangraduated.Text = Item.AmericanIndiangraduated == null ? "" : Item.AmericanIndiangraduated.ToString();
            txtAmericanIndianpercentage.Text = Item.AmericanIndianpercentage == null ? "" : Item.AmericanIndianpercentage;

            txtAsiancohort.Text = Item.Asiancohort == null ? "" : Item.Asiancohort.ToString();
            txtAsiangraduated.Enabled = txtAsiancohort.Text == "0" ? false : true;
            txtAsiangraduated.Text = Item.Asiangraduated == null ? "" : Item.Asiangraduated.ToString();
            txtAsianpercentage.Text = Item.Asianpercentage == null ? "" : Item.Asianpercentage;

            txtAfricanAmericancohort.Text = Item.AfricanAmericancohort == null ? "" : Item.AfricanAmericancohort.ToString();
            txtAfricanAmericangraduated.Enabled = txtAfricanAmericancohort.Text == "0" ? false : true;
            txtAfricanAmericangraduated.Text = Item.AfricanAmericangraduated == null ? "" : Item.AfricanAmericangraduated.ToString();
            txtAfricanAmericanpercentage.Text = Item.AfricanAmericanpercentage == null ? "" : Item.AfricanAmericanpercentage;

            txtHispanicLatinocohort.Text = Item.HispanicLatinocohort == null ? "" : Item.HispanicLatinocohort.ToString();
            txtHispanicLatinograduated.Enabled = txtHispanicLatinocohort.Text == "0" ? false : true;
            txtHispanicLatinograduated.Text = Item.HispanicLatinograduated == null ? "" : Item.HispanicLatinograduated.ToString();
            txtHispanicLatinopercentage.Text = Item.HispanicLatinopercentage == null ? "" : Item.HispanicLatinopercentage;

            txtNativeHawaiiancohort.Text = Item.NativeHawaiiancohort == null ? "" : Item.NativeHawaiiancohort.ToString();
            txtNativeHawaiiangraduated.Enabled = txtNativeHawaiiancohort.Text == "0" ? false : true;
            txtNativeHawaiiangraduated.Text = Item.NativeHawaiiangraduated == null ? "" : Item.NativeHawaiiangraduated.ToString();
            txtNativeHawaiianpercentage.Text = Item.NativeHawaiianpercentage == null ? "" : Item.NativeHawaiianpercentage;

            txtWhitecohort.Text = Item.Whitecohort == null ? "" : Item.Whitecohort.ToString();
            txtWhitegraduated.Enabled = txtWhitecohort.Text == "0" ? false : true;
            txtWhitegraduated.Text = Item.Whitegraduated == null ? "" : Item.Whitegraduated.ToString();
            txtWhitepercentage.Text = Item.Whitepercentage == null ? "" : Item.Whitepercentage;

            txtMoreRacescohort.Text = Item.MoreRacescohort == null ? "" : Item.MoreRacescohort.ToString();
            txtMoreRacesgraduated.Enabled = txtMoreRacescohort.Text == "0" ? false : true;
            txtMoreRacesgraduated.Text = Item.MoreRacesgraduated == null ? "" : Item.MoreRacesgraduated.ToString();
            txtMoreRacespercentage.Text = Item.MoreRacespercentage == null ? "" : Item.MoreRacespercentage;

            txtEconomicallycohort.Text = Item.Economicallycohort == null ? "" : Item.Economicallycohort.ToString();
            txtEconomicallygraduated.Enabled = txtEconomicallycohort.Text == "0" ? false : true;
            txtEconomicallygraduated.Text = Item.Economicallygraduated == null ? "" : Item.Economicallygraduated.ToString();
            txtEconomicallypercentage.Text = Item.Economicallypercentage == null ? "" : Item.Economicallypercentage;

            txtEnglishlearnerscohort.Text = Item.Englishlearnerscohort == null ? "" : Item.Englishlearnerscohort.ToString();
            txtEnglishlearnersgraduated.Enabled = txtEnglishlearnerscohort.Text == "0" ? false : true;
            txtEnglishlearnersgraduated.Text = Item.Englishlearnersgraduated == null ? "" : Item.Englishlearnersgraduated.ToString();
            txtEnglishlearnerspercentage.Text = Item.Englishlearnerspercentage == null ? "" : Item.Englishlearnerspercentage;

        }

    }
    protected void OnSave(object sender, EventArgs e)
    {
        int schoolID = Convert.ToInt32(Request.QueryString["id"]);

        if (string.IsNullOrEmpty(hfID.Value))
        {
            MagnetGPRA Newitem = new MagnetGPRA();
            Newitem.ReportID = Convert.ToInt32(hfReportID.Value);
            Newitem.SchoolID = schoolID;
            Newitem.Save();
            hfID.Value = Newitem.ID.ToString();
        }

        if (string.IsNullOrEmpty(hfPerformanceMeasure.Value))
        {
            MagnetGPRAPerformanceMeasure mpm = new MagnetGPRAPerformanceMeasure();
            mpm.ReportType = 8;
            mpm.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            mpm.Save();
            hfPerformanceMeasure.Value = mpm.ID.ToString();
        }


        MagnetGPRAPerformanceMeasure item = new MagnetGPRAPerformanceMeasure();
        if (!string.IsNullOrEmpty(hfPerformanceMeasure.Value))
            item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfPerformanceMeasure.Value));
        item.Allcohort = string.IsNullOrEmpty(txtAllcohort.Text) ? (int?)null : Convert.ToInt32(txtAllcohort.Text);
        item.Allgraduated = string.IsNullOrEmpty(txtAllgraduated.Text) ? (int?)null : Convert.ToInt32(txtAllgraduated.Text);
        if (!string.IsNullOrEmpty(txtAllcohort.Text) && (!string.IsNullOrEmpty(txtAllgraduated.Text) && Convert.ToInt32(txtAllgraduated.Text)>0))
            item.allpercentage = (Convert.ToInt32(txtAllgraduated.Text) * 100.00 / Convert.ToInt32(txtAllcohort.Text) / 100).ToString("0.0%");
        else
            item.allpercentage = "";
        //2
        item.AmericanIndiancohort = string.IsNullOrEmpty(txtAmericanIndiancohort.Text) ? (int?)null : Convert.ToInt32(txtAmericanIndiancohort.Text);
        item.AmericanIndiangraduated = string.IsNullOrEmpty(txtAmericanIndiangraduated.Text) ? (int?)null : Convert.ToInt32(txtAmericanIndiangraduated.Text);
        if (!string.IsNullOrEmpty(txtAmericanIndiancohort.Text) && !string.IsNullOrEmpty(txtAmericanIndiangraduated.Text) && Convert.ToInt32(txtAmericanIndiangraduated.Text) > 0)
            item.AmericanIndianpercentage = (Convert.ToInt32(txtAmericanIndiangraduated.Text) * 100.00 / Convert.ToInt32(txtAmericanIndiancohort.Text)/100).ToString("0.0%");
        else
            item.AmericanIndianpercentage = "";
        //3
        item.Asiancohort = string.IsNullOrEmpty(txtAsiancohort.Text) ? (int?)null : Convert.ToInt32(txtAsiancohort.Text);
        item.Asiangraduated = string.IsNullOrEmpty(txtAsiangraduated.Text) ? (int?)null : Convert.ToInt32(txtAsiangraduated.Text);
        if (!string.IsNullOrEmpty(txtAsiancohort.Text) && !string.IsNullOrEmpty(txtAsiangraduated.Text) && Convert.ToInt32(txtAsiangraduated.Text) > 0)
            item.Asianpercentage = (Convert.ToInt32(txtAsiangraduated.Text) * 100.00 / Convert.ToInt32(txtAsiancohort.Text)/100).ToString("0.0%");
        else
            item.Asianpercentage = "";
        //4
        item.AfricanAmericancohort = string.IsNullOrEmpty(txtAfricanAmericancohort.Text) ? (int?)null : Convert.ToInt32(txtAfricanAmericancohort.Text);
        item.AfricanAmericangraduated = string.IsNullOrEmpty(txtAfricanAmericangraduated.Text) ? (int?)null : Convert.ToInt32(txtAfricanAmericangraduated.Text);
        if (!string.IsNullOrEmpty(txtAfricanAmericancohort.Text) && !string.IsNullOrEmpty(txtAfricanAmericangraduated.Text)
            && Convert.ToInt32(txtAfricanAmericangraduated.Text) > 0)
            item.AfricanAmericanpercentage = (Convert.ToInt32(txtAfricanAmericangraduated.Text) * 100.00 / Convert.ToInt32(txtAfricanAmericancohort.Text)/100).ToString("0.0%");
        else
            item.AfricanAmericanpercentage = "";
        //5
        item.HispanicLatinocohort = string.IsNullOrEmpty(txtHispanicLatinocohort.Text) ? (int?)null : Convert.ToInt32(txtHispanicLatinocohort.Text);
        item.HispanicLatinograduated = string.IsNullOrEmpty(txtHispanicLatinograduated.Text) ? (int?)null : Convert.ToInt32(txtHispanicLatinograduated.Text);
        if (!string.IsNullOrEmpty(txtHispanicLatinocohort.Text) && !string.IsNullOrEmpty(txtHispanicLatinograduated.Text) && Convert.ToInt32(txtHispanicLatinograduated.Text) > 0)
            item.HispanicLatinopercentage = (Convert.ToInt32(txtHispanicLatinograduated.Text) * 100.00 / Convert.ToInt32(txtHispanicLatinocohort.Text)/100).ToString("0.0%");
        else
            item.HispanicLatinopercentage = "";

        //6
        item.NativeHawaiiancohort = string.IsNullOrEmpty(txtNativeHawaiiancohort.Text) ? (int?)null : Convert.ToInt32(txtNativeHawaiiancohort.Text);
        item.NativeHawaiiangraduated = string.IsNullOrEmpty(txtNativeHawaiiangraduated.Text) ? (int?)null : Convert.ToInt32(txtNativeHawaiiangraduated.Text);
        if (!string.IsNullOrEmpty(txtNativeHawaiiancohort.Text) && !string.IsNullOrEmpty(txtNativeHawaiiangraduated.Text) && Convert.ToInt32(txtNativeHawaiiangraduated.Text) > 0)
        {
            item.NativeHawaiianpercentage = (Convert.ToInt32(txtNativeHawaiiangraduated.Text) * 100.00 / Convert.ToInt32(txtNativeHawaiiancohort.Text) / 100).ToString("0.0%");
        }
        else
            item.NativeHawaiianpercentage = "";

        //7
        item.Whitecohort = string.IsNullOrEmpty(txtWhitecohort.Text) ? (int?)null : Convert.ToInt32(txtWhitecohort.Text);
        item.Whitegraduated = string.IsNullOrEmpty(txtWhitegraduated.Text) ? (int?)null : Convert.ToInt32(txtWhitegraduated.Text);
        if (!string.IsNullOrEmpty(txtWhitecohort.Text) && !string.IsNullOrEmpty(txtWhitegraduated.Text) && Convert.ToInt32(txtWhitegraduated.Text) > 0)
            item.Whitepercentage = (Convert.ToInt32(txtWhitegraduated.Text) * 100.00 / Convert.ToInt32(txtWhitecohort.Text)/100).ToString("0.0%");
        else
            item.Whitepercentage = "";

        //8
        item.MoreRacescohort = string.IsNullOrEmpty(txtMoreRacescohort.Text) ? (int?)null : Convert.ToInt32(txtMoreRacescohort.Text);
        item.MoreRacesgraduated = string.IsNullOrEmpty(txtMoreRacesgraduated.Text) ? (int?)null : Convert.ToInt32(txtMoreRacesgraduated.Text);
        if (!string.IsNullOrEmpty(txtMoreRacescohort.Text) && !string.IsNullOrEmpty(txtMoreRacesgraduated.Text) && Convert.ToInt32(txtMoreRacesgraduated.Text) > 0)
            item.MoreRacespercentage = (Convert.ToInt32(txtMoreRacesgraduated.Text) * 100.00 / Convert.ToInt32(txtMoreRacescohort.Text)/100).ToString("0.0%");
        else
            item.MoreRacespercentage = "";
        //9
        item.Economicallycohort = string.IsNullOrEmpty(txtEconomicallycohort.Text) ? (int?)null : Convert.ToInt32(txtEconomicallycohort.Text);
        item.Economicallygraduated = string.IsNullOrEmpty(txtEconomicallygraduated.Text) ? (int?)null : Convert.ToInt32(txtEconomicallygraduated.Text);
        if (!string.IsNullOrEmpty(txtEconomicallycohort.Text) && !string.IsNullOrEmpty(txtEconomicallygraduated.Text) && Convert.ToInt32(txtEconomicallygraduated.Text) > 0)
        {
            item.Economicallypercentage = (Convert.ToInt32(txtEconomicallygraduated.Text) * 100.00 / Convert.ToInt32(txtEconomicallycohort.Text) / 100).ToString("0.0%");
        }
        else
            item.Economicallypercentage = "";
        //10
        item.Englishlearnerscohort = string.IsNullOrEmpty(txtEnglishlearnerscohort.Text) ? (int?)null : Convert.ToInt32(txtEnglishlearnerscohort.Text);
        item.Englishlearnersgraduated = string.IsNullOrEmpty(txtEnglishlearnersgraduated.Text) ? (int?)null : Convert.ToInt32(txtEnglishlearnersgraduated.Text);
        if (!string.IsNullOrEmpty(txtEnglishlearnerscohort.Text) && !string.IsNullOrEmpty(txtEnglishlearnersgraduated.Text) && Convert.ToInt32(txtEnglishlearnersgraduated.Text) > 0)
            item.Englishlearnerspercentage = (Convert.ToInt32(txtEnglishlearnersgraduated.Text) * 100.00 / Convert.ToInt32(txtEnglishlearnerscohort.Text)/100).ToString("0.0%");
        else
            item.Englishlearnerspercentage = "";

        item.ReportType = 8;
        item.MagnetGPRAID = Convert.ToInt32(hfID.Value);

        item.Save();

        setFields(item.ID);

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 3);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 3;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    
    }
}