﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_report_leaenrollment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            hfStageID.Value = "1";

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            hfGranteeID.Value = granteeID.ToString();

            int reportPeriod = 1;
            if (Session["ReportID"] != null)
            {
                reportPeriod = (int)Session["ReportID"];
                hfReportID.Value = reportPeriod.ToString();
            }
            else
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }

            LoadGrade();
            GranteeReport Report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
            if (Report.FirstTimeMagnetProgram != null && Report.FirstTimeMagnetProgram == true)
                ckbFirstTimeProgram.Checked = true;
            LoadEnrollmentData();
        }
    }
    private void LoadGrade()
    {
        //Grade
        foreach (MagnetDLL item in MagnetDLL.Find(x => x.TypeID == 1))
        {
            ddlGradeLevel.Items.Add(new System.Web.UI.WebControls.ListItem(item.TypeName, item.TypeIndex.ToString()));
        }
    }
    protected void OnFirstTimeProgramChanged(object sender, EventArgs e)
    {
        GranteeReport Report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        Report.FirstTimeMagnetProgram = ckbFirstTimeProgram.Checked ? true : false;
        Report.Save();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetSchoolEnrollment data = MagnetSchoolEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        ddlGradeLevel.SelectedValue = data.GradeLevel.ToString();
        txtAmericanIndian.Text = data.AmericanIndian.ToString();
        txtAsian.Text = data.Asian.ToString();
        txtBlack.Text = data.AfricanAmerican.ToString();
        txtHispanic.Text = data.Hispanic.ToString();
        txtWhite.Text = data.White.ToString();
        txtHawaiian.Text = data.Hawaiian.ToString();
        txtMultiRaces.Text = data.MultiRacial.ToString();
        mpeResourceWindow.Show();
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetSchoolEnrollment.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadGrade();
        LoadEnrollmentData();
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        MagnetSchoolEnrollment data = new MagnetSchoolEnrollment();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetSchoolEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            data.GranteeReportID = Convert.ToInt32(hfReportID.Value);
            data.StageID = Convert.ToInt32(hfStageID.Value);
        }
        data.GradeLevel = Convert.ToInt32(ddlGradeLevel.SelectedValue);
        data.AmericanIndian = string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtAmericanIndian.Text));
        data.Asian = string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtAsian.Text));
        data.AfricanAmerican = string.IsNullOrEmpty(txtBlack.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtBlack.Text));
        data.Hispanic = string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtHispanic.Text));
        data.White = string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtWhite.Text));
        data.Hawaiian = string.IsNullOrEmpty(txtHawaiian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtHawaiian.Text));
        data.MultiRacial = string.IsNullOrEmpty(txtMultiRaces.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtMultiRaces.Text));
        data.Save();
        LoadEnrollmentData();
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        LoadEnrollmentData();
        ClearFields();
        mpeResourceWindow.Show();
    }
    private void ClearFields()
    {
        hfID.Value = "";
        ddlGradeLevel.SelectedIndex = 0;
        txtAmericanIndian.Text = "";
        txtAsian.Text = "";
        txtBlack.Text = "";
        txtHispanic.Text = "";
        txtWhite.Text = "";
        txtHawaiian.Text = "";
        txtMultiRaces.Text = "";
        lblMessage.Text = "";
    }
    private void LoadEnrollmentData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetSchoolEnrollments
                   from n in db.MagnetDLLs
                   where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                    && m.StageID == Convert.ToInt32(hfStageID.Value)
                    && m.GradeLevel == n.TypeIndex
                    && n.TypeID == 1
                   orderby m.GradeLevel
                   select new { m.ID, GradeLevel = n.TypeName, m.AmericanIndian, m.Asian, m.AfricanAmerican, m.Hispanic, m.White, m.Hawaiian, m.MultiRacial };
        GridView1.DataSource = data;
        //GridView1.DataBind();

        /*foreach (var enrollment in data)
        {
            ddlGradeLevel.Items.Remove(ddlGradeLevel.Items.FindByText(enrollment.GradeLevel));
        }*/
    }
  /*YPA 11/30/11 Removed the Proceed to Table 8 button*/
  /*protected void OnProceed(object sender, EventArgs e)
    {
        Response.Redirect("manageschoolyear.aspx", true);
    }-->*/
}