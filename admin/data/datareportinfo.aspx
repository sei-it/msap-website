﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="datareportinfo.aspx.cs" Inherits="admin_data_datareportinfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Data Report Info
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        <a href="datareport.aspx">Home</a> --> Data Report Status</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <table>
        <tr>
            <td>
                Site Visit:
            </td>
            <td>
                <asp:DropDownList ID="ddlSiteVisit" runat="server" CssClass="msapDataTxt">
                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Site Visit Occurred:
            </td>
            <td>
                <asp:DropDownList ID="ddlSiteVisitOccurred" runat="server" CssClass="msapDataTxt">
                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Notes:
            </td>
            <td>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="msapDataTxt" MaxLength="5000" Width="450"
                    TextMode="MultiLine" Rows="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan='2' align="center">
                <asp:Button ID="Button1" runat="server" Text="Save" CssClass="msapBtn" OnClick="OnSaveData" />
            </td>
        </tr>
    </table>
</asp:Content>
