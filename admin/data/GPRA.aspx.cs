﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_gpra : System.Web.UI.Page
{
    int reportyear = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            reportyear = Convert.ToInt32(Session["ReportYear"]);

            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            //Report type
            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
            foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == granteeID && x.ReportYear==reportyear && x.isActive))
            {
                ddlSchool.Items.Add(new ListItem(School.SchoolName, School.ID.ToString()));
            }
            foreach (MagnetDLL Item in MagnetDLL.Find(x => x.TypeID == 29).OrderBy(x => x.TypeIndex))
            {
                cblGrades.Items.Add(new ListItem(Item.TypeName, Item.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("managegpra.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadData(id);
                }
                else
                {
                    MagnetGPRA item = new MagnetGPRA();
                    item.ReportID = Convert.ToInt32(hfReportID.Value);
                    item.Save();
                    hfID.Value = item.ID.ToString();
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetGPRA item = new MagnetGPRA();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            item = MagnetGPRA.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            item.ReportID = Convert.ToInt32(hfReportID.Value);
        }
        item.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        string str = "";
        foreach (ListItem li in cblGrades.Items)
        {
            if (li.Selected)
            {
                if (!string.IsNullOrEmpty(str))
                    str += ";";
                str += li.Value;
            }
        }
        item.SchoolGrade = str;
        item.ProgramType = Convert.ToBoolean(Convert.ToInt32(ddlProgramType.SelectedValue));
        item.TitleISchoolFunding = Convert.ToBoolean(Convert.ToInt32(ddlTitleISchoolFunding.SelectedValue));
        if(ddlTitleISchoolImprovement.SelectedIndex>0) item.TitleISchoolFundingImprovement = Convert.ToBoolean(Convert.ToInt32(ddlTitleISchoolImprovement.SelectedValue));
        if (ddlImprovementStatus.SelectedIndex > 0) item.TitleISchoolFundingImprovementStatus = Convert.ToInt32(ddlImprovementStatus.SelectedValue);
        if (ddlLowestArchieving.SelectedIndex > 0) item.PersistentlyLlowestAchievingSchool = Convert.ToBoolean(Convert.ToInt32(ddlLowestArchieving.SelectedValue));
        if (ddlSIGFunds.SelectedIndex > 0) item.SchoolImprovementGrant = Convert.ToBoolean(Convert.ToInt32(ddlSIGFunds.SelectedValue));
        item.Save();
        hfID.Value = item.ID.ToString();
    }
    private void LoadData(int ID)
    {
        MagnetGPRA item = MagnetGPRA.SingleOrDefault(x => x.ID == ID);
        ddlSchool.SelectedValue = Convert.ToString(item.SchoolID);
        if (!string.IsNullOrEmpty(item.SchoolGrade))
        {
            foreach (string str in item.SchoolGrade.Split(';'))
            {
                foreach (ListItem li in cblGrades.Items)
                {
                    if (li.Value.Equals(str))
                        li.Selected = true;
                }
            }
        }
        ddlProgramType.SelectedValue = Convert.ToString(Convert.ToInt32(item.ProgramType));
        ddlTitleISchoolFunding.SelectedValue = Convert.ToString(Convert.ToInt32(item.TitleISchoolFunding));
        ddlTitleISchoolImprovement.SelectedValue = Convert.ToString(Convert.ToInt32(item.TitleISchoolFundingImprovement));
        ddlImprovementStatus.SelectedValue = Convert.ToString(item.TitleISchoolFundingImprovementStatus);
        ddlLowestArchieving.SelectedValue = Convert.ToString(Convert.ToInt32(item.PersistentlyLlowestAchievingSchool));
        ddlSIGFunds.SelectedValue = Convert.ToString(Convert.ToInt32(item.SchoolImprovementGrant));
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("managegpra.aspx");
    }
    protected void OnSaveMeasureData(object sender, EventArgs e)
    {
        MagnetGPRAPerformanceMeasure item = new MagnetGPRAPerformanceMeasure();
        switch (hfTypeID.Value)
        {
            case "1":
                if (!string.IsNullOrEmpty(hfApplicationPool.Value))
                    item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfApplicationPool.Value));
                else
                {
                    item.ReportType = 1;
                }
                break;
            case "2":
                if (!string.IsNullOrEmpty(hfEnrollment.Value))
                    item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfEnrollment.Value));
                else
                {
                    item.ReportType = 2;
                }
                break;
            case "3":
                if (!string.IsNullOrEmpty(hfParticipationReading.Value))
                    item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfParticipationReading.Value));
                else
                {
                    item.ReportType = 3;
                }
                break;
            case "4":
                if (!string.IsNullOrEmpty(hfParticipationMath.Value))
                    item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfParticipationMath.Value));
                else
                {
                    item.ReportType = 4;
                }
                break;
            case "5":
                if (!string.IsNullOrEmpty(hfArchivementReading.Value))
                    item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfArchivementReading.Value));
                else
                {
                    item.ReportType = 5;
                }
                break;
            case "6":
                if (!string.IsNullOrEmpty(hfArchivementMath.Value))
                    item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfArchivementMath.Value));
                else
                {
                    item.ReportType = 6;
                }
                break;
            case "7":
                if (!string.IsNullOrEmpty(hfPerformanceMeasure.Value))
                    item = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfPerformanceMeasure.Value));
                else
                {
                    item.ReportType = 7;
                }
                break;
        }
        item.MagnetGPRAID = Convert.ToInt32(hfID.Value);
        item.Indian = string.IsNullOrEmpty(txtIndian.Text) ? 0 : Convert.ToInt32(txtIndian.Text);
        item.Asian = string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(txtAsian.Text);
        item.Black = string.IsNullOrEmpty(txtBlack.Text) ? 0 : Convert.ToInt32(txtBlack.Text);
        item.Hispanic = string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(txtHispanic.Text);
        item.Hawaiian = string.IsNullOrEmpty(txtHawaiian.Text) ? 0 : Convert.ToInt32(txtHawaiian.Text);
        item.White = string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(txtWhite.Text);
        item.MultiRaces = string.IsNullOrEmpty(txtMultiRaces.Text) ? 0 : Convert.ToInt32(txtMultiRaces.Text);
        item.Total = string.IsNullOrEmpty(txtTotalSutdents.Text) ? 0 : Convert.ToInt32(txtTotalSutdents.Text);
        item.ExtraFiled1 = string.IsNullOrEmpty(txtExtraField1.Text) ? 0 : Convert.ToInt32(txtExtraField1.Text);
        item.ExtraFiled2 = string.IsNullOrEmpty(txtExtraField2.Text) ? 0 : Convert.ToInt32(txtExtraField2.Text);
        item.Save();
        switch (hfTypeID.Value)
        {
            case "1":
                hfApplicationPool.Value = Convert.ToString(item.ID);
                break;
            case "2":
                hfEnrollment.Value = Convert.ToString(item.ID);
                break;
            case "3":
                hfParticipationReading.Value = Convert.ToString(item.ID);
                break;
            case "4":
                hfParticipationMath.Value = Convert.ToString(item.ID);
                break;
            case "5":
                hfArchivementReading.Value = Convert.ToString(item.ID);
                break;
            case "6":
                hfArchivementMath.Value = Convert.ToString(item.ID);
                break;
            case "7":
                hfPerformanceMeasure.Value = Convert.ToString(item.ID);
                break;
        }
    }
    private void LoadMeasureData(MagnetGPRAPerformanceMeasure Item)
    {
        txtIndian.Text = Convert.ToString(Item.Indian);
        txtAsian.Text = Convert.ToString(Item.Asian);
        txtBlack.Text = Convert.ToString(Item.Black);
        txtHispanic.Text = Convert.ToString(Item.Hispanic);
        txtHawaiian.Text = Convert.ToString(Item.Hawaiian);
        txtWhite.Text = Convert.ToString(Item.White);
        txtMultiRaces.Text = Convert.ToString(Item.MultiRaces);
        txtTotalSutdents.Text = Convert.ToString(Item.Total);
        txtExtraField1.Text = Convert.ToString(Item.ExtraFiled1);
        txtExtraField2.Text = Convert.ToString(Item.ExtraFiled2);
    }
    private void ClearFields()
    {
        txtIndian.Text = "";
        txtAsian.Text = "";
        txtBlack.Text = "";
        txtHispanic.Text = "";
        txtHawaiian.Text = "";
        txtWhite.Text = "";
        txtMultiRaces.Text = "";
        txtTotalSutdents.Text = "";
        txtExtraField1.Text = "";
        txtExtraField2.Text = "";
    }
    protected void OnApplicantPool(object sender, EventArgs e)
    {
        hfTypeID.Value = "1";
        lblTitle.Text = "Application Pool Data";
        lblItemLabel.Text = "New students not required to apply for admission";
        dynamicTR.Visible = false;
        MultiRacesRow.Visible = true;
        //Load up data
        var data = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value) && x.ReportType == 1);
        if (data.Count > 0)
        {
            hfApplicationPool.Value = data[0].ID.ToString();
            LoadMeasureData(data[0]);
        }
        else
        {
            ClearFields();
        }
        mpeWindow.Show();
    }
    protected void OnEnrollment(object sender, EventArgs e)
    {
        hfTypeID.Value = "2";
        lblTitle.Text = "Enrollment Data";
        lblItemLabel.Text = "New students who applied then enrolled";
        lblDynamicLabel.Text = "Continuing studetns who enrolled";
        dynamicTR.Visible = true;
        MultiRacesRow.Visible = true;
        //Load up data
        var data = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value) && x.ReportType == 2);
        if (data.Count > 0)
        {
            hfApplicationPool.Value = data[0].ID.ToString();
            LoadMeasureData(data[0]);
        }
        else
        {
            ClearFields();
        }
        mpeWindow.Show();
    }
    protected void OnParticipationReading(object sender, EventArgs e)
    {
        hfTypeID.Value = "3";
        lblTitle.Text = "Adequate yearly progress participation data - Reading";
        lblItemLabel.Text = "Economically disadvantaged students";
        lblDynamicLabel.Text = "Enlish learners";
        dynamicTR.Visible = true;
        MultiRacesRow.Visible = false;
        //Load up data
        var data = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value) && x.ReportType == 3);
        if (data.Count > 0)
        {
            hfApplicationPool.Value = data[0].ID.ToString();
            LoadMeasureData(data[0]);
        }
        else
        {
            ClearFields();
        }
        mpeWindow.Show();
    }
    protected void OnParticipationMath(object sender, EventArgs e)
    {
        hfTypeID.Value = "4";
        lblTitle.Text = "Adequate yearly progress participation data - Mathematics";
        lblItemLabel.Text = "Economically disadvantaged students";
        lblDynamicLabel.Text = "Enlish learners";
        dynamicTR.Visible = true;
        MultiRacesRow.Visible = false;
        //Load up data
        var data = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value) && x.ReportType == 4);
        if (data.Count > 0)
        {
            hfApplicationPool.Value = data[0].ID.ToString();
            LoadMeasureData(data[0]);
        }
        else
        {
            ClearFields();
        }
        mpeWindow.Show();

    }
    protected void OnArchievementReading(object sender, EventArgs e)
    {
        hfTypeID.Value = "5";
        lblTitle.Text = "Adequate yearly progress achievement data - Reading";
        lblItemLabel.Text = "Economically disadvantaged students";
        lblDynamicLabel.Text = "Enlish learners";
        dynamicTR.Visible = true;
        MultiRacesRow.Visible = false;
        //Load up data
        var data = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value) && x.ReportType == 5);
        if (data.Count > 0)
        {
            hfApplicationPool.Value = data[0].ID.ToString();
            LoadMeasureData(data[0]);
        }
        else
        {
            ClearFields();
        }
        mpeWindow.Show();

    }
    protected void OnArchievementMath(object sender, EventArgs e)
    {
        hfTypeID.Value = "6";
        lblTitle.Text = "Adequate yearly progress achievement data - Mathematics";
        lblItemLabel.Text = "Economically disadvantaged students";
        lblDynamicLabel.Text = "Enlish learners";
        dynamicTR.Visible = true;
        MultiRacesRow.Visible = false;
        //Load up data
        var data = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value) && x.ReportType == 6);
        if (data.Count > 0)
        {
            hfApplicationPool.Value = data[0].ID.ToString();
            LoadMeasureData(data[0]);
        }
        else
        {
            ClearFields();
        }
        mpeWindow.Show();

    }
    protected void OnData(object sender, EventArgs e)
    {
        hfTypeID.Value = "7";
        lblTitle.Text = "GPRA Performance Measure 6 Data";
        lblItemLabel.Text = "Total Annual Budget";
        dynamicTR.Visible = false;
        MultiRacesRow.Visible = true;
        //Load up data
        var data = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == Convert.ToInt32(hfID.Value) && x.ReportType == 7);
        if (data.Count > 0)
        {
            hfApplicationPool.Value = data[0].ID.ToString();
            LoadMeasureData(data[0]);
        }
        else
        {
            ClearFields();
        }
        mpeWindow.Show();
    }
}