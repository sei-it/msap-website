﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="adhocreview.aspx.cs" Inherits="admin_data_adhocreview" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit Ad Hoc Review
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h1>
            Add/Edit Ad Hoc Review</h1>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <table>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 1?
                </td>
                <td>
                    <asp:DropDownList ID="ddlGPRAMeasure1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtGPRAMeasure1" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 2?
                </td>
                <td>
                    <asp:DropDownList ID="ddlGPRAMeasure2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtGPRAMeasure2" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 3?
                </td>
                <td>
                    <asp:DropDownList ID="ddlGPRAMeasure3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtGPRAMeasure3" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 4?
                </td>
                <td>
                    <asp:DropDownList ID="ddlGPRAMeasure4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtGPRAMeasure4" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 5?
                </td>
                <td>
                    <asp:DropDownList ID="ddlGPRAMeasure5" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtGPRAMeasure5" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 6?
                </td>
                <td>
                    <asp:DropDownList ID="ddlGPRAMeasure6" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtGPRAMeasure6" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    General Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtGeneralNotes" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ad Hoc Report Status:
                </td>
                <td>
                    <asp:DropDownList ID="ddlReportStatus" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Date Ad Hoc Report Expected:
                </td>
                <td>
                    <asp:TextBox ID="txtDateExpected" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDateExpected">
                    </ajax:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Date Received:
                </td>
                <td>
                    <asp:TextBox ID="txtDateReceived" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDateReceived">
                    </ajax:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtDateNotes" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ad Hoc Status:
                </td>
                <td>
                    <asp:DropDownList ID="ddlAdHocStatus" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />&nbsp;&nbsp;
                    <asp:Button ID="Button3" runat="server" CssClass="msapBtn" Text="Return" CausesValidation="false"
                        OnClick="OnReturn" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
