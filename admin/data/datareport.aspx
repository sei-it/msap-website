﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="datareport.aspx.cs" Inherits="admin_data_datareport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Data Entry
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server" GroupingText="Budget Report" Style="margin-top: 20px;">
        <table class="buttonTbl">
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton15" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/datareportinfo.aspx"
                        Text="Report Status"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton16" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/schoolstatus.aspx"
                        Text="School Status"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/enrollment.aspx?datatype=1"
                        Text="School enrollment for LEAs that HAVE converted"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/enrollment.aspx?datatype=2"
                        Text="Applicant pool for LEAs that HAVE converted"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton5" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/feederenrollment.aspx"
                        Text="Feeder School enrollment for LEAs that HAVE converted"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton7" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/managecontext.aspx"
                        Text="Context Data"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton8" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/managesips.aspx"
                        Text="SIP /data"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton9" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/managergiobj.aspx"
                        Text="MGI Objective Data"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton10" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/managergidefinition.aspx"
                        Text="MGI Definition Data"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton11" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/manageayp.aspx"
                        Text="AYP Data"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton12" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/manageadhocreview.aspx"
                        Text="Ad Hoc Data"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton13" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/manageapplicationpool.aspx"
                        Text="Applicant Pool Data"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton14" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/manageenrollment.aspx"
                        Text="Enrollment Data Data"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkButton17" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/managergioutcome.aspx"
                        Text="MGI Outcome Data"></asp:LinkButton>
                </td>
                <td >
                    <asp:LinkButton ID="LinkButton18" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/manageenrollment.aspx"
                        Text="Enrollment Data Report" Visible="false"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
