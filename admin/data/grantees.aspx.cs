﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_grantees : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetGrantee.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    private void LoadData(int PageNumber)
    {
        GridView1.DataSource = MagnetGrantee.All();
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtGranteeName.Text = "";
        hfID.Value = "";
    }
    protected void OnAddGrantee(object sender, EventArgs e)
    {
        ClearFields();
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtGranteeName.Text = grantee.GranteeName;
        mpeResourceWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetGrantee grantee = new MagnetGrantee();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        grantee.GranteeName = txtGranteeName.Text;
        grantee.Save();
        LoadData(GridView1.PageIndex);
    }
}