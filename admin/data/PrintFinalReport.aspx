﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="PrintFinalReport.aspx.cs" Inherits="admin_PrintFinalReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script src="../../js/jquery-1.5.1.js" type="text/javascript"></script>
    <script src="../../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../../js/jquery.cookie.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#<%=Button2.ClientID %>").click(function () {
            blockUIForDownload();
        });
    });

    var fileDownloadCheckTimer;
    function blockUIForDownload() {
        var token = new Date().getTime(); //use the current timestamp as the token value
        $('#<%=hfTokenID.ClientID %>').val(token);
        $.blockUI({ message: '<h1><img src="../../img/busy.gif" /> Just a moment...</h1>' });
        fileDownloadCheckTimer = window.setInterval(function () {
            var cookieValue = $.cookie('fileDownloadToken');
            if (cookieValue == token)
                finishDownload();
        }, 1000);
    }

    function finishDownload() {
        window.clearInterval(fileDownloadCheckTimer);
        $.cookie('fileDownloadToken', null); //clears this cookie value
        $.unblockUI();
    }

    
</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <h1>My Grantee</h1>
    <h2 class="space_top">
        Print Final Report</h2>
    <p>
        Please select a Grantee and a report period.</p>
    <table style="width: 50%; margin-left: 20px;">
    <tr>
    <td>
    <asp:RadioButtonList ID="rblstPrintCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" 
                onselectedindexchanged="rblstPrintCohort_SelectedIndexChanged">
    <asp:ListItem Value="1"  >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2" >MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
    </td>
    </tr>
        <tr>
            <td>
                <asp:DropDownList ID="DropDownList2" runat="server"  AutoPostBack="true" 
                    onselectedindexchanged="OnGranteeChanged">
                    <asp:ListItem Value="" Text="Please select"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right; padding-top: 15px;">
                <asp:Button ID="Button2" runat="server" Text="Print Final Report" CssClass="surveyBtn2"
                    OnClick="OnPrint" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfTokenID" runat="server" />
</asp:Content>

