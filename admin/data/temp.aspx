﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="temp.aspx.cs" Inherits="admin_data_temp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
            <tr>
                <td>
                    Number of American Indian/Alaskan Native students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtAmericanIndianMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtAmericanIndianMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of American Indian/Alaskan Native students proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtAmericanIndianMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtAmericanIndianMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of American Indian/Alaskan Native students proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtAmericanIndianMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender10" TargetControlID="txtAmericanIndianMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did American Indian/Alaskan Native students make AYP in mathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlAmericanIndianMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Black or African American students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtBlackMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtBlackMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Black or African American students proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtBlackMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="txtBlackMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of Black or African American students proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtBlackMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender11" TargetControlID="txtBlackMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did Black or African American students make AYP in mathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlBlackMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Hispanic/Latino students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtHispanicMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="txtHispanicMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Hispanic/Latino students proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtHispanicMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txtHispanicMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of Hispanic/Latino students proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtHispanicMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender12" TargetControlID="txtHispanicMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did Hispanic/Latino students make AYP in mathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlHispanicMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of  Native Hawaiian or Other Pacific Islander students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtHawaiianMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="txtHawaiianMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of  Native Hawaiian or Other Pacific Islander students proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtHawaiianMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="txtHawaiianMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of  Native Hawaiian or Other Pacific Islandere students proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtHawaiianMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender13" TargetControlID="txtHawaiianMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did  Native Hawaiian or Other Pacific Islander students make AYP in mathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlHawaiianMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of White students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtWhiteMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="txtWhiteMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of White students proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtWhiteMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="txtWhiteMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of White students proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtWhiteMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender14" TargetControlID="txtWhiteMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did White students make AYP in mathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlWhiteMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of students of Two or more races  tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtMoreRacesMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="txtMoreRacesMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of students of Two or more races   proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtMoreRacesMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txtMoreRacesMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of students of Two or more races   proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtMoreRacesMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender15" TargetControlID="txtMoreRacesMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did students of Two or more races make AYP in rmathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlMoreRacesMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Economically Disadvantaged  students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtEconomicallyMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="txtEconomicallyMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Economically Disadvantaged  students    proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtEconomicallyMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="txtEconomicallyMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of Economically Disadvantaged  students proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtEconomicallyMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender16" TargetControlID="txtEconomicallyMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did Economically Disadvantaged  students  make AYP in mathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlEconomicallyMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of English Learner students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtEnglishLearnerMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtEnglishLearnerMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of English Learner students    proficient or above
                </td>
                <td>
                      <asp:TextBox ID="txtEnglishLearnerMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtEnglishLearnerMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage of English Learner students proficient or above
                </td>
                <td>
                    <asp:TextBox ID="txtEnglishLearnerMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender17" TargetControlID="txtEnglishLearnerMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did English Learner students  make AYP in mathematics?
                </td>
                <td>
                    <asp:DropDownList ID="ddlEnglishLearnerMathematics" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Total number of students tested in mathematics
                </td>
                <td>
                    <asp:TextBox ID="txtTotalMathematics" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="txtTotalMathematics"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                   Total number of students proficient or above State reading standards
                </td>
                <td>
                      <asp:TextBox ID="txtTotalMathematicsProficient" runat="server" MaxLength="4"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="txtTotalMathematicsProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                    </td>
            </tr>
            <tr>
                <td>
                    Percentage proficient or above State mathematics standards
                </td>
                <td>
                    <asp:TextBox ID="txtTotalMathematicsPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender18" TargetControlID="txtTotalMathematicsPercentage" Mask="999.99"
                    MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                    MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                    ErrorTooltipEnabled="True" />
                </td>
            </tr>
</asp:Content>

