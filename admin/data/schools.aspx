﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="schools.aspx.cs" Inherits="admin_data_schools" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Schools
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
//    function OvrdSubmit() {
//        var ftbSubmit = document.forms[0].onsubmit;
//        if (typeof (ftbSubmit) == 'function') {
//            document.forms[0].onsubmit = function () {
//                try { ftbSubmit(); }
//                catch (ex) { }
//            }
//        }

//        // We are ok
//        return true;
//    }

</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Manage Magnet Schools</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        
        <table>
        <tr>
        <td colspan="3">
        <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" onselectedindexchanged="rbtnlstCohort_SelectedIndexChanged" >
    <asp:ListItem Value="1"  >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2" >MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
        </td>
        </tr>
        <tr>
        <td><asp:Button ID="Newbutton" runat="server" Text="New School" CssClass="msapBtn" OnClick="OnAdd" />
</td>
       
        <td>
        <fieldset>
        <legend>Report Year</legend>
            <asp:RadioButtonList ID="rblActive" runat="server" RepeatDirection="Horizontal" 
                AutoPostBack="True">
            <asp:ListItem Value="1" Selected="True">Year 1</asp:ListItem>
            <asp:ListItem Value="2">Year 2</asp:ListItem>
            <asp:ListItem Value="3">Year 3</asp:ListItem>
            </asp:RadioButtonList>
        </fieldset>
         
        </td>
         <td><asp:DropDownList ID="ddlGrantees" runat="server" AutoPostBack="True" 
                onselectedindexchanged="ddlGrantees_SelectedIndexChanged" >
         </asp:DropDownList>
        </td>
        </tr>
        </table>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            >
            <Columns>
                <asp:BoundField DataField="SchoolName" SortExpression="" HtmlEncode="false" HeaderText="School Name"  />
                <asp:BoundField DataField="Website" SortExpression="" HeaderText="Website" />
                <asp:BoundField DataField="Notes" SortExpression="" HeaderText="Notes" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                          <asp:LinkButton ID="lbtnActive" runat="server" Text='<%# Eval("isActive").Equals(true)?"Active(year " + Eval("ReportYear") + ")":"Deactive(year " + Eval("ReportYear") + ")" %>' CommandArgument='<%# Eval("ID") %>'
                            Enabled='<%# Eval("isActive") %>'
                            OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to change this record's status?');"></asp:LinkButton>

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
              <EmptyDataTemplate>
                <asp:Label ID="lblNorecId" runat="server"><h3>Sorry! Requested Records Are Not Found Please select With other grantee from dropdown list.</h3></asp:Label>
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:HiddenField ID="hfID" runat="server" />
        
    <%-- Panel --%>
    <asp:Panel ID="PopupPanel" runat="server" >
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit School</div>
                    <table>
                        <tr>
                            <td>
                                School Name:
                            </td>
                            <td>
                                <asp:TextBox ID="txtSchoolName" runat="server" MaxLength="500" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Theme:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTheme" runat="server" MaxLength="1000" TextMode="MultiLine" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Website:
                            </td>
                            <td>
                                <asp:TextBox ID="txtWebsite" runat="server" MaxLength="250" Width="450"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                School Status:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStatus" runat="server">
                                    <asp:ListItem Value="1">Status 1</asp:ListItem>
                                    <asp:ListItem Value="2">Status 2</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Notes:
                            </td>
                            <td>
                                <cc1:Editor ID="txtNotes" runat="server" Width="470" Height="200" >
                                </cc1:Editor>
                            </td>
                        </tr>
                        <tr>
                    <td>
                        Active:
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblShow" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                            </td>
                            <td>
                                <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                            </td>
                        </tr>
                    </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeResourceWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
