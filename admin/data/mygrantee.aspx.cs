﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Synergy.Magnet;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using log4net;
using ExcelLibrary.SpreadSheet;
using System.Text;
using OfficeOpenXml;
using System.Collections;
using System.Web.UI.WebControls;

public partial class admin_report_mygrantee : System.Web.UI.Page
{
    int reportyear = 1, reportPeriodID=0;
    string excelName = "";
    int cohortType = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.Identity.Name == "user13")
            ltlMyGrantee.Text = "My Grantee";
        else
            ltlMyGrantee.Text = "My Current Grantee";

        if (Session["ReportYear"] != null)
            reportyear = Convert.ToInt32(Session["ReportYear"]);
        

        if (HttpContext.Current.User.Identity.Name == "jwang")
            btnExcel.Visible = true;
        else
            btnExcel.Visible = false;

        if (!Page.IsPostBack)
        {

            var reportPeriods = MagnetReportPeriod.Find(x => x.isReportPeriod == true);
            if (reportPeriods.Count == 0)
            {
                reportPeriods = MagnetReportPeriod.Find(x => x.isActive == true);
            }
            cohortType = Convert.ToInt32(reportPeriods.Last().CohortType);
            Session["reportyear"] = Convert.ToInt32(reportPeriods.Last().reportYear);
            rbtnlstCohort.SelectedValue = cohortType.ToString();
            rbtnlstDownloadCohort.SelectedValue = cohortType.ToString();
            rblstPrintCohort.SelectedValue = cohortType.ToString();
            Session["cohortType"] = cohortType;

            reportPeriodID= reportPeriods.Last().ID;
            Session["ReportPeriodID"] =reportPeriodID;
            DropDownList1.Items.Clear();
            DropDownList2.Items.Clear();
            DropDownList4.Items.Clear();
            DropDownList5.Items.Clear();
            DropDownList6.Items.Clear();
            DropDownList1.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
            DropDownList2.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
            DropDownList4.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
            DropDownList5.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
            DropDownList6.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));

            foreach (MagnetReportPeriod period in MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == cohortType).OrderByDescending(x => x.ID))
            {
                DropDownList2.Items.Add(new System.Web.UI.WebControls.ListItem(period.Des, period.ID.ToString()));
                DropDownList4.Items.Add(new System.Web.UI.WebControls.ListItem(period.Des, period.ID.ToString()));
                DropDownList5.Items.Add(new System.Web.UI.WebControls.ListItem(period.Des, period.ID.ToString()));
            }

            //report period close
            if (HttpContext.Current.User.IsInRole("Administrators") || Context.User.IsInRole("Data") || HttpContext.Current.User.Identity.Name == "user13")
            {
                NotifyDiv.Visible = true;
                pnlPrints.Visible = true;
            }
            if(Context.User.IsInRole("ED"))
                pnlPrints.Visible = true;

            var priod = MagnetReportPeriod.Find(x => x.isReportPeriod == true);
            //report period open
            if (priod.Count > 0 && (HttpContext.Current.User.IsInRole("GrantStaff") || HttpContext.Current.User.IsInRole("evaluatortwg") ||
               HttpContext.Current.User.IsInRole("ED") || HttpContext.Current.User.IsInRole("projectdirector") ||
               Context.User.IsInRole("Project Director TWG") || HttpContext.Current.User.IsInRole("evaluator")
                ))
                NotifyDiv.Visible = true;

            if (priod.Count > 0 && (HttpContext.Current.User.IsInRole("GrantStaff") || HttpContext.Current.User.IsInRole("evaluatortwg") ||
              HttpContext.Current.User.IsInRole("ED") || HttpContext.Current.User.IsInRole("projectdirector") ||
              Context.User.IsInRole("Project Director TWG") 
               ))
                pnlPrints.Visible = true;

            setDropdowlist(ddlGrantees, reportPeriodID);
            setDropdowlist(DropDownList3, reportPeriodID);
            //setAdminDropdownlist();
            //setFinalPrintDropdownlist();
        }

        if (Convert.ToInt32(Session["ReportPeriodID"]) % 2 == 0)
        {
            //Ad Hoc 
            ltlGranteeTitle.Text = "Grantee Ad Hoc";
        }
        else
        {
            //APR
            ltlGranteeTitle.Text = "Grantee APR";
        }
       
    }

    private void setCurrentReportPeriodID()
    {
        var reportPeriods = MagnetReportPeriod.Find(x => x.isReportPeriod == true);
        if (reportPeriods.Count == 0)
        {
            reportPeriods = MagnetReportPeriod.Find(x => x.isActive == true);
        }
        Session["ReportPeriodID"] = reportPeriods.Last().ID;
    }


    private void setDropdowlist(DropDownList GranteeDropdown, int reportPeriod)
    {

        GranteeDropdown.Items.Clear();
        string Username = HttpContext.Current.User.Identity.Name;
        var users = MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()));


        if (users.Count > 0)
        {
            hfID.Value = users[0].ID.ToString();

            foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == users[0].ID))
            {
                MagnetGrantee grantee = null;
                if (reportPeriod > 8)
                    grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == cohortType && x.ID != 69 && x.ID != 70);  //Lee county and CHampaign
                else
                    grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == cohortType && x.ID != 71);  //Wake County

                String GranteeName = "";
                if (grantee != null)
                {
                    var contacts = MagnetGranteeContact.Find(x => x.LinkID == district.GranteeID && x.ContactType == 1);
                    
                    if(contacts!=null  && (HttpContext.Current.User.IsInRole("Administrators") || Context.User.IsInRole("Data") || HttpContext.Current.User.IsInRole("ED")))
                    {
                        GranteeName = contacts.Count > 0 ? contacts[0].State + " " + grantee.GranteeName : grantee.GranteeName;
                        if (!HttpContext.Current.User.IsInRole("ED"))
                        {
                            ReportDiv.Visible = true;
                            NotifyDiv.Visible = true;
                            pnlPrints.Visible = true;
                        }

                        GranteeDiv.Visible = true;

                    }
                    else if (contacts != null && (HttpContext.Current.User.IsInRole("ProjectDirector") || HttpContext.Current.User.IsInRole("Evaluator")
                    || HttpContext.Current.User.IsInRole("Project Director TWG") || HttpContext.Current.User.IsInRole("EvaluatorTWG")
                    || HttpContext.Current.User.IsInRole("GrantStaff") || HttpContext.Current.User.IsInRole("ED")))
                    {
                        pnlPrints.Visible = true;
                        GranteeName = grantee.GranteeName;
                    }
                    else
                        GranteeName = grantee.GranteeName;

                    System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem();
                    item.Value = grantee.ID.ToString();
                    item.Text = GranteeName;
                    GranteeDropdown.Items.Add(item);
                   
                }

            }

            List<System.Web.UI.WebControls.ListItem> li = new List<System.Web.UI.WebControls.ListItem>();
            foreach (System.Web.UI.WebControls.ListItem list in GranteeDropdown.Items)
            {
                li.Add(list);
            }
            li.Sort((x, y) => string.Compare(x.Text, y.Text));
            GranteeDropdown.Items.Clear();
            GranteeDropdown.DataSource = li;
            GranteeDropdown.DataTextField = "Text";
            GranteeDropdown.DataValueField = "Value";
            GranteeDropdown.DataBind();
            GranteeDropdown.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Please select"));

            if (users[0].GranteeID != null && GranteeDropdown.ID == "ddlGrantees")
            {
                try
                {
                    ddlGrantees.SelectedValue = Convert.ToString(users[0].GranteeID);
                }
                catch
                {

                }
            }
            Session["GranteeID"] = users[0].GranteeID;
        }
    }

    protected void OnDownload(object sender, EventArgs e)
    {
        if (DropDownList4.SelectedIndex > 0)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Worksheets.Add("Grantees");
                ExcelWorksheet ws1 = p.Workbook.Worksheets[1];
                p.Workbook.Worksheets.Add("Reports");
                ExcelWorksheet ws2 = p.Workbook.Worksheets[2];
                p.Workbook.Worksheets.Add("Cover Sheet1");
                ExcelWorksheet ws3 = p.Workbook.Worksheets[3];
                p.Workbook.Worksheets.Add("Cover Sheet2");
                ExcelWorksheet ws4 = p.Workbook.Worksheets[4];
                p.Workbook.Worksheets.Add("Cover Sheet3");
                ExcelWorksheet ws5 = p.Workbook.Worksheets[5];
                p.Workbook.Worksheets.Add("Cover Sheet4");
                ExcelWorksheet ws6 = p.Workbook.Worksheets[6];
                p.Workbook.Worksheets.Add("Executive Summary");
                ExcelWorksheet ws7 = p.Workbook.Worksheets[7];
               

                Report.GetGrantees(ws1, Convert.ToInt32(DropDownList4.SelectedValue));
                Report.GetReports(ws2, Convert.ToInt32(DropDownList4.SelectedValue));
                Report.getCoverSheet1(ws3, Convert.ToInt32(DropDownList4.SelectedValue));
                Report.getCoverSheet2(ws4, Convert.ToInt32(DropDownList4.SelectedValue));
                Report.getCoverSheet3(ws5, Convert.ToInt32(DropDownList4.SelectedValue));
                Report.getCoverSheet4(ws6, Convert.ToInt32(DropDownList4.SelectedValue));
                Report.getExecutiveSummary(ws7, Convert.ToInt32(DropDownList4.SelectedValue));

                excelName = MagnetReportPeriod.SingleOrDefault(x => x.ID == Convert.ToInt32(DropDownList4.SelectedValue)).ExcelFileName;
                //Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendCookie(new HttpCookie("fileDownloadToken", hfTokenID.Value)); //downloadTokenValue will have been provided in the form submit via the hidden input field
                Response.AddHeader("Content-Disposition", "attachment;filename=" + excelName + "_Report_1.xlsx");
                MemoryStream ms = new MemoryStream();
                p.SaveAs(ms);
                Response.BinaryWrite(ms.ToArray());
                Response.End();
                ms.Dispose();
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "PastReportPDF", "<script>alert('You have to select a report year from a drop-down list.');</script>", false);
        }
    }
    protected void OnDownloadSec(object sender, EventArgs e)
    {
        if (DropDownList4.SelectedIndex > 0)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Worksheets.Add("Status Objective");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Name = "StatusObjective"; //Setting Sheet's name

                Report.GetStatusChart2(ws, Convert.ToInt32(DropDownList4.SelectedValue));

                excelName = MagnetReportPeriod.SingleOrDefault(x => x.ID == Convert.ToInt32(DropDownList4.SelectedValue)).ExcelFileName;

                //Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendCookie(new HttpCookie("fileDownloadToken", hfTokenID.Value)); //downloadTokenValue will have been provided in the form submit via the hidden input field
                Response.AddHeader("Content-Disposition", "attachment;filename=" + excelName + "_Report_2.xlsx");
                MemoryStream ms = new MemoryStream();
                p.SaveAs(ms);
                Response.BinaryWrite(ms.ToArray());
                Response.End();
                ms.Dispose();
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "PastReportPDF", "<script>alert('You have to select a report year from a drop-down list.');</script>", false);
        }
    }
    protected void OnDownloadThird(object sender, EventArgs e)
    {

        if (DropDownList4.SelectedIndex > 0)
        {
            reportyear = (int)MagnetReportPeriod.SingleOrDefault(x => x.ID == Convert.ToInt32(DropDownList4.SelectedValue)).reportYear;

            using (ExcelPackage p = new ExcelPackage())
            {
                if (Convert.ToInt32(DropDownList4.SelectedValue) > 0)  //gpra 6 started at apr 2015 (year 2) reportPeriodId = 9
                {
                    p.Workbook.Worksheets.Add("GPRA I");
                    ExcelWorksheet ws1 = p.Workbook.Worksheets[1];
                    p.Workbook.Worksheets.Add("GPRA II");
                    ExcelWorksheet ws2 = p.Workbook.Worksheets[2];
                    p.Workbook.Worksheets.Add("GPRA III");
                    ExcelWorksheet ws3 = p.Workbook.Worksheets[3];
                    p.Workbook.Worksheets.Add("GPRA IV");
                    ExcelWorksheet ws4 = p.Workbook.Worksheets[4];
                    p.Workbook.Worksheets.Add("GPRA V");
                    ExcelWorksheet ws5 = p.Workbook.Worksheets[5];

                    p.Workbook.Worksheets.Add("GPRA VI");
                    ExcelWorksheet ws6 = p.Workbook.Worksheets[6];

                    p.Workbook.Worksheets.Add("Budget 1 of 2");
                    ExcelWorksheet ws7 = p.Workbook.Worksheets[7];
                    p.Workbook.Worksheets.Add("Budget 2 of 2");
                    ExcelWorksheet ws8 = p.Workbook.Worksheets[8];
                    p.Workbook.Worksheets.Add("Desegregation");
                    ExcelWorksheet ws9 = p.Workbook.Worksheets[9];
                    p.Workbook.Worksheets.Add("Table1");
                    ExcelWorksheet ws10 = p.Workbook.Worksheets[10];
                    p.Workbook.Worksheets.Add("Table2");
                    ExcelWorksheet ws11 = p.Workbook.Worksheets[11];
                    p.Workbook.Worksheets.Add("Table3");
                    ExcelWorksheet ws12 = p.Workbook.Worksheets[12];
                    p.Workbook.Worksheets.Add("Table4");
                    ExcelWorksheet ws13 = p.Workbook.Worksheets[13];

                    p.Workbook.Worksheets.Add("Additional Uploads");
                    ExcelWorksheet ws14 = p.Workbook.Worksheets[14];

                    Report.GetGPRA1(ws1, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA2(ws2, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA3(ws3, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA4(ws4, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA5(ws5, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);


                    Report.GetGPRA6(ws6, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);

                    Report.GetBudgetSummary(ws7, Convert.ToInt32(DropDownList4.SelectedValue));
                    Report.GetBudget2(ws8, Convert.ToInt32(DropDownList4.SelectedValue));

                    Report.GetDesegregation(ws9, Convert.ToInt32(DropDownList4.SelectedValue));
                    Report.GetTable7(ws10, Convert.ToInt32(DropDownList4.SelectedValue));
                    Report.GetTable8(ws11, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetTable9(ws12, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetTable11(ws13, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetAdditionalUpdate(ws14, Convert.ToInt32(DropDownList4.SelectedValue));
                }
                else
                {
                    p.Workbook.Worksheets.Add("GPRA I");
                    ExcelWorksheet ws1 = p.Workbook.Worksheets[1];
                    p.Workbook.Worksheets.Add("GPRA II");
                    ExcelWorksheet ws2 = p.Workbook.Worksheets[2];
                    p.Workbook.Worksheets.Add("GPRA III");
                    ExcelWorksheet ws3 = p.Workbook.Worksheets[3];
                    p.Workbook.Worksheets.Add("GPRA IV");
                    ExcelWorksheet ws4 = p.Workbook.Worksheets[4];
                    p.Workbook.Worksheets.Add("GPRA V");
                    ExcelWorksheet ws5 = p.Workbook.Worksheets[5];


                    p.Workbook.Worksheets.Add("Budget 1 of 2");
                    ExcelWorksheet ws6 = p.Workbook.Worksheets[6];
                    p.Workbook.Worksheets.Add("Budget 2 of 2");
                    ExcelWorksheet ws7 = p.Workbook.Worksheets[7];
                    p.Workbook.Worksheets.Add("Desegregation");
                    ExcelWorksheet ws8 = p.Workbook.Worksheets[8];
                    p.Workbook.Worksheets.Add("Table1");
                    ExcelWorksheet ws9 = p.Workbook.Worksheets[9];
                    p.Workbook.Worksheets.Add("Table2");
                    ExcelWorksheet ws10 = p.Workbook.Worksheets[10];
                    p.Workbook.Worksheets.Add("Table3");
                    ExcelWorksheet ws11 = p.Workbook.Worksheets[11];
                    p.Workbook.Worksheets.Add("Table4");
                    ExcelWorksheet ws12 = p.Workbook.Worksheets[12];

                    p.Workbook.Worksheets.Add("Additional Uploads");
                    ExcelWorksheet ws13 = p.Workbook.Worksheets[13];

                    Report.GetGPRA1(ws1, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA2(ws2, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA3(ws3, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA4(ws4, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetGPRA5(ws5, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);

                    Report.GetBudgetSummary(ws6, Convert.ToInt32(DropDownList4.SelectedValue));
                    Report.GetBudget2(ws7, Convert.ToInt32(DropDownList4.SelectedValue));

                    Report.GetDesegregation(ws8, Convert.ToInt32(DropDownList4.SelectedValue));
                    Report.GetTable7(ws9, Convert.ToInt32(DropDownList4.SelectedValue));
                    Report.GetTable8(ws10, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetTable9(ws11, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetTable11(ws12, Convert.ToInt32(DropDownList4.SelectedValue), reportyear);
                    Report.GetAdditionalUpdate(ws13, Convert.ToInt32(DropDownList4.SelectedValue));
                }
                excelName = MagnetReportPeriod.SingleOrDefault(x => x.ID == Convert.ToInt32(DropDownList4.SelectedValue)).ExcelFileName;

                //Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendCookie(new HttpCookie("fileDownloadToken", hfTokenID.Value)); //downloadTokenValue will have been provided in the form submit via the hidden input field
                Response.AddHeader("Content-Disposition", "attachment;filename=" + excelName + "_Report_3.xlsx");
                MemoryStream ms = new MemoryStream();
                p.SaveAs(ms);
                Response.BinaryWrite(ms.ToArray());
                Response.End();
                ms.Dispose();
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "PastReportPDF", "<script>alert('You have to select a report year from a drop-down list.');</script>", false);
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        if (ddlGrantees.SelectedIndex > 0)
        {
            MagnetGranteeUser user = MagnetGranteeUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
            user.GranteeID = Convert.ToInt32(ddlGrantees.SelectedValue);
            user.Save();
            Session["GranteeID"] = Convert.ToInt32(ddlGrantees.SelectedValue);
        }
    }
    protected void OnJump(object sender, EventArgs e)
    {
        setCurrentReportPeriodID();
        if (ddlGrantees.SelectedIndex > 0)
            Response.Redirect("quarterreports.aspx", true);
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "PastReportPDF", "<script>alert('You have to select a grantee from a drop-down list.');</script>", false);
        }
    }
    protected void OnReview(object sender, EventArgs e)
    {
        if (DropDownList5.SelectedIndex > 0 && Convert.ToInt32(DropDownList6.SelectedIndex) > 0)
        {
            MagnetGranteeUser user = MagnetGranteeUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
            user.GranteeID = Convert.ToInt32(DropDownList6.SelectedValue);
            user.Save();

            Session["GranteeID"] = Convert.ToInt32(DropDownList6.SelectedValue);
            Session["ReportPeriodID"] = DropDownList5.SelectedValue;
            Response.Redirect("quarterreports.aspx", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "PastReportPDF", "<script>alert('You have to select a report year and a grantee from drop-down lists.');</script>", false);
        }
    }

    protected void OnAdminGranteeChanged(object sender, EventArgs e)
    {
        cohortType = Convert.ToInt32(rblstPrintCohort.SelectedValue);
        DropDownList6.Items.Clear();
        DropDownList6.Items.Add(new System.Web.UI.WebControls.ListItem("Please select"));

        if (DropDownList5.SelectedIndex > 0)
        {
            setDropdowlist(DropDownList6, Convert.ToInt32(DropDownList5.SelectedValue));
        }

    }


    protected void OnGranteeChanged(object sender, EventArgs e)
    {
        cohortType = Convert.ToInt32(rblstPrintCohort.SelectedValue);
        DropDownList1.Items.Clear();
        DropDownList1.Items.Add(new System.Web.UI.WebControls.ListItem("Please select"));

        if (DropDownList2.SelectedIndex > 0)
        {
            setDropdowlist(DropDownList1, Convert.ToInt32(DropDownList2.SelectedValue));
        }

    }
    protected void OnPrint(object sender, EventArgs e)
    {

        if (DropDownList2.SelectedIndex > 0 && DropDownList1.SelectedIndex > 0)
        {
            List<string> TempFiles = new List<string>();
            try
            {
                using (System.IO.MemoryStream output = new MemoryStream())
                {
                    int ReportID = GranteeReport.SingleOrDefault(x => x.GranteeID == Convert.ToInt32(DropDownList1.SelectedValue) && x.ReportPeriodID == Convert.ToInt32(DropDownList2.SelectedValue)).ID;  //Convert.ToInt32(DropDownList2.SelectedValue);

                    var directors = MagnetGranteeDatum.Find(x => x.GranteeReportID == ReportID);
                    GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == ReportID);
                    MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID);
                    MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID);

                    reportyear = (int)period.reportYear;
                    

                    Document doc = new Document(PageSize.A4, 40f, 20f, 170f, 30f);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(doc, output);

                    PDFFooter PageEventHandler = new PDFFooter();
                    pdfWriter.PageEvent = PageEventHandler;

                    // Define the page header
                    PageEventHandler.PRAward = grantee.PRAward;
                    PageEventHandler.FooterFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 10, iTextSharp.text.Font.BOLD);

                    doc.Open();
                    PdfContentByte pdfContentByte = pdfWriter.DirectContent;
                    PdfOutline root = pdfContentByte.RootOutline;

                    float width = 0;
                    float height = 0;

                    //Create font
                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font Time12Bold = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.BOLD);
                    iTextSharp.text.Font Time10BoldItalic = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLDITALIC);
                    iTextSharp.text.Font Time10Bold = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLD);
                    iTextSharp.text.Font Time10Normal = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font Time8Normal = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font Time6Normal = new iTextSharp.text.Font(bfTimes, 6, iTextSharp.text.Font.NORMAL);

                    //Cover Sheet
                    //Cover sheet files
                    string TmpPDF = "";
                    int totalPages = 0;

                    //Report cover
                    {
                        PdfDestination coverDest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(1, coverDest, pdfWriter);
                        PdfOutline coverOutline = new PdfOutline(root, coverDest, "Report cover");

                        PdfStamper coverps = null;

                        // read existing PDF document
                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TempFiles.Add(TmpPDF);

                        PdfReader coverr = new PdfReader(Server.MapPath("../doc/CoverPage.pdf"));
                        coverps = new PdfStamper(coverr, new FileStream(TmpPDF, FileMode.Create));
                        AcroFields coveraf = coverps.AcroFields;

                        coveraf.SetField("Report_Type1", "MSAP Annual Performance Report");
                        coveraf.SetField("PR_Award_num", grantee.PRAward);
                        coveraf.SetField("Budget_Period", reportyear.ToString());
                        coveraf.SetField("Report_type2", report.ReportPeriodID % 2 == 1 ? "Annual Performance" : "Ad Hoc Report");

                        coverps.FormFlattening = true;

                        coverr.Close();
                        coverps.Close();

                        //Add to output
                        PdfReader coverreader2 = new PdfReader(TmpPDF);
                        for (int j = 1; j <= coverreader2.NumberOfPages; j++)
                        {
                            totalPages++;
                            doc.NewPage();
                            PdfImportedPage coverfimportedPage = pdfWriter.GetImportedPage(coverreader2, j);
                            pdfContentByte.AddTemplate(coverfimportedPage, 0, 0);
                        }
                        coverreader2.Close();
                    }

                    //Add uploaded cover sheet
                    var coversheets = MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 0);
                    if (coversheets.Count > 0)
                    {
                        PdfDestination coversheetDest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, coversheetDest, pdfWriter);
                        PdfOutline coversheetOutline = new PdfOutline(root, coversheetDest, "Cover sheet");

                        foreach (MagnetUpload upload in coversheets)
                        {
                            PdfReader csuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + upload.PhysicalName);
                            for (int t = 1; t <= csuploadReader.NumberOfPages; t++)
                            {
                                width = csuploadReader.GetPageSize(t).Width;
                                height = csuploadReader.GetPageSize(t).Height;
                                if (width > height)
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                else
                                    doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage csuploadPage = pdfWriter.GetImportedPage(csuploadReader, t);
                                pdfContentByte.AddTemplate(csuploadPage, 0, 0);
                            }
                            csuploadReader.Close();
                        }
                    }
                    //change address
                    if (directors.Count > 0 && directors[0].AddressChanged == true)
                    {
                        string pdfTemplatecvr = Server.MapPath("../doc/addressinfo.pdf");
                        string AddressChangecvrPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(AddressChangecvrPDF))
                            File.Delete(AddressChangecvrPDF);
                        TempFiles.Add(AddressChangecvrPDF);

                        PdfReader pdfReadercvr = new PdfReader(pdfTemplatecvr);
                        PdfStamper pdfStampercvr = new PdfStamper(pdfReadercvr, new FileStream(
                            AddressChangecvrPDF, FileMode.Create));

                        AcroFields pdfFormFieldscvr = pdfStampercvr.AcroFields;

                        pdfFormFieldscvr.SetField("address", directors[0].Address);
                        pdfFormFieldscvr.SetField("city", directors[0].City);
                        pdfFormFieldscvr.SetField("state", directors[0].State);
                        pdfFormFieldscvr.SetField("zipcode", directors[0].Zipcode);


                        // flatten the form to remove editting options, set it to false
                        // to leave the form open to subsequent manual edits
                        pdfStampercvr.FormFlattening = true;

                        // close the pdf
                        pdfReadercvr.Close();
                        pdfStampercvr.Close();

                        //Add to final report

                        PdfReader addrsreadercvr = new PdfReader(AddressChangecvrPDF);

                        doc.SetPageSize(PageSize.A4);
                        doc.NewPage();

                        PdfImportedPage bsimportedPagecvr = pdfWriter.GetImportedPage(addrsreadercvr, 1);
                        pdfContentByte.AddTemplate(bsimportedPagecvr, 0, 0);
                        addrsreadercvr.Close();

                    }


                    //Copy & Past Executive Summary
                    var sheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == ReportID);
                    if (sheets.Count > 0 && !string.IsNullOrEmpty(sheets[0].ExecutiveSummary))
                    {
                        doc.SetMargins(10f, 10f, 20f, 20f);
                        doc.NewPage();
                        doc.Add(new Paragraph(new Chunk("Executive Summary", Time12Bold)));
                        doc.Add(new Paragraph(new Chunk(sheets[0].ExecutiveSummary)));
                    }

                    //Uploaded Executive Summary
                    foreach (MagnetUpload upload in MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == -1))
                    {
                        PdfReader csuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + upload.PhysicalName);
                        for (int t = 1; t <= csuploadReader.NumberOfPages; t++)
                        {
                            width = csuploadReader.GetPageSize(t).Width;
                            height = csuploadReader.GetPageSize(t).Height;
                            if (width > height)
                                doc.SetPageSize(PageSize.A4.Rotate());
                            else
                                doc.SetPageSize(PageSize.A4);
                            doc.NewPage();
                            PdfImportedPage csuploadPage = pdfWriter.GetImportedPage(csuploadReader, t);
                            pdfContentByte.AddTemplate(csuploadPage, 0, 0);
                        }
                        csuploadReader.Close();
                    }



                    var Data = MagnetProjectObjective.Find(x => x.ReportID == ReportID).OrderBy(x => x.ID).ToList();
                    if (Data.Count > 0)
                    {

                        doc.SetPageSize(PageSize.A4);
                        //Project Status Chart
                        PdfDestination statuschartDest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, statuschartDest, pdfWriter);
                        PdfOutline statuschartOutline = new PdfOutline(root, statuschartDest, "Status chart");

                        //Print
                        int ID = 1;
                        StringBuilder sb = new StringBuilder();
                        foreach (MagnetProjectObjective ObjData in Data)
                        {
                            sb.Clear();
                            var PerformanceData = MagnetGrantPerformance.Find(x => x.ProjectObjectiveID == ObjData.ID && x.IsActive == true).OrderBy(x => x.ID).ToList();
                            if (PerformanceData.Count > 0)
                            {
                                PdfStamper pscps = null;
                                PdfReader pscr = null;
                                AcroFields pscaf = null;
                                int current = 0;
                                for (int idx = 1; idx <= PerformanceData.Count; idx++)
                                {
                                    MagnetGrantPerformance Performance = PerformanceData[idx - 1];

                                    //if (idx != current && (idx % 2 == 1))
                                    //{
                                    //    current = idx;
                                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                        if (File.Exists(TmpPDF))
                                            File.Delete(TmpPDF);
                                        TempFiles.Add(TmpPDF);

                                        pscr = new PdfReader(Server.MapPath("../doc/statuschart.pdf"));
                                        pscps = new PdfStamper(pscr, new FileStream(TmpPDF, FileMode.Create));
                                        pscaf = pscps.AcroFields;
                                    //}

                                    if (!string.IsNullOrEmpty(Performance.ExplanationProgress))
                                    {
                                        if (idx > 1)
                                            sb.AppendLine("");
                                        sb.AppendLine(ID.ToString() + "." + (idx < 27 ? Convert.ToString((char)(96 + idx)) : Convert.ToString(((char)(96 + idx - 26)) + Convert.ToString((char)(96 + idx - 26)))));
                                        sb.Append(Performance.ExplanationProgress);
                                    }

                                    pscaf.SetField("PRAward", grantee.PRAward);
                                    pscaf.SetField("ID", ID.ToString());
                                    pscaf.SetField("MeasureID", ID.ToString() + "." + (idx < 27 ? Convert.ToString((char)(96 + idx)) : Convert.ToString(((char)(96 + idx - 26)) + Convert.ToString((char)(96 + idx - 26)))));
                                    if (ObjData.StatusUpdate != null && (bool)ObjData.StatusUpdate == true) pscaf.SetField("UpdateStatus", "Yes");
                                    pscaf.SetField("ProjectObjective", (bool)ObjData.IsActive ? ObjData.ProjectObjective : "DEACTIVATED " + ObjData.ProjectObjective);

                                    //foreach (MagnetGrantPerformance Performance in PerformanceData)
                                    {
                                        pscaf.SetField("Performance MeasureRow", (bool)Performance.IsActive ? Performance.PerformanceMeasure : "DEACTIVATED " + Performance.PerformanceMeasure);
                                        pscaf.SetField("Measure Type", Performance.MeasureType);
                                        pscaf.SetField("Target Raw Number", Performance.TargetNumber == null ? "" : Convert.ToString(Performance.TargetNumber));
                                        if (!string.IsNullOrEmpty(Performance.TargetRatio1))
                                        {
                                            pscaf.SetField("Target Ratio Top", Performance.TargetRatio1);
                                            pscaf.SetField("Target Ratio Bottom", Performance.TargetRatio2);
                                        }
                                        pscaf.SetField("Target Percentage", Performance.TargetPercentage == null ? "" : Convert.ToString(Performance.TargetPercentage));
                                        pscaf.SetField("APD Raw Number", Performance.ActualNumber == null ? "" : Convert.ToString(Performance.ActualNumber));
                                        if (!string.IsNullOrEmpty(Performance.AcutalRatio1))
                                        {
                                            pscaf.SetField("APD Ratio Top", Performance.AcutalRatio1);
                                            pscaf.SetField("APD Ratio Bottom", Performance.AcutalRatio2);
                                        }
                                        pscaf.SetField("APD Percent", Performance.ActualPercentage == null ? "" : Convert.ToString(Performance.ActualPercentage));                                        //idx++;
                                    }

                                    //Close document
                                    //if (idx % 2 == 0 || idx == PerformanceData.Count)
                                    //{
                                        pscps.FormFlattening = true;

                                        pscps.Close();
                                        pscr.Close();

                                        //Add to final report
                                        PdfReader reader = new PdfReader(TmpPDF);
                                        doc.SetPageSize(PageSize.A4.Rotate());
                                        doc.NewPage();
                                        PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                                        pdfContentByte.AddTemplate(importedPage, 27, -15);
                                        reader.Close();
                                    //}
                                }

                                if (sb.Length > 0)
                                {
                                    //Add page for explanation of progress
                                    string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                    string FinalPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(FinalPDF))
                                        File.Delete(FinalPDF);
                                    TempFiles.Add(FinalPDF);
                                    string Stationery = Server.MapPath("../doc/sc") + MagnetGrantee.SingleOrDefault(x => x.ID == report.GranteeID).PRAward + ".pdf";
                                    if (File.Exists(ContentPDF))
                                        File.Delete(ContentPDF);
                                    TempFiles.Add(ContentPDF);

                                    Document ContentDocument = new Document(PageSize.A4.Rotate(), 40f, 80f, 120f, 60f);
                                    PdfWriter ContentWriter = PdfWriter.GetInstance(ContentDocument, new FileStream(ContentPDF, FileMode.Create));
                                    ContentDocument.Open();
                                    //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                                    ContentDocument.Add(new Paragraph(new Chunk(sb.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                                    ContentDocument.Close();

                                    //Add stamp
                                    PdfReader o_reader = new PdfReader(ContentPDF);
                                    PdfReader s_reader = new PdfReader(Stationery);
                                    PdfStamper stamper = new PdfStamper(o_reader, new FileStream(FinalPDF, FileMode.Create));

                                    PdfImportedPage inpage = stamper.GetImportedPage(s_reader, 1);
                                    int n = o_reader.NumberOfPages;
                                    PdfContentByte background;
                                    for (int i = 1; i <= n; i++)
                                    {

                                        background = stamper.GetUnderContent(i);
                                        background.AddTemplate(inpage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                    }
                                    stamper.Close();

                                    //Add to output
                                    PdfReader freader = new PdfReader(FinalPDF);
                                    for (int j = 1; j <= freader.NumberOfPages; j++)
                                    {
                                        doc.SetPageSize(PageSize.A4.Rotate());
                                        doc.NewPage();
                                        PdfImportedPage fimportedPage = pdfWriter.GetImportedPage(freader, j);
                                        pdfContentByte.AddTemplate(fimportedPage, 0, -1.0F, 1.0F, 0, 30, PageSize.A4.Width);
                                    }
                                    freader.Close();
                                }
                            }
                            else
                            {
                                //No active measure, only print out objecitve
                                TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                if (File.Exists(TmpPDF))
                                    File.Delete(TmpPDF);
                                TempFiles.Add(TmpPDF);

                                PdfStamper pscps = null;

                                //
                                PdfReader pscr = null;
                                pscr = new PdfReader(Server.MapPath("../doc/statuchartp1.pdf"));

                                pscps = new PdfStamper(pscr, new FileStream(TmpPDF, FileMode.Create));

                                AcroFields pscaf = pscps.AcroFields;


                                pscaf.SetField("PRAward", grantee.PRAward);
                                pscaf.SetField("ID", ID.ToString());
                                if (ObjData.StatusUpdate != null && (bool)ObjData.StatusUpdate == true) pscaf.SetField("UpdateStatus", "Yes");
                                pscaf.SetField("ProjectObjective", ObjData.ProjectObjective);

                                pscps.FormFlattening = true;

                                pscps.Close();
                                pscr.Close();


                                //Add to final report
                                PdfReader pscreader = new PdfReader(TmpPDF);
                                doc.SetPageSize(PageSize.A4.Rotate());
                                doc.NewPage();
                                PdfImportedPage pscimportedPage = pdfWriter.GetImportedPage(pscreader, 1);
                                pdfContentByte.AddTemplate(pscimportedPage, 27, -15);
                                pscreader.Close();
                            }
                            ID++;
                        }
                    }

                    //GPRA 
                    MagnetDBDB db = new MagnetDBDB();

                    //int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(report.ID)).GranteeID;
                    int granteeID = Convert.ToInt32(DropDownList1.SelectedValue.Trim());
                    var schooldata = from n in db.MagnetSchools
                                     where n.GranteeID == granteeID
                                     && n.ReportYear == reportyear
                                     && n.isActive
                                     orderby n.SchoolName
                                     select n;
                    if (schooldata.Count() > 0)
                    {

                        foreach (MagnetSchool itm in schooldata)
                        {
                            var data = from rows in db.MagnetGPRAs
                                       join school in db.MagnetSchools on
                                       rows.SchoolID equals school.ID
                                       where rows.ReportID == Convert.ToInt32(report.ID)
                                       && school.ReportYear == reportyear
                                       && school.isActive
                                       && rows.SchoolID == itm.ID
                                       orderby school.SchoolName
                                       select rows;
                           int item18_Indian = 0, item19_Asian = 0,
                               item20_Black = 0, item21_Hispanic = 0,
                               item22_Hawaiian = 0, item23_White = 0,

                               item26a_newIndian = 0, item26b_newAsian = 0,
                               item26c_newBlack = 0, item26d_newHispanic = 0,
                               item26e_newHawaiian = 0, item26f_newWhite = 0,

                               item27a_ContIndian = 0, item27b_ContAsian = 0,
                               item27c_ContBlack = 0, item27d_ContHispanic = 0,
                               item27e_ContHawaiian = 0, item27f_ContWhite = 0,
                               item28_total = 0;

                            if (data.Count() > 0)
                            {
                                Hashtable htblImpStatus = new Hashtable();
                                htblImpStatus.Add("8", "7_1");
                                htblImpStatus.Add("4", "7_2");
                                htblImpStatus.Add("23", "7_3");
                                htblImpStatus.Add("2", "7_4");
                                htblImpStatus.Add("5", "7_5");
                                htblImpStatus.Add("6", "7_6");
                                htblImpStatus.Add("7", "7_7");
                                htblImpStatus.Add("1", "7_8");
                                htblImpStatus.Add("9", "7_9");
                                htblImpStatus.Add("10", "7_10");
                                htblImpStatus.Add("11", "7_11");
                                htblImpStatus.Add("12", "7_12");
                                htblImpStatus.Add("13", "7_13");
                                htblImpStatus.Add("14", "7_14");
                                htblImpStatus.Add("15", "7_15");
                                htblImpStatus.Add("16", "7_16");
                                htblImpStatus.Add("17", "7_17");
                                htblImpStatus.Add("18", "7_18");
                                htblImpStatus.Add("19", "7_19");
                                htblImpStatus.Add("20", "7_20");
                                htblImpStatus.Add("21", "7_21");
                                htblImpStatus.Add("22", "7_22");
                                htblImpStatus.Add("3", "7_23");
                                htblImpStatus.Add("24", "7_24");
                                htblImpStatus.Add("25", "7_25");
                                htblImpStatus.Add("26", "7_26");
                                htblImpStatus.Add("27", "7_27");
                                htblImpStatus.Add("28", "7_28");
                                htblImpStatus.Add("29", "7_29");
                                htblImpStatus.Add("30", "7_30");
                                htblImpStatus.Add("31", "7_31");



                                foreach (MagnetGPRA GPRAData in data)
                                {
                                    try
                                    {
                                        bool isHighSchool = false;
                                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                        if (File.Exists(TmpPDF))
                                            File.Delete(TmpPDF);
                                        TempFiles.Add(TmpPDF);

                                        PdfStamper ps = null;

                                         // Fill out form
                                    PdfReader r = null;
                                    if (report.ReportPeriodID > 5)
                                    {
                                        if(report.ReportPeriodID > 8)
                                            r = new PdfReader(Server.MapPath("../doc/newGPRA.pdf"));
                                        else
                                            r = new PdfReader(Server.MapPath("../doc/gpranew.pdf"));
                                    }
                                    else
                                    {
                                       r = new PdfReader(Server.MapPath("../doc/gpra.pdf"));
                                    }

                                        ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                                        AcroFields af = ps.AcroFields;

                                        af.SetField("1 School name", MagnetSchool.SingleOrDefault(x => x.ID == GPRAData.SchoolID).SchoolName);
                                        af.SetField("2 Grantee name", MagnetGrantee.SingleOrDefault(x => x.ID == granteeID).GranteeName);
                                        if (!string.IsNullOrEmpty(GPRAData.SchoolGrade))
                                        {
                                            foreach (string str in GPRAData.SchoolGrade.Split(';'))
                                            {
                                                af.SetField("Grades" + str, "Yes");
                                                if (Convert.ToInt32(str) >= 11) //high school: 9 - 12 offset 2
                                                    isHighSchool = true;
                                            }
                                        }
                                        if (GPRAData.ProgramType != null)
                                        {
                                            if ((bool)GPRAData.ProgramType == false)
                                                af.SetField("4 whole school magnet", "Yes");
                                            else af.SetField("4 partial school magnet", "Yes");
                                        }
                                        if (GPRAData.TitleISchoolFunding != null)
                                        {
                                            if ((bool)GPRAData.TitleISchoolFunding == true)
                                            {
                                                af.SetField("5 title 1 funded school", "Yes");
                                            }
                                            else af.SetField("5 non title 1 funded school", "Yes");
                                        }
                                        if (GPRAData.TitleISchoolFundingImprovement != null)
                                        {
                                            if ((bool)GPRAData.TitleISchoolFundingImprovement == true)
                                            {
                                                af.SetField("6 Title 1 school improvement Yes", "Yes");
                                            }
                                            else
                                                af.SetField("6 Title 1 school improvement No", "Yes");
                                        }

                                        if (GPRAData.TitleISchoolFundingImprovementStatus != null)
                                        {
                                            string skey = GPRAData.TitleISchoolFundingImprovementStatus.ToString();

                                            af.SetField(htblImpStatus[skey].ToString(), "Yes");


                                            if (GPRAData.StatusOtherDes != null)
                                                af.SetField("Other", GPRAData.StatusOtherDes);
                                        }

                                        if (GPRAData.PersistentlyLlowestAchievingSchool != null)
                                        {
                                            if ((bool)GPRAData.PersistentlyLlowestAchievingSchool == true)
                                            {
                                                af.SetField("8 Persistently low achieving school Yes", "Yes");
                                            }
                                            else
                                                af.SetField("8 Persistently low achieving school No", "Yes");
                                        }
                                        if (GPRAData.SchoolImprovementGrant != null)
                                        {
                                            if ((bool)GPRAData.SchoolImprovementGrant == true)
                                                af.SetField("9 SIG Funds Yes", "Yes");
                                            else
                                                af.SetField("9 SIG Funds No", "Yes");
                                        }
                                        if(GPRAData.FRPMPpercentage!=null && !string.IsNullOrEmpty(GPRAData.FRPMPpercentage.Trim()))
                                            af.SetField("9apercent", GPRAData.FRPMPpercentage);

                                        //Part II
                                        int ddd = Convert.ToInt32(period.ReportPeriod.Substring(7, 2));
                                        //af.SetField("Applicant_pool_date", "20" + period.ReportPeriod.Substring(7, 2) + " - " + Convert.ToString(ddd + 1));
                                        //af.SetField("Enrollment_date", "20" + period.ReportPeriod.Substring(7, 2) + " - " + Convert.ToString(ddd + 1));
                                        af.SetField("Applicant_pool_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));
                                        af.SetField("Enrollment_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));

                                        var ApplicationPools = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 1 && x.MagnetGPRAID == GPRAData.ID);
                                        if (ApplicationPools.Count > 0)
                                        {
                                            if (ApplicationPools[0].Total != null) af.SetField("10", ManageUtility.FormatInteger((int)ApplicationPools[0].Total));
                                            if (ApplicationPools[0].Indian != null && ApplicationPools[0].Indian >= 0) af.SetField("11", ManageUtility.FormatInteger((int)ApplicationPools[0].Indian));
                                            if (ApplicationPools[0].Asian != null && ApplicationPools[0].Asian >= 0) af.SetField("12", ManageUtility.FormatInteger((int)ApplicationPools[0].Asian));
                                            if (ApplicationPools[0].Black != null && ApplicationPools[0].Black >= 0) af.SetField("13", ManageUtility.FormatInteger((int)ApplicationPools[0].Black));
                                            if (ApplicationPools[0].Hispanic != null && ApplicationPools[0].Hispanic >= 0) af.SetField("14", ManageUtility.FormatInteger((int)ApplicationPools[0].Hispanic));
                                            if (ApplicationPools[0].Hawaiian != null && ApplicationPools[0].Hawaiian >= 0) af.SetField("15", ManageUtility.FormatInteger((int)ApplicationPools[0].Hawaiian));
                                            if (ApplicationPools[0].White != null || ApplicationPools[0].White >= 0) af.SetField("16", ManageUtility.FormatInteger((int)ApplicationPools[0].White));
                                            if (ApplicationPools[0].Undeclared != null && ApplicationPools[0].Undeclared >= 0) af.SetField("16a", ManageUtility.FormatInteger((int)ApplicationPools[0].Undeclared));
                                            if (ApplicationPools[0].MultiRaces != null && ApplicationPools[0].MultiRaces >= 0) af.SetField("17", ManageUtility.FormatInteger((int)ApplicationPools[0].MultiRaces));
                                        }

                                        var EnrollmentDatas = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 2 && x.MagnetGPRAID == GPRAData.ID);
                                        if (EnrollmentDatas.Count > 0)
                                        {
                                            int btotal = 0; bool isBtotalHasData = false;
                                            if (EnrollmentDatas[0].ExtraFiled1 != null)
                                            {
                                                isBtotalHasData = true;
                                                btotal += (int)EnrollmentDatas[0].ExtraFiled1;
                                                af.SetField("18", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled1));
                                            }
                                            if (EnrollmentDatas[0].Total != null)
                                            {
                                                isBtotalHasData = true;
                                                btotal += (int)EnrollmentDatas[0].Total;
                                                af.SetField("19", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Total));
                                            }
                                            if (EnrollmentDatas[0].Indian != null && EnrollmentDatas[0].Indian >= 0)
                                            {
                                                af.SetField("20", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Indian));
                                                item18_Indian = (int)EnrollmentDatas[0].Indian;
                                            }
                                            if (EnrollmentDatas[0].Asian != null && EnrollmentDatas[0].Asian >= 0)
                                            {
                                                af.SetField("21", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Asian));
                                                item19_Asian = (int)EnrollmentDatas[0].Asian;
                                            }
                                            if (EnrollmentDatas[0].Black != null && EnrollmentDatas[0].Black >= 0)
                                            {
                                                af.SetField("22", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Black));
                                                item20_Black = (int)EnrollmentDatas[0].Black;
                                            }
                                            if (EnrollmentDatas[0].Hispanic != null && EnrollmentDatas[0].Hispanic >= 0)
                                            {
                                                af.SetField("23", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hispanic));
                                                item21_Hispanic = (int)EnrollmentDatas[0].Hispanic;
                                            }
                                            if (EnrollmentDatas[0].Hawaiian != null && EnrollmentDatas[0].Hawaiian >= 0)
                                            {
                                                af.SetField("24", ManageUtility.FormatInteger((int)EnrollmentDatas[0].Hawaiian));
                                                item22_Hawaiian = (int)EnrollmentDatas[0].Hawaiian;
                                            }
                                            if (EnrollmentDatas[0].White != null && EnrollmentDatas[0].White >= 0)
                                            {
                                                af.SetField("25", ManageUtility.FormatInteger((int)EnrollmentDatas[0].White));
                                                item23_White = (int)EnrollmentDatas[0].White;
                                            }
                                            if (EnrollmentDatas[0].MultiRaces != null) af.SetField("26", ManageUtility.FormatInteger((int)EnrollmentDatas[0].MultiRaces));

                                            //26a - 26g
                                            if (EnrollmentDatas[0].NewIndian != null && EnrollmentDatas[0].NewIndian >= 0)
                                            {
                                                af.SetField("26a", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewIndian));
                                                item26a_newIndian = (int)EnrollmentDatas[0].NewIndian;
                                            }
                                            if (EnrollmentDatas[0].NewAsian != null && EnrollmentDatas[0].NewAsian >= 0)
                                            {
                                                af.SetField("26b", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewAsian));
                                                item26b_newAsian = (int)EnrollmentDatas[0].NewAsian;
                                            }
                                            if (EnrollmentDatas[0].NewBlack != null && EnrollmentDatas[0].NewBlack >= 0)
                                            {
                                                af.SetField("26c", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewBlack));
                                                item26c_newBlack = (int)EnrollmentDatas[0].NewBlack;
                                            }
                                            if (EnrollmentDatas[0].NewHispanic != null && EnrollmentDatas[0].NewHispanic >= 0)
                                            {
                                                af.SetField("26d", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewHispanic));
                                                item26d_newHispanic = (int)EnrollmentDatas[0].NewHispanic;
                                            }
                                            if (EnrollmentDatas[0].NewHawaiian != null && EnrollmentDatas[0].NewHawaiian >= 0)
                                            {
                                                af.SetField("26e", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewHawaiian));
                                                item26e_newHawaiian = (int)EnrollmentDatas[0].NewHawaiian;
                                            }
                                            if (EnrollmentDatas[0].NewWhite != null && EnrollmentDatas[0].NewWhite >= 0)
                                            {
                                                af.SetField("26f", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewWhite));
                                                item26f_newWhite = (int)EnrollmentDatas[0].NewWhite;
                                            }
                                            if (EnrollmentDatas[0].NewMultiRaces != null && EnrollmentDatas[0].NewMultiRaces >= 0) af.SetField("26g", ManageUtility.FormatInteger((int)EnrollmentDatas[0].NewMultiRaces));
                                            /////

                                            //27a - 27g
                                            if (EnrollmentDatas[0].ContIndian != null && EnrollmentDatas[0].ContIndian >= 0)
                                            {
                                                af.SetField("27a", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContIndian));
                                                item27a_ContIndian = (int)EnrollmentDatas[0].ContIndian;
                                            }
                                            if (EnrollmentDatas[0].ContAsian != null && EnrollmentDatas[0].ContAsian >= 0)
                                            {
                                                af.SetField("27b", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContAsian));
                                                item27b_ContAsian = (int)EnrollmentDatas[0].ContAsian;
                                            }
                                            if (EnrollmentDatas[0].ContBlack != null && EnrollmentDatas[0].ContBlack >= 0)
                                            {
                                                af.SetField("27c", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContBlack));
                                                item27c_ContBlack = (int)EnrollmentDatas[0].ContBlack;
                                            }
                                            if (EnrollmentDatas[0].ContHispanic != null && EnrollmentDatas[0].ContHispanic >= 0)
                                            {
                                                af.SetField("27d", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContHispanic));
                                                item27d_ContHispanic = (int)EnrollmentDatas[0].ContHispanic;
                                            }
                                            if (EnrollmentDatas[0].ContHawaiian != null && EnrollmentDatas[0].ContHawaiian >= 0)
                                            {
                                                af.SetField("27e", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContHawaiian));
                                                item27e_ContHawaiian = (int)EnrollmentDatas[0].ContHawaiian;
                                            }
                                            if (EnrollmentDatas[0].ContWhite != null && EnrollmentDatas[0].ContWhite >= 0)
                                            {
                                                af.SetField("27f", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContWhite));
                                                item27f_ContWhite = (int)EnrollmentDatas[0].ContWhite;
                                            }

                                            if (EnrollmentDatas[0].ContMultiRaces != null && EnrollmentDatas[0].ContMultiRaces >= 0) af.SetField("27g", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ContMultiRaces));

                                            //
                                            if (EnrollmentDatas[0].ExtraFiled2 != null && EnrollmentDatas[0].ExtraFiled2 >= 0)
                                            {
                                                isBtotalHasData = true;
                                                btotal += (int)EnrollmentDatas[0].ExtraFiled2;
                                                af.SetField("27", ManageUtility.FormatInteger((int)EnrollmentDatas[0].ExtraFiled2));
                                            }
                                            if (isBtotalHasData)
                                            {
                                                af.SetField("28", ManageUtility.FormatInteger(btotal));
                                                item28_total = (int)EnrollmentDatas[0].ExtraFiled3;
                                            }
                                        }

                                        //if (report.ReportPeriodID > 5)
                                        //{
                                        //    af.SetField("GPRAIII_Head1", "State Assessment Participation Data");
                                        //    af.SetField("GPRAIII_Head2", "State Assessment Achievement Data");
                                        //    af.SetField("GPRAIII_Col1", "Number of students that participated in the state assessment in reading/language arts and mathematics: ");
                                        //    af.SetField("GPRAIII_Col2", "Number of students who met or exceeded the state standards in reading/language arts and mathematics:");
                                        //}
                                        //else
                                        //{
                                        //    af.SetField("GPRAIII_Head1", "AYP Participation Data");
                                        //    af.SetField("GPRAIII_Head2", "AYP Achievement Data");
                                        //    af.SetField("GPRAIII_Col1", "Number of students that participated in the state adequate yearly progress assessment in reading/language arts and mathematics:");
                                        //    af.SetField("GPRAIII_Col2", "Number of students who meet or exceed the state’s adequate yearly progress standard in reading/language arts and mathematics:");
                                        //}

                                        af.SetField("AYP_Participation_date_read", period.ReportPeriod);
                                        af.SetField("AYP_Participation_date_math", period.ReportPeriod);
                                        af.SetField("AYP_achievement_date_read", period.ReportPeriod);
                                        af.SetField("AYP_achievement_date_math", period.ReportPeriod);

                                        var ParticipationReading = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 3 && x.MagnetGPRAID == GPRAData.ID);
                                        if (ParticipationReading.Count > 0)
                                        {
                                            if (ParticipationReading[0].Total != null) af.SetField("29 Reading", ParticipationReading[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Total));
                                            if (ParticipationReading[0].Indian != null) af.SetField("30 Reading", ParticipationReading[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Indian));
                                            if (ParticipationReading[0].Asian != null) af.SetField("31 Reading", ParticipationReading[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Asian));
                                            if (ParticipationReading[0].Black != null) af.SetField("32 Reading", ParticipationReading[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Black));
                                            if (ParticipationReading[0].Hispanic != null) af.SetField("33 Reading", ParticipationReading[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Hispanic));
                                            if (ParticipationReading[0].Hawaiian != null) af.SetField("34 Reading", ParticipationReading[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].Hawaiian));
                                            if (ParticipationReading[0].White != null) af.SetField("35 Reading", ParticipationReading[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].White));
                                            if (ParticipationReading[0].MultiRaces != null) af.SetField("47 Reading", ParticipationReading[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].MultiRaces));
                                            if (ParticipationReading[0].ExtraFiled2 != null) af.SetField("36 Reading", ParticipationReading[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].ExtraFiled2));
                                            if (ParticipationReading[0].ExtraFiled1 != null) af.SetField("37 Reading", ParticipationReading[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationReading[0].ExtraFiled1));
                                        }

                                        var ParticipationMath = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 4 && x.MagnetGPRAID == GPRAData.ID);
                                        if (ParticipationMath.Count > 0)
                                        {
                                            if (ParticipationMath[0].Total != null) af.SetField("29 Math", ParticipationMath[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Total));
                                            if (ParticipationMath[0].Indian != null) af.SetField("30 Math", ParticipationMath[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Indian));
                                            if (ParticipationMath[0].Asian != null) af.SetField("31 Math", ParticipationMath[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Asian));
                                            if (ParticipationMath[0].Black != null) af.SetField("32 Math", ParticipationMath[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Black));
                                            if (ParticipationMath[0].Hispanic != null) af.SetField("33 Math", ParticipationMath[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Hispanic));
                                            if (ParticipationMath[0].Hawaiian != null) af.SetField("34 Math", ParticipationMath[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].Hawaiian));
                                            if (ParticipationMath[0].White != null) af.SetField("35 Math", ParticipationMath[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].White));
                                            if (ParticipationMath[0].MultiRaces != null) af.SetField("47 Math", ParticipationMath[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].MultiRaces));
                                            if (ParticipationMath[0].ExtraFiled2 != null) af.SetField("36 Math", ParticipationMath[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].ExtraFiled2));
                                            if (ParticipationMath[0].ExtraFiled1 != null) af.SetField("37 Math", ParticipationMath[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ParticipationMath[0].ExtraFiled1));
                                        }

                                        var ArchievementReading = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 5 && x.MagnetGPRAID == GPRAData.ID);
                                        if (ArchievementReading.Count > 0)
                                        {
                                            if (ArchievementReading[0].Total != null) af.SetField("38 Reading", ArchievementReading[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Total));
                                            if (ArchievementReading[0].Indian != null) af.SetField("39 Reading", ArchievementReading[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Indian));
                                            if (ArchievementReading[0].Asian != null) af.SetField("40 Reading", ArchievementReading[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Asian));
                                            if (ArchievementReading[0].Black != null) af.SetField("41 Reading", ArchievementReading[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Black));
                                            if (ArchievementReading[0].Hispanic != null) af.SetField("42 Reading", ArchievementReading[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Hispanic));
                                            if (ArchievementReading[0].Hawaiian != null) af.SetField("43 Reading", ArchievementReading[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].Hawaiian));
                                            if (ArchievementReading[0].White != null) af.SetField("44 Reading", ArchievementReading[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].White));
                                            if (ArchievementReading[0].MultiRaces != null) af.SetField("48 Reading", ArchievementReading[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].MultiRaces));
                                            if (ArchievementReading[0].ExtraFiled2 != null) af.SetField("45 Reading", ArchievementReading[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].ExtraFiled2));
                                            if (ArchievementReading[0].ExtraFiled1 != null) af.SetField("46 Reading", ArchievementReading[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementReading[0].ExtraFiled1));
                                        }

                                        var ArchievementMath = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 6 && x.MagnetGPRAID == GPRAData.ID);
                                        if (ArchievementMath.Count > 0)
                                        {
                                            if (ArchievementMath[0].Total != null) af.SetField("38 Math", ArchievementMath[0].Total < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Total));
                                            if (ArchievementMath[0].Indian != null) af.SetField("39 Math", ArchievementMath[0].Indian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Indian));
                                            if (ArchievementMath[0].Asian != null) af.SetField("40 Math", ArchievementMath[0].Asian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Asian));
                                            if (ArchievementMath[0].Black != null) af.SetField("41 Math", ArchievementMath[0].Black < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Black));
                                            if (ArchievementMath[0].Hispanic != null) af.SetField("42 Math", ArchievementMath[0].Hispanic < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Hispanic));
                                            if (ArchievementMath[0].Hawaiian != null) af.SetField("43 Math", ArchievementMath[0].Hawaiian < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].Hawaiian));
                                            if (ArchievementMath[0].White != null) af.SetField("44 Math", ArchievementMath[0].White < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].White));
                                            if (ArchievementMath[0].MultiRaces != null) af.SetField("48 Math", ArchievementMath[0].MultiRaces < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].MultiRaces));
                                            if (ArchievementMath[0].ExtraFiled2 != null) af.SetField("45 Math", ArchievementMath[0].ExtraFiled2 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].ExtraFiled2));
                                            if (ArchievementMath[0].ExtraFiled1 != null) af.SetField("46 Math", ArchievementMath[0].ExtraFiled1 < 0 ? "" : ManageUtility.FormatInteger((int)ArchievementMath[0].ExtraFiled1));
                                        }

                                        af.SetField("cost_perstudent_date", period.ReportPeriod);

                                        var PerformanceMeasure = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 7 && x.MagnetGPRAID == GPRAData.ID);
                                        if (PerformanceMeasure.Count > 0)
                                        {
                                            if (PerformanceMeasure[0].Budget != null) af.SetField("49", "$" + string.Format("{0:#,0.00}", PerformanceMeasure[0].Budget));
                                            if (PerformanceMeasure[0].Total != null) af.SetField("50", ManageUtility.FormatInteger((int)PerformanceMeasure[0].Total));
                                        }

                                        ps.FormFlattening = true;

                                        r.Close();
                                        ps.Close();

                                        //Add to final report
                                        PdfReader reader = new PdfReader(TmpPDF);
                                        doc.SetPageSize(PageSize.A4);
                                        doc.NewPage();
                                        PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                                        pdfContentByte.AddTemplate(importedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                        doc.NewPage();
                                        importedPage = pdfWriter.GetImportedPage(reader, 2);
                                        pdfContentByte.AddTemplate(importedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                        doc.NewPage();
                                        importedPage = pdfWriter.GetImportedPage(reader, 3);
                                        pdfContentByte.AddTemplate(importedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                        reader.Close();

                                        if (isHighSchool)
                                        {
                                            //gpra v
                                            var gpraVdata = MagnetGPRAPerformanceMeasure.Find(x => x.ReportType == 8 && x.MagnetGPRAID == GPRAData.ID);
                                            if (gpraVdata.Count > 0)
                                            {
                                                string pdfTemplate = Server.MapPath("../doc/gprav.pdf");
                                                string gpraVpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                                if (File.Exists(gpraVpdf))
                                                    File.Delete(gpraVpdf);
                                                TempFiles.Add(gpraVpdf);

                                                PdfReader pdfReader = new PdfReader(pdfTemplate);
                                                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                                    gpraVpdf, FileMode.Create));

                                                AcroFields pdfFormFields = pdfStamper.AcroFields;
                                                pdfFormFields.SetField("titleyear", period.ReportPeriod);
                                                pdfFormFields.SetField("Allcohort", gpraVdata[0].Allcohort == null ? "" : gpraVdata[0].Allcohort.ToString());
                                                pdfFormFields.SetField("Allgraduated", gpraVdata[0].Allgraduated == null ? "" : gpraVdata[0].Allgraduated.ToString());
                                                pdfFormFields.SetField("allpercentage", gpraVdata[0].allpercentage == null ? "" : gpraVdata[0].allpercentage);

                                                pdfFormFields.SetField("AmericanIndiancohort", gpraVdata[0].AmericanIndiancohort == null ? "" : gpraVdata[0].AmericanIndiancohort.ToString());
                                                pdfFormFields.SetField("AmericanIndiangraduated", gpraVdata[0].AmericanIndiangraduated == null ? "" : gpraVdata[0].AmericanIndiangraduated.ToString());
                                                pdfFormFields.SetField("AmericanIndianpercentage", gpraVdata[0].AmericanIndianpercentage == null ? "" : gpraVdata[0].AmericanIndianpercentage);

                                                pdfFormFields.SetField("Asiancohort", gpraVdata[0].Asiancohort == null ? "" : gpraVdata[0].Asiancohort.ToString());
                                                pdfFormFields.SetField("Asiangraduated", gpraVdata[0].Asiangraduated == null ? "" : gpraVdata[0].Asiangraduated.ToString());
                                                pdfFormFields.SetField("Asianpercentage", gpraVdata[0].Asianpercentage == null ? "" : gpraVdata[0].Asianpercentage);

                                                pdfFormFields.SetField("AfricanAmericancohort", gpraVdata[0].AfricanAmericancohort == null ? "" : gpraVdata[0].AfricanAmericancohort.ToString());
                                                pdfFormFields.SetField("AfricanAmericangraduated", gpraVdata[0].AfricanAmericangraduated == null ? "" : gpraVdata[0].AfricanAmericangraduated.ToString());
                                                pdfFormFields.SetField("AfricanAmericanpercentage", gpraVdata[0].AfricanAmericanpercentage == null ? "" : gpraVdata[0].AfricanAmericanpercentage);

                                                pdfFormFields.SetField("HispanicLatinocohort", gpraVdata[0].HispanicLatinocohort == null ? "" : gpraVdata[0].HispanicLatinocohort.ToString());
                                                pdfFormFields.SetField("HispanicLatinograduated", gpraVdata[0].HispanicLatinograduated == null ? "" : gpraVdata[0].HispanicLatinograduated.ToString());
                                                pdfFormFields.SetField("HispanicLatinopercentage", gpraVdata[0].HispanicLatinopercentage == null ? "" : gpraVdata[0].HispanicLatinopercentage);

                                                pdfFormFields.SetField("NativeHawaiiancohort", gpraVdata[0].NativeHawaiiancohort == null ? "" : gpraVdata[0].NativeHawaiiancohort.ToString());
                                                pdfFormFields.SetField("NativeHawaiiangraduated", gpraVdata[0].NativeHawaiiangraduated == null ? "" : gpraVdata[0].NativeHawaiiangraduated.ToString());
                                                pdfFormFields.SetField("NativeHawaiianpercentage", gpraVdata[0].NativeHawaiianpercentage == null ? "" : gpraVdata[0].NativeHawaiianpercentage);

                                                pdfFormFields.SetField("Whitecohort", gpraVdata[0].Whitecohort == null ? "" : gpraVdata[0].Whitecohort.ToString());
                                                pdfFormFields.SetField("Whitegraduated", gpraVdata[0].Whitegraduated == null ? "" : gpraVdata[0].Whitegraduated.ToString());
                                                pdfFormFields.SetField("Whitepercentage", gpraVdata[0].Whitepercentage == null ? "" : gpraVdata[0].Whitepercentage);

                                                pdfFormFields.SetField("MoreRacescohort", gpraVdata[0].MoreRacescohort == null ? "" : gpraVdata[0].MoreRacescohort.ToString());
                                                pdfFormFields.SetField("MoreRacesgraduated", gpraVdata[0].MoreRacesgraduated == null ? "" : gpraVdata[0].MoreRacesgraduated.ToString());
                                                pdfFormFields.SetField("MoreRacespercentage", gpraVdata[0].MoreRacespercentage == null ? "" : gpraVdata[0].MoreRacespercentage);

                                                pdfFormFields.SetField("Economicallycohort", gpraVdata[0].Economicallycohort == null ? "" : gpraVdata[0].Economicallycohort.ToString());
                                                pdfFormFields.SetField("Economicallygraduated", gpraVdata[0].Economicallygraduated == null ? "" : gpraVdata[0].Economicallygraduated.ToString());
                                                pdfFormFields.SetField("Economicallypercentage", gpraVdata[0].Economicallypercentage == null ? "" : gpraVdata[0].Economicallypercentage);

                                                pdfFormFields.SetField("Englishlearnerscohort", gpraVdata[0].Englishlearnerscohort == null ? "" : gpraVdata[0].Englishlearnerscohort.ToString());
                                                pdfFormFields.SetField("Englishlearnersgraduated", gpraVdata[0].Englishlearnersgraduated == null ? "" : gpraVdata[0].Englishlearnersgraduated.ToString());
                                                pdfFormFields.SetField("Englishlearnerspercentage", gpraVdata[0].Englishlearnerspercentage == null ? "" : gpraVdata[0].Englishlearnerspercentage);



                                                // flatten the form to remove editting options, set it to false
                                                // to leave the form open to subsequent manual edits
                                                pdfStamper.FormFlattening = true;

                                                // close the pdf
                                                pdfReader.Close();
                                                pdfStamper.Close();

                                                //Add to final report

                                                PdfReader addrsreader = new PdfReader(gpraVpdf);

                                                doc.NewPage();

                                                PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                                //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                                pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                                addrsreader.Close();
                                            }
                                            else
                                            {
                                                //empty gpra v
                                                string pdfTemplate = Server.MapPath("../doc/gprav.pdf");
                                                string gpraVpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                                if (File.Exists(gpraVpdf))
                                                    File.Delete(gpraVpdf);
                                                TempFiles.Add(gpraVpdf);

                                                PdfReader pdfReader = new PdfReader(pdfTemplate);
                                                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                                    gpraVpdf, FileMode.Create));

                                                // close the pdf
                                                pdfReader.Close();
                                                pdfStamper.Close();

                                                //Add to final report

                                                PdfReader addrsreader = new PdfReader(gpraVpdf);

                                                doc.NewPage();

                                                PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                                //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                                pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                                addrsreader.Close();
                                            }
                                        }
                                        ///////////////GPRA 6  ///////////////////////////////
                                        MagnetGPRA6 gpra6Record = MagnetGPRA6.SingleOrDefault(x => x.MagnetGPRAID == GPRAData.ID);
                                        if (gpra6Record!=null)
                                        {
                                            string pdfTemplate = Server.MapPath("../doc/GPRAVI.pdf");
                                            string gpraVIpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                            if (File.Exists(gpraVIpdf))
                                                File.Delete(gpraVIpdf);
                                            TempFiles.Add(gpraVIpdf);

                                            PdfReader pdfReader = new PdfReader(pdfTemplate);
                                            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                                gpraVIpdf, FileMode.Create));

                                            AcroFields pdfFormFields = pdfStamper.AcroFields;
                                            //items 61
                                            foreach (string str in gpra6Record.MinorityIsolatedGroups.Split(','))
                                            {
                                                pdfFormFields.SetField("61_" + str, "Yes");
                                            }
                                            //items 62
                                            if(gpra6Record.IndianMSAP62)
                                                pdfFormFields.SetField("IndianMSAP62", "Yes");
                                            if(gpra6Record.IndianFeeder62)
                                                pdfFormFields.SetField("IndianFeeder62", "Yes");
                                            if (gpra6Record.AsianMSAP62)
                                                pdfFormFields.SetField("AsianMSAP62", "Yes");
                                            if (gpra6Record.AsianFeeder62)
                                                pdfFormFields.SetField("AsianFeeder62", "Yes");
                                            if (gpra6Record.BlackMSAP62)
                                                pdfFormFields.SetField("BlackMSAP62", "Yes");
                                            if (gpra6Record.BlackFeeder62)
                                                pdfFormFields.SetField("BlackFeeder62", "Yes");
                                            if (gpra6Record.HispanicMSAP62)
                                                pdfFormFields.SetField("HispanicMSAP62", "Yes");
                                            if (gpra6Record.HispanicFeeder62)
                                                pdfFormFields.SetField("HispanicFeeder62", "Yes");
                                            if (gpra6Record.NativeMSAP62)
                                                pdfFormFields.SetField("NativeMSAP62", "Yes");
                                            if (gpra6Record.NativeFeeder62)
                                                pdfFormFields.SetField("NativeFeeder62", "Yes");
                                            if (gpra6Record.WhiteMSAP62)
                                                pdfFormFields.SetField("WhiteMSAP62", "Yes");
                                            if (gpra6Record.WhiteFeeder62)
                                                pdfFormFields.SetField("WhiteFeeder62", "Yes");
                                            //items 63
                                            if (gpra6Record.IndianDec63)
                                                pdfFormFields.SetField("IndianDec63", "Yes");
                                            if (gpra6Record.IndianInc63)
                                                pdfFormFields.SetField("IndianInc63", "Yes");
                                            if (gpra6Record.IndianMtn63)
                                                pdfFormFields.SetField("IndianMtn63", "Yes");
                                            if (gpra6Record.AsianDec63)
                                                pdfFormFields.SetField("AsianDec63", "Yes");
                                            if (gpra6Record.AsianInc63)
                                                pdfFormFields.SetField("AsianInc63", "Yes");
                                            if (gpra6Record.AsianMtn63)
                                                pdfFormFields.SetField("AsianMtn63", "Yes");
                                            if (gpra6Record.BlackDec63)
                                                pdfFormFields.SetField("BlackDec63", "Yes");
                                            if (gpra6Record.BlackInc63)
                                                pdfFormFields.SetField("BlackInc63", "Yes");
                                            if (gpra6Record.BlackMtn63)
                                                pdfFormFields.SetField("BlackMtn63", "Yes");
                                            if (gpra6Record.HispanicDec63)
                                                pdfFormFields.SetField("HispanicDec63", "Yes");
                                            if (gpra6Record.HispanicInc63)
                                                pdfFormFields.SetField("HispanicInc63", "Yes");
                                            if (gpra6Record.HispanicMtn63)
                                                pdfFormFields.SetField("HispanicMtn63", "Yes");
                                            if (gpra6Record.NativeDec63)
                                                pdfFormFields.SetField("NativeDec63", "Yes");
                                            if (gpra6Record.NativeInc63)
                                                pdfFormFields.SetField("NativeInc63", "Yes");
                                            if (gpra6Record.NativeMtn63)
                                                pdfFormFields.SetField("NativeMtn63", "Yes");
                                            if (gpra6Record.WhiteDec63)
                                                pdfFormFields.SetField("WhiteDec63", "Yes");
                                            if (gpra6Record.WhiteInc63)
                                                pdfFormFields.SetField("WhiteInc63", "Yes");
                                            if (gpra6Record.WhiteMtn63)
                                                pdfFormFields.SetField("WhiteMtn63", "Yes");
                                            //items 64
                                            foreach (string str in gpra6Record.TargetRacialGroup64.Split(','))
                                            {
                                                pdfFormFields.SetField("64_" + str, "Yes");
                                            }
                                            //items 65
                                            string IndianActualPercentage65 = "", AsianActualPercentage65 = "",
                                                   BlackActualPercentage65 = "", HispanicActualPercentage65 = "",
                                                   NativeActualPercentage65 = "", WhiteActualPercentage65 = "";

                                            IndianActualPercentage65 = item28_total == 0 ? "" : ((double)(item18_Indian + item26a_newIndian + item27a_ContIndian) * 100 /
                                                                        (item28_total)).ToString("F");
                                            AsianActualPercentage65 = item28_total == 0 ? "" : ((double)(item19_Asian + item26b_newAsian + item27b_ContAsian) * 100 /
                                                                        (item28_total)).ToString("F");
                                            BlackActualPercentage65 = item28_total == 0 ? "" : ((double)(item20_Black + item26c_newBlack + item27c_ContBlack) * 100 / (item28_total)).ToString("F");
                                            HispanicActualPercentage65 = item28_total == 0 ? "" : ((double)(item21_Hispanic + item26d_newHispanic + item27d_ContHispanic) * 100 / (item28_total)).ToString("F");
                                            NativeActualPercentage65 = item28_total == 0 ? "" : ((double)(item22_Hawaiian + item26e_newHawaiian + item27e_ContHawaiian) * 100 / (item28_total)).ToString("F");
                                            WhiteActualPercentage65 = item28_total == 0 ? "" : ((double)(item23_White + item26f_newWhite + item27f_ContWhite) * 100 / (item28_total)).ToString("F");

                                            foreach (string str in gpra6Record.MinorityIsolatedGroups.Split(','))
                                            {
                                                switch (str)
                                                {
                                                    case "1":
                                                        pdfFormFields.SetField("IndianTargetPercentage65", gpra6Record.IndianTargetPercentage65);
                                                        pdfFormFields.SetField("ActIndianTargetPercentage65", IndianActualPercentage65);
                                                        break;
                                                    case "2":
                                                        pdfFormFields.SetField("AsianTargetPercentage65", gpra6Record.AsianTargetPercentage65);
                                                        pdfFormFields.SetField("ActAsianTargetPercentage65", AsianActualPercentage65);
                                                        break;

                                                    case "3":
                                                        pdfFormFields.SetField("BlackTargetPercentage65", gpra6Record.BlackTargetPercentage65);
                                                        pdfFormFields.SetField("ActBlackTargetPercentage65", BlackActualPercentage65);
                                                        break;
                                                    case "4":
                                                        pdfFormFields.SetField("HispanicTargetPercentage65", gpra6Record.HispanicTargetPercentage65);
                                                        pdfFormFields.SetField("ActHispanicTargetPercentage65", HispanicActualPercentage65);
                                                        break;
                                                    case "5":
                                                        pdfFormFields.SetField("NativeTargetPercentage65", gpra6Record.NativeTargetPercentage65);
                                                        pdfFormFields.SetField("ActNativeTargetPercentage65", NativeActualPercentage65);
                                                        break;
                                                    case "6":
                                                        pdfFormFields.SetField("WhiteTargetPercentage65", gpra6Record.WhiteTargetPercentage65);
                                                        pdfFormFields.SetField("ActWhiteTargetPercentage65", WhiteActualPercentage65);
                                                        break;
                                                }
                                            }

                                            //items 66
                                            if(gpra6Record.IndianMet66)
                                                pdfFormFields.SetField("IndianMet66", "Yes");
                                            if (gpra6Record.IndianNotMet66)
                                                pdfFormFields.SetField("IndianNotMet66", "Yes");
                                            if (gpra6Record.AsianMet66)
                                                pdfFormFields.SetField("AsianMet66", "Yes");
                                            if (gpra6Record.AsianNotMet66)
                                                pdfFormFields.SetField("AsianNotMet66", "Yes");
                                            if (gpra6Record.BlackMet66)
                                                pdfFormFields.SetField("BlackMet66", "Yes");
                                            if (gpra6Record.BlackNotMet66)
                                                pdfFormFields.SetField("BlackNotMet66", "Yes");
                                            if (gpra6Record.HispanicMet66)
                                                pdfFormFields.SetField("HispanicMet66", "Yes");
                                            if (gpra6Record.HispanicNotMet66)
                                                pdfFormFields.SetField("HispanicNotMet66", "Yes");
                                            if (gpra6Record.NativeMet66)
                                                pdfFormFields.SetField("NativeMet66", "Yes");
                                            if (gpra6Record.NativeNotMet66)
                                                pdfFormFields.SetField("NativeNotMet66", "Yes");
                                            if (gpra6Record.WhiteMet66)
                                                pdfFormFields.SetField("WhiteMet66", "Yes");
                                            if (gpra6Record.WhiteNotMet66)
                                                pdfFormFields.SetField("WhiteNotMet66", "Yes");

                                            //items 67
                                            if(gpra6Record.IndianMade67)
                                                pdfFormFields.SetField("IndianMade67", "Yes");
                                            if (gpra6Record.IndianNotMade67)
                                                pdfFormFields.SetField("IndianNotMade67", "Yes");
                                            if (gpra6Record.IndianNA67)
                                                pdfFormFields.SetField("IndianNA67", "Yes");
                                            if (gpra6Record.AsianMade67)
                                                pdfFormFields.SetField("AsianMade67", "Yes");
                                            if (gpra6Record.AsianNotMade67)
                                                pdfFormFields.SetField("AsianNotMade67", "Yes");
                                            if (gpra6Record.AsianNA67)
                                                pdfFormFields.SetField("AsianNA67", "Yes");
                                            if (gpra6Record.BlackMade67)
                                                pdfFormFields.SetField("BlackMade67", "Yes");
                                            if (gpra6Record.BlackNotMade67)
                                                pdfFormFields.SetField("BlackNotMade67", "Yes");
                                            if (gpra6Record.BlackNA67)
                                                pdfFormFields.SetField("BlackNA67", "Yes");
                                            if (gpra6Record.HispanicMade67)
                                                pdfFormFields.SetField("HispanicMade67", "Yes");
                                            if (gpra6Record.HispanicNotMade67)
                                                pdfFormFields.SetField("HispanicNotMade67", "Yes");
                                            if (gpra6Record.HispanicNA67)
                                                pdfFormFields.SetField("HispanicNA67", "Yes");
                                            if (gpra6Record.NativeMade67)
                                                pdfFormFields.SetField("NativeMade67", "Yes");
                                            if (gpra6Record.NativeNotMade67)
                                                pdfFormFields.SetField("NativeNotMade67", "Yes");
                                            if (gpra6Record.NativeNA67)
                                                pdfFormFields.SetField("NativeNA67", "Yes");
                                            if (gpra6Record.WhiteMade67)
                                                pdfFormFields.SetField("WhiteMade67", "Yes");
                                            if (gpra6Record.WhiteNotMade67)
                                                pdfFormFields.SetField("WhiteNotMade67", "Yes");
                                            if (gpra6Record.WhiteNA67)
                                                pdfFormFields.SetField("WhiteNA67", "Yes");

                                            // flatten the form to remove editting options, set it to false
                                            // to leave the form open to subsequent manual edits
                                            pdfStamper.FormFlattening = true;

                                            // close the pdf
                                            pdfReader.Close();
                                            pdfStamper.Close();

                                            //Add to final report

                                            PdfReader addrsreader = new PdfReader(gpraVIpdf);
                                            doc.SetPageSize(PageSize.A4);
                                            doc.NewPage();
                                            PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                            pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                            doc.NewPage();
                                            bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 2);
                                            pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                            addrsreader.Close();
                                        }
                                        else
                                        {
                                            //empty gpra vi
                                            string pdfTemplate = Server.MapPath("../doc/GPRAVI.pdf");
                                            string gpraVIpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                            if (File.Exists(gpraVIpdf))
                                                File.Delete(gpraVIpdf);
                                            TempFiles.Add(gpraVIpdf);

                                            PdfReader pdfReader = new PdfReader(pdfTemplate);
                                            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                                gpraVIpdf, FileMode.Create));

                                            // close the pdf
                                            pdfReader.Close();
                                            pdfStamper.Close();

                                            //Add to final report

                                            PdfReader addrsreader = new PdfReader(gpraVIpdf);

                                            doc.NewPage();

                                            PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                            //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                            pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                            doc.NewPage();
                                            bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 2);
                                            pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                            addrsreader.Close();
                                        }

                                        /////////////END GPRA 6 ////////////////////
                                    }
                                    catch (System.Exception ex)
                                    {
                                        ILog Log = LogManager.GetLogger("EventLog");
                                        Log.Error("GPRA report:", ex);
                                    }
                                    finally
                                    {
                                    }
                                }  //end MagnetGPRA datas

                            } //end if data
                            else  //empty school info
                            {

                                try
                                {
                                    TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(TmpPDF))
                                        File.Delete(TmpPDF);
                                    TempFiles.Add(TmpPDF);

                                    PdfStamper ps = null;

                                    // Fill out form
                                    PdfReader r = null;
                                    if (report.ReportPeriodID > 5)
                                    {
                                        if (report.ReportPeriodID > 8)
                                            r = new PdfReader(Server.MapPath("../doc/newGPRA.pdf"));
                                        else
                                            r = new PdfReader(Server.MapPath("../doc/gpranew.pdf"));
                                    }
                                    else
                                    {
                                        r = new PdfReader(Server.MapPath("../doc/gpra.pdf"));
                                    }

                                    ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                                    AcroFields af = ps.AcroFields;

                                    af.SetField("1 School name", itm.SchoolName);
                                    af.SetField("2 Grantee name", MagnetGrantee.SingleOrDefault(x => x.ID == granteeID).GranteeName);
                                    af.SetField("cost_perstudent_date", period.ReportPeriod);
                                    af.SetField("Applicant_pool_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));
                                    af.SetField("Enrollment_date", "Fall 20" + period.ReportPeriod.Substring(7, 2));

                                    af.SetField("AYP_Participation_date_read", period.ReportPeriod);
                                    af.SetField("AYP_Participation_date_math", period.ReportPeriod);
                                    af.SetField("AYP_achievement_date_read", period.ReportPeriod);
                                    af.SetField("AYP_achievement_date_math", period.ReportPeriod);
                                    af.SetField("cost_perstudent_date", period.ReportPeriod);

                                    ps.FormFlattening = true;

                                    r.Close();
                                    ps.Close();

                                    //Add to final report
                                    PdfReader reader = new PdfReader(TmpPDF);
                                    for (int t = 1; t <= reader.NumberOfPages; t++)
                                    {
                                        width = reader.GetPageSize(t).Width;
                                        height = reader.GetPageSize(t).Height;
                                        if (width > height)
                                            doc.SetPageSize(PageSize.A4.Rotate());
                                        else
                                            doc.SetPageSize(PageSize.A4);
                                        doc.NewPage();

                                        PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, t);
                                        //pdfContentByte.AddTemplate(importedPage, -3, 0);
                                        pdfContentByte.AddTemplate(importedPage, 0, 0);
                                    }

                                    //PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                                    //doc.NewPage();
                                    //importedPage = pdfWriter.GetImportedPage(reader, 2);
                                    //pdfContentByte.AddTemplate(importedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                    //doc.NewPage();
                                    //importedPage = pdfWriter.GetImportedPage(reader, 3);
                                    //pdfContentByte.AddTemplate(importedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                    reader.Close();

                                    //empty gpra v
                                    string pdfTemplate = Server.MapPath("../doc/gprav.pdf");
                                    string gpraVpdf = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(gpraVpdf))
                                        File.Delete(gpraVpdf);
                                    TempFiles.Add(gpraVpdf);

                                    PdfReader pdfReader = new PdfReader(pdfTemplate);
                                    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                                        gpraVpdf, FileMode.Create));

                                    AcroFields af5 = pdfStamper.AcroFields; 
                                    af5.SetField("titleyear", period.ReportPeriod);

                                    pdfStamper.FormFlattening = true;

                                    // close the pdf
                                    pdfReader.Close();
                                    pdfStamper.Close();

                                    //Add to final report

                                    PdfReader addrsreader = new PdfReader(gpraVpdf);

                                    doc.NewPage();

                                    PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(addrsreader, 1);
                                    //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                    pdfContentByte.AddTemplate(bsimportedPage, 1.0F, 0, 0, 1.0F, -3, 0);
                                    addrsreader.Close();

                                    //empty gpra 6
                                    string pdfTemplate6 = Server.MapPath("../doc/GPRAVI.pdf");
                                    string gpraVpdf6 = Server.MapPath("../doc/") + "tmp\\" + Guid.NewGuid().ToString() + ".pdf";
                                    if (File.Exists(gpraVpdf6))
                                        File.Delete(gpraVpdf6);
                                    TempFiles.Add(gpraVpdf6);

                                    PdfReader pdfReader6 = new PdfReader(pdfTemplate6);
                                    PdfStamper pdfStamper6 = new PdfStamper(pdfReader6, new FileStream(
                                        gpraVpdf6, FileMode.Create));


                                    // close the pdf
                                    pdfReader6.Close();
                                    pdfStamper6.Close();

                                    //Add to final report

                                    PdfReader addrsreader6 = new PdfReader(gpraVpdf6);

                                    doc.NewPage();

                                    PdfImportedPage bsimportedPage6 = pdfWriter.GetImportedPage(addrsreader6, 1);
                                    //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                    pdfContentByte.AddTemplate(bsimportedPage6, 1.0F, 0, 0, 1.0F, -3, 0);
                                    doc.NewPage();
                                    bsimportedPage6 = pdfWriter.GetImportedPage(addrsreader6, 2);
                                    //pdfContentByte.AddTemplate(bsimportedPage, 0, 0);
                                    pdfContentByte.AddTemplate(bsimportedPage6, 1.0F, 0, 0, 1.0F, -3, 0);
                                    addrsreader6.Close();

                                }
                                catch (System.Exception ex)
                                {
                                    ILog Log = LogManager.GetLogger("EventLog");
                                    Log.Error("GPRA report:", ex);
                                }


                            }  //empty school info

                        } //foreach (MagnetSchool

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('There is no School in GPRA.');</script>", false);
                    }

                    //end GPRA
                    if (report.ReportPeriodID % 2 == 1)
                    {
                        //Apr report 

                        //Budget Summary
                        PdfDestination budgetDest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, budgetDest, pdfWriter);
                        PdfOutline budgetOutline = new PdfOutline(root, budgetDest, "Budget Summary");

                        PdfStamper bsps = null;

                        // read existing PDF document
                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TempFiles.Add(TmpPDF);

                        PdfReader bsr = new PdfReader(Server.MapPath("../doc/ed524budget.pdf"));
                        bsps = new PdfStamper(bsr, new FileStream(TmpPDF, FileMode.Create));

                        AcroFields bsaf = bsps.AcroFields;

                        decimal[] BudgetTotal = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                        decimal ColumnTotal = 0;
                        decimal ColumnTotalNonFederal = 0;
                        string[] affix = { "a", "b", "c", "d", "e", "f" };

                        int bsidx = 1;
                        int cnn = 1;
                        int cohortBoundary = 6;
                        string summary = "", itemized = "";
                        if (report.ReportPeriodID > 6) //cohort 2013
                        {
                            bsidx = 7;
                            cohortBoundary = 12;
                            cnn = 7;
                        }


                        while (bsidx < cohortBoundary && bsidx <= report.ReportPeriodID)
                        {
                            var adhocReports = GranteeReport.Find(x => x.GranteeID == grantee.ID && x.ReportPeriodID == bsidx + 1 && x.ReportType == false);
                            var reports = GranteeReport.Find(x => x.GranteeID == grantee.ID && x.ReportPeriodID == bsidx && x.ReportType == false);

                            if (reports.Count > 0 || adhocReports.Count > 0)
                            {
                                GranteeReport bsreport = null;
                                MagnetBudgetInformation MBI = null;
                                if (adhocReports.Count > 0)
                                {
                                    //Use ad hoc data if available
                                    var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == adhocReports[0].ID);
                                    if (data.Count > 0)
                                        MBI = data[0];
                                    bsreport = adhocReports[0];
                                }
                                else
                                {
                                    var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == reports[0].ID);
                                    if (data.Count > 0)
                                        MBI = data[0];
                                    bsreport = reports[0];
                                }
                                if (MBI != null)
                                {
                                    ColumnTotal = 0;
                                    ColumnTotalNonFederal = 0;
                                    decimal dd = 0;

                                    summary = MBI.BudgetSummary;
                                    itemized = MBI.BudgetItemized;

                                    //Personnel
                                    bsaf.SetField("1" + affix[bsidx - cnn] + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.Personnel));
                                    ColumnTotal += MBI.Personnel == null ? 0 : (decimal)MBI.Personnel;
                                    BudgetTotal[0] += MBI.Personnel == null ? 0 : (decimal)MBI.Personnel;
                                    bsaf.SetField("B" + cnn.ToString() + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.PersonnelNF));
                                    ColumnTotalNonFederal += MBI.PersonnelNF == null ? 0 : (decimal)MBI.PersonnelNF;
                                    BudgetTotal[13] += MBI.PersonnelNF == null ? 0 : (decimal)MBI.PersonnelNF;

                                    //Fringe Benefits
                                    dd = MBI.Fringe == null ? 0 : (decimal)MBI.Fringe;
                                    bsaf.SetField("2" + affix[bsidx - cnn] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[1] += dd;
                                    dd = MBI.FringeNF == null ? 0 : (decimal)MBI.FringeNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[14] += dd;

                                    //Travel
                                    dd = MBI.Travel == null ? 0 : (decimal)MBI.Travel;
                                    bsaf.SetField("3" + affix[bsidx - cnn] + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[2] += dd;
                                    dd = MBI.TravelNF == null ? 0 : (decimal)MBI.TravelNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[15] += dd;

                                    //Equipment
                                    dd = MBI.Equipment == null ? 0 : (decimal)MBI.Equipment;
                                    bsaf.SetField("4" + affix[bsidx - cnn] + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[3] += dd;
                                    dd = MBI.EquipmentNF == null ? 0 : (decimal)MBI.EquipmentNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[16] += dd;

                                    //Supplies
                                    dd = MBI.Supplies == null ? 0 : (decimal)MBI.Supplies;
                                    bsaf.SetField("5" + affix[bsidx - cnn] + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[4] += dd;
                                    dd = MBI.SuppliesNF == null ? 0 : (decimal)MBI.SuppliesNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[17] += dd;

                                    //contractual
                                    dd = MBI.Contractual == null ? 0 : (decimal)MBI.Contractual;
                                    bsaf.SetField("6" + affix[bsidx - cnn] + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[5] += dd;
                                    dd = MBI.ContractualNF == null ? 0 : (decimal)MBI.ContractualNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[18] += dd;

                                    //Construction
                                    dd = MBI.Construction == null ? 0 : (decimal)MBI.Construction;
                                    bsaf.SetField("7" + affix[bsidx - cnn] + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[6] += dd;
                                    dd = MBI.ConstructionNF == null ? 0 : (decimal)MBI.ConstructionNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[19] += dd;

                                    //Other
                                    dd = MBI.Other == null ? 0 : (decimal)MBI.Other;
                                    bsaf.SetField("8" + affix[bsidx - cnn] + " Other", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[7] += dd;
                                    dd = MBI.OtherNF == null ? 0 : (decimal)MBI.OtherNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Other", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[20] += dd;

                                    //Total Direct Costs
                                    if (ColumnTotal.CompareTo((decimal)0.0) >= 0)
                                    {
                                        bsaf.SetField("9" + affix[bsidx - cnn] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                        BudgetTotal[8] += ColumnTotal;
                                    }

                                    if (ColumnTotalNonFederal.CompareTo((decimal)0.0) >= 0)
                                    {
                                        bsaf.SetField("B" + cnn.ToString() + " Total Direct Costs", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                        BudgetTotal[21] += ColumnTotalNonFederal;
                                    }

                                    dd = MBI.IndirectCost == null ? 0 : (decimal)MBI.IndirectCost;
                                    bsaf.SetField("10" + affix[bsidx - cnn] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[9] += dd;
                                    dd = MBI.IndirectCostNF == null ? 0 : (decimal)MBI.IndirectCostNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[22] += dd;

                                    dd = MBI.TrainingStipends == null ? 0 : (decimal)MBI.TrainingStipends;
                                    bsaf.SetField("11" + affix[bsidx - cnn] + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[10] += dd;
                                    dd = MBI.TrainingStipendsNF == null ? 0 : (decimal)MBI.TrainingStipendsNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[23] += dd;

                                    //Total Costs
                                    bsaf.SetField("12" + affix[bsidx - cnn] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                    BudgetTotal[11] += ColumnTotal;

                                    bsaf.SetField("B" + cnn.ToString() + " Total Costs Lines", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                    BudgetTotal[24] += ColumnTotalNonFederal;
                                }
                            }

                            bsidx += 2;
                            cnn++;
                        }

                        //Total expense
                        bsidx = 6;
                        bsaf.SetField("1" + affix[bsidx - 1] + " Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[0]));
                        bsaf.SetField("2" + affix[bsidx - 1] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[1]));
                        bsaf.SetField("3" + affix[bsidx - 1] + " Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[2]));
                        bsaf.SetField("4" + affix[bsidx - 1] + " Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[3]));
                        bsaf.SetField("5" + affix[bsidx - 1] + " Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[4]));
                        bsaf.SetField("6" + affix[bsidx - 1] + " Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[5]));
                        bsaf.SetField("7" + affix[bsidx - 1] + " Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[6]));
                        bsaf.SetField("8" + affix[bsidx - 1] + " Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[7]));
                        bsaf.SetField("9" + affix[bsidx - 1] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", BudgetTotal[8]));
                        bsaf.SetField("10" + affix[bsidx - 1] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[9]));
                        bsaf.SetField("11" + affix[bsidx - 1] + " Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[10]));
                        bsaf.SetField("12" + affix[bsidx - 1] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", BudgetTotal[11]));

                        //Indirect Cost Information from lastest cover sheet
                        bsaf.SetField("B6 Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[13]));
                        bsaf.SetField("B6 Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[14]));
                        bsaf.SetField("B6 Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[15]));
                        bsaf.SetField("B6 Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[16]));
                        bsaf.SetField("B6 Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[17]));
                        bsaf.SetField("B6 Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[18]));
                        bsaf.SetField("B6 Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[19]));
                        bsaf.SetField("B6 Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[20]));
                        bsaf.SetField("B6 Total Direct Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[21]));
                        bsaf.SetField("B6 Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[22]));
                        bsaf.SetField("B6 Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[23]));
                        bsaf.SetField("B6 Total Costs Lines", "$" + string.Format("{0:#,0.00}", BudgetTotal[24]));

                        //Curent report period?
                        var bddata = MagnetBudgetInformation.Find(x => x.GranteeReportID == report.ID);
                        if (bddata.Count > 0)
                        {
                            MagnetBudgetInformation bdMBI = bddata[0];

                            if (bdMBI.IndirectCostAgreement != null)
                            {
                                if ((bool)bdMBI.IndirectCostAgreement)
                                {
                                    bsaf.SetField("AgreementYes", "Yes");
                                    if (!string.IsNullOrEmpty(bdMBI.AgreementFrom))
                                    {
                                        string[] strs = bdMBI.AgreementFrom.Split('/');
                                        bsaf.SetField("PeriodFrom1", strs[0]);
                                        bsaf.SetField("PeriodFrom2", strs.Count() >= 2 ? strs[1] : "");
                                        bsaf.SetField("PeriodFrom3", strs.Count() >= 3 ? strs[2] : "");
                                    }
                                    if (!string.IsNullOrEmpty(bdMBI.AgreementTo))
                                    {
                                        string[] strs = bdMBI.AgreementTo.Split('/');
                                        bsaf.SetField("PeriodTo1", strs[0]);
                                        bsaf.SetField("PeriodTo2", strs.Count() >= 2 ? strs[1] : "");
                                        bsaf.SetField("PeriodTo3", strs.Count() >= 3 ? strs[2] : "");
                                    }
                                    if (bdMBI.ApprovalAgency != null)
                                    {
                                        if ((bool)bdMBI.ApprovalAgency)
                                            bsaf.SetField("Ed", "Yes");
                                        else
                                        {
                                            bsaf.SetField("Other", "Yes");
                                            bsaf.SetField("Other approving agency", bdMBI.OtherAgency);
                                        }
                                    }
                                    bsaf.SetField("Indirect Cost Rate Percentage", Convert.ToString(bdMBI.IndirectCostRate));
                                }
                                else
                                    bsaf.SetField("AgreementNo", "Yes");
                            }

                            if (bdMBI.RestrictedRateProgram != null)
                            {
                                if (!(bool)bdMBI.na)
                                {
                                    if ((bool)bdMBI.RestrictedRateProgram)
                                        bsaf.SetField("Is it included in ICRA", "Yes");
                                    else
                                        bsaf.SetField("Complies with 34 CFR", "Yes");
                                }
                            }
                            bsaf.SetField("Restricted Indirect Cost Rate Percentage", Convert.ToString(bdMBI.RestrictedRate));
                        }

                        bsaf.SetField("InstitutionOrganization", grantee.GranteeName);

                        bsps.FormFlattening = true;

                        bsr.Close();
                        bsps.Close();

                        //Add to final report
                        PdfReader bsreader = new PdfReader(TmpPDF);
                        doc.SetPageSize(PageSize.A4.Rotate());
                        doc.NewPage();
                        PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(bsreader, 1);
                        pdfContentByte.AddTemplate(bsimportedPage, 24, -13);
                        doc.NewPage();
                        PdfImportedPage bsimportedPage2 = pdfWriter.GetImportedPage(bsreader, 2);
                        pdfContentByte.AddTemplate(bsimportedPage2, 24, -20);

                        doc.SetPageSize(PageSize.A4.Rotate());
                        doc.NewPage();
                        PdfImportedPage bsimportedPage3 = pdfWriter.GetImportedPage(bsreader, 3);
                        pdfContentByte.AddTemplate(bsimportedPage3, 0, -10);
                        bsreader.Close();

                        MagnetBudgetInformation MBIf = null;
                        var reportsf = GranteeReport.Find(x => x.GranteeID == report.GranteeID && x.ReportPeriodID == report.ReportPeriodID);
                        var dataf = MagnetBudgetInformation.Find(x => x.GranteeReportID == reportsf[0].ID);
                        if (dataf.Count > 0)
                            MBIf = dataf[0];

                        ReportID = Convert.ToInt32(reportsf[0].ID);
                        summary = "";
                        itemized = "";
                        if (MBIf != null)
                        {
                            summary = MBIf.BudgetSummary;
                            itemized = MBIf.BudgetItemized;
                        }

                        if (!string.IsNullOrEmpty(summary))
                        {
                            //Add budget summary
                            string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(ContentPDF))
                                File.Delete(ContentPDF);
                            TempFiles.Add(ContentPDF);

                            Document bsContentDocument = new Document(PageSize.A4, 10f, 10f, 20f, 20f);
                            PdfWriter bsContentWriter = PdfWriter.GetInstance(bsContentDocument, new FileStream(ContentPDF, FileMode.Create));
                            bsContentDocument.Open();
                            //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                            bsContentDocument.Add(new Paragraph(new Chunk("Budget Narrative", Time12Bold)));
                            bsContentDocument.Add(new Paragraph(new Chunk(summary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                            bsContentDocument.Close();


                            //Add to output
                            PdfReader bsfreader2 = new PdfReader(ContentPDF);
                            for (int j = 1; j <= bsfreader2.NumberOfPages; j++)
                            {
                                doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage bsfimportedPage = pdfWriter.GetImportedPage(bsfreader2, j);
                                //pdfContentByte.AddTemplate(bsfimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                pdfContentByte.AddTemplate(bsfimportedPage, 0, 0);
                            }
                            bsfreader2.Close();
                        }
                        // upload budget summary
                        doc.SetPageSize(PageSize.A4);
                        foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && (x.FormID == 13)))
                        {
                            PdfReader bsuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                            for (int t = 1; t <= bsuploadReader.NumberOfPages; t++)
                            {
                                width = bsuploadReader.GetPageSize(t).Width;
                                height = bsuploadReader.GetPageSize(t).Height;
                                if (width > height)
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                else
                                    doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage bsuploadPage = pdfWriter.GetImportedPage(bsuploadReader, t);
                                pdfContentByte.AddTemplate(bsuploadPage, 0, 0);
                            }
                            bsuploadReader.Close();
                        }

                        if (!string.IsNullOrEmpty(itemized))
                        {
                            //Add budget summary
                            string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(ContentPDF))
                                File.Delete(ContentPDF);
                            TempFiles.Add(ContentPDF);

                            Document bsContentDocument = new Document(PageSize.A4, 10f, 10f, 20f, 20f);
                            PdfWriter bsContentWriter = PdfWriter.GetInstance(bsContentDocument, new FileStream(ContentPDF, FileMode.Create));
                            bsContentDocument.Open();
                            //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                            bsContentDocument.Add(new Paragraph(new Chunk("Budget Itemized", Time12Bold)));
                            bsContentDocument.Add(new Paragraph(new Chunk(itemized, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                            bsContentDocument.Close();


                            //Add to output
                            PdfReader bsfreader2 = new PdfReader(ContentPDF);
                            for (int j = 1; j <= bsfreader2.NumberOfPages; j++)
                            {
                                doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage bsfimportedPage = pdfWriter.GetImportedPage(bsfreader2, j);
                                //pdfContentByte.AddTemplate(bsfimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                pdfContentByte.AddTemplate(bsfimportedPage, 0, 0);
                            }
                            bsfreader2.Close();
                        }



                        //Uploaded budget itemized file
                        doc.SetPageSize(PageSize.A4);
                        foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && (x.FormID == 14)))
                        {
                            PdfReader bsuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                            for (int t = 1; t <= bsuploadReader.NumberOfPages; t++)
                            {
                                width = bsuploadReader.GetPageSize(t).Width;
                                height = bsuploadReader.GetPageSize(t).Height;
                                if (width > height)
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                else
                                    doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage bsuploadPage = pdfWriter.GetImportedPage(bsuploadReader, t);
                                pdfContentByte.AddTemplate(bsuploadPage, 0, 0);
                            }
                            bsuploadReader.Close();
                        }

                        doc.SetPageSize(PageSize.A4);
                        //Desegregation
                        var DesegregationData = MagnetDesegregationPlan.Find(x => x.ReportID == ReportID);
                        if (DesegregationData.Count > 0)
                        {
                            PdfDestination desegregationDest = new PdfDestination(PdfDestination.FITBH);
                            PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, desegregationDest, pdfWriter);
                            PdfOutline desegregationOutline = new PdfOutline(root, desegregationDest, "Desegregation");

                            PdfStamper desegps = null;

                            // read existing PDF document
                            TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(TmpPDF))
                                File.Delete(TmpPDF);
                            TempFiles.Add(TmpPDF);

                            PdfReader desegr = new PdfReader(Server.MapPath("../doc/desegregation.pdf"));
                            desegps = new PdfStamper(desegr, new FileStream(TmpPDF, FileMode.Create));
                            AcroFields desegaf = desegps.AcroFields;


                            if (DesegregationData[0].isVoluntaryUpload != null)
                            {
                                if (DesegregationData[0].isVoluntaryUpload == true)

                                    desegaf.SetField("VoluntaryPlan", "Yes");
                            }

                            if (DesegregationData[0].isRequiredUpload != null)
                            {
                                if (DesegregationData[0].isRequiredUpload == true)
                                    desegaf.SetField("RequiredPlan", "Yes");
                            }
                            desegps.FormFlattening = true;

                            desegr.Close();
                            desegps.Close();

                            //Add to output
                            PdfReader desegfreader2 = new PdfReader(TmpPDF);
                            for (int j = 1; j <= desegfreader2.NumberOfPages; j++)
                            {
                                doc.NewPage();
                                PdfImportedPage desegfimportedPage = pdfWriter.GetImportedPage(desegfreader2, j);
                                pdfContentByte.AddTemplate(desegfimportedPage, 1, 60);
                            }
                            desegfreader2.Close();
                        }

                        //Uploaded Desegregation file
                        foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 9))
                        {
                            PdfReader uploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                            for (int t = 1; t <= uploadReader.NumberOfPages; t++)
                            {
                                width = uploadReader.GetPageSize(t).Width;
                                height = uploadReader.GetPageSize(t).Height;
                                if (width > height)
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                else
                                    doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage uploadPage = pdfWriter.GetImportedPage(uploadReader, t);
                                pdfContentByte.AddTemplate(uploadPage, 0, 0);
                            }
                            uploadReader.Close();
                        }

                        foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && (x.FormID == 10 || x.FormID == 11)))  //voluntary Desegregation Plan
                        {
                            PdfReader uploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                            for (int t = 1; t <= uploadReader.NumberOfPages; t++)
                            {
                                width = uploadReader.GetPageSize(t).Width;
                                height = uploadReader.GetPageSize(t).Height;
                                if (width > height)
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                else
                                    doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage uploadPage = pdfWriter.GetImportedPage(uploadReader, t);
                                pdfContentByte.AddTemplate(uploadPage, 0, 0);
                            }
                            uploadReader.Close();
                        }

                        //Assurance
                        //Uploaded Assurance file
                        var assurances = MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 12);
                        if (assurances.Count > 0)
                        {
                            PdfDestination assuranceDest = new PdfDestination(PdfDestination.FITBH);
                            PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, assuranceDest, pdfWriter);
                            PdfOutline assuranceOutline = new PdfOutline(root, assuranceDest, "Assurance");

                            foreach (MagnetUpload UploadedFile in assurances)
                            {
                                PdfReader uploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                                for (int t = 1; t <= uploadReader.NumberOfPages; t++)
                                {
                                    width = uploadReader.GetPageSize(t).Width;
                                    height = uploadReader.GetPageSize(t).Height;
                                    if (width > height)
                                        doc.SetPageSize(PageSize.A4.Rotate());
                                    else
                                        doc.SetPageSize(PageSize.A4);
                                    doc.NewPage();
                                    PdfImportedPage uploadPage = pdfWriter.GetImportedPage(uploadReader, t);
                                    pdfContentByte.AddTemplate(uploadPage, 0, 0);
                                }
                                uploadReader.Close();
                            }
                        }

                        doc.SetPageSize(PageSize.A4);
                        doc.SetMargins(10, 10, 80, 10);

                        //Table 7,8
                        MagnetDBDB dbGPRA = new MagnetDBDB();
                        var LeaData = from m in dbGPRA.MagnetSchoolEnrollments
                                      where m.GranteeReportID == ReportID
                                      && m.StageID == 1
                                      orderby m.GradeLevel
                                      select new
                                      {
                                          m.GradeLevel,
                                          AmericanIndian = m.AmericanIndian == null ? (int?)null : m.AmericanIndian,
                                          Asian = m.Asian == null ? (int?)null : m.Asian,
                                          AfricanAmerican = m.AfricanAmerican == null ? (int?)null : m.AfricanAmerican,
                                          Hispanic = m.Hispanic == null ? (int?)null : m.Hispanic,
                                          Hawaiian = m.Hawaiian == null ? (int?)null : m.Hawaiian,
                                          White = m.White == null ? (int?)null : m.White,
                                          MultiRacial = m.MultiRacial == null ? (int?)null : m.MultiRacial,
                                          GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                                          + (m.Asian == null ? 0 : m.Asian)
                                          + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                                          + (m.Hispanic == null ? 0 : m.Hispanic)
                                          + (m.Hawaiian == null ? 0 : m.Hawaiian)
                                          + (m.White == null ? 0 : m.White)
                                          + (m.MultiRacial == null ? 0 : m.MultiRacial))
                                      };

                        PdfDestination table78Dest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, table78Dest, pdfWriter);
                        PdfOutline table78Outline = new PdfOutline(root, table78Dest, "Table 1 & 2");

                        PdfStamper leaps = null;

                        // read existing PDF document
                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TempFiles.Add(TmpPDF);

                        PdfReader lear = new PdfReader(Server.MapPath("../doc/Tables12.pdf"));
                        leaps = new PdfStamper(lear, new FileStream(TmpPDF, FileMode.Create));
                        AcroFields leaaf = leaps.AcroFields;

                        if (report.FirstTimeMagnetProgram != null && report.FirstTimeMagnetProgram == true)
                            leaaf.SetField("FirstYearProgram", "Yes");

                        //if (LeaData.Count() > 0)
                        {

                            string DateString = "(Year ";
                            DateString += reportyear.ToString();
                            DateString += " of Project—Oct 1, ";
                            DateString += MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID).ReportPeriod.Substring(0, 4);
                            DateString += ")";
                            leaaf.SetField("Date", DateString);

                            int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                            int total = 0;
                            foreach (var Enrollment in LeaData)
                            {
                                total = 0;
                                if (Enrollment.AmericanIndian != null)
                                {
                                    total += Convert.ToInt32(Enrollment.AmericanIndian);
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.AmericanIndian)));
                                }
                                else
                                {
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "India", " ");
                                }
                                if (Enrollment.Asian != null)
                                {
                                    total += Convert.ToInt32(Enrollment.Asian);
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Asian)));
                                }
                                else
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Asian", " ");
                                if (Enrollment.AfricanAmerican != null)
                                {
                                    total += Convert.ToInt32(Enrollment.AfricanAmerican);
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.AfricanAmerican)));
                                }
                                else
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Black", " ");
                                if (Enrollment.Hispanic != null)
                                {
                                    total += Convert.ToInt32(Enrollment.Hispanic);
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Hispanic)));
                                }
                                else
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", " ");
                                if (Enrollment.Hawaiian != null)
                                {
                                    total += Convert.ToInt32(Enrollment.Hawaiian);
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Hawaiian)));
                                }
                                else
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", " ");
                                if (Enrollment.White != null)
                                {
                                    total += Convert.ToInt32(Enrollment.White);
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.White)));
                                }
                                else
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "White", " ");
                                if (Enrollment.MultiRacial != null)
                                {
                                    total += Convert.ToInt32(Enrollment.MultiRacial);
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.MultiRacial)));
                                }
                                else
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "Multi", " ");

                                leaaf.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger(total));

                                if (total > 0)
                                {
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", Enrollment.AmericanIndian == null ? 0 : Convert.ToDouble(Enrollment.AmericanIndian) / total * 100));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", Enrollment.Asian == null ? 0 : Convert.ToDouble(Enrollment.Asian) / total * 100));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", Enrollment.AfricanAmerican == null ? 0 : Convert.ToDouble(Enrollment.AfricanAmerican) / total * 100));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", Enrollment.Hispanic == null ? 0 : Convert.ToDouble(Enrollment.Hispanic) / total * 100));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", Enrollment.Hawaiian == null ? 0 : Convert.ToDouble(Enrollment.Hawaiian) / total * 100));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", Enrollment.White == null ? 0 : Convert.ToDouble(Enrollment.White) / total * 100));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", Enrollment.MultiRacial == null ? 0 : Convert.ToDouble(Enrollment.MultiRacial) / total * 100));

                                }
                                else
                                {
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", 0));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", 0));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", 0));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", 0));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", 0));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", 0));
                                    leaaf.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", 0));
                                }

                                if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += Convert.ToInt32(Enrollment.AmericanIndian);
                                if (Enrollment.Asian != null) TotalEnrollments[1] += Convert.ToInt32(Enrollment.Asian);
                                if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += Convert.ToInt32(Enrollment.AfricanAmerican);
                                if (Enrollment.Hispanic != null) TotalEnrollments[3] += Convert.ToInt32(Enrollment.Hispanic);
                                if (Enrollment.Hawaiian != null) TotalEnrollments[4] += Convert.ToInt32(Enrollment.Hawaiian);
                                if (Enrollment.White != null) TotalEnrollments[5] += Convert.ToInt32(Enrollment.White);
                                if (Enrollment.MultiRacial != null) TotalEnrollments[6] += Convert.ToInt32(Enrollment.MultiRacial);
                                if (Enrollment.GradeTotal != null) TotalEnrollments[7] += total;

                            }

                            leaaf.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                            leaaf.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                            leaaf.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                            leaaf.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                            leaaf.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                            leaaf.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                            leaaf.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                            leaaf.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));

                            if (TotalEnrollments[7] > 0)
                            {
                                leaaf.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                                leaaf.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                                leaaf.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                                leaaf.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                                leaaf.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                                leaaf.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                                leaaf.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));
                            }
                            else
                            {
                                leaaf.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", 0));
                                leaaf.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", 0));
                                leaaf.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", 0));
                                leaaf.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", 0));
                                leaaf.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", 0));
                                leaaf.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", 0));
                                leaaf.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", 0));
                            }

                            int Idx = 1;

                            int GranteeID = Convert.ToInt32(DropDownList1.SelectedValue);

                            var SchoolYearData = (from m in db.MagnetSchoolYears
                                                  from n in db.MagnetSchools
                                                  where m.SchoolID == n.ID
                                                  && m.ReportID == Convert.ToInt32(ReportID)
                                                  && n.ReportYear == reportyear && n.isActive && n.GranteeID == GranteeID
                                                  select new { m.ID, n.SchoolName, m.SchoolYear });
                            foreach (var SchoolYear in SchoolYearData)
                            {

                                leaaf.SetField("School NameRow" + Idx.ToString(), SchoolYear.SchoolName);
                                leaaf.SetField("School YearRow" + Idx.ToString(), SchoolYear.SchoolYear);

                                Idx++;
                            }
                        }
                        leaps.FormFlattening = true;

                        lear.Close();
                        leaps.Close();


                        //Add to output
                        PdfReader leafreader2 = new PdfReader(TmpPDF);
                        for (int j = 1; j <= leafreader2.NumberOfPages; j++)
                        {
                            doc.NewPage();
                            PdfImportedPage leaimportedPage = pdfWriter.GetImportedPage(leafreader2, j);
                            pdfContentByte.AddTemplate(leaimportedPage, 1.0F, 0, 0, 1.0F, 0, 60);
                        }
                        leafreader2.Close();


                        //Table 9
                        PdfDestination table9Dest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, table9Dest, pdfWriter);
                        PdfOutline table9Outline = new PdfOutline(root, table9Dest, "Table 3");
                        foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == grantee.ID && x.ReportYear == reportyear && x.isActive).OrderBy(x => x.SchoolName))
                        {
                            TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(TmpPDF))
                                File.Delete(TmpPDF);
                            TempFiles.Add(TmpPDF);

                            PdfStamper enrollps = null;

                            // Fill out form
                            PdfReader enrollr = new PdfReader(Server.MapPath("../doc/Table3.pdf"));
                            enrollps = new PdfStamper(enrollr, new FileStream(TmpPDF, FileMode.Create));

                            AcroFields enrollaf = enrollps.AcroFields;

                            enrollaf.SetField("SchoolName", School.SchoolName);

                            string DateString = "(Year ";
                            DateString += reportyear.ToString();
                            DateString += " of Project—Oct 1, ";
                            DateString += MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID).ReportPeriod.Substring(0, 4);
                            DateString += ")";
                            enrollaf.SetField("Date", DateString);

                            var EnrollmentData = from m in db.MagnetSchoolEnrollments
                                                 where m.GranteeReportID == ReportID
                                                 && m.SchoolID == School.ID
                                                 && m.StageID == 2//School enrollment data
                                                 orderby m.GradeLevel
                                                 select new
                                                 {
                                                     m.GradeLevel,
                                                     AmericanIndian = m.AmericanIndian == null ? (int?)null : m.AmericanIndian,
                                                     Asian = m.Asian == null ? (int?)null : m.Asian,
                                                     AfricanAmerican = m.AfricanAmerican == null ? (int?)null : m.AfricanAmerican,
                                                     Hispanic = m.Hispanic == null ? (int?)null : m.Hispanic,
                                                     Hawaiian = m.Hawaiian == null ? (int?)null : m.Hawaiian,
                                                     White = m.White == null ? (int?)null : m.White,
                                                     MultiRacial = m.MultiRacial == null ? (int?)null : m.MultiRacial,
                                                     GradeTotal = ((m.AmericanIndian == null ? 0 : m.AmericanIndian)
                                                     + (m.Asian == null ? 0 : m.Asian)
                                                     + (m.AfricanAmerican == null ? 0 : m.AfricanAmerican)
                                                     + (m.Hispanic == null ? 0 : m.Hispanic)
                                                     + (m.Hawaiian == null ? 0 : m.Hawaiian)
                                                     + (m.White == null ? 0 : m.White)
                                                     + (m.MultiRacial == null ? 0 : m.MultiRacial))
                                                 };

                            int[] TotalEnrollments = { 0, 0, 0, 0, 0, 0, 0, 0 };
                            int total = 0;
                            foreach (var Enrollment in EnrollmentData)
                            {
                                total = 0;
                                if (Enrollment.AmericanIndian != null)
                                {
                                    total += Convert.ToInt32(Enrollment.AmericanIndian);
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "India", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.AmericanIndian)));
                                }
                                else
                                {
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "India", " ");
                                }
                                if (Enrollment.Asian != null)
                                {
                                    total += Convert.ToInt32(Enrollment.Asian);
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Asian", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Asian)));
                                }

                                if (Enrollment.AfricanAmerican != null)
                                {
                                    total += Convert.ToInt32(Enrollment.AfricanAmerican);
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Black", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.AfricanAmerican)));
                                }
                                if (Enrollment.Hispanic != null)
                                {
                                    total += Convert.ToInt32(Enrollment.Hispanic);
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Hispanic", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Hispanic)));
                                }
                                if (Enrollment.Hawaiian != null)
                                {
                                    total += Convert.ToInt32(Enrollment.Hawaiian);
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Hawaiian", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Hawaiian)));
                                }
                                if (Enrollment.White != null)
                                {
                                    total += Convert.ToInt32(Enrollment.White);
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "White", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.White)));
                                }
                                if (Enrollment.MultiRacial != null)
                                {
                                    total += Convert.ToInt32(Enrollment.MultiRacial);
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Multi", ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.MultiRacial)));
                                }
                                enrollaf.SetField(Enrollment.GradeLevel.ToString() + "Total", ManageUtility.FormatInteger(total));
                                if (total > 0)
                                {

                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", Enrollment.AmericanIndian == null ? 0 : Convert.ToDouble(Enrollment.AmericanIndian) / total * 100));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", Enrollment.Asian == null ? 0 : Convert.ToDouble(Enrollment.Asian) / total * 100));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", Enrollment.AfricanAmerican == null ? 0 : Convert.ToDouble(Enrollment.AfricanAmerican) / total * 100));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", Enrollment.Hispanic == null ? 0 : Convert.ToDouble(Enrollment.Hispanic) / total * 100));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", Enrollment.Hawaiian == null ? 0 : Convert.ToDouble(Enrollment.Hawaiian) / total * 100));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", Enrollment.White == null ? 0 : Convert.ToDouble(Enrollment.White) / total * 100));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", Enrollment.MultiRacial == null ? 0 : Convert.ToDouble(Enrollment.MultiRacial) / total * 100));

                                }
                                else
                                {

                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "IndiaPercentage", String.Format("{0:,0.00}", 0));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "AsianPercentage", String.Format("{0:,0.00}", 0));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "BlackPercentage", String.Format("{0:,0.00}", 0));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "HispanicPercentage", String.Format("{0:,0.00}", 0));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "HawaiianPercentage", String.Format("{0:,0.00}", 0));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "WhitePercentage", String.Format("{0:,0.00}", 0));
                                    enrollaf.SetField(Enrollment.GradeLevel.ToString() + "MultiPercentage", String.Format("{0:,0.00}", 0));

                                }


                                if (Enrollment.AmericanIndian != null) TotalEnrollments[0] += Convert.ToInt32(Enrollment.AmericanIndian);
                                if (Enrollment.Asian != null) TotalEnrollments[1] += Convert.ToInt32(Enrollment.Asian);
                                if (Enrollment.AfricanAmerican != null) TotalEnrollments[2] += Convert.ToInt32(Enrollment.AfricanAmerican);
                                if (Enrollment.Hispanic != null) TotalEnrollments[3] += Convert.ToInt32(Enrollment.Hispanic);
                                if (Enrollment.Hawaiian != null) TotalEnrollments[4] += Convert.ToInt32(Enrollment.Hawaiian);
                                if (Enrollment.White != null) TotalEnrollments[5] += Convert.ToInt32(Enrollment.White);
                                if (Enrollment.MultiRacial != null) TotalEnrollments[6] += Convert.ToInt32(Enrollment.MultiRacial);
                                if (Enrollment.GradeTotal != null) TotalEnrollments[7] += total;
                            }

                            enrollaf.SetField("TotalIndia", ManageUtility.FormatInteger((int)TotalEnrollments[0]));
                            enrollaf.SetField("TotalAsian", ManageUtility.FormatInteger((int)TotalEnrollments[1]));
                            enrollaf.SetField("TotalBlack", ManageUtility.FormatInteger((int)TotalEnrollments[2]));
                            enrollaf.SetField("TotalHispanic", ManageUtility.FormatInteger((int)TotalEnrollments[3]));
                            enrollaf.SetField("TotalHawaiian", ManageUtility.FormatInteger((int)TotalEnrollments[4]));
                            enrollaf.SetField("TotalWhite", ManageUtility.FormatInteger((int)TotalEnrollments[5]));
                            enrollaf.SetField("TotalMulti", ManageUtility.FormatInteger((int)TotalEnrollments[6]));
                            enrollaf.SetField("TotalTotal", ManageUtility.FormatInteger((int)TotalEnrollments[7]));

                            if (TotalEnrollments[7] > 0)
                            {
                                enrollaf.SetField("TotalIndiaPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[0] / TotalEnrollments[7] * 100));
                                enrollaf.SetField("TotalAsianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[1] / TotalEnrollments[7] * 100));
                                enrollaf.SetField("TotalBlackPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[2] / TotalEnrollments[7] * 100));
                                enrollaf.SetField("TotalHispanicPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[3] / TotalEnrollments[7] * 100));
                                enrollaf.SetField("TotalHawaiianPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[4] / TotalEnrollments[7] * 100));
                                enrollaf.SetField("TotalWhitePercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[5] / TotalEnrollments[7] * 100));
                                enrollaf.SetField("TotalMultiPercentage", String.Format("{0:,0.00}", (double)TotalEnrollments[6] / TotalEnrollments[7] * 100));
                            }
                            else
                            {
                                enrollaf.SetField("TotalIndiaPercentage", "0");
                                enrollaf.SetField("TotalAsianPercentage", "0");
                                enrollaf.SetField("TotalBlackPercentage", "0");
                                enrollaf.SetField("TotalHispanicPercentage", "0");
                                enrollaf.SetField("TotalHawaiianPercentage", "0");
                                enrollaf.SetField("TotalWhitePercentage", "0");
                                enrollaf.SetField("TotalMultiPercentage", "0");
                            }

                            enrollps.FormFlattening = true;

                            enrollr.Close();
                            enrollps.Close();

                            //Add to final report
                            PdfReader enrollreader = new PdfReader(TmpPDF);
                            doc.NewPage();
                            PdfImportedPage enrollimportedPage = pdfWriter.GetImportedPage(enrollreader, 1);
                            pdfContentByte.AddTemplate(enrollimportedPage, 0, 80);
                            doc.NewPage();
                            enrollreader.Close();
                        }

                        //Table 4
                        PdfDestination table11Dest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber, table11Dest, pdfWriter);
                        PdfOutline table11Outline = new PdfOutline(root, table11Dest, "Table 4");

                        doc.SetMargins(10, 10, 40, 10);
                        doc.NewPage();
                        iTextSharp.text.Rectangle page = new iTextSharp.text.Rectangle(PageSize.A4.Width, PageSize.A4.Height); // doc.PageSize;
                        PdfPTable head = new PdfPTable(2);
                        head.TotalWidth = PageSize.A4.Width - 50; //100;
                        head.LockedWidth = true;
                        head.SetWidths(new float[] { 10f, 90f });
                        Phrase phrase = new Phrase("Table 4", Time12Bold);
                        PdfPCell c = new PdfPCell(phrase);
                        c.Colspan = 2;
                        c.MinimumHeight = 40f;
                        c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_TOP;
                        c.HorizontalAlignment = Element.ALIGN_CENTER;
                        //head.AddCell(c);

                        phrase = new Phrase("Table 4: Feeder School - Enrollment Data ", Time10Bold);
                        //phrase.Add(new Chunk("(LEAs that HAVE converted to new race and ethnic categories)", Time10BoldItalic));
                        c = new PdfPCell(phrase);
                        c.Colspan = 2;
                        c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_BOTTOM;
                        c.HorizontalAlignment = Element.ALIGN_LEFT;
                        head.AddCell(c);

                        phrase = new Phrase("•", Time10Bold);
                        c = new PdfPCell(phrase);
                        c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_TOP;
                        c.HorizontalAlignment = Element.ALIGN_CENTER;
                        head.AddCell(c);

                        phrase = new Phrase("For each feeder school, identify the magnet school(s) to which the feeder school would send students.  If a feeder school would send students to all magnet schools at a   particular grade level (for example, Elementary Feeder School “X” would send students to all of the elementary magnet schools participating in the project, indicate “All” in the “Magnet” column associated with Elementary Feeder School “X”). ", Time10Normal);
                        c = new PdfPCell(phrase);
                        c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_TOP;
                        c.HorizontalAlignment = Element.ALIGN_LEFT;
                        head.AddCell(c);

                        phrase = new Phrase("•", Time10Bold);
                        c = new PdfPCell(phrase);
                        c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_TOP;
                        c.HorizontalAlignment = Element.ALIGN_CENTER;
                        head.AddCell(c);

                        phrase = new Phrase("Use additional sheets, if necessary.", Time10Normal);
                        c = new PdfPCell(phrase);
                        c.Colspan = 2;
                        c.MinimumHeight = 20f;
                        c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_TOP;
                        c.HorizontalAlignment = Element.ALIGN_LEFT;
                        head.AddCell(c);
                        doc.Add(head);

                        //Data table
                        PdfPTable DataTable = new PdfPTable(17);
                        DataTable.TotalWidth = PageSize.A4.Width - 50; //100;
                        DataTable.LockedWidth = true;
                        DataTable.SetWidths(new float[] { 12f, 13f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f });

                        phrase = new Phrase("Schools", Time10Bold);
                        c = new PdfPCell(phrase);
                        c.Colspan = 2;
                        //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_CENTER;
                        c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                        DataTable.AddCell(c);

                        string ds = "Actual Enrollment as of October 1, ";
                        ds += MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID).ReportPeriod.Substring(0, 4);
                        ds += "\r\n";
                        //ds += "(Year ";
                        //ds += MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID).reportYear;
                        //ds += " of Project)";
                        ds += "(Current School Year)";

                        //phrase = new Phrase("Actual Enrollment as of October 1, 2010 \r\n(Year 1 of Project)", Time10Bold);
                        phrase = new Phrase(ds, Time10Bold);
                        c = new PdfPCell(phrase);
                        c.Colspan = 15;
                        //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_CENTER;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("FEEDER", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("MAGNET(S)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("American Indian /Alaskan Native (Number)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("American Indian /Alaskan Native (%)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Asian (Number)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Asian (%)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Black or African-American (Number) ", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Black or African-American (%) ", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Hispanic/Latino (Number)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Hispanic/Latino (%)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander  (Number)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander (%)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("White (Number)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("White (%)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Two or more races (Number) ", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Two or more races (%)", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        c = new PdfPCell(new Phrase("Total  Students", Time10Bold));
                        c.Rotation = 90;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.HorizontalAlignment = Element.ALIGN_BOTTOM;
                        DataTable.AddCell(c);

                        //Reorder
                        db.spp_feederenrollment_order(ReportID, 3).Execute();

                        var FeederData = from m in db.MagnetSchoolFeederEnrollments
                                         where m.GranteeReportID == ReportID
                                          && m.StageID == 3

                                         orderby m.TotalEnrollment
                                         select new
                                         {
                                             m.ID,
                                             m.FeederSchoolID,
                                             m.SchoolID,
                                             AmericanIndian = m.AmericanIndian == null ? "" : m.AmericanIndian.ToString(),
                                             Asian = m.Asian == null ? "" : m.Asian.ToString(),
                                             AfricanAmerican = m.AfricanAmerican == null ? "" : m.AfricanAmerican.ToString(),
                                             Hispanic = m.Hispanic == null ? "" : m.Hispanic.ToString(),
                                             Hawaiian = m.Hawaiian == null ? "" : m.Hawaiian.ToString(),
                                             White = m.White == null ? "" : m.White.ToString(),
                                             MultiRacial = m.MultiRacial == null ? "" : m.MultiRacial.ToString(),
                                             GradeTotal = ((m.AmericanIndian == null ? 0 : Convert.ToInt32(m.AmericanIndian))
                                             + (m.Asian == null ? 0 : Convert.ToInt32(m.Asian))
                                             + (m.AfricanAmerican == null ? 0 : Convert.ToInt32(m.AfricanAmerican))
                                             + (m.Hispanic == null ? 0 : Convert.ToInt32(m.Hispanic))
                                             + (m.Hawaiian == null ? 0 : Convert.ToInt32(m.Hawaiian))
                                             + (m.White == null ? 0 : Convert.ToInt32(m.White))
                                             + (m.MultiRacial == null ? 0 : Convert.ToInt32(m.MultiRacial)))
                                         };

                        int count = 0;
                        List<int> excludelist = new List<int>();
                        var mschools = MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && x.isActive == false);
                        var enrollments = MagnetSchoolFeederEnrollment.Find(m => m.GranteeReportID == report.ID && m.StageID == 3);
                        string[] deactiveshools = mschools.Select(x => x.ID.ToString()).ToArray();

                        foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == report.GranteeID && x.ReportYear == reportyear && !x.isActive))
                        {
                            foreach (var item in FeederData)
                            {
                                if (school.ID.ToString() == item.SchoolID)
                                    excludelist.Add(item.ID);
                                else
                                {
                                    foreach (string meId in item.SchoolID.Split(';'))
                                    {
                                        if (!deactiveshools.Contains(meId))
                                            break;
                                        count++;
                                    }
                                    if (item.SchoolID.Split(';').Count() == count)
                                        excludelist.Add(item.ID);
                                    count = 0;
                                }

                            }

                        }

                        foreach (var Enrollment in FeederData)
                        {
                            if (!excludelist.Exists(element => element == Enrollment.ID))
                            {
                                MagnetFeederSchool feederSchool = MagnetFeederSchool.SingleOrDefault(x => x.ID == (int)(Enrollment.FeederSchoolID));
                                if (feederSchool != null)
                                {
                                    c = new PdfPCell(new Phrase(feederSchool.SchoolName, Time10Normal));
                                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                                    DataTable.AddCell(c);
                                }
                                else
                                {
                                    c = new PdfPCell(new Phrase("", Time10Normal));
                                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                                    DataTable.AddCell(c);
                                }

                                if (!string.IsNullOrEmpty(Enrollment.SchoolID))
                                {
                                    string strSchoolName = "";
                                    if (Enrollment.SchoolID.Equals("999999"))
                                    {
                                        strSchoolName = "All";
                                    }
                                    else
                                    {
                                        foreach (string magnetSchoolID in Enrollment.SchoolID.Split(';'))
                                        {
                                            MagnetSchool magnetSchool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(magnetSchoolID) && x.isActive);
                                            if (magnetSchool != null)
                                                strSchoolName += string.IsNullOrEmpty(strSchoolName) ? magnetSchool.SchoolName : "\r\n" + magnetSchool.SchoolName;
                                        }
                                    }
                                    c = new PdfPCell(new Phrase(strSchoolName, Time10Normal));
                                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                                    DataTable.AddCell(c);
                                }
                                else
                                {
                                    c = new PdfPCell(new Phrase("", Time10Normal));
                                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                                    DataTable.AddCell(c);
                                }
                                if (!string.IsNullOrEmpty(Enrollment.AmericanIndian))
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.AmericanIndian)), Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : Convert.ToDouble(Enrollment.AmericanIndian) / Enrollment.GradeTotal * 100), Time8Normal)));
                                }
                                else
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(" ", Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));

                                }
                                if (!string.IsNullOrEmpty(Enrollment.Asian))
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Asian)), Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : Convert.ToDouble(Enrollment.Asian) / Enrollment.GradeTotal * 100), Time8Normal)));
                                }
                                else
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(" ", Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                                }
                                if (!string.IsNullOrEmpty(Enrollment.AfricanAmerican))
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.AfricanAmerican)), Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : Convert.ToDouble(Enrollment.AfricanAmerican) / Enrollment.GradeTotal * 100), Time8Normal)));
                                }
                                else
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(" ", Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                                }
                                if (!string.IsNullOrEmpty(Enrollment.Hispanic))
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Hispanic)), Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : Convert.ToDouble(Enrollment.Hispanic) / Enrollment.GradeTotal * 100), Time8Normal)));
                                }
                                else
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(" ", Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                                }
                                if (!string.IsNullOrEmpty(Enrollment.Hawaiian))
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.Hawaiian)), Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : Convert.ToDouble(Enrollment.Hawaiian) / Enrollment.GradeTotal * 100), Time8Normal)));
                                }
                                else
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(" ", Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                                }
                                if (!string.IsNullOrEmpty(Enrollment.White))
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.White)), Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : Convert.ToDouble(Enrollment.White) / Enrollment.GradeTotal * 100), Time8Normal)));
                                }
                                else
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(" ", Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                                }
                                if (!string.IsNullOrEmpty(Enrollment.MultiRacial))
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger(Convert.ToInt32(Enrollment.MultiRacial)), Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", Enrollment.GradeTotal == 0 ? 0 : Convert.ToDouble(Enrollment.MultiRacial) / Enrollment.GradeTotal * 100), Time8Normal)));
                                }
                                else
                                {
                                    DataTable.AddCell(new PdfPCell(new Phrase(" ", Time8Normal)));
                                    DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", 0), Time8Normal)));
                                }
                                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.GradeTotal), Time8Normal)));
                            }
                        }

                        //add empty rows for table11 if no data avaiable
                        if (FeederData.Count() == 0)
                        {
                            for (int i = 0; i < 25; i++)
                            {
                                c = new PdfPCell();

                                c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                                DataTable.AddCell(c);

                                c = new PdfPCell(new Phrase(" ", Time10Normal));
                                c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                                DataTable.AddCell(c);

                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));
                                DataTable.AddCell(new PdfPCell(new Phrase(" ", Time10Normal)));

                            }
                        }

                        doc.Add(DataTable);

                    }
                    else  //ad hoc
                    {
                        //Budget Summary
                        PdfDestination budgetDest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, budgetDest, pdfWriter);
                        PdfOutline budgetOutline = new PdfOutline(root, budgetDest, "Budget Summary");

                        PdfStamper bsps = null;

                        // read existing PDF document
                        TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                        if (File.Exists(TmpPDF))
                            File.Delete(TmpPDF);
                        TempFiles.Add(TmpPDF);

                        PdfReader bsr = new PdfReader(Server.MapPath("../doc/ed524budget.pdf"));
                        bsps = new PdfStamper(bsr, new FileStream(TmpPDF, FileMode.Create));

                        AcroFields bsaf = bsps.AcroFields;

                        decimal[] BudgetTotal = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                        decimal ColumnTotal = 0;
                        decimal ColumnTotalNonFederal = 0;
                        string[] affix = { "a", "b", "c", "d", "e", "f" };

                        int bsidx = 1;
                        int cnn = 1;
                        int cohortBoundary = 6;
                        string summary = "", itemized = "";
                        if (report.ReportPeriodID > 6) //cohort 2013
                        {
                            bsidx = 7;
                            cohortBoundary = 12;
                            cnn = 7;
                        }


                        while (bsidx < cohortBoundary && bsidx <= report.ReportPeriodID)
                        {
                            var adhocReports = GranteeReport.Find(x => x.GranteeID == grantee.ID && x.ReportPeriodID == bsidx + 1 && x.ReportType == false);
                            var reports = GranteeReport.Find(x => x.GranteeID == grantee.ID && x.ReportPeriodID == bsidx && x.ReportType == false);

                            if (reports.Count > 0 || adhocReports.Count > 0)
                            {
                                GranteeReport bsreport = null;
                                MagnetBudgetInformation MBI = null;
                                if (adhocReports.Count > 0)
                                {
                                    //Use ad hoc data if available
                                    var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == adhocReports[0].ID);
                                    if (data.Count > 0)
                                        MBI = data[0];
                                    bsreport = adhocReports[0];
                                }
                                else
                                {
                                    var data = MagnetBudgetInformation.Find(x => x.GranteeReportID == reports[0].ID);
                                    if (data.Count > 0)
                                        MBI = data[0];
                                    bsreport = reports[0];
                                }
                                if (MBI != null)
                                {
                                    ColumnTotal = 0;
                                    ColumnTotalNonFederal = 0;
                                    decimal dd = 0;

                                    summary = MBI.BudgetSummary;
                                    itemized = MBI.BudgetItemized;

                                    //Personnel
                                    bsaf.SetField("1" + affix[bsidx - cnn] + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.Personnel));
                                    ColumnTotal += MBI.Personnel == null ? 0 : (decimal)MBI.Personnel;
                                    BudgetTotal[0] += MBI.Personnel == null ? 0 : (decimal)MBI.Personnel;
                                    bsaf.SetField("B" + cnn.ToString() + " Personnel", "$" + string.Format("{0:#,0.00}", MBI.PersonnelNF));
                                    ColumnTotalNonFederal += MBI.PersonnelNF == null ? 0 : (decimal)MBI.PersonnelNF;
                                    BudgetTotal[13] += MBI.PersonnelNF == null ? 0 : (decimal)MBI.PersonnelNF;

                                    //Fringe Benefits
                                    dd = MBI.Fringe == null ? 0 : (decimal)MBI.Fringe;
                                    bsaf.SetField("2" + affix[bsidx - cnn] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[1] += dd;
                                    dd = MBI.FringeNF == null ? 0 : (decimal)MBI.FringeNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[14] += dd;

                                    //Travel
                                    dd = MBI.Travel == null ? 0 : (decimal)MBI.Travel;
                                    bsaf.SetField("3" + affix[bsidx - cnn] + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[2] += dd;
                                    dd = MBI.TravelNF == null ? 0 : (decimal)MBI.TravelNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Travel", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[15] += dd;

                                    //Equipment
                                    dd = MBI.Equipment == null ? 0 : (decimal)MBI.Equipment;
                                    bsaf.SetField("4" + affix[bsidx - cnn] + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[3] += dd;
                                    dd = MBI.EquipmentNF == null ? 0 : (decimal)MBI.EquipmentNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Equipment", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[16] += dd;

                                    //Supplies
                                    dd = MBI.Supplies == null ? 0 : (decimal)MBI.Supplies;
                                    bsaf.SetField("5" + affix[bsidx - cnn] + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[4] += dd;
                                    dd = MBI.SuppliesNF == null ? 0 : (decimal)MBI.SuppliesNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Supplies", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[17] += dd;

                                    //contractual
                                    dd = MBI.Contractual == null ? 0 : (decimal)MBI.Contractual;
                                    bsaf.SetField("6" + affix[bsidx - cnn] + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[5] += dd;
                                    dd = MBI.ContractualNF == null ? 0 : (decimal)MBI.ContractualNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Contractual", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[18] += dd;

                                    //Construction
                                    dd = MBI.Construction == null ? 0 : (decimal)MBI.Construction;
                                    bsaf.SetField("7" + affix[bsidx - cnn] + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[6] += dd;
                                    dd = MBI.ConstructionNF == null ? 0 : (decimal)MBI.ConstructionNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Construction", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[19] += dd;

                                    //Other
                                    dd = MBI.Other == null ? 0 : (decimal)MBI.Other;
                                    bsaf.SetField("8" + affix[bsidx - cnn] + " Other", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[7] += dd;
                                    dd = MBI.OtherNF == null ? 0 : (decimal)MBI.OtherNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Other", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[20] += dd;

                                    //Total Direct Costs
                                    if (ColumnTotal.CompareTo((decimal)0.0) >= 0)
                                    {
                                        bsaf.SetField("9" + affix[bsidx - cnn] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                        BudgetTotal[8] += ColumnTotal;
                                    }

                                    if (ColumnTotalNonFederal.CompareTo((decimal)0.0) >= 0)
                                    {
                                        bsaf.SetField("B" + cnn.ToString() + " Total Direct Costs", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                        BudgetTotal[21] += ColumnTotalNonFederal;
                                    }

                                    dd = MBI.IndirectCost == null ? 0 : (decimal)MBI.IndirectCost;
                                    bsaf.SetField("10" + affix[bsidx - cnn] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[9] += dd;
                                    dd = MBI.IndirectCostNF == null ? 0 : (decimal)MBI.IndirectCostNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Indirect Costs", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[22] += dd;

                                    dd = MBI.TrainingStipends == null ? 0 : (decimal)MBI.TrainingStipends;
                                    bsaf.SetField("11" + affix[bsidx - cnn] + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotal += dd;
                                    BudgetTotal[10] += dd;
                                    dd = MBI.TrainingStipendsNF == null ? 0 : (decimal)MBI.TrainingStipendsNF;
                                    bsaf.SetField("B" + cnn.ToString() + " Training Stipends", "$" + string.Format("{0:#,0.00}", dd));
                                    ColumnTotalNonFederal += dd;
                                    BudgetTotal[23] += dd;

                                    //Total Costs
                                    bsaf.SetField("12" + affix[bsidx - cnn] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", ColumnTotal));
                                    BudgetTotal[11] += ColumnTotal;

                                    bsaf.SetField("B" + cnn.ToString() + " Total Costs Lines", "$" + string.Format("{0:#,0.00}", ColumnTotalNonFederal));
                                    BudgetTotal[24] += ColumnTotalNonFederal;
                                }
                            }

                            bsidx += 2;
                            cnn++;
                        }

                        //Total expense
                        bsidx = 6;
                        bsaf.SetField("1" + affix[bsidx - 1] + " Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[0]));
                        bsaf.SetField("2" + affix[bsidx - 1] + " Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[1]));
                        bsaf.SetField("3" + affix[bsidx - 1] + " Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[2]));
                        bsaf.SetField("4" + affix[bsidx - 1] + " Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[3]));
                        bsaf.SetField("5" + affix[bsidx - 1] + " Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[4]));
                        bsaf.SetField("6" + affix[bsidx - 1] + " Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[5]));
                        bsaf.SetField("7" + affix[bsidx - 1] + " Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[6]));
                        bsaf.SetField("8" + affix[bsidx - 1] + " Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[7]));
                        bsaf.SetField("9" + affix[bsidx - 1] + " Total Direct Costs lines 18", "$" + string.Format("{0:#,0.00}", BudgetTotal[8]));
                        bsaf.SetField("10" + affix[bsidx - 1] + " Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[9]));
                        bsaf.SetField("11" + affix[bsidx - 1] + " Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[10]));
                        bsaf.SetField("12" + affix[bsidx - 1] + " Total Costs lines 911", "$" + string.Format("{0:#,0.00}", BudgetTotal[11]));

                        //Indirect Cost Information from lastest cover sheet
                        bsaf.SetField("B6 Personnel", "$" + string.Format("{0:#,0.00}", BudgetTotal[13]));
                        bsaf.SetField("B6 Fringe Benefits", "$" + string.Format("{0:#,0.00}", BudgetTotal[14]));
                        bsaf.SetField("B6 Travel", "$" + string.Format("{0:#,0.00}", BudgetTotal[15]));
                        bsaf.SetField("B6 Equipment", "$" + string.Format("{0:#,0.00}", BudgetTotal[16]));
                        bsaf.SetField("B6 Supplies", "$" + string.Format("{0:#,0.00}", BudgetTotal[17]));
                        bsaf.SetField("B6 Contractual", "$" + string.Format("{0:#,0.00}", BudgetTotal[18]));
                        bsaf.SetField("B6 Construction", "$" + string.Format("{0:#,0.00}", BudgetTotal[19]));
                        bsaf.SetField("B6 Other", "$" + string.Format("{0:#,0.00}", BudgetTotal[20]));
                        bsaf.SetField("B6 Total Direct Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[21]));
                        bsaf.SetField("B6 Indirect Costs", "$" + string.Format("{0:#,0.00}", BudgetTotal[22]));
                        bsaf.SetField("B6 Training Stipends", "$" + string.Format("{0:#,0.00}", BudgetTotal[23]));
                        bsaf.SetField("B6 Total Costs Lines", "$" + string.Format("{0:#,0.00}", BudgetTotal[24]));

                        //Curent report period?
                        var bddata = MagnetBudgetInformation.Find(x => x.GranteeReportID == report.ID);
                        if (bddata.Count > 0)
                        {
                            MagnetBudgetInformation bdMBI = bddata[0];

                            if (bdMBI.IndirectCostAgreement != null)
                            {
                                if ((bool)bdMBI.IndirectCostAgreement)
                                {
                                    bsaf.SetField("AgreementYes", "Yes");
                                    if (!string.IsNullOrEmpty(bdMBI.AgreementFrom))
                                    {
                                        string[] strs = bdMBI.AgreementFrom.Split('/');
                                        bsaf.SetField("PeriodFrom1", strs[0]);
                                        bsaf.SetField("PeriodFrom2", strs.Count() >= 2 ? strs[1] : "");
                                        bsaf.SetField("PeriodFrom3", strs.Count() >= 3 ? strs[2] : "");
                                    }
                                    if (!string.IsNullOrEmpty(bdMBI.AgreementTo))
                                    {
                                        string[] strs = bdMBI.AgreementTo.Split('/');
                                        bsaf.SetField("PeriodTo1", strs[0]);
                                        bsaf.SetField("PeriodTo2", strs.Count() >= 2 ? strs[1] : "");
                                        bsaf.SetField("PeriodTo3", strs.Count() >= 3 ? strs[2] : "");
                                    }
                                    if (bdMBI.ApprovalAgency != null)
                                    {
                                        if ((bool)bdMBI.ApprovalAgency)
                                            bsaf.SetField("Ed", "Yes");
                                        else
                                        {
                                            bsaf.SetField("Other", "Yes");
                                            bsaf.SetField("Other approving agency", bdMBI.OtherAgency);
                                        }
                                    }
                                    bsaf.SetField("Indirect Cost Rate Percentage", Convert.ToString(bdMBI.IndirectCostRate));
                                }
                                else
                                    bsaf.SetField("AgreementNo", "Yes");
                            }

                            if (bdMBI.RestrictedRateProgram != null)
                            {
                                if (!(bool)bdMBI.na)
                                {
                                    if ((bool)bdMBI.RestrictedRateProgram)
                                        bsaf.SetField("Is it included in ICRA", "Yes");
                                    else
                                        bsaf.SetField("Complies with 34 CFR", "Yes");
                                }
                            }
                            bsaf.SetField("Restricted Indirect Cost Rate Percentage", Convert.ToString(bdMBI.RestrictedRate));
                        }

                        bsaf.SetField("InstitutionOrganization", grantee.GranteeName);

                        bsps.FormFlattening = true;

                        bsr.Close();
                        bsps.Close();

                        //Add to final report
                        PdfReader bsreader = new PdfReader(TmpPDF);
                        doc.SetPageSize(PageSize.A4.Rotate());
                        doc.NewPage();
                        PdfImportedPage bsimportedPage = pdfWriter.GetImportedPage(bsreader, 1);
                        pdfContentByte.AddTemplate(bsimportedPage, 24, -13);
                        doc.NewPage();
                        PdfImportedPage bsimportedPage2 = pdfWriter.GetImportedPage(bsreader, 2);
                        pdfContentByte.AddTemplate(bsimportedPage2, 24, -20);

                        doc.SetPageSize(PageSize.A4.Rotate());
                        doc.NewPage();
                        PdfImportedPage bsimportedPage3 = pdfWriter.GetImportedPage(bsreader, 3);
                        pdfContentByte.AddTemplate(bsimportedPage3, 0, -10);
                        bsreader.Close();

                        MagnetBudgetInformation MBIf = null;
                        var reportsf = GranteeReport.Find(x => x.GranteeID == report.GranteeID && x.ReportPeriodID == report.ReportPeriodID);
                        var dataf = MagnetBudgetInformation.Find(x => x.GranteeReportID == reportsf[0].ID);
                        if (dataf.Count > 0)
                            MBIf = dataf[0];

                        ReportID = Convert.ToInt32(reportsf[0].ID);

                        if (MBIf != null)
                        {
                            summary = MBIf.BudgetSummary;
                            itemized = MBIf.BudgetItemized;
                        }

                        if (!string.IsNullOrEmpty(summary))
                        {
                            //Add budget summary
                            string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(ContentPDF))
                                File.Delete(ContentPDF);
                            TempFiles.Add(ContentPDF);

                            Document bsContentDocument = new Document(PageSize.A4, 10f, 10f, 20f, 20f);
                            PdfWriter bsContentWriter = PdfWriter.GetInstance(bsContentDocument, new FileStream(ContentPDF, FileMode.Create));
                            bsContentDocument.Open();
                            //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                            bsContentDocument.Add(new Paragraph(new Chunk("Budget Narrative", Time12Bold)));
                            bsContentDocument.Add(new Paragraph(new Chunk(summary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                            bsContentDocument.Close();


                            //Add to output
                            PdfReader bsfreader2 = new PdfReader(ContentPDF);
                            for (int j = 1; j <= bsfreader2.NumberOfPages; j++)
                            {
                                doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage bsfimportedPage = pdfWriter.GetImportedPage(bsfreader2, j);
                                //pdfContentByte.AddTemplate(bsfimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                pdfContentByte.AddTemplate(bsfimportedPage, 0, 0);
                            }
                            bsfreader2.Close();
                        }
                        // upload budget summary
                        doc.SetPageSize(PageSize.A4);
                        foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && (x.FormID == 13)))
                        {
                            PdfReader bsuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                            for (int t = 1; t <= bsuploadReader.NumberOfPages; t++)
                            {
                                width = bsuploadReader.GetPageSize(t).Width;
                                height = bsuploadReader.GetPageSize(t).Height;
                                if (width > height)
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                else
                                    doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage bsuploadPage = pdfWriter.GetImportedPage(bsuploadReader, t);
                                pdfContentByte.AddTemplate(bsuploadPage, 0, 0);
                            }
                            bsuploadReader.Close();
                        }

                        if (!string.IsNullOrEmpty(itemized))
                        {
                            //Add budget summary
                            string ContentPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
                            if (File.Exists(ContentPDF))
                                File.Delete(ContentPDF);
                            TempFiles.Add(ContentPDF);

                            Document bsContentDocument = new Document(PageSize.A4, 10f, 10f, 20f, 20f);
                            PdfWriter bsContentWriter = PdfWriter.GetInstance(bsContentDocument, new FileStream(ContentPDF, FileMode.Create));
                            bsContentDocument.Open();
                            //ContentDocument.SetPageSize(PageSize.A4.Rotate());
                            bsContentDocument.Add(new Paragraph(new Chunk("Budget Itemized", Time12Bold)));
                            bsContentDocument.Add(new Paragraph(new Chunk(itemized, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, (float)10))));
                            bsContentDocument.Close();


                            //Add to output
                            PdfReader bsfreader2 = new PdfReader(ContentPDF);
                            for (int j = 1; j <= bsfreader2.NumberOfPages; j++)
                            {
                                doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage bsfimportedPage = pdfWriter.GetImportedPage(bsfreader2, j);
                                //pdfContentByte.AddTemplate(bsfimportedPage, 0, -1.0F, 1.0F, 0, 0, PageSize.A4.Width);
                                pdfContentByte.AddTemplate(bsfimportedPage, 0, 0);
                            }
                            bsfreader2.Close();
                        }

                        //Uploaded budget itemized file
                        var reportPeriods = MagnetReportPeriod.Find(x => x.isReportPeriod == true);
                        if (reportPeriods.Count == 0)
                        {
                            reportPeriods = MagnetReportPeriod.Find(x => x.isActive == true);
                        }
                        cohortType = Convert.ToInt32(reportPeriods.Last().CohortType);

                        if (cohortType > 1) //new rule started from Cohort 2013 | Year 1 Ad Hoc
                        {
                            doc.SetPageSize(PageSize.A4);
                            foreach (MagnetUpload UploadedFile in MagnetUpload.Find(x => x.ProjectID == ReportID && (x.FormID == 14)))
                            {
                                PdfReader bsuploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                                for (int t = 1; t <= bsuploadReader.NumberOfPages; t++)
                                {
                                    width = bsuploadReader.GetPageSize(t).Width;
                                    height = bsuploadReader.GetPageSize(t).Height;
                                    if (width > height)
                                        doc.SetPageSize(PageSize.A4.Rotate());
                                    else
                                        doc.SetPageSize(PageSize.A4);
                                    doc.NewPage();
                                    PdfImportedPage bsuploadPage = pdfWriter.GetImportedPage(bsuploadReader, t);
                                    pdfContentByte.AddTemplate(bsuploadPage, 0, 0);
                                }
                                bsuploadReader.Close();
                            }
                        }

                    }

                    //Ed approved file
                    var approvedFiles = MagnetUpload.Find(x => x.ProjectID == ReportID && x.FormID == 20);
                    if (approvedFiles.Count > 0)
                    {
                        doc.SetMargins(40f, 20f, 170f, 30f);
                        PdfDestination uploadDest = new PdfDestination(PdfDestination.FITBH);
                        PdfAction.GotoLocalPage(pdfWriter.PageNumber + 1, uploadDest, pdfWriter);
                        PdfOutline uploadOutline = new PdfOutline(root, uploadDest, "Uploaded Files");

                        foreach (MagnetUpload UploadedFile in approvedFiles)
                        {
                            PdfReader EDApproveduploadReader = new PdfReader(Server.MapPath("") + "/../upload/" + UploadedFile.PhysicalName);
                            if(EDApproveduploadReader.AcroForm !=null)
                                EDApproveduploadReader = new PdfReader(FlattenPdfFormToBytes(EDApproveduploadReader));
                             
                            for (int t = 1; t <= EDApproveduploadReader.NumberOfPages; t++)
                            {
                                width = EDApproveduploadReader.GetPageSize(t).Width;
                                height = EDApproveduploadReader.GetPageSize(t).Height;
                                if (width > height)
                                    doc.SetPageSize(PageSize.A4.Rotate());
                                else
                                    doc.SetPageSize(PageSize.A4);
                                doc.NewPage();
                                PdfImportedPage EDApproveduploadPage = pdfWriter.GetImportedPage(EDApproveduploadReader, t);
                                pdfContentByte.AddTemplate(EDApproveduploadPage, 0, -20);
                            }
                            EDApproveduploadReader.Close();
                        }
                    }

                    doc.Close();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hfTokenID.Value)); //downloadTokenValue will have been provided in the form submit via the hidden input field
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_FinalReport.pdf");

                    Response.BinaryWrite(output.ToArray());
                    Response.OutputStream.Flush();
                    Response.OutputStream.Close();
                }
            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("Combine report:", ex);

                foreach (string FileName in TempFiles)
                {
                    if (File.Exists(FileName))
                        File.Delete(FileName);
                }

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Download a final report is failed.Please contact administrator to solve this problem. ( " + DateTime.Now + " )');</script>", false);

            }
            finally
            {
                foreach (string FileName in TempFiles)
                {
                    if (File.Exists(FileName))
                        File.Delete(FileName);
                }
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "PastReportPDF", "<script>alert('You have to select a report year and a grantee from drop-down lists.');</script>", false);
        }
    }

    private byte[] FlattenPdfFormToBytes(PdfReader reader)
    {
        var memStream = new MemoryStream();
        var stamper = new PdfStamper(reader, memStream) { FormFlattening = true };
        stamper.Close();
        return memStream.ToArray();
    }

    protected void OnNotify(object sender, EventArgs e)
    {
        if (DropDownList3.SelectedIndex > 0)
        {
            Session["NdfyEDgranteeid"] = DropDownList3.SelectedValue;
            Response.Redirect("NotifyEDconfirm.aspx");
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "PastReportPDF", "<script>alert('You have to select a grantee from a drop-down list.');</script>", false);
        }
    }

    protected void ExportExcelReport(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.IsInRole("Administrators"))
        {
            using (System.IO.MemoryStream output = new System.IO.MemoryStream())
            {
                Workbook workbook = new Workbook();

                int row = 0;

                Worksheet worksheet = new Worksheet("Confirmation");

                worksheet.Cells[row, 0] = new Cell("GranteeName");
                worksheet.Cells[row, 1] = new Cell("UserName");
                worksheet.Cells[row, 2] = new Cell("ConfrimationNo");
                worksheet.Cells[row, 3] = new Cell("Date Created");
                //ReportType: true --- first time a report created
                foreach (EDConfirmation cfm in EDConfirmation.All())
                {
                    row++;
                    worksheet.Cells[row, 0] = new Cell(cfm.GranteeName);
                    worksheet.Cells[row, 1] = new Cell(cfm.UserName);
                    worksheet.Cells[row, 2] = new Cell(cfm.ConfirmNo);
                    worksheet.Cells[row, 3] = new Cell(cfm.Created, "mm/dd/yyyy");

                }
                workbook.Worksheets.Add(worksheet);
                workbook.Save(output);
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + "Confirmationlist.xls");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
                output.Close();
            }



        }
        else
            Response.Redirect("~/Login.aspx");
    }

    private void OutputHTMLexcels(StringBuilder htmlTable)
    {
        Response.Clear();
        Response.ContentType = "application/force-download";
        Response.AddHeader("content-disposition", "attachment; filename=Print.xls");
        Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        Response.Write("<head>");
        Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        Response.Write("<x:Name>Report Data</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        Response.Write(htmlTable); // give ur html string here
        Response.Write("</head>");
        Response.Flush();
        Response.End();
    }

    protected void rbtnlstCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        setAdminDropdownlist();
    }

    private void setAdminDropdownlist()
    {
        int adminCohorttype = Convert.ToInt32(rbtnlstCohort.SelectedValue);
        DropDownList6.Items.Clear();
        DropDownList5.Items.Clear();
        DropDownList5.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
        DropDownList6.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
        foreach (MagnetReportPeriod period in MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == adminCohorttype).OrderByDescending(x => x.ID))
        {
            DropDownList5.Items.Add(new System.Web.UI.WebControls.ListItem(period.Des, period.ID.ToString()));
        }


        //string Username = HttpContext.Current.User.Identity.Name;
        //var users = MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()));

        //if (users.Count > 0)
        //{
        //    hfID.Value = users[0].ID.ToString();
        //    bool setOnce = false;
        //    foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == users[0].ID))
        //    {
        //        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == adminCohorttype);
        //        string GranteeName = "";
        //        if (grantee != null)
        //        {
        //            if (!setOnce && adminCohorttype == 2)
        //            {
        //                rbtnlstCohort.Items[1].Enabled = true;
        //                setOnce = true;
        //            }
        //            var contacts = MagnetGranteeContact.Find(x => x.LinkID == district.GranteeID && x.ContactType == 1);
                    
        //            if (contacts!=null && (HttpContext.Current.User.IsInRole("Administrators") || Context.User.IsInRole("Data") || HttpContext.Current.User.IsInRole("ED")))
        //            {
        //                GranteeName = contacts.Count > 0 ? contacts[0].State + " " + grantee.GranteeName : grantee.GranteeName;
        //                if (!HttpContext.Current.User.IsInRole("ED"))
        //                {
        //                    ReportDiv.Visible = true;
        //                    NotifyDiv.Visible = true;
        //                    pnlPrints.Visible = true;
        //                }

        //                GranteeDiv.Visible = true;

        //            }
        //            else if (contacts != null && (HttpContext.Current.User.IsInRole("ProjectDirector") || HttpContext.Current.User.IsInRole("Evaluator")
        //            || HttpContext.Current.User.IsInRole("Project Director TWG") || HttpContext.Current.User.IsInRole("EvaluatorTWG")
        //            || HttpContext.Current.User.IsInRole("GrantStaff") || HttpContext.Current.User.IsInRole("ED")))
        //            {
        //                pnlPrints.Visible = true;
        //                GranteeName = grantee.GranteeName;
        //            }
        //            else
        //                GranteeName = grantee.GranteeName;

        //            System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem();
        //            item.Value = grantee.ID.ToString();
        //            item.Text = GranteeName;
        //            DropDownList6.Items.Add(item);
                  
        //        }

        //    }
        //    List<System.Web.UI.WebControls.ListItem> li = new List<System.Web.UI.WebControls.ListItem>();
        //    foreach (System.Web.UI.WebControls.ListItem list in DropDownList6.Items)
        //    {
        //        li.Add(list);
        //    }
        //    li.Sort((x, y) => string.Compare(x.Text, y.Text));
        //    DropDownList6.Items.Clear();
        //    DropDownList6.DataSource = li;
        //    DropDownList6.DataTextField = "Text";
        //    DropDownList6.DataValueField = "Value";
        //    DropDownList6.DataBind();
        //    DropDownList6.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Please select"));

        //}
    }

    protected void rblstPrintCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFinalPrintDropdownlist();
    }

    private void setFinalPrintDropdownlist()
    {
        int FinalPrintCohortType = Convert.ToInt32(rblstPrintCohort.SelectedValue);
        DropDownList1.Items.Clear();
        DropDownList2.Items.Clear();
        DropDownList1.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
        DropDownList2.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
        foreach (MagnetReportPeriod period in MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == FinalPrintCohortType).OrderByDescending(x => x.ID))
        {
            DropDownList2.Items.Add(new System.Web.UI.WebControls.ListItem(period.Des, period.ID.ToString()));
        }
        return;

        //string Username = HttpContext.Current.User.Identity.Name;
        //var users = MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()));

        //if (users.Count > 0)
        //{
        //    hfID.Value = users[0].ID.ToString();
        //    bool setOnce = false;
        //    foreach (MagnetGranteeUserDistrict district in MagnetGranteeUserDistrict.Find(x => x.GranteeUserID == users[0].ID))
        //    {
        //        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == district.GranteeID && x.CohortType == FinalPrintCohortType);
        //        string GranteeName = "";
        //        if (grantee != null)
        //        {
        //            if (!setOnce && FinalPrintCohortType == 2)
        //            {
        //                rblstPrintCohort.Items[1].Enabled = true;
        //                setOnce = true;
        //            }
        //            var contacts = MagnetGranteeContact.Find(x => x.LinkID == district.GranteeID && x.ContactType == 1);

        //            if (contacts != null && (HttpContext.Current.User.IsInRole("Administrators") || Context.User.IsInRole("Data") || HttpContext.Current.User.IsInRole("ED")))
        //            {
        //                GranteeName = contacts.Count > 0 ? contacts[0].State + " " + grantee.GranteeName : grantee.GranteeName;
        //                if (HttpContext.Current.User.IsInRole("ED"))
        //                {
        //                    ReportDiv.Visible = true;
        //                    NotifyDiv.Visible = true;
        //                    pnlPrints.Visible = true;
        //                }

        //                GranteeDiv.Visible = true;

        //            }
        //            else if (contacts != null && (HttpContext.Current.User.IsInRole("ProjectDirector") || HttpContext.Current.User.IsInRole("Evaluator")
        //           || HttpContext.Current.User.IsInRole("Project Director TWG") || HttpContext.Current.User.IsInRole("EvaluatorTWG")
        //           || HttpContext.Current.User.IsInRole("GrantStaff") || HttpContext.Current.User.IsInRole("ED")))
        //            {
        //                pnlPrints.Visible = true;
        //                GranteeName = grantee.GranteeName;
        //            }
        //            else
        //                GranteeName = grantee.GranteeName;

        //            System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem();
        //            item.Value = grantee.ID.ToString();
        //            item.Text = GranteeName;
        //            DropDownList1.Items.Add(item);

        //        }

        //    }
        //    List<System.Web.UI.WebControls.ListItem> li = new List<System.Web.UI.WebControls.ListItem>();
        //    foreach (System.Web.UI.WebControls.ListItem list in DropDownList1.Items)
        //    {
        //        li.Add(list);
        //    }
        //    li.Sort((x, y) => string.Compare(x.Text, y.Text));
        //    DropDownList1.Items.Clear();
        //    DropDownList1.DataSource = li;
        //    DropDownList1.DataTextField = "Text";
        //    DropDownList1.DataValueField = "Value";
        //    DropDownList1.DataBind();
        //    DropDownList1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Please select"));

        //}
    }
    protected void rbtnlstDownloadCohort_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList4.Items.Clear();

        DropDownList4.Items.Add(new System.Web.UI.WebControls.ListItem("Please select", ""));
        foreach (MagnetReportPeriod period in MagnetReportPeriod.Find(x => x.isActive == true && x.CohortType == Convert.ToInt32(rbtnlstDownloadCohort.SelectedValue)).OrderByDescending(x => x.ID))
        {
            DropDownList4.Items.Add(new System.Web.UI.WebControls.ListItem(period.Des, period.ID.ToString()));
        }
    }


}

