﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    U.S. Department of Education Grant Performance Report Cover Sheet (ED 524B)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        $(function () {
            $("input:submit").click(function () {
                $("#hfControlID").val($(this).attr("id"));
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        User Guide Documents</h4>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    	<p>
            Please choose the type of desegregation plan.
        </p>
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Type of Desegregation Plan</b>
    </span>
    <ContentTemplate>
    	<table style="padding-left:15px;margin-left:15px;line-height:150%;padding-top:15px;">
        	<tr>
               <td valign="top">
               	<asp:RadioButton ID="RadioButton1" runat="server" GroupName="plangroup" />
                A Required Plan <img src='../../images/question_mark-thumb.png' title='Attach the following:&lt;br /&gt;&lt;ul&gt;&lt;li&gt;A copy of the court 					or agency order that demonstrated that the magnet school(s) for which assistance is sought under the grant are a part of the approved plan.  NOTE: If the applicant is implementing a previously approved plan that does not include the magnet school(s) for which assistance is requested, the plan must be modified to include the new magnet school(s). The applicant must obtain approval of the new magnet schools, or any other modification to its desegregation plan, from the court, agency or official that originally approved the plan. The date by which proof of approval of any desegregation plan modification must be submitted to the US Department of Education is identified in the closing date notice.&lt;/li&gt;&lt;/ul&gt;' />
                     </td>
             </tr>
             <tr>
                     <td>
                     	<asp:RadioButton ID="RadioButton2" runat="server" GroupName="plangroup" /> A Voluntary Plan <img src='../../images/question_mark-thumb.png' title='Attach the following:<br /><ul><li>A copy of the plan.</li><li>A copy of the school board resolution adopting and implementing the plan, or agreeing to adopt and implement the plan upon the award of assistance.</li></ul>' />
                     </td>
        	</tr>
    	</table>
        <table width="100%">
        <tr align="right">
            <td width="30">
                <asp:ImageButton Width="29" Height="36" ID="ImageButton2" runat="server" ImageUrl="button_arrow.jpg"
                    />
            </td>
        </tr>
    </table>
    </ContentTemplate>
    
</asp:Content>
