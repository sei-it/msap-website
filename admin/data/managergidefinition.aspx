﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="managergidefinition.aspx.cs" Inherits="admin_data_managedefinition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage MGI Objective
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h1>
            <a href="datareport.aspx">Home</a> --> Manage MGI Definition</h1>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New MGI Definition" CssClass="msapBtn" OnClick="OnAddData" />
        </p>
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="School Name" />
                <asp:BoundField DataField="RGIDefinition" SortExpression="" HeaderText="MGI Definition" />
                <asp:BoundField DataField="Notes" SortExpression="" HeaderText="Notes" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
</asp:Content>

