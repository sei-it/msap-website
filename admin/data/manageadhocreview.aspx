﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="manageadhocreview.aspx.cs" Inherits="admin_data_manageadhocreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage Ad Hoc Review
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h1>
            <a href="datareport.aspx">Home</a> --> Manage Ad Hoc Review</h1>
        <p style="text-align: left">
            <asp:Button ID="Newbutton" runat="server" Text="New Ad Hoc Review Report" CssClass="msapBtn" OnClick="OnAddData" />
        </p>
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:BoundField DataField="Notes1" SortExpression="" HeaderText="Notes 1" />
                <asp:BoundField DataField="Notes2" SortExpression="" HeaderText="Notes 2" />
                <asp:BoundField DataField="Notes3" SortExpression="" HeaderText="Notes 3" />
                <asp:BoundField DataField="Notes4" SortExpression="" HeaderText="Notes 4" />
                <asp:BoundField DataField="Notes5" SortExpression="" HeaderText="Notes 5" />
                <asp:BoundField DataField="Notes6" SortExpression="" HeaderText="Notes 6" />
                <asp:BoundField DataField="GeneralNotes" SortExpression="" HeaderText="General Notes" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
</asp:Content>

