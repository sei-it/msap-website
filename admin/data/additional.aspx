﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="additional.aspx.cs" Inherits="admin_data_additional" ErrorPage="~/Error_page.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP center - Upload additional documents
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
    </style>
    <script type="text/javascript">
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {
                var who = document.getElementsByName('<%= FileUpload1.UniqueID %>')[0];
                who.value = "";

                var who2 = who.cloneNode(false);
                who2.onchange = who.onchange;
                who.parentNode.replaceChild(who2, who);

                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Additional Information</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <div class="titles_noTabs">
        Additional Information</div>
    <br />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <p class="clear">
        Please upload any additional information you would like to include.</p>
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Upload Documents</b></span>
    <table width="100%">
        <tr>
            <td colspan="2">
               Use the Browse button to find and upload documents. These must be in PDF format or MAPS will not accept them. </td>
        </tr>
        <tr>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="msapDataTxt" />
                <asp:Button ID="Button1" runat="server" Text="Save & Upload" CssClass="surveyBtn1" OnClick="OnUpload" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl" GridLines="None">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    <table class="DotnetTbl">
                        <tr>
                            <td>
                                <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                            </td>
                            <td>
                                Uploaded Files
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Eval("FileNames")%></font>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <table class="DotnetTbl">
                        <tr>
                            <td>
                                <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                            </td>
                            <td>
                                Form
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <font color="#4e8396">
                        <%# Eval("FileType")%></font>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete"
                        CausesValidation="false" CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you sure you want to delete this uploaded file?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
