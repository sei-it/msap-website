﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="reportcoversheet4.aspx.cs" Inherits="admin_reportcoversheet4" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    U.S. Department of Education Grant Performance Report Cover Sheet (ED 524B)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {
                var who = document.getElementsByName('<%= FileUpload1.UniqueID %>')[0];
                who.value = "";

                var who2 = who.cloneNode(false);
                who2.onchange = who.onchange;
                who.parentNode.replaceChild(who2, who);

                alert('You can upload files with the following extension only:\n.pdf.');
                return false;
            }
            else {
                return true;
            }
        }
        function checkWordLen(obj, wordLen, cid) {
            var len = obj.value.split(/[\s]+/);
            $("#" + cid).val(len.length.toString());
            if (len.length > wordLen) {
                alert("You've exceeded the " + wordLen + " word limit for this field!");
                obj.value = obj.SavedValue;
                //obj.value = obj.value.substring(0, wordLen - 1);
                len = obj.value.split(/[\s]+/);
                $("#" + cid).val(len.length.toString());
                return false;
            } else {
                obj.SavedValue = obj.value;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Performance Report Cover Sheet (ED 524B)</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
    <br />

    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfFileID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <asp:HiddenField ID="hfControlID" runat="server" ClientIDMode="Static" />
    <br />
    <div class="tab_area">
        <div class="titles">
            Performance Report Cover Sheet (ED 524B) - Page 4 of 4</div>
        <ul class="tabs">
            <li class="tab_active">Page 4</li>
            <li><a href="reportcoversheet3.aspx">Page 3</a></li>
            <li><a href="reportcoversheet2.aspx">Page 2</a></li>
            <li><a href="reportcoversheet1.aspx">Page 1</a></li>
        </ul>
    </div>
    <br />
    <table width="100%">
        <tr>
            <td>
                Please print and upload your signed cover sheet. 
                <p>
                    <asp:Button ID="Button2" runat="server" Text="Print" CssClass="surveyBtn postbutton"
                        OnClick="OnPrint" ToolTip="Print" />
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="msapDataTxt" />&nbsp;&nbsp;
                <asp:Button ID="UploadBtn" CssClass="surveyBtn1 postbutton" runat="server" Text="Save & Upload"
                    OnClick="OnUpload" />
            </td>
        </tr>
        <tr id="fileRow" runat="server" visible="false">
            <td colspan="2">
                <table class="msapDataTbl" style="width: 300px;">
                    <tr>
                        <th colspan="2">
                            Uploaded File
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <a id="UploadedFile" target="_blank" runat="server">
                                <asp:Label ID="lblFile" runat="server"></asp:Label></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="Button3" runat="server" CssClass="postbutton" Text="Delete" OnClick="OnDelete"
                             OnClientClick="return confirm('Are you sure you want to delete this uploaded file?');" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton Width="29" Height="36" ID="ImageButton1" runat="server" ImageUrl="button_arrow2.jpg"
                                OnClick="OnPrevious" CssClass="postbutton" ToolTip="Previous" Visible="false" />
                        </td>
                        <td style="padding-top: 18px;">
                            <asp:Button ID="Button1" runat="server" Text="Save Record" CssClass="surveyBtn1 postbutton"
                                OnClick="OnSaveData" Visible="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="text-align: right; margin-top: 20px;">
        <a href="reportcoversheet1.aspx">Page 1</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="reportcoversheet2.aspx">
            Page 2</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="reportcoversheet3.aspx">Page 3</a>&nbsp;&nbsp;&nbsp;&nbsp;
        Page 4
    </div>
</asp:Content>
