﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_personnelbudget : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/default.aspx");
            }
            if(Request.QueryString["type"].Equals("1"))
            {
                hfReportType.Value = "1";
                lblBudgetType.Text = "Federal Funds";
            }else
            {
                hfReportType.Value = "2";
                lblBudgetType.Text = "Non Federal Funds";
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            var data = MagnetBudgetSummary.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.ReportType == Convert.ToInt32(hfReportType.Value));
            if (data.Count == 1)
            {
                hfSummaryID.Value = data[0].ID.ToString();
                txtSummary.Text = data[0].ReportSummary;
            }
            RegisterHelper();
            LoadData();
        }
    }
    private void RegisterHelper()
    {
        GridViewHelper helper = new GridViewHelper(this.GridView1);
        helper.RegisterSummary("ApprovedFederalFunds", SummaryOperation.Sum);
        helper.RegisterSummary("BudgetExpenditures", SummaryOperation.Sum);
        helper.RegisterSummary("Carryover", SummaryOperation.Sum);
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetPersonnelBudgets
                   where m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.ReportType == Convert.ToInt32(hfReportType.Value)
                   orderby m.PersonnelName
                   select new { m.ID, m.PersonnelName, m.PersonnelPosition, m.AnnualSalary, m.MSAPPercentage, m.ApprovedFederalFunds, 
                       BudgetExpenditures = (m.AnnualSalary != null?Convert.ToInt32(m.AnnualSalary * m.MSAPPercentage / 100):0), 
                       Carryover = (m.AnnualSalary!=null?Convert.ToInt32(m.ApprovedFederalFunds - m.AnnualSalary * m.MSAPPercentage / 100):0) };
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtPersonnel.Text = "";
        txtPosition.Text = "";
        txtAnnualSalary.Text = "";
        txtPercentage.Text = "";
        txtApprovedFunds.Text = "";
        hfID.Value = "";
    }
    protected void OnNewBudget(object sender, EventArgs e)
    {
        ClearFields();
        mpeWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetPersonnelBudget data = MagnetPersonnelBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtPersonnel.Text = data.PersonnelName;
        txtPosition.Text = data.PersonnelPosition;
        if (data.ApprovedFederalFunds != null) txtApprovedFunds.Text = Convert.ToString(data.ApprovedFederalFunds);
        if (data.AnnualSalary != null) txtAnnualSalary.Text = Convert.ToString(data.AnnualSalary);
        if (data.MSAPPercentage != null) txtPercentage.Text = Convert.ToString(data.MSAPPercentage);
        mpeWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetPersonnelBudget data = new MagnetPersonnelBudget();
        if(!string.IsNullOrEmpty(hfID.Value))
            data = MagnetPersonnelBudget.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
            data.ReportType = Convert.ToInt32(hfReportType.Value);
        }
        data.ApprovedFederalFunds = string.IsNullOrEmpty(txtApprovedFunds.Text) ? 0 : Convert.ToInt32(txtApprovedFunds.Text);

        data.PersonnelName = txtPersonnel.Text;
        data.PersonnelPosition = txtPosition.Text;
        data.AnnualSalary = string.IsNullOrEmpty(txtAnnualSalary.Text) ? 0 : Convert.ToDecimal(txtAnnualSalary.Text);
        data.MSAPPercentage = string.IsNullOrEmpty(txtPercentage.Text) ? 0 : Convert.ToInt32(txtPercentage.Text);
        data.Save();
        RegisterHelper();
        LoadData();
    }
    protected void OnSaveSummary(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSummary.Text))
        {
            //Save to database
            MagnetBudgetSummary summary = new MagnetBudgetSummary();
            if (!string.IsNullOrEmpty(hfSummaryID.Value))
                summary = MagnetBudgetSummary.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSummaryID.Value));
            else
            {
                summary.ReportID = Convert.ToInt32(hfReportID.Value);
                summary.ReportType = Convert.ToInt32(hfReportType.Value);
            }
            summary.ReportSummary = txtSummary.Text;
            summary.Save();
        }
    }
}