﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_report_feederenrollment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            hfStageID.Value = Request.QueryString["id"];

            int reportPeriod = 0;
            if (Session["ReportID"] != null)
            {
                reportPeriod = (int)Session["ReportID"];
                hfReportID.Value = reportPeriod.ToString();
            }
            else
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            hfGranteeID.Value = granteeID.ToString();

            //Feeder School
            foreach (MagnetFeederSchool school in MagnetFeederSchool.Find(x => x.GranteeID == Convert.ToInt32(hfGranteeID.Value)).OrderBy(x => x.SchoolName))
            {
                ddlFeederSchool.Items.Add(new System.Web.UI.WebControls.ListItem(school.SchoolName, school.ID.ToString()));
            }

            //Magnet School
            int idx = 0;
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == Convert.ToInt32(hfGranteeID.Value)).OrderBy(x => x.SchoolName))
            {
                idx++;
                cklMagnetSchool.Items.Add(new System.Web.UI.WebControls.ListItem(school.SchoolName, school.ID.ToString()));
            }
            if (idx > 1)
                cklMagnetSchool.Items.Add(new System.Web.UI.WebControls.ListItem("All", "999999"));

            LoadEnrollmentData();
        }
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetSchoolFeederEnrollment data = MagnetSchoolFeederEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        ddlFeederSchool.SelectedValue = data.FeederSchoolID.ToString();
        //ddlMagnetSchool.SelectedValue = data.SchoolID.ToString();
        if (!string.IsNullOrEmpty(data.SchoolID))
        {
            foreach (string str in data.SchoolID.Split(';'))
            {
                foreach (System.Web.UI.WebControls.ListItem item in cklMagnetSchool.Items)
                {
                    if (item.Value.Equals(str))
                        item.Selected = true;
                }
            }
        }
        txtAmericanIndian.Text = data.AmericanIndian.ToString();
        txtAsian.Text = data.Asian.ToString();
        txtBlack.Text = data.AfricanAmerican.ToString();
        txtHispanic.Text = data.Hispanic.ToString();
        txtWhite.Text = data.White.ToString();
        txtHawaiian.Text = data.Hawaiian.ToString();
        txtMultiRaces.Text = data.MultiRacial.ToString();
        mpeResourceWindow.Show();
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetSchoolFeederEnrollment.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadEnrollmentData();
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        MagnetSchoolFeederEnrollment data = new MagnetSchoolFeederEnrollment();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetSchoolFeederEnrollment.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            data.GranteeReportID = Convert.ToInt32(hfReportID.Value);
            data.StageID = Convert.ToInt32(hfStageID.Value);
        }
        data.FeederSchoolID = Convert.ToInt32(ddlFeederSchool.SelectedValue);
        string cklSelection = "";
        foreach (System.Web.UI.WebControls.ListItem item in cklMagnetSchool.Items)
        {
            if (item.Selected)
            {
                cklSelection += string.IsNullOrEmpty(cklSelection) ? item.Value : ";" + item.Value;
            }
        }
        data.SchoolID = cklSelection; //Convert.ToInt32(ddlMagnetSchool.SelectedValue);
        data.AmericanIndian = string.IsNullOrEmpty(txtAmericanIndian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtAmericanIndian.Text));
        data.Asian = string.IsNullOrEmpty(txtAsian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtAsian.Text));
        data.AfricanAmerican = string.IsNullOrEmpty(txtBlack.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtBlack.Text));
        data.Hispanic = string.IsNullOrEmpty(txtHispanic.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtHispanic.Text));
        data.White = string.IsNullOrEmpty(txtWhite.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtWhite.Text));
        data.Hawaiian = string.IsNullOrEmpty(txtHawaiian.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtHawaiian.Text));
        data.MultiRacial = string.IsNullOrEmpty(txtMultiRaces.Text) ? 0 : Convert.ToInt32(ManageUtility.FormatIntegerString(txtMultiRaces.Text));
        data.Save();
        LoadEnrollmentData();
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeResourceWindow.Show();
    }
    private void ClearFields()
    {
        hfID.Value = "";
        ddlFeederSchool.SelectedIndex = 0;
        foreach (System.Web.UI.WebControls.ListItem item in cklMagnetSchool.Items)
        {
            item.Selected = false;
        }
        txtAmericanIndian.Text = "";
        txtAsian.Text = "";
        txtBlack.Text = "";
        txtHispanic.Text = "";
        txtWhite.Text = "";
        txtHawaiian.Text = "";
        txtMultiRaces.Text = "";
        lblMessage.Text = "";
    }
    public class EnrollmentDataClass
    {
        public int ID { set; get; }
        public string MagnetSchool { get; set; }
        public string FeederSchool { get; set; }
        public string MagnetSchoolIDs { get; set; }
        public int FeederSchoolID { get; set; }
        public int AmericanIndian { get; set; }
        public int Asian { get; set; }
        public int AfricanAmerican { get; set; }
        public int Hispanic { get; set; }
        public int White { get; set; }
        public int Hawaiian { get; set; }
        public int MultiRacial { get; set; }
    }
    private void LoadEnrollmentData()
    {
        List<EnrollmentDataClass> data = new List<EnrollmentDataClass>();
        foreach (MagnetSchoolFeederEnrollment enrollmentData in MagnetSchoolFeederEnrollment.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                     && x.StageID == Convert.ToInt32(hfStageID.Value)))
        {
            EnrollmentDataClass tmp = new EnrollmentDataClass
            {
                ID = enrollmentData.ID,
                FeederSchoolID = (int)enrollmentData.FeederSchoolID,
                MagnetSchoolIDs = (string)enrollmentData.SchoolID,
                AmericanIndian = (int)enrollmentData.AmericanIndian,
                Asian = (int)enrollmentData.Asian,
                AfricanAmerican = (int)enrollmentData.AfricanAmerican,
                Hispanic = (int)enrollmentData.Hispanic,
                White = (int)enrollmentData.White,
                Hawaiian = (int)enrollmentData.Hawaiian,
                MultiRacial = (int)enrollmentData.MultiRacial
            };

            if (!string.IsNullOrEmpty(tmp.MagnetSchoolIDs))
            {
                foreach (string str in tmp.MagnetSchoolIDs.Split(';'))
                {
                    if (!string.Equals(str, "999999"))
                        tmp.MagnetSchool += string.IsNullOrEmpty(tmp.MagnetSchool) ? MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(str)).SchoolName :  ", " + MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(str)).SchoolName; //"<br><br>";
                    else
                        tmp.MagnetSchool = "All";
                }
            }
            if (tmp.FeederSchoolID != null)
            {
                tmp.FeederSchool = MagnetFeederSchool.SingleOrDefault(x => x.ID == tmp.FeederSchoolID).SchoolName;
            }
            data.Add(tmp);
        }

        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    protected void OnMagnetSchoolChanged(object sender, EventArgs e)
    {
        if (cklMagnetSchool.Items[cklMagnetSchool.Items.Count - 1].Selected)
        {
            //Last one, all school
            foreach (System.Web.UI.WebControls.ListItem item in cklMagnetSchool.Items)
            {
                if (item.Selected && !item.Value.Equals("999999"))
                    item.Selected = false;
            }
        }
    }
    protected void OnPrint(object sender, EventArgs e)
    {
        using (System.IO.MemoryStream Output = new MemoryStream())
        {
            //Print
            Document document = new Document(PageSize.A4, 10, 10, 40, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, Output);
            document.Open();
            PdfContentByte pdfContentByte = pdfWriter.DirectContent;

            //Create font
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font Time12Bold = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font Time10BoldItalic = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLDITALIC);
            iTextSharp.text.Font Time10Bold = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font Time10Normal = new iTextSharp.text.Font(bfTimes, 9, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font Time8Normal = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL);

            iTextSharp.text.Rectangle page = document.PageSize;
            PdfPTable head = new PdfPTable(2);
            head.TotalWidth = 100;
            head.SetWidths(new float[] { 10f, 90f });
            Phrase phrase = new Phrase("Table 11", Time12Bold);
            PdfPCell c = new PdfPCell(phrase);
            c.Colspan = 2;
            c.MinimumHeight = 40f;
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            head.AddCell(c);

            phrase = new Phrase("Feeder School-(Converted)-Enrollment Data ", Time10Bold);
            phrase.Add(new Chunk("(LEAs that HAVE converted to new race and ethnic categories)", Time10BoldItalic));
            c = new PdfPCell(phrase);
            c.Colspan = 2;
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_BOTTOM;
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            head.AddCell(c);

            phrase = new Phrase("•", Time10Bold);
            c = new PdfPCell(phrase);
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            head.AddCell(c);

            phrase = new Phrase("For each feeder school, identify the magnet school(s) to which the feeder school would send students.  If a feeder school would send students to all magnet schools at a particular grade level (for example, Elementary Feeder School “X” would send students to all of the elementary magnet schools participating in the project, indicate “All” in the “Magnet” column associated with Elementary Feeder School “X”.  ", Time10Normal);
            c = new PdfPCell(phrase);
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_TOP;
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            head.AddCell(c);

            phrase = new Phrase("Use additional sheets, if necessary.", Time10Normal);
            c = new PdfPCell(phrase);
            c.Colspan = 2;
            c.MinimumHeight = 20f;
            c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_BOTTOM;
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            head.AddCell(c);
            document.Add(head);

            //Data table
            PdfPTable DataTable = new PdfPTable(17);
            DataTable.TotalWidth = 70;
            DataTable.SetWidths(new float[] { 12f, 13f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f });

            phrase = new Phrase("Schools", Time10Bold);
            c = new PdfPCell(phrase);
            c.Colspan = 2;
            //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
            DataTable.AddCell(c);

            phrase = new Phrase("Actual Enrollment as of October 1, 2010 \r\n(Year 1 of Project)", Time10Bold);
            c = new PdfPCell(phrase);
            c.Colspan = 15;
            //c.Border = iTextSharp.text.Rectangle.NO_BORDER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("FEEDER", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("MAGNET(S)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("American Indian /Alaskan Native (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("American Indian /Alaskan Native (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Asian (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Asian (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Black or African-American (Number) ", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Black or African-American (%) ", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Hispanic/Latino (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Hispanic/Latino (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander  (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Native Hawaiian or Other Pacific Islander (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("White (Number)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("White (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Two or more races (Number) ", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Two or more races (%)", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            c = new PdfPCell(new Phrase("Total  Students", Time10Bold));
            c.Rotation = 90;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.HorizontalAlignment = Element.ALIGN_BOTTOM;
            DataTable.AddCell(c);

            MagnetDBDB db = new MagnetDBDB();
            var data = from m in db.MagnetSchoolFeederEnrollments
                       //from n in db.MagnetSchools
                       //from p in db.MagnetFeederSchools
                       where m.GranteeReportID == Convert.ToInt32(hfReportID.Value)
                        && m.StageID == 3
                       //&& m.FeederSchoolID == p.ID
                       //&& m.SchoolID == n.ID
                       orderby m.GradeLevel
                       select new
                       {
                           m.ID,
                           //FeederSchool = p.SchoolName,
                           //MagnetSchool = n.SchoolName,
                           m.FeederSchoolID,
                           m.SchoolID,
                           m.AmericanIndian,
                           m.Asian,
                           m.AfricanAmerican,
                           m.Hispanic,
                           m.White,
                           m.Hawaiian,
                           m.MultiRacial,
                           GradeTotal = (m.AmericanIndian + m.Asian + m.Hispanic + m.Hawaiian + m.White + m.MultiRacial + m.AfricanAmerican)
                       };

            foreach (var Enrollment in data)
            {
                MagnetFeederSchool feederSchool = MagnetFeederSchool.SingleOrDefault(x => x.ID == (int)(Enrollment.FeederSchoolID));
                c = new PdfPCell(new Phrase(feederSchool.SchoolName, Time10Normal));
                c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                DataTable.AddCell(c);

                if (!string.IsNullOrEmpty(Enrollment.SchoolID))
                {
                    string strSchoolName = "";
                    if (Enrollment.SchoolID.Equals("999999"))
                    {
                        strSchoolName = "All";
                    }
                    else
                    {
                        foreach (string magnetSchoolID in Enrollment.SchoolID.Split(';'))
                        {
                            MagnetSchool magnetSchool = MagnetSchool.SingleOrDefault(x => x.ID == Convert.ToInt32(magnetSchoolID));
                            strSchoolName += string.IsNullOrEmpty(strSchoolName) ? magnetSchool.SchoolName : "\r\n" + magnetSchool.SchoolName;
                        }
                    }
                    c = new PdfPCell(new Phrase(strSchoolName, Time10Normal));
                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                    DataTable.AddCell(c);
                }
                else
                {
                    c = new PdfPCell(new Phrase("", Time10Normal));
                    c.BackgroundColor = iTextSharp.text.BaseColor.GRAY;
                    DataTable.AddCell(c);
                }

                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.AmericanIndian), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.AmericanIndian / Enrollment.GradeTotal * 100), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Asian), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.Asian / Enrollment.GradeTotal * 100), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.AfricanAmerican), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.AfricanAmerican / Enrollment.GradeTotal * 100), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Hispanic), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.Hispanic / Enrollment.GradeTotal * 100), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.Hawaiian), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.Hawaiian / Enrollment.GradeTotal * 100), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.White), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.White / Enrollment.GradeTotal * 100), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.MultiRacial), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(String.Format("{0:,0.00}", (double)Enrollment.MultiRacial / Enrollment.GradeTotal * 100), Time8Normal)));
                DataTable.AddCell(new PdfPCell(new Phrase(ManageUtility.FormatInteger((int)Enrollment.GradeTotal), Time8Normal)));
            }

            document.Add(DataTable);

            //Close document
            document.Close();

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + grantee.GranteeName.Replace(' ', '_') + "_Table_11.pdf");
            Response.OutputStream.Write(Output.GetBuffer(), 0, Output.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            Output.Close();
        }
    }
}