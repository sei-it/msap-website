﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="GPRA.aspx.cs" Inherits="admin_data_gpra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit GPRA
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h1>
            Add/Edit GPRA</h1>
        <asp:Button ID="Button3" runat="server" CssClass="msapBtn" Text="Return" CausesValidation="false"
            OnClick="OnReturn" />
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfApplicationPool" runat="server" />
        <asp:HiddenField ID="hfEnrollment" runat="server" />
        <asp:HiddenField ID="hfParticipationReading" runat="server" />
        <asp:HiddenField ID="hfParticipationMath" runat="server" />
        <asp:HiddenField ID="hfArchivementReading" runat="server" />
        <asp:HiddenField ID="hfArchivementMath" runat="server" />
        <asp:HiddenField ID="hfPerformanceMeasure" runat="server" />
        <asp:HiddenField ID="hfTypeID" runat="server" />
        <asp:Panel ID="Panel1" runat="server" GroupingText="Part I. School demographic data"
            Style="margin-top: 20px;">
            <table class="buttonTbl">
                <tr>
                    <td>
                        School Name:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlSchool"
                            ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Grades:
                    </td>
                    <td>
                        <asp:CheckBoxList ID="cblGrades" runat="server" RepeatColumns="3">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Magnet program type:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlProgramType" runat="server" CssClass="msapDataTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="0">Whole-school magnet program</asp:ListItem>
                            <asp:ListItem Value="1">Partial magnet program</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlProgramType"
                            ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Title I school funding:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTitleISchoolFunding" runat="server" CssClass="msapDataTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="1">Title I-funded school</asp:ListItem>
                            <asp:ListItem Value="0">Not Title I-funded school</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlTitleISchoolFunding"
                            ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Are you in Title I school improvement?
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTitleISchoolImprovement" runat="server" CssClass="msapDataTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        What is your Title I school improvement status?
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlImprovementStatus" runat="server" CssClass="msapDataTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="1">Improvement</asp:ListItem>
                            <asp:ListItem Value="2">Corrective action</asp:ListItem>
                            <asp:ListItem Value="3">Restructuring</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Has this school been identified by your state as a persistently lowest-achieving
                        school?
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLowestArchieving" runat="server" CssClass="msapDataTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Does this school receive School Improvement Grant (SIG) funds?
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSIGFunds" runat="server" CssClass="msapDataTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server" GroupingText="Part II. GPRA Performance Measure 1 data"
            Style="margin-top: 20px; padding-top: 15px;">
            <table class="buttonTbl">
                <tr>
                    <td width="20%">
                        <asp:Button ID="Button1" runat="server" CssClass="msapBtn" Text="Applicant pool data"
                            CausesValidation="false" OnClick="OnApplicantPool" />
                    </td>
                    <td>
                        <asp:Button ID="Button4" runat="server" CssClass="msapBtn" Text="Enrollment data"
                            CausesValidation="false" OnClick="OnEnrollment" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="Panel3" runat="server" GroupingText="Part III. GPRA Performance Measures 2 and 3 data"
            Style="margin-top: 20px;">
            <table class="buttonTbl">
                <tr>
                    <td width="35%">
                        Adequate yearly progress participation data
                    </td>
                    <td width="15%">
                        <asp:Button ID="Button5" runat="server" CssClass="msapBtn" Text="Reading" OnClick="OnParticipationReading"
                            CausesValidation="false" />
                    </td>
                    <td>
                        <asp:Button ID="Button6" runat="server" CssClass="msapBtn" Text="Mathematics" OnClick="OnParticipationMath"
                            CausesValidation="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Adequate yearly progress achievement data
                    </td>
                    <td>
                        <asp:Button ID="Button7" runat="server" CssClass="msapBtn" Text="Reading" OnClick="OnArchievementReading"
                            CausesValidation="false" />
                    </td>
                    <td>
                        <asp:Button ID="Button8" runat="server" CssClass="msapBtn" Text="Mathematics" OnClick="OnArchievementMath"
                            CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="Panel4" runat="server" GroupingText="Part IV. GPRA Performance Measure 6 data"
            Style="margin-top: 20px;">
            <table class="buttonTbl">
                <tr>
                    <td>
                        <asp:Button ID="Button9" runat="server" CssClass="msapBtn" Text="Performance Measure 6 Data" OnClick="OnData"
                            CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <%-- Budget --%>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDiv">
                <div class="mpeDivHeader">
                    Add/Edit
                    <asp:Label ID="lblTitle" runat="server"></asp:Label></div>
                <table>
                    <tr>
                        <td>
                            American Indian or Alaska Native:
                        </td>
                        <td>
                            <asp:TextBox ID="txtIndian" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers"
                                TargetControlID="txtIndian">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Asian:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAsian" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                TargetControlID="txtAsian">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Black or African American:
                        </td>
                        <td>
                            <asp:TextBox ID="txtBlack" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtBlack">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Hispanic or Latino:
                        </td>
                        <td>
                            <asp:TextBox ID="txtHispanic" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                TargetControlID="txtHispanic">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Native Hawaiian or Other Pacific Islander:
                        </td>
                        <td>
                            <asp:TextBox ID="txtHawaiian" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                                TargetControlID="txtHawaiian">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            White:
                        </td>
                        <td>
                            <asp:TextBox ID="txtWhite" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                                TargetControlID="txtWhite">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr id="MultiRacesRow" runat="server">
                        <td>
                            Two or more races:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMultiRaces" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                                TargetControlID="txtMultiRaces">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total Students:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTotalSutdents" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                                TargetControlID="txtTotalSutdents">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblItemLabel" runat="server"></asp:Label>:
                        </td>
                        <td>
                            <asp:TextBox ID="txtExtraField1" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers"
                                TargetControlID="txtExtraField1">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr id="dynamicTR" runat="server" visible="false">
                        <td>
                            <asp:Label ID="lblDynamicLabel" runat="server"></asp:Label>:
                        </td>
                        <td>
                            <asp:TextBox ID="txtExtraField2" runat="server" 
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers"
                                TargetControlID="txtExtraField2">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="Button10" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveMeasureData" />
                            <asp:Button ID="Button11" runat="server" Visible="true" CssClass="msapBtn" Text="Close Window" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button3" CancelControlID="Button3"
            BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
