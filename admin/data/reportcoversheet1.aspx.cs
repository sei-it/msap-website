﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_reportcoversheet1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }

            hfReportID.Value = Convert.ToString(Session["ReportID"]);
            //string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;
            hfGranteeID.Value = granteeID.ToString();
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);
            if (string.IsNullOrEmpty(grantee.PRAward) || string.IsNullOrEmpty(grantee.NCESID)
                || string.IsNullOrEmpty(grantee.ProjectTitle) || string.IsNullOrEmpty(grantee.GranteeName))
            {
                //txtPRAward.Enabled = true;
                //txtNCESID.Enabled = true;
                txtTitle.Enabled = true;
                txtGranteeName.Enabled = true;
            }
            
            txtPRAward.Text = grantee.PRAward;
            txtNCESID.Text = grantee.NCESID;
            txtTitle.Text = grantee.ProjectTitle;
            txtGranteeName.Text = grantee.GranteeName;
            var directors = MagnetGranteeDatum.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));
            //var directors = MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(granteeID) && x.ContactType == 1 );
            if (directors.Count > 0)
            {
                hfDirectorID.Value = directors[0].ID.ToString();
                txtDirectorName.Text = directors[0].ProjectDirector;
                txtDirectorTitle.Text = directors[0].Title;
                txtPhone.Text = directors[0].Phone;
                txtExt.Text = directors[0].Ext;
                txtFax.Text = directors[0].Fax;
                txtEmail.Text = directors[0].Email;
                txtAddress.Text = directors[0].Address;
                txtCity.Text = directors[0].City;
                txtState.Text = directors[0].State;
                txtZipcode.Text = directors[0].Zipcode;
                EnableDisableComponent(false);
            }
            else
                EnableDisableComponent(true);
            
            
            if (directors.Count > 0 && directors[0].AddressChanged == true)
            {
                //ckChangeAddress.Checked = true;
                //txtAddress.Enabled = true;
                //txtCity.Enabled = true;
                //txtState.Enabled = true;
                //txtZipcode.Enabled = true;
            }

            if (Session["ReportPeriodID"] == null)
                Response.Redirect("quarterreports.aspx", true);
            MagnetReportPeriod period = MagnetReportPeriod.SingleOrDefault(x => x.ID == Convert.ToInt32(Session["ReportPeriodID"]));
            //hfReportID.Value = Convert.ToString((int)Session["ReportID"]);
            txtReportPeriodStart.Text = period.PeriodFrom;
            txtReportPeriodEnd.Text = period.PeriodTo;

            txtState.Attributes.Add("onkeyup", "setUpcaseletter(this);");
        }
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        SaveData();
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    protected void MarkChange(object sender, EventArgs e)
    {
        hfSaved.Value = "0";
    }
    private void EnableDisableComponent(bool val)
    {
        txtAddress.Enabled = val;
        txtCity.Enabled = val;
        txtState.Enabled = val;
        txtZipcode.Enabled = val;
    }
    protected void OnChangeAddress(object sender, EventArgs e)
    {
        if (ckChangeAddress.Checked)
        {
            EnableDisableComponent(true);
        }
        else
        {
            EnableDisableComponent(false);
        }
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("reportcoversheet2.aspx", true);
    }
    private void SaveData()
    {
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(hfGranteeID.Value));
        grantee.PRAward = txtPRAward.Text.Trim();
        grantee.NCESID = txtNCESID.Text.Trim();
        grantee.ProjectTitle = txtTitle.Text.Trim();
        grantee.GranteeName = txtGranteeName.Text.Trim();
        grantee.Save();

        /*GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        report.ReportStart = txtReportPeriodStart.Text;
        report.ReportEnd = txtReportPeriodEnd.Text;
        //report.Preparer = HttpContext.Current.User.Identity.Name;
        report.SubmitDate = DateTime.Now;
        report.Save();*/

        if (!string.IsNullOrEmpty(hfDirectorID.Value))
        {
            MagnetGranteeDatum director = MagnetGranteeDatum.SingleOrDefault(x => x.ID == Convert.ToInt32(hfDirectorID.Value));
            //MagnetGranteeContact director = MagnetGranteeContact.SingleOrDefault(x => x.ID == Convert.ToInt32(hfDirectorID.Value));
            director.ProjectDirector = txtDirectorName.Text.Trim();
            director.Title = txtDirectorTitle.Text.Trim();
            director.Phone = txtPhone.Text.Trim();
            director.Ext = txtExt.Text.Trim();
            director.Fax = txtFax.Text.Trim();
            director.Email = txtEmail.Text.Trim();

            bool isAddressChange = false;
            if (ckChangeAddress.Checked)
            {
                if (director.Address != txtAddress.Text)
                {
                    director.Address = txtAddress.Text;
                    isAddressChange = true;
                }
                if (director.City != txtCity.Text)
                {
                    director.City = txtCity.Text;
                    isAddressChange = true;
                }
                if (director.State != txtState.Text)
                {
                    director.State = txtState.Text;
                    isAddressChange = true;
                }
                if (director.Zipcode != txtZipcode.Text)
                {
                    director.Zipcode = txtZipcode.Text;
                    isAddressChange = true;
                }
                if(isAddressChange)
                    director.AddressChanged = true;
            }
            director.Save();
        }
        else //create new grantee data
        {
            var newdirector = new MagnetGranteeDatum();
            newdirector.ProjectDirector = txtDirectorName.Text.Trim();
            newdirector.Title = txtDirectorTitle.Text.Trim();
            newdirector.Phone = txtPhone.Text.Trim();
            newdirector.Ext = txtExt.Text.Trim();
            newdirector.Fax = txtFax.Text.Trim();
            newdirector.Email = txtEmail.Text.Trim();
            newdirector.Address = txtAddress.Text.Trim();
            newdirector.City = txtCity.Text.Trim();
            newdirector.State = txtState.Text.Trim();
            newdirector.Zipcode = txtZipcode.Text.Trim();
            newdirector.AddressChanged = false;
            newdirector.GranteeReportID = Convert.ToInt32(hfReportID.Value);
            newdirector.Save();
        }
        //hfSaved.Value = "1";

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 1);
        if (muhs.Count > 0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 1;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    /*protected void OnPrint(object sender, EventArgs e)
    {
        SaveData();
        int granteeID = Convert.ToInt32(hfGranteeID.Value);
        MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == granteeID);

        GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value));
        var sheets = GranteePerformanceCoverSheet.Find(x => x.GranteeReportID == Convert.ToInt32(hfReportID.Value));
        GranteePerformanceCoverSheet sheet = sheets[0];
        var directors = MagnetGranteeContact.Find(x => x.LinkID == Convert.ToInt32(granteeID));

        //Print
        string TmpPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
        if (File.Exists(TmpPDF))
            File.Delete(TmpPDF);
        string SummaryPDF = Server.MapPath("../doc/") + Guid.NewGuid().ToString() + ".pdf";
        if (File.Exists(SummaryPDF))
            File.Delete(SummaryPDF);
        string Stationery = Server.MapPath("../doc/") + grantee.PRAward + ".pdf";
        using (System.IO.MemoryStream output = new MemoryStream())
        {
            Document document = new Document(PageSize.A4, 40f, 20f, 170f, 30f);
            try
            {
                PdfStamper ps = null;

                // read existing PDF document
                PdfReader r = new PdfReader(Server.MapPath("../doc/coversheet.pdf"));
                ps = new PdfStamper(r, new FileStream(TmpPDF, FileMode.Create));

                AcroFields af = ps.AcroFields;
                af.SetField("1 PRAward #", grantee.PRAward);
                af.SetField("2 Grantee NCES ID #", grantee.NCESID);
                af.SetField("3 Project Title", grantee.ProjectTitle);
                af.SetField("4 Grantee Name Block 1 of the Grant Award Notification", grantee.GranteeName);

                if (report.ReportPeriodID == 4)
                {
                    af.SetField("Final Performance Report", "Yes");
                }
                else
                {
                    af.SetField("Annual Performance Report", "Yes");
                }

                if (directors.Count > 0)
                {
                    af.SetField("6 Project Director See instructions Name", directors[0].ContactFirstName + " " + directors[0].ContactLastName);
                    af.SetField("Project Director Title", directors[0].ContactTitle);
                    if (!string.IsNullOrEmpty(directors[0].Phone))
                    {
                        if (directors[0].Phone.Length >= 4)
                            af.SetField("Project Phone Area", directors[0].Phone.Substring(1, 3));
                        if (directors[0].Phone.Length >= 8)
                            af.SetField("Project Phone A", directors[0].Phone.Substring(6, 3));
                        if (directors[0].Phone.Length >= 13)
                            af.SetField("Project Phone B", directors[0].Phone.Substring(10, 4));

                    }
                    if (!string.IsNullOrEmpty(directors[0].Fax))
                    {
                        if (directors[0].Fax.Length >= 4)
                            af.SetField("Project Fax Area", directors[0].Fax.Substring(1, 3));
                        if (directors[0].Fax.Length >= 8)
                            af.SetField("Project Fax A", directors[0].Fax.Substring(6, 3));
                        if (directors[0].Fax.Length >= 13)
                            af.SetField("Project Fax B", directors[0].Fax.Substring(10, 4));

                    } af.SetField("Project Phone Ext", "");
                    af.SetField("Email Address", directors[0].Email);
                }

                if (!string.IsNullOrEmpty(report.ReportStart))
                {
                    string[] strs = report.ReportStart.Split('/');
                    af.SetField("Reporting From Month", ManageUtility.PaddingString(strs[0]));
                    af.SetField("Reporting From Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("Reporting From Year", strs[2]);
                }
                if (!string.IsNullOrEmpty(report.ReportEnd))
                {
                    string[] strs = report.ReportEnd.Split('/');
                    af.SetField("Reporting To Month", ManageUtility.PaddingString(strs[0]));
                    af.SetField("Reporting To Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("Reporting To Year", strs[2]);
                }

                af.SetField("Federal Grant Fundsa Previous Budget Period", "$" + (sheet.PreviousFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.PreviousFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Sharea Previous Budget Period", "$" + (sheet.PreviousNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.PreviousNonFederalFunds)));
                af.SetField("Federal Grant Fundsb Current Budget Period", "$" + (sheet.CurrentFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.CurrentFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Shareb Current Budget Period", "$" + (sheet.CurrentNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.CurrentNonFederalFunds)));
                af.SetField("Federal Grant Fundsc Entire Project Period For Final Performance Reports only", "$" + (sheet.EntireFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.EntireFederalFunds)));
                af.SetField("NonFederal Funds MatchCost Sharec Entire Project Period For Final Performance Reports only", "$" + (sheet.EntireNonFederalFunds == null ? "0.00" : String.Format("{0:#,0.00}", sheet.EntireNonFederalFunds)));

                if ((bool)sheet.IndirectCost)
                    af.SetField("9a Yes", "Yes");
                else
                    af.SetField("9a No", "Yes");

                if ((bool)sheet.IndirectCostApproved)
                    af.SetField("9b Yes", "Yes");
                else
                    af.SetField("9b No", "Yes");

                if (!string.IsNullOrEmpty(sheet.IndirectCostStart))
                {
                    string[] strs = sheet.IndirectCostStart.Split('/');
                    af.SetField("9c Period From", ManageUtility.PaddingString(strs[0]));
                    af.SetField("9c Period From Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("9c Period From Year", strs[2]);
                }

                if (!string.IsNullOrEmpty(sheet.IndirectCostEnd))
                {
                    string[] strs = sheet.IndirectCostEnd.Split('/');
                    af.SetField("9c Period To", ManageUtility.PaddingString(strs[0]));
                    af.SetField("9c Period To Day", ManageUtility.PaddingString(strs[1]));
                    af.SetField("9c Period To Year", strs[2]);
                }

                if ((bool)sheet.EDApproved)
                    af.SetField("9c Yes", "Yes");
                if (!string.IsNullOrEmpty(sheet.OtherAgency))
                {
                    af.SetField("9c No", "Yes");
                    af.SetField("Approving agency other", sheet.OtherAgency);
                }

                switch ((int)sheet.RateType)
                {
                    case 1:
                        af.SetField("Type of rate provisional", "Yes");
                        break;
                    case 2:
                        af.SetField("Type of rate final", "Yes");
                        break;
                    case 3:
                        af.SetField("Type of rate other", "Yes");
                        break;
                }
                if (!string.IsNullOrEmpty(sheet.OtherRateType))
                    af.SetField("Type of rate other specify", sheet.OtherRateType);

                if ((bool)sheet.RestrictedRateProgram)
                    af.SetField("9d Is included in approved indirect cost rate agreement", "Yes");

                if ((bool)sheet.Comply34CFR)
                    af.SetField("9d complies with 34 CFR 76.564", "Yes");

                switch ((int)sheet.AnnualCertificationApproval)
                {
                    case 1:
                        af.SetField("IRB attached Yes", "Yes");
                        break;
                    case 2:
                        af.SetField("IRB attached No", "Yes");
                        break;
                    case 3:
                        af.SetField("IRB attached N/A", "Yes");
                        break;
                }

                if ((bool)sheet.CompletionDate)
                    af.SetField("11a Performance Measures Status Yes", "Yes");
                else
                    af.SetField("11a Performance Measures Status No", "Yes");

                if (!string.IsNullOrEmpty(sheet.DataAvailableDate))
                {
                    string[] strs = sheet.DataAvailableDate.Split('/');
                    af.SetField("b If no when will the data be available", strs[0]);
                    af.SetField("b If no when will the data be available Month", strs[1]);
                    af.SetField("b If no when will the data be available Year", strs[2]);
                }

                //af.SetField("ExecutiveSummary", sheet.ExecutiveSummary);

                ps.FormFlattening = true;

                r.Close();
                ps.Close();

                PdfReader reader = new PdfReader(TmpPDF);
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, new FileStream(SummaryPDF, FileMode.Create));
                document.Open();
                PdfContentByte pdfContentByte = pdfWriter.DirectContent;

                document.NewPage();
                PdfImportedPage importedPage = pdfWriter.GetImportedPage(reader, 1);
                pdfContentByte.AddTemplate(importedPage, 0, 0);

                document.NewPage();
                document.Add(new Paragraph(new Chunk(sheet.ExecutiveSummary)));

                document.Close();

                //Add stamp
                PdfReader o_reader = new PdfReader(SummaryPDF);
                PdfReader s_reader = new PdfReader(Stationery);
                PdfStamper stamper = new PdfStamper(o_reader, output);

                PdfImportedPage page = stamper.GetImportedPage(s_reader, 1);
                int n = o_reader.NumberOfPages;
                PdfContentByte background;
                for (int i = 2; i <= n; i++)
                {
                    background = stamper.GetUnderContent(i);
                    background.AddTemplate(page, 0, 0);
                }
                stamper.Close();

                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment;filename=coversheet.pdf");
                Response.OutputStream.Write(output.GetBuffer(), 0, output.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
            }
            catch (System.Exception ex)
            {
                ILog Log = LogManager.GetLogger("EventLog");
                Log.Error("Cover Sheet report:", ex);
            }
            finally
            {
                if (File.Exists(TmpPDF))
                    File.Delete(TmpPDF);
                if (File.Exists(SummaryPDF))
                    File.Delete(SummaryPDF);
                output.Close();
            }
        }
    }*/
}