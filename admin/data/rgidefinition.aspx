﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="rgidefinition.aspx.cs" Inherits="admin_data_rgidefinition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit MGI Definition
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Add/Edit MGI Definition</h1>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <table>
            <tr>
                <td>
                    Magnet School:
                </td>
                <td>
                    <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    MGI Definition:
                </td>
                <td>
                    <asp:TextBox ID="txtRGIDefinition" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine" CssClass="msapDataTxt"
                        Rows="3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    American Indian/ Alaskan Native:
                </td>
                <td>
                    <asp:DropDownList ID="ddlIndian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Asian:
                </td>
                <td>
                    <asp:DropDownList ID="ddlAsian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Black or African American (Not of Hispanic Origin):
                </td>
                <td>
                    <asp:DropDownList ID="ddlBlack" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Hispanic/Latino:
                </td>
                <td>
                    <asp:DropDownList ID="ddlHispanic" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Native Hawaiian or Other Pacific Islander:
                </td>
                <td>
                    <asp:DropDownList ID="ddlHawaiian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtNotes" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine" CssClass="msapDataTxt"
                        Rows="3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />&nbsp;&nbsp;
                    <asp:Button ID="Button3" runat="server" CssClass="msapBtn" Text="Return" OnClick="OnReturn" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
