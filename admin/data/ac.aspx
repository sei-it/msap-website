﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="ac.aspx.cs" Inherits="admin_data_ac"  ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Upload Assurances and Certifications
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        $(function () {
            $("input:submit").click(function () {
                $("#hfControlID").val($(this).attr("id"));
            });
        });
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {
                var who = document.getElementsByName('<%= Assurance.UniqueID %>')[0];
                who.value = "";

                var who2 = who.cloneNode(false);
                who2.onchange = who.onchange;
                who.parentNode.replaceChild(who2, who);

                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            Assurances and Certifications</h4>
<div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <div class="titles_noTabs">Assurances and Certifications</div><br/>
        <p class="clear">Upload a copy of the signed and dated Assurances and Certification form. To create the form, please follow these steps: (1) print a paper copy of the form, (2) have the authorized representative sign and date the paper copy, (3) scan the signed copy to create a PDF file, and (4) upload the PDF file to MAPS.</p>
<p>If there are any known internal control weaknesses concerning data quality (as disclosed through audits or other reviews), this information must be disclosed along with the remedies taken to ensure the accuracy, reliability, and completeness of the data. 
</p>
        <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Upload Assurances and Certifications</b></span>

            <table style="margin-left: 20px;margin-top:10px;">
                <tr>
                    <td >
                        Attach a copy of your signed and dated Magnet Schools Assistance Program Assurances and Certification form. <img src="../../images/question_mark-thumb.png" title="You can find the Assurances and Certifications document in the Resources section of the website." /><br/><br/>
                        <asp:FileUpload ID="Assurance" runat="server" CssClass="msapDataTxt" />
                           <asp:Button ID="Button10" runat="server" CssClass="surveyBtn1" Text="Save &amp; Upload"
                OnClick="OnSave" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView3" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="false" DataKeyNames="ID" GridLines="None"
                            Width="300" CssClass="msapDataTbl">
                            <Columns>
                                <asp:BoundField HeaderText="Uploaded Files" DataField="FilePath" HtmlEncode="false" />
                                <asp:TemplateField>
                                 <ItemStyle CssClass="TDWithBottomBorder" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete"
                                            CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you sure you want to delete this uploaded file?');"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div class="clear">
        </div>
    </div>
</asp:Content>
