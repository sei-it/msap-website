﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="GPRAVI.aspx.cs" Inherits="admin_data_GPRAVI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
 Add/Edit Minority Group Isolation Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script src="../../js/GPRAVI.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });

       
    </script>
    <style type="text/css">
        .tdpaddingleft
        {
           padding-left:20px;
           vertical-align:top;
        }
      
        
        .msapDataTbl tr td:first-child
        {
            color: #4e8396 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-left: 0px !important;
            text-align: center;
        }

        .msapDataTbl th
        {
            color: #000;
            text-align: left;
        }
        .TDCenterClass
        {
            text-align: center !important;
        }
       
        </style>



</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managegpra.aspx">GPRA Table</a> <span class="greater_than">&gt;</span>
            Part VI. Minority Group Isolation Data</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
        <div class="GPRA_titles">
            <asp:Label ID="lblSchoolName" runat="server" Width="380px"></asp:Label></div>
        <div class="tab_area">
            <ul class="tabs">
                <li class="tab_active">Part VI</li>
                <li><a runat="server" id="tabPartV" href="gprav.aspx">Part V</a></li>
                <li><a runat="server" id="tabPartIV" href="gpraiv.aspx">Part IV</a></li>
                <li><a runat="server" id="tabPartIII" href="gpraiii.aspx">Part III</a></li>
                <li><a runat="server" id="tabPartII" href="gpraii.aspx">Part II</a></li>
                <li><a runat="server" id="tabPartI" href="gprai.aspx">Part I</a></li>
            </ul>
        </div>
        <br />
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Part VI. Minority Group Isolation Data</b> </span>
        <p>
        Part VI of the GPRA Table also collects data for calculating GPRA performance measure 1 outcomes for each minority/racially isolated group in a magnet school. See the <i>GPRA Guide</i> for instructions about reporting these data. When you finish entering data for this page, click on Save Record before proceeding.
        </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <table>
        <tr>
        <td class="tdpaddingleft">
        61.
        </td>
        <td>
        For this school, identify which student group is minority/racially isolated. If the school has more than one minority/racially isolated group, select all that apply.
        </td>
        </tr>
        <tr>
        <td></td>
        <td>
        <asp:CheckBoxList ID="cbxlstMinRacialIsolateGroups" runat="server" CssClass="change"
                AutoPostBack="true" 
                onselectedindexchanged="cbxlstMinRacialIsolateGroups_SelectedIndexChanged" >
        <asp:ListItem Value="1">American Indian or Alaska Native students</asp:ListItem>
        <asp:ListItem Value="2">Asian students</asp:ListItem>
        <asp:ListItem Value="3">Black or African-American students</asp:ListItem>
        <asp:ListItem Value="4">Hispanic or Latino students</asp:ListItem>
        <asp:ListItem Value="5">Native Hawaiian or Other Pacific Islander students</asp:ListItem>
        <asp:ListItem Value="6">White students</asp:ListItem>
        </asp:CheckBoxList>
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-bottom:5px;">
            &nbsp;
         </td>
        </tr>
        <tr>
        <td class="tdpaddingleft">62.</td>
        <td>
        Indicate whether each minority/racially isolated group identified in item 61 is located at this MSAP school or at a feeder school. 
        </td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        <table class="msapDataTbl">
                <tr>
                    <th class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                    Racial/ethnic groups
                    </th>
                    <th  class="TDWithBorder" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        MSAP school
                    </th>
                    <th colspan="2" class="TDWithBorder" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        Feeder school
                    </th>
                    
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblIndian62" ForeColor="Gray" runat="server" Text="American Indian or Alaska Native students" />
                        
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMsapIndian62" runat="server" CssClass="change" GroupName="Indiangroup62" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnFeederIndian62" runat="server" CssClass="change" GroupName="Indiangroup62" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalIndian62" Enabled="false" SetFocusOnError="true" runat="server" Display="Dynamic" ErrorMessage="Please choose one" 
                      />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblAsian62" ForeColor="Gray" runat="server" Text="Asian students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMSAPAsian62" runat="server" CssClass="change" GroupName="Asiangroup62" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnFeederAsian62" runat="server" CssClass="change" GroupName="Asiangroup62" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalAsian62" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one"
                     />
                    </td>
                 </tr>
                <tr>
                    <td>
                      <asp:Label ID="lblBlack62" ForeColor="Gray" runat="server" Text="Black or African-American students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMSAPBlack62" runat="server" CssClass="change" GroupName="Blackgroup62" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnFeederBlack62" runat="server" CssClass="change" GroupName="Blackgroup62" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalBlack62" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td>
                      <asp:Label ID="lblHispanic62" ForeColor="Gray" runat="server" Text="Hispanic or Latino students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMSAPHispanic62" runat="server" CssClass="change" GroupName="Hispanicgroup62" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnFeederHispanic62" runat="server" CssClass="change" GroupName="Hispanicgroup62" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalHispanic62" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td >
                       <asp:Label ID="lblNative62" ForeColor="Gray" runat="server" Text="Native Hawaiian or Other Pacific Islander students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMSAPNative62" runat="server" CssClass="change" GroupName="Nativegroup62" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnFeederNative62" runat="server" CssClass="change" GroupName="Nativegroup62" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalNative62" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td >
                      <asp:Label ID="lblWhite62" ForeColor="Gray" runat="server" Text="White students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMSAPWhite62" runat="server" CssClass="change" GroupName="Whitegroup62" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnFeederWhite62" runat="server" CssClass="change" GroupName="Whitegroup62" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalWhite62" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                      />
                    </td>
                </tr>

     </table>
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-bottom:5px;">
            &nbsp;
         </td>
        </tr>
        <tr>
        <td class="tdpaddingleft">63.</td>
        <td>
        Indicate how the school with the minority/racially isolated group(s) plans to change the enrollment percentage of each minority/racially isolated group identified in item 61.
        </td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        <table class="msapDataTbl">
                <tr>
                    <th class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                    Racial/ethnic groups
                    </th>
                    <th  class="TDWithBorder" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        Decrease enrollment percentage
                    </th>
                    <th class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        Increase enrollment percentage	
                    </th>
                    <th colspan="2" class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
					  Maintain enrollment percentage
  				  </th>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblIndian63" ForeColor="Gray" runat="server" Text="American Indian or Alaska Native students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnDecIndian63" runat="server" CssClass="change" GroupName="Indiangroup63" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnIncIndian63" runat="server" CssClass="change" GroupName="Indiangroup63" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnMtnIndian63" runat="server" CssClass="change" GroupName="Indiangroup63" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalIndian63" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:Label ID="lblAsian63" ForeColor="Gray" runat="server" Text="Asian students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnDecAsian63" runat="server" CssClass="change" GroupName="Asiangroup63" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnIncAsian63" CssClass="change" runat="server" GroupName="Asiangroup63" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnMtnAsian63" CssClass="change" runat="server" GroupName="Asiangroup63" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalAsian63" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one"
                     />
                    </td>
                 </tr>
                <tr>
                    <td>
                      <asp:Label ID="lblBlack63" ForeColor="Gray" runat="server" Text="Black or African-American students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnDecBlack63" CssClass="change" runat="server" GroupName="Blackgroup63" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnIncBlack63" CssClass="change" runat="server" GroupName="Blackgroup63" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnMtnBlack63" runat="server" CssClass="change" GroupName="Blackgroup63" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalBlack63" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:Label ID="lblHispanic63" ForeColor="Gray" runat="server" Text="Hispanic or Latino students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnDecHispanic63" runat="server" CssClass="change" GroupName="Hispanicgroup63" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnIncHispanic63" runat="server" CssClass="change" GroupName="Hispanicgroup63" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnMtnHispanic63" runat="server" CssClass="change" GroupName="Hispanicgroup63" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalHispanic63" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td >
                       <asp:Label ID="lblNative63" ForeColor="Gray" runat="server" Text="Native Hawaiian or Other Pacific Islander students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnDecNative63" CssClass="change" runat="server" GroupName="Nativegroup63" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnIncNative63" CssClass="change" runat="server" GroupName="Nativegroup63" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnMtnNative63" CssClass="change" runat="server" GroupName="Nativegroup63" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalNative63" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" />
                    </td>
                </tr>
                <tr>
                    <td >
                      <asp:Label ID="lblWhite63" ForeColor="Gray" runat="server" Text="White students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnDecWhite63" CssClass="change" runat="server" GroupName="Whitegroup63" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnIncWhite63" CssClass="change" runat="server" GroupName="Whitegroup63" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnMtnWhite63" CssClass="change" runat="server" GroupName="Whitegroup63" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalWhite63" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                      />
                    </td>
                </tr>

     </table>
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-bottom:5px;">
            &nbsp;
         </td>
        </tr>
        <tr>
        <td class="tdpaddingleft">64.</td>
        <td>
        Indicate the targeted racial/ethnic group for the school with the minority/racially isolated group(s). The targeted racial/ethnic group is the group the school would like to enroll more of in order to change the enrollment of the minority/racially isolated group. If the school has more than one targeted racial/ethnic group, select all that apply.
        </td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        <asp:CheckBoxList ID="cbxlstTargetMinRacialIsolateGroups" runat="server" >
        <asp:ListItem Value="1">American Indian or Alaska Native students</asp:ListItem>
        <asp:ListItem Value="2">Asian students</asp:ListItem>
        <asp:ListItem Value="3">Black or African-American students</asp:ListItem>
        <asp:ListItem Value="4">Hispanic or Latino students</asp:ListItem>
        <asp:ListItem Value="5">Native Hawaiian or Other Pacific Islander students</asp:ListItem>
        <asp:ListItem Value="6">White students</asp:ListItem>
        </asp:CheckBoxList>
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-bottom:5px;">
            &nbsp;
         </td>
        </tr>
        <tr>
        <td class="tdpaddingleft">65.</td>
        <td>
        Enter an annual target enrollment percentage and an actual enrollment percentage for each minority/racially isolated group selected in item 61. Leave fields blank for racial/ethnic groups that have not been identified as minority/racially isolated.
        </td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        <table class="msapDataTbl">
                <tr>
                    <th class="TDWithBorder" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                    Racial/ethnic groups
                    </th>
                    <th  class="TDWithBorder" colspan="2" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                    <asp:Label ID="lblReportPeriod" runat="server" Text=""></asp:Label>
                    </th>
                    <th  class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        <asp:Label ID="lblReportPeriod2" runat="server" Text=""></asp:Label>
                    </th>
                    
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblIndian65" ForeColor="Gray" runat="server" Text="American Indian or Alaska Native students" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
					  <telerik:RadNumericTextBox ID="txtTargetIndian65" runat="server" Enabled="false" CssClass="change"
                                  MaxLength="6" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="2" 
                                  Skin="" Type="Number"  />
                    </td>
                    <td>
                    <asp:CustomValidator id="rqvalIndian65" Enabled="false" runat="server" ControlToValidate="txtTargetIndian65" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Required" />
                    </td>
                    <td >
                     <asp:Label ID="lblActualIndian65" runat="server"  />
                    </td>
                </tr>
                <tr>
                    <td >
                       <asp:Label ID="lblAsian65" ForeColor="Gray" runat="server" Text="Asian students" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
					 <telerik:RadNumericTextBox ID="txtTargetAsian65" runat="server" Enabled="false" CssClass="change"
                                  MaxLength="6" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="2" 
                                  Skin="" Type="Number"  />
                    </td>
                    <td>
                     <asp:CustomValidator id="rqvalAsian65" Enabled="false" runat="server" ControlToValidate="txtTargetAsian65" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Required" />
                    </td>
                    <td >
                     <asp:Label ID="lblActualAsian65" runat="server" />
                    </td>
                 </tr>
                <tr>
                    <td >
                      <asp:Label ID="lblBlack65" ForeColor="Gray" runat="server" Text="Black or African-American students" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
					 <telerik:RadNumericTextBox ID="txtTargetBlack65" runat="server" Enabled="false" CssClass="change"
                                  MaxLength="6" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="2" 
                                  Skin="" Type="Number"  />
                   </td>
                   <td>
                     <asp:CustomValidator id="rqvalBlack65" Enabled="false" runat="server" ControlToValidate="txtTargetBlack65" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Required" />
                    </td>
                    <td>
                     <asp:Label ID="lblActualBlack65" runat="server"  />
                    </td>
                </tr>
                <tr>
                    <td >
                       <asp:Label ID="lblHispanic65" ForeColor="Gray" runat="server" Text="Hispanic or Latino students" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
					 <telerik:RadNumericTextBox ID="txtTargetHispanic65" runat="server" Enabled="false" CssClass="change"
                                  MaxLength="6" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="2" 
                                  Skin="" Type="Number"  />
                    </td><td>
                     <asp:CustomValidator id="rqvalHispanic65" Enabled="false" runat="server" ControlToValidate="txtTargetHispanic65" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Required" />
                    </td>
                    <td>
                     <asp:Label ID="lblActualHispanic65" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td  >
                       <asp:Label ID="lblNative65" ForeColor="Gray" runat="server" Text="Native Hawaiian or Other Pacific Islander students" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
					 <telerik:RadNumericTextBox ID="txtTargetNative65" runat="server" Enabled="false" CssClass="change"
                                  MaxLength="6" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="2" 
                                  Skin="" Type="Number"  />
                    </td>
                    <td>
                      <asp:CustomValidator id="rqvalNative65" Enabled="false" runat="server" ControlToValidate="txtTargetNative65" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Required" />
                    </td>
                    <td >
                     <asp:Label ID="lblActualNative65" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td  >
                      <asp:Label ID="lblWhite65" ForeColor="Gray" runat="server" Text="White students" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
					 <telerik:RadNumericTextBox ID="txtTargetWhite65" runat="server" Enabled="false" CssClass="change"
                     MaxLength="6" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="2" 
                     Skin="" Type="Number"  />
                     </td><td>					 
                     <asp:CustomValidator id="rqvalWhite65" Enabled="false" runat="server" ControlToValidate="txtTargetWhite65" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Required" />
                    </td>
                    <td >
                     <asp:Label ID="lblActualWhite65" runat="server" />
                    </td>
                </tr>
     </table>
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-bottom:5px;">
            &nbsp;
         </td>
        </tr>
        <tr>
        <td class="tdpaddingleft">66.</td>
        <td>
        Indicate whether the school met its enrollment target for each minority/racially isolated group identified in item 61.
        </td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        <table class="msapDataTbl">
                <tr>
                    <th class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                    Racial/ethnic groups
                    </th>
                    <th  class="TDWithBorder" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        Met target
                    </th>
                    <th colspan="2" class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        Did not meet target
                    </th>
                    
                </tr>
                <tr>
                    <td>
                       <asp:Label ID="lblIndian66" ForeColor="Gray" runat="server" Text="American Indian or Alaska Native students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMetIndian66" runat="server" CssClass="change" GroupName="Indiangroup66" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNotMetIndian66" runat="server" CssClass="change" GroupName="Indiangroup66" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalIndian66" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:Label ID="lblAsian66" ForeColor="Gray" runat="server" Text="Asian students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMetAsian66" runat="server" CssClass="change" GroupName="Asiangroup66" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNotMetAsian66" runat="server" CssClass="change" GroupName="Asiangroup66" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalAsian66" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one"
                    />
                    </td>
                 </tr>
                <tr>
                    <td>
                      <asp:Label ID="lblBlack66" ForeColor="Gray" runat="server" Text="Black or African-American students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMetBlack66" runat="server" CssClass="change" GroupName="Blackgroup66" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNotMetBlack66" runat="server" CssClass="change" GroupName="Blackgroup66" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalBlack66" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:Label ID="lblHispanic66" ForeColor="Gray" runat="server" Text="Hispanic or Latino students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMetHispanic66" runat="server" CssClass="change" GroupName="Hispanicgroup66" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNotMetHispanic66" runat="server" CssClass="change" GroupName="Hispanicgroup66" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalHispanic66" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td >
                      <asp:Label ID="lblNative66" ForeColor="Gray" runat="server" Text="Native Hawaiian or Other Pacific Islander students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMetNative66" runat="server" CssClass="change" GroupName="Nativegroup66" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNotMetNative66" runat="server" CssClass="change" GroupName="Nativegroup66" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalNative66" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td >
                      <asp:Label ID="lblWhite66" ForeColor="Gray" runat="server" Text="White students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMetWhite66" runat="server" CssClass="change" GroupName="Whitegroup66" Enabled="false" />
                    </td>
                    <td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNotMetWhite66" runat="server" CssClass="change" GroupName="Whitegroup66" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalWhite66" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                      />
                    </td>
                </tr>

     </table>
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-bottom:5px;">
            &nbsp;
         </td>
        </tr>
        <tr>
        <td class="tdpaddingleft">67.</td>
        <td>
        If this school <b>did not meet</b> its annual minority/racial group isolation enrollment target, indicate whether the school made progress toward achieving the target. “Made progress” means a school did not meet its annual enrollment target for a minority/racially isolated group, but enrollment for that minority/racially isolated group moved in the right direction. <br />Select the appropriate response for each minority/racially isolated group identified in item 61. If the school met its target for a particular minority/racially isolated group, select “Not applicable.”
        </td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        <table class="msapDataTbl">
                <tr>
                    <th class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                    Racial/ethnic groups
                    </th>
                    <th  class="TDWithBorder" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        Made progress
                    </th>
                    <th class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
                        Did not make progress	
                    </th>
                    <th colspan="2" class="TDWithBorder LeftAlignCell" style=" font-weight: bold;border-top: 1px solid #a9def2;">
					  Not applicable
  				  </th>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblIndian67" ForeColor="Gray" runat="server" Text="American Indian or Alaska Native students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMadeIndian67" runat="server" CssClass="change" GroupName="Indiangroup67" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnNotMadeIndian67" runat="server" CssClass="change" GroupName="Indiangroup67" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNAIndian67" runat="server" CssClass="change" GroupName="Indiangroup67" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalIndian67" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:Label ID="lblAsian67" ForeColor="Gray" runat="server" Text="Asian students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMadeAsian67" runat="server" CssClass="change" GroupName="Asiangroup67" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnNotMadeAsian67" runat="server" CssClass="change" GroupName="Asiangroup67" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNAAsian67" runat="server" CssClass="change" GroupName="Asiangroup67" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalAsian67" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one"
                    />
                    </td>
                 </tr>
                <tr>
                    <td>
                      <asp:Label ID="lblBlack67" ForeColor="Gray" runat="server" Text="Black or African-American students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMadeBlack67" CssClass="change" runat="server" GroupName="Blackgroup67" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnNotMadeBlack67" CssClass="change" runat="server" GroupName="Blackgroup67" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNABlack67" runat="server" CssClass="change" GroupName="Blackgroup67" Enabled="false" />
                   </td>
                   <td>
                    <asp:CustomValidator id="cusvalBlack67" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:Label ID="lblHispanic67" ForeColor="Gray" runat="server" Text="Hispanic or Latino students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMadeHispanic67" runat="server" CssClass="change" GroupName="Hispanicgroup67" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnNotMadeHispanic67" runat="server" CssClass="change" GroupName="Hispanicgroup67" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNAHispanic67" runat="server" CssClass="change" GroupName="Hispanicgroup67" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalHispanic67" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                    />
                    </td>
                </tr>
                <tr>
                    <td >
                       <asp:Label ID="lblNative67" ForeColor="Gray" runat="server" Text="Native Hawaiian or Other Pacific Islander students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMadeNative67" runat="server" CssClass="change" GroupName="Nativegroup67" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnNotMadeNative67" runat="server" CssClass="change" GroupName="Nativegroup67" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNANative67" runat="server" CssClass="change" GroupName="Nativegroup67" Enabled="false" />
                    </td>
                    <td>
                    <asp:CustomValidator id="cusvalNative67" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                    />
                    </td>
                </tr>
                <tr>
                    <td >
                      <asp:Label ID="lblWhite67" ForeColor="Gray" runat="server" Text="White students" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnMadeWhite67" runat="server" CssClass="change" GroupName="Whitegroup67" Enabled="false" />
                    </td>
                    <td>
                     <asp:RadioButton ID="rbtnNotMadeWhite67" runat="server" CssClass="change" GroupName="Whitegroup67" Enabled="false" />
                    </td>
					<td class="TDWithBottomNoRightBorder">
                     <asp:RadioButton ID="rbtnNAWhite67" runat="server" CssClass="change" GroupName="Whitegroup67" Enabled="false" />
                     </td>
                     <td>
                    <asp:CustomValidator id="cusvalWhite67" Enabled="false" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please choose one" 
                     />
                    </td>
                </tr>
     </table>
        </td>
        </tr>
        </table>

        </ContentTemplate>
        </asp:UpdatePanel>

        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfPerformanceMeasure" runat="server" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        
        <div style="text-align: right; margin-top: 20px;">
            <asp:Button ID="btnSave" runat="server" CssClass="surveyBtn1 postbutton" Text="Save Record"
                OnClick="OnSave" />
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <a href="manageGPRA.aspx">Back to GPRA table</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                id="LinkI" href="GPRAI.aspx">Part I</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                    id="LinkII" href="GPRAII.aspx">Part II</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                        id="LinkIII" href="GPRAIII.aspx">Part III</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server"  id="LinkIV" href="GPRAIV.aspx">Part IV</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server"  id="LinkV" href="GPRAV.aspx">Part V</a>&nbsp;&nbsp;&nbsp;&nbsp;
            Part VI
        </div>
    </div>
</asp:Content>

