﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_gpraiii : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlSubTitle.Text = Session["ReportPeriodTitle"] == null ? "" : Session["ReportPeriodTitle"].ToString();

        if (Request.QueryString["id"] == null)
            Response.Redirect("managegpra.aspx");
        else
        {
            LinkI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
            tabPartI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
            LinkII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
            tabPartII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
            LinkIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
            tabPartIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];
            LinkV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
            tabPartV.HRef = "gprav.aspx?id=" + Request.QueryString["id"];
            LinkVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];
            tabPartVI.HRef = "gpravi.aspx?id=" + Request.QueryString["id"];
        }

        if (!Page.IsPostBack)
        {
            if (Session["ReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["ReportID"]);

            //Report type
            int granteeID = (int)GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(hfReportID.Value)).GranteeID;

            if (Request.QueryString["id"] == null)
                Response.Redirect("managegpra.aspx");
            else
            {
                /*LinkButton1.PostBackUrl = "gprai.aspx?id=" + Request.QueryString["id"];
                tabPartI.HRef = "gprai.aspx?id=" + Request.QueryString["id"];
                LinkButton2.PostBackUrl = "gpraii.aspx?id=" + Request.QueryString["id"];
                tabPartII.HRef = "gpraii.aspx?id=" + Request.QueryString["id"];
                LinkButton3.PostBackUrl = "gpraiv.aspx?id=" + Request.QueryString["id"];
                tabPartIV.HRef = "gpraiv.aspx?id=" + Request.QueryString["id"];*/

                int schoolID = Convert.ToInt32(Request.QueryString["id"]);
                lblSchoolName.Text = MagnetSchool.SingleOrDefault(x => x.ID == schoolID).SchoolName;
                lblReportPeriod.Text = MagnetReportPeriod.SingleOrDefault(x => x.ID == (Convert.ToInt32(Session["ReportPeriodID"]))).ReportPeriod.Replace(" ","");
                lblReportPeriod2.Text = lblReportPeriod.Text;
                lblReportPeriod3.Text = lblReportPeriod.Text;
                lblReportPeriod4.Text = lblReportPeriod.Text;

                //if (Session["ReportPeriodID"] != null && Convert.ToInt32(Session["ReportPeriodID"]) ==6)
                //{
                //    ltlTitleDes.Text = "Part III of the GPRA Table collects data for the two student achievement measures. See the GPRA Guide for instructions about reporting these data. When you finish entering data for this page, click on Save Record before proceeding.";
                //    ltlHead1.Text = "State Assessment Participation Data";
                //    //ltlHead2.Text = " State Assessment Achievement Data ";
                //    ltlHeadCol1.Text = "&nbsp;";
                //    ltlHeadCol2.Text = "Number of students who met or exceeded the state standards in reading /language arts and mathematics:";

                //}
                if (Session["ReportPeriodID"] != null && Convert.ToInt32(Session["ReportPeriodID"]) > 5)
                {
                    ltlTitleDes.Text = "Part III of the GPRA Table collects data for the two student achievement GPRA Performance Measures. See the <i>GPRA Guide</i> for instructions about reporting these data. When you finish entering data for this page, click on Save Record before proceeding.";
                    ltlHead1.Text = "State assessment participation data";
                    ltlHead2.Text = " State assessment achievement data ";
                    ltlHeadCol1.Text = "&nbsp;";
                    ltlHeadCol2.Text = "Number of students who met or exceeded the state standards in reading/language arts and mathematics";                
                }
                else
                { 
                    ltlTitleDes.Text = "Part III of the GPRA Table collects data for the two student achievement measures that address adequate yearly progress (AYP). See the GPRA Guide for instructions about reporting these data. When you finish entering data for this page, click on Save Record before proceeding.";
                    ltlHeadCol1.Text = " adequate yearly progress ";
                    ltlHeadCol2.Text = "Number of students who meet or exceed the state’s adequate yearly progress standard in reading/language arts and mathematics";
                    ltlHead1.Text = "AYP participation data";
                    ltlHead2.Text = "AYP achievement data";

                }
                
                var gpraData = MagnetGPRA.Find(x => x.SchoolID == schoolID && x.ReportID == Convert.ToInt32(hfReportID.Value));
                if (gpraData.Count >0)
                {
                    hfID.Value = gpraData[0].ID.ToString();

                    //Participation reading
                    var participationReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 3);
                    if (participationReading.Count > 0)
                    {
                        hfParticipationReading.Value = participationReading[0].ID.ToString();
                        if (participationReading[0].Indian >= 0) txtIndianParticipationReading.Text = Convert.ToString(participationReading[0].Indian); else txtIndianParticipationReading.Text = "";
                        if (participationReading[0].Asian >= 0) txtAsianParticipationReading.Text = Convert.ToString(participationReading[0].Asian); else txtAsianParticipationReading.Text = "";
                        if (participationReading[0].Black >= 0) txtBlackParticipationReading.Text = Convert.ToString(participationReading[0].Black); else txtBlackParticipationReading.Text = "";
                        if (participationReading[0].Hispanic >= 0) txtHispanicParticipationReading.Text = Convert.ToString(participationReading[0].Hispanic); else txtHispanicParticipationReading.Text = "";
                        if (participationReading[0].Hawaiian >= 0) txtHawaiianParticipationReading.Text = Convert.ToString(participationReading[0].Hawaiian); else txtHawaiianParticipationReading.Text = "";
                        if (participationReading[0].White >= 0) txtWhiteParticipationReading.Text = Convert.ToString(participationReading[0].White); else txtWhiteParticipationReading.Text = "";
                        if (participationReading[0].MultiRaces >= 0) txtMultiRaceParticipationReading.Text = Convert.ToString(participationReading[0].MultiRaces); else txtMultiRaceParticipationReading.Text = "";
                        if (participationReading[0].ExtraFiled2 >= 0) txtDisadvantagedParticipationReading.Text = Convert.ToString(participationReading[0].ExtraFiled2); else txtDisadvantagedParticipationReading.Text = "";
                        if (participationReading[0].Total >= 0) txtTotalParticipationReading.Text = Convert.ToString(participationReading[0].Total); else txtTotalParticipationReading.Text = "";
                        if (participationReading[0].ExtraFiled1 >= 0) txtEnglishLearnerParticipationReading.Text = Convert.ToString(participationReading[0].ExtraFiled1); else txtEnglishLearnerParticipationReading.Text = "";
                    }
                    else
                    {
                        //MagnetGPRAPerformanceMeasure pr = new MagnetGPRAPerformanceMeasure();
                        //pr.ReportType = 3;
                        //pr.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                        //pr.Save();
                        //hfParticipationReading.Value = pr.ID.ToString();
                    }

                    //Participation math
                    var participationMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 4);
                    if (participationMath.Count >0)
                    {
                        hfParticipationMath.Value = participationMath[0].ID.ToString();
                        if (participationMath[0].Indian >=0) txtIndianParticipationMath.Text = Convert.ToString(participationMath[0].Indian); else txtIndianParticipationMath.Text = "";
                        if (participationMath[0].Asian >=0) txtAsianParticipationMath.Text = Convert.ToString(participationMath[0].Asian); else txtAsianParticipationMath.Text = "";
                        if (participationMath[0].Black >=0) txtBlackParticipationMath.Text = Convert.ToString(participationMath[0].Black); else txtBlackParticipationMath.Text = "";
                        if (participationMath[0].Hispanic >=0) txtHispanicParticipationMath.Text = Convert.ToString(participationMath[0].Hispanic); else txtHispanicParticipationMath.Text = "";
                        if (participationMath[0].Hawaiian >=0) txtHawaiianParticipationMath.Text = Convert.ToString(participationMath[0].Hawaiian); else txtHawaiianParticipationMath.Text = "";
                        if (participationMath[0].White >=0) txtWhiteParticipationMath.Text = Convert.ToString(participationMath[0].White); else txtWhiteParticipationMath.Text = "";
                        if (participationMath[0].MultiRaces >=0) txtMultiRaceParticipationMath.Text = Convert.ToString(participationMath[0].MultiRaces); else txtMultiRaceParticipationMath.Text = "";
                        if (participationMath[0].ExtraFiled2 >=0) txtDisadvantagedParticipationMath.Text = Convert.ToString(participationMath[0].ExtraFiled2); else txtDisadvantagedParticipationMath.Text = "";
                        if (participationMath[0].Total >=0) txtTotalParticipationMath.Text = Convert.ToString(participationMath[0].Total); else txtTotalParticipationMath.Text = "";
                        if (participationMath[0].ExtraFiled1 >=0) txtEnglishLearnerParticipationMath.Text = Convert.ToString(participationMath[0].ExtraFiled1); else txtEnglishLearnerParticipationMath.Text = "";
                    }
                    else
                    {
                        //MagnetGPRAPerformanceMeasure pr = new MagnetGPRAPerformanceMeasure();
                        //pr.ReportType = 4;
                        //pr.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                        //pr.Save();
                        //hfParticipationMath.Value = pr.ID.ToString();
                    }

                    //Achievement reading
                    var achievementReading = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 5);
                    if (achievementReading.Count >0)
                    {
                        hfAchievementReading.Value = achievementReading[0].ID.ToString();
                        if (achievementReading[0].Indian >=0) txtIndianAchievementReading.Text = Convert.ToString(achievementReading[0].Indian); else txtIndianAchievementReading.Text = "";
                        if (achievementReading[0].Asian >=0) txtAsianAchievementReading.Text = Convert.ToString(achievementReading[0].Asian); else txtAsianAchievementReading.Text = "";
                        if (achievementReading[0].Black >=0) txtBlackAchievementReading.Text = Convert.ToString(achievementReading[0].Black); else txtBlackAchievementReading.Text = "";
                        if (achievementReading[0].Hispanic >=0) txtHispanicAchievementReading.Text = Convert.ToString(achievementReading[0].Hispanic); else txtHispanicAchievementReading.Text = "";
                        if (achievementReading[0].Hawaiian >=0) txtHawaiianAchievementReading.Text = Convert.ToString(achievementReading[0].Hawaiian); else txtHawaiianAchievementReading.Text = "";
                        if (achievementReading[0].White >=0) txtWhiteAchievementReading.Text = Convert.ToString(achievementReading[0].White); else txtWhiteAchievementReading.Text = "";
                        if (achievementReading[0].MultiRaces >=0) txtMultiRaceAchievementReading.Text = Convert.ToString(achievementReading[0].MultiRaces); else txtMultiRaceAchievementReading.Text = "";
                        if (achievementReading[0].ExtraFiled2 >=0) txtDisadvantagedAchievementReading.Text = Convert.ToString(achievementReading[0].ExtraFiled2); else txtDisadvantagedAchievementReading.Text = "";
                        if (achievementReading[0].Total >=0) txtTotalAchievementReading.Text = Convert.ToString(achievementReading[0].Total); else txtTotalAchievementReading.Text = "";
                        if (achievementReading[0].ExtraFiled1 >=0) txtEnglishLearnerAchievementReading.Text = Convert.ToString(achievementReading[0].ExtraFiled1); else txtEnglishLearnerAchievementReading.Text = "";
                    }
                    else
                    {
                        //MagnetGPRAPerformanceMeasure pr = new MagnetGPRAPerformanceMeasure();
                        //pr.ReportType = 5;
                        //pr.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                        //pr.Save();
                        //hfAchievementReading.Value = pr.ID.ToString();
                    }

                    //Participation math
                    var achievementMath = MagnetGPRAPerformanceMeasure.Find(x => x.MagnetGPRAID == gpraData[0].ID && x.ReportType == 6);
                    if (achievementMath.Count >0)
                    {
                        hfAchievementMath.Value = achievementMath[0].ID.ToString();
                        if (achievementMath[0].Indian >=0) txtIndianAchievementMath.Text = Convert.ToString(achievementMath[0].Indian); else txtIndianAchievementMath.Text = "";
                        if (achievementMath[0].Asian >=0) txtAsianAchievementMath.Text = Convert.ToString(achievementMath[0].Asian); else txtAsianAchievementMath.Text = "";
                        if (achievementMath[0].Black >=0) txtBlackAchievementMath.Text = Convert.ToString(achievementMath[0].Black); else txtBlackAchievementMath.Text = "";
                        if (achievementMath[0].Hispanic >=0) txtHispanicAchievementMath.Text = Convert.ToString(achievementMath[0].Hispanic); else txtHispanicAchievementMath.Text = "";
                        if (achievementMath[0].Hawaiian >=0) txtHawaiianAchievementMath.Text = Convert.ToString(achievementMath[0].Hawaiian); else txtHawaiianAchievementMath.Text = "";
                        if (achievementMath[0].White >=0) txtWhiteAchievementMath.Text = Convert.ToString(achievementMath[0].White); else txtWhiteAchievementMath.Text = "";
                        if (achievementMath[0].MultiRaces >=0) txtMultiRaceAchievementMath.Text = Convert.ToString(achievementMath[0].MultiRaces); else txtMultiRaceAchievementMath.Text = "";
                        if (achievementMath[0].ExtraFiled2 >=0) txtDisadvantagedAchievementMath.Text = Convert.ToString(achievementMath[0].ExtraFiled2); else txtDisadvantagedAchievementMath.Text = "";
                        if (achievementMath[0].Total >=0) txtTotalAchievementMath.Text = Convert.ToString(achievementMath[0].Total); else txtTotalAchievementMath.Text = "";
                        if (achievementMath[0].ExtraFiled1 >=0) txtEnglishLearnerAchievementMath.Text = Convert.ToString(achievementMath[0].ExtraFiled1); else txtEnglishLearnerAchievementMath.Text = "";
                    }
                    else
                    {
                        //MagnetGPRAPerformanceMeasure pr = new MagnetGPRAPerformanceMeasure();
                        //pr.ReportType = 6;
                        //pr.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                        //pr.Save();
                        //hfAchievementMath.Value = pr.ID.ToString();
                    }
                }
                else
                {
                    //MagnetGPRA item = new MagnetGPRA();
                    //item.ReportID = Convert.ToInt32(hfReportID.Value);
                    //item.SchoolID = schoolID;
                    //item.Save();
                    //hfID.Value = item.ID.ToString();

                    //MagnetGPRAPerformanceMeasure pr = new MagnetGPRAPerformanceMeasure();
                    //pr.ReportType = 3;
                    //pr.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    //pr.Save();
                    //hfParticipationReading.Value = pr.ID.ToString();

                    //MagnetGPRAPerformanceMeasure pm = new MagnetGPRAPerformanceMeasure();
                    //pm.ReportType = 4;
                    //pm.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    //pm.Save();
                    //hfParticipationMath.Value = pm.ID.ToString();

                    //MagnetGPRAPerformanceMeasure ar = new MagnetGPRAPerformanceMeasure();
                    //ar.ReportType = 5;
                    //ar.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    //ar.Save();
                    //hfAchievementReading.Value = ar.ID.ToString();

                    //MagnetGPRAPerformanceMeasure am = new MagnetGPRAPerformanceMeasure();
                    //am.ReportType = 6;
                    //am.MagnetGPRAID = Convert.ToInt32(hfID.Value);
                    //am.Save();
                    //hfAchievementMath.Value = am.ID.ToString();
                }
            }
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        int schoolID = Convert.ToInt32(Request.QueryString["id"]);
        if (string.IsNullOrEmpty(hfID.Value))
        {
            MagnetGPRA item = new MagnetGPRA();
            item.ReportID = Convert.ToInt32(hfReportID.Value);
            item.SchoolID = schoolID;
            item.Save();
            hfID.Value = item.ID.ToString();
        }

        if (string.IsNullOrEmpty(hfParticipationReading.Value))
        {
            MagnetGPRAPerformanceMeasure pr = new MagnetGPRAPerformanceMeasure();
            pr.ReportType = 3;
            pr.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            pr.Save();
            hfParticipationReading.Value = pr.ID.ToString();
        }

        if (string.IsNullOrEmpty(hfParticipationMath.Value))
        {
            MagnetGPRAPerformanceMeasure pm = new MagnetGPRAPerformanceMeasure();
            pm.ReportType = 4;
            pm.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            pm.Save();
            hfParticipationMath.Value = pm.ID.ToString();
        }

        if (string.IsNullOrEmpty(hfAchievementReading.Value))
        {
            MagnetGPRAPerformanceMeasure ar = new MagnetGPRAPerformanceMeasure();
            ar.ReportType = 5;
            ar.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            ar.Save();
            hfAchievementReading.Value = ar.ID.ToString();
        }

        if (string.IsNullOrEmpty(hfAchievementMath.Value))
        {
            MagnetGPRAPerformanceMeasure am = new MagnetGPRAPerformanceMeasure();
            am.ReportType = 6;
            am.MagnetGPRAID = Convert.ToInt32(hfID.Value);
            am.Save();
            hfAchievementMath.Value = am.ID.ToString();
        }

        MagnetGPRAPerformanceMeasure participationReading = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfParticipationReading.Value));
        participationReading.Indian = string.IsNullOrEmpty(txtIndianParticipationReading.Text) ? -1 : Convert.ToInt32(txtIndianParticipationReading.Text);
        participationReading.Asian = string.IsNullOrEmpty(txtAsianParticipationReading.Text) ? -1 : Convert.ToInt32(txtAsianParticipationReading.Text);
        participationReading.Black = string.IsNullOrEmpty(txtBlackParticipationReading.Text) ? -1 : Convert.ToInt32(txtBlackParticipationReading.Text);
        participationReading.Hispanic = string.IsNullOrEmpty(txtHispanicParticipationReading.Text) ? -1 : Convert.ToInt32(txtHispanicParticipationReading.Text);
        participationReading.Hawaiian = string.IsNullOrEmpty(txtHawaiianParticipationReading.Text) ? -1 : Convert.ToInt32(txtHawaiianParticipationReading.Text);
        participationReading.White = string.IsNullOrEmpty(txtWhiteParticipationReading.Text) ? -1 : Convert.ToInt32(txtWhiteParticipationReading.Text);
        participationReading.MultiRaces = string.IsNullOrEmpty(txtMultiRaceParticipationReading.Text) ? -1 : Convert.ToInt32(txtMultiRaceParticipationReading.Text);
        participationReading.ExtraFiled2 = string.IsNullOrEmpty(txtDisadvantagedParticipationReading.Text) ? -1 : Convert.ToInt32(txtDisadvantagedParticipationReading.Text);
        participationReading.Total = string.IsNullOrEmpty(txtTotalParticipationReading.Text) ? -1 : Convert.ToInt32(txtTotalParticipationReading.Text);
        participationReading.ExtraFiled1 = string.IsNullOrEmpty(txtEnglishLearnerParticipationReading.Text) ? -1 : Convert.ToInt32(txtEnglishLearnerParticipationReading.Text);
        participationReading.Save();
        //hfParticipationReading.Value = participationReading.ID.ToString();

        MagnetGPRAPerformanceMeasure participationMath = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfParticipationMath.Value));
        participationMath.Indian = string.IsNullOrEmpty(txtIndianParticipationMath.Text) ? -1 : Convert.ToInt32(txtIndianParticipationMath.Text);
        participationMath.Asian = string.IsNullOrEmpty(txtAsianParticipationMath.Text) ? -1 : Convert.ToInt32(txtAsianParticipationMath.Text);
        participationMath.Black = string.IsNullOrEmpty(txtBlackParticipationMath.Text) ? -1 : Convert.ToInt32(txtBlackParticipationMath.Text);
        participationMath.Hispanic = string.IsNullOrEmpty(txtHispanicParticipationMath.Text) ? -1 : Convert.ToInt32(txtHispanicParticipationMath.Text);
        participationMath.Hawaiian = string.IsNullOrEmpty(txtHawaiianParticipationMath.Text) ? -1 : Convert.ToInt32(txtHawaiianParticipationMath.Text);
        participationMath.White = string.IsNullOrEmpty(txtWhiteParticipationMath.Text) ? -1 : Convert.ToInt32(txtWhiteParticipationMath.Text);
        participationMath.MultiRaces = string.IsNullOrEmpty(txtMultiRaceParticipationMath.Text) ? -1 : Convert.ToInt32(txtMultiRaceParticipationMath.Text);
        participationMath.ExtraFiled2 = string.IsNullOrEmpty(txtDisadvantagedParticipationMath.Text) ? -1 : Convert.ToInt32(txtDisadvantagedParticipationMath.Text);
        participationMath.Total = string.IsNullOrEmpty(txtTotalParticipationMath.Text) ? -1 : Convert.ToInt32(txtTotalParticipationMath.Text);
        participationMath.ExtraFiled1 = string.IsNullOrEmpty(txtEnglishLearnerParticipationMath.Text) ? -1 : Convert.ToInt32(txtEnglishLearnerParticipationMath.Text);
        participationMath.Save();
        //hfParticipationMath.Value = participationMath.ID.ToString();

        MagnetGPRAPerformanceMeasure achievementReading = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfAchievementReading.Value));
        achievementReading.Indian = string.IsNullOrEmpty(txtIndianAchievementReading.Text) ? -1 : Convert.ToInt32(txtIndianAchievementReading.Text);
        achievementReading.Asian = string.IsNullOrEmpty(txtAsianAchievementReading.Text) ? -1 : Convert.ToInt32(txtAsianAchievementReading.Text);
        achievementReading.Black = string.IsNullOrEmpty(txtBlackAchievementReading.Text) ? -1 : Convert.ToInt32(txtBlackAchievementReading.Text);
        achievementReading.Hispanic = string.IsNullOrEmpty(txtHispanicAchievementReading.Text) ? -1 : Convert.ToInt32(txtHispanicAchievementReading.Text);
        achievementReading.Hawaiian = string.IsNullOrEmpty(txtHawaiianAchievementReading.Text) ? -1 : Convert.ToInt32(txtHawaiianAchievementReading.Text);
        achievementReading.White = string.IsNullOrEmpty(txtWhiteAchievementReading.Text) ? -1 : Convert.ToInt32(txtWhiteAchievementReading.Text);
        achievementReading.MultiRaces = string.IsNullOrEmpty(txtMultiRaceAchievementReading.Text) ? -1 : Convert.ToInt32(txtMultiRaceAchievementReading.Text);
        achievementReading.ExtraFiled2 = string.IsNullOrEmpty(txtDisadvantagedAchievementReading.Text) ? -1 : Convert.ToInt32(txtDisadvantagedAchievementReading.Text);
        achievementReading.Total = string.IsNullOrEmpty(txtTotalAchievementReading.Text) ? -1 : Convert.ToInt32(txtTotalAchievementReading.Text);
        achievementReading.ExtraFiled1 = string.IsNullOrEmpty(txtEnglishLearnerAchievementReading.Text) ? -1 : Convert.ToInt32(txtEnglishLearnerAchievementReading.Text);
        achievementReading.Save();
        //hfAchievementReading.Value = achievementReading.ID.ToString();

        MagnetGPRAPerformanceMeasure achievementMath = MagnetGPRAPerformanceMeasure.SingleOrDefault(x => x.ID == Convert.ToInt32(hfAchievementMath.Value));
        achievementMath.Indian = string.IsNullOrEmpty(txtIndianAchievementMath.Text) ? -1 : Convert.ToInt32(txtIndianAchievementMath.Text);
        achievementMath.Asian = string.IsNullOrEmpty(txtAsianAchievementMath.Text) ? -1 : Convert.ToInt32(txtAsianAchievementMath.Text);
        achievementMath.Black = string.IsNullOrEmpty(txtBlackAchievementMath.Text) ? -1 : Convert.ToInt32(txtBlackAchievementMath.Text);
        achievementMath.Hispanic = string.IsNullOrEmpty(txtHispanicAchievementMath.Text) ? -1 : Convert.ToInt32(txtHispanicAchievementMath.Text);
        achievementMath.Hawaiian = string.IsNullOrEmpty(txtHawaiianAchievementMath.Text) ? -1 : Convert.ToInt32(txtHawaiianAchievementMath.Text);
        achievementMath.White = string.IsNullOrEmpty(txtWhiteAchievementMath.Text) ? -1 : Convert.ToInt32(txtWhiteAchievementMath.Text);
        achievementMath.MultiRaces = string.IsNullOrEmpty(txtMultiRaceAchievementMath.Text) ? -1 : Convert.ToInt32(txtMultiRaceAchievementMath.Text);
        achievementMath.ExtraFiled2 = string.IsNullOrEmpty(txtDisadvantagedAchievementMath.Text) ? -1 : Convert.ToInt32(txtDisadvantagedAchievementMath.Text);
        achievementMath.Total = string.IsNullOrEmpty(txtTotalAchievementMath.Text) ? -1 : Convert.ToInt32(txtTotalAchievementMath.Text);
        achievementMath.ExtraFiled1 = string.IsNullOrEmpty(txtEnglishLearnerAchievementMath.Text) ? -1 : Convert.ToInt32(txtEnglishLearnerAchievementMath.Text);
        achievementMath.Save();
        //hfAchievementMath.Value = achievementMath.ID.ToString();

        var muhs = MagnetUpdateHistory.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value) && x.FormIndex == 3);
        if (muhs.Count >0)
        {
            muhs[0].UpdateUser = Context.User.Identity.Name;
            muhs[0].TimeStamp = DateTime.Now;
            muhs[0].Save();
        }
        else
        {
            MagnetUpdateHistory muh = new MagnetUpdateHistory();
            muh.ReportID = Convert.ToInt32(hfReportID.Value);
            muh.FormIndex = 3;
            muh.UpdateUser = Context.User.Identity.Name;
            muh.TimeStamp = DateTime.Now;
            muh.Save();
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data have been saved.');</script>", false);
    }
    /*protected void OnParticipationReading(object sender, EventArgs e)
    {
        string str = (sender as TextBox).Text;
        int total = 0;
        if (int.TryParse(str, out total))
        {
            total = 0;
            if (!string.IsNullOrEmpty(txtIndianParticipationReading.Text)) total += Convert.ToInt32(txtIndianParticipationReading.Text);
            if (!string.IsNullOrEmpty(txtAsianParticipationReading.Text)) total += Convert.ToInt32(txtAsianParticipationReading.Text);
            if (!string.IsNullOrEmpty(txtBlackParticipationReading.Text)) total += Convert.ToInt32(txtBlackParticipationReading.Text);
            if (!string.IsNullOrEmpty(txtHispanicParticipationReading.Text)) total += Convert.ToInt32(txtHispanicParticipationReading.Text);
            if (!string.IsNullOrEmpty(txtHawaiianParticipationReading.Text)) total += Convert.ToInt32(txtHawaiianParticipationReading.Text);
            if (!string.IsNullOrEmpty(txtWhiteParticipationReading.Text)) total += Convert.ToInt32(txtWhiteParticipationReading.Text);
            //if (!string.IsNullOrEmpty(txtDisadvantagedParticipationReading.Text)) total += Convert.ToInt32(txtDisadvantagedParticipationReading.Text);
            //if (!string.IsNullOrEmpty(txtEnglishLearnerParticipationReading.Text)) total += Convert.ToInt32(txtEnglishLearnerParticipationReading.Text);
            txtTotalParticipationReading.Text = Convert.ToString(total);
        }
        TextBox[] controls = { txtIndianParticipationReading, txtAsianParticipationReading, txtBlackParticipationReading ,
                             txtHispanicParticipationReading, txtHawaiianParticipationReading, txtWhiteParticipationReading,
                             txtDisadvantagedParticipationReading, txtEnglishLearnerParticipationReading};
        int idx = (controls.ToList()).IndexOf((sender as TextBox));
        if (idx < (controls.Count() - 1))
            controls[idx + 1].Focus();
        else
            txtIndianParticipationMath.Focus();
    }
    protected void OnParticipationMath(object sender, EventArgs e)
    {
        string str = (sender as TextBox).Text;
        int total = 0;
        if (int.TryParse(str, out total))
        {
            total = 0;
            if (!string.IsNullOrEmpty(txtIndianParticipationMath.Text)) total += Convert.ToInt32(txtIndianParticipationMath.Text);
            if (!string.IsNullOrEmpty(txtAsianParticipationMath.Text)) total += Convert.ToInt32(txtAsianParticipationMath.Text);
            if (!string.IsNullOrEmpty(txtBlackParticipationMath.Text)) total += Convert.ToInt32(txtBlackParticipationMath.Text);
            if (!string.IsNullOrEmpty(txtHispanicParticipationMath.Text)) total += Convert.ToInt32(txtHispanicParticipationMath.Text);
            if (!string.IsNullOrEmpty(txtHawaiianParticipationMath.Text)) total += Convert.ToInt32(txtHawaiianParticipationMath.Text);
            if (!string.IsNullOrEmpty(txtWhiteParticipationMath.Text)) total += Convert.ToInt32(txtWhiteParticipationMath.Text);
            //if (!string.IsNullOrEmpty(txtDisadvantagedParticipationMath.Text)) total += Convert.ToInt32(txtDisadvantagedParticipationMath.Text);
            //if (!string.IsNullOrEmpty(txtEnglishLearnerParticipationMath.Text)) total += Convert.ToInt32(txtEnglishLearnerParticipationMath.Text);
            txtTotalParticipationMath.Text = Convert.ToString(total);
        }
        TextBox[] controls = { txtIndianParticipationMath, txtAsianParticipationMath, txtBlackParticipationMath ,
                             txtHispanicParticipationMath, txtHawaiianParticipationMath, txtWhiteParticipationMath,
                             txtDisadvantagedParticipationMath, txtEnglishLearnerParticipationMath};
        int idx = (controls.ToList()).IndexOf((sender as TextBox));
        if (idx < (controls.Count() - 1))
            controls[idx + 1].Focus();
        else
            txtIndianAchievementReading.Focus();
    }
    protected void OnArchieveReading(object sender, EventArgs e)
    {
        string str = (sender as TextBox).Text;
        int total = 0;
        if (int.TryParse(str, out total))
        {
            total = 0;
            if (!string.IsNullOrEmpty(txtIndianAchievementReading.Text)) total += Convert.ToInt32(txtIndianAchievementReading.Text);
            if (!string.IsNullOrEmpty(txtAsianAchievementReading.Text)) total += Convert.ToInt32(txtAsianAchievementReading.Text);
            if (!string.IsNullOrEmpty(txtBlackAchievementReading.Text)) total += Convert.ToInt32(txtBlackAchievementReading.Text);
            if (!string.IsNullOrEmpty(txtHispanicAchievementReading.Text)) total += Convert.ToInt32(txtHispanicAchievementReading.Text);
            if (!string.IsNullOrEmpty(txtHawaiianAchievementReading.Text)) total += Convert.ToInt32(txtHawaiianAchievementReading.Text);
            if (!string.IsNullOrEmpty(txtWhiteAchievementReading.Text)) total += Convert.ToInt32(txtWhiteAchievementReading.Text);
            //if (!string.IsNullOrEmpty(txtDisadvantagedAchievementReading.Text)) total += Convert.ToInt32(txtDisadvantagedAchievementReading.Text);
            //if (!string.IsNullOrEmpty(txtEnglishLearnerAchievementReading.Text)) total += Convert.ToInt32(txtEnglishLearnerAchievementReading.Text);
            txtTotalAchievementReading.Text = Convert.ToString(total);
        }
        TextBox[] controls = { txtIndianAchievementReading, txtAsianAchievementReading, txtBlackAchievementReading ,
                             txtHispanicAchievementReading, txtHawaiianAchievementReading, txtWhiteAchievementReading,
                             txtDisadvantagedAchievementReading, txtEnglishLearnerAchievementReading};
        int idx = (controls.ToList()).IndexOf((sender as TextBox));
        if (idx < (controls.Count() - 1))
            controls[idx + 1].Focus();
        else
            txtIndianAchievementMath.Focus();
    }
    protected void OnArchieveMath(object sender, EventArgs e)
    {
        string str = (sender as TextBox).Text;
        int total = 0;
        if (int.TryParse(str, out total))
        {
            total = 0;
            if (!string.IsNullOrEmpty(txtIndianAchievementMath.Text)) total += Convert.ToInt32(txtIndianAchievementMath.Text);
            if (!string.IsNullOrEmpty(txtAsianAchievementMath.Text)) total += Convert.ToInt32(txtAsianAchievementMath.Text);
            if (!string.IsNullOrEmpty(txtBlackAchievementMath.Text)) total += Convert.ToInt32(txtBlackAchievementMath.Text);
            if (!string.IsNullOrEmpty(txtHispanicAchievementMath.Text)) total += Convert.ToInt32(txtHispanicAchievementMath.Text);
            if (!string.IsNullOrEmpty(txtHawaiianAchievementMath.Text)) total += Convert.ToInt32(txtHawaiianAchievementMath.Text);
            if (!string.IsNullOrEmpty(txtWhiteAchievementMath.Text)) total += Convert.ToInt32(txtWhiteAchievementMath.Text);
            //if (!string.IsNullOrEmpty(txtDisadvantagedAchievementMath.Text)) total += Convert.ToInt32(txtDisadvantagedAchievementMath.Text);
            //if (!string.IsNullOrEmpty(txtEnglishLearnerAchievementMath.Text)) total += Convert.ToInt32(txtEnglishLearnerAchievementMath.Text);
            txtTotalAchievementMath.Text = Convert.ToString(total);
        }
        TextBox[] controls = { txtIndianAchievementMath, txtAsianAchievementMath, txtBlackAchievementMath ,
                             txtHispanicAchievementMath, txtHawaiianAchievementMath, txtWhiteAchievementMath,
                             txtDisadvantagedAchievementMath, txtEnglishLearnerAchievementMath};
        int idx = (controls.ToList()).IndexOf((sender as TextBox));
        if (idx < (controls.Count() - 1))
            controls[idx + 1].Focus();
        else
            txtEnglishLearnerAchievementMath.Focus();
    }*/
}