﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="GPRAII.aspx.cs" Inherits="admin_data_gpraii" ErrorPage="~/Error_page.aspx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit GPRA Performance Measure 1 Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
            $('.Applicant').keyup(function () {
                var total;
                total = 0;
                $('.Applicant').each(function (index) {
                    //$(this).val((!$(this).val() ? "0" : $(this).val()));
                    if ($(this).val())
                        total = total + parseInt($(this).val(), 10);
                });
                $('#txtTotalSutdents').val(total.toString());
            });
            $('.Enrollment').keyup(function () {
                var total;
                total = 0;
                $('.Enrollment1').each(function (index) {
                    if ($(this).val())
                        total = total + parseInt($(this).val(), 10);
                });
                $('#txtNewStudentEnrollment').val(total.toString());
                $('#<%=hidNewStudentEnrollment.ClientID %>').val(total.toString());

                $('.Enrollment2').each(function (index) {
                    if ($(this).val())
                        total = total + parseInt($(this).val(), 10);
                });
                $('#txtTotalSutdentsEnrollment').val(total.toString());
            });

            function UnloadConfirm() {
                var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
                if (tmp == "0")
                    return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
            }
            $(function () {
                $(".postbutton").click(function () {
                    $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
                });
                $(".change").change(function () {
                    $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
                });
                window.onbeforeunload = UnloadConfirm;
            });
            function setTotal(index) {
                var nIndian = $('#<%=TxtNewIndianEnrollment.ClientID %>').val();
                var nAsian = $('#<%=TxtNewAsianEnrollment.ClientID %>').val();
                var nBlack = $('#<%=txtNewBlackEnrollment.ClientID %>').val();
                var nHispanic = $('#<%=txtNewHispanicEnrollment.ClientID %>').val();
                var nHawaiian = $('#<%=txtNewHawaiianEnrollment.ClientID %>').val();
                var nWhite = $('#<%=txtNewWhiteEnrollment.ClientID %>').val();
                var nMultiRaces = $('#<%=txtNewMultiRacesEnrollment.ClientID %>').val();

                var cIndian = $('#<%=TxtContIndianEnrollment.ClientID %>').val();
                var cAsian = $('#<%=TxtContAsianEnrollment.ClientID %>').val();
                var cBlack = $('#<%=txtContBlackEnrollment.ClientID %>').val();
                var cHispanic = $('#<%=txtContHispanicEnrollment.ClientID %>').val();
                var cHawaiian = $('#<%=txtContHawaiianEnrollment.ClientID %>').val();
                var cWhite = $('#<%=txtContWhiteEnrollment.ClientID %>').val();
                var cMultiRaces = $('#<%=txtContMultiRacesEnrollment.ClientID %>').val();

                if (index == 1) {
                    var tmp = (nIndian == "" ? 0 : parseInt(nIndian)) + (nAsian == "" ? 0 : parseInt(nAsian)) + (nBlack == "" ? 0 : parseInt(nBlack)) + (nHispanic == "" ? 0 : parseInt(nHispanic)) + (nHawaiian == "" ? 0 : parseInt(nHawaiian)) + (nWhite == "" ? 0 : parseInt(nWhite)) + (nMultiRaces == "" ? 0 : parseInt(nMultiRaces));
                    $('#<%=txtNewStudents.ClientID %>').val(tmp.toString());
                    $('#<%=hidNewStudents.ClientID %>').val(tmp.toString());
                }
                else {
                    var tmp = (cIndian == "" ? 0 : parseInt(cIndian)) + (cAsian == "" ? 0 : parseInt(cAsian)) + (cBlack == "" ? 0 : parseInt(cBlack)) + (cHispanic == "" ? 0 : parseInt(cHispanic)) + (cHawaiian == "" ? 0 : parseInt(cHawaiian)) + (cWhite == "" ? 0 : parseInt(cWhite)) + (cMultiRaces == "" ? 0 : parseInt(cMultiRaces));
                    $('#<%=txtContinuingEnrollment.ClientID %>').val(tmp.toString());
                    $('#<%=hidContinuingEnrollment.ClientID %>').val(tmp.toString());
                }

                var v25 = $('#<%=txtNewStudents.ClientID %>').val() == "" ? 0 : parseInt($('#<%=txtNewStudents.ClientID %>').val());
                var v26 = $('#<%=txtNewStudentEnrollment.ClientID %>').val() == "" ? 0 : parseInt($('#<%=txtNewStudentEnrollment.ClientID %>').val());
                var v27 = $('#<%=txtContinuingEnrollment.ClientID %>').val() == "" ? 0 : parseInt($('#<%=txtContinuingEnrollment.ClientID %>').val());
                var total = v25 + v26 + v27;

                $('#<%=txtTotalSutdentsEnrollment.ClientID %>').val(total);
            }
            // 26(a) - 26(g)
            $('#<%=TxtNewIndianEnrollment.ClientID %>').keyup(function () {
                setTotal(1);
            });

            $('#<%=TxtNewAsianEnrollment.ClientID %>').keyup(function () {
                setTotal(1);
            });

            $('#<%=txtNewBlackEnrollment.ClientID %>').keyup(function () {
                setTotal(1);
            });

            $('#<%=txtNewHispanicEnrollment.ClientID %>').keyup(function () {
                setTotal(1);
            });

            $('#<%=txtNewHawaiianEnrollment.ClientID %>').keyup(function () {
                setTotal(1);
            });

            $('#<%=txtNewWhiteEnrollment.ClientID %>').keyup(function () {
                setTotal(1);
            });
            $('#<%=txtNewMultiRacesEnrollment.ClientID %>').keyup(function () {
                setTotal(1);
            });

            // 27(a) - 27(g)
            $('#<%=TxtContIndianEnrollment.ClientID %>').keyup(function () {
                setTotal(2);
            });

            $('#<%=TxtContAsianEnrollment.ClientID %>').keyup(function () {
                setTotal(2);
            });

            $('#<%=txtContBlackEnrollment.ClientID %>').keyup(function () {
                setTotal(2);
            });

            $('#<%=txtContHispanicEnrollment.ClientID %>').keyup(function () {
                setTotal(2);
            });

            $('#<%=txtContHawaiianEnrollment.ClientID %>').keyup(function () {
                setTotal(2);
            });

            $('#<%=txtContWhiteEnrollment.ClientID %>').keyup(function () {
                setTotal(2);
            });
            $('#<%=txtContMultiRacesEnrollment.ClientID %>').keyup(function () {
                setTotal(2);
            });

            $(".rw26").hide();
            $(".rw27").hide();

            $("#img26").click(function () {
              var oldSrc = $("#img26").attr('src');
               if (oldSrc.search('right') != -1) {
                  $("#img26").attr('src', oldSrc.replace('right', 'down'));
                   $(".rw26").show();
               }
               else {
                   $("#img26").attr('src', oldSrc.replace('down', 'right'));
                   $(".rw26").hide();
               }

            });

		   $("#img27").click(function () {
                var oldSrc = $("#img27").attr('src');
                if (oldSrc.search('right') != -1) {
                    $("#img27").attr('src', oldSrc.replace('right', 'down'));
                    $(".rw27").show();
                }
                else {
                    $("#img27").attr('src', oldSrc.replace('down', 'right'));
                    $(".rw27").hide();
                }

            });
		   
	
	
            $('#<%=txtMultiRacesEnrollment.ClientID %>').bind('keydown', function (e) {
                var oldSrc = $("#img26").attr('src');
                if (e.which == 9) {  //tab keycode =9
                    if (oldSrc.search('right') != -1) {   //right arrow
                        $("#img26").attr('src', oldSrc.replace('right', 'down'));
                        $(".rw26").show();
                        $('#<%=txtNewStudentEnrollment.ClientID %>').focus();
                    }

                }
            });

            $('#<%=txtNewMultiRacesEnrollment.ClientID %>').bind('keydown', function (e) {
                var oldSrc = $("#img27").attr('src');
                if (e.which == 9) {  //tab keycode =9
                    if (oldSrc.search('right') != -1) {   //right arrow
                        $("#img27").attr('src', oldSrc.replace('right', 'down'));
                        $(".rw27").show();
                        $('#<%=txtNewStudents.ClientID %>').focus();
                    }
                }
            });
        });
    </script>
    <style type="text/css">
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
        }
        .msapDataTbl th
        {
            color: #000;
        }
        .style1
        {
            width: 246px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h4 class="text_nav">
            
            <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
            <a href="managegpra.aspx">GPRA Table</a> <span class="greater_than">&gt;</span>
            Part II. GPRA Performance Measure 1 Data</h4>
        <div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />

        <div class="GPRA_titles">
            <asp:Label ID="lblSchoolName" runat="server" Width="380px"></asp:Label></div>
        <div class="tab_area">
            <ul class="tabs">
                <li><a runat="server" id="tabPartVI" href="">Part VI</a></li>
                <li><a runat="server" id="tabPartV" href="">Part V</a></li>
                <li><a runat="server" id="tabPartIV" href="">Part IV</a></li>
                <li><a runat="server" id="tabPartIII" href="">Part III</a></li>
                <li class="tab_active">Part II</li>
                <li><a runat="server" id="tabPartI" href="">Part I</a></li>
            </ul>
        </div>
        <br />
        <span style="color: #f58220;">
            <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Part II.
                GPRA Performance Measure 1 Data</b> </span>
        <p>
            Part II of the GPRA Table collects data for the minority group isolation GPRA Performance Measure, in which applicant pool and student enrollment data are reported. When you finish entering data for this page, click on Save Record before proceeding.
       </p>
        <asp:Panel ID ="pnlMeasure1Des" runat="server" Visible="false">
            <ul style="list-style-type: none">
                <li><b>GPRA Measure 1</b>
                    :&nbsp; The percentage of magnet schools receiving assistance whose student 
                    enrollment reduces, eliminates, or prevents<br />
minority group isolation.
        </li>
        </ul>
    </asp:Panel>
    <p>
    Report both student applicant pool and enrollment data in this section of the GPRA table. Use data reported as of October 1.
    </p>
     <asp:ValidationSummary 
         ID="ValSum" runat="server" 
         DisplayMode="SingleParagraph"
         HeaderText="The following fields with * are required. " ShowSummary="true" />
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hidNewStudentEnrollment" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfApplicationPool" runat="server" />
        <asp:HiddenField ID="hfEnrollment" runat="server" />
        <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
        <asp:HiddenField ID="hidContinuingEnrollment" runat="server" />
        <asp:HiddenField  ID="hidNewStudents" runat="server" />
        <div style="margin-top: 20px;">
            <table class="msapDataTbl">
                <tr>
                    <td colspan="3" align="center" class="TDWithBottomBorder" style="color: #F58220 !important; font-weight: bold;">
                        Applicant 
                        pool data
                    </td>
                    <td colspan="3" align="center" class="TDWithBottomBorder" style="color: #F58220; font-weight: bold;">
                        Enrollment data
                    </td>
                </tr>
                <tr>
                    <th class="style1" style="color: #4E8396; font-weight: normal;">Number of applications received for
                        <img src="../../images/question_mark-thumb.png" title="Report only the number of student applications received. <b>Refer to <i>GPRA Guide</i> for instructions.</b>" />
                    </th>
                    <th colspan="2" class="TDWithBorder" style="color: #4E8396; font-weight: normal;">
                        <asp:Label ID="lblReportPeriod" runat="server"></asp:Label>
                    </th>
                    <th class="TDWithBorder LeftAlignCell" style="color: #4E8396; font-weight: normal;">
                        Number of students who applied and then enrolled</th>
                    <th colspan="2" style="color: #4E8396; font-weight: normal;">
                        <asp:Label ID="lblReportPeriod2" runat="server"></asp:Label>
                    </th>
                </tr>
                <tr>
                    <td class="style1">
                        10. <b>American Indian or Alaska Native</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtIndian" runat="server" CssClass="msapDataTxtSmall Applicant change"
                            TabIndex="200" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers"
                            TargetControlID="txtIndian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtIndian" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                   <td>
                        18. <b>American Indian or Alaska Native</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                       <asp:TextBox ID="txtIndianEnrollment" runat="server" CssClass="msapDataTxtSmall Enrollment Enrollment1 change"
                            TabIndex="210" MaxLength="6" />
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers"
                            TargetControlID="txtIndianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                      </td>
                      <td>
                      <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator14" ControlToValidate="txtIndianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                      </td>
                </tr>
                <tr>
                    <td class="style1">
                        11. <b>Asian</b> students</td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtAsian" runat="server" CssClass="msapDataTxtSmall Applicant change"
                            TabIndex="201" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtAsian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtAsian" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                    <td>
                        19. <b>Asian</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtAsianEnrollment" runat="server" CssClass="msapDataTxtSmall Enrollment Enrollment1 change"
                            TabIndex="211" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Numbers"
                            TargetControlID="txtAsianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtAsianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        12. <b>Black or African-American</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtBlack" runat="server" CssClass="msapDataTxtSmall Applicant change"
                            TabIndex="202" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                            TargetControlID="txtBlack">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtBlack" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                    <td>
                        20. <b>Black or African-American</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtBlackEnrollment" runat="server" CssClass="msapDataTxtSmall Enrollment Enrollment1 change"
                            TabIndex="212" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" FilterType="Numbers"
                            TargetControlID="txtBlackEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtBlackEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        13. <b>Hispanic or Latino</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtHispanic" runat="server" CssClass="msapDataTxtSmall Applicant change"
                            TabIndex="203" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                            TargetControlID="txtHispanic">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtHispanic" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                    <td>
                        21. <b>Hispanic or Latino</b> students</td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtHispanicEnrollment" runat="server" CssClass="msapDataTxtSmall Enrollment Enrollment1 change"
                            TabIndex="213" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterType="Numbers"
                            TargetControlID="txtHispanicEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtHispanicEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="style1" >
                        14. <b>Native Hawaiian or Other Pacific Islander</b> students</td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtHawaiian" runat="server" CssClass="msapDataTxtSmall Applicant change"
                            TabIndex="204" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                            TargetControlID="txtHawaiian">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtHawaiian" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                    <td>
                        22. <b>Native Hawaiian or Other Pacific Islander</b> students</td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtHawaiianEnrollment" runat="server" CssClass="msapDataTxtSmall Enrollment Enrollment1 change"
                            TabIndex="214" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" FilterType="Numbers"
                            TargetControlID="txtHawaiianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtHawaiianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="style1" >
                        15. <b>White</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtWhite" runat="server" CssClass="msapDataTxtSmall Applicant change"
                            TabIndex="205" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                            TargetControlID="txtWhite">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtWhite" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                    <td>
                        23. <b>White</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtWhiteEnrollment" runat="server" CssClass="msapDataTxtSmall Enrollment Enrollment1 change"
                            TabIndex="215" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterType="Numbers"
                            TargetControlID="txtWhiteEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtWhiteEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr id="MultiRacesRow" runat="server">
                    <td class="style1">
                        16. <b>Two or more races</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtMultiRaces" runat="server" CssClass="msapDataTxtSmall Applicant change"
                            TabIndex="206" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                            TargetControlID="txtMultiRaces">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="txtMultiRaces" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                    <td>
                        24. <b>Two or more races</b> students</td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtMultiRacesEnrollment" runat="server" CssClass="msapDataTxtSmall Enrollment Enrollment1 change"
                            TabIndex="216" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                            TargetControlID="txtMultiRacesEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" ControlToValidate="txtMultiRacesEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblUndeclared" runat="server" Text="16a. <b>Unknown</b>"></asp:Label>
                        <img src="../../images/question_mark-thumb.png" title="Contact your program officer for permission to enter data into this field. " />
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtUndeclared" runat="server" CssClass="msapDataTxtSmall Applicant change" BackColor="LightGray"
                            TabIndex="207" MaxLength="6" Enabled="false"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers"
                            TargetControlID="txtUndeclared">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td></td>
                    <td >
                        25. Total students who applied and then enrolled (Items 18-24)
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 18-24." />
                    </td>
                    <td colspan="2" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtNewStudentEnrollment" runat="server" CssClass="msapDataTxtSmall" BackColor="LightGray"
                            ClientIDMode="Static" Enabled="false" TabIndex="217"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        17. Total students who applied (Items 10-16a) 
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 10-16a." />
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtTotalSutdents" ClientIDMode="Static" runat="server" BackColor="LightGray" CssClass="msapDataTxtSmall" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" ControlToValidate="txtTotalSutdents" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                    <td colspan="2" style="border-right:0px;" >
                    <span style="color:#4E8396;vertical-align:top;">Number of new students who enrolled but were not required<br /> to apply for admission</span>
                    <span style="padding-left:374px;" class="GPRAarrow"><img id="img26" src="../../img/arrow_right.png"/></span>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr class="rw26">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>26a. <b>American Indian or Alaska Native</b> students </td>
                 <td style="border-right: 0px !important;">
                        <asp:TextBox ID="TxtNewIndianEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="218" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers"
                            TargetControlID="TxtNewIndianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" ControlToValidate="TxtNewIndianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr class="rw26">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>26b.  <b>Asian</b> students </td>
                 <td style="border-right: 0px !important;">
                        <asp:TextBox ID="TxtNewAsianEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="219" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers"
                            TargetControlID="TxtNewAsianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" ControlToValidate="TxtNewAsianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr class="rw26">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>26c.  <b>Black or African-American</b> students</td>
                 <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtNewBlackEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="220" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers"
                            TargetControlID="txtNewBlackEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator18" ControlToValidate="txtNewBlackEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr class="rw26">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>26d.  <b>Hispanic or Latino</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtNewHispanicEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="221" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers"
                            TargetControlID="txtNewHispanicEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator19" ControlToValidate="txtNewHispanicEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr class="rw26">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>26e.  <b>Native Hawaiian or Other Pacific Islander</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtNewHawaiianEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="222" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Numbers"
                            TargetControlID="txtNewHawaiianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator20" ControlToValidate="txtNewHawaiianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr class="rw26">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>26f.   <b>White</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtNewWhiteEnrollment" runat="server" TabIndex="223" MaxLength="6" CssClass="msapDataTxtSmall change"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Numbers" 
                            TargetControlID="txtNewWhiteEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator21" ControlToValidate="txtNewWhiteEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr class="rw26">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>26g. <b>Two or more races</b> students
                    </td>
                    <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtNewMultiRacesEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="224" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Numbers"
                            TargetControlID="txtNewMultiRacesEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator22" ControlToValidate="txtNewMultiRacesEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                <td class="style1"></td>
                <td colspan="2"></td>
                  <td>
                        26h. 
                        Total number of new students not required to apply for admission (Items 26a-26g)
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 26(a)-26(g)." /></td>
                    <td colspan="2" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtNewStudents" runat="server" ClientIDMode="Static" BackColor="LightGray" CssClass="msapDataTxtSmall Enrollment2 Enrollment change" 
                          Enabled="false"    TabIndex="225"></asp:TextBox>
                    </td>
                </tr>
                <tr >
                    <td class="style1">
                    </td>
                    <td colspan="2">
                    </td>
                    <td colspan="3" style="border-right:0px;">
           
                     <span style="color:#4E8396; vertical-align:top; ">Number of continuing students who enrolled</span>
                  <span style="padding-left:150px;" class="GPRAarrow"><img id="img27" src="../../img/arrow_right.png"  /></span>
                  </td>
                </tr>
                <tr class="rw27">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>27a. <b>American Indian or Alaska Native</b> students </td>
                 <td style="border-right: 0px !important;">
                        <asp:TextBox ID="TxtContIndianEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="226" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Numbers"
                            TargetControlID="TxtContIndianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator23" ControlToValidate="TxtContIndianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr  class="rw27">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>27b.  <b>Asian</b> students </td>
                   <td style="border-right: 0px !important;">
                        <asp:TextBox ID="TxtContAsianEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="227" MaxLength="6" />
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Numbers"
                            TargetControlID="TxtContAsianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator24" ControlToValidate="TxtContAsianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr  class="rw27">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>27c.  <b>Black or African-American</b> students</td>
                  <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtContBlackEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="228" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers"
                            TargetControlID="txtContBlackEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator25" ControlToValidate="txtContBlackEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr  class="rw27">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>27d.  <b>Hispanic or Latino</b> students
                    </td>
                  <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtContHispanicEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="229" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Numbers"
                            TargetControlID="txtContHispanicEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator26" ControlToValidate="txtContHispanicEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr  class="rw27">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>27e.  <b>Native Hawaiian or Other Pacific Islander</b> students</td>
                 <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtContHawaiianEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="230" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers"
                            TargetControlID="txtContHawaiianEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator27" ControlToValidate="txtContHawaiianEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr  class="rw27">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>27f.   <b>White</b> students
                    </td>
               <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtContWhiteEnrollment" runat="server" TabIndex="231" MaxLength="6" CssClass="msapDataTxtSmall change"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterType="Numbers"
                            TargetControlID="txtContWhiteEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator28" ControlToValidate="txtContWhiteEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr  class="rw27">
                <td class="style1"></td>
                <td colspan="2"></td>
                <td>27g. <b>Two or more races</b> students</td>
                     <td style="border-right: 0px !important;">
                        <asp:TextBox ID="txtContMultiRacesEnrollment" runat="server" CssClass="msapDataTxtSmall change"
                            TabIndex="232" MaxLength="6"></asp:TextBox>
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" FilterType="Numbers"
                            TargetControlID="txtContMultiRacesEnrollment">
                        </ajax:FilteredTextBoxExtender>
                    </td>
                    <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator29" ControlToValidate="txtContMultiRacesEnrollment" runat="server" ForeColor="Red" SetFocusOnError="true" >*</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
               <tr>
                 <td class="style1"></td>
                 <td colspan="2"></td>
                 <td>
                        27h. Total number of continuing students who enrolled (Items 27a-27g)
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 27(a)-27(g)." />
                    </td>
                 <td colspan="2" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtContinuingEnrollment"  ClientIDMode="Static" runat="server" TabIndex="233" BackColor="LightGray" CssClass="msapDataTxtSmall Enrollment2 Enrollment change" Enabled="false"></asp:TextBox>
                           
                    </td>
                    </tr>
                <tr>
                    <td class="style1">&nbsp;
                        
                    </td>
                    <td colspan="2">&nbsp;
                        
                    </td>
                    <td>
                        28. Total school enrollment (Items 25, 26h and 27h) 
                        <img src="../../images/question_mark-thumb.png" title="This field automatically sums items 25-27." />
                    </td>
                    <td colspan="2" style="border-right: 0px !important;">
                        <asp:TextBox ID="txtTotalSutdentsEnrollment" ClientIDMode="Static" runat="server" TabIndex="234" BackColor="LightGray" CssClass="msapDataTxtSmall" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <asp:Button ID="Button10" runat="server" CssClass="surveyBtn1 postbutton" Text="Save Record"
              CausesValidation="false"   OnClick="OnSave" />
        </div>
        <div style="text-align: right; margin-top: 20px;">
            <a href="manageGPRA.aspx">Back to GPRA table</a>&nbsp;&nbsp;&nbsp;&nbsp; <a runat="server"
                id="LinkI" href="GPRAII.aspx">Part I</a>&nbsp;&nbsp;&nbsp;&nbsp; Part II&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server" id="LinkIII" href="GPRAIII.aspx">Part III</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server" id="LinkIV" href="GPRAIV.aspx">Part IV</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server" id="LinkV" href="GPRAV.aspx">Part V</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server" id="LinkVI" href="GPRAV.aspx">Part VI</a>
        </div>
    </div>
</asp:Content>
