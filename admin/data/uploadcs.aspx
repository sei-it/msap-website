﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="uploadcs.aspx.cs" Inherits="admin_data_uploadcs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Executive Summary
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script>
        function UnloadConfirm() {
            var tmp = $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val();
            if (tmp == "0")
                return "Please make sure you have saved your changes. If you do not click the 'Save Record' button, changes will be lost. Would you like to continue?";
        }
        $(function () {
            $(".postbutton").click(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('1');
            });
            $(".change").change(function () {
                $('input[name=ctl00$ContentPlaceHolder1$hfSaved]').val('0');
            });
            window.onbeforeunload = UnloadConfirm;
        });
        function checkWordLen(obj, wordLen, cid) {
            var len = obj.value.split(/[\s]+/);
            $("#" + cid).val(len.length.toString());
            if (len.length > wordLen) {
                alert("You've exceeded the " + wordLen + " word limit for this field!");
                obj.value = obj.SavedValue;
                //obj.value = obj.value.substring(0, wordLen - 1);
                len = obj.value.split(/[\s]+/);
                $("#" + cid).val(len.length.toString());
                return false;
            } else {
                obj.SavedValue = obj.value;
            }
            return true;
        }
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {
                var who = document.getElementsByName('<%= FileUpload1.UniqueID %>')[0];
                who.value = "";

                var who2 = who.cloneNode(false);
                who2.onchange = who.onchange;
                who.parentNode.replaceChild(who2, who);

                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        Executive Summary</h4>
<div id="MAPS_Year"><asp:Literal ID="ltlSubTitle" runat="server" /></div>
<br />
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <asp:HiddenField ID="hfFileID" runat="server" />
    <asp:HiddenField ID="hfSaved" runat="server" Value="1" />
    <asp:HiddenField ID="hfControlID" runat="server" ClientIDMode="Static" />

    <asp:Panel ID="pnlCohort2010" runat="server" Visible="false">
    <div class='titles_noTabs'>Executive Summary</div>
    
    <br />
    <p class="clear">
        Provide an executive summary for all performance reports. Discuss highlights of the project's goals; the extent to which the expected outcomes and performance measures were achieved; and what contributions the project has made to research, knowledge, practice, and/or policy by responding to Executive Summary Instructions.
    </p>
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Executive
            Summary Instructions</b> </span>
    <!--<p style="color: #4e8396;">
        <i>You must address all of the following questions in the Executive Summary</i>:</p>
    <div style="padding-left: 15px; color: #4e8396;">
        <strong>District Administration</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>Describe the role and efforts of the Project Director at the state, district, and
                school level in implementing the MSAP project.</li>
            <li>What types of activities are being implemented to facilitate sustainability of your
                project? (MSAP Statute §5305(b)(1)(C))</li>
        </ol>
        <strong>Project Implementation</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>How have MSAP funds been used to develop the capacity of district staff and staff
                at each school to support the continuation of magnet-themed education and promote
                ongoing student achievement? (If your school is in improvement, how are these activities
                linked to your school improvement plan?)(MSAP Statute §5307(a)(5))</li>
            <li>How has the grant been used to leverage community partnerships and resources to
                enhance theme integration and improve academic achievement at each school?</li>
            <li>How do your marketing and recruitment efforts encourage greater parental decision-making?<br />
                How have MSAP funds been used to increase systemic family and community engagement?
                (MSAP Statute §5305(b)(2)(D))</li>
        </ol>
        <strong>Evaluation</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>How are you evaluating your program?<br />
                What is the design and methodology used for your evaluation?</li>
            <li>What are the results of your evaluations?</li>
            <li>How have you used the evaluation results to improve your programs?</li>
        </ol>
        <strong>Proposed Changes</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>Describe any changes that you wish to make in the grant’s activities for the next
                budget period that are consistent with the scope and objectives of your approved
                application.</li>
            <li>Attach a resume if you are requesting changes to key personnel.</li>
        </ol>
    </div>
    </p>-->
    <table width="100%">
        <tr>
            <td style="color: #4e8396;">
            <asp:Literal ID="ltlDes" runat="server"></asp:Literal>
                <br/><br />
                
                <asp:Literal ID="ltlExeTitle" runat="server" Visible="false"></asp:Literal>
                Please type your executive summary in the text box or use the Browse button to find it on your desktop and upload it in the space provided. The file must be in PDF format or MAPS will not accept it.
            </td>
        </tr>
    </table>

    </asp:Panel>
    <asp:Panel ID="pnlCohort2013" runat="server">
    <div class='titles_noTabs'>Executive Summary</div>
    
    <br />
    <p class="clear">
        Provide an executive summary for all performance reports. Discuss highlights of 
        the project’s goals; the extent to which the expected outcomes and performance 
        measures were achieved; and what contributions the project has made to research, 
        knowledge, practice, and/or policy by responding to the following Executive 
        Summary Guiding Questions &amp; Prompts. <b><u>All questions and prompts should be 
        addressed with the established reporting period in mind.</u></b> If questions or 
        prompts have multiple parts, please make sure to address each part in your 
        response.
    </p>
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>Executive Summary Guiding Questions & Prompts</b> </span>
    <!--<p style="color: #4e8396;">
        <i>You must address all of the following questions in the Executive Summary</i>:</p>
    <div style="padding-left: 15px; color: #4e8396;">
        <strong>District Administration</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>Describe the role and efforts of the Project Director at the state, district, and
                school level in implementing the MSAP project.</li>
            <li>What types of activities are being implemented to facilitate sustainability of your
                project? (MSAP Statute §5305(b)(1)(C))</li>
        </ol>
        <strong>Project Implementation</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>How have MSAP funds been used to develop the capacity of district staff and staff
                at each school to support the continuation of magnet-themed education and promote
                ongoing student achievement? (If your school is in improvement, how are these activities
                linked to your school improvement plan?)(MSAP Statute §5307(a)(5))</li>
            <li>How has the grant been used to leverage community partnerships and resources to
                enhance theme integration and improve academic achievement at each school?</li>
            <li>How do your marketing and recruitment efforts encourage greater parental decision-making?<br />
                How have MSAP funds been used to increase systemic family and community engagement?
                (MSAP Statute §5305(b)(2)(D))</li>
        </ol>
        <strong>Evaluation</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>How are you evaluating your program?<br />
                What is the design and methodology used for your evaluation?</li>
            <li>What are the results of your evaluations?</li>
            <li>How have you used the evaluation results to improve your programs?</li>
        </ol>
        <strong>Proposed Changes</strong>
        <ol type="a" style="padding-top: 0px;">
            <li>Describe any changes that you wish to make in the grant’s activities for the next
                budget period that are consistent with the scope and objectives of your approved
                application.</li>
            <li>Attach a resume if you are requesting changes to key personnel.</li>
        </ol>
    </div>
    </p>-->
    <table width="100%">
        <tr>
            <td style="color: #4e8396;">
                <b>Administration</b></td>
        </tr>
         <tr>
            <td style="color: #4e8396;">
            <ol>
             <li>Identify and elaborate upon both your biggest success and biggest challenge in the implementation of your project.</li>
             <li>Have there been any changes at the district level, at the school level, in the community at large, or in the greater environment that have impacted the implementation of your project, either positively or negatively? If yes, please describe.</li>
            </ol>
            </td>
        </tr>
          <tr>
            <td style="color: #4e8396;">
                    <b>
                    Implementation</b>
            </td>
        </tr>
          <tr>
            <td style="color: #4e8396;">
            <ol>
<li>How has the grant been used to reduce minority group isolation at each school? To what extent have your efforts been successful, and what has been done to meet targets moving forward if shortfalls exist? [MSAP Statute 5301(b)(1)]</li>
<li>How has the grant been used to leverage community partnerships and resources to enhance theme integration and improve academic achievement at each school? [MSAP Statute 5301(b)(4)]</li>
<li>How have MSAP funds been used to develop the capacity of both district and school staff to support the implementation of magnet-themed education and promote ongoing student achievement? [MSAP Statute 5301(b)(5)]</li>
<li>How has the grant been used to facilitate the sustainability of your project? [MSAP Statute 5301(b)(5)]</li>
<li>How have MSAP funds been used to increase systemic family and community engagement at each school? [MSAP Statute 5305(b)(2)(D)]</li>
<li>To what extent does each site provide theme-aligned out-of-school time and extended learning time? Please describe your implementation efforts in this regard. To what extent does this programming involve the participation/engagement of external partners? How extensive is student participation in this programming?</li>
</ol>
            </td>
        </tr>
          <tr>
            <td style="color: #4e8396;">
                <b>Evaluation</b></td>
        </tr>
          <tr>
            <td style="color: #4e8396;">
            <ol>
<li>How are you evaluating your program? What is the design and methodology of your evaluation? Is your evaluation formative and/or summative in nature? Is your evaluation methodology classified as “rigorous”?</li>
<li>How have you used ongoing evaluation results to improve your programs?</li>
<li><asp:Literal ID="ltlEval3" runat="server" /></li>
</ol>
            </td>
        </tr>
<%--          <tr>
            <td style="color: #4e8396;">
                <b>Proposed Changes</b></td>
        </tr>
          <tr>
            <td style="color: #4e8396;">
            <ol>
<li>Describe any changes that you wish to make in the grant’s activities for the next budget period that are consistent with the scope and objectives of your approved application.</li>
<li>Attach a resume if you are requesting changes to key personnel.</li>
</ol>
            </td>
        </tr>--%>
    </table>
    <p style="color: #4e8396;">
    Please type your executive summary in the text box or use the Browse button to find it on your desktop and upload it in the space provided. The file must be in PDF format or MAPS will not accept it.
    </p>
    </asp:Panel>
    <table>
        <tr id="fileRow" runat="server" visible="false">
            <td colspan="2">
                <table class="msapDataTbl" style="width: 300px;">
                    <tr>
                        <th colspan="2">
                            Uploaded File
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <a id="UploadedFile" target="_blank" runat="server">
                                <asp:Label ID="lblFile" runat="server"></asp:Label></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="Button3" runat="server" CssClass="postbutton" Text="Delete" 
                            OnClick="OnDelete" OnClientClick="return confirm('Are you sure you want delete this uploaded file?');" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="txtExecutiveSummary" runat="server" Font-Size="10" Columns="97"
                    Rows="10" TextMode="MultiLine" CssClass="msapDataTxt change" onkeyup="checkWordLen(this, 1000, 'summaryLength');"></asp:TextBox>
                <!---->
                <input type="text" disabled="disabled" size="4" id="summaryLength" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="msapDataTxt" />&nbsp;&nbsp;
                <asp:Button ID="UploadBtn" CssClass="surveyBtn postbutton" runat="server" Text="Upload"
                    Visible="false" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-top: 20px;">
                <asp:Button ID="Button1" CssClass="surveyBtn2 postbutton" runat="server" Text="Save & Upload"
                    OnClick="OnSave" />
            </td>
        </tr>
    </table>
</asp:Content>
