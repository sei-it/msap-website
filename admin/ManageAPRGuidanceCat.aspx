﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="ManageAPRGuidanceCat.aspx.cs" Inherits="admin_ManageAPRGuidanceCat" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Manage APR Guidance</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <h2>Category</h2>
    <p style="text-align:left">
        <asp:Button ID="btnNewCat" runat="server" Text="New category" CssClass="msapBtn" OnClick="OnAddCategory" />
        <span style="padding-left:560px;">
        <asp:CheckBox ID="cbxAll" Text="Display All Categories" AutoPostBack="true"  
            TextAlign="Left" runat="server" oncheckedchanged="cbxAll_CheckedChanged" 
            ForeColor="#F58220" />
        </span>
    </p>
    <div>
     <asp:GridView ID="gvAPRCat" runat="server"
        PageSize="20" AutoGenerateColumns="False" CssClass="msapTbl" 
        DataKeyNames="id">
         <Columns>
             <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                 ReadOnly="True" SortExpression="id" Visible="false" />
             <asp:BoundField DataField="category" HeaderText="category" 
                 SortExpression="category" />
             <asp:BoundField DataField="note" HeaderText="note" SortExpression="note" />
             <asp:CheckBoxField DataField="isActive" HeaderText="Display on Page" 
                 SortExpression="isActive"  />
              <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnAPRCat" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEditCat"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
         </Columns>
    
    </asp:GridView>
   </div>

    <asp:Panel ID="pnlWebinar" runat="server">
   <br /><br />
    <hr />
    <h2>APR Guidance</h2>
       <p style="text-align:left">
        <asp:Button ID="btnNewWebinar" runat="server" Text="New APR Guidance" CssClass="msapBtn" OnClick="OnAddWebinar" />
           <asp:DropDownList ID="ddlAPRCategory" runat="server" AutoPostBack="True" 
                DataTextField="category" DataValueField="id">
           </asp:DropDownList>
    </p>
    <asp:GridView ID="gvAPR" runat="server"
        PageSize="20" AutoGenerateColumns="False" CssClass="msapTbl" 
        DataSourceID="ldsWebinar" DataKeyNames="id"
        >
        <Columns>
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" Visible="false"
                ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="cat_id" HeaderText="cat_id" Visible="false"
                SortExpression="cat_id" />
            <asp:BoundField DataField="ImageTag" HeaderText="ImageTag" Visible="false"
                SortExpression="ImageTag" />
            <asp:BoundField DataField="imageAlt" HeaderText="imageAlt" Visible="false"
                SortExpression="imageAlt" />
            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
            <asp:BoundField DataField="EventDate" HeaderText="EventDate"  DataFormatString="{0:MMMM d, yyyy}"
                SortExpression="EventDate" />
            <asp:BoundField DataField="Description" HeaderText="Description" Visible="false"
                SortExpression="Description" />
            <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" 
                SortExpression="CreatedDate" />
            <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" 
                SortExpression="CreatedBy" />
             <asp:BoundField DataField="displayorder" HeaderText="Display Order" 
                SortExpression="CreatedBy" />
                 <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </asp:Panel>

    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfCatID" runat="server" />
    <%-- News --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Past Webinar Category</div>
            <table>
                <tr>
                    <td>
                        Category Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCatName" runat="server" MaxLength="250" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description:
                    </td>
                    <td>
                    <asp:TextBox ID="txtDes" runat="server" Width="470" Height="200" TextMode="MultiLine"/>

                    </td>
                </tr>
                <tr>
                    <td>
                        Active:
                    </td>
                    <td>
                       <asp:CheckBox ID="cbxActive" runat="server" Text=" " Checked="true"/>
                    </td>
                </tr>
                 <tr>
                    <td>
                        Publish:
                    </td>
                    <td>
                       <asp:CheckBox ID="cbxPublish" runat="server" Text=" " Checked="true"/>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                    </td>
                    <td>
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeNewsWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
    <asp:LinqDataSource ID="ldsWebinar" runat="server" 
        ContextTypeName="APRGuidanceDataClassesDataContext" EntityTypeName="" 
        TableName="APRGuidances" Where="cat_id == @cat_id" OrderBy="displayorder">
        <WhereParameters>
            <asp:ControlParameter ControlID="ddlAPRCategory" 
                 Name="cat_id" PropertyName="SelectedValue" 
                Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>
</asp:Content>


