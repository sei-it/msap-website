﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace EVEA.project
{
    public partial class eventdetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                string[] strs = Request.QueryString["id"].Split(';');
                StringBuilder sb = new StringBuilder("<table class='eventsTbl'>");
                foreach (string TimeStamp in strs)
                {
                    if (!string.IsNullOrEmpty(TimeStamp))
                    {
                        Synergy.Magnet.Calendar calendar = Synergy.Magnet.Calendar.SingleOrDefault(x => x.ID == Convert.ToInt32(TimeStamp));
                        sb.AppendFormat("<tr><td><b>Title</b>:</td><td>{0}</td></tr>", calendar.CalendarTitle);
                        if (((DateTime)calendar.CalendarStart).Year == ((DateTime)calendar.CalendarEnd).Year
                            && ((DateTime)calendar.CalendarStart).Month == ((DateTime)calendar.CalendarEnd).Month
                            && ((DateTime)calendar.CalendarStart).Day == ((DateTime)calendar.CalendarEnd).Day)
                        {
                            sb.AppendFormat("<tr><td><b>Date</b>:</td><td>{0}</td></tr>", ((DateTime)calendar.CalendarStart).ToShortDateString());
                        }
                        else
                            sb.AppendFormat("<tr><td><b>Start Date</b>:</td><td>{0}</td></tr>", ((DateTime)calendar.CalendarStart).ToShortDateString());
                        sb.AppendFormat("<tr><td><b>Start Time</b>:</td><td>{0}</td></tr>", ((DateTime)calendar.CalendarStart).ToShortTimeString() + " " + calendar.TimeZone);
                        if (((DateTime)calendar.CalendarStart).Year == ((DateTime)calendar.CalendarEnd).Year
                            && ((DateTime)calendar.CalendarStart).Month == ((DateTime)calendar.CalendarEnd).Month
                            && ((DateTime)calendar.CalendarStart).Day == ((DateTime)calendar.CalendarEnd).Day)
                        { }
                        else
                            sb.AppendFormat("<tr><td><b>End Date</b>:</td><td>{0}</td></tr>", ((DateTime)calendar.CalendarEnd).ToShortDateString());
                        sb.AppendFormat("<tr><td><b>End Time</b>:</td><td>{0}</td></tr>", ((DateTime)calendar.CalendarEnd).ToShortTimeString() + " " + calendar.TimeZone);
                        sb.AppendFormat("<tr><td><b>Description</b>:</td><td>{0}</td></tr>", calendar.CalendarDescription);
                        sb.Append("<tr><td>&nbsp;</td></tr>");
                    }
                }
                sb.Append("</table>");
                ContentDiv.InnerHtml = sb.ToString();
            }
        }
    }
}
