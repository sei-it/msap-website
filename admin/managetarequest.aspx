﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managetarequest.aspx.cs" Inherits="admin_managetarequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage TA Request
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

    <script type="text/javascript" src="../js/thickbox-compressed.js"></script>

    <link rel="stylesheet" href="../css/thickbox.css" type="text/css" media="screen" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Manage TA Request</h1>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        OnPageIndexChanging="OnGridViewPageIndexChanged">
        <Columns>
            <asp:BoundField DataField="FirstName" SortExpression="" HeaderText="First Name" />
            <asp:BoundField DataField="LastName" SortExpression="" HeaderText="Last Name" />
            <asp:BoundField DataField="Title" SortExpression="" HeaderText="Title" />
            <asp:BoundField DataField="District" SortExpression="" HeaderText="District" />
            <asp:BoundField DataField="Telephone" SortExpression="" HeaderText="Telephone" />
            <asp:BoundField DataField="Fax" SortExpression="" HeaderText="Fax" />
            <asp:BoundField DataField="Email" SortExpression="" HeaderText="Email" />
            <asp:BoundField DataField="CreatedDate" SortExpression="" HeaderText="Time Stamp" />
            <asp:TemplateField>
                <ItemTemplate>
                    <a href='#' onclick="tb_show('TA Request Detail','requestdetail.aspx?id=<%# Eval("ID")%>&height=540&width=540','');">
                        Detail</a>&nbsp;
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
