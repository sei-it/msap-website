﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageupload.aspx.cs" Inherits="admin_manageupload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Upload/Download Private File
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function validateFileUpload(obj) {
            var fileName = new String();
            var fileExtension = new String();

            // store the file name into the variable
            fileName = obj.value;

            // extract and store the file extension into another variable
            fileExtension = fileName.substr(fileName.length - 3, 3);

            // array of allowed file type extensions
            var validFileExtensions = new Array("pdf");

            var flag = false;

            // loop over the valid file extensions to compare them with uploaded file
            for (var index = 0; index < validFileExtensions.length; index++) {
                if (fileExtension.toLowerCase() == validFileExtensions[index].toString().toLowerCase()) {
                    flag = true;
                }
            }

            // display the alert message box according to the flag value
            if (flag == false) {
                alert('You can upload the files with following extensions only:\n.pdf');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Upload/Download Private File</h1>
    <p style="text-align: left">
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="Newbutton" runat="server" Text="Upload" CssClass="msapBtn"
            OnClick="OnUpload" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl" GridLines="None">
        <Columns>
            <asp:BoundField DataField="FileLink" SortExpression="" HeaderText="File" HtmlEncode="false" />
            <asp:BoundField DataField="UploadDate" SortExpression="" HeaderText="Upload Date" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" OnClick="OnDelete" CommandArgument='<%# Eval("ID") %>'
                    OnClientClick="return confirm('Are you sure you want delete this uploaded file?');" ></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
