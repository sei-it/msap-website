﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="TA_toolkits.aspx.cs" Inherits="admin_TechAss_Toolkits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Technical Assistance Toolkits
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Technical Assistance Toolkits
    </h1>
    <p>
        These toolkits are designed to guide MSAP grantees through implementation of key
        magnet components. Each toolkit contains explanatory guidance as well as tools grantees
        can use to do the work in their districts to ensure successful program implementation.
        The toolkits can also be used as professional development resources for magnet staff.</p>
    <div class="course_info_toolkit">
        <img class="left_img_frame" src="../images/TA_toolkit_imgs/AppleAbstract.jpg" width="106"
            height="100" />
        <p class="msapHPadmin">
            Student Recruitment</p>
        <p>
            This toolkit provides guidance on how to target magnet school marketing and recruitment
            activities to meet Magnet Schools Assistance Program (MSAP) minority group isolation
            objectives. When you know your recruitment targets, understand your audience, build
            assessment into marketing campaigns, and collect and analyze data, your MSAP project
            can achieve its goals of diversity, excellence, and equity. The toolkit includes
            a calculator that will help you define your targets.</p>
    </div>
    <table style="margin-left: 130px">
        <tr>
            <td style="width: 580px">
                <span style="color: #4E8396"><b>File name</b></span>
            </td>
            <td>
                <span style="color: #4E8396"><b>Files available</b></span>
            </td>
        </tr>
        <tr style="background-color: #E8E8E8">
            <td>
                Student Recruitment Blank Tools
            </td>
            <td>
                <a href="doc/Toolkits/Recruitment_Appendixes_C-E_Blank_Tools.docx" target="_blank">DOC</a>
            </td>
        </tr>
        <tr>
            <td>
                Student Recruitment Calculator
            </td>
            <td>
                <a href="doc/Toolkits/Student_Recruitment_Calculator.xlsx" target="_blank">EXCEL</a>
            </td>
        </tr>
        <tr style="background-color: #E8E8E8">
            <td>
                Student Recruitment Examples of Completed Tools
            </td>
            <td>
                <a href="doc/Toolkits/Recruitment_Appendix_B_Completed_Tools.pdf" target="_blank">PDF</a>
            </td>
        </tr>
        <tr>
            <td>
                Student Recruitment Guidance
            </td>
            <td>
                <a href="doc/Toolkits/MSAP_StudentRecruitToolkit_Guidance.pdf" target="_blank">PDF</a>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <div class="course_info_toolkit">
        <img class="left_img_frame" src="../images/TA_toolkit_imgs/TreeAbstract.jpg" width="106"
            height="100" />
        <p class="msapHPadmin">
            Planning for Sustainability</p>
        <p>
            This toolkit provides magnet staff with the information and resources necessary
            to create a written sustainability plan that covers all aspects of sustainability.
            The toolkit also provides timelines for implementing the sustainability plan and
            tools for determining the cost of all program components that must be sustained.</p>
    </div>
    <table style="margin-left: 130px">
        <tr>
            <td style="width: 580px">
                <span style="color: #4E8396"><b>File name</b></span>
            </td>
            <td>
                <span style="color: #4E8396"><b>Files available</b></span>
            </td>
        </tr>
        <tr style="background-color: #E8E8E8">
            <td>
                Planning for Sustainability Blank Tools
            </td>
            <td>
                <a href="doc/Toolkits/Sustainability_Appendix_of_tools.docx" target="_blank">DOC</a>
            </td>
        </tr>
        <tr>
            <td>
                Planning for Sustainability Guidance
            </td>
            <td>
                <a href="doc/Toolkits/MSAP_Planning_for_Sustainability_Toolkit.pdf" target="_blank">
                    PDF</a>
            </td>
        </tr>
    </table>
    <br /><br />
    <div class="course_info_toolkit">
        <img class="left_img_frame" src="../images/TA_toolkit_imgs/DevicesAbstract.jpg" width="106"
            height="100" />
        <p class="msapHPadmin">
            Personal Device Initiatives</p>
        <p>
As magnet schools integrate more technology use into their theme-based curricula, they may consider initiatives that provide each student with a personal technology device. These initiatives not only increase technology integration, but they also increase student engagement and academic achievement. This planning brief provides a step-by-step guide for the successful implementation of personal device initiatives in magnet schools, including vision, considerations, and implementation strategies.</p>
    </div>
    <table style="margin-left: 130px">
        <tr>
            <td style="width: 580px">
                <span style="color: #4E8396"><b>File name</b></span>
            </td>
            <td>
                <span style="color: #4E8396"><b>Files available</b></span>
            </td>
        </tr>
        <tr style="background-color: #E8E8E8">
            <td>
                Personal Device Initiatives Guidance
            </td>
            <td>
                <a href="doc/Toolkits/MSAP Device_Guide.pdf" target="_blank">PDF</a>
            </td>
        </tr>
        
    </table>
    <p>
    </p>
</asp:Content>
