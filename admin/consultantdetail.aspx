﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="consultantdetail.aspx.cs"
    Inherits="admin_memberdetail" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
</head>
<body>
    <div class="twoColElsLtHdr">
        <div id="container">
            <div id="title">
                <h1>
                    <asp:Literal ID="ltTitle" runat="server"></asp:Literal></h1>
                <!-- end #title -->
            </div>
            <div id="sidebar1" runat="server">
            </div>
            <div id="mainContent" runat="server">
            </div>
            <!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats -->
            <br class="clearfloat" />
            <!-- end #container -->
        </div>
    </div>
</body>
</html>
