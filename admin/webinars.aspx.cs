﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class admin_webinars : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='100%' border='0' cellspacing='0' cellpadding='3'><tr><td width='90%'><b>File name</b></td><td width='10%'><span style='font-weight: bold;'>Files available</span></td></tr>");
            int i = 0;
            foreach (MagnetWebinar webinar in MagnetWebinar.Find(x => x.DocType == false && x.DocCategory == false).OrderBy(x => x.DocumentName))
            {
                i++;
                if (i % 2 == 0)
                    sb.Append("<tr bgcolor='#E8E8E8'>");
                else
                    sb.Append("<tr>");
                sb.AppendFormat("<td>{0}</td><td width='10%'><a href='../applicationdoc/{1}' target='_blank'>{2}</a>", webinar.DocumentName, webinar.PhysicalName, webinar.DocumentType);
                if (!string.IsNullOrEmpty(webinar.AlternativePhysicalName))
                {
                    sb.AppendFormat(" | <a href='../applicationdoc/{0}' target='_blank'>{1}</a></td></tr>", webinar.AlternativePhysicalName, webinar.AlternativeDocumentType);
                }
                else
                    sb.Append("</td></tr>");
            }
            sb.Append("</table>");
            Div1.InnerHtml = sb.ToString();

            sb = new StringBuilder();
            sb.Append("<table width='100%' border='0' cellspacing='0' cellpadding='3'><tr><td width='90%'><b>File name</b></td><td width='10%'><span style='font-weight: bold;'>Files available</span></td></tr>");
            i = 0;
            foreach (MagnetWebinar webinar in MagnetWebinar.Find(x => x.DocType == true && x.DocCategory == false).OrderBy(x => x.DocumentName))
            {
                i++;
                if (i % 2 == 0)
                    sb.Append("<tr bgcolor='#E8E8E8'>");
                else
                    sb.Append("<tr>");
                sb.AppendFormat("<td>{0}</td><td width='10%'><A href='../applicationdoc/{1}' target='_blank'>{2}</A>", webinar.DocumentName, webinar.PhysicalName, webinar.DocumentType);
                if (!string.IsNullOrEmpty(webinar.AlternativePhysicalName))
                {
                    sb.AppendFormat(" | <A href='../applicationdoc/{0}' target='_blank'>{1}</A></td></tr>", webinar.AlternativePhysicalName, webinar.AlternativeDocumentType);
                }
                else
                    sb.Append("</td></tr>");
            }
            sb.Append("</table>");
            Div2.InnerHtml = sb.ToString();

        }
    }
}