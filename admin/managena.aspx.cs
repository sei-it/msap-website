﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_managena : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetWebinar.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetWebinars
                   where m.DocCategory == true
                   orderby m.DocumentName
                   select new { m.ID, m.DocumentName,m.DocumentType,m.UploadDate};
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtDocumentName.Text = "";
        //ddlDocType.SelectedIndex = 0;
        ddlFirstFileType.SelectedIndex = 0;
        //ddlSecondFileType.SelectedIndex = 0;
        hfID.Value = "";
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeNewsWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetWebinar data = MagnetWebinar.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtDocumentName.Text = data.DocumentName;
        //ddlDocType.SelectedIndex = Convert.ToInt32(data.DocType);
        if (data.DocumentType != null) ddlFirstFileType.SelectedValue = data.DocumentType;
        //if (data.AlternativeDocumentType != null) ddlSecondFileType.SelectedValue = data.AlternativeDocumentType;
        mpeNewsWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetWebinar data = new MagnetWebinar();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetWebinar.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        //data.Title = txtTitle.Text;
        data.DocCategory = true;
        data.DocumentName = txtDocumentName.Text;
        data.DocType = false;
        if (FileUpload1.HasFile)
        {
            data.UploadDate = DateTime.Now;
            string DocName = System.Guid.NewGuid().ToString() +  FileUpload1.FileName;
            FileUpload1.SaveAs(Server.MapPath("../applicationdoc/" + DocName));
            data.PhysicalName = DocName;
            data.DocumentType = ddlFirstFileType.SelectedValue;
        }
        data.Save();
        LoadData(0);
    }
}