﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_managecontext : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetContext.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = (from m in db.MagnetContexts
                   from n in db.MagnetSchools
                   from l in db.MagnetDLLs
                   where m.SchoolID == n.ID
                   && m.ReportID == Convert.ToInt32(hfReportID.Value)
                   && m.SchoolType == l.TypeIndex
                   && l.TypeID == 2
                   select new {m.ID, n.SchoolName, m.HistoricalContext, SchoolType = l.TypeName, m.TitleFunding, m.PercentFRL}).ToList();
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    protected void OnAddGrantee(object sender, EventArgs e)
    {
        Response.Redirect("context.aspx?option=0");
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        Response.Redirect("context.aspx?option=" + (sender as LinkButton).CommandArgument);
    }
}