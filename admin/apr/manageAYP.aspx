﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="manageAYP.aspx.cs" Inherits="admin_data_manageAYP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage AYP Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            AYP Report</h4>
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    Report Entity
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("AYPType")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    Report Type
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("ReportType")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div style="margin-top: 12px;">
            <table width="100%">
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblMessage"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Button ID="Button1" runat="server" Text="New AYP Report" CssClass="surveyBtn2"
                            OnClick="OnAddData" />
                    </td>
                </tr>
            </table>
        </div>
</asp:Content>

