﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="adhocreview.aspx.cs" Inherits="admin_data_adhocreview" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit Ad Hoc Review
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
<style>
        .msapDataTbl th
        {
            color: #000000 !important;
        }
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            <a href="manageadhocreview.aspx">Ad Hoc Review</a><img src="button_arrow.jpg" width="29"
                height="36" />Add/Edit Ad Hoc Review
            <asp:Label ID="lblSSID" runat="server"></asp:Label></h4>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <table class="msapDataTbl">
            <tr>
                <td style="color: #4e8396;" width="40%" class="TDWithTopBorder">
                    Were data in APR sufficient to address GPRA measure 1?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlGPRAMeasure1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtGPRAMeasure1" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 2?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlGPRAMeasure2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtGPRAMeasure2" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 3?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlGPRAMeasure3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtGPRAMeasure3" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 4?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlGPRAMeasure4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtGPRAMeasure4" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 5?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlGPRAMeasure5" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtGPRAMeasure5" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Were data in APR sufficient to address GPRA measure 6?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlGPRAMeasure6" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtGPRAMeasure6" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    General Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtGeneralNotes" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ad Hoc Report Status:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlReportStatus" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Date Ad Hoc Report Expected:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtDateExpected" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDateExpected">
                    </ajax:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Date Received:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtDateReceived" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDateReceived">
                    </ajax:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtDateNotes" runat="server" MaxLength="1000" CssClass="msapDataTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ad Hoc Status:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlAdHocStatus" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="Button4" runat="server" Text="Save Record" CssClass="surveyBtn1"
                        OnClick="OnSave" />
                    <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" Text="Return" OnClick="OnReturn"  CausesValidation="false"/>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
