﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="rgioutcome.aspx.cs" Inherits="admin_data_rgioutcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit MGI Outcome
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
<style>
        .msapDataTbl th
        {
            color: #000000 !important;
        }
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Add/Edit MGI Outcome</h1>
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            <a href="managergioutcome.aspx">MGI Outcome</a><img src="button_arrow.jpg" width="29"
                height="36" />Add/Edit MGI Outcome
            <asp:Label ID="lblSSID" runat="server"></asp:Label></h4>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <table class="msapDataTbl">
            <tr>
                <td style="color: #4e8396;" width="40%" class="TDWithTopBorder">
                    Magnet School:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    MGI objective:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlRGIObjective" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 1:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlRacialGroup1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 2:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlRacialGroup2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 3:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlRacialGroup3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Racial isolated group 4:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlRacialGroup4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 1:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlTargetedGroup1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 2:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlTargetedGroup2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 3:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlTargetedGroup3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Targeted racial/ethnic group 4:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlTargetedGroup4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Met objective?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlMeetObjective" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 1:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlIncresedTargetedGroup1" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 2:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlIncresedTargetedGroup2" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 3:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlIncresedTargetedGroup3" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Increased targeted racial/ethnic group 4:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlIncresedTargetedGroup4" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Met GPRA measure 1?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlMetGPRAMeasure" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="Button4" runat="server" Text="Save Record" CssClass="surveyBtn1"
                        OnClick="OnSave" />
                    <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" Text="Return" OnClick="OnReturn"  CausesValidation="false"/>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
