﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Web.Security;

public partial class admin_data_Context : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //School
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == granteeID).OrderBy(x=>x.SchoolName))
            {
                ddlSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
            }
            foreach (MagnetDLL SchoolType in MagnetDLL.Find(x => x.TypeID == 2 && x.TypeIndex > -9))
            {
                ddlSchoolType.Items.Add(new ListItem(SchoolType.TypeName, SchoolType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL Stage in MagnetDLL.Find(x => x.TypeID == 17 && x.TypeIndex > -9))
            {
                ddlTitleFunding.Items.Add(new ListItem(Stage.TypeName, Stage.TypeIndex.ToString()));
            }
            foreach (MagnetDLL Stage in MagnetDLL.Find(x => x.TypeID == 3 && x.TypeIndex > -9).OrderBy(x=>x.TypeIndex))
            {
                ddlImprovementStage.Items.Add(new ListItem(Stage.TypeName, Stage.TypeIndex.ToString()));
            }
            foreach (MagnetDLL AYP in MagnetDLL.Find(x => x.TypeID == 4 && x.TypeIndex > -9).OrderBy(x => x.TypeIndex))
            {
                ddlYearsAYP.Items.Add(new ListItem(AYP.TypeName, AYP.TypeIndex.ToString()));
            }
            foreach (MagnetDLL Desegregation in MagnetDLL.Find(x => x.TypeID == 5 && x.TypeIndex > -9).OrderBy(x => x.TypeIndex))
            {
                ddlDesegregation.Items.Add(new ListItem(Desegregation.TypeName, Desegregation.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ProgramType in MagnetDLL.Find(x => x.TypeID == 6 && x.TypeIndex > -9).OrderBy(x => x.TypeIndex))
            {
                ddlProgramType.Items.Add(new ListItem(ProgramType.TypeName, ProgramType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL AttendanceType in MagnetDLL.Find(x => x.TypeID == 7 && x.TypeIndex > -9).OrderBy(x => x.TypeIndex))
            {
                ddlAttendanceType.Items.Add(new ListItem(AttendanceType.TypeName, AttendanceType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ProgramStatus in MagnetDLL.Find(x => x.TypeID == 8 && x.TypeIndex > -9).OrderBy(x => x.TypeIndex))
            {
                ddlProgramStatus.Items.Add(new ListItem(ProgramStatus.TypeName, ProgramStatus.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("managecontext.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    LoadSIPData(id);
                }
            }
        }
    }
    private void LoadSIPData(int id)
    {
        MagnetContext data = MagnetContext.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        ddlSchool.SelectedValue = data.SchoolID.ToString();
        ddlSchoolType.SelectedValue = data.SchoolType.ToString();
        txtHistoricalContext.Text = data.HistoricalContext;
        ddlTitleFunding.SelectedValue = Convert.ToString(data.TitleFunding);
        txtPercentFRL.Text = Convert.ToString(data.PercentFRL);
        ddlImprovementStage.SelectedValue = data.ImprovementStage.ToString();
        ddlYearsAYP.SelectedValue = data.YearsSinceAYP.ToString();
        ddlDesegregation.SelectedValue = data.DesegregationPlanType.ToString();
        ddlProgramType.SelectedValue = data.ProgramType.ToString();
        ddlAttendanceType.SelectedValue = data.AttendanceType.ToString();
        ddlProgramStatus.SelectedValue = data.ProgramStatus.ToString();
        ddlAdmMMissing.SelectedValue = Convert.ToInt32(data.AdmMissing).ToString();
        ddlAdmMPlanning.SelectedValue = Convert.ToInt32(data.AdmPlanningYear).ToString();
        ddlAdmMOpenLottery.SelectedValue = Convert.ToInt32(data.AdmOpenLottery).ToString();
        ddlAdmMWeightedLottery.SelectedValue = Convert.ToInt32(data.AdmWeightedLottery).ToString();
        ddlAdmMWeightedLotteryTitleIStatus.SelectedValue = Convert.ToInt32(data.AdmWeightedLotteryTitleStatus).ToString();
        ddlAdmMSelectiveAdmissions.SelectedValue = Convert.ToInt32(data.AdmSelectiveAdmissions).ToString();
        //ddlFacMRemovate.SelectedValue = Convert.ToInt32(data.FacRenovate).ToString();
        //ddlFacMRenovateHandicappedAccessibility.SelectedValue = Convert.ToInt32(data.FacRenovateHandicappedAccessibility).ToString();
        ddlWeightedLotterySiblings.SelectedValue = Convert.ToInt32(data.AdmWeightedLotterySiblings).ToString();
        ddlWeightedLotteryRace.SelectedValue = Convert.ToInt32(data.AdmWeightedLotteryRace).ToString();
        ddlWeightedOther.SelectedValue = Convert.ToInt32(data.AdmWeightedLotteryOther).ToString();
        txtWeightedLottery.Text = data.AdmWeightedLotteryOtherSpecify;
        //ddlFacMRenovateIntegration.SelectedValue = Convert.ToInt32(data.FacRenovateThemeIntegration).ToString();
        //ddlRacMRenovateTechnologyInfrastructure.SelectedValue = Convert.ToInt32(data.FacRenovateTechnologyInfrastructure).ToString();
        //ddlFacMConstructNewBuilding.SelectedValue = Convert.ToInt32(data.FacConstructNewBuilding).ToString();
        //ddlFacMNoModificationsPlanned.SelectedValue = Convert.ToInt32(data.FacNoModificationsPlanned).ToString();
        txtNotes.Text = data.Notes;
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetContext data = new MagnetContext();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetContext.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            data.ReportID = Convert.ToInt32(hfReportID.Value);
        }
        data.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        data.SchoolType = Convert.ToInt32(ddlSchoolType.SelectedValue);
        data.HistoricalContext = txtHistoricalContext.Text;
        data.TitleFunding = Convert.ToInt32(ddlTitleFunding.SelectedValue);
        data.PercentFRL = string.IsNullOrEmpty(txtPercentFRL.Text)?0:Convert.ToInt32(txtPercentFRL.Text);
        data.ImprovementStage = Convert.ToInt32(ddlImprovementStage.SelectedValue);
        data.YearsSinceAYP = Convert.ToInt32(ddlYearsAYP.SelectedValue);
        data.DesegregationPlanType = Convert.ToInt32(ddlDesegregation.SelectedValue);
        data.ProgramType = Convert.ToInt32(ddlProgramType.SelectedValue);
        data.AttendanceType = Convert.ToInt32(ddlAttendanceType.SelectedValue);
        data.ProgramStatus = Convert.ToInt32(ddlProgramStatus.SelectedValue);
        data.AdmMissing = Convert.ToBoolean(Convert.ToInt32(ddlAdmMMissing.SelectedValue));
        data.AdmPlanningYear = Convert.ToBoolean(Convert.ToInt32(ddlAdmMPlanning.SelectedValue));
        data.AdmOpenLottery = Convert.ToBoolean(Convert.ToInt32(ddlAdmMOpenLottery.SelectedValue));
        data.AdmWeightedLottery = Convert.ToBoolean(Convert.ToInt32(ddlAdmMWeightedLottery.SelectedValue));
        data.AdmWeightedLotteryTitleStatus = Convert.ToBoolean(Convert.ToInt32(ddlAdmMWeightedLotteryTitleIStatus.SelectedValue));
        data.AdmSelectiveAdmissions = Convert.ToBoolean(Convert.ToInt32(ddlAdmMSelectiveAdmissions.SelectedValue));
        //data.FacRenovate = Convert.ToBoolean(Convert.ToInt32(ddlFacMRemovate.SelectedValue));
        //data.FacRenovateHandicappedAccessibility = Convert.ToBoolean(Convert.ToInt32(ddlFacMRenovateHandicappedAccessibility.SelectedValue));
        data.AdmWeightedLotterySiblings = Convert.ToBoolean(Convert.ToInt32(ddlWeightedLotterySiblings.SelectedValue));
        data.AdmWeightedLotteryRace = Convert.ToBoolean(Convert.ToInt32(ddlWeightedLotteryRace.SelectedValue));
        data.AdmWeightedLotteryOther = Convert.ToBoolean(Convert.ToInt32(ddlWeightedOther.SelectedValue));
        data.AdmWeightedLotteryOtherSpecify = txtWeightedLottery.Text;
        //data.FacRenovateThemeIntegration = Convert.ToBoolean(Convert.ToInt32(ddlFacMRenovateIntegration.SelectedValue));
        //data.FacRenovateTechnologyInfrastructure = Convert.ToBoolean(Convert.ToInt32(ddlRacMRenovateTechnologyInfrastructure.SelectedValue));
        //data.FacConstructNewBuilding = Convert.ToBoolean(Convert.ToInt32(ddlFacMConstructNewBuilding.SelectedValue));
        //data.FacNoModificationsPlanned = Convert.ToBoolean(Convert.ToInt32(ddlFacMNoModificationsPlanned.SelectedValue));
        data.Notes = txtNotes.Text;
        data.Save();
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("managecontext.aspx");
    }
}