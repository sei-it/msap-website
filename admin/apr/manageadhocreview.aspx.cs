﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_manageadhocreview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);
            LoadData();
        }
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        Response.Redirect("adhocreview.aspx?option=" + (sender as LinkButton).CommandArgument);
    }
    protected void OnAddData(object sender, EventArgs e)
    {
        Response.Redirect("adhocreview.aspx?option=0");
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        MagnetAdHocReview.Delete(x => x.ID == id);
        LoadData();
    }
    private void LoadData()
    {
        var data = MagnetAdHocReview.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}