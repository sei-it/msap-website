﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="Context.aspx.cs" Inherits="admin_data_Context" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit Context
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        .msapDataTbl th
        {
            color: #000000 !important;
        }
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfID" runat="server" />
        <div>
            <h4 style="color: #F58220">
                <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
                <a href="managecontext.aspx">Context</a><img src="button_arrow.jpg" width="29" height="36" />Add/Edit
                Context</h4>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table class="msapDataTbl">
                        <tr>
                            <td style="color: #4e8396;" class="TDWithTopBorder">
                                School:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                School Type:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlSchoolType" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Historical Context:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:TextBox ID="txtHistoricalContext" runat="server" MaxLength="5000" Width="450"
                                    CssClass="msapDataTxt" TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Title I funding:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlTitleFunding" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Percent FRL:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:TextBox ID="txtPercentFRL" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPercentFRL"
                                    FilterType="Numbers">
                                </ajax:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Improvement stage:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlImprovementStage" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Years since made AYP:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlYearsAYP" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Desegregation Plan Type:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlDesegregation" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Program Type:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlProgramType" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Attendance Type:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlAttendanceType" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Program Status:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlProgramStatus" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Missing:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlAdmMMissing" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Planning Year:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlAdmMPlanning" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Open Lottery:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlAdmMOpenLottery" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery :
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlAdmMWeightedLottery" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Title I Status:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlAdmMWeightedLotteryTitleIStatus" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Siblings:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlWeightedLotterySiblings" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Race/Ethnicity:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlWeightedLotteryRace" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Other:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlWeightedOther" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Weighted Lottery by Other_Specify:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:TextBox ID="txtWeightedLottery" runat="server" MaxLength="5000" Width="450"
                                    CssClass="msapDataTxt" TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AdmM_Selective Admissions:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:DropDownList ID="ddlAdmMSelectiveAdmissions" runat="server" CssClass="msapDataTxt">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Notes:
                            </td>
                            <td class="TDWithTopNoRightBorder">
                                <asp:TextBox ID="txtNotes" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine"
                                    CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div style="margin-top: 12px;">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblMessage"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="Button1" runat="server" Text="Save Record" CssClass="surveyBtn2"
                                        OnClick="OnSave" />
                                    <asp:Button ID="Button2" runat="server" Text="Return" CssClass="surveyBtn" OnClick="OnReturn" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button1" />
                    <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
