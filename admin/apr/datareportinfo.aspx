﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="datareportinfo.aspx.cs" Inherits="admin_data_datareportinfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Data Report Info
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 style="color: #F58220">
        <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
        Data Report Status</h4>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfGranteeID" runat="server" />
    <table class="msapDataTbl">
        <tr>
            <td style="color: #4e8396;" class="TDWithTopBorder">
                Site Visit:
            </td>
            <td class="TDWithTopNoRightBorder">
                <asp:DropDownList ID="ddlSiteVisit" runat="server" CssClass="msapDataTxt">
                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Site Visit Occurred:
            </td>
            <td class="TDWithTopNoRightBorder">
                <asp:DropDownList ID="ddlSiteVisitOccurred" runat="server" CssClass="msapDataTxt">
                    <asp:ListItem Value="-9">Please select</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes:
            </td>
            <td class="TDWithTopNoRightBorder">
                <asp:TextBox ID="txtNotes" runat="server" CssClass="msapDataTxt" MaxLength="5000"
                    Width="600" TextMode="MultiLine" Rows="3"></asp:TextBox>
            </td>
        </tr>
    </table>
    <div style="margin-top: 12px;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="Button4" runat="server" Text="Save" CssClass="surveyBtn1" OnClick="OnSaveData" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
