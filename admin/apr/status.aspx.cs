﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_status : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 28 && x.TypeIndex > -9))
            {
                ddlSchoolStatus.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;

            foreach (MagnetSchool School in MagnetSchool.Find(x => x.GranteeID == granteeID))
            {
                ddlSchool.Items.Add(new ListItem(School.SchoolName, School.ID.ToString()));
            }

            if (Request.QueryString["id"] == null)
                Response.Redirect("schoolstatus.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                if (id > 0)
                {
                    hfID.Value = id.ToString();
                    MagnetSchoolStatus Item = MagnetSchoolStatus.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
                    if (Item.SchoolID != null) ddlSchool.SelectedValue = Convert.ToString(Item.SchoolID);
                    if (Item.SchoolStatus != null) ddlSchoolStatus.SelectedValue = Convert.ToString(Item.SchoolStatus);
                    txtNotes.Text = Item.Notes;
                }
                else
                {
                    MagnetSchoolStatus item = new MagnetSchoolStatus();
                    item.ReportID = Convert.ToInt32(hfReportID.Value);
                    item.Save();
                    hfID.Value = item.ID.ToString();
                }
            }
        }
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("schoolstatus.aspx", true);
    }
   
    protected void OnSaveData(object sender, EventArgs e)
    {
        MagnetSchoolStatus Item = MagnetSchoolStatus.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        Item.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        Item.SchoolStatus = Convert.ToInt32(ddlSchoolStatus.SelectedValue);
        Item.Notes = txtNotes.Text;
        Item.Save();
        Response.Redirect("schoolstatus.aspx", true);
    }
}