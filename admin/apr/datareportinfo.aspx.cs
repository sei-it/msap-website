﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_data_datareportinfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/admin/data/datareport.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 26 && x.TypeIndex > -9))
            {
                ddlSiteVisit.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }
            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 17 && x.TypeIndex > -9))
            {
                ddlSiteVisitOccurred.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }


            var data = MagnetDataReportInfo.Find(x => x.ReportID == Convert.ToInt32(hfReportID.Value));
            if (data.Count > 0)
            {
                hfID.Value = data[0].ID.ToString();
                ddlSiteVisit.SelectedValue = Convert.ToString(data[0].SiteVisit);
                ddlSiteVisitOccurred.SelectedValue = Convert.ToString(data[0].SitVisitOccurred);
                txtNotes.Text = data[0].Notes;
            }
        }
    }
    protected void OnSaveData(object sender, EventArgs e)
    {
        MagnetDataReportInfo info = new MagnetDataReportInfo();
        if(!string.IsNullOrEmpty(hfID.Value))
            info = MagnetDataReportInfo.SingleOrDefault(x=>x.ID == Convert.ToInt32(hfID.Value));
        else
            info.ReportID = Convert.ToInt32(hfReportID.Value);
        info.SiteVisit = Convert.ToInt32(ddlSiteVisit.SelectedValue);
        info.SitVisitOccurred = Convert.ToInt32(ddlSiteVisitOccurred.SelectedValue);
        info.Notes = txtNotes.Text;
        info.Save();
    }
}