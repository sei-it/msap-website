﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_sip : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/default.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            //School
            string Username = HttpContext.Current.User.Identity.Name;
            int granteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            foreach (MagnetSchool school in MagnetSchool.Find(x => x.GranteeID == granteeID).OrderBy(x => x.SchoolName))
            {
                ddlSchool.Items.Add(new ListItem(school.SchoolName, school.ID.ToString()));
            }

            foreach (MagnetDLL ReportType in MagnetDLL.Find(x => x.TypeID == 9 && x.TypeIndex > 0))
            {
                ddlStrategyType.Items.Add(new ListItem(ReportType.TypeName, ReportType.TypeIndex.ToString()));
            }

            if (Request.QueryString["option"] == null)
                Response.Redirect("managesip.aspx");
            else
            {
                int id = Convert.ToInt32(Request.QueryString["option"]);
                if (id > 0)
                {
                    hfSIPID.Value = id.ToString();
                    LoadSIPData(id);
                    LoadStrategyData();
                }
            }
        }
    }
    protected void OnSaveSIP(object sender, EventArgs e)
    {
        MagnetSIP sip = new MagnetSIP();
        if (!string.IsNullOrEmpty(hfSIPID.Value))
        {
            sip = MagnetSIP.SingleOrDefault(x => x.ID == Convert.ToInt32(hfSIPID.Value));
        }
        else
        {
            sip.ReportID = Convert.ToInt32(hfReportID.Value);
        }
        sip.SchoolID = Convert.ToInt32(ddlSchool.SelectedValue);
        if (!string.IsNullOrEmpty(txtCurrentAttendance.Text)) sip.CurrentStudentAttendance = Convert.ToInt32(txtCurrentAttendance.Text);
        if (!string.IsNullOrEmpty(txtExpectedAttendance.Text)) sip.ExpectedStudentAttendance = Convert.ToInt32(txtExpectedAttendance.Text);
        if (!string.IsNullOrEmpty(txtCurrentSuspensions.Text)) sip.NumberInSchoolSuspensions = Convert.ToInt32(txtCurrentSuspensions.Text);
        if (!string.IsNullOrEmpty(txtExpectedSuspensions.Text)) sip.ExpectedInSchoolSuspensions = Convert.ToInt32(txtExpectedSuspensions.Text);
        if (!string.IsNullOrEmpty(txtCurrentOutSchoolSuspensions.Text)) sip.NumberOutSchoolSuspensions = Convert.ToInt32(txtCurrentOutSchoolSuspensions.Text);
        if (!string.IsNullOrEmpty(txtExpectedOutSchoolSuspensions.Text)) sip.ExpectedOutSchoolSuspensions = Convert.ToInt32(txtExpectedOutSchoolSuspensions.Text);
        if (!string.IsNullOrEmpty(txtCurrentDropoutRate.Text)) sip.CurrentDropoutRate = Convert.ToInt32(txtCurrentDropoutRate.Text);
        if (!string.IsNullOrEmpty(txtExpectedDropoutRate.Text)) sip.ExpectedDropoutRate = Convert.ToInt32(txtExpectedDropoutRate.Text);
        if (!string.IsNullOrEmpty(txtCurrentParentalInvolvementRate.Text)) sip.CurrentparentalInvolvementRate = Convert.ToInt32(txtCurrentParentalInvolvementRate.Text);
        if (!string.IsNullOrEmpty(txtExpectedParentalInvolvementRate.Text)) sip.ExpectedParentalInvolvementRate = Convert.ToInt32(txtExpectedParentalInvolvementRate.Text);
        if (rblFLSInitiativesMissing.SelectedValue != null) sip.FLSInitiativesMissing = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesMissing.SelectedValue));
        if (rblFLSInitiativesPlanningYear.SelectedValue != null) sip.FLSInitiativesPlannningYear = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesPlanningYear.SelectedValue));
        if (rblFLSInitiativesNotTitleISchool.SelectedValue != null) sip.FLSInitiativesNotTitleISchool = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesNotTitleISchool.SelectedValue));
        if (rblFLSInitiativesTitleIPartA.SelectedValue != null) sip.FLSInitiativesTitleIPartA = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesTitleIPartA.SelectedValue));
        if (rblFLSInitiativesTitleIPartC.SelectedValue != null) sip.FLSInitiativesTitleIPartC = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesTitleIPartC.SelectedValue));
        if (rblFLSInitiativesTitleIPartD.SelectedValue != null) sip.FLSInitiativesTitleIPartD = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesTitleIPartD.SelectedValue));
        if (rblFLSInitiativesTitleII.SelectedValue != null) sip.FLSInitiativesTitleII = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesTitleII.SelectedValue));
        if (rblFLSInitiativesTitleIII.SelectedValue != null) sip.FLSInitiativesTitleIII = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesTitleIII.SelectedValue));
        if (rblFLSInitiativesTitleXhomeless.SelectedValue != null) sip.FLSInitiativesTitleXHomeless = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesTitleXhomeless.SelectedValue));
        if (rblFLSInitiativesSupAcadInstruction.SelectedValue != null) sip.FLSInitiativesSupAcadInstruction = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesSupAcadInstruction.SelectedValue));
        if (rblFLSInitiativesViolencePreventionPrograms.SelectedValue != null) sip.FLSInitiativesViolencePreventionPrograms = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesViolencePreventionPrograms.SelectedValue));
        sip.FLSInitiativesViolencePreventionProgramSpecify = txtFLSInitiativesViolencePreventionProgramsSpecify.Text;
        if (rblFLSInitiativesNutritionPrograms.SelectedValue != null) sip.FLSInitiativesNutritionPrograms = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesNutritionPrograms.SelectedValue));
        sip.FLSInitiativesNutritionProgramsSpecify = txtFLSInitiativesNutritionProgramsSpecify.Text;
        if (rblFLSInitiativesHousingPrograms.SelectedValue != null) sip.FLSInitiativesHousingPrograms = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesHousingPrograms.SelectedValue));
        sip.FLSInitiativesHousingProgramsSpecify = txtFLSInitiativesHousingProgramsSpecify.Text;
        if (rblFLSInitiativesHeadStart.SelectedValue != null) sip.FLSInitiativesHeadStart = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesHeadStart.SelectedValue));
        if (rblFLSInitiativesCareerTechnicalEducation.SelectedValue != null) sip.FLSInitiativesCareerTechnicalEducation = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesCareerTechnicalEducation.SelectedValue));
        if (rblFLSInitiativesJobTraining.SelectedValue != null) sip.FLSInitiativesJobTraining = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesJobTraining.SelectedValue));
        if (rblFLSInitiativesOther.SelectedValue != null) sip.FLSInitiativesOther = Convert.ToBoolean(Convert.ToInt32(rblFLSInitiativesOther.SelectedValue));
        sip.FLSInitiativesOtherSpecify = txtFLSInitiativesOtherSpecify.Text;
        sip.ReadingGoals = txtReadingGoals.Text;
        sip.MathGoals = txtMathGoals.Text;
        sip.Notes = txtNotes.Text;
        sip.Save();
        hfSIPID.Value = sip.ID.ToString();
    }
    private void LoadSIPData(int SIPID)
    {
        MagnetSIP sip = MagnetSIP.SingleOrDefault(x => x.ID == SIPID);
        ddlSchool.SelectedValue = sip.SchoolID.ToString();
        txtCurrentAttendance.Text = sip.CurrentStudentAttendance.ToString();
        txtExpectedAttendance.Text = sip.ExpectedStudentAttendance.ToString();
        txtCurrentSuspensions.Text = sip.NumberInSchoolSuspensions.ToString();
        txtExpectedSuspensions.Text = sip.ExpectedInSchoolSuspensions.ToString();
        txtCurrentOutSchoolSuspensions.Text = sip.NumberOutSchoolSuspensions.ToString();
        txtExpectedOutSchoolSuspensions.Text = sip.ExpectedOutSchoolSuspensions.ToString();
        txtCurrentDropoutRate.Text = sip.CurrentDropoutRate.ToString();
        txtExpectedDropoutRate.Text = sip.ExpectedDropoutRate.ToString();
        txtCurrentParentalInvolvementRate.Text = sip.CurrentparentalInvolvementRate.ToString();
        txtExpectedParentalInvolvementRate.Text = sip.ExpectedParentalInvolvementRate.ToString();
        if (sip.FLSInitiativesMissing != null) rblFLSInitiativesMissing.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesMissing)).ToString();
        if (sip.FLSInitiativesPlannningYear != null) rblFLSInitiativesPlanningYear.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesPlannningYear)).ToString();
        if (sip.FLSInitiativesNotTitleISchool != null) rblFLSInitiativesNotTitleISchool.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesNotTitleISchool)).ToString();
        if (sip.FLSInitiativesTitleIPartA != null) rblFLSInitiativesTitleIPartA.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesTitleIPartA)).ToString();
        if (sip.FLSInitiativesTitleIPartC != null) rblFLSInitiativesTitleIPartC.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesTitleIPartC)).ToString();
        if (sip.FLSInitiativesTitleIPartD != null) rblFLSInitiativesTitleIPartD.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesTitleIPartD)).ToString();
        if (sip.FLSInitiativesTitleII != null) rblFLSInitiativesTitleII.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesTitleII)).ToString();
        if (sip.FLSInitiativesTitleIII != null) rblFLSInitiativesTitleIII.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesTitleIII)).ToString();
        if (sip.FLSInitiativesTitleXHomeless != null) rblFLSInitiativesTitleXhomeless.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesTitleXHomeless)).ToString();
        if (sip.FLSInitiativesSupAcadInstruction != null) rblFLSInitiativesSupAcadInstruction.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesSupAcadInstruction)).ToString();
        if (sip.FLSInitiativesViolencePreventionPrograms != null) rblFLSInitiativesViolencePreventionPrograms.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesViolencePreventionPrograms)).ToString();
        txtFLSInitiativesViolencePreventionProgramsSpecify.Text = sip.FLSInitiativesViolencePreventionProgramSpecify;
        if (sip.FLSInitiativesNutritionPrograms != null) rblFLSInitiativesNutritionPrograms.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesNutritionPrograms)).ToString();
        txtFLSInitiativesNutritionProgramsSpecify.Text = sip.FLSInitiativesNutritionProgramsSpecify;
        if (sip.FLSInitiativesHousingPrograms != null) rblFLSInitiativesHousingPrograms.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesHousingPrograms)).ToString();
        txtFLSInitiativesHousingProgramsSpecify.Text = sip.FLSInitiativesHousingProgramsSpecify;
        if (sip.FLSInitiativesHeadStart != null) rblFLSInitiativesHeadStart.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesHeadStart)).ToString();
        if (sip.FLSInitiativesCareerTechnicalEducation != null) rblFLSInitiativesCareerTechnicalEducation.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesCareerTechnicalEducation)).ToString();
        if (sip.FLSInitiativesJobTraining != null) rblFLSInitiativesJobTraining.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesJobTraining)).ToString();
        if (sip.FLSInitiativesOther != null) rblFLSInitiativesOther.SelectedValue = (Convert.ToInt32(sip.FLSInitiativesOther)).ToString();
        txtFLSInitiativesOtherSpecify.Text = sip.FLSInitiativesOtherSpecify;
        txtReadingGoals.Text = sip.ReadingGoals;
        txtMathGoals.Text = sip.MathGoals;
        txtNotes.Text = sip.Notes;
    }
    protected void OnReturn(object sender, EventArgs e)
    {
        Response.Redirect("managesips.aspx");
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetHQHSSStrategy.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadStrategyData();
    }
    private void LoadStrategyData()
    {
        GridView1.DataSource = MagnetHQHSSStrategy.Find(x => x.MagnetSIPlID == Convert.ToInt32(hfSIPID.Value)).OrderBy(x => x.HQHSSType);
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        ddlStrategyType.SelectedIndex = 0;
        txtStrategy.Text = "";
        txtPersonResponsible.Text = "";
        txtTargetDate.Text = "";
        txtNAExplanation.Text = "";
        hfID.Value = "";
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetHQHSSStrategy data = MagnetHQHSSStrategy.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        ddlStrategyType.SelectedValue = data.HQHSSType.ToString();
        txtStrategy.Text = data.HQHSS;
        txtPersonResponsible.Text = data.HQHSSPersonResponsible;
        txtTargetDate.Text = data.HQHSSTargetDate;
        txtNAExplanation.Text = data.HQHSSNAExplanation;
        mpeWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {

        MagnetHQHSSStrategy data = new MagnetHQHSSStrategy();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetHQHSSStrategy.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            if (string.IsNullOrEmpty(hfSIPID.Value))
            {
                MagnetSIP sip = new MagnetSIP();
                sip.ReportID = Convert.ToInt32(hfReportID.Value);
                sip.Save();
                hfSIPID.Value = sip.ID.ToString();
            }
            data.MagnetSIPlID = Convert.ToInt32(hfSIPID.Value);
        }
        data.HQHSSType = Convert.ToInt32(ddlStrategyType.SelectedValue);
        data.HQHSS = txtStrategy.Text;
        data.HQHSSPersonResponsible = txtPersonResponsible.Text;
        data.HQHSSTargetDate = txtTargetDate.Text;
        data.HQHSSNAExplanation = txtNAExplanation.Text;
        data.Save();
        LoadStrategyData();
    }
}