﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_data_datareport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportPeriodID"] == null)
            {
                Session["DataReportPeriodID"] = 1;
            }

            string Username = HttpContext.Current.User.Identity.Name;
            int GranteeID = (int)MagnetGranteeUser.Find(x => x.UserName.ToLower().Equals(Username.ToLower()))[0].GranteeID;
            var reports = GranteeReport.Find(x => x.ReportType == true && x.ReportPeriodID == Convert.ToInt32(Session["DataReportPeriodID"]) && x.GranteeID == GranteeID);
            if (reports.Count > 0)
            {
                Session["DataReportID"] = reports[0].ID;
            }
            else
            {
                GranteeReport report = new GranteeReport();
                report.GranteeID = GranteeID;
                report.ReportType = true;
                report.ReportPeriodID = Convert.ToInt32(Session["DataReportPeriodID"]);
                report.CreateDate = DateTime.Now;
                report.Save();
                Session["DataReportID"] = report.ID;
            }
        }
    }
}