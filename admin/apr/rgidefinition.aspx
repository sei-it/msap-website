﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="rgidefinition.aspx.cs" Inherits="admin_data_rgidefinition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit MGI Definition
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        .msapDataTbl th
        {
            color: #000000 !important;
        }
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            <a href="managergidefiniton.aspx">MGI Definition</a><img src="button_arrow.jpg" width="29"
                height="36" />Add/Edit MGI Definition
            <asp:Label ID="lblSSID" runat="server"></asp:Label></h4>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <table class="msapDataTbl">
            <tr>
                <td style="color: #4e8396;" class="TDWithTopBorder">
                    Magnet School:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    MGI Definition:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtRGIDefinition" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine"
                        CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    American Indian/ Alaskan Native:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlIndian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Asian:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlAsian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Black or African American (Not of Hispanic Origin):
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlBlack" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Hispanic/Latino:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlHispanic" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Native Hawaiian or Other Pacific Islander:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlHawaiian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtNotes" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine"
                        CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="Button4" runat="server" Text="Save Record" CssClass="surveyBtn1"
                        OnClick="OnSave" />
                    <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" Text="Return" OnClick="OnReturn" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
