﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="AYP.aspx.cs" Inherits="admin_data_AYP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit AYP
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
  <style>
        .msapDataTbl th
        {
            color: #000000 !important;
        }
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            <a href="manageayp.aspx">AYP</a><img src="button_arrow.jpg" width="29"
                height="36" />Add/Edit AYP
            <asp:Label ID="lblSSID" runat="server"></asp:Label></h4>
        <%-- SIP --%>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <table class="msapDataTbl">
            <tr>
                <td style="color: #4e8396;" class="TDWithTopBorder">
                    AYP Report Entity:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlAYPType" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="">Please select</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAYPType"
                        ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AYP Report Type:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlReportType" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="">Please select</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlReportType"
                        ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Number of American Indian/Alaskan Native students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtAmericanIndian" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAmericanIndian"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of American Indian/Alaskan Native students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtAmericanIndianProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtAmericanIndianProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of American Indian/Alaskan Native students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtAmericanIndianPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtAmericanIndianPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did American Indian/Alaskan Native students make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlAmericanIndian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Black or African American students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtBlack" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtBlack"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Black or African American students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtBlackProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtBlackProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of Black or African American students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtBlackPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtBlackPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did Black or African American students make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlBlack" runat="server">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Hispanic/Latino students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtHispanic" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtHispanic"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Hispanic/Latino students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtHispanicProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtHispanicProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of Hispanic/Latino students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtHispanicPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtHispanicPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did Hispanic/Latino students make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlHispanic" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Native Hawaiian or Other Pacific Islander students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtHawaiian" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtHawaiian"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Native Hawaiian or Other Pacific Islander students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtHawaiianProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtHawaiianProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of Native Hawaiian or Other Pacific Islander students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtHawaiianPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender4" TargetControlID="txtHawaiianPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did Native Hawaiian or Other Pacific Islander students make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlHawaiian" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of White students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtWhite" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtWhite"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of White students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtWhiteProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtWhiteProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of White students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtWhitePercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender5" TargetControlID="txtWhitePercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did White students make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlWhite" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of students of Two or more races tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtMoreRaces" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtMoreRaces"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of students of Two or more races proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtMoreRacesProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtMoreRacesProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of students of Two or more races proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtMoreRacesPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender6" TargetControlID="txtMoreRacesPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did students of Two or more races make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlMoreRaces" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Economically Disadvantaged students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtEconomically" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtEconomically"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of Economically Disadvantaged students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtEconomicallyProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtEconomicallyProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of Economically Disadvantaged students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtEconomicallyPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender7" TargetControlID="txtEconomicallyPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did Economically Disadvantaged students make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlEconomically" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Number of English Learner students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtEnglishLearner" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtEnglishLearner"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Number of English Learner students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtEnglishLearnerProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtEnglishLearnerProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage of English Learner students proficient or above:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtEnglishLearnerPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender8" TargetControlID="txtEnglishLearnerPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
            <tr>
                <td>
                    Did English Learner students make AYP?
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlEnglishLearner" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Total number of students tested:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtTotalReading" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtTotalReading"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Total number of students proficient or above State reading standards:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtTotalReadingProficient" runat="server" MaxLength="4" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtTotalReadingProficient"
                        FilterType="Numbers">
                    </ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage proficient or above State reading standards:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtTotalReadingPercentage" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender9" TargetControlID="txtTotalReadingPercentage"
                        Mask="999.99" MessageValidatorTip="true" runat="server" OnInvalidCssClass="MaskedEditError"
                        MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left" DisplayMoney="Left"
                        ErrorTooltipEnabled="True" />
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="Button4" runat="server" Text="Save Record" CssClass="surveyBtn1"
                        OnClick="OnSave" />
                    <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" Text="Return" OnClick="OnReturn"  CausesValidation="false"/>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
