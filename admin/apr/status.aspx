﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="status.aspx.cs" Inherits="admin_data_status" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit School Status
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        function checkWordLen(obj, wordLen, cid) {
            var len = obj.value.split(/[\s]+/);
            $("#" + cid).val(len.length.toString());
            if (len.length > wordLen) {
                alert("You've exceeded the " + wordLen + " word limit for this field!");
                obj.value = obj.SavedValue;
                //obj.value = obj.value.substring(0, wordLen - 1);
                len = obj.value.split(/[\s]+/);
                $("#" + cid).val(len.length.toString());
                return false;
            } else {
                obj.SavedValue = obj.value;
            }
            return true;
        }
    </script>
    <style>
        .msapDataTbl th
        {
            color: #000000 !important;
        }
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            <a href="schoolstatus.aspx">School Status</a><img src="button_arrow.jpg" width="29"
                height="36" />Add/Edit School Status
            <asp:Label ID="lblSSID" runat="server"></asp:Label></h4>
        <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfStatusID" runat="server" />
        <p>
        </p>
        <table class="msapDataTbl">
            <tr>
                <td style="color: #4e8396;" class="TDWithTopBorder">
                    School:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="">Please select</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="ddlSchool"
                        ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    School Status:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:DropDownList ID="ddlSchoolStatus" runat="server" CssClass="msapDataTxt">
                        <asp:ListItem Value="-9">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Notes:
                </td>
                <td class="TDWithTopNoRightBorder">
                    <asp:TextBox ID="txtNotes" runat="server" CssClass="msapDataTxt" MaxLength="5000"
                        Width="450" TextMode="MultiLine" Rows="3"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="Button4" runat="server" Text="Save Record" CssClass="surveyBtn1"
                        OnClick="OnSaveData" />
                    <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" Text="Return" OnClick="OnReturn" CausesValidation="false" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
