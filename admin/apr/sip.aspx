﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="sip.aspx.cs" Inherits="admin_data_sip" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Add/Edit SIP
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        .msapDataTbl th
        {
            color: #000000 !important;
        }
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            <a href="managesips.aspx">SIP</a><img src="button_arrow.jpg" width="29"
                height="36" />Add/Edit SIP
            <asp:Label ID="lblSSID" runat="server"></asp:Label></h4>
        <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajax:ToolkitScriptManager>
        <%-- SIP --%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfReportID" runat="server" />
                <asp:HiddenField ID="hfSIPID" runat="server" />
                <table class="msapDataTbl">
                    <tr>
                        <td style="color: #4e8396;" class="TDWithTopBorder">
                            Magnet School:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:DropDownList ID="ddlSchool" runat="server" CssClass="msapDataTxt">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Strategy:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:Button ID="Button1" runat="server" CssClass="msapBtn" Text="Add Strategy" OnClick="OnAdd" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
                                PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
                                <Columns>
                                    <asp:BoundField DataField="HQHSS" SortExpression="" HeaderText="Strategy" />
                                    <asp:BoundField DataField="HQHSSPersonResponsible" SortExpression="" HeaderText="Person responsible" />
                                    <asp:BoundField DataField="HQHSSTargetDate" SortExpression="" HeaderText="Target date" />
                                    <asp:BoundField DataField="HQHSSNAExplanation" SortExpression="" HeaderText="NA Explanation" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                                                OnClick="OnEdit"></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                                                Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Current student attendance:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtCurrentAttendance" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCurrentAttendance"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2010-11 Expected student attendance:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtExpectedAttendance" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtExpectedAttendance"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of current in-school suspensions:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtCurrentSuspensions" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtCurrentSuspensions"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2010-11 Expected in-school suspensions:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtExpectedSuspensions" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtExpectedSuspensions"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Number of current out-of-school suspensions:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtCurrentOutSchoolSuspensions" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtCurrentOutSchoolSuspensions"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2010-11 Expected out-of-school suspensions:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtExpectedOutSchoolSuspensions" runat="server" MaxLength="250"
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtExpectedOutSchoolSuspensions"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Current dropout rate:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtCurrentDropoutRate" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtCurrentDropoutRate"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2010-11 expected dropout rate:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtExpectedDropoutRate" runat="server" MaxLength="250" CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtExpectedDropoutRate"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Current parental involvement rate:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtCurrentParentalInvolvementRate" runat="server" MaxLength="250"
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtCurrentParentalInvolvementRate"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2010-11 expected parental involvement rate:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtExpectedParentalInvolvementRate" runat="server" MaxLength="250"
                                CssClass="msapDataTxt"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtExpectedParentalInvolvementRate"
                                FilterType="Numbers">
                            </ajax:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Missing:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesMissing" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="rblFLSInitiativesMissing"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_PlanningYear:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesPlanningYear" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rblFLSInitiativesPlanningYear"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Not a Title I School:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesNotTitleISchool" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="rblFLSInitiativesNotTitleISchool"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Title I Part A:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesTitleIPartA" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="rblFLSInitiativesTitleIPartA"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Title I Part C (Migrant):
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesTitleIPartC" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rblFLSInitiativesTitleIPartC"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Title I Part D:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesTitleIPartD" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="rblFLSInitiativesTitleIPartD"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Title II:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesTitleII" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="rblFLSInitiativesTitleII"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Title III:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesTitleIII" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="rblFLSInitiativesTitleIII"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Title X homeless:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesTitleXhomeless" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="rblFLSInitiativesTitleXhomeless"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_SupAcadInstruction:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesSupAcadInstruction" CssClass="DotnetTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="rblFLSInitiativesSupAcadInstruction"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_ViolencePreventionPrograms:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesViolencePreventionPrograms" CssClass="DotnetTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="rblFLSInitiativesViolencePreventionPrograms"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_ViolencePreventionPrograms_Specify:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtFLSInitiativesViolencePreventionProgramsSpecify" MaxLength="5000"
                                runat="server" Width="450" TextMode="MultiLine" Rows="3" CssClass="msapDataTxt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_NutritionPrograms:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesNutritionPrograms" CssClass="DotnetTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="rblFLSInitiativesNutritionPrograms"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_NutritionPrograms_Specify:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtFLSInitiativesNutritionProgramsSpecify" runat="server" MaxLength="5000"
                                CssClass="msapDataTxt" Width="450" TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_HousingPrograms:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesHousingPrograms" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="rblFLSInitiativesHousingPrograms"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_HousingPrograms_Specify:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtFLSInitiativesHousingProgramsSpecify" runat="server" MaxLength="5000"
                                CssClass="msapDataTxt" Width="450" TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Head Start:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesHeadStart" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="rblFLSInitiativesHeadStart"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_CareerTechnicalEducation:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesCareerTechnicalEducation" CssClass="DotnetTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="rblFLSInitiativesCareerTechnicalEducation"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_JobTraining:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesJobTraining" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="rblFLSInitiativesJobTraining"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_Other:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:RadioButtonList ID="rblFLSInitiativesOther" CssClass="DotnetTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="rblFLSInitiativesOther"
                                ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FLSInitiatives_OtherSpecify:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtFLSInitiativesOtherSpecify" runat="server" MaxLength="5000" Width="450"
                                CssClass="msapDataTxt" TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Reading Goals:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtReadingGoals" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine"
                                CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Math Goals:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtMathGoals" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine"
                                CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Notes:
                        </td>
                        <td class="TDWithTopNoRightBorder">
                            <asp:TextBox ID="txtNotes" runat="server" MaxLength="5000" Width="450" TextMode="MultiLine"
                                CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <div style="margin-top: 12px;">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblMessage"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:Button ID="Button4" runat="server" Text="Save Record" CssClass="surveyBtn2"
                                    OnClick="OnSaveSIP" />
                                <asp:Button ID="Button5" runat="server" Text="Return" CausesValidation="false" CssClass="surveyBtn"
                                    OnClick="OnReturn" />
                            </td>
                        </tr>
                    </table>
                </div>
                <%-- Strategy --%>
                <asp:Panel ID="PopupPanel" runat="server">
                    <div class="mpeDiv">
                        <div class="mpeDivHeader">
                            Add/Edit Strategy</div>
                        <table>
                            <tr>
                                <td>
                                    Strategy Type:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlStrategyType" runat="server" CssClass="msapDataTxt">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Staffing Strategy
                                </td>
                                <td>
                                    <asp:TextBox ID="txtStrategy" runat="server" MaxLength="2000" Width="450" TextMode="MultiLine"
                                        CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Person responsible
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPersonResponsible" runat="server" MaxLength="500" Width="450"
                                        CssClass="msapDataTxt"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Target date
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTargetDate" runat="server" CssClass="msapDataTxt"></asp:TextBox>
                                    <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTargetDate">
                                    </ajax:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    NA Explanation
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNAExplanation" runat="server" MaxLength="2000" Width="450" TextMode="MultiLine"
                                        CssClass="msapDataTxt" Rows="3"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave"
                                        CausesValidation="false" />&nbsp;&nbsp;
                                    <asp:Button ID="Button3" runat="server" CssClass="msapBtn" Text="Return" OnClick="OnReturn"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
                <ajax:ModalPopupExtender ID="mpeWindow" runat="server" TargetControlID="LinkButton7"
                    PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button3" CancelControlID="Button3"
                    BackgroundCssClass="magnetMPE" Y="20" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
