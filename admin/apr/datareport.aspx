﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="datareport.aspx.cs" Inherits="admin_data_datareport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Data Entry
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!--[if IE]>
        <style type="text/css">
        table { border-collapse: collapse; }
        </style>
    <![endif]-->
    <style type="text/css">
        legend
        {
            font-size: 1.0em;
            font-weight: bold;
        }
        img
        {
            color: none;
            border: none;
            background: none;
            background-color: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Grantee Database</h1>
        <asp:Panel ID="Panel1" runat="server" GroupingText="Grantee Database">
            <table class="msapQuarterReport">
                <tr>
                    <td width="744">
                        Report Status
                    </td>
                    <td align="center">
                        <a href="datareportinfo.aspx">
                            <asp:Image ID="imgCoverSheet" runat="server" ImageUrl="~/images/pencil.png" alt="report status" /></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        School Status
                    </td>
                    <td align="center">
                        <a href="schoolstatus.aspx">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/pencil.png" alt="School Status" /></a>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                    </td>
                    <td align="center">
                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/enrollment.aspx?datatype=1"
                            Text="School enrollment for LEAs that HAVE converted"></asp:LinkButton>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                    </td>
                    <td align="center">
                        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/enrollment.aspx?datatype=2"
                            Text="Applicant pool for LEAs that HAVE converted"></asp:LinkButton>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                    </td>
                    <td align="center">
                        <asp:LinkButton ID="LinkButton5" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/feederenrollment.aspx"
                            Text="Feeder School enrollment for LEAs that HAVE converted"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        Context
                    </td>
                    <td align="center">
                        <a href="managecontext.aspx">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/pencil.png" alt="manage context" /></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        SIPs
                    </td>
                    <td align="center">
                        <a href="managesips.aspx">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/pencil.png" alt="manage sips" /></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        MGI Objectives
                    </td>
                    <td align="center">
                        <a href="managergiobj.aspx">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/pencil.png" alt="manage mgi objective" /></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        MGI Definition
                    </td>
                    <td align="center">
                        <a href="managergidefinition.aspx">
                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/pencil.png" alt="manage mgi definition" /></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        AYP Data
                    </td>
                    <td align="center">
                        <a href="manageayp.aspx">
                            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/pencil.png" alt="AYP Data" /></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ad Hoc Review
                    </td>
                    <td align="center">
                        <a href="manageadhocreview.aspx">
                            <asp:Image ID="Image7" runat="server" ImageUrl="~/images/pencil.png" alt="Ad Hoc Review" /></a>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                    </td>
                    <td align="center">
                        <asp:LinkButton ID="LinkButton13" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/manageapplicationpool.aspx"
                            Text="Applicant Pool Data"></asp:LinkButton>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                    </td>
                    <td align="center">
                        <asp:LinkButton ID="LinkButton14" runat="server" CssClass="msapBtn linkBtn" PostBackUrl="~/admin/data/manageenrollment.aspx"
                            Text="Enrollment Data Data"></asp:LinkButton>
                    </td>
                </tr>
                <tr class="msapDataTblLastRow">
                    <td>
                        MGI Outcome Data
                    </td>
                    <td align="center" width="80" >
                        <a href="managergioutcome.aspx">
                            <asp:Image ID="Image8" runat="server" ImageUrl="~/images/pencil.png" alt="MGI Outcome Data" /></a>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
