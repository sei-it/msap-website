﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using log4net;

public partial class admin_data_schoolstatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DataReportID"] == null)
            {
                Session.Abandon();
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/login.aspx");
            }
            hfReportID.Value = Convert.ToString(Session["DataReportID"]);

            LoadData();
        }
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetSchools
                   from n in db.MagnetSchoolStatuses
                   from l in db.MagnetDLLs
                   where m.ID == n.SchoolID
                   && n.ReportID == Convert.ToInt32(hfReportID.Value)
                   && n.SchoolStatus == l.TypeIndex
                   && l.TypeID == 28
                   select new { n.ID, m.SchoolName, l.TypeName, n.Notes};
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
    protected void OnNew(object sender, EventArgs e)
    {
        Response.Redirect("status.aspx?id=0", true);
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        string id =  (sender as LinkButton).CommandArgument;
        MagnetSchoolStatus.Delete(x => x.ID == Convert.ToInt32(id));
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        hfID.Value = (sender as LinkButton).CommandArgument;
        Response.Redirect("status.aspx?id=" + hfID.Value, true);
    }
}