﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managecontext.aspx.cs" Inherits="admin_data_managecontext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Context
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            Context</h4>
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    School Name
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("SchoolName")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    School Type
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("SchoolType")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    Historical Context
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("HistoricalContext")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    Title Funding
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("TitleFunding")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    Percent FRL
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("PercentFRL")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle CssClass="TDWithBottomBorder" />
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            CausesValidation="false" OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            CausesValidation="false" Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div style="margin-top: 12px;">
            <table width="100%">
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblMessage"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Button ID="Button1" runat="server" Text="New Context" CssClass="surveyBtn2"
                            OnClick="OnAddGrantee" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hfID" runat="server" />
    </div>
</asp:Content>
