﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="schoolstatus.aspx.cs" Inherits="admin_data_schoolstatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    School Status
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h4 style="color: #F58220">
            <a href="datareport.aspx">Main Menu</a><img src="button_arrow.jpg" width="29" height="36" />
            School Status</h4>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfReportID" runat="server" />
        <asp:HiddenField ID="hfGranteeID" runat="server" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
            PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapDataTbl"
            GridLines="None">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    School Name
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("SchoolName")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    School Status
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("TypeName")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table class="DotnetTbl">
                            <tr>
                                <td>
                                    <img src="../../images/head_button.jpg" align="ABSMIDDLE" />
                                </td>
                                <td>
                                    Notes
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <font color="#4e8396">
                            <%# Eval("Notes")%></font>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle CssClass="TDWithBottomBorder" />
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                            CausesValidation="false" OnClick="OnEdit"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                            CausesValidation="false" Visible="false" OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div style="margin-top: 12px;">
            <table width="100%">
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblMessage"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Button ID="Newbutton" runat="server" Text="New School Status" CssClass="surveyBtn2"
                            OnClick="OnNew" />
                    </td>
                </tr>
            </table>
        </div>
</asp:Content>
