﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="TA_courses.aspx.cs" Inherits="admin_TA_courses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Technical Assistance Courses
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Technical Assistance Courses
    </h1>
   <p>Magnet schools have different missions than traditional schools, and face some different challenges. These technical assistance courses are designed to help MSAP grantees implement their programs as quickly and successfully as possible.  The courses rely on team-based activities to help magnet programs build communities of practice around important magnet content areas.</p>
   <h2 style="margin-bottom:0px;">Theme Integration Courses <img src="../images/Puzzle_icon.png" alt="puzzle pieces" width="35" style="vertical-align:middle;"/></h2> 
   <p>These courses focus on magnet theme integration. The content builds from the first course to the second, or you can pick and choose lessons and tools from each course to meet the needs of individual schools in your magnet program.</p>
   
   <div class="course_info">
   		<img class="left_img_frame" src="../images/TA_course_imgs/MultiColor_kids.jpg" width="106" height="100"/>
 
   		<div class="subtitle_green"><a href="/modules/intro.aspx">Course 1: Facilitating Magnet Theme Integration</a></div>
         <p>This course helps magnet staffs consider whether they are successfully communicating the magnet theme to students, staff, parents, and members of the wider community. Course tools, including the School Walkthrough and Sample Survey Questions, can also be used periodically to assess a school’s level of responsiveness to student and community needs.</p>
   </div>
    <div class="course_info">
   		<img class="left_img_frame" src="../images/TA_course_imgs/Icon2_78732964.jpg" width="106" height="100"/>
 
   		<div class="subtitle_green"><a href="/modules/module2_intro.aspx">Course 2: Mapping the Magnet Theme Into the Curriculum</a></div>
         <p>In this course, magnet leaders and teachers work together to create curriculum maps—or to  revise existing ones—that integrate theme-based content into teaching, learning, and assessment activities. The final lesson supports continued growth by proposing a process of ongoing review, refinement, and renewal of curriculum maps.</p>
   </div>
   
   
   

</asp:Content>
