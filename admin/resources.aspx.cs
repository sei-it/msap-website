﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_resources : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (rbtnlstCohort.SelectedValue == "2010")
        {
            pnlCohort2010.Visible = true;
            pnlCohort2013.Visible = false;
        }
        else
        {
            pnlCohort2010.Visible = false;
            pnlCohort2013.Visible = true;
        }
    }
}