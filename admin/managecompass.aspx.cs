﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_managecompass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        magnetCompass.Delete(x => x.id == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.magnetCompasses
                   //where m.isCurrent == true
                   orderby m.DocumentName
                   select new { m.id, m.Newsletterimage, m.DocumentName, m.CreatedBy, m.CreatedDate, m.modifiedBy, m.isCurrent, m.ModifiedDate };
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtDocumentName.Text = "";
        txtAlt.Text = "";
        edtContent.Content = "";
        rblDisplay.SelectedIndex = 0;
        hfID.Value = "";
        ddlmonth.SelectedIndex = -1;
        ddlyear.SelectedIndex = -1;
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeNewsWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        magnetCompass data = magnetCompass.SingleOrDefault(x => x.id == Convert.ToInt32(hfID.Value));
        bool isOnHomepage = magnetCompass.Find(x => x.isCurrent).Count > 0 ? true : false;
        txtDocumentName.Text = data.DocumentName;
        ddlmonth.SelectedValue = Convert.ToDateTime(data.IssueDate).Month.ToString();
        ddlyear.SelectedValue = Convert.ToDateTime(data.IssueDate).Year.ToString();
        rblDisplay.SelectedIndex = (bool)data.isCurrent ? 1 : 0;
        txtAlt.Text = data.imgAlt;
        edtContent.Content = data.newscontents;

        if (isOnHomepage && !data.isCurrent)
        {
            rblDisplay.Enabled = false;
        }
        else
            rblDisplay.Enabled = true;

        mpeNewsWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        magnetCompass data = new magnetCompass();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = magnetCompass.SingleOrDefault(x => x.id == Convert.ToInt32(hfID.Value));
            data.modifiedBy = HttpContext.Current.User.Identity.Name;
            data.ModifiedDate = DateTime.Now;
        }
        else
        {
            data.CreatedBy = HttpContext.Current.User.Identity.Name;
            data.CreatedDate = DateTime.Now;

        }
        //data.Title = txtTitle.Text; 

        int len = fUploadImage.PostedFile.ContentLength;
        if (len > 0)
        {
            byte[] pic = new byte[len];
            fUploadImage.PostedFile.InputStream.Read(pic, 0, len);
            data.Newsletterimage = pic;
        }
        data.imgAlt = txtAlt.Text;
        data.newscontents = edtContent.Content;
      
        if (fuploadDoc.HasFile)
        {
            string DocName = fuploadDoc.FileName;
            fuploadDoc.SaveAs(Server.MapPath("../doc/" + DocName));
            data.DocURL = "/doc/" + DocName;
            data.IssueDate = Convert.ToDateTime(ddlmonth.SelectedValue + "/01/" + ddlyear.SelectedValue);
            data.DocumentName = DocName;  
        }
        data.isCurrent = rblDisplay.SelectedIndex == 1 ? true : false;
        data.Save();
        LoadData(0);
    }
}