﻿<%@ Page Title="Manage News" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" 
CodeFile="managenews.aspx.cs" Inherits="admin_managenews" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Manage News
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>
        Manage News</h1>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New News" CssClass="msapBtn" OnClick="OnAddNews" />
        <asp:Button ID="btnExcel" runat="server" Text="Track Log" OnClick="ExportExcelReport" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
        PageSize="20" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
        OnPageIndexChanging="OnGridViewPageIndexChanged">
        <Columns>
            <asp:BoundField DataField="Title" SortExpression="" HeaderText="Title" />
            <asp:BoundField DataField="Source" SortExpression="" HeaderText="Source" />
            <asp:BoundField DataField="Author" SortExpression="" HeaderText="Author" />
            <asp:BoundField DataField="Date" SortExpression="" HeaderText="Date" />
            <asp:BoundField DataField="CreatedDate" SortExpression="" HeaderText="Created Date" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnEdit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>'
                        OnClick="OnDelete" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hfID" runat="server" />
    <%-- News --%>
    <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit News</div>
            <table>
                <tr>
                    <td>
                        News Title:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        News Source:
                    </td>
                    <td>
                        <asp:TextBox ID="txtSource" runat="server" MaxLength="250" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        News Author:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAuthor" runat="server" MaxLength="250" Width="470" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        News Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDate" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text="Please type in a correct date." Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        News Content:
                    </td>
                    <td>
                    <cc1:Editor ID="txtContent" runat="server" Width="470" Height="200">
                    </cc1:Editor>
                    </td>
                </tr>
                <tr>
                    <td>
                        Display On Home Page:
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblDisplay" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSaveNews" />
                    </td>
                    <td>
                        <asp:Button ID="Button15" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeNewsWindow" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" OkControlID="Button15" CancelControlID="Button15"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>

