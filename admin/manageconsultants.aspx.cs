﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.IO;

public partial class admin_managemembers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetConsultant.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    private void LoadData(int PageNumber)
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetConsultants
                   select new { m.ID, m.Name, m.Expertise, Image="<img src='../images/upload/" + m.ImagePath + "' />",
                                Active = (bool)m.IsActive?"<img runat='server' src='../images/checkmark.jpg' />":""
                   };
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtMemberName.Text = "";
        txtExpertise.Content = "";
        txtBio.Content = "";
        hfID.Value = "";
        rblActive.ClearSelection();
        lblMessage.Visible = false;
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        mpeNewsWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        MagnetConsultant data = MagnetConsultant.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        //txtTitle.Text = data.Title;
        txtMemberName.Text = data.Name;
        txtExpertise.Content = data.Expertise;
        txtBio.Content = data.ConsultantBio;
        if (data.IsActive != null) rblActive.SelectedIndex = (bool)data.IsActive ? 0 : 1;
        mpeNewsWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetConsultant data = new MagnetConsultant();
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            data = MagnetConsultant.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
            
        //data.Title = txtTitle.Text;
        data.Name = txtMemberName.Text;
        data.Expertise = txtExpertise.Content;
        data.ConsultantBio = txtBio.Content;
        data.IsActive = rblActive.SelectedIndex == 0 ? true : false;
        if (FileUpload1.HasFile)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(FileUpload1.FileBytes, 0, FileUpload1.FileBytes.Length);
            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            if (img.Width > 117 || img.Height > 117)
            {
                lblMessage.Visible = true;
                mpeNewsWindow.Show();
            }
            else
            {
                FileUpload1.SaveAs(Server.MapPath("../images/upload/") + FileUpload1.FileName);
                data.ImagePath = FileUpload1.FileName;
            }
        }
        data.Save();
        LoadData(0);
    }
}