﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_managepastwebinar : System.Web.UI.Page
{
    PastWebinarDataClassesDataContext db = new PastWebinarDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (ddlwebinarCategory.SelectedValue == "" || ddlwebinarCategory.SelectedValue == "1")
        //    pnlWebinar.Visible = false;
        //else
        //    pnlWebinar.Visible = true;

        if (Session["webCatID"] != null)
            ddlwebinarCategory.SelectedValue = Session["webCatID"].ToString();
        Session["webCatID"] = null;
        if (!IsPostBack)
        {
            loaddata();
        }

    }
    private void loaddata()
    {
        var data = cbxAll.Checked ? (from rd in db.PastedWebinarsCategories orderby rd.category select rd) : (from rd in db.PastedWebinarsCategories where rd.isActive orderby rd.category select rd);
        var data1 = cbxAll.Checked ? (from rd in db.PastedWebinarsCategories orderby rd.category select rd) : (from rd in db.PastedWebinarsCategories where rd.isActive || rd.category.Trim().ToLower() == "select one" orderby rd.category select rd);

        gvWebCat.DataSource = data;
        ddlwebinarCategory.DataSource = data1;
        gvWebCat.DataBind();
        ddlwebinarCategory.DataBind();
    }
    protected void OnAddCategory(object sender, EventArgs e)
    {
        mpeNewsWindow.Show();
    }
    protected void OnAddWebinar(object sender, EventArgs e)
    {
         Response.Redirect("editpastwebinar.aspx?catid=" + ddlwebinarCategory.SelectedValue);
    }

    protected void OnEdit(object sender, EventArgs e)
    {
        hfID.Value = (sender as LinkButton).CommandArgument;
        Response.Redirect("editpastwebinar.aspx?webinarid=" + hfID.Value);
    }

    protected void OnSave(object sender, EventArgs e)
    {
        PastedWebinarsCategory rcd = hfCatID.Value == "" ? new PastedWebinarsCategory() : db.PastedWebinarsCategories.SingleOrDefault(x => x.id == Convert.ToInt32(hfCatID.Value));

        rcd.category = txtCatName.Text.Trim();
        rcd.note = txtDes.Text.Trim();
        rcd.isActive = cbxActive.Checked;
        rcd.isPublish = cbxPublish.Checked;
        rcd.CreatedDate = DateTime.Now;
        rcd.CreatedBy = HttpContext.Current.User.Identity.Name;

        if(hfCatID.Value=="")
            db.PastedWebinarsCategories.InsertOnSubmit(rcd);
        db.SubmitChanges();

        Response.Redirect("managepastwebinar.aspx");
    }
    protected void cbxAll_CheckedChanged(object sender, EventArgs e)
    {
        loaddata();
    }

    protected void OnEditCat(object sender, EventArgs e)
    {
        int Catid = Convert.ToInt32((sender as LinkButton).CommandArgument);
        hfCatID.Value = Catid.ToString();
        var data = db.PastedWebinarsCategories.SingleOrDefault(x=>x.id == Catid);
        if (data != null)
        {
            txtCatName.Text = data.category;
            txtDes.Text = data.note;
            cbxActive.Checked = data.isActive;
            cbxPublish.Checked = data.isPublish;

            mpeNewsWindow.Show();
        }

    }
}