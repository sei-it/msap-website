﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="assessment.aspx.cs" Inherits="admin_assessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Needs Assessment
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Needs Assessment Overview</h1>
    <p>
        In order to gain a broad understanding of the MSAP grantees' needs, the MSAP Center
        will conduct needs assessments. The primary purpose of these assessments is to define,
        measure, and prioritize the 2010 MSAP grantees' program implementation, program
        management, and organizational needs. The needs assessment is an evaluation tool
        that will provide valuable data to identify grantees' immediate and long-term needs
        during the three-year grant cycle.
    </p>
  <p>
        <strong>Needs Assessment Framework </strong>
        <br />
        The MSAP Center will apply a modified version of Burton and Merrill's (1994) Needs
        Assessment Framework to identify and prioritize the 2010 MSAP grantees' program
        management and implementation needs. The four-step process will be used to collect
        and analyze the needs of these grantees to identify gaps in grantee resources, training,
        and support systems.
    </p>
    
            
        <p><img src="../images/assessment.jpg" title="Needs Assessment" style="float: left; margin-right: 8px;" />
            The Needs Assessment Framework illustrates the four steps of conducting a needs
            assessment. The first step determines "what ought to be", which is defined through
                program goals, objectives, and targeted outcomes. This step establishes a common
                vision for MSAP projects and how they should ideally operate.        </p>
<p>
      The second step in the needs assessment framework establishes "what is", which depicts
                the actual condition of each grant project, including student characteristics, staff
                performance, parental involvement, and relevant external factors. In addition to
                determining "what is" for MSAP projects, we will also determine the breath of resources
                available to magnet programs.
            </p>
            <p>
                The third step identifies discrepancies between actual and desired performance,
                which involves comparing the information collected in the previous two steps in
                a systematic way to detect performance gaps.
            </p>
            <p>
                The fourth step establishes priorities for action using all of the information gleaned
                from the previous three steps.
            </p>
       
    <p style="border-top: 1px solid #999; padding-top: 4px;"><small>
        Burton, J. K., & Merrill, P. F. (1991) Needs assessment: Goals, needs, and priorities.
        In I. J. Briggs., K. L. Gustafson & M. Tillman (Eds.), Instruction design: Principles
        and applications. Englewood Cliffs, NJ: Educational Technology.</small>
    </p>
</asp:Content>