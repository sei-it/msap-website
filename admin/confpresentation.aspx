﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="confpresentation.aspx.cs" Inherits="admin_confpresentation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">Conference presentations
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="border:none;">Conference Presentations</h1>

        <strong class="pastconf_subtitle">An Analysis of MSAP Grantee Contextual Factors and Project Activities</strong><i>(4/24/2015)</i><br />

    <p>
       This presentation was given at the 2015 Magnet Schools of America 2015 National Conference.  In this session, the presenters share descriptive statistics about how the 2013 MSAP cohort is implementing some of these required program components including information about school context and MSAP project activities. Budget expenditure and cost efficiency data are also presented along with annual performance data for grant year 1. All magnet leaders will benefit from learning about the successes and challenges of this group of magnet schools.
	</p>

    <div style="float: left; margin-top: 0; width: 98%; padding-bottom:15px;border-left:0px solid #A9DEF0;padding-left:20px;">
	<div id="Div3" runat="server" class="indent1">
        <table width='100%' border='0' cellspacing='0' cellpadding='3'>
            <tr class="teal_txt">
                <td width='70%'>
                    <b>File name</b>
                </td>
                <td width='30%'>
                    <span style='font-weight: bold;'>Files available</span>
                </td>
            </tr>
            <tr  bgcolor='#E8E8E8'>
                <td>
                    Analysis of MSAP Grantee Factors and Activities Presentation
                </td>
                <td width='20%'>
                    <img src="../images/PDFlogo.png" alt="PDF" style="margin-left: 10px; margin-right: 4px;
                vertical-align: middle; margin-bottom: 6px;" /><a href="doc/conference_presentation/MSAP_MSA_Presentation.pdf" target="_blank">Download
                    PDF</a> 
                </td>
            </tr>
        </table>
    </div>

    </div>

    
</asp:Content>

