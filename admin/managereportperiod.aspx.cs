﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using ExcelLibrary.SpreadSheet;

public partial class admin_managesurvey : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (MagnetReportPeriod reportPeriod in MagnetReportPeriod.All())
            {
                ddlPeriod.Items.Add(new ListItem(reportPeriod.ReportPeriod, reportPeriod.ID.ToString()));
            }
            if (ddlPeriod.Items.Count > 0)
                FetchPeriodData(Convert.ToInt32(ddlPeriod.Items[0].Value));
        }
    }
    private void FetchPeriodData(int periodID)
    {
        MagnetReportPeriod reportPeriod = MagnetReportPeriod.SingleOrDefault(x => x.ID == periodID);
        txtFrom.Text = reportPeriod.PeriodFrom;
        txtTo.Text = reportPeriod.PeriodTo;
    }
    protected void OnPeriodChanged(object sender, EventArgs e)
    {
        FetchPeriodData(Convert.ToInt32(ddlPeriod.SelectedValue));
    }
    protected void OnSave(object sender, EventArgs e)
    {
        MagnetReportPeriod reportPeriod = MagnetReportPeriod.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlPeriod.SelectedValue));
        reportPeriod.PeriodFrom = txtFrom.Text;
        reportPeriod.PeriodTo = txtTo.Text;
        reportPeriod.Save();
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data has been saved!');</script>", false);
    }
}