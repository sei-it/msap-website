﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using Synergy.Magnet;

public partial class admin_magnetadmin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getMasterTitle();

        //Home
        LoginView6.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        LoginViewHome.RoleGroups[0].Roles = new string[16] { "School Staff","Principal","moderator","twg","Magnet Coordinator","Consultant","District Staff","Teacher","Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        //TWG
        LoginView7.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        LoginViewTWG.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        //Events
        LoginView8.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        //Resources
        LoginView9.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        LoginViewRsc.RoleGroups[0].Roles = new string[16] {"School Staff","Principal","moderator","twg","Magnet Coordinator","Consultant","District Staff","Teacher","Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        //Magnet Connector
        LoginView11.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        LoginViewMagnet.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };

        //Prof. Reporting
        LoginView10.RoleGroups[0].Roles = new string[9] { "moderator","Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
        LoginViewProfReport.RoleGroups[0].Roles = new string[9] { "moderator","Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };

        LoginView lvwMygrantee = null;
        LoginView loginvwPastReport = null;

        ITemplate template = LoginView12.RoleGroups[0].ContentTemplate;
        if (template != null)
        {
            Control container = new Control();
            template.InstantiateIn(container);

            foreach (Control c in container.Controls)
            {
                if (c is LoginView)
                {
                    if (c.ID == "lvwMygrantee")
                    {
                        lvwMygrantee = c as LoginView; 
                    }
                    else if (c.ID == "loginvwPastReport")
                    {
                        loginvwPastReport = c as LoginView; 
                    }

                }
            }
        }


        if (MagnetReportPeriod.Find(x => x.isReportPeriod == true).Count() > 0)  //report period open
        {
            //MAPS tag
            LoginView12.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };
            lvwMygrantee.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };
            loginvwPastReport.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };
          
            //Data
            LoginView1.RoleGroups[0].Roles = new string[3] { "Administrators", "Data", "ED" };
            //Admin
            LoginView15.RoleGroups[0].Roles = new string[1] { "Administrators"};
        }
        else if (HttpContext.Current.User.Identity.Name == "user13")  //report period close when use is user13
            LoginView12.RoleGroups[0].Roles = new string[5] { "Administrators", "ED", "GrantStaff", "Data", "projectdirector" };
        else  //report period close
        {
            ////Home
            //LoginView6.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //LoginViewHome.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            ////TWG
            //LoginView7.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //LoginViewTWG.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            ////Events
            //LoginView8.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            ////Resources
            //LoginView9.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //LoginViewRsc.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            ////Magnet Connector
            //LoginView11.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //LoginViewMagnet.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };
            ////Prof. Reporting
            //LoginView10.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //LoginViewProfReport.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };

            //MAPS
            LoginView12.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };
            lvwMygrantee.RoleGroups[0].Roles = new string[4] { "Administrators", "ED", "GrantStaff", "Data" };
            loginvwPastReport.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };

            //LoginView loginvwPastReport = LoginView12.FindControl("loginvwPastReport") as LoginView;
            //loginvwPastReport.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };

            //Data
            LoginView1.RoleGroups[0].Roles = new string[3] { "Administrators", "Data","ED" };
            //Admin
            LoginView15.RoleGroups[0].Roles = new string[1] { "Administrators" };
        }
        if (!Page.IsPostBack)
        {
        }
    }
    protected void OnLogout(object sender, EventArgs e)
    {
        Session.Abandon();
        FormsAuthentication.SignOut();
        Response.Redirect("~/Default.aspx", true);
        //FormsAuthentication.RedirectToLoginPage();
    }
    protected void On1STQuarter(object sender, EventArgs e)
    {
        Session["ReportPeriodID"] = 1;
        Response.Redirect("~/admin/data/quarterreports.aspx", true);
    }

    private void getMasterTitle()
    {
        string[] dbcnnParams = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString.Split(';');
        string dbname = "";

        foreach (string dbtmp in dbcnnParams)
        {

            if (dbtmp.Contains("Initial Catalog"))
            {
                dbname = dbtmp.Substring(dbtmp.IndexOf('=') + 1);
                break;
            }

        }

        switch (dbname.ToLower())
        {
            case "magnet_dev":
                ltlTitle.Text = "DEVELOPMENT SITE";
                break;
            case "magnet_staging":
                ltlTitle.Text = "STAGING SITE";
                break;
            case "magnet_staging1":
                ltlTitle.Text = "Magnet Mirror Site";
                break;
            case "magnet2015preprod":
                ltlTitle.Text = "Pre-Production Site";
                break;
			case "magnet01":
                ltlTitle.Text = "Demo site";
                break;
            case "magnet":
                ltlTitle.Text = "";
                break;
            default:
                ltlTitle.Text = "Other DB site";
                break;
        }
    }
}
