﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageundeclared.aspx.cs" Inherits="manageundeclared" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Survey Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Manage Grantee Undeclared Field</h1>
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table>
                <tr>
        <td colspan="2">
        <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" 
                RepeatDirection="Horizontal" onselectedindexchanged="rbtnlstCohort_SelectedIndexChanged" >
                <asp:ListItem Value="1"  >MSAP 2010 Cohort</asp:ListItem>
                <asp:ListItem Value="2" >MSAP 2013 Cohort</asp:ListItem>
        </asp:RadioButtonList>
        </td>
        </tr>
                    <tr>
                        <td>
                            Grantee
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGrantees" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnGranteeChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Report
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlReports" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnReportChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server" id="dataRow" visible="false">
                        <td>
                            Undeclared Field Available?
                        </td>
                        <td>
                            <asp:RadioButton ID="rbYes" runat="server" GroupName="dd" Text="Yes" />
                            <asp:RadioButton ID="rbNo" runat="server" GroupName="dd" Text="No" />
                        </td>
                    </tr>
                    <tr runat="server" id="saveRow" visible="false">
                        <td colspan="2" align="center">
                            <asp:Button ID="button1" runat="server" Text="Save" CssClass="msapBtn" OnClick="OnSave" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <h1 style="visibility:hidden">
            Manage Desegregation Plan</h1>
        <table style="visibility:hidden">
            <tr>
                <td>
                    Grantee
                </td>
                <td>
                    <asp:DropDownList ID="ddlGrantees2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnGrantee2Changed">

                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Only one document required for required desegregation plan?
                </td>
                <td>
                    <asp:RadioButton ID="RadioButton1" runat="server" GroupName="dd" Text="Yes" />
                    <asp:RadioButton ID="RadioButton2" runat="server" GroupName="dd" Text="No" />
                </td>
            </tr>
            <tr runat="server" id="Tr2" visible="true">
                <td colspan="2" align="center">
                    <asp:Button ID="button2" runat="server" Text="Save" CssClass="msapBtn" OnClick="OnSaveDesegregation" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
