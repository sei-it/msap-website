﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="managedesegregationplan.aspx.cs" Inherits="managedesegregationplan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage Survey Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <ajax:ToolkitScriptManager ID="Manager1" runat="server">
        </ajax:ToolkitScriptManager>
        <asp:HiddenField ID="hfID" runat="server" />
        <h1>
            Manage Desegregation Plan</h1>
        <table>
            <tr>
                <td>
                    Grantee
                </td>
                <td>
                    <asp:DropDownList ID="ddlGrantees2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnGrantee2Changed">
                        <asp:ListItem Value="">Please select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Only one document required for required desegregation plan?
                </td>
                <td>
                    <asp:RadioButton ID="RadioButton1" runat="server" GroupName="dd" Text="Yes" />
                    <asp:RadioButton ID="RadioButton2" runat="server" GroupName="dd" Text="No" />
                </td>
            </tr>
            <tr runat="server" id="Tr2" visible="true">
                <td colspan="2" align="center">
                    <asp:Button ID="button2" runat="server" Text="Save" CssClass="msapBtn" OnClick="OnSaveDesegregation" />
                </td>
            </tr>
        </table>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" Visible="false">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            Grantee
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGrantees" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnGranteeChanged">
                                <asp:ListItem Value="">Please select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Report
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlReports" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnReportChanged">
                                <asp:ListItem Value="">Please select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server" id="dataRow" visible="false">
                        <td>
                            Undeclared Field Available?
                        </td>
                        <td>
                            <asp:RadioButton ID="rbYes" runat="server" GroupName="dd" Text="Yes" />
                            <asp:RadioButton ID="rbNo" runat="server" GroupName="dd" Text="No" />
                        </td>
                    </tr>
                    <tr runat="server" id="saveRow" visible="false">
                        <td colspan="2" align="center">
                            <asp:Button ID="button1" runat="server" Text="Save" CssClass="msapBtn" OnClick="OnSave" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
