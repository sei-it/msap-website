﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using ExcelLibrary.SpreadSheet;

public partial class admin_manageupload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FileUpload1.Attributes.Add("onchange", "return validateFileUpload(this);");
            Newbutton.Attributes.Add("onclick", "return validateFileUpload(document.getElementById('" + FileUpload1.ClientID + "'));");

            LoadData();
        }
    }
    protected void OnUpload(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            MagnetAdminUpload upload = new MagnetAdminUpload();
            upload.FileName = FileUpload1.FileName;
            String PhysicalName = Guid.NewGuid().ToString() + "-" + FileUpload1.FileName;
            FileUpload1.SaveAs(Server.MapPath("../upload/") + PhysicalName);
            upload.PhysicalName = PhysicalName;
            upload.UploadDate = DateTime.Now;
            upload.UploadUser = Context.User.Identity.Name;
            upload.Save();
            LoadData();
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        MagnetAdminUpload.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData();
    }
    private void LoadData()
    {
        MagnetDBDB db = new MagnetDBDB();
        var data = from m in db.MagnetAdminUploads
                   select new { m.ID, FileLink = "<a target='_blank' href='../upload/" + m.PhysicalName + "'>" + m.FileName + "</a>", m.UploadDate };
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}