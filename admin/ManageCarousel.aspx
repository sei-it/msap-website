﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="ManageCarousel.aspx.cs" Inherits="admin_ManageCarousel" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 431px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <h1>Manage Carousel</h1>
 <p style="text-align:left">
        <asp:Button ID="Newbutton" runat="server" Text="New Carousel" CssClass="msapBtn" OnClick="OnAddCarousel" />
    </p>
     <asp:GridView ID="gvCarousel" runat="server" AllowPaging="True"
        PageSize="20" AutoGenerateColumns="False" DataKeyNames="id" 
        CssClass="msapTbl" DataSourceID="ldsCarousel">
         <Columns>
             <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" Visible="false"
                 ReadOnly="True" SortExpression="id" />
                 <asp:BoundField DataField="title" HeaderText="Title" SortExpression="title" />
                  <asp:TemplateField>
             <HeaderTemplate>Image</HeaderTemplate>
                <ItemTemplate>
                <asp:Image ID="imageid" runat="server" Width="100px" ImageUrl='<%#"../img/Handler.ashx?CarouselID=" + Eval("id")%>' />
                </ItemTemplate>
            </asp:TemplateField>
                  <asp:BoundField DataField="FileName" HeaderText="File Name" Visible="false"   SortExpression="FileName" />
                 <asp:BoundField DataField="image_alt" HeaderText="image alt" Visible="false"  SortExpression="image_alt" />
             
                 <asp:BoundField DataField="body" HeaderText="Contents" SortExpression="body" Visible="false" />
                
                 <asp:BoundField DataField="CreatedBy" HeaderText="Created By" 
                     SortExpression="CreatedBy" />
                 <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" 
                     SortExpression="CreatedDate" />
                 <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By" 
                     SortExpression="ModifiedBy" />
                 <asp:BoundField DataField="ModifiedDate" HeaderText="Modified Date" 
                     SortExpression="ModifiedDate" />
                 <asp:CheckBoxField DataField="isActive" HeaderText="Active" 
                     SortExpression="isActive" />
                    <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("id") %>'
                            OnClick="OnEdit"></asp:LinkButton>
                   
                </ItemTemplate>
            </asp:TemplateField>
         </Columns>
        
    </asp:GridView>
     <asp:Panel ID="PopupPanel" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Add/Edit Carousel</div>
             <table>
            <tr>
            <td>Image:</td>
            <td class="style1">
            <asp:FileUpload ID="fuploadimg" runat="server" CssClass="msapDataTxt" Height="19px" Width="100%" />
            </td>
            <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="fuploadimg" 
ErrorMessage="Invalid Image File" ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.gif|.GIF|.png|.PNG)$"> 
</asp:RegularExpressionValidator> 
            </td>
            </tr>
            <tr>
            <td>Image File Name:</td>
            <td class="style1"><asp:TextBox ID="txtFileName" runat="server" Width="100%"   /></td>
                
            <td></td>
            </tr>
            <tr>
            <td>Image Alt.:</td>
            <td class="style1"> 
            <asp:TextBox ID="image_altTextBox" runat="server" Width="100%"  />
            </td>
            <td></td>
            </tr>
            <tr>
            <td>Title:</td>
            <td class="style1"><asp:TextBox ID="titleTextBox" runat="server" Width="100%"  />
            </td>
            <td></td>
            </tr>
            <tr>
            <td valign="top">Contents:</td>
                
            <td  valign="top" class="style1"><cc:Editor ID="bodyTextBox" runat="server"  /></td>
            <td rowspan="2"  valign="top">
            <asp:Image ID="ImageID" runat="server" Width="100px" Height="120px"></asp:Image>
            <br/><br/><br/><div style='text-align:center'>
                <asp:Literal ID="ltlImgName" runat="server" />
            </div>
            </td>
            </tr>
            <tr>
            <td>Display on Page:</td>
            <td class="style1" >
             <asp:RadioButtonList ID="rblActive"  runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="No" >No</asp:ListItem>
                            <asp:ListItem Value="Yes" >Yes</asp:ListItem>
                        </asp:RadioButtonList>
            </td>
           

            </tr>
                 <tr>
                    <td>
                        <asp:Button ID="btnSaveFile" runat="server" CssClass="msapBtn" Text="Save File" OnClick="OnSaveUploadFile" />
                    </td>
                    <td colspan="2">
                        <asp:Button ID="btnSaveFileCancel" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
   <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
    <ajaxToolkit:modalpopupextender ID="mpeAddUploadFile" runat="server" TargetControlID="LinkButton7"
        PopupControlID="PopupPanel" DropShadow="true" 
        OkControlID="btnSaveFileCancel" CancelControlID="btnSaveFileCancel"
        BackgroundCssClass="magnetMPE" Y="20" />
   
    <asp:LinqDataSource ID="ldsCarousel" runat="server" 
         ContextTypeName="CarouselDataClassesDataContext" EntityTypeName="" 
         TableName="MagnetCarousels" >
    </asp:LinqDataSource>
    <asp:HiddenField ID="hfID" runat="server" />
</asp:Content>

