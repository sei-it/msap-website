﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class admin_consultants : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='consultTbl'>");
            var data2 = MagnetConsultant.Find(x=>x.IsActive == true).ToList();
            //data2.Sort(CompareMemberName);
            var data = data2.ToArray();
            int pos = 0;
            while (pos < data.Length)
            {
                if (pos % 2 == 0)
                {
                    if(pos>0)
                        sb.Append("</tr>");
                    sb.Append("<tr>");
                }
                sb.AppendFormat("<td href='consultantdetail.aspx?id={1}&height=300&width=300' class='thickbox'><div class='consult_title'>{0}</div><div class='consult_info'><i>Expertise</i>{2}</div><img src='../images/upload/{3}' class='consult_img'/></td>", data[pos].Name, data[pos].ID, data[pos].Expertise, data[pos].ImagePath);
                pos++;
            }
            sb.Append("</tr>");
            sb.Append("</table>");
            content.InnerHtml = sb.ToString();
        }
    }
    private static int CompareMemberName(MagnetConsultant x, MagnetConsultant y)
    {
        string x1 = x.Name.Split(' ')[1];
        string x2 = y.Name.Split(' ')[1];
        return x1.CompareTo(x2);
    }
}