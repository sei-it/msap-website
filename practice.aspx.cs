﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using log4net;
using System.Web.Security;
using Synergy.Magnet;

public partial class ptractice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (Session["Magnet"] != null)
            {
                Session["ReportPeriodID"] = MagnetReportPeriod.SingleOrDefault(x => x.isReportPeriod == true).ID; 
                Response.Redirect("~/practice/data/quarterreports.aspx");
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            ValdiateUser();
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = ex.Message;
            ILog Log = LogManager.GetLogger("EventLog");
            Log.Fatal("User login.", ex);
        }
    }

    protected void Clear_Click(object sender, EventArgs e)
    {
        txtUserName.Text = String.Empty;
        txtPassword.Text = String.Empty;
        lblError.Visible = false;
    }

    private void ValdiateUser()
    {

        if (Membership.ValidateUser(txtUserName.Text, txtPassword.Text))
        {
            String sPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text, "md5");

            Session["Magnet"] = "Login";
            FormsAuthentication.SetAuthCookie(txtUserName.Text, true);

            if (ViewState["ReturnUrl"] != null)
            {
                Response.Redirect(ViewState["ReturnUrl"].ToString());
            }
            else
            {
                Response.Redirect("~/practice/data/quarterreports.aspx");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = "Your username and/or password are invalid.";
        }
    }

    protected void btnLostPassword_Click(object sender, EventArgs e)
    {
        LoginView.Visible = false;
        RecoverView.Visible = true;
        ResetView.Visible = false;
    }
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        LoginView.Visible = false;
        RecoverView.Visible = false;
        ResetView.Visible = true;
    }
    protected void OnChangePassword(object sender, EventArgs e)
    {
        MembershipUser user = Membership.GetUser(txtResetUserName.Text);
        user.ChangePassword(txtOldPassword.Text, txtNewPassword.Text);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('Password change successfully!');</script>", false);
    }
    protected void Recover_Click(object sender, EventArgs e)
    {
        lblError.Visible = false;
        try
        {
            if (LostUserName.Text.Length == 0)
            {
                lblError.Visible = true;
                lblError.Text = "User name is required.";
                return;
            }

            MembershipUser user = Membership.GetUser(LostUserName.Text);
            string oldPassword = user.ResetPassword();

            //send email
            string strFrom = "SEInfo@seiservices.com";
            MailMessage objMailMsg = new MailMessage(strFrom, user.Email);

            objMailMsg.BodyEncoding = Encoding.UTF8;
            objMailMsg.Subject = "Your new password for Magnet website.";
            objMailMsg.Body = "<p>Dear " + user.UserName + ",</p><p>Here is your new password for magnet web site. " + oldPassword + "</p>";
            objMailMsg.Priority = MailPriority.High;
            objMailMsg.IsBodyHtml = true;

            //prepare to send mail via SMTP transport
            SmtpClient objSMTPClient = new SmtpClient();
            objSMTPClient.Host = "sei-exch01.synergyentinc.local";
            NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
            objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
            objSMTPClient.Send(objMailMsg);
            LoginView.Visible = true;
            RecoverView.Visible = false;
        }
        catch (Exception x)
        {
            ILog Log = LogManager.GetLogger("EventLog");
            Log.Fatal("Recover password.", x);
            lblError.Visible = true;
            lblError.Text = "There was an error when creating your new password.";
        }
    }
}
