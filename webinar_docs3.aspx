﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Technical Assistance Webinar</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 14px;
    font-weight: bold;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}

#download_list li{
	padding-bottom:10px;
}
-->
</style></head>
<body class="oneColElsCtrHdr">

<div id="container">
  <div id="header">
    <img src="Webinar_docs/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  <div id="mainContent">
    <h1>Program Sustainability Resources</h1>
   
   <table width="90%" border="0">
   		<tr>
 			<td style="text-align:right;vertical-align:top;">
            	<img src="Webinar_docs/Program_sustainability_webinar_5_2_12/PS_webinar_img3.jpg" style="border:2px #2274A0 solid;" />
            </td>
   			<td style="vertical-align:top;padding-left:15px;">
             <div><span id="subtitle">Planning for Sustainability: Creating a Strategic Financing Approach</span><br/>
  Thank you for registering for the third webinar in the MSAP Center's Planning for Sustainability series. This will be held on May 2, 2012, from 1:00 p.m. to 2:00 p.m. Eastern time. 

<p>Topics will include taking into account the total anticipated costs of your projects, and the array of cash and in-kind resources that may be available to offset those costs. Using this strategic financing approach will help you better understand what resource gaps to expect, when you are likely to need support, and how to align your financing strategies to address your needs.</p>

   Here are the tools for this webinar: 
   </div>
            <ol id="download_list">
                <li><strong>Clarifying What MSAP Grantees Want to Sustain</strong>: To help you define the scope and scale of project activities, this worksheet shows how to break down each activity and think about how it may need to be sustained differently over time. <a href="Webinar_docs/Program_sustainability_webinar_5_2_12/Wk1_What_do_you_want_to_sustain.doc" target="_blank">Download</a>
              </li>
                <li><strong>Determining the Full Cost of an MSAP Project</strong>: This tool can help MSAP grantees and school leaders look beyond the U.S. Department of Education budget template when estimating fiscal needs for their projects. 
                <a href="Webinar_docs/Program_sustainability_webinar_5_2_12/Wk2_Cost_Worksheet.doc" target="_blank">Download</a>
                </li>
                <li><strong>Determining the Resources Available to MSAP Grantees</strong>: This worksheet can help you take an inventory of your current resources and assess what resources you may have in the future. 
                <a href="Webinar_docs/Program_sustainability_webinar_5_2_12/Wk3_Resources_Worksheet.doc" target="_blank">Download</a>
                </li>
                <li><strong>Assess the Resource Gaps in an MSAP Project</strong>: On this worksheet you will assess the resource gap between what you need and what you have by taking the inventory of current resources (from Worksheet 3) and subtracting the estimated fiscal needs (from Worksheet 2) to calculate the remaining resource gap (or, if you’re fortunate, surplus). 
                <a href="Webinar_docs/Program_sustainability_webinar_5_2_12/Wk4_Assess_Funding_Gaps.doc" target="_blank">Download</a>
                </li>
            </ol>
            </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
     </table>
	<!-- end #mainContent --></div>
  <div id="footer">
    <img src="Webinar_docs/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>
</body>
</html>
