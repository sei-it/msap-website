﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP - Login
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <asp:Panel ID="LoginView" runat="server" DefaultButton="btnSubmit">
            <h1>
                Login to the MSAP Center Private Workspace
            </h1>
         <!--   <p>
                Enter your username and password below to access the MSAP Center’s private workspace.</p>-->
                <p style="width:65%;">This content is designed for members of the MSAP grant cohort. Please log in here to access the Magnet Connector, the magnet theme integration course, MAPS, and more.</p>
            <div style="padding-top: 20px;">
                <table border="0" cellpadding="8" cellspacing="0">
                    <tr>
                        <td align="left">
                            <strong>Username:</strong>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox class="msapTxt" ID="txtUserName" runat="server" Width="148" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>Password:</strong>
                        </td>
                        <td>
                            <asp:TextBox class="msapTxt" ID="txtPassword" runat="server" TextMode="Password"
                                Width="148" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button class="msapBtn" ID="btnSubmit" Text="Login" runat="server" OnClick="btnSubmit_Click" />&nbsp;&nbsp;
                            <asp:Button ID="Clear" runat="server" class="msapBtn" Text="Clear" OnClick="Clear_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnLostPassword" class="msapBtn" runat="server" Text="Forgot Password"
                                OnClick="btnLostPassword_Click" />
                            <asp:Button ID="Button2" class="msapBtn" runat="server" Text="Change Password" OnClick="btnChangePassword_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<asp:Label ID="lblUserID" runat="server" Visible="false" EnableViewState="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="RecoverView" runat="server" Visible="false" DefaultButton="Recover">
            <h1>
                Get Temporary Password
            </h1>
            <p>
                Enter your username below to have a temporary password sent to your email.</p>
            <div>
                <table border="0" cellpadding="8" cellspacing="0">
                    <tr>
                        <td align="left">
                            Username:&nbsp;
                        </td>
                        <td>
                            <asp:TextBox class="msapTxt" ID="LostUserName" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="middle">
                            <asp:Button ID="Recover" runat="server" class="msapBtn" Text="Get temporary password"
                                CausesValidation="false" OnClick="Recover_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="ResetView" runat="server" Visible="false" DefaultButton="button1">
            <h1>
                Change Password</h1>
            <p>
                Your new password must be at least 6 characters.</p>
            <table>
                <tr>
                    <td align="left">
                        Username:&nbsp;
                    </td>
                    <td>
                        <asp:TextBox class="msapTxt" ID="txtResetUserName" runat="server" Width="140" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtResetUserName"
                            ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Old Password:
                    </td>
                    <td>
                        <asp:TextBox ID="txtOldPassword" runat="server" class="msapTxt" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="txtOldPassword"
                            ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        New Password:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewPassword" runat="server" class="msapTxt" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewPassword"
                            ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Your new password must be at least 6 characters."
                            ControlToValidate="txtNewPassword" ValidationExpression="[0-9a-zA-Z!@#$%^&*()]{6,}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Repeat Password:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRepeatPassword" runat="server" class="msapTxt" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRepeatPassword"
                            ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewPassword"
                            ControlToValidate="txtRepeatPassword" ErrorMessage="Please re-type your new password!"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="middle">
                        <asp:Button ID="button1" runat="server" class="msapBtn" Text="Change Password" CausesValidation="true"
                            OnClick="OnChangePassword" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <p style="color: red;">
            <asp:Label class="etacTxt" ID="lblError" runat="server" Visible="false" />
        </p>
    </div>
</asp:Content>
