﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_STEMBlogDL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
 displayRecords();
        }
       
    }
    private void displayRecords()
    {
        object objVal = null;
        using (STEMDataContext db = new STEMDataContext())
        {

            var vCases = db.get_STEMBlogBlogPageList();
            //HttpContext.Current.User.Identity.Name

            repBlogs.DataSource = vCases;
            repBlogs.DataBind();


        }

    }
    protected void repResources_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Literal oLit;
        string sResName = "";
        if (DataBinder.Eval(e.Item.DataItem, "ResourceLink") != null)
        {
            //  sResName = @" <a href=""" + DataBinder.Eval(e.Item.DataItem, "ResourceLink").ToString() +  @""" target=""_blank"">" + DataBinder.Eval(e.Item.DataItem, "ResourceName").ToString() + "</a>" ;
            sResName = @" <a href=""" + DataBinder.Eval(e.Item.DataItem, "ResourceLink").ToString() + @""" target=""_blank"">" + DataBinder.Eval(e.Item.DataItem, "ResourceLink").ToString() + "</a>";

        }
        else
        {
            //sResName = DataBinder.Eval(e.Item.DataItem, "ResourceName").ToString();
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            oLit = (Literal)e.Item.FindControl("litResourceName");
            oLit.Text = sResName;
        }
    }
    protected void repBlogs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        
        Literal oLit;
        string sResName = "";
        if (DataBinder.Eval(e.Item.DataItem, "Title") != null)
        {
            sResName =   DataBinder.Eval(e.Item.DataItem, "Title").ToString() ;
            oLit = (Literal)e.Item.FindControl("LiteralBlogTitle");
            oLit.Text = sResName;
        }
        if (DataBinder.Eval(e.Item.DataItem, "BlogDate") != null)
        {
            sResName =   DataBinder.Eval(e.Item.DataItem, "BlogDate").ToString() ;
            oLit = (Literal)e.Item.FindControl("LiteralBlogDate");
            oLit.Text = sResName;
            oLit.Text = String.Format("{0:MMMM d, yyyy}", DataBinder.Eval(e.Item.DataItem, "BlogDate"));
        }
        if (DataBinder.Eval(e.Item.DataItem, "Blog") != null)
        {
            sResName =   DataBinder.Eval(e.Item.DataItem, "Blog").ToString() ;
            oLit = (Literal)e.Item.FindControl("LiteralBlog");
            oLit.Text = sResName;
        }
        if (DataBinder.Eval(e.Item.DataItem, "blogImage") != null)
        {
            sResName =   DataBinder.Eval(e.Item.DataItem, "blogImage").ToString() ;
            oLit = (Literal)e.Item.FindControl("LiteralImage");
            oLit.Text = @"<img src=""" + @"imagesBlog/" + sResName + @""" title="""">";  
        }  
         
 
    }
    protected void repBlogs_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Show":
                Response.Redirect("STEMBlogCommentsList.aspx?lngBlogID=" + e.CommandArgument);
                break;
            case "Post":
                Response.Redirect("STEMBlogCommentsAdd.aspx?lngBlogID=" + e.CommandArgument);
                break;
        }

    }
}