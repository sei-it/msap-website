﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="STEMnewsdetail.aspx.cs" Inherits="STEM_STEMnewsdetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    MSAP Center - STEM News
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
            <div class="mainContent">
                            <a href="stemhome.aspx">STEM  Beacon</a> | <a href="STEMBlogDL.aspx">STEM  Blog</a> | <a href="STEMNews.aspx">STEM  News</a> | <a href="STEMResources.aspx">STEM  Resources</a>  

               <div class="STEMContent"> 
                <h1>STEM News</h1>
                <b><asp:Literal ID="litNewsTitle" runat="server"></asp:Literal></b>
                    <a name="skip"></a>
                <div class="msapHP_Content">
                    <asp:Literal ID="litNewsDeatil" runat="server"></asp:Literal>
                    <div runat="server" id="videoContent">
                    </div>
                </div>
            </div>
                </div>
</asp:Content>

