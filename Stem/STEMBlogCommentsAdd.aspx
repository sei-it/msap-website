﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="STEMBlogCommentsAdd.aspx.cs" Inherits="STEM_STEMBlogCommentsAdd" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="mainContent">
            <div class="STEMContent">
            <h1>STEM Blog Comments</h1>
            <h2>
                <asp:Literal ID="LiteralBlogTitle" runat="server"></asp:Literal>
            </h2>
            <h4><asp:Literal ID="LiteralBlogDate" runat="server"></asp:Literal></h4>
            <div>
 
              <asp:Literal ID="LiteralBlog" runat="server"></asp:Literal>
          
            </div>
 
	                    <p>
		                    <label for="txtcommentBy">Name</label><br />
		                    <asp:textbox id="txtcommentBy" runat="server" Width="300px" ></asp:textbox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="txtcommentBy"
                            ErrorMessage="A contact name is required."></asp:RequiredFieldValidator>
	                    </p>
                
	                	<p>
		                    <label for="txtBlogComment">Comment</label><br />
                            <asp:textbox id="txtBlogComment" runat="server" TextMode="MultiLine" Height="157px" Width="449px"  ></asp:textbox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="Red" ControlToValidate="txtBlogComment"
                            ErrorMessage="A comment is required."></asp:RequiredFieldValidator>
	                    </p>


                         <p>
                            <asp:Button ID="btnSave" class="msapBtn" runat="server" Text="Submit Comments" onclick="btnSave_Click" />&nbsp;<asp:Button 
                                ID="btnClose" runat="server" class="msapBtn" CausesValidation="false" Text="Close" onclick="btnClose_Click" Visible="true" />&nbsp;&nbsp;&nbsp;
          
                        </p>
                 <p>
                                                 
                 </p>
 
        </div>
            </div>
</asp:Content>

