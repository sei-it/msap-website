﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="STEMNewsList.aspx.cs" Inherits="STEM_admin_STEMNewsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
            <div class="mainContent">
                <div class="STEMContent">
        <h1>STEM News List</h1>
        <p>
                <asp:Button ID="btnNew" class="msapBtn" runat="server"   Text="New STEM News Record" OnClick="btnNew_Click" />
        </p>
 
    
         <asp:GridView ID="grdVwList" runat="server"  CssClass="msapTbl"  
            AutoGenerateColumns="False"   
            AllowSorting="false" OnRowCommand="grdVwList_RowCommand"     >
            <Columns>
 
		            <asp:BoundField DataField="NewsID" Visible="false" HeaderText="ID" DataFormatString="{0:g}" >
		            <HeaderStyle Width="75px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="Title" HeaderText="Title" DataFormatString="{0:g}" >
                        	<HeaderStyle Width="250px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:CheckBoxField DataField="Display" HeaderText="Display"   >
                        		            
		            </asp:CheckBoxField>
 		            <asp:BoundField DataField="Source" HeaderText="Source" DataFormatString="{0:g}" >
                        	<HeaderStyle Width="150px"></HeaderStyle>
		            </asp:BoundField>
 		            <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:g}" >
                        	<HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
 
                     <asp:TemplateField>
                       <HeaderTemplate>  
                         Edit                     
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" visible=true  CommandArgument='<%# Eval("NewsID") %>'>Edit</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField>  
 
 
            </Columns>
        </asp:GridView>

    </div>
                </div>
</asp:Content>

