﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="STEMResourcesEdit.aspx.cs" Inherits="STEM_admin_STEMResourcesEdit" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="mainContent">
            <div class="STEMContent">
            <h1>STEM Resource Edit</h1>
             <div class="formLayout">
 	                <p>
		                <label for="txtResourceName">Resource Name</label>
		                <asp:textbox id="txtResourceName" runat="server" Width="500px" ></asp:textbox>
	                </p>
	                <p>
		                <label for="txtResourceLink">Link (url)</label>
		                <asp:textbox id="txtResourceLink" runat="server" Width="500px" ></asp:textbox>
	                </p>
	                <p>
		                <label for="txtResourceDescription">Description</label><br />
                         <CKEditor:CKEditorControl ID="txtResourceDescription" runat="server" Width="800px">
                        </CKEditor:CKEditorControl> 
	                </p>

	                <p>
		                <label for="rblshowInHomePage">Show In Home Page</label>
		                <asp:RadioButtonList id="rblshowInHomePage" runat="server"  RepeatDirection="Horizontal"  > 
		                <asp:ListItem Value="1">Yes</asp:ListItem>
		                <asp:ListItem Value="0">No</asp:ListItem>
		                </asp:RadioButtonList>
	                </p>

                         <p>
                            <asp:Button ID="btnSave" class="msapBtn" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;<asp:Button 
                                ID="btnClose" class="msapBtn" runat="server" Text="Close" onclick="btnClose_Click" Visible="true" />&nbsp;&nbsp;&nbsp;
          
                        </p>
                 <p>
                                                 
                 </p>
             </div >
        </div>
            </div>
</asp:Content>
 

