﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="STEMBlogEdit.aspx.cs" Inherits="STEM_admin_STEMBlogEdit" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style>

    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="mainContent">
            <div class="STEMContent">
            <h1>STEM Blogs Edit</h1>
             <div class="formLayout">
 	                <p>
		                <label for="txtTitle">Title</label>
		                <asp:textbox id="txtTitle" runat="server" Width="500px" ></asp:textbox>
	                </p>
	                <p>
		                <label for="txtBlog">Blog Content</label><br />

                        <CKEditor:CKEditorControl ID="txtBlog" runat="server" Width="800px">
                        </CKEditor:CKEditorControl> 
	                </p>
	                <p>
		                <label for="txtBlogPreview">Blog Preview</label><br />
                        <CKEditor:CKEditorControl ID="txtBlogPreview" runat="server" Width="800px">
                        </CKEditor:CKEditorControl> 
 
	                </p>

                 

	                <p>
		                <label for="txtBlogDate">Blog Date</label>
		                <asp:textbox id="txtBlogDate" runat="server" ></asp:textbox>
  	                    
  	                </p>
	                <p>
		                <label for="rblShowInMainYN">Show In Main Page?</label>
		                <asp:RadioButtonList id="rblShowInMainYN" runat="server"  RepeatDirection="Horizontal" RepeatLayout="Table"  > 
		                <asp:ListItem Value="1">Yes</asp:ListItem>
		                <asp:ListItem Value="0">No</asp:ListItem>
		                </asp:RadioButtonList>
	                </p>
 
	                <p>
		                <label for="txtshowOrder">Show Order</label>
		                <asp:textbox id="txtshowOrder" runat="server" ></asp:textbox>
	                </p>


	                <p>
		                <label for="FileUploadImage">Image File</label>
                        <asp:FileUpload ID="FileUploadImage" runat="server" />
                        <asp:Button ID="btnUploadFile" runat="server" Text="Upload" OnClick="btnUploadFile_Click" />
                        <asp:Literal ID="UploadStatusLabel" runat="server"></asp:Literal>
	                </p>
	                <p>
		                <label for="FileUploadImagePreview">Preview  Image File</label>
                        <asp:FileUpload ID="FileUploadImagePreview" runat="server" />
                        <asp:Button ID="btnUploadFileImage" runat="server" Text="Upload" OnClick="btnUploadFileImage_Click" />
                        <asp:Literal ID="UploadStatusLabel2" runat="server"></asp:Literal>
	                </p>

                    <p>
		                &nbsp;</p>

                         <p>
                            <asp:Button ID="btnSave" class="msapBtn" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;<asp:Button 
                                ID="btnClose" class="msapBtn" runat="server" Text="Close" onclick="btnClose_Click" Visible="true" />&nbsp;&nbsp;&nbsp;
          
                        </p>
                 <p>
                                                 
                 </p>
             </div >
        </div>
            </div>
</asp:Content>

