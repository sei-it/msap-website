﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="STEMResourcesList.aspx.cs" Inherits="STEM_admin_STEMResourcesList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="mainContent">
        <div class="STEMContent">
        <h1>Resource List</h1>
        <p>
                <asp:Button ID="btnNew" class="msapBtn" runat="server"   Text="New STEM Resource Record" OnClick="btnNew_Click" />
        </p>

    
                   
    
         <asp:GridView ID="grdVwList" runat="server"   CssClass="msapTbl" 
            AutoGenerateColumns="False"   
            AllowSorting="false" OnRowCommand="grdVwList_RowCommand"     >
            <Columns>
 
		            <asp:BoundField DataField="STEMResourceID" HeaderText="ID" DataFormatString="{0:g}" >
		            <HeaderStyle Width="75px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="ResourceName" HeaderText="Resource Name" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="300px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="ResourceLink" HeaderText="Resource Link" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="300px"></HeaderStyle>
		            </asp:BoundField>
 

 
                     <asp:TemplateField>
                       <HeaderTemplate>  
                         Edit                     
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" visible=true  CommandArgument='<%# Eval("STEMResourceID") %>'>Edit</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="75px" />
                        </asp:TemplateField>  
 
 
            </Columns>
        </asp:GridView>

    </div>
 </div>
</asp:Content>



