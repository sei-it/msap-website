﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="STEMBlogCommentsList.aspx.cs" Inherits="STEM_admin_STEMBlogCommentsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

            <div class="mainContent">
                <div class="STEMContent"> 
        <h1>STEM Blog Comments </h1>
     <h2>
        <asp:Literal ID="LiteralBlogTitle" runat="server"></asp:Literal>

    </h2>
        <h4><asp:Literal ID="LiteralBlogDate" runat="server"></asp:Literal></h4>
         
                  <div runat="server" id="divResults">
                    <div style="text-align:right">
                    <asp:DropDownList    ID="ddlStatuschange" runat="server" EnableViewState="true" Visible="true">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="1">Approve</asp:ListItem>
                        <asp:ListItem Value="0">Reject</asp:ListItem>
                        </asp:DropDownList>
                    <asp:Button ID="btnChangeStatus" class="msapBtn" runat="server" Text="Change Status" Visible="true" OnClick="btnChangeStatus_Click" />
                </div>  
         <asp:GridView ID="grdVwList" runat="server" CssClass="msapTbl"  
            AutoGenerateColumns="False"   
            AllowSorting="false"     >
            <Columns>
 
		            <asp:BoundField DataField="BlogCommentID" HeaderText="ID" DataFormatString="{0:g}" >
		            <HeaderStyle Width="75px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="commentBy" HeaderText="Title" DataFormatString="{0:g}" >
                        	<HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
  		            <asp:BoundField DataField="commentDate" HeaderText="Comment Date" DataFormatString="{0:g}" >
                        	<HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
                    <asp:CheckBoxField DataField="rejectCommentYN" HeaderText="Approved?" />
 		            <asp:BoundField DataField="BlogComment" HeaderText="Comment" DataFormatString="{0:g}" >
                        	<HeaderStyle Width="300px"></HeaderStyle>
		            </asp:BoundField>
                     <asp:TemplateField itemStyle-Width = "75px" HeaderStyle-Width ="75px" Visible="true"><HeaderTemplate>Actions</HeaderTemplate>
                        <ItemTemplate >  
                                <asp:CheckBox   ID="chkChangeStatus" runat="server"  />
                            <asp:HiddenField ID="ohiddBlogCommentID" Value=<%# Eval("BlogCommentID")%> runat="server" /> 
                        </ItemTemplate>            
                    </asp:TemplateField>
 
 
 
            </Columns>
        </asp:GridView>
        <div style="text-align:right">
            <asp:DropDownList    ID="ddlStatuschange1" runat="server" EnableViewState="true" Visible="true"> 
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="1">Approve</asp:ListItem>
                        <asp:ListItem Value="0">Reject</asp:ListItem>

            </asp:DropDownList>
            <asp:Button ID="btnChangeStatus1" class="msapBtn" runat="server" Text="Change Status" Visible="true" OnClick="btnChangeStatus1_Click" />
        </div>


    </div>
</div>
</div>
</asp:Content>

