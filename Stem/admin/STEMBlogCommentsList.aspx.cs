﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_admin_STEMBlogCommentsList : System.Web.UI.Page
{
    int intBlogID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            intBlogID = Convert.ToInt32(Request.QueryString["id"]);

        }
        if (!IsPostBack)
        {
            displayBlog();
        }
    }

    private void displayBlog()
    {
        STEMDataContext db = new STEMDataContext();
        var oPages = from pg in db.STEMBlogs
                     where pg.BlogID == Convert.ToInt32(Request.QueryString["id"])
                     select pg;
        foreach (var news in oPages)
        {
            LiteralBlogTitle.Text = news.Title;
             LiteralBlogDate.Text = String.Format("{0:MMMM d, yyyy}", news.BlogDate);

        }
        displayComments();
    }
    private void displayComments()
    {
        object objVal = null;
        using (STEMDataContext db = new STEMDataContext())
        {
            var vCases = db.get_STEMBlogAllComments(intBlogID);
            //HttpContext.Current.User.Identity.Name
            grdVwList.DataSource = vCases;
            grdVwList.DataBind();
        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["intBlogID"] != null)))
        {
            intBlogID = Convert.ToInt32(this.ViewState["intBlogID"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["intBlogID"] = intBlogID;
        return (base.SaveViewState());
    }


    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        changeStatus();
        displayBlog();
    }
    private void changeStatus()
    {
	GridViewRow itm = default(GridViewRow);
	Boolean blStatusID = false;
	if ((ddlStatuschange.SelectedItem != null)) {
		if (!(ddlStatuschange.SelectedItem.Text == "")) {

            if (ddlStatuschange.SelectedValue == "1")
            {
                blStatusID = true ;
            }
            else
            {
                blStatusID = false;

            }
            foreach (GridViewRow itm2 in grdVwList.Rows)
            {
                changeItemStatusX(itm2, blStatusID);
			}
		}
	}
}


    private void changeItemStatusX(GridViewRow di, Boolean  blStatusID)
    {
        CheckBox oCheckBox = default(CheckBox);
 
        HiddenField ohiddBlogCommentID = default(HiddenField);
        oCheckBox = (CheckBox)di.FindControl("chkChangeStatus");

        ohiddBlogCommentID = (HiddenField)di.FindControl("ohiddBlogCommentID");
        if (oCheckBox.Checked == true)
        {
            updateSession(Convert.ToInt32(ohiddBlogCommentID.Value), blStatusID);
        }
    }

private void updateSession(Int32 intBlogCommentID, Boolean blStatus)
{
	bool blNew = false;
	int strTmp = 0;
	DateTime dtTmp = default(DateTime);
	using (STEMDataContext db = new STEMDataContext()) {

		STEMBlogComment oRegSession = (from c in db.STEMBlogComments where c.BlogCommentID == intBlogCommentID  select c ).FirstOrDefault();
		if ((oRegSession == null)) {
			oRegSession = new STEMBlogComment();
			blNew = true;
		}
		oRegSession.rejectCommentYN = blStatus;
		//oRegSession.UpdatedBy = HttpContext.Current.User.Identity.Name;
		//oRegSession.UpdatedDate = DateAndTime.Now.Date;

		if (blNew == true) {
			//This should never happen, if it does then needs to be chacked
			oRegSession.BlogID_Fk = 0;

			db.STEMBlogComments.InsertOnSubmit(oRegSession);
		}

		db.SubmitChanges();


	}
}

protected void btnChangeStatus1_Click(object sender, EventArgs e)
{
    changeStatus1();
    displayBlog();
}
private void changeStatus1()
{
    GridViewRow itm = default(GridViewRow);
    Boolean blStatusID = false;
    if ((ddlStatuschange1.SelectedItem != null))
    {
        if (!(ddlStatuschange1.SelectedItem.Text == ""))
        {

            if (ddlStatuschange1.SelectedValue == "1")
            {
                blStatusID = true;
            }
            else
            {
                blStatusID = false;

            }
            foreach (GridViewRow itm2 in grdVwList.Rows)
            {
                changeItemStatusX(itm2, blStatusID);
            }
        }
    }
}
}