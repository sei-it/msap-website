﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_admin_STEMBlogEdit : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;


        if (ViewState["IsLoaded1"] == null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
        }
 
        if (ViewState["IsLoaded1"] == null)
        {
 
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

    }

 

    protected void displayRecords()
    {
        object objVal = null;
        STEMDataContext db = new STEMDataContext();
        var oPages = from pg in db.STEMBlogs
                     where pg.BlogID == lngPkID
                     select pg;
        foreach (var oBlog in oPages)
        {


            objVal = oBlog.Title;
            if (objVal != null)
            {
                txtTitle.Text = objVal.ToString();
            }
            objVal = oBlog.Blog;
            if (objVal != null)
            {
                txtBlog.Text = objVal.ToString();
            }
            objVal = oBlog.BlogPreview;
            if (objVal != null)
            {
                txtBlogPreview.Text = objVal.ToString();
            }
            objVal = oBlog.BlogDate;
            if (objVal != null)
            {
                txtBlogDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            }
            objVal = oBlog.ShowInMainYN;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true) { rblShowInMainYN.Items[0].Selected = true; } else if (Convert.ToBoolean(objVal) == false) { rblShowInMainYN.Items[1].Selected = true; }
            }
 
            objVal = oBlog.showOrder;
            if (objVal != null)
            {
                txtshowOrder.Text = objVal.ToString();
            }




        }

        db.Dispose();
 
    }

 

    public void updateBlogs()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        STEMDataContext db = new STEMDataContext();
        STEMBlog oBlog = (from c in db.STEMBlogs where c.BlogID == lngPkID select c).FirstOrDefault();

        if ((oBlog == null))
        {
            oBlog = new STEMBlog();
            blNew = true;
        }



        oBlog.Title = txtTitle.Text;
        oBlog.Blog = txtBlog.Text;
        oBlog.BlogPreview = txtBlogPreview.Text;
 
        DateTime.TryParse(txtBlogDate.Text, out dtTmp);
        if (txtBlogDate.Text != "")
        {
            oBlog.BlogDate = dtTmp;
        }
        else
        {
            oBlog.BlogDate = null;
        }

        if (rblShowInMainYN.SelectedValue == "1")
        {
            oBlog.ShowInMainYN = true;
        }
        else if (rblShowInMainYN.SelectedValue == "0")
        {
            oBlog.ShowInMainYN = false;
        }
        int intTmp;
        int.TryParse(txtshowOrder.Text, out intTmp);

        if (txtshowOrder.Text != "")
        {
            oBlog.showOrder = intTmp;
        }
        else
        {
            oBlog.showOrder = null;
        }
        //--
        if (FileUploadImagePreview.HasFile)
        {
            oBlog.blogImagePreview = Path.GetFileName(FileUploadImagePreview.FileName);
        }
        if (FileUploadImage.HasFile)
        {
            oBlog.blogImage = Path.GetFileName(FileUploadImage.FileName);
        }

        //////////////////////
        if (blNew == true)
        {

            oBlog.CreatedBy = HttpContext.Current.User.Identity.Name;
            oBlog.createdDate = DateTime.Now;
            db.STEMBlogs.InsertOnSubmit(oBlog);
        }
        else
        {
            oBlog.updatedBy = HttpContext.Current.User.Identity.Name;
            oBlog.updatedDate = DateTime.Now;
        }

        db.SubmitChanges();
        lngPkID = oBlog.BlogID;
        db.Dispose();
 

    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {


        updateBlogs();
        displayRecords();

        Response.Redirect("STEMBlogList.aspx?ID=" + lngPkID);

    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
  
        Response.Redirect("STEMBlogList.aspx");
    }



    protected void btnUploadFile_Click(object sender, EventArgs e)
    {
        if (FileUploadImage.HasFile)
        {
            try
            {
                if (FileUploadImage.PostedFile.ContentType == "image/jpeg")
                {
                    if (FileUploadImage.PostedFile.ContentLength < 102400)
                    {
                        string filename = Path.GetFileName(FileUploadImage.FileName);
                        //FileUploadImage.SaveAs(Server.MapPath("~/") + filename);
                        FileUploadImage.SaveAs(WebConfigurationManager.AppSettings["BlogImagePath"] + filename);

                        UploadStatusLabel.Text = "Upload status: File uploaded!";
                        updateBlogs();
                    }
                    else
                        UploadStatusLabel.Text = "Upload status: The file has to be less than 100 kb!";
                }
                else
                    UploadStatusLabel.Text = "Upload status: Only JPEG files are accepted!";
            }
            catch (Exception ex)
            {
                UploadStatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
            }
        }

    }
    protected void btnUploadFileImage_Click(object sender, EventArgs e)
    {
        if (FileUploadImagePreview.HasFile)
        {
            try
            {
                if (FileUploadImagePreview.PostedFile.ContentType == "image/jpeg")
                {
                    if (FileUploadImagePreview.PostedFile.ContentLength < 102400)
                    {
                        string filename = Path.GetFileName(FileUploadImagePreview.FileName);
                        //FileUploadImage.SaveAs(Server.MapPath("~/") + filename);
                        FileUploadImagePreview.SaveAs(WebConfigurationManager.AppSettings["BlogImagePath"] + filename);

                        UploadStatusLabel2.Text = "Upload status: File uploaded!";
                        updateBlogs();
                    }
                    else
                        UploadStatusLabel2.Text = "Upload status: The file has to be less than 100 kb!";
                }
                else
                    UploadStatusLabel2.Text = "Upload status: Only JPEG files are accepted!";
            }
            catch (Exception ex)
            {
                UploadStatusLabel2.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
            }
        }

    }
}