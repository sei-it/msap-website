﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_admin_STEMNewsEdit : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;


        if (ViewState["IsLoaded1"] == null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
        }

        if (ViewState["IsLoaded1"] == null)
        {

            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

    }



    protected void displayRecords()
    {
        object objVal = null;
        STEMDataContext db = new STEMDataContext();
        var oPages = from pg in db.STEMNews
                     where pg.NewsID  == lngPkID
                     select pg;
        foreach (var oNews in oPages)
        {


            objVal = oNews.Title;
            if (objVal != null)
            {
                txtTitle.Text = objVal.ToString();
            }
            objVal = oNews.Abstract;
            if (objVal != null)
            {
                txtAbstract.Text = objVal.ToString();
            }
            objVal = oNews.NewsContent;
            if (objVal != null)
            {
                txtNewsContent.Text = objVal.ToString();
            }
            objVal = oNews.Display;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true) { rblDisplay.Items[0].Selected = true; } else if (Convert.ToBoolean(objVal) == false) { rblDisplay.Items[1].Selected = true; }
            }
            objVal = oNews.homePageYN;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true) { rblhomePageYN.Items[0].Selected = true; } else if (Convert.ToBoolean(objVal) == false) { rblhomePageYN.Items[1].Selected = true; }
            }
 
            objVal = oNews.Source;
            if (objVal != null)
            {
                txtSource.Text = objVal.ToString();
            }
            objVal = oNews.Author;
            if (objVal != null)
            {
                txtAuthor.Text = objVal.ToString();
            }
            objVal = oNews.Date;
            if (objVal != null)
            {
                txtDate.Text = objVal.ToString();
            }



        }

        db.Dispose();

    }



    public void updateBlogs()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        STEMDataContext db = new STEMDataContext();
        STEMNew oNews = (from c in db.STEMNews where c.NewsID == lngPkID select c).FirstOrDefault();

        if ((oNews == null))
        {
            oNews = new STEMNew();
            blNew = true;
        }



        oNews.Title = txtTitle.Text;
        oNews.Abstract = txtAbstract.Text;
        oNews.NewsContent = txtNewsContent.Text;
        if (rblDisplay.SelectedValue == "1")
        {
            oNews.Display = true;
        }
        else if (rblDisplay.SelectedValue == "0")
        {
            oNews.Display = false;
        }
        if (rblhomePageYN.SelectedValue == "1")
        {
            oNews.homePageYN = true;
        }
        else if (rblhomePageYN.SelectedValue == "0")
        {
            oNews.homePageYN = false;
        } 
        oNews.Source = txtSource.Text;
        oNews.Author = txtAuthor.Text;
        oNews.Date = txtDate.Text;


        //////////////////////
        if (blNew == true)
        {

            oNews.createdBy = HttpContext.Current.User.Identity.Name;
            oNews.CreatedDate = DateTime.Now;
            db.STEMNews.InsertOnSubmit(oNews);
        }
        else
        {
            oNews.updatedBy = HttpContext.Current.User.Identity.Name;
            oNews.updatedOn = DateTime.Now;
        }

        db.SubmitChanges();
        lngPkID = oNews.NewsID;
        db.Dispose();


    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {


        updateBlogs();
        displayRecords();

        Response.Redirect("STEMNewsList.aspx?ID=" + lngPkID);

    }

    protected void btnClose_Click(object sender, EventArgs e)
    {

        Response.Redirect("STEMNewsList.aspx");
    }


 
}