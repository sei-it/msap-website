﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_admin_STEMNewsList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            displayRecords();
        }

    }

    private void displayRecords()
    {
        object objVal = null;
        using (STEMDataContext db = new STEMDataContext())
        {

            var vCases = db.get_STEMNewsListALL();
            //HttpContext.Current.User.Identity.Name

            grdVwList.DataSource = vCases;
            grdVwList.DataBind();


        }

    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Begin")
        {
            int intCaseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("STEMNewsEdit.aspx?lngPkID=" + intCaseID);
        }

    }
    protected void btnNew_Click(object sender, EventArgs e)
    {

        Response.Redirect("STEMNewsEdit.aspx");
    }
}