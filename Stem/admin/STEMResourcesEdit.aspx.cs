﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_admin_STEMResourcesEdit : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;


        if (ViewState["IsLoaded1"] == null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
        }

        if (ViewState["IsLoaded1"] == null)
        {

            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

    }



    protected void displayRecords()
    {
        object objVal = null;
        STEMDataContext db = new STEMDataContext();
        var oPages = from pg in db.STEMResources
                     where pg.STEMResourceID == lngPkID
                     select pg;
        foreach (var oResource in oPages)
        {


            objVal = oResource.ResourceName;
            if (objVal != null)
            {
                txtResourceName.Text = objVal.ToString();
            }
            objVal = oResource.ResourceLink;
            if (objVal != null)
            {
                txtResourceLink.Text = objVal.ToString();
            }
            objVal = oResource.ResourceDescription;
            if (objVal != null)
            {
                txtResourceDescription.Text = objVal.ToString();
            }
 
            objVal = oResource.showInHomePage;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true) { rblshowInHomePage.Items[0].Selected = true; } else if (Convert.ToBoolean(objVal) == false) { rblshowInHomePage.Items[1].Selected = true; }
            }
             



    

        }

        db.Dispose();

    }



    public void updateResource()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        STEMDataContext db = new STEMDataContext();
        STEMResource oResource = (from c in db.STEMResources where c.STEMResourceID  == lngPkID select c).FirstOrDefault();

        if ((oResource == null))
        {
            oResource = new STEMResource();
            blNew = true;
        }


        oResource.ResourceName = txtResourceName.Text;
        oResource.ResourceLink = txtResourceLink.Text;
        oResource.ResourceDescription = txtResourceDescription.Text;
 
        if (rblshowInHomePage.SelectedValue == "1")
        {
            oResource.showInHomePage = true;
        }
        else if (rblshowInHomePage.SelectedValue == "0")
        {
            oResource.showInHomePage = false;
        }


        //////////////////////
        if (blNew == true)
        {

            oResource.createdBy  = HttpContext.Current.User.Identity.Name;
            oResource.cretatedOn = DateTime.Now;
            db.STEMResources.InsertOnSubmit(oResource);
        }
        else
        {
            oResource.updatedBy = HttpContext.Current.User.Identity.Name;
            oResource.updatedOn = DateTime.Now;
        }

        db.SubmitChanges();
        lngPkID = oResource.STEMResourceID;
        db.Dispose();


    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {


        updateResource();
        displayRecords();

        Response.Redirect("STEMResourcesList.aspx?ID=" + lngPkID);

    }

    protected void btnClose_Click(object sender, EventArgs e)
    {

        Response.Redirect("STEMResourcesList.aspx");
    }


 
}