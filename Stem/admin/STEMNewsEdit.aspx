﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="STEMNewsEdit.aspx.cs" Inherits="STEM_admin_STEMNewsEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"  TagPrefix="cc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
<link href="../../css/calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="mainContent">
            <div class="STEMContent">
            <h1>STEM News Edit</h1>
             <div class="formLayout">
	                <p>
		                <label for="txtTitle">Title</label>
		                <asp:textbox id="txtTitle" runat="server" Width="390px" ></asp:textbox>
	                </p>
	                <p>
		                <label for="txtSource">Source</label>
		                <asp:textbox id="txtSource" runat="server" Width="370px" ></asp:textbox>
	                </p>
	                <p>
		                <label for="txtAuthor">Author</label>
		                <asp:textbox id="txtAuthor" runat="server" Width="375px" ></asp:textbox>
	                </p>
	                <p>
		                <label for="txtDate">Date</label>
		                <asp:textbox id="txtDate" runat="server" Width="153px" ></asp:textbox>
                        <ajaxToolkit:CalendarExtender ID="cldextDate" runat="server" Format="MMMM dd, yyyy" TargetControlID="txtDate"  CssClass="AjaxCalendar" />
	                </p>
                 
	                <p>
		                <label for="rblhomePageYN">Show in Home Page?</label>
		                <asp:RadioButtonList id="rblhomePageYN" runat="server" RepeatDirection="Horizontal"  > 
		                <asp:ListItem Value="1">Yes</asp:ListItem>
		                <asp:ListItem Value="0">No</asp:ListItem>
		                </asp:RadioButtonList>
	                </p> 

	                <p>
		                <label for="txtAbstract">Abstract</label>
                        <CKEditor:CKEditorControl ID="txtAbstract" runat="server" Width="800px">
                        </CKEditor:CKEditorControl> 
	                </p>
	                <p>
		                <label for="txtNewsContent">News Content</label>
                        <CKEditor:CKEditorControl ID="txtNewsContent" runat="server" Width="800px">
                        </CKEditor:CKEditorControl> 
	                </p>
	                <p>
		                <label for="rblDisplay">Display</label>
		                <asp:RadioButtonList id="rblDisplay" runat="server" Visible="true" RepeatDirection="Horizontal"  > 
		                <asp:ListItem Value="1">Yes</asp:ListItem>
		                <asp:ListItem Value="0">No</asp:ListItem>
		                </asp:RadioButtonList>
	                </p>



 
                         <p>
                            <asp:Button ID="btnSave" class="msapBtn" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;<asp:Button 
                                ID="btnClose" class="msapBtn" runat="server" Text="Close" onclick="btnClose_Click" Visible="true" />&nbsp;&nbsp;&nbsp;
          
                        </p>
                 <p>
                                                 
                 </p>
             </div >
        </div>
            </div>
</asp:Content>

