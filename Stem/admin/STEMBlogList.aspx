﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="STEMBlogList.aspx.cs" Inherits="STEM_admin_STEMBlogList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="mainContent">
        <div class="STEMContent">
        <h1>STEM Blog List</h1>
        <p>
                <asp:Button ID="btnNew" class="msapBtn" runat="server"   Text="New STEM Blog Record" OnClick="btnNew_Click" />
        </p>


    
                   
    
         <asp:GridView ID="grdVwList" runat="server"  CssClass="msapTbl"  
            AutoGenerateColumns="False"   
            AllowSorting="false" OnRowCommand="grdVwList_RowCommand"     >
            <Columns>
 
		            <asp:BoundField DataField="BlogID" HeaderText="ID" Visible="false" DataFormatString="{0:g}" >
		            <HeaderStyle Width="50px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="Title" HeaderText="Title" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="300px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="BlogDate" HeaderText="Blog Date" DataFormatString="{0:d}" >
                        		            <HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:CheckBoxField DataField="ShowInMainYN" HeaderText="Main Page"  >                        		             
		            </asp:CheckBoxField> 

                	<asp:BoundField DataField="showOrder" HeaderText="Show Order" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="75px"></HeaderStyle>
		            </asp:BoundField>
                     <asp:TemplateField>
                       <HeaderTemplate>  
                         Edit                     
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" visible=true  CommandArgument='<%# Eval("BlogID") %>'>Edit</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="75px" />
                        </asp:TemplateField>  
                     <asp:TemplateField>
                       <HeaderTemplate>  
                         Comments                     
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnComments" runat="server" CommandName="Comments" visible=true  CommandArgument='<%# Eval("BlogID") %>'>Comments</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="75px" />
                        </asp:TemplateField>  
 
            </Columns>
        </asp:GridView>

    </div>
 </div>
</asp:Content>

