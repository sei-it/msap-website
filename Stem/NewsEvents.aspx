﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="NewsEvents.aspx.cs" Inherits="Stem_NewsEvents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="mainContent">
<div class="events-hl">
           <h1>News / Events</h1>
                 <ul>
                 <li><a href="http://www.detroitnews.com/story/news/local/metro-detroit/2014/10/23/detroit-stem-conference-may-open-doors-women/17759967/" title="" target="_blank">Detroit STEM conference may open doors for women </a></li>
                 
                 <li><a href="http://www.4-traders.com/news/Bosch-Awards-50000-for-Four-Grants-in-Broward-and-Miami-Dade-Counties--19247914/" title="" target="_blank">Bosch awards $50,000 for four grants in Broward and Miami-Dade Counties </a></li>
               
                     <li><a href="http://www.deseretnews.com/article/865613656/Special-assembly-held-to-promote-STEM-education.html" title="" target="_blank">Special assembly held to promote STEM education</a></li>
                     <li><a href="http://www.businesswire.com/news/home/20141021006362/en/BioUtah-Partners-Utah-STEM-Action-Center-Provide#.VElet1ewV50" title="" target="_blank">BioUtah Partners with Utah STEM Action Center to Provide STEM Student Scholarships, Special Tickets to Utah Life Science Summit</a></li>
                    
                    
                 </ul> 
                           
        </div>
        <div></div><br/>
            </div>
</asp:Content>

