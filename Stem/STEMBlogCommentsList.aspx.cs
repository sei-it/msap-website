﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_STEMBlogCommentsList : System.Web.UI.Page
{
    int lngPkID;
    int intBlogID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["lngBlogID"] != null)
            {
                intBlogID = Convert.ToInt32(Page.Request.QueryString["lngBlogID"]);

            }

            displayComments();
            displayBlog();
        }
    }

    private void displayComments()
    {
        object objVal = null;
        LiteralCommentesYesNo.Text = "";
        using (STEMDataContext db = new STEMDataContext())
        {
            var vCases = db.get_STEMBlogComments(intBlogID);
            //HttpContext.Current.User.Identity.Name
            repComments.DataSource = vCases;
            repComments.DataBind();
            if (repComments.Items.Count == 0)
            {LiteralCommentesYesNo.Text = "There are no comments.";
            }
        }
    }
    private void displayBlog()
    {
        STEMDataContext db = new STEMDataContext();
        var oPages = from pg in db.STEMBlogs
                     where pg.BlogID == intBlogID
                     select pg;
        foreach (var news in oPages)
        {
            LiteralBlogTitle.Text = news.Title;
            // LiteralBlog.Text = news.Blog;
           // LiteralBlogDate.Text = String.Format("{0:MMMM d, yyyy}", news.BlogDate);
        }

    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intBlogID"] != null)))
        {
            intBlogID = Convert.ToInt32(this.ViewState["intBlogID"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intBlogID"] = intBlogID;
        return (base.SaveViewState());
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("STEMBlogDL.aspx");
    }
}