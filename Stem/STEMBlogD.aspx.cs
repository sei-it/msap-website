﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_STEMBlogD : System.Web.UI.Page
{
    int intBlogID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            intBlogID = Convert.ToInt32(Request.QueryString["id"]);
            displayBlog();


        }
    }
    private void displayBlog()
    {
            STEMDataContext db = new STEMDataContext();
            var oPages = from pg in db.STEMBlogs
                         where pg.BlogID == Convert.ToInt32(Request.QueryString["id"])
                         select pg;
            foreach (var news in oPages)
            {
                LiteralBlogTitle.Text = news.Title;
                LiteralBlog.Text = news.Blog;
                LiteralBlogDate.Text = String.Format("{0:MMMM d, yyyy}", news.BlogDate);
                if (news.blogImage  !=null)
                {
                    LiteralImage.Text = @"<img src=""" + @"imagesBlog/" + news.blogImage + @""" title="""">";
                }
 
            }
            displayComments();
    }
    private void displayComments()
    {
        object objVal = null;
        using (STEMDataContext db = new STEMDataContext())
        {
            var vCases = db.get_STEMBlogComments(intBlogID);
            //HttpContext.Current.User.Identity.Name
            repComments.DataSource = vCases;
            repComments.DataBind();
        }
    }


    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["intBlogID"] != null)))
        {
            intBlogID = Convert.ToInt32(this.ViewState["intBlogID"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["intBlogID"] = intBlogID;
        return (base.SaveViewState());
    }


}