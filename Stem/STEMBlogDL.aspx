﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="STEMBlogDL.aspx.cs" Inherits="STEM_STEMBlogDL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">


<script type = "text/javascript">

    function SetTarget() {

        document.forms[0].target = "_blank";

    }

</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <a name="skip"></a>
          <img src="./images/stem-blog.jpg" width="842px" style="margin:0px;"/>
        <div class="mainContent">
                        <a href="stemhome.aspx">STEM  Beacon</a> | <a href="STEMNews.aspx">STEM  News</a> | <a href="STEMResources.aspx">STEM  Resources</a>  

            <div class="STEMContent"> 
                <h1>STEM Blog</h1>
                <asp:Repeater ID="repBlogs" runat="server"  OnItemDataBound="repBlogs_ItemDataBound" OnItemCommand="repBlogs_ItemCommand" >
                    <ItemTemplate>
                        <div style="min-height:300px;border-bottom:1px solid #A9DEF2;">
                       <h2><a name="blog<%# DataBinder.Eval(Container.DataItem, "BlogID") %>" id="blog<%# DataBinder.Eval(Container.DataItem, "BlogID") %>">
                           </a> 
                           <asp:Literal ID="LiteralBlogTitle" runat="server"></asp:Literal>
                           
                        </h2>
                        <h4><asp:Literal ID="LiteralBlogDate" runat="server"></asp:Literal></h4>
                        <div>
                            <div style="float:right; padding-left:10px">
                                    <asp:Literal ID="LiteralImage" runat="server"></asp:Literal>
                            </div>
                            <asp:Literal ID="LiteralBlog" runat="server"></asp:Literal>         
                        </div>

                        <p><asp:Button ID="btnShowComments" class="msapBtn" OnClientClick = "SetTarget();" runat="server" Text="Show Comments" CommandName="Show" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BlogID") %>'/>
                            <asp:Button ID="btnPostComments" class="msapBtn" runat="server" Text="Post Comments" OnClientClick = "SetTarget();" CommandName="Post" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BlogID") %>' />
                        </p>
                        <%--<p style="border-top:1px solid #A9DEF2"></p>--%>
                            </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
</asp:Content>

