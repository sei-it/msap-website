using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class magnetpubSTEM : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getMasterTitle();
    }
    protected void OnSearch(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(txtSearchBox.Text))
            Response.Redirect("~/sitesearch.aspx?query=" + txtSearchBox.Text);
    }

    private void getMasterTitle()
    {
        string[] dbcnnParams = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString.Split(';');
        string dbname = "";

        foreach (string dbtmp in dbcnnParams)
        {
            if (dbtmp.Contains("Initial Catalog"))
            {
                dbname = dbtmp.Substring(dbtmp.IndexOf('=') + 1);
                break;
            }

        }

        switch (dbname.ToLower())
        {
            case "magnet_dev":
                ltlTitle.Text = "DEVELOPMENT SITE";
                break;
            case "magnet_staging":
                ltlTitle.Text = "STAGING SITE";
                break;
            case "magnet_staging1":
                ltlTitle.Text = "Magnet Mirror Site";
                break;
            case "msap_demo2014":
                ltlTitle.Text = "DEMO SITE";
                break;

            case "magnet":
                ltlTitle.Text = "";
                break;
            default:
                ltlTitle.Text = "Other DB site";
                break;
        }
    }
}
