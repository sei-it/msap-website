﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="ExcitingStudentsCont.aspx.cs" Inherits="Stem_ExcitingStuCont" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <%--<link rel="stylesheet" type="text/css" href="css/MSAP-STEMBeacon.css">--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="mainContent">
<h1>STEM Resource
</h1>
<h2>Exciting Students Through STEM Career Exploration
</h2>

<p>
<div class="holder">	<img src="images/career.jpg" title="Career"></div>

A great way to excite students about STEM learning is to provide real-world applications. In particular, sharing with students the array of STEM-related careers that are available can help them to see their own future in STEM. In addition, explain to students how STEM literacy skills can apply to other careers that are not directly related to science, technology, engineering, or mathematics.</p>
<p></p>

<p>
While inviting STEM professionals to speak to and work with your magnet students is ideal, it is not usually possible to bring a representative from every type of career to the school site. Online career exploration tools can be a great supplement to these in-person visits. These tools introduce students to STEM-related careers that they may not have been aware of. Exploration of these tools can be built into lessons, especially those that leverage technology – learning about careers can help bring the content presented to life. 
</p>


</div>
 <p>Some resources we have found regarding this topic include </p>
 <p></p>
        <div class="links-hl">

<ul>
<li><a href="http://thefunworks.edc.org/SPT--homegraphic.php" target="_blank">FunWorks. </a>This website is about math and science careers presented in an engaging manner for students.</li>
<li>	<a href="http://engineeryourlife.org/" target="_blank">Engineer Your Life</a> and <a href="http://www.engineergirl.org" target="_blank">Engineer Girl.</a> These websites aim to encourage girls to enter engineering careers. </li>
<li>	<a href="http://www.discovere.org/discover-engineering/engineering-careers" target="_blank">Discover Engineering</a>. This website includes a page that describes the many types of engineering careers.</li>
<li>	<a href="http://www.ams.org/careers" target="_blank">American Mathematical Society</a>. This website provides information on careers in mathematics.</li>
<li>	<a href="http://www.pbs.org/wgbh/nova/blogs/secretlife" target="_blank">The Secret Life of Scientists and Engineers</a>. This PBS webpage includes videos from scientists talking about the work that they do.</li>
<li>	<a href="http://www.genome.gov/GenomicCareers/" target="_blank">Geonomic Careers</a>. This website run by the National Human Genome Research Institute explores the types of geonomic careers available.</li>
<li>	<a href="http://profiles.jsc.nasa.gov/index1.cfm" target="_blank">NASA Profiles: Careers in Spaceflight</a>. This website describes careers related to spaceflight.</li>

</ul></div>
<p>
Let us know in the comments if you have used any other resources to help students explore STEM careers.</p>


</asp:Content>

