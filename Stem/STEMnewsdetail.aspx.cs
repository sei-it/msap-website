﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class STEM_STEMnewsdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
                    STEMDataContext db = new STEMDataContext();
                    var oPages = from pg in db.STEMNews
                                 where pg.NewsID == Convert.ToInt32(Request.QueryString["id"])
                                 select pg;
                    foreach (var news in oPages)
                    {
                        litNewsDeatil.Text  = string.Format("<b>{0}</b><i>{2} {3} {4}.</i><br /><br />{1}<br><p>Visit the <a href='STEMnews.aspx'>STEM  News</a> for more STEM news.</p>",
                                        "", news.NewsContent,
                                        !string.IsNullOrEmpty(news.Source) ? news.Source + "." : news.Source,
                                        !string.IsNullOrEmpty(news.Author) ? news.Author + "." : news.Author,
                                        news.Date);
                        litNewsTitle.Text = news.Title;
                    }

 
        }
    }
}