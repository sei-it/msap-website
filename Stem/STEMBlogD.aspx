﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="STEMBlogD.aspx.cs" Inherits="STEM_STEMBlogD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <a name="skip"></a>
          <img src="../images/stem-blog.jpg" width="842px" style="margin:0px;"/>
    <div class="mainContent">
                        <div class="STEMContent"> 


    <h1>STEM Blog</h1>
    <h2>
        <asp:Literal ID="LiteralBlogTitle" runat="server"></asp:Literal>

    </h2>
        <h4><asp:Literal ID="LiteralBlogDate" runat="server"></asp:Literal></h4>
        <div>
            <div style="float:right;">
                    <asp:Literal ID="LiteralImage" runat="server"></asp:Literal>
            </div>
          <asp:Literal ID="LiteralBlog" runat="server"></asp:Literal>
          
        </div>
    <h3>Comments</h3>
        <div>
                    <asp:Repeater ID="repComments" runat="server"   >
                        <ItemTemplate>
                            <h4>
                                Commented by <%# DataBinder.Eval(Container.DataItem, "commentBy") %> on <%# DataBinder.Eval(Container.DataItem, "commentDate") %>

                            </h4>
                            <p><%# DataBinder.Eval(Container.DataItem, "BlogComment") %></p>
                        </ItemTemplate>
                    </asp:Repeater>
        </div>
    </div>
        </div>
    
</asp:Content>

