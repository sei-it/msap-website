﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_STEMBlogCommentsAdd : System.Web.UI.Page
{
    int lngPkID;
    int lngBlogID;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;


        if (ViewState["IsLoaded1"] == null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
        }
        if (Page.Request.QueryString["lngBlogID"]     != null)
        {
            lngBlogID = Convert.ToInt32(Page.Request.QueryString["lngBlogID"]);
        }

        if (ViewState["IsLoaded1"] == null)
        {

            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        displayBlog();

    }



    protected void displayRecords()
    {
        object objVal = null;
        STEMDataContext db = new STEMDataContext();
        var oPages = from pg in db.STEMBlogComments
                     where pg.BlogID_Fk == lngPkID
                     select pg;
        foreach (var oBlog in oPages)
        {


            objVal = oBlog.BlogComment ;
            if (objVal != null)
            {
                txtcommentBy.Text = objVal.ToString();
            }
            objVal = oBlog.commentBy;
            if (objVal != null)
            {
                
                txtcommentBy.Text = objVal.ToString();
            }
         

        }

        db.Dispose();

    }



    public void updateBlogs()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        STEMDataContext db = new STEMDataContext();
        STEMBlogComment oBlog = (from c in db.STEMBlogComments where c.BlogCommentID == lngPkID select c).FirstOrDefault();

        if ((oBlog == null))
        {
            oBlog = new STEMBlogComment();
            blNew = true;
        }



        oBlog.BlogComment = txtBlogComment.Text;
        oBlog.commentBy = txtcommentBy.Text;
        oBlog.commentDate = DateTime.Now;
         
        //////////////////////
        if (blNew == true)
        {
            oBlog.BlogID_Fk = lngBlogID;
            db.STEMBlogComments.InsertOnSubmit(oBlog);
        }
        else
        {
         }

        db.SubmitChanges();
        lngPkID = oBlog.BlogCommentID;
        db.Dispose();


    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["lngBlogID"] != null)))
        {
            lngBlogID = Convert.ToInt32(this.ViewState["lngBlogID"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["lngBlogID"] = lngBlogID;
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        updateBlogs();
        displayRecords();
        SendEmailNotification();
        //Response.Redirect("STEMBlogDL.aspx?ID=" + lngPkID);
        Response.Write("<script>window.close()</script>");

    }

    protected void btnClose_Click(object sender, EventArgs e)
    {

        //Response.Redirect("STEMBlogDL.aspx");
        Response.Write("<script>window.close()</script>");
    }

    private void displayBlog()
    {
        STEMDataContext db = new STEMDataContext();
        var oPages = from pg in db.STEMBlogs
                     where pg.BlogID == lngBlogID
                     select pg;
        foreach (var news in oPages)
        {
            LiteralBlogTitle.Text = news.Title;
           // LiteralBlog.Text = news.Blog;
            LiteralBlogDate.Text = String.Format("{0:MMMM d, yyyy}", news.BlogDate);
         }
 
    }
       private void SendEmailNotification()
    {


            try
            {

                string strfrom = System.Web.Configuration.WebConfigurationManager.AppSettings["STEMNotificationSenderAddress"];
                string strTo = System.Web.Configuration.WebConfigurationManager.AppSettings["STEMNotificationTo"];				
                MailMessage objMailMsg = new MailMessage(strfrom,strTo);
                objMailMsg.BodyEncoding = System.Text.Encoding.UTF8;
                // objMailMsg.CC.Add("ASeddoh@seiservices.com");
                // objMailMsg.CC.Add("VKothale@seiservices.com");

                objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["STEMEmailSubject"];
                 // //-------Email body-----------------------

                String strBody1;
                strBody1 = "<p>The following  comment has been  added to </p>";
                strBody1 = strBody1 + "<p><B>" + LiteralBlogTitle.Text  + "</b></p>";
                strBody1 = strBody1 + "<p>By: " + txtcommentBy.Text  + "</p>";
                strBody1 = strBody1 + "<p>" + txtBlogComment.Text  + "</p>";
                objMailMsg.Body = strBody1;

                // //=============================

                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

                // //--------prepare to send mail via SMTP transport-----//

                SmtpClient objSMTPClient = new SmtpClient();
                objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                 objSMTPClient.Send(objMailMsg);
              //  litMessage.Text = "password have been sent to " + oResSubmission.sUserID;

            }
            catch (Exception ex)
            {
                // litMessage.Text="Could not send the e-mail - error: " + ex.Message;
                throw ex;
            }
     }

 }