﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="STEMResources.aspx.cs" Inherits="STEM_STEMResources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <a name="skip"></a>
          <img src="./images/stem-resources.jpg" width="842px" style="margin:0px;"/>
        <div class="mainContent">
            <a href="stemhome.aspx">STEM  Beacon</a> | <a href="STEMBlogDL.aspx">STEM  Blog</a> | <a href="STEMNews.aspx">STEM  News</a>   

                        <div class="STEMContent"> 
            <h1>STEM Resources</h1>
            <asp:Literal ID="ltlfilter" runat="server"></asp:Literal>
            <asp:Repeater ID="repResources" runat="server" OnItemDataBound="repResources_ItemDataBound">
                <ItemTemplate>
                    <h2><%# DataBinder.Eval(Container.DataItem, "ResourceName") %>  </h2>
                    <p><%# DataBinder.Eval(Container.DataItem, "ResourceDescription") %> </p>
                        <asp:Literal ID="litResourceName" runat="server"></asp:Literal> 
                      <p style="border-top:1px solid #A9DEF2"></p>
                </ItemTemplate>
            </asp:Repeater>
         <%--   ResourceName, ResourceLink, ResourceDescription--%>
        </div>
            </div>
</asp:Content>

