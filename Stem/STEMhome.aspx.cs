﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Stem_STEMhome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
        displayRecords();
    }
    private void displayRecords()
    {
        object objVal = null;
        using (STEMDataContext db = new STEMDataContext())
        {

            var vCases = db.get_STEMTop5();
            //HttpContext.Current.User.Identity.Name

            repResources.DataSource = vCases;
            repResources.DataBind();

            var vCasesNews = db.get_STEMNewsHomePageList();
            repNews.DataSource = vCasesNews;
            repNews.DataBind();

            var vCasesBlogs = db.get_STEMBlogHomePageList();
            repBlog.DataSource = vCasesBlogs;
            repBlog.DataBind();



        }

    }
 
}