﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="PlanSTEM ImplementationCont.aspx.cs" Inherits="Stem_PlanningSTEM_ImplementationCont" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="mainContent">
<h1>STEM Resource</h1>
<h2>Planning for STEM Implementation</h2>
<div>
<p><div class="holder">	<img src="images/planning.jpg" title="Planning"></div>

Like any other part of implementing your magnet program, implementing your STEM curricula and partnerships requires careful planning. One resource to help your magnet program evaluate its STEM initiatives and plan next steps is the <a href="http://stemisphere.carnegiesciencecenter.org/educators" target="_blank">STEM Excellence Pathway</a> from the Carnegie Science Center.The pathway provides a self-evaluation tool, a guide for effective steps to take to improve implementation, stories of successful STEM initiatives, and personalized assistance in identifying goals for implementation. Visit the website to use the tools today, and let us know in the comments how you use them.</p>

</div>
</div>


</asp:Content>

