﻿<%@ Page Title="" Language="C#" MasterPageFile="magnetpubSTEM.master" AutoEventWireup="true" CodeFile="STEMhome.aspx.cs" Inherits="Stem_STEMhome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <meta charset="UTF-8">
<title>MSAP STEM Beacon</title>

<link rel="stylesheet" type="text/css" href="css/MSAP-STEMBeacon.css">

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="js/bjqs-1.3.min.js"></script>
<link type="text/css" rel="Stylesheet" href="css/bjqs.css" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="header" >
<%--    <style>
        .bjqsContent {
            float: left;
            height: 120px;
            overflow: hidden;
            width: 200px;
        }
    </style>--%>
</div>

<div id="main-content"><!--MAIN CONTENT-->
  <div id="Div1">
 
</div>  
    <div id="highlights">
        <div class="logo"><img src="images/STEM-Beacon-logo.png" width="134" height="64" alt="STEM Beacon logo"/></div>
	
    <div class="plus">
		<p class="a">&#43;</p><p class="b">&#43;</p>
    </div>
    
        <div class="resource-hl">
             <h3>Beacon Blog</h3>

      <div id="banner-slide">
        <ul class="bjqs">
                     <asp:Repeater ID="repBlog" runat="server"   >
                       <ItemTemplate>
                                     <li>
                                         <div class="bjqsContentText"> 
                                            <h4><b><%# DataBinder.Eval(Container.DataItem, "Title") %></b></h4>
                                            <%# DataBinder.Eval(Container.DataItem, "BlogPreview") %> <br /><br />
                                             <a class="more" href="STEMBlogDL.aspx?id=<%# DataBinder.Eval(Container.DataItem, "BlogID") %>#blog<%# DataBinder.Eval(Container.DataItem, "BlogID") %>" >Read more</a>
                                        </div>
                                        <div class="bjqsImage">
                                           <img src="imagesBlog/<%# DataBinder.Eval(Container.DataItem, "blogImagePreview") %>" title=""/>

                                        </div>                                         
                                      </li>
                        </ItemTemplate>
                    </asp:Repeater>
        </ul>
      </div>
        </div>
        
        <div class="events-hl">
           <h3>STEM News</h3>
            <div  class="rss" style="overflow: hidden;">
                 <ul>
                     <asp:Repeater ID="repNews" runat="server"   >
                        <ItemTemplate>
                            <li><font face="Helvetica"><strong><a href="STEMnewsdetail.aspx?id=<%# DataBinder.Eval(Container.DataItem, "NewsID") %> " title=""  > 
                                <b><%#DataBinder.Eval(Container.DataItem, "Title")%></b></a></strong><br />
                                <%#DataBinder.Eval(Container.DataItem, "Source")%>.
                                <%#DataBinder.Eval(Container.DataItem, "Author")%>.
                                <%#DataBinder.Eval(Container.DataItem, "Date")%>... <a class="more"  href="STEMnewsdetail.aspx?id=<%# DataBinder.Eval(Container.DataItem, "NewsID") %> " title=""  > 
                                Read  more</a></font>
                                </li>
                        </ItemTemplate>
                    </asp:Repeater>
                 </ul>
            </div>
                  <p><a class="more" href="STEMNews.aspx" style="color:royalblue;" >Read more STEM news</a></p>    
        </div>
        
        <div class="links-hl">
           <h3>STEM Resources</h3>
            <p>The STEM Beacon resources contain lessons, activities, professional development, and other information to help teachers, students and parents learn about and engage in all things STEM. The resources are educational, innovative, and fun.</p>
           <br />
                 <ul>

                     <asp:Repeater ID="repResources" runat="server"   >
                        <ItemTemplate>
                            <li><a href="<%# DataBinder.Eval(Container.DataItem, "ResourceLink") %> " title="" target="_blank"><%# DataBinder.Eval(Container.DataItem, "ResourceName") %></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                 </ul>
           <%-- <p class="more"><a style="color:#f7be08" href="STEMResources.aspx">View resources </a></p>  --%>  
            <p><a class="more"  href="STEMResources.aspx">View resources </a></p>  
        </div>
        
    </div><!--/HIGHLIGHTS-->
</div><!--/MAIN CONTENT-->
    

<script>
    jQuery(document).ready(function ($) {
        $('#banner-slide').bjqs({
            'width': 375,
            'height': 180,
            'responsive': false
        });
    });
</script>





</asp:Content>

