﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="STEMBlogCommentsList.aspx.cs" Inherits="STEM_STEMBlogCommentsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="mainContent">
            <div class="STEMContent">
            <h1>STEM Blog Comments</h1>
            <h2>
                <asp:Literal ID="LiteralBlogTitle" runat="server"></asp:Literal>
            </h2>
            <h4><asp:Literal ID="LiteralBlogDate" runat="server"></asp:Literal></h4>
            <div>
 
              <asp:Literal ID="LiteralBlog" runat="server"></asp:Literal>
                <asp:Literal ID="LiteralCommentesYesNo" runat="server"></asp:Literal>        
            </div>
                        <asp:Repeater ID="repComments" runat="server"   >
                        <ItemTemplate>
                            <h4>
                                Commented by <%# DataBinder.Eval(Container.DataItem, "commentBy") %> on <%# DataBinder.Eval(Container.DataItem, "commentDate") %>

                            </h4>
                            <p><%# DataBinder.Eval(Container.DataItem, "BlogComment") %></p>
                        </ItemTemplate>
                    </asp:Repeater>
                                         <p>
                               <asp:Button 
                                ID="btnClose" runat="server"  class="msapBtn" Text="Close" OnClientClick="window.close(); return false;" Visible="true" />&nbsp;&nbsp;&nbsp;
          
                        </p>
            </div>
            </div>
</asp:Content>

