﻿<%@ Page Title="" Language="C#" MasterPageFile="magnetpubSTEM.master" AutoEventWireup="true" CodeFile="UsingSTEMImproveWorkforce.aspx.cs" Inherits="Stem_UsingSTEMImproveWorkforce" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/MSAP-STEMBeacon.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="mainContent">
<h1>STEM Resource</h1>

<h2>Using STEM to Improve the Workforce</h2>
<div>

<p><div class="holder">	<img src="imagesBlog/blog01.jpg" title="Work Force"></div>

A new study released by the Carnegie Science Center convened STEM leaders in three states to evaluate the condition of STEM in the region and identify ways that improving STEM education can improve the region’s workforce. </p>

<p>Sharing research such as this with your magnet program’s stakeholders can help explain the importance of the education your magnet schools are providing. Hearing how STEM education benefits the entire community, including local businesses, can help increase buy-in for your magnet schools. </p>

<p>You can read the report <a href="http://www.carnegiesciencecenter.org/stemcenter/stemcenter-work-to-do-the-role-of-stem-education/?utm_source=signage&utm_medium=print&utm_campaign=Work%20To%20Do" target="_blank">here</a>. Then let us know your thoughts in the comments.</p>
</div>
<p></p>
</div>
</asp:Content>

