﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="STEMhome.aspx.cs" Inherits="Stem_STEMhome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="css/MSAP-STEMBeacon.css">

	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="js/bjqs-1.3.min.js"></script>
    <link type="text/css" rel="Stylesheet" href="css/bjqs.css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="container">

<div id="header">
    <div id="menu">
    <ul>
        <li class="projects"><a href="#" title="Projects">PROJECTS</a></li>
        <li class="mobile-apps"><a href="#" title="Mobile Apps">MOBILE APPS</a></li>
        <li class="events"><a href="#" title="Events">EVENTS</a></li>
        <li class="case-studies"><a href="#" title="Case Studies">CASE STUDIES</a></li>
        <li class="careers"><a href="#" title="Careers">CAREERS</a></li>
    </ul>
	</div>
</div>

<div id="main-content"><!--MAIN CONTENT-->
    
    <div id="highlights">
        <div class="logo"><img src="images/STEM-Beacon-logo.png" width="134" height="64" alt="STEM Beacon logo"/></div>
		
    <div class="plus">
		<p class="a">&#43;</p><p class="b">&#43;</p>
    </div>
        
        <div class="resource-hl">
             <h3>Featured Resource</h3>

      <div id="banner-slide">
        <ul class="bjqs">
          <li><img src="images/banner01.jpg" title=""></li>
          <li><img src="images/banner02.jpg" title=""></li>
          <li><img src="images/banner03.jpg" title=""></li>
        </ul>
      </div>
      
      <p><b>Mariam County School</b> Fusce phar etr anisi eget lorem efficitur, id con val lisli gula facilisis. Crase uma gna a magna congueh endrerit. Suspen dissed igni ssim fermentu mante sit amet ult rices. Nunc var iusl ibero leo, a element umar cufin ibusat. Dui sauc torn islet element um maximus. Cras eff ici turor ciin aug ueor nare, quis imperdiet</p>
      <p class="more"><a href="#">View More &#43;</a></p>
            
        </div>
        
        <div class="events-hl">
           <h3>News / Events</h3>
                 <ul>
                     <li><a href="#" title="">Vivamus elementum semper nisi.</a></li>
                     <li><a href="#" title="">Aenean vulputate eleifend tellus.</a></li>
                     <li><a href="#" title="">Aenean leo ligula, porttitor eu, consequatvitae, eleifend ac, enim.</a></li>
                     <li><a href="#" title="">Phasellus viverra nulla ut metus varius laoreet</a></li>
                 </ul>
        </div>
        
        <div class="links-hl">
           <h3>Links</h3>
                 <ul>
                     <li><a href="#" title="">Phasellus viverra nulla ut metus vari laoreet.</a></li>
                     <li><a href="#" title="">Quisque rutrum. Aenean imperdiet. Etiam ultricinisi vel augue.</a></li>
                     <li><a href="#" title="">Curabitur ullamcorper ultricies nisi.Nam eget dui. Etiam rhoncus.</a></li>
                 </ul>
        </div>
        
    </div><!--/HIGHLIGHTS-->
</div><!--/MAIN CONTENT-->
    
<div id="footer">
</div>
 
</div>
<script>
    jQuery(document).ready(function ($) {
        $('#banner-slide').bjqs({
            'width': 168,
            'height': 125,
            'responsive': true
        });
    });
</script>



</asp:Content>

