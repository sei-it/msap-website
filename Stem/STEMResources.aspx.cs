﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class STEM_STEMResources : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
        buildFitlerLetterList();
            displayRecords();
    }

    private void buildFitlerLetterList()
    {
        string filterTBL = "<table class='content' width='100%'>" + 
"<tbody>" + 
"<tr>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=*'>All</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=A'>A</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=B'>B</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=C'>C</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=D'>D</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=E'>E</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=F'>F</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=G'>G</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=H'>H</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=I'>I</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=J'>J</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=K'>K</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=L'>L</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=M'>M</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=N'>N</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=O'>O</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=P'>P</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=Q'>Q</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=R'>R</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=S'>S</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=T'>T</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=U'>U</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=V'>V</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=W'>W</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=X'>X</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=Y'>Y</a>" + 
"</td>" + 
"<td class='post' align='left'>" + 
"<a href='STEMResources.aspx?letter=Z'>Z</a>" + 
"</td>" + 
"</tr>" + 
"</tbody>" +
"</table>";

        string selectedLetter = string.IsNullOrEmpty(Request.QueryString["letter"]) ? "*" : Request.QueryString["letter"];
        if (selectedLetter =="*")
        filterTBL = filterTBL.Replace("<a href='STEMResources.aspx?letter=" + selectedLetter + "'>All</a>", "<a><font color='black'><b>All</b></font></a>" );
        else
            filterTBL = filterTBL.Replace("<a href='STEMResources.aspx?letter=" + selectedLetter + "'>" + selectedLetter + "</a>", "<a><font color='black'><b>" + selectedLetter + "</b></font></a>");

        ltlfilter.Text = filterTBL;
    }
    private void displayRecords()
    {
        object objVal = null;
        string selectedLetter = string.IsNullOrEmpty(Request.QueryString["letter"]) ? "*" : Request.QueryString["letter"];

        using (STEMDataContext db = new STEMDataContext())
        {
            var vCases = db.filter_STEMResourcesList(selectedLetter);
            //HttpContext.Current.User.Identity.Name

            repResources.DataSource = vCases;
            repResources.DataBind();


        }

    }
    protected void repResources_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Literal oLit;
        string sResName = "";
        if (DataBinder.Eval(e.Item.DataItem, "ResourceLink") != null)
        {
          //  sResName = @" <a href=""" + DataBinder.Eval(e.Item.DataItem, "ResourceLink").ToString() +  @""" target=""_blank"">" + DataBinder.Eval(e.Item.DataItem, "ResourceName").ToString() + "</a>" ;
            sResName = @" <a href=""" + DataBinder.Eval(e.Item.DataItem, "ResourceLink").ToString() + @""" target=""_blank"">" + DataBinder.Eval(e.Item.DataItem, "ResourceLink").ToString() + "</a>";
             
        }
        else
        {
            //sResName = DataBinder.Eval(e.Item.DataItem, "ResourceName").ToString();
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) 
        {
            oLit = (Literal)e.Item.FindControl("litResourceName");
            oLit.Text = sResName;
        }
    }
}