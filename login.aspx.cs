﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using log4net;
using System.Web.Security;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            /*if (Session["Magnet"] != null)
            {
                Response.Redirect("~/admin/default.aspx");
            }*/
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            ValdiateUser();
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = ex.Message ;

            ILog Log = LogManager.GetLogger("EventLog");
            Log.Fatal("User login.", ex);
        }
    }

    protected void Clear_Click(object sender, EventArgs e)
    {
        txtUserName.Text = String.Empty;
        txtPassword.Text = String.Empty;
        lblError.Visible = false;
    }

    private void ValdiateUser()
    {

        if (Membership.ValidateUser(txtUserName.Text.Trim(), txtPassword.Text))
        {
            String sPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text, "md5");

            Session["Magnet"] = "Login";
            FormsAuthentication.SetAuthCookie(txtUserName.Text, true);

            if (Roles.IsUserInRole(txtUserName.Text.Trim(), "Subcontractor"))
            {
                Response.Redirect("~/modules/intro.aspx");
            }
            else
            {
                if (Request.Params["ReturnUrl"] != null)
                {
                    switch (Request.Params["ReturnUrl"].ToString())
                    {
                        case "/admin/messageboard/default.aspx":
                        case "modules/intro.aspx":
                            FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, false);
                            break;
                        default:
                            Response.Redirect("admin/default.aspx", false);
                            break;
                    }
                }
                else
                    Response.Redirect("admin/default.aspx", false);

                //tracking user login
                UsersDataContext userdb = new UsersDataContext();
                int userid = userdb.yaf_Users.SingleOrDefault(x => x.Name ==  txtUserName.Text.Trim()).UserID;

                UserTracker myTracker = new UserTracker();
                myTracker.SessionID = HttpContext.Current.Session.SessionID;
                myTracker.UserID = userid;
                myTracker.fstLogin = DateTime.Now;
                myTracker.Login = DateTime.Now;
                myTracker.IP = HttpContext.Current.Request.UserHostAddress;
                myTracker.Location =  HttpContext.Current.Request.FilePath;
                myTracker.BoardID = 0;
                myTracker.LastActive = DateTime.Now;
                userdb.UserTrackers.InsertOnSubmit(myTracker);
                userdb.SubmitChanges();
            }
        }
        else
        {
            lblError.Visible = true;
            string userName = txtUserName.Text.Trim();
            MembershipUser memCheckUser = Membership.GetUser(userName);
            int maxAtempt = Membership.Providers["SqlProvider"].MaxInvalidPasswordAttempts;
            if (memCheckUser != null && memCheckUser.IsLockedOut == true)
            {
                lblError.Text = "Your account has been temporarily locked due to too many login attempts. Please contact the <a href='mailto:msapcenter@leedmci.com'>MSAP Center</a> for assistance.";
            }
            else
            {
              UsersDataContext db = new UsersDataContext();
              var usrRcds = from u in db.vwUserFailtologins where (u.UserName==userName) select u;
              //  ((SqlMembershipProvider)System.Web.Security.Membership.Provider).GetFailedPasswordAttemptCount(...);
              if (usrRcds != null && usrRcds.Count()>0)
              {
                  foreach (var usr in usrRcds)
                  {
                      lblError.Text = "Your username and/or password are invalid.<br/> You have " + (maxAtempt - usr.FailedPasswordAttemptCount) + " times left before getting locked out.";
                      break;
                  }
              }
              else
                  lblError.Text = "Your username and/or password are invalid.<br/>";
            }
            
        }
    }

    protected void btnLostPassword_Click(object sender, EventArgs e)
    {
        LoginView.Visible = false;
        RecoverView.Visible = true;
        ResetView.Visible = false;
        lblError.Visible = false;
    }
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        LoginView.Visible = false;
        RecoverView.Visible = false;
        ResetView.Visible = true;
        lblError.Visible = false;
    }
    protected void OnChangePassword(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            MembershipUser user = Membership.GetUser(txtResetUserName.Text);
            try
            {
                if (user != null && !string.IsNullOrEmpty(txtOldPassword.Text) && !string.IsNullOrEmpty(txtNewPassword.Text))
                {
                    if (user.ChangePassword(txtOldPassword.Text, txtNewPassword.Text))
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('Your password has been successfully changed!');</script>", false);
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('We were unable to change your password. Please ensure that you have entered the correct username and old password, and that your new password contains at least 6 characters.');</script>", false);
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('We were unable to change your password. Please ensure that you have entered the correct username and old password, and that your new password contains at least 6 characters.');</script>", false);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('We were unable to change your password. Please ensure that you have entered the correct username and old password, and that your new password contains at least 6 characters.');</script>", false);
            }
        }
    }
    protected void Recover_Click(object sender, EventArgs e)
    {
        lblError.Visible = false;
        try
        {
            if (LostUserName.Text.Length == 0)
            {
                lblError.Visible = true;
                lblError.Text = "User name is required.";
                return;
            }

            MembershipUser user = Membership.GetUser(LostUserName.Text);
            string oldPassword = user.ResetPassword();

            //send email
            string strFrom = "SEInfo@synergyE365.onmicrosoft.com";
            MailMessage objMailMsg = new MailMessage(strFrom, user.Email);

            objMailMsg.BodyEncoding = Encoding.UTF8;
            objMailMsg.Subject = "New password for MSAP Center website.";
            objMailMsg.Body = "<p>Dear " + user.UserName + ",</p><p>As requested, here is your new password to log into the MSAP Center private workspace. The next time you log in, click “Change Password” below the username and password fields, and enter your temporary password in the “Old Password” field.</p><p>Temporary password: "
            + oldPassword + "</p><p>If you did not request this change, please contact the  <a href='mailto:msapcenter@seiservices.com'>MSAP Center</a> or (866) 997-6727.</p>";
            objMailMsg.Priority = MailPriority.High;
            objMailMsg.IsBodyHtml = true;

            //prepare to send mail via SMTP transport
            SmtpClient objSMTPClient = new SmtpClient();
            objSMTPClient.Host = "mail2.seiservices.com";
            //NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
            //objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
            objSMTPClient.Send(objMailMsg);
            LoginView.Visible = true;
            RecoverView.Visible = false;

            ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('A temporary password has been e-mailed to you.');</script>", false);
        }
        catch (Exception x)
        {
            ILog Log = LogManager.GetLogger("EventLog");
            Log.Fatal("Recover password.", x);
            lblError.Visible = true;
            lblError.Text = "We were unable to create your new password.<br/> Please make sure you have entered a valid username.";
        }
    }
}
