﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class manageundeclared : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (MagnetGrantee grantee in MagnetGrantee.All())
            {
                ddlGrantees.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
                ddlGrantees2.Items.Add(new ListItem(grantee.GranteeName, grantee.ID.ToString()));
            }
        }
    }
    protected void OnGranteeChanged(object sender, EventArgs e)
    {
        string strReportPeriod = "";
        ddlReports.Items.Clear();

        if (ddlGrantees.SelectedIndex > 0)
        {
            
            foreach (GranteeReport report in GranteeReport.Find(x => x.GranteeID == Convert.ToInt32(ddlGrantees.SelectedValue) && (bool)x.ReportType == false))
            {
                strReportPeriod = MagnetReportPeriod.SingleOrDefault(x => x.ID == report.ReportPeriodID).Des.Split('|')[1];
                ddlReports.Items.Add(new ListItem(strReportPeriod, report.ID.ToString()));
            }
        }
    }
    protected void OnReportChanged(object sender, EventArgs e)
    {
        if (ddlReports.SelectedIndex >= 0)
        {
            dataRow.Visible = true;
            saveRow.Visible = true;
            GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlReports.SelectedValue));
            if (report.UndeclaredField != null)
            {
                if (report.UndeclaredField == true)
                    rbYes.Checked = true;
                else
                    rbNo.Checked = true;
            }
            else
            {
                rbYes.Checked = false
                    ;
                rbNo.Checked = false;
            }
        }
        else
        {
            dataRow.Visible = false;
            saveRow.Visible = false;
        }
    }
    protected void OnSave(object sender, EventArgs e)
    {
        if (ddlGrantees.SelectedIndex > 0 && ddlReports.SelectedIndex >= 0)
        {
            if (rbNo.Checked || rbYes.Checked)
            {
                GranteeReport report = GranteeReport.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlReports.SelectedValue));
                report.UndeclaredField = rbYes.Checked;
                report.Save();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Your data has been saved!');</script>", false);
            }
        }
    }
    protected void OnGrantee2Changed(object sender, EventArgs e)
    {
        if (ddlGrantees2.SelectedIndex > 0)
        {
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlGrantees2.SelectedValue));
            if (grantee.Desegregation != null)
            {
                if ((bool)grantee.Desegregation)
                    RadioButton1.Checked = true;
                else
                    RadioButton2.Checked = true;
            }
        }
    }
    protected void OnSaveDesegregation(object sender, EventArgs e)
    {
        if (ddlGrantees2.SelectedIndex > 0)
        {
            MagnetGrantee grantee = MagnetGrantee.SingleOrDefault(x => x.ID == Convert.ToInt32(ddlGrantees2.SelectedValue));
            if (RadioButton1.Checked)
                grantee.Desegregation = true;
            if(RadioButton2.Checked)
                grantee.Desegregation = false;
            grantee.Save();
        }
    }
}