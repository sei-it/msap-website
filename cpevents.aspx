﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="cpevents.aspx.cs" Inherits="events" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP - Upcoming events
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Upcoming Events</h1>
        <div>
            View upcoming webinars and conferences.
        </div>
        <h3>
            Webinars</h3>
        <h2>
            Media Literacy: The 21st Century Critical Thinking Skills All Students Need with
            Frank Baker</h2>
        <p>
            <strong>Organization</strong>: ASCD
            <br />
            <strong>Date</strong>: February 9, 2011
            <br />
            <strong>Time</strong>: 3:00 - 4:00 PM EASTERN
            <br />
            <strong>Registration</strong>: Individual registration is required to participate
            in this webinar.
            <br />
            <strong>Description</strong>: Most of our students are media-savvy, but most are
            not media literate. Students today tend to believe everything they see, read, and
            hear--especially if it originates on a screen. Media literacy, among other things,
            involves using media and popular culture to engage students in critical thinking
            and viewing, while at the same time meeting teaching standards. In this webinar,
            you will learn<br />
            <ul>
                <li>Why media literacy is an important 21st century skill; </li>
                <li>How to integrate media literacy into your classroom instruction; and </li>
                <li>How to help students use critical thinking skills to analyze media. </li>
            </ul>
            <a href="https://www1.gotomeeting.com/register/502680472" target="_blank">Learn More</a>
        </p>
        <h2>
            February 2011 Technical Assistance Webinars for ED Grantees & Sub-Grantees: ED Discretionary
            Grants Administration
        </h2>
        <p>
            <strong>Organization</strong>:U.S. Department of Education
            <br />
            <strong>Date</strong>:February 14, 2011
            <br />
            <strong>Time</strong>:2:00 - 3:30 PM EASTERN
            <br />
            <strong>Registration</strong>:Individual registration is required to participate
            in this webinar.
            <br />
            <strong>Description</strong>:This webinar is designed to provide recipients of ED
            grants (including grant project personnel and business office staff) with a general
            overview of ED’s discretionary grants administration process. With the significant
            increase of federal funds, recipients have a heightened responsibility to ensure
            their projects are managed in compliance with federal rules, regulatory requirements
            such as the Education Department General Administrative Regulations (EDGAR) and
            Office of Management and Budget (OMB) Cost Principles, and sound business practices.
            This webinar will provide the audience with information on:
            <ul>
                <li>Accountability and its importance;</li>
                <li>EDGAR grant requirements;</li>
                <li>Roles and responsibilities;</li>
                <li>Project Revisions, formerly known as Expanded Authorities; and</li>
                <li>Grant policy documents and other grant administration resources available on ED’s
                    website.</li>
            </ul>
            <a href="http://www2.ed.gov/policy/gen/leg/recovery/rms-web-conferences.html" target="_blank">
                Learn More </a>
        </p>
        <h2>
            February 2011 Technical Assistance Webinars for ED Grantees & Sub-Grantees: Internal
            Control
        </h2>
        <p>
            <strong>Organization</strong>:U.S. Department of Education
            <br />
            <strong>Date</strong>:February 22, 2011
            <br />
            <strong>Time</strong>:2:00 - 3:30 PM EASTERN
            <br />
            <strong>Registration</strong>:Individual registration is required to participate
            in this webinar.
            <br />
            <strong>Description</strong>:This webinar will define and describe internal control
            and explain the role it plays in managing federal grant programs. Presenters will
            review the benefits of good internal controls, identify the parties involved and
            their roles and responsibilities, and describe the importance of controls in achieving
            successful grant outcomes. A review of the five standards of internal controls developed
            by the General Accountability Office (GAO) and a list of resources for further guidance
            and self-assessment will also be presented. The primary audience for this presentation
            will be all ED grantees and sub-grantees; however, other state and local education
            officials are encouraged and welcome to participate as well. We hope you all can
            join us! <a href="http://www2.ed.gov/policy/gen/leg/recovery/rms-web-conferences.html"
                target="_blank">Learn More</a>
        </p>
        <h2>
            Key Emerging Technologies for Elementary and Secondary Education: 2011 Horizon Report:
            K-12 Edition
        </h2>
        <p>
            <strong>Organization</strong>:Consortium for School Networking (CoSN)
            <br />
            <strong>Date</strong>:April 12, 2011
            <br />
            <strong>Time</strong>:1:00 -2:00 PM EASTERN
            <br />
            <strong>Registration</strong>:Individual registration is required to participate
            in this webinar.
            <br />
            <strong>Description</strong>:For the third year, CoSN partners with the New Media
            Consortium to create the highly influential Horizon Report: K-12 Edition. Don’t
            miss this much anticipated discussion of key learning technologies. Learn about
            the two most important emerging technologies for this year, two more in the two
            to three year horizon, and finally two additional in the four to five year horizon.
            Also, hear about the accompanying Toolkit, which includes a facilitator’s guide
            and presentation, to enable a discussion around these issues in your institution.
            <a href="http://www.cosn.org/Events/20102011WebinarSeriesDescriptions/tabid/7304/Default.aspx"
                target="_blank">Learn More</a>
        </p>
        <h2>
            Finding Your Pathway
        </h2>
        <p>
            <strong>Organization</strong>:Consortium for School Networking (CoSN)
            <br />
            <strong>Date</strong>:May 10, 2011
            <br />
            <strong>Time</strong>:1:00-2:00PM EASTERN
            <br />
            <strong>Registration</strong>:Individual registration is required to participate
            in this webinar.
            <br />
            <strong>Description</strong>:What are the skills that you, the district technology
            leader, need to know to succeed? How have these skills changed in recent years?
            Take a self-assessment evaluation tied to CoSN’s Framework of Essential Skills of
            the K-12 CTO. Then get practical tips on how to upgrade your core competencies with
            professional development designed for you. The panel will be led by the CoSN Certification
            Committee. <a href="http://www.cosn.org/Events/20102011WebinarSeriesDescriptions/tabid/7304/Default.aspx"
                target="_blank">Learn More</a>
        </p>
        <h3>
            Conferences</h3>
        <h2>
            CORE Annual Leadership Summit Sustaining Excellence: What it Takes
        </h2>
        <p>
            <strong>Organization</strong>:CORE
            <br />
            <strong>Date</strong>:March 3–4, 2011
            <br />
            <strong>Location</strong>:San Francisco, CA
            <br />
            <strong>Description</strong>:The CORE Leadership Summit 2011, “Sustaining Excellence:
            What it Takes” addresses the need for educators and administrators to maintain their
            commitment to student success. Nationally recognized experts will provide relevant
            examples of what works to improve teaching and learning and sustain academic achievement
            for all. <a href="http://www.corelearn.com/Events/CORE-Annual-Leadership-Summit.html"
                target="_blank">Learn More</a>
        </p>
        <h2>
            Mobile Learning Experience 2011
        </h2>
        <p>
            <strong>Organization</strong>:Arizona K-12 Center, Mashable, and LanSchool
            <br />
            <strong>Date</strong>:April 6-8, 2011
            <br />
            <strong>Location</strong>:Phoenix, AZ
            <br />
            <strong>Description</strong>:Interested in iPods, iPads, netbooks, laptops, and
            phones? Excited about mobile technology for teaching and learning? Want to connect
            with other educators and learn from their successes and challenges? <a href="http://mobile2011.org/"
                target="_blank">Learn More</a>
        </p>
        <h2>
            NAESP Annual Convention & Exposition
        </h2>
        <p>
            <strong>Organization</strong>:National Association of Elementary School Principals
            <br />
            <strong>Date</strong>:April 7-10, 2010
            <br />
            <strong>Location</strong>:Tampa, FL
            <br />
            <strong>Description</strong>:The NAESP Convention program will focus on five content
            areas. The plenary sessions, as well as the concurrent sessions, will spotlight
            these topics to support a scaffolding of discussions, interactions, and information
            to strengthen and sustain the goals of NAESP in providing a comprehensive learning
            opportunity for principals. A strong component of our programming this year is to
            engage and include school-based professional learning teams and teacher leaders,
            which are necessary for a successful school that is result-oriented. <a href="http://s15.a2zinc.net/clients/naesp/naesp11/Public/MainHall.aspx?ID=805&sortMenu=101000"
                target="_blank">Learn More</a>
        </p>
        <h2>
            International Society for Technology in Education 2011
        </h2>
        <p>
            <strong>Organization</strong>:International Society for Technology in Education
            <br />
            <strong>Date</strong>:June 26-29, 2011
            <br />
            <strong>Location</strong>:Philadelphia, PA
            <br />
            <strong>Description</strong>:Hosted by ISTE at rotating cities each year, in cooperation
            with our regional affiliate partners, our international conference attracts thousands
            of PK-12 teachers, teacher educators, administrators, technical coordinators, staff
            developers, and anyone interested in staying on the forefront of utilizing ed tech
            to improve learning and teaching. <a href="http://www.iste.org/conference.aspx" target="_blank">
                Learn More</a>
        </p>
        <h2>
            ASCD Summer Conference: Differentiated Instruction, Learning by Design, What Works
            in Schools, Curriculum Mapping
        </h2>
        <p>
            <strong>Organization</strong>:ASCD
            <br />
            <strong>Date</strong>:July 1-3, 2011
            <br />
            <strong>Location</strong>:Boston, MA
            <br />
            <strong>Description</strong>: Four great conferences in one:
            <ul>
                <li>Differentiated Instruction (DI) — a systematic approach to ensuring that every student
                    is learning, regardless of interests, learning styles, or readiness for school;
                </li>
                <li>Understanding by Design (UbD) — a framework to design new curriculum based on achieving
                    student understanding of content; </li>
                <li>What Works in Schools (WWIS) — a research-based approach to focusing your entire
                    school or district on the school-, teacher-, and student-level factors that influence
                    achievement; and </li>
                <li>Curriculum Mapping — a way to document and analyze what exactly is being taught
                    and when in the actual school calendar. </li>
            </ul>
            Here’s the one opportunity you’ll have all year to learn about all four proven approaches
            or go in-depth on the one that matters to you most. <a href="http://www.ascd.org/conferences/summer-conference/2011.aspx"
                target="_blank">Learn More</a>
        </p>
    </div>
</asp:Content>
