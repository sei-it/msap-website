﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="unsubscribe.aspx.cs" Inherits="unsubscribe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Unsubscribe News letter
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="brnImg">
        <img src="images/newsHeader.jpg" width="842" alt="Text" style="border-bottom: 1px solid #CCC;" />
    </div>
    <div class="mainContent">
        Sign up for our monthly newsletter.<br />
        <asp:TextBox ID="txtSubscription" runat="server" CssClass="msapTxt"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Unsubscribe" CssClass="msapBtn" OnClick="OnSubscribe" /><br />
    </div>
</asp:Content>
