﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class webinar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            LoadEvents(0);
    }
    private static int CompareEventByDate(MagnetPublicEvent x, MagnetPublicEvent y)
    {
        DateTime dt1 = new DateTime();
        DateTime dt2 = new DateTime();
        if (DateTime.TryParse(x.Date, out dt1) && DateTime.TryParse(y.Date, out dt2))
        {
            return dt1.CompareTo(dt2);
        }
        else
            return 0;
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadEvents(e.NewPageIndex);
    }
    private void LoadEvents(int PageNumber)
    {
        int count = 0;
        var webniars = MagnetPublicEvent.Find(x => x.EventType == false && x.Display == true).ToList();
        webniars.Sort(CompareEventByDate);
                List<ManageUtility.PublicationData> data = new List<ManageUtility.PublicationData>();

        foreach (MagnetPublicEvent webniar in webniars)
        {
            StringBuilder sb = new StringBuilder();
            count++;
            sb.AppendFormat("<a name='event{3}'></a><h2>{0}</h2> <p><strong>Organization</strong>: {1}<br /> <strong>Date</strong>: {2}<br />", webniar.EventTitle, webniar.Organization, webniar.Date, count);
            sb.AppendFormat("<strong>Time</strong>: {0}<br /><strong>Registration</strong>: {1}<br /><strong>Description</strong>: {2}</p>", webniar.Time, webniar.Registration, webniar.Description);
            ManageUtility.PublicationData display = new ManageUtility.PublicationData();
            display.DisplayData = sb.ToString();
            data.Add(display);
        }
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
}