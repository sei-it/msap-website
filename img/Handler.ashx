﻿<%@ WebHandler Language="VB" Class="Handler" %>
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web
Imports System.Configuration
Imports System

Public Class Handler
	Implements IHttpHandler

	Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
		Get
			Return True
		End Get
	End Property

	Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
		' Set up the response settings
        context.Response.ContentType = "image/jpeg"
		context.Response.Cache.SetCacheability(HttpCacheability.[Public])
		context.Response.BufferOutput = False

		Dim photoId As Integer = -1
        Dim carId As Integer = -1
        Dim CompassId As Integer = -1
        Dim gcId As Integer = -1
		Dim stream As Stream = Nothing

		If context.Request.QueryString("PhotoID") IsNot Nothing AndAlso context.Request.QueryString("PhotoID") <> "" Then
            photoId = CType(context.Request.QueryString("PhotoID"), Integer)
			stream = GetPhoto(photoId)
        Else If context.Request.QueryString("CarouselID") IsNot Nothing AndAlso context.Request.QueryString("CarouselID") <> "" Then
            carId = CType(context.Request.QueryString("CarouselID"), Integer)
            stream = GetCarousel(carId)
        ElseIf context.Request.QueryString("CompassID") IsNot Nothing AndAlso context.Request.QueryString("CompassID") <> "" Then
            CompassId = CType(context.Request.QueryString("CompassID"), Integer)
            stream = GetCompass(CompassId)
        ElseIf context.Request.QueryString("GCID") IsNot Nothing AndAlso context.Request.QueryString("GCID") <> "" Then
            gcId = CType(context.Request.QueryString("GCID"), Integer)
            stream = GetGC(gcId)
        End If


		Const  buffersize As Integer = 1024 * 16
		Dim buffer As Byte() = New Byte(buffersize - 1) {}
		Dim count As Integer = stream.Read(buffer, 0, buffersize)
		While count > 0
			context.Response.OutputStream.Write(buffer, 0, count)
			count = stream.Read(buffer, 0, buffersize)
		End While
	End Sub

	Public Function GetPhoto(photoId As Integer) As Stream
        Dim myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("MagnetServer").ConnectionString)
        Dim myCommand As New SqlCommand("SELECT images FROM PastedWebinars WHERE id=@PhotoID", myConnection)
        myCommand.CommandType = CommandType.Text
        myCommand.Parameters.Add(New SqlParameter("@PhotoID", photoId))
        myConnection.Open()
       
        
        Dim result As Object = myCommand.ExecuteScalar()

        Try
            Return New MemoryStream(DirectCast(result, Byte()))
        Catch e As ArgumentNullException
            Return Nothing
        Finally
            myConnection.Close()
        End Try
    End Function

    Public Function GetCarousel(carId As Integer) As Stream
        Dim myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("MagnetServer").ConnectionString)
        Dim myCommand As New SqlCommand("SELECT image FROM MagnetCarousel WHERE id=@CarID", myConnection)
        myCommand.CommandType = CommandType.Text
        myCommand.Parameters.Add(New SqlParameter("@CarID", carId))
        myConnection.Open()
       
        
        Dim result As Object = myCommand.ExecuteScalar()

        Try
            Return New MemoryStream(DirectCast(result, Byte()))
        Catch e As ArgumentNullException
            Return Nothing
        Finally
            myConnection.Close()
        End Try
    End Function

    Private Function GetCompass(CompassId As Integer) As Stream
        Dim myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("MagnetServer").ConnectionString)
        Dim myCommand As New SqlCommand("SELECT Newsletterimage FROM magnetCompass WHERE id=@CompID", myConnection)
        myCommand.CommandType = CommandType.Text
        myCommand.Parameters.Add(New SqlParameter("@CompID", CompassId))
        myConnection.Open()
       
        
        Dim result As Object = myCommand.ExecuteScalar()

        Try
            Return New MemoryStream(DirectCast(result, Byte()))
        Catch e As ArgumentNullException
            Return Nothing
        Finally
            myConnection.Close()
        End Try
    End Function

    Private Function GetGC(gcId As Integer) As Stream
        Dim myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("MagnetServer").ConnectionString)
        Dim myCommand As New SqlCommand("SELECT image FROM MagnetGCImages WHERE id=@gcID", myConnection)
        myCommand.CommandType = CommandType.Text
        myCommand.Parameters.Add(New SqlParameter("@gcID", gcId))
        myConnection.Open()
       
        
        Dim result As Object = myCommand.ExecuteScalar()

        Try
            Return New MemoryStream(DirectCast(result, Byte()))
        Catch e As ArgumentNullException
            Return Nothing
        Finally
            myConnection.Close()
        End Try
    End Function

End Class