﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Technical Assistance Webinar</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 14px;
    font-weight: bold;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}

#download_list li{
	padding-bottom:10px;
}
-->
</style></head>
<body class="oneColElsCtrHdr">

<div id="container">
  <div id="header">
    <img src="Webinar_docs/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  <div id="mainContent">
    <h1>Program Sustainability Resources</h1>
   
   <table width="85%" border="0">
   		<tr>
 			<td style="text-align:right;vertical-align:top;">
            	<img src="Webinar_docs/Program_sustainability_webinar_6_6_12/PS_webinar_img4.jpg" style="border:2px #2274A0 solid;" />
            </td>
   			<td style="vertical-align:top;padding-left:15px;">
             <div><span id="subtitle">Planning for Sustainability: Putting it All Together</span><br/>
 Thank you for registering for the fourth webinar in the MSAP Center's Planning for Sustainability series. It will be held on June 6, 2012, from 1:00 p.m. to 2:00 p.m. Eastern time. 

<p>This webinar will bring together information from the entire webinar series. Topics will include understanding the sustainability planning process; adapting that process to your unique goals and needs; and writing a sustainability plan that is clear, compelling, and convincing. </p>

   Here are the tools for this webinar: 
   </div>
            <ol id="download_list">
                <li><strong>Workplan for Sustainability Planning</strong>: Use this worksheet as a guide to completing the tasks involved in sustainability planning. <a href="Webinar_docs/Program_sustainability_webinar_6_6_12/Sustainability_Planning_Workplan_final.docx" target="_blank">Download</a>
              </li>
                <li><strong>Sustainability Plan Sample Outline</strong>: This document outlines the information that should be included in your written sustainability plan and includes tips for writing each section. 
                <a href="Webinar_docs/Program_sustainability_webinar_6_6_12/Sustainability_Plan_Outline_MSAP_final.docx" target="_blank">Download</a>
                </li>
                
            </ol>
            </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
     </table>
	<!-- end #mainContent --></div>
  <div id="footer">
    <img src="Webinar_docs/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>
</body>
</html>
