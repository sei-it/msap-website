﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Homepage
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>


<script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1)
    </script>

<script type="text/javascript">

    var feedcontainer = document.getElementById("rssid");
    var feedurl = "http://www.ed.gov/feed/free/free-rss.xml";
    var feedlimit = 5;
    var rssoutput = "";

    function rssfeedsetup() {
        var feedpointer = new google.feeds.Feed(feedurl);  //Google Feed API method
        feedpointer.setNumEntries(feedlimit);  //Google Feed API method
        feedpointer.load(displayfeed);  //Google Feed API method
    }

    function displayfeed(result) {
        if (!result.error) {
            var thefeeds = result.feed.entries
            for (var i = 0; i < thefeeds.length; i++) {
                rssoutput += "<b><a href='" + thefeeds[i].link + "'>" + thefeeds[i].title + "</a></b><br/>";
                rssoutput += thefeeds[i].contentSnippet + "<br/>";
            }

            if (feedcontainer == null)
                feedcontainer = document.getElementById("rssid");
            feedcontainer.innerHTML = rssoutput;
        }
        else
            alert("Error fetching feeds!");
    }

    $(document).ready(function() {
    window.onload = function () {
        rssfeedsetup();
    }
});
 </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="brnImg">
        <script type="text/javascript">            bnrInit();</script>
    </div>
    <a name="skip"></a>
    <div class="msapHP_Content">

                    <div class="granteeCorner">
            <div class="gcTop">
                &nbsp;</div>
            <div class="gcContent">
              <div class="gcTxt">
                    <h1>
                    <a href="cornerarchives.aspx" style="text-decoration: none; color: #0060b7;">
                      Grantee<br />
                        Corner</a></h1>
                  <p>
                        <strong><asp:Literal ID="ltlTitle" runat="server"></asp:Literal></strong>
                  <p>
                        
                        <asp:Literal ID="ltlImageHolder" runat="server" />
                      <br />
                        <small><asp:Literal ID="ltlCaption" runat="server" /></small><br /></p>
        
    
                      <asp:Literal ID="ltlAbsText" runat="server"></asp:Literal>
       
</div>
                <!-- /gcTxt -->

            </div>
            <!-- /gcContent -->
            <div class="gcBot">
                &nbsp;</div>
            <p style="margin-top: 0; margin-bottom: 0;">&nbsp;
                
            </p>
             <!-- temporarily replace  -->
                      <%--  <div class="gcTop">
                &nbsp;</div>--%>
            <%--<div class="gcContent" style="padding: 6px;">
        		<div class="gcTxt">
                        <img src="images/TAtraininglog.png" border="0"  style="float: left; margin-right: 10px;
                            margin-bottom: 0px; width: 148px;" 
                            alt="post-it, written Magnet Connector" /> <span style="color: #1d5ba9;
                                font-size: 1.4em; line-height: 20px;">Fall Technical Assistance & Training Conference</span><br />
                        <p><b>Date:</b> October 12-14, 2014<br/>
                        <b>Location:</b> Baltimore, MD</p>
                     Learn how to create, sustain and improve magnet schools. Sessions will also focus on magnet best practices in marketing & recruitment, budget and leadership needs.
                    <a href="http://www.msapcenter.com/conference.aspx" target="_blank">Learn more</a>
                </div>

                <!-- /gcTxt -->
            </div>--%>
            <!-- /gcContent -->
<%--            <div class="gcBot">
                &nbsp;</div>--%>
             <!-- temporarily replace  -->
              <div class="gcTop">
                &nbsp;</div>
            <div class="gcContent" style="padding: 6px;">
        		<div class="gcTxt">
                    <a href="admin/messageboard/default.aspx">
                        <img src="images/MagnetConnector.png" border="0" width="56" style="float: left; margin-right: 10px;
                            margin-bottom: 0px;" alt="post-it, written Magnet Connector" /></a> <span style="color: #1d5ba9;
                                font-size: 1.4em; line-height: 20px;">Magnet Connector</span><br /><br/><br/>
                     MSAP grantees get together with one another to talk about magnet topics.
                    <a href="admin/messageboard/default.aspx">Learn more</a>
                </div>

                <!-- /gcTxt -->
            </div>
            <!-- /gcContent -->
            <div class="gcBot">
                &nbsp;</div>
            <p style="margin-top: 0; margin-bottom: 0;">&nbsp;
                
            </p>
            <div class="gcTop">
                &nbsp;
                </div>
            <div class="gcContent" style="padding: 6px;">
             
              <div class="gcTxt">
                    <a href="login.aspx?ReturnUrl=admin/TA_courses.aspx" ><img src="images/Puzzle_icon.png" decoration: "none" alt="puzzle icon" width="55" style="margin-right: 10px;
                        margin-bottom: 10px; float: left;" border="0" /></a><span style="color: #1d5ba9; font-size: 1.4em;
                            line-height: 20px;">Theme Integration Courses</span>
                    <p style="margin-top: 15px; margin-bottom: 0;">
                    
                        MSAP grantees can use these online courses to support schoolwide work... <a href="login.aspx?ReturnUrl=admin/TA_courses.aspx">Learn more</a>
                    </p>
                </div>
                <!-- /gcTxt -->
            </div>
            <!-- /gcContent -->
            <div class="gcBot">
                &nbsp;</div>
        </div>
        <!-- /granteeCorner -->
        <div class="msapHP_colR">
          <!-- <div class="FY2013">
            <span class="welcomeTxt">FY 2013 MSAP <br/>Grant Competition</span>
            <p>
            The U.S. Department of Education announced the Magnet Schools Assistance Program grant competition on December 31, 2012.  </p>
             
            <strong>Important Dates</strong>
            <ul style="margin-top:0px;padding-left:10px;">
            <li>Preapplication Webinar: January 17, 2013, 1:00-4:00 p.m. EST </li>
            <li>Deadline for Notice to Apply: January 30, 2013</li>
            <li>Deadline for Transmittal of Applications: March 1, 2013</li>
            </ul>
            
            <strong>Call for Reviewers</strong><br/>
            The Department encourages individuals from various backgrounds and professions with content expertise to apply to be peer reviewers for the FY 2013 MSAP Competition. 

<p><img src="img/FY2013Icon.jpg" alt="" width="65" style="float:right;"/>Download information and <br/><a href="http://www2.ed.gov/programs/magnet/index.html#info" target="_blank">learn more</a></p>

                     
            </div>-->
            <h1>
                <span style="font-family: Helvetica, Arial, sans-serif;">Recent News</span></h1>
            <div class="rss" style="overflow: hidden;">
                <ul style="margin-top: 2px;">
                    <asp:Repeater ID="newsRepeater" runat="server">
                        <ItemTemplate>
                            <li><strong><a href='newsdetail.aspx?id=<%#DataBinder.Eval(Container.DataItem,"ID")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Title")%></a></strong><br />
                                <%#DataBinder.Eval(Container.DataItem, "Source")%>.
                                <%#DataBinder.Eval(Container.DataItem, "Author")%>.
                                <%#DataBinder.Eval(Container.DataItem, "Date")%>. </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
								
            </div><a href="news.aspx"><span style="color: #F58220">Read more magnet school news</span></a>
                    <!-- /rss  -->
                    <div style="display: table; height: auto; width: 100%; padding-top: 10px;">
                        <h1>
                            <a href="http://www.ed.gov"><img src="images/Edgov.gif" style="float: left; margin-right: 6px;" alt="Ed.gov" border="0"/></a>
                                <div style="float: left; margin-top: 4px; font-family: Georgia, 'Times New Roman', Times, serif;">
                                | Updates</div>
                        </h1>
                    </div>
                    <!-- /style -->
                    <div class="rss" id="rssid">
                      
                    </div>
                    <!-- /rss  -->
                    <p style="margin-top: 0; padding-top: 4px;">
                        <img src="images/rss.gif" alt="rss icon" width="21" height="20" style="vertical-align: middle;
                            margin-right: 4px;" />RSS provided by <a href="http://www.ed.gov" target="_blank">http://www.ed.gov</a></p>
        </div>
        <!-- /msapHP_colR  -->
        
        <div class="msapHPcolL">
  <%--      <img src="img/MSAP_NatlSchlsMonth_banner.png" alt="National Magnet Schools Month icon" />
        <span class="welcomeTxt">Magnet Schools Assistance Program Technical Assistance Center</span><br />
        <p>
Welcome! Please join the <b>Magnet Schools Assistance Program Technical Assistance Center (MSAP Center)</b> in celebrating February as National Magnet Schools Month. Please browse our resources page to find articles, tool kits, and newsletters about best practices and innovative approaches that can help you learn more about magnet schools and promote them in your community.
</p>
<p>
The MSAP Center supports the U.S. Department of Education Office of Innovation and Improvement as it provides students with equity and choice in public school education through the Magnet Schools Assistance Program (MSAP).
</p> --%>

            <p>
                <span class="welcomeTxt">Magnet Schools Assistance Program Technical Assistance Center</span><br />
                <br />
                Welcome! The <strong>Magnet Schools Assistance Program Technical Assistance Center (MSAP
                    Center)</strong> supports the U.S. Department of Education Office of Innovation and Improvement as it provides students with equity and choice in public school education through the Magnet Schools Assistance Program (MSAP). 
            </p>
            <p>
                The MSAP Center is a technical assistance resource for MSAP grantees and the greater magnet school community. It offers tools, information, and strategies to assist in planning, implementing, and revising magnet school programs. The MSAP Center’s resources and tailored support services focus on diversity, academic excellence, and equity.</p>
            <p>
                Please browse our resources page to find articles, tool kits, and newsletters about best practices and innovative approaches. 
            </p>
             <div class="msapHP_blurbs">
                <h1>The Quarterly Newsletter</h1>
                <p>
                 <asp:Literal ID="ltlCompass" runat="server" />
                </p>
                <div class="past_upcomingpgs">
                    View past newsletters <a href="newsletters.aspx">
                    <img src="images/book_icon.jpg" alt="three books" /></a>

            	</div>
            </div>

            <div class="msapHP_blurbs">
            
              <h1>Upcoming Conference</h1>
                    <a href="conference.aspx">
                    		<img src="images/conferenceIcon2.jpg" alt="upcoming conference" border="0" style="float: left; margin-right: 10px; 
                            margin-bottom: 0px;" />
                        </a>
          
             <p id="webConferenceDiv" runat="server"></p>
               <div class="past_upcomingpgs">
                    View all upcoming conferences <a href="conference.aspx"><img src="images/staricon.jpg" alt="star icon" /></a>
                </div>
            </div>
            <!-- /msapHP_blurbs -->
           
            <!-- /msapHP_blurbs -->
              
            <div class="msapHP_blurbs_last">
                <h1>Upcoming Webinar</h1>
                <a href="webinar.aspx">
                    <img src="images/webinarIcon2.jpg" alt="upcoming webinars" style="float: left; margin-right: 10px;margin-bottom: 0px;" border="0"/></a>
                   
                <div id="webinarDiv" runat="server">
                </div>
                <div class="past_upcomingpgs">
                    View all upcoming webinars <a href="webinar.aspx"><img src="images/staricon.jpg" alt="star icon" /></a>

                </div>
            </div>
            <!-- /msapHP_blurb -->
        </div>
        <!-- /msapHPcolL  -->
    </div>
    <!-- /msapHP_Content -->
</asp:Content>
