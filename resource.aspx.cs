﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.Documents;
using Lucene.Net.Store;
using Lucene.Net.Search;
using System.Data;
using System.Data.SqlClient;
using Synergy.Magnet;
using System.Text;
using System.Linq.Dynamic;

public partial class resource : System.Web.UI.Page
{
   List<ManageUtility.PublicationData> data = new List<ManageUtility.PublicationData>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
        }
    }

    protected void OnClearSearch(object sender, EventArgs e)
    {
        txtKeywords.Text = "";
        ltlPagingSummary.Text = "";
        ddlTypeResource.SelectedIndex = -1;
        LboxTopics.SelectedIndex = -1;
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void OnSearch(object sender, EventArgs e)
    {
        string strTopics = "";

        foreach (ListItem li in this.LboxTopics.Items)
        {
            strTopics += li.Selected ? "'" + li.Value + "'," : "";
        }

        if (!string.IsNullOrEmpty(strTopics))
            strTopics = strTopics.Substring(0, strTopics.Length - 1);

        data = ManageUtility.SearchKeyword(txtKeywords.Text.Trim(), ddlTypeResource.SelectedValue.Trim(), strTopics);
        if (data.Count == 0)
            ltlPagingSummary.Text = "";
        GridView1.DataSource = data;
        GridView1.PageIndex = 0;
        GridView1.DataBind();
        
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }

    private void LoadData(int page)
    {
        string strTopics = "";
       
        foreach (ListItem li in this.LboxTopics.Items)
        {
            strTopics += li.Selected ? "'" + li.Value + "',":"";
        }

        if (!string.IsNullOrEmpty(strTopics))
            strTopics = strTopics.Substring(0, strTopics.Length - 1);

        data = ManageUtility.SearchKeyword(txtKeywords.Text.Trim(), ddlTypeResource.SelectedValue.Trim(), strTopics);
        GridView1.DataSource = data;
        GridView1.PageIndex = page;
        GridView1.DataBind();


    }
    private void SearchByCategory(int Page)
    {
    //    List<ManageUtility.PublicationData> retData = new List<ManageUtility.PublicationData>();
    //    MagnetDBDB db = new MagnetDBDB();
    //    var data = (from m in db.MagnetPublications
    //                from n in db.MagnetResourceCategories
    //                where m.ID == n.ResourceID
    //                && n.CategoryID == Convert.ToInt32(ddlTopics.SelectedValue)
    //                orderby m.PublicationDate
    //                select m ).ToList();
    //    data.Sort(ManageUtility.ComparePublicationByDate);

    //    foreach (var publication in data)
    //    {
    //        string str = string.Format("<b>{0}</b><br /><b>Organization</b>: {1}<br /><b>Date</b>: {2}<br /><b>Description</b>: {3}",
    //            publication.PublicationTitle,
    //            publication.Organization,
    //            publication.PublicationDate,
    //            publication.Description);

    //        if (!string.IsNullOrEmpty(publication.OriginalFileName))
    //        {
    //            str += "<br /><a href='applicationdoc/" + publication.PhysicalFileName + "' target='_blank'><img border=0 src='images/PDFlogo.jpg'> <b>Download PDF</b></a>";
    //        }

    //        retData.Add(new ManageUtility.PublicationData()
    //        {
    //            ID = publication.ID,
    //            DisplayData = str
    //        });
    //    }
    //    GridView1.DataSource = retData;
    //    GridView1.PageIndex = Page;
    //    GridView1.DataBind();
    //}
    //protected void OnTopicChanged(object sender, EventArgs e)
    //{
    //    txtKeywords.Text = "";
    //    if (ddlTopics.SelectedIndex > 0)
    //    {
    //        hfSelection.Value = "3";
    //        SearchByCategory(0);
    //    }
    //    else
    //    {
    //        GridView1.DataSource = null;
    //        GridView1.DataBind();
    //    }
    //}
    //protected void OnClearSearch(object sender, EventArgs e)
    //{
    //    txtKeywords.Text = "";
    //    GridView1.DataSource = null;
    //    GridView1.DataBind();
    //}
    //protected void OnSearch(object sender, EventArgs e)
    //{
    //    //hfKeyword.Value = txtKeywords.Text;

    //    //if (rblKeywordType.SelectedIndex == 0)
    //    //{
    //    //    hfSelection.Value = "0";
    //    //    GridView1.DataSource = ManageUtility.SearchAllKeyword(txtKeywords.Text);
    //    //    GridView1.DataBind();
    //    //}
    //    //else
    //    //{
    //    //    hfSelection.Value = "1";
    //        GridView1.DataSource = ManageUtility.SearchAnyKeyword(txtKeywords.Text);
    //    //    GridView1.DataBind();
    //    //}
    //}
    //protected void OnResourceTypeChanged(object sender, EventArgs e)
    //{
    //    /*if (ddlResrouceType.SelectedIndex > 0)
    //    {
    //        hfSelection.Value = "2";
    //        hfKeyword.Value = ddlResrouceType.SelectedValue.ToLower();
    //        GridView1.DataSource = ManageUtility.SearchPublicationType(ddlResrouceType.SelectedValue.ToLower());
    //        GridView1.DataBind();
    //    }
    //    else
    //    {
    //        GridView1.DataSource = null;
    //        GridView1.DataBind();
    //    }*/
    //}
    //protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    LoadData(e.NewPageIndex);
    //}
    //private void LoadData(int page)
    //{
    //    switch (hfSelection.Value)
    //    {
    //        case "0":
    //            GridView1.DataSource = ManageUtility.SearchAllKeyword(hfKeyword.Value);
    //            GridView1.PageIndex = page;
    //            GridView1.DataBind();
    //            break;
    //        case "1":
    //            GridView1.DataSource = ManageUtility.SearchAnyKeyword(hfKeyword.Value);
    //            GridView1.PageIndex = page;
    //            GridView1.DataBind();
    //            break;
    //        case "2":
    //            GridView1.DataSource = ManageUtility.SearchPublicationType(hfKeyword.Value);
    //            GridView1.PageIndex = page;
    //            GridView1.DataBind();
    //            break;
    //        case "3":
    //            SearchByCategory(page);
    //            break;

    //    }
    }
    private void LuceneSearch()
    {
        /*Directory dir =  FSDirectory.GetDirectory(Server.MapPath("index"), true);

        //create an index searcher that will perform the search
        IndexSearcher searcher = new IndexSearcher(dir);

        //build a query object
        Term searchTerm = new Term("Keywords", txtKeywords.Text);
        Query query = new TermQuery(searchTerm);

        //execute the query
        Hits hits = searcher.Search(query);

        //iterate over the results.
        for (int i = 0; i < hits.Length(); i++)
        {
            Document doc = hits.Doc(i);
            string contentValue = doc.Get("content");
            Console.WriteLine(contentValue);
        }*/
    }
    private void CreateIndex()
    {
        /*IndexWriter writer = new IndexWriter(Server.MapPath("index"), new StandardAnalyzer(), true);
        foreach (MagnetPublication publication in MagnetPublication.All())
        {
            Document doc = new Document();
            doc.Add(new Field("PublicationName", publication.PublicationName, Field.Store.YES, Field.Index.TOKENIZED));
            doc.Add(new Field("Organization", publication.Organization, Field.Store.YES, Field.Index.TOKENIZED));
            doc.Add(new Field("PublicationDate", publication.PublicationDate, Field.Store.YES, Field.Index.TOKENIZED));
            doc.Add(new Field("Authors", publication.Authors, Field.Store.YES, Field.Index.TOKENIZED));
            doc.Add(new Field("Authors", publication.Authors, Field.Store.YES, Field.Index.TOKENIZED));
            doc.Add(new Field("Keywords", publication.PublicationKeyword, Field.Store.YES, Field.Index.TOKENIZED));
            writer.AddDocument(doc);
        }
        writer.Optimize();
        writer.Close();*/
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            int rcdtotal = data.Count();
            int pageFrom = (GridView1.PageIndex) * GridView1.PageSize + 1;
            int pageTo = (GridView1.PageIndex + 1) * GridView1.PageSize > rcdtotal ? rcdtotal : (GridView1.PageIndex + 1) * GridView1.PageSize;


            ltlPagingSummary.Text = "Display " + pageFrom + " - " + pageTo + " of " + rcdtotal + " ";

        }
    }
}