﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
<div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Tucson Unified School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
         <br />
            <br/>
            <img src="../images/gc102012/Robison_student_art_class2.JPG" alt="A Robison Magnet student draws during art class." width="200" /><br />
            <small>Robison Magnet student enjoys art class.</small><br />
            <br />
         <img src="../images/gc102012/Ochoa_students_in_garden.JPG" alt="Ochoa Community Magnet students work in the school’s garden." width="200" /><br />
            <small>Ochoa Community Magnet students explore the garden during class.</small><br />
            <br />
         
            <img src="../images/gc102012/Safford_students_learning_math.JPG" alt="Two Safford K-8 Magnet students learn math with their teacher using blocks." width="200" /><br />
            <small>Safford K-8 Magnet students learn math.</small><br />
            <br />
            
               <img src="../images/gc102012/Vicky_Callison.jpg" alt="Victoria Callison, Tucson Unified School District Project Director." width="200" /><br />
            <small>Victoria Callison, <br/>Tucson Unified School District <br/>Project Director.</small><br />
            <br />
            
      </div>
     <p>Tucson Unified School District in southern Arizona is the second-largest district in the state. Established in 1867, the district is also one of the oldest. Over the past 10 years, the population in Tucson has shifted dramatically. With more than 52,000 students, and 73 percent minority, Tucson Unified faces funding and state mandates that increase accountability for English language learners.</p>
     
<p>As a community, Tucson celebrates its rich cultural diversity. This is evident in the three schools that receive Magnet Schools Assistance Program (MSAP) funding. Safford K-8 Magnet and Robison Magnet are International Baccalaureate Candidate schools. Drawing from the diverse community, each school embraces a true global perspective. Ochoa Community Magnet is a Reggio Emilia-inspired K-5 program; its curriculum uses a constructivist approach to build on the child’s experiences. </p>

<p>Student learning is at the forefront of each program. Intense professional development and a focus on quality instruction propel all three schools as they experience academic growth. The rigorous approach to improving instruction attracts new students to the district, brings back students who have tried charter schools, and energizes the community with the promise of successful, innovative education. </p>
<p>Tucson’s magnet program has changed the school landscape. Students engage in deeper and richer learning experiences and learn to appreciate other cultures. Teachers demonstrate high expectations for all students and use technology to enrich the curriculum. Parents feel valued and are more involved in the schools. Through the program, students, parents, and teachers are better prepared to live and work in an increasingly diverse world.</p>
<p>Victoria Callison, the Project Director, has more than 30 years of experience in education. Her driving passion lies in bringing about change through professional development and innovative instruction. An adjunct professor at several colleges and universities and a Tucson native, she believes in the power of magnet programs to contribute to the educational prosperity of the community.</p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
