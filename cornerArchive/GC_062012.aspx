﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
   <div class="mainContent">
  <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Metropolitan Nashville Public Schools</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <br />
         <img src="../images/gc062012/Stratford.jpg" alt="Students at Stratford STEM Magnet High School work in a science lab." width="200" /><br />
            <small>Students at Stratford STEM Magnet High School work in a science lab.</small><br />
            <br />
         <img src="../images/gc062012/PearlCohn.jpg" alt="An Audio Technology teacher helps a student with computerized recording equipment at Pearl-Cohn Entertainment Magnet High School." width="200" /><br />
            <small>An Audio Technology teacher helps a student at Pearl-Cohn Entertainment Magnet High School.</small><br />
            <br />
         <img src="../images/gc062012/AlanCoverstone.jpg" alt="Alan Coverstone, Metropolitan Nashville Public Schools Project Director" width="200" /><br />
            <small>Alan Coverstone,<br/> Metropolitan Nashville Public Schools <br/>Project Director</small><br />
            <br />
      </div>
      <p>Metropolitan Nashville Public Schools (MNPS), with more than 79,000 students and 144 schools, is the 48th-largest urban school district in the nation and one of the most diverse districts in the region. MNPS offers a variety of learning experiences and opportunities to meet the needs of its students, many of whom come from economically disadvantaged families, and who represent more than 120 languages and cultures. The district has many students with exceptional needs, and students overall have a wide mix of auditory, visual, and tactile learning styles. The district embraces its diversity—in ethnicity, interests, and learning styles—allowing families to choose when, where, and how their students learn.</p>

<p>The Magnet Schools Assistance Program (MSAP) grant supports the addition of three themes to an already robust magnet program: (1) Museum magnets, which serve grades K-8; (2) Tennessee’s only K-12 science, technology, engineering, and mathematics (STEM) magnet pathway; and (3) Nashville’s first 9-12 entertainment magnet program. Each school offers a unique and enriched curriculum capable of attracting students from across the county, furthering the district’s efforts to offer programs that are specific to the needs and wants of students. The goal is for every child to have a great educational experience and graduate prepared for college, career, and life.</p>

<p>Community partnerships within each of the new magnet schools enhance real-world application of each theme. MNPS is fortunate to be able to partner with the top companies in the region, utilizing the most current technologies to provide students of all abilities with hands-on activities and projects that are based on research and inquiry.  Community partners include Adventure Science Center, BMI (Broadcast Music, Inc.), Country Radio Broadcasters, Frist Center for the Visual Arts, and Vanderbilt Center for Science Outreach.</p>

<p>Alan Coverstone, the MNPS Executive Director of Innovation and project director for the MSAP grant, is responsible for dramatic restructuring and rapid turnaround. He has been called a “relentless champion of excellence in education,” and his leadership and vision have become the heart of the district’s innovative and multifaceted approach to school turnaround. 
</p>
      <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
