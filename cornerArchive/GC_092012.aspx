﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
<div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Houston Independent School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <br />
         <img src="../images/gc092012/Covey_Leadership_Training_at_Garden_Oaks_Elementary.JPG" alt="Garden Oaks Elementary Montessori Magnet School teachers sit around tables while attending Covey leadership training." width="200" /><br />
            <small>Garden Oaks Elementary Montessori <br/>Magnet School teachers attend Covey <br/>leadership training.</small><br />
            <br />
         
            <img src="../images/gc092012/Dodson_Elementary_Montessori_Theme_Mural_Project.JPG" alt="Dodson Elementary Montessori Magnet School artist-in-residence stands beneath the large mural of two children she painted for the school to exemplify the Montessori theme." width="200" /><br />
            <small>Dodson Elementary Montessori Magnet School artist-in-residence stands beneath the mural she painted for the school to exemplify the Montessori theme.</small><br />
            <br />
            
               <img src="../images/gc092012/Garden_Oaks_Elementary_Environmental_Science_in_the_Greenhouse.jpg" alt="Garden Oaks Elementary Montessori Magnet School students pose in a  greenhouse during environmental science class." width="200" /><br />
            <small>Garden Oaks Elementary Montessori Magnet School students study environmental science in the greenhouse.</small><br />
            <br />
         <img src="../images/gc092012/Garden_Oaks_Elementary_Montessori_Family_Night.JPG" alt="Garden Oaks Elementary Montessori Magnet School students display art projects they completed with their father during Family Night." width="200" /><br />
            <small>Garden Oaks Elementary Montessori Magnet School students complete an art project with their father during Family Night.</small><br />
            <br />
            <img src="../images/gc092012/Hinojosa.jpg" alt="Lupita Hinojosa, Houston Independent School District Project Director" width="200" /><br />
            <small>Lupita Hinojosa, <br/>Houston Independent School District<br/> Project Director</small><br />
            <br />
            
      </div>
     <p>As the largest school district in Texas, and the seventh largest in the nation, the Houston Independent School District (HISD) boasts an ethnically and socioeconomically diverse enrollment of more than 203,000 students. Its 279 schools include 6 early childhood centers and 160 elementary, 41 middle, 44 high, and 28 combined/other schools.</p>

<p>In 2010, the HISD Board of Education took proactive steps to prepare students for an ever-changing international market by applying for the Magnet Schools Assistance Program (MSAP) grant for five HISD schools. </p>

<p>HISD strives to have the MSAP schools mirror community diversity. Academic achievement will be increased by involving parents and the community in education, offering specialized training for teachers, and infusing state-of-the-art equipment and technology, thus providing innovation in instruction for students. </p>

<p>Dodson Elementary Montessori Magnet School, one of the district’s original magnet programs, is being renewed through extensive Montessori training for teachers, which creates increased program offerings. Classrooms have been augmented with improved technology and new Montessori student materials, facilitating rapid expansion of the program to include the whole school.</p>

<p>Garden Oaks Elementary Montessori Magnet School, a new magnet program, focuses on environmental science. In addition to Montessori training and updated classrooms, this unique program thrives because of its outdoor classrooms for students and parents, renewable energy resources, and ornithology equipment.</p> 

<p>Whidby Elementary has a new magnet theme focusing on leadership and medical health science. Whidby’s collaboration with Baylor College of Medicine provides rigorous teacher training in the latest health science curriculum for elementary students. Student projects include schoolwide dental screenings, cancer awareness events, and community health fairs. The leadership component enhances this exciting program, which aims to build future leaders in the medical field.</p>

<p>Fondren Middle School has a revised magnet theme; it is  an International Baccalaureate (IB) Middle Years Programme Candidate. IB training for teachers and administrators has greatly enhanced the core academic program while building international awareness in students, who also participate in leadership and character education, urban gardening, mural projects, and an afterschool apprenticeship program.</p>  
<p>Jones High School is a new magnet program, with STEM academies in architecture, biotechnology, and engineering. Hands-on cross-curricular STEM training supplements core content instruction. Biotechnology students partner with Challenger 7 Memorial Park to implement service-learning projects with elementary and middle school students, exposing urban students to environmental wetlands within their community.</p>

<p>As the Assistant Superintendent of School Choice and MSAP Project Director, Dr. Lupita Hinojosa works closely with magnet school leaders to develop, advance, and sustain quality magnet programs. With the funding from the MSAP grant and the vision of the HISD Board of Trustees, the Office of School Choice strives to ensure equity in access to high-quality programs for all students.

</p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
