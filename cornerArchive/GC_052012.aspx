﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
   <div class="mainContent">
  <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Tangipahoa Parish School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <br />
            <img src="../images/gc_052012/Amite_Elementary_students.jpg" alt="Amite Elementary students wait on set in preparation for the morning broadcast." width="200" border="0"  /><br />
        <small>Amite Elementary students prepare for the morning broadcast.</small>
          <br />
            <br />
        <img src="../images/gc_052012/Kentwood_High_students.jpg" alt="Kentwood High students play saxophone and trumpet during a rehearsal for an upcoming jazz concert." width="200" /><br />
            <small>Kentwood High students rehearse for an upcoming jazz concert.</small><br />
            <br />
            
         <img src="../images/gc_052012/Career_Education_Center_students.jpg" alt="Career Education Center students construct electrical circuits and connect them to light bulbs." width="200" /><br />
            <small>Career Education Center students learning to connect electrical circuits.</small><br />
            <br />
         <img src="../images/gc_052012/Hammond_High_School.jpg" alt="A Hammond High School student learns rehabilitation techniques by monitoring another student working out in the school’s state-of-the-art sports medicine health care training facility." width="200" /><br />
            <small>Hammond High School student learns rehabilitation techniques in the school’s <br/>state-of-the-art sports medicine health care training facility.</small><br />
            <br />
         <img src="../images/gc_052012/AlisonAndrews.jpg" alt="Alison Andrews, Tangipahoa Parish School District Project Director" width="200" /><br />
            <small>Alison Andrews, <br/>Tangipahoa Parish School District <br/>Project Director</small><br />
            <br />
      </div>
      <p>Tangipahoa Parish School District (TPSD) consists of 35 elementary, middle, and high schools staffed by 2,600 employees who 
      serve 19,616 students. A long, narrow, rural district in southeast Louisiana, TPSD stretches 50 miles from its north end, near the 
      Mississippi state line, to its south end, near Lake Pontchartrain, which is referred to as the “north shore” by New Orleans 
      residents. TPSD strives for each student to achieve at his or her maximum intellectual capacity and to become an independently 
      contributing, responsible member of society.</p>
      
      <p>The Magnet Schools Assistance Program (MSAP) grant at TPSD supports 10 magnet programs, which range from communication and fine 
      arts to medicine and industry.</p>
      
      <p>Three magnet programs—one elementary, one junior high, and one high school—are “International Baccalaureate (IB) World Schools” 
      candidates. In addition to being IB candidates, the elementary school supports a fine and performing arts program and the high 
      school supports a sports medicine program that partners with the local North Oaks Hospital to give students hands-on experiences in
       the field.</p>
      
      <p>Two other magnet programs are implementing Montessori teaching and learning methods in elementary schools, and three other 
      programs – two elementary schools and one middle school – have communication arts themes. These three schools boast student-
      produced television broadcasts and have been profiled on the “School Video News” website, with featured articles in the monthly 
      <i>School Video News</i> magazine.</p> 
      
      <p>Another magnet program is a fine and performing arts high school that focuses on ballet, jazz, and tap dance methods and that 
      supports a jazz band. Students in this program participate in performances throughout the year.</p>
      
      <p>The 10th magnet program is a Career Education Center. This magnet provides opportunities for juniors and seniors to participate 
      in a technical- and industry-based program where they can earn certifications while completing graduation requirements at their 
      regular high schools.</p>
      
      <p>Alison Andrews, the TPSD project director, has 27 years of experience in education. She has served as a magnet school principal 
      and administrator of grant programs in two districts. All of her work with grant programs has focused on promoting equity and 
      diversity for students, and on providing opportunities they may not otherwise have.</p>
      <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
