﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
 <div class="mainContent">
              <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Broward County Public Schools</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
           
             <img src="../images/gc042013/margate_middle_school_science_test.JPG" alt="Students wearing lab goggles test the temperature of a liquid." width="200" /><br />
            <small>Margate Middle School students use Vernier Proware to measure the increasing temperature of a liquid during science class.</small><br />
            <br />
          	
            <img src="../images/gc042013/broward_county_public_school_science_test.jpg" alt="Students friction test a wagon with their teacher." width="200" /><br />
            <small>McNicol Middle School STEM students <br/>friction test a wagon.</small><br />
            <br />
            
               <img src="../images/gc042013/florida_atlantic_univ_and_broward_school_staff.JPG" alt="Professors and teacher pose together." width="200" /><br />
            <small>BCPS staff members pose with professors from Florida Atlantic University.</small><br />
            <br />
           
            
            <img src="../images/gc042013/Leona_Miracola.jpg" alt="Leona Miracola, Project Director" width="200" /><br />
            <small>Leona Miracola,<br/> Project Director</small><br />
            <br />
            
      </div>
      <p>Broward County Public Schools (BCPS) is the sixth largest public school system in the United States, the second largest in the state of Florida, and the largest fully accredited K-12 and adult school district in the nation. BCPS enrolls more than 260,000 students in 232 schools and education centers. More than 44,000 students are enrolled in magnet programs.</p>
      <p>BCPS created six science, technology, engineering, and mathematics (STEM) magnet middle schools with its 2010 Magnet Schools Assistance Program (MSAP) grant. The priority goal is to increase student achievement and to create interest in STEM disciplines at the postsecondary and career levels. Curriculum for the STEM schools was created through a partnership with Florida Atlantic University College of Engineering, The College’s teaching assistants provide classroom support to students and teachers. The curriculum was designed so all the STEM students can compete in robotics competitions and use global information systems on projects to map locations and interpret data. In addition to developing STEM curriculum, STEM teachers have participated in more than 180 hours of professional development.</p>
      <p>Apollo Middle School enrolls 1,233 students and focuses on aeronautical sciences. Students engage in collaborative planning and design as they build aerospace and engineering systems. Partners, such as Embry-Riddle Aeronautical University, provide industry expertise to students.</p>
      <p>Students at Lauderhill Middle School delve into the worlds of forensic sciences, medical sciences, and health sciences through academic instruction, guest speakers, field trips, and distance learning. In partnership with Florida Medical Center, students can study patient cases with the help of physician mentors. Students work with Nova Southeastern University School of Nursing on health-related projects. The YMCA serves as a business partner and has a facility on the school’s campus.</p>
      <p>At Margate Middle School, a key feature of the STEM learning environment is a virtual science and mathematics lab. Approximately 1,250 students participate in labs that expose them to mechanical and civil engineering, biomedical engineering, and biomechanics. One of the main partners is Motorola Solutions.</p>
      <p>McNicol Middle School students are involved in bioengineering and alternative energy as they build solar ovens, outdoor heaters, and solar-powered cars. McNicol hosted the first Lego League robotics competition. Students visited Broward County Wheelabrator Solid Waste Facility, where they learned about waste production and energy conservation. Partners include the Florida Engineering Society.</p>
      <p>Students at Parkway Middle School design alternative energy vehicles, analyze ways to improve the natural environment, and learn how to forecast weather. In partnership with the America Culinary Federation, students plant hydroponic and critical gardens in their outdoor classroom.</p>
      <p>Students at Silver Lakes Middle School implement the STEM curriculum by engaging in hands-on engineering projects with elective teachers. The STEMology club gives students chances to participate in a schoolwide recycling program and adopt a street in the school’s community.</p> 
      <p>Leona Miracola is the MSAP Project Director. She has worked in public education for 30 years and oversees all the district’s magnet and innovative programs.
      </p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
