﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true" CodeFile="GC_082013.aspx.cs" Inherits="cornerArchive_GC_082013" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<a name="skip"></a>
    <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;San Diego Unified School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
           
             <img src="../images/gc082013/photo1.JPG" alt="Three students sit around a computer to create a 3-D sketch." width="200" /><br />
            <small>Roosevelt Middle School students recreate a 3-D sketch using Auto-Desk Inventor for their Gateway to Technology class.</small><br />
            <br />
          	
            <img src="../images/gc082013/photo2.JPG" alt="A female student smiles during the mock election." width="200" /><br />
            <small>Roosevelt Middle School student participates in the MyVote California mock election.</small><br />
            <br />           
            <br />
            <img src="../images/gc082013/photo3.JPG" alt="Three students sit by a stream holding scientific equipment." width="200" /><br />
            <small>Mission Bay High IB Biology students investigate human impact on the Rose Creek Watershed.</small><br />
             <br />           
            <br />
            <img src="../images/gc082013/photo3.JPG" alt="Alicia Caesar, MSAP Project Director" width="200" /><br />
            <small>Maria Nichols, <br />SDUSD Project Interim Director</small><br />
            <br />
            
      </div>
    <p>San Diego Unified School District (SDUSD) is the second largest district in California; it covers 208 square miles and serves more than 132,000 students. The SDUSD student population is extremely diverse, representing more than 15 ethnic groups and more than 60 languages.</p><p>SDUSD’s mission is to ensure all students will graduate with the skills, motivation, curiosity, and resilience to succeed in their choice of college and career, prepared to lead and participate in the society of tomorrow. To support achievement of this goal, SDUSD offers 34 magnet programs for kindergarten through 12th grades, with pathways in Communications and Community Leadership; Creative, Visual, and Performing Arts; International Baccalaureate (IB) and Global Citizenship; Language Enrichment and Immersion; and science, technology, engineering, and mathematics (STEM). All SDUSD magnet schools work toward harnessing the potential of the diverse district population by creating innovative academic environments where students move beyond learning about others to learning with others. </p><p>Currently two sites, Roosevelt Middle IB Magnet School and Mission Bay High IB Magnet School, receive support from the Magnet Schools Assistance Program (MSAP) grant. </p><p>A fully authorized IB Middle Years Program (MYP), Roosevelt Middle School is adjacent to the world famous Balboa Park and San Diego Zoo. With MSAP support, Roosevelt staff have taken advantage of this location to forge strong partnerships with numerous museums, including the Reuben H. Fleet Air and Space Museum, the Natural History Museum, and the Museum of Photographic Arts, as well as the Zoo. Roosevelt has added a science, technology, reading and writing, engineering, art, and mathematics (STREAM) focus to its IB curriculum. In specialized STREAM labs, students focus on projects that are integrated across content areas. The newest STREAM lab project is the development of a sustainable food production system called Aquaponics, and interdisciplinary teams are engaged in developing an Aquaponics curriculum. </p><p>Mission Bay High School is in the coastal community of Pacific Beach, bordered by the Pacific Ocean and Mission Bay. The MSAP grant helps Mission Bay High expand its IB Diploma Program and add an IB MYP. Designed for all 9th- and 10th-grade students, the MYP embeds the concept of inquiry-based learning across the disciplines and provides an instructional framework for developing an understanding of the connections between academic subjects and the real world. Mission Bay High capitalizes on its geographic location by offering a Marine Biology course as part of a broader STREAM curriculum that includes Project Lead the Way, IB Design Technology, bioengineering, information communications, and multimedia visual arts courses. </p><p>Maria Nichols, the Project Interim Director, has 26 years of experience with SDUSD. She works closely with magnet school leaders to ensure success for all students. Ms. Nichols is the author of several professional books and articles focusing on the development of thoughtful curriculum and instruction. </p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
</asp:Content>

