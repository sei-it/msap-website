﻿<%@ page title="" language="C#" masterpagefile="~/magnetpub.master"%>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| &nbsp;<span style="color: #666;">The Rhodes School </span></h1>
        <div style="margin-top: -20px; float: right; margin-left: 20px; width: 200px;">
            
	    <img src="../images/IMG_0234.jpg" alt="Children posing with teacher on a mountain hike." /><br />
            <small>The Rhodes School students at the Literacy Cafe</small><br />
            <br />
            <img src="../images/IMG_0218.jpg" alt="Students in Springfield Central High School band pose for the camera." /><br />
            <small>The Rhodes School crew meeting</small><br />
            <br />
            <img src="../images/Michelle-Bonton-pic.jpg" alt="Young children chasing butterflies with nets." /><br />
            <small>Michelle Bonton, The Rhodes School Project Director</small></div>
	    
        <p>
	The Rhodes School is an open enrollment PreK-5 charter school and local education agency (LEA) serving 350 students with the capacity to serve 850 students.  It is located within the boundaries of Sheldon ISD and serves Channelview, Galena Park, Houston, Humble, North Forest, and Sheldon school districts.  The Rhodes School was formed as an alternative educational choice for students enrolled in schools identified for school improvement or with low performance among minority students. More than 80% of the schools served meet Title I requirements and nearly 80% (11 of 14) of the schools in North Forest were identified for improvement. 
	</p>
	
	<p>The Rhodes School is the first charter school that is also LEA to receive a Magnet Schools Assistance Program grant. The Rhodes School is using its grant funds to expand educational choice, to provide increased opportunities for minority and low income students to experience and create art, and to offer students from different ethnic and socioeconomic backgrounds opportunities to interact with each other.  
	</p>
	
	<p>As the only magnet school in Channelview, Galena Park, Humble, North Forest, or Sheldon ISDs, The Rhodes School will implement Guggenheim's Learning through the Arts curriculum, which is an innovative multiple intelligences approach designed to develop critical thinking, art, and literacy skills while mastering basic content.  The Rhodes School plans to add an additional 150 K-5 students to its current enrollment over the three-year grant cycle. </p>
	
	<p>The Rhodes School will partner with organizations such as Young Audiences of Houston, a community based arts non-profit committed to helping schools integrate the arts into the core curriculum.  The school currently maintains partnerships with other organizations such as the Ensemble Theatre, Stage Presence Performing Arts Company, and Literacy Through Photography.  The Rhodes School is working to establish additional partnerships with the Houston Museum of Fine Arts, Houston Grand Opera Education Outreach Program, and the Houston Met Dance Company. 
	</p>
	
	<p>The Rhodes School is administered by Mrs. Michelle Bonton, the Superintendent and Project Director.  She has 13 years of education experience in curriculum development, education, and school administration. 
	</p>

<p><a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
</asp:Content>
