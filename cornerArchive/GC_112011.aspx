﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Galveston Independent School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px; text-align:center;">
          <br />
            <br />
            <img src="../images/gc112011/octobots.jpg" alt="Children in group photo wearing matching orange t-shirts" width="200" border="0"  /><br />
        <small>L.A. Morgan Elementary School Octo-bot winners celebrate their victory </small><br />
            <br />
      <img src="../images/gc112011/filming.jpg" alt="Boy filming an interview of fellow students" width="200" /><br />
            <small>Central Middle School students doing their daily, live broadcast</small>
          <br />
            <br />
        <img src="../images/gc112011/circle-reading.jpg" alt="Teacher reads a story to 3 young students" width="200" /><br />
            <small>Parker Elementary School students listen to a story in Chinese </small><br />
            <br />
        <img src="../images/gc112011/ship.jpg" alt="Children observe boats in a harbor" width="200" /><br />
            <small>Greta Oppe Elementary School students on a coastal expedition</small><br />
            <br />
        <img src="../images/gc112011/annette.jpg" alt="Annette Scott, Galveston Independent School District Project Director" width="200" /><br />
            <small>Annette Scott, Galveston Independent School District Project Director</small>
      </div>

        <p>
        Galveston Independent School District (GISD) serves 6,444 Pre-K through 12th grade students. In the 2009-10 school year, GISD had nearly 2,000 English as a second language (ESL) or limited-English-proficient (LEP) students. While most students' other language is Spanish, 17 languages are represented in the district. 
        </p>
        
        <p>
        The <em>Galveston Academic Programs for Equity and Excellence</em> (APEX) project involves five magnet schools, which include one middle school and four elementary schools. 
        </p>
        
        <p>
        The middle school, Central Middle School, integrates instructional technology through media arts. Students participate in cross-curricular learning activities which encompass broadcasting, digital photography, video game arts, and the visual arts. Each elementary school has its own specific magnet theme. 
        </p>
        
        <p>
        The Early Childhood University at Weis is a new program designed by a community group along with district leaders to focus on the needs of children 3-years-old and up, and focuses on research-based practices for exploratory learning and project-based learning techniques. 
        </p>
        
        <p>
        Greta Oppe Elementary School focuses on Coastal Studies. Through an interdisciplinary curriculum related to Galveston Island and coastal communities, Oppe integrates hands-on activities, student-guided enrichment projects, and study trips to explore resources such as beaches, coastal wetlands, and historical markers. 
        </p>
        
        <p>
        L.A. Morgan Elementary School has a Science and Engineering magnet theme and has students participate in robotics lessons from Carnegie Mellon and NASA, while also providing a two-way immersion model for LEP and ESL students. 
        </p>
        
        <p>
        Parker Elementary School is a new magnet program based on International Studies and the study of multiple foreign languages and cultures. 
        </p>
        
        <p>
        Each school has corresponding partnerships with community and state organizations. Central Middle School's partners include Channel 11, Galveston Daily Newspaper, Weatherbug, Strand Theater, and Artist Boat. The Early Childhood University at Weis's partners includes the University of Houston Clear Lake as well as local businesses, colleges, and universities who sponsor grade levels.  Greta Oppe Elementary School has partners spanning the likes of Texas A & M Galveston, Friends of Galveston Island State Park, Galveston Bay Foundation, Galveston Historical Foundation, and Mary Moody Northern Foundation.  L.A. Morgan Elementary School has the City of Galveston, University of Texas Medical Branch Galveston, Texas Parks and Wildlife, and Galveston Daily News as partners, among others. Parker Elementary School's partners include Rice University Institute for Chinese Language Teaching, Institute of Texan Cultures, United Nations Association of Houston, German Consulate of Houston, and the League of United Latin American Citizens Council &#35;151. 
        </p>
        
        <p>
        <em>APEX</em> is administered by Project Director Annette Scott. Dr. Scott is the Assistant Superintendent for Curriculum and Instruction for GISD. As a former magnet school principal with 25 years of administrative experience, her knowledge spans Pre-K through the collegiate level. She has an extensive background in gifted education, at-risk populations, and curriculum management. 
        </p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
