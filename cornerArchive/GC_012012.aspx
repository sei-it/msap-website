﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
   <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Cleveland School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <!--<br />  <img src="images/gc120711/choir.JPG" alt="Richland Northeast High School's Pop, Broadway, and Jazz Ensemble singing" width="200" /><br />
            <small>Richland Northeast High School's Pop, Broadway, and Jazz Ensemble performs during the International Assembly</small><br />-->
            <br />
    
            <img src="../images/gc010312/flowers.JPG" alt="Students making paper flowers" width="200" border="0"  /><br />
        <small>B.L. Bell Academy students learn about plants</small>
          <br />
            <br />
        <img src="../images/gc010312/snacks.JPG" alt="students prepare healthy snacks" width="200" /><br />
            <small>B.L. Bell Academy students make healthy snacks</small><br />
            <br />
        <!--<img src="images/gc120711/presents.jpg" alt="Students sitting with gifts for the needy" width="200" /><br />
            <small>E.L. Wright students offer supplies to needy families within the school during the holiday season</small><br />
            <br />-->
        <img src="../images/gc010312/beverly_hardy.JPG" alt="Beverly Hardy, Cleveland School District Project Director" width="200" /><br />
            <small>Beverly Hardy, Cleveland School District<br/> Project Director</small>
      </div>
      
       <p>Cleveland School District (CSD), in the Mississippi Delta, is the largest district in Bolivar County, Mississippi, serving 
       3,530 students. The district’s students come mostly from agricultural families.  Two-thirds (67 percent) of the students are 
       minorities, and most of the minority students are Black.  </p>
       
       <p>Due to the high rates of juvenile diabetes and asthma in both the county and the state, Bolivar County citizens care deeply 
       about health education. Based on this interest and other community feedback, the Magnet Schools Assistance Program (MSAP) grant
        project at B.L. Bell Academy offers its preK through grade 6 students a magnet theme of mathematics, science, and health 
        education. The Academy nurtures a culture of learning and community, and aims to motivate students from diverse backgrounds to
         work together, think critically, solve problems, and master rigorous academic standards.  </p>
          
        <p>B.L. Bell Academy immerses students in engaging environments that integrate research-based programs in reading, 
        mathematics, science, and all other subjects. Learning is supported by innovative technologies and authentic project- and 
        problem-based learning. 
         </p>
         <p>Community partnerships play an important role in assuring that the program and curriculum are relevant and that B.L. Bell 
         Academy students have real-world educational experiences. The school partners with local organizations, including the Delta 
         Health Center, Bolivar Medical Center, Bolivar County Extension Office, Bolivar County Health Department, and the Bolivar 
         County Chamber of Commerce. In addition, the Academy partners with several departments at Delta State University: the Center 
         for Science and Environmental Education; the Division of Health, Physical Education and Recreation; and the Nursing School.
         </p>
                  
        <p>Beverly Hardy, a veteran teacher and administrator in CSD, has served as Project Director for the magnet program since 
        2003. She is an adjunct professor in the Curriculum and Instruction Department at Delta State University. Her wealth of 
        knowledge and experience in the areas of curriculum development, student assessment, and professional development ensure great
         success for this project. 
        </p>


        <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
