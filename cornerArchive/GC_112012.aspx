﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
 <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Hillsborough County Public Schools</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
            <br />
            <br/>
            <img src="../images/gc112012/CulturalDressContest.jpg" alt="Four Walker Middle Magnet School students pose after winning the Cultural Dress Contest at the school’s International Festival." width="200" /><br />
            <small>Walker Middle Magnet School students win the Cultural Dress Contest at the school’s International Festival.</small><br />
            <br />
         <img src="../images/gc112012/Students_test_water.jpg" alt="Young Middle Magnet School students at the Creative Science Centre conduct water quality tests at Stormwater Pond." width="200" /><br />
            <small>Young Middle Magnet School students at the Creative Science Centre conduct water quality tests.</small><br />
            <br />
         
            <img src="../images/gc112012/museum_ofScienceTrip.jpg" alt="Lockhart Elementary Magnet School 4th grade student perform hands-on investigations on static electricity during a field trip to the Museum of Science and Industry." width="200" /><br />
            <small>Lockhart Elementary Magnet School 4th grade students perform hands-on investigations during a field trip to the Museum of Science and Industry.</small><br />
            <br />
            
               <img src="../images/gc112012/Pansy_Houghton.jpg" alt=" Dr. Pansy Houghton, Hillsborough County Public Schools Project Director" width="200" /><br />
            <small>Dr. Pansy Houghton,<br/> Hillsborough County Public Schools<br/> Project Director</small><br />
            <br />
            
      </div>
     <p>Economic and ethnic diversity characterize Hillsborough County, Florida, which includes the city of Tampa and surrounding municipalities and 
     communities. The eighth largest school district in the nation, Hillsborough County Public Schools (HCPS) serves nearly 200,000 students in 142 
     elementary schools, 46 middle schools, 2 K-8 schools, 27 high schools, and 4 career centers.</p>
     
     <p>With 35 magnet programs, HCPS commits to several strategies that support success, including professional development, technology, local partnerships, and student transportation. Professional development helps staff integrate the various magnet themes into the curricula. Specialized support staff, cutting-edge technology, and up-to-date library and media resources support curriculum delivery. Partnerships with local businesses enhance program effectiveness, and more than 160 school buses transport magnet students around the district every day.</p>
    
     <p>Hillsborough’s Magnet Schools Assistance Program (MSAP) schools share a focus on academic rigor aimed at college and career readiness. The new themes align with results from a random survey of 6,000 parents who indicated a strong need for magnet schools that prepare students for postsecondary education. 
    
     <p>Roland Park K-8 Magnet School is implementing the International Baccalaureate (IB) Primary Years Programme and Middle Years Programme and working toward authorization from the IB World Organization. More than 500 students benefit from a partnership with the Westshore Alliance, an association that represents the business district where many parents are employed. The Alliance provides these parents the time during work hours to attend school functions, read to classes, serve as guest speakers, eat lunch with their children, and volunteer in other ways. Roland Park also partners with the South Tampa Chamber of Commerce and Freedom Health Care.</p>
    
     <p>Walker Middle Magnet School, also implementing the IB Middle Years Programme, has more than 700 students. Its partners include Kitty City, National Humane Society, the Humane Society of Tampa Bay, and St. Francis Animal Rescue. Guava Tours serves as a business partner for the School Advisory Council and Bay News 9 provides guest speakers.</p>
     
     <p>The HCPS Creative Science Centre comprises the nearby campuses of Young Middle Magnet School and Lockhart Elementary Magnet School. Young Middle Magnet School partners with the University of South Florida (USF) Office of Sustainability. USF professors and students teach jointly with Young’s faculty and collaborated with teachers and students to develop a year-long curriculum. With USF support, students conduct water quality tests at nearby Stormwater Pond and upload data to a national database. Through a partnership with Tampa Electric Company, 7th grade students learn to conduct energy audits of their homes, uncovering prime energy wasters and identifying ways to save energy.</p>
     
     <p>Lockhart Elementary Magnet School has several community partners. These partners include the South Florida Water Management District, which provided curriculum resources and a Splash Grant to fund a hydroponics garden; the Hillsborough Extension Office, which provided resources, including presentations on composting and soil; IKEA, which donated containers for the science lab; Yum Yum Drums, which instructed kindergarten students in African drumming; and the NASA Speaker’s Bureau, which instructed students on the latest space technology.</p>
     
     <p>Dr. Pansy Houghton directs all HCPS magnet programs, including the ones currently funded by MSAP. Since 2001, Dr. Houghton served as the HCPS 
     Coordinator of Evaluation, the Supervisor of Choice Technology, and the Director of Student Planning and Placement. She also directed several local, 
     state, and federal grant projects.</p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
