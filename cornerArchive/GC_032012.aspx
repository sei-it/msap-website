﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
   <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Glendale Unified School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <!--<br />  <img src="images/gc120711/choir.JPG" alt="Richland Northeast High School's Pop, Broadway, and Jazz Ensemble singing" width="200" /><br />
            <small>Richland Northeast High School's Pop, Broadway, and Jazz Ensemble performs during the International Assembly</small><br />-->
            <br />
    
        <img src="../images/gc030212/Teacher_student.png" alt="student with face paint and teacher celebrate Day of the Dead." width="200" border="0"  /><br />
        <small>A teacher and student at Thomas Edison Advanced Technology and Spanish FLAG Magnet celebrate Day of the Dead.</small>
          <br />
            <br />
        <img src="../images/gc030212/MarkKeppelSchool.png" alt="Students creating clay sculptures." width="200" /><br />
            <small>Students at Mark Keppel Visual and Performing Arts and Korean FLAG Magnet participate in a three-dimensional art class.</small><br />
            <br />
            
         <img src="../images/gc030212/teacher_student_BenjaminFranklinILA.png" alt="Student and teachers putting up decorations." width="200" /><br />
            <small>Students and teachers at Benjamin Franklin International Languages Academy of Glendale celebrate Hispanic culture.</small><br />
            <br />
         <img src="../images/gc030212/Dr_kellyKing.jpg" alt="Dr. Kelly King image" width="200" /><br />
            <small>Dr. Kelly King, Glendale Unified School District Project Director</small><br />
            <br />
  
      </div>
      
    <p>As the third-largest district in Los Angeles County, the Glendale Unified School District 
       (GUSD) has more than 2,620 employees who serve a culturally diverse population of approximately 
       26,250 K-12 students. The district operates 20 elementary schools, 4 middle schools, 5 high 
       schools, a developmental center for students with multiple handicaps, and numerous child care 
       centers.</p>
       
       <p>The three elementary schools funded by the Magnet Schools Assistance Program (MSAP) provide 
       unique experiences that attract and retain students while balancing ethnic distribution 
       districtwide. In addition, the program ensures that all magnet students meet or exceed rigorous 
       academic standards.</p>
       
       <p>At Thomas Edison Advanced Technology and Spanish Foreign Language Academy of Glendale (FLAG) 
       Magnet, studies for about 800 K-6 students are facilitated by multimedia technology. Collaboration
        with Planet Bravo gives every student daily technology access to enhance and support high 
        academic achievement and to master 21st century technology skills. In addition, students in the 
        Spanish FLAG Program participate in a research-based, 90/10 dual immersion program designed to 
        lead to academic and cultural proficiency in both Spanish and English.</p>
        
        <p>At Benjamin Franklin International Foreign Languages Academy, about 500 K-6 students have the 
        chance to become biliterate, bilingual, and culturally aware in French, German, Italian, or 
        Spanish in a research-based, 90/10 dual immersion program. Students who begin in kindergarten 
        will achieve academic and social proficiency in two or more languages by the end of 6th grade. 
        Franklin’s collaborations with the Italian Government, Fondazione Italia, the German Government, 
        ZfA Deutsche Auslandsschularbeit International, and German American School Association help to 
        make these opportunities possible.</p>
        
        <p>About 900 K-5 students at Mark Keppel Visual and Performing Arts and Korean FLAG Magnet enjoy 
        a program that promotes academic achievement and social competencies by infusing the visual and 
        performing arts into the core curriculum.  Partnerships with The Armory Center for the Arts and 
        The Music Center help to increase academic rigor through a multidisciplinary approach. In 
        addition, students in Keppel’s Korean FLAG program participate in a research-based, 50/50 dual 
        immersion program designed to lead to academic and cultural proficiency in both Korean and 
        English. </p>
        
        <p>In addition to these theme-related activities, the three MSAP schools collaborate with the 
        California Association for Bilingual Education’s Project INSPIRE to increase parent involvement 
        and support.</p>
        
        <p>Dr. Kelly King, Project Director, has served GUSD for 21 years as a teacher, specialist, 
        assistant principal, and principal. Dr. King is also the Director of Categorical Programs.</p>


        <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
