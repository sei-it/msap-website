﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"  %>


<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Polk County Public Schools</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
           
             <img src="../images/gc072013/StudentatBethune.JPG" alt="Three students work with trays of papyrus strips." width="200" /><br />
            <small>Bethune Academy students observe plant growth.</small><br />
            <br />
          	
            <img src="../images/gc072013/STEMstudent.JPG" alt="A student works with a plant." width="200" /><br />
            <small>Bethune Academy student interacts with a hands-on science application.</small><br />
            <br />
            
            
            <br />
           
            
            <img src="../images/gc072013/BrianWarren.JPG" alt="Alicia Caesar, MSAP Project Director" width="200" /><br />
            <small>Brian Warren, <br/>MSAP Project Directorr</small><br />
            <br />
            
      </div>
     <p>Polk County Public Schools (Polk County) is one of Florida’s largest school districts, and the thirty-first largest in the United States. East of Tampa in central Florida, Polk County covers more than 1,850 square miles. It has 163 school sites and centers, including nine magnet schools. More than 15,000 of the 94,000 diverse students speak a primary language other than English. The school district is the largest employer in Polk County, with more than 6,500 teachers on its payroll.</p>
<p>Polk County has eight Magnet Schools Assistance Program (MSAP) schools; seven schools have revised magnet programs and one has a new magnet program. The program revisions include creating five STEM, schools and three International Baccalaureate Middle Years Programme (MYP) schools. Along with these changes, the district developed a new student assignment process that uses a grid system based on adequate yearly progress indicators: free or reduced-price lunch status, ethnicity, students with disabilities, and English language learners. </p>
<p>The nine magnet schools across the district provide themed options to expand choice for students. Magnet teachers receive professional development in the latest methodologies and technologies, and students have resources that help them access and demonstrate learning opportunities in a global environment. Local partnerships provide additional resources to meet the needs of each school or community. </p>
<p>MSAP support has helped several schools earn recognition. Lincoln Avenue Academy was chosen as one of 219 National Blue Ribbon Schools nationwide. The school has experienced great success with its model STEM program. Crystal Academy of Science and Engineering, another STEM magnet program, participated in the University of Florida’s first Florida Sea Grant’s Coastal Science Symposium. Students interacted with researchers, specialists in the field, and public and private sector representatives who have interests in coastal zone and marine science. Lawton Chiles Middle Academy, an MYP school, became the first middle school in Polk County to win the International Baccalaureate Designation. </p> 
<p>Brian Warren, Director of Magnet Schools, oversees implementation of Polk County’s nine magnet schools, including the eight currently funded by MSAP. Mr. Warren has presented at national education conferences and conducted state-level meeting sessions for the Florida Department of Education on topics that include defining choice, student recruitment, and student achievement. </p>

      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
</asp:Content>

