﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
 <div class="mainContent">
              <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Los Angeles Unified School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
           
             <img src="../images/gc052013/MidCityPrescottschool_studentScienceInvestigations.png" alt="Student works with the teacher to experiment with an air compressor." width="200" /><br />
            <small>Mid City's Prescott School of Enriched Sciences 3rd grade students perform hands-on investigations on the effects of air compression.</small><br />
            <br />
          	
            <img src="../images/gc052013/KidsinCourt.png" alt="Student stands before the audience to deliver an opening statement." width="200" /><br />
            <small>Tom Bradley Global Awareness Magnet student attorney gives opening statement during "Kids in Court."</small><br />
            <br />
            
               <img src="../images/gc052013/AcademyforEnrichedSciences_students.png" alt="Student uses bubbles to create a three-dimensional prism." width="200" /><br />
            <small>The Academy for Enriched Sciences students participate in hands-on experiments creating three-dimensional prisms during Science Night.</small><br />
            <br />
           
            
            <img src="../images/gc052013/DeborahBrandy.jpg" alt="Deborah Brandy, Project Director" width="200" /><br />
            <small>Deborah Brandy,<br/> Project Director</small><br />
            <br />
            
      </div>
      <p>Since 1977, Los Angeles Unified School District (LAUSD) has been in the forefront of offering unique and exciting educational choices to the diverse residents of the second largest school district in the country. More than 660,000 students attend 1,200 schools, of which 179 are magnets. LAUSD prepares every student for college and career in a safe and caring learning environment.</p> 

<p>The Magnet Schools Assistance Program (MSAP) grant supports the significant revision of three existing magnet schools and the creation of one new magnet school. Teachers receive 60 hours of professional development in systemic reforms and magnet theme development to provide an innovative academic program so all students have challenging and authentic learning opportunities. The schools also aim to increase parent participation by 5 percent each school year. While all students learn to be critical thinkers by studying real-life situations, each school has a separate and distinct focus.</p>

<p>Tom Bradley Global Awareness Magnet emphasizes the study of government and politics. In partnership with the University of Southern California, these elementary students become environmentally and socially responsible community members. Community service projects, civil debates, and human rights studies help students make connections between the classroom and the real world, supporting their growth as responsible citizens who positively impact people around the globe.</p>

<p>Westside Global Awareness Magnet is situated in a unique ecosystem between the Pacific Ocean and the Venice Canals. Here, K-8 students develop environmental and social responsibility by linking society, health, and environment with community actions to improve the physical and social environments of the school and its neighborhood. Community service projects are required and include a 166-foot sustainable garden, maintained by students. This provides hands-on application of concepts learned in class that become pathways to academic success. </p>

<p>At Mid-City's Prescott School of Enriched Sciences, elementary students study the biosphere. Weekly investigations and laboratory experiments help students understand how the global sum of all ecosystems impacts life, earth, and physical sciences. Students carefully document the data from their hands-on science investigations, and then write and submit abstracts for <i>The New Journal of Student Research Abstracts</i>, published annually by California State University, Northridge.</p>

<p>The Academy for Enriched Sciences opened in 2011 with five state-of-the-art classrooms and one dedicated science lab. This school year, the population grew to more than 200 students. In 10 classrooms, elementary students are immersed daily in science and technology. Recently, the school hosted a Science Night where parents and students experienced hands-on investigations to compare solar power to battery power, build molecules, and make personalized toothpaste.</p>
<p>Deborah Brandy, the MSAP Project Director, has served magnet students in LAUSD for more than 25 years. She was the principal at Hillcrest CES Music Magnet for 7 years and at Braddock Gifted Magnet for 6 years, and was a classroom teacher at Windsor Hills Science Magnet for many years. Her wealth of knowledge enables her to support the four MSAP schools as they develop programs that increase student achievement and prepare students for college and career.
      </p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
