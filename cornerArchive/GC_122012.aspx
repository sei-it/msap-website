﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
 <div class="mainContent">
                <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Moorpark Unified School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
            <br />
            <br/>
            <img src="../images/gc122012/LetsBuildAZoo.jpg" alt=" Campus Canyon College Preparatory Academy students hold bugs while working with a Moorpark College Zoology student during an Enrichment class titled 'Let's Build a Zoo.'" width="200" /><br />
            <small>Campus Canyon College Preparatory Academy students work with a Moorpark College Zoology student during an Enrichment class titled "Let's Build a Zoo."</small><br />
            <br />
         <img src="../images/gc122012/InventionConvention.jpg" alt="Two Campus Canyon College Preparatory Academy students work with craft materials at Invention Convention, an enrichment class on building simple machines." width="200" /><br />
            <small>Campus Canyon College Preparatory Academy students attend Invention Convention, an enrichment class on building simple machines.</small><br />
            <br />
         
               <img src="../images/gc122012/WritersWorkshop.jpg" alt="Two Arroyo West Active Learning Academy first graders share writing pieces at Writer’s Workshop." width="200" /><br />
            <small>Arroyo West Active Learning Academy first graders share writing pieces at Writer’s Workshop.</small><br />
            <br />
            
            <img src="../images/gc122012/ReceivingProfessionalDev.jpg" alt="Arroyo West Active Learning Academy teachers work with students while receiving professional development from Columbia Teachers College Reading and Writing Project Staff Developers." width="200" /><br />
            <small>Arroyo West Active Learning Academy teachers receive professional development from Columbia Teachers College Reading and Writing Project Staff Developers.</small><br />
            <br />
            
            
            <img src="../images/gc122012/AngelaRyals.jpg" alt="Angela Ryals, Moorpark Unified School District Project Director" width="200" /><br />
            <small>Angela Ryals,<br/> Moorpark Unified School District<br/> Project Director</small><br />
            <br />
            
      </div>
     <p>Moorpark Unified School District is located in Ventura County California, about 30 miles outside of Los Angeles.  Moorpark Unified serves 6,995 students in five elementary schools— one K-8 school, two middle schools, one high school, one continuation high school, and one alternative high school.  This diverse district serves 1,291 English language learners.</p> 
     <p>In addition to the two schools participating in the current MSAP funding cycle, Arroyo West Active Learning Academy (AWALA) and Campus Canyon College Preparatory Academy (C&#xb3;PA), the district has two elementary schools that were funded in previous MSAP grant cycles.  These two elementary schools, Flory Academy of Science and Technology and Walnut Canyon Arts and Technology Magnet, have sustained their magnet themes and programs to the great benefit of their students, families, and community.</p>
     <p>AWALA serves 413 students and applies instructional techniques from Reading and Writing Workshop, Responsive Classroom, and project-based learning. The entire teaching staff attended the Reading and Writing Project Institutes at Columbia University to study with Lucy Caulkins, the founder and director of the Reading and Writing Project.  Currently, AWALA teachers work with visiting Institute coaches.  In addition, the teachers use project-based learning techniques to ensure the flow of curriculum is guided by student questions, interests, and enthusiasm.  Students have the opportunity to work on problems and issues relevant to their lives and to learn vital work and life skills necessary to their success. AWALA is where students are active with their bodies as well as their minds.</p>
     <p>C&#xb3;PA serves 518 students and combines College Connection with the Schoolwide Enrichment Model.  The Schoolwide Enrichment Model offers students a diverse selection of enrichment classes available before, during, and after school. Students experience core curriculum through these engaging, self-selected classes and activities. C&#xb3;PA staff members were trained through Confratute at the University of Connecticut, where they studied gifted and talented education philosophies and methods and the best way to use them to accelerate learning for all students.  C&#xb3;PA is transitioning from a K-5 program to a K-8 one.  The school added 7th grade this year and will add 8th grade next year.</p>
     <p>The MSAP program at Moorpark Unified is administered by Angela Ryals, Project Director, along with a team of other administrators and support staff. Ryals has worked for 8 years within Moorpark Unified. She has experience working on several other federal grants and a background in school counseling. Ryals is supported by the site coordinator of C&#xb3;PA, Anida Brock, who also serves the important role of Enrichment Coach on the C&#xb3;PA campus. Both sites are led by enthusiastic and supportive principals, Stephanie Brazell (C&#xb3;PA) and Susanne Smith-Stein (AWALA).
</p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
