﻿<%@ page title="" language="C#" masterpagefile="~/magnetpub.master" autoeventwireup="true" enableEventValidation="false" viewStateEncryptionMode="Never" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
    Grantee Corner&nbsp; | <span style="color: #666; line-height:120%;">&nbsp;Metropolitan School District of Lawrence Township</span></h1>
        <div style="margin-top: -25px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <br />
    <img src="../images/gc040811/photo-1.jpg" alt="Mary Castle School of International Studies students prepare desserts from around the world" /><br />
            <small>Mary Castle School of International Studies students prepare desserts from around the world</small><br />
            <br />
        <img src="../images/gc040811/photo-3.jpg" alt="Brook Park School of Environmental Studies students at work on a shark project" /><br />
            <small>Brook Park School of Environmental Studies students at work on a shark project</small><br />
            <br />
        <img src="../images/gc040811/photo-7.jpg" alt="Crestview School of Communications students participate in a daily writers’ workshop" /><br />
            <small>Crestview School of Communications students participate in a daily writers’ workshop</small><br />
            <br />
        <img src="../images/gc040811/Jan-Reckley.jpg" alt="Jan Reckly, Metropolitan School District of Lawrence Township Project Director" /><br />
            <small>Jan Reckly, Metropolitan School District of Lawrence Township Project Director</small></div>
	    
        <p>
The ninth-largest district in Indiana, Metropolitan School District of Lawrence Township (MSDLT) is a suburban-turned-urban district in the Northeast corner of Indianapolis that serves approximately 15,400 students. MSDLT is using Magnet Schools Assistance Program (MSAP) funds to create 11 magnet elementary schools, making it the only district nationwide in which every elementary school is a magnet school. The districtwide magnet school program offers five themes: (1) science, technology, engineering, and math; (2) communications; (3) environmental studies; (4) inquiry and performing arts; and (5) international studies. Each theme incorporates project-based learning and creates an instructional match with rigor and high expectations for all students.
	</p>
	<p>
School leadership teams have identified community partners to support each magnet school’s unique area of focus. These local partners include McKenzie Career Center, Butler University, Indiana University-Purdue University Indianapolis, Fort Benjamin Harrison, the MSDLT Foundation, Loving Care, Bob Logan of Wireless Business Solutions, Sprint, HP Dell, the 21st Century Community Learning Centers, InDesign Engineering, Young Audiences, the International Spanish Academy, Binford Redevelopment and Growth, Inc., Apple Excavating, Marion County Soil and Water, Indianapolis Power and Light, and Lee/Willis Communications. 
	</p>
	<p>
The <em>Bold Choices, New Beginnings!</em> MSAP project is administered by Jan Reckley, who previously served the district as a Title I support team member, literacy and technology coach, and classroom teacher. Mrs. Reckley has lived in Lawrence Township for more than 30 years.	
</p>
	
<p><a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
