﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
<div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Duval County Public Schools</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <br />
         <img src="../images/gc082012/Carter_Woodson_Medical_Arts.JPG" alt="Two Carter Woodson Elementary Medical Arts Magnet students pour a green chemical substance into a graduated cylinder to measure its volume during a science experiment." width="200" /><br />
            <small>Carter Woodson Elementary Medical Arts Magnet students measure a chemical substance during a science experiment.</small><br />
            <br />
            <img src="../images/gc082012/Mayport_Elementary_Coastal_Sciences.jpg" alt="Mayport Elementary Coastal Sciences Magnet School students use magnifying glasses to observe and count rainbow trout eggs which they raised in the school’s “Labitat.”" width="200" /><br />
            <small>Mayport Elementary Coastal Sciences Magnet School students observe and count rainbow trout eggs which they raised in the school’s “Labitat.”</small><br />
            <br />
            <img src="../images/gc082012/Woodland_Acres_Medical_Arts.jpg" alt="Woodland Acres Elementary School of the Medical Arts 3rd graders view x-rays to study the skeletal and muscular systems and how they work together to give us the ability to move." width="200" /><br />
            <small>Woodland Acres Elementary School of the Medical Arts 3rd graders study the skeletal and muscular systems and how they work together to give us the ability to move.</small><br />
            <br />
               <img src="../images/gc082012/Ortega_Elementary_Museum_Studies_Magnet.JPG" alt="Ortega Elementary Museum Studies Magnet 2nd graders paint exhibit pieces with mentors from the University of North Florida College of Art and Design." width="200" /><br />
            <small>Ortega Elementary Museum Studies Magnet<br/> 2nd graders create exhibit pieces with mentors from the University of North Florida College of<br/> Art and Design.</small><br />
            <br />
         <img src="../images/gc082012/SallyHague.jpg" alt="Dr. Sally Hague, Duval County Public Schools Project Director." width="200" /><br />
            <small>Dr. Sally Hague,<br/> Duval County Public Schools<br/> Project Director.</small><br />
            <br />
            
      </div>
     <p>Duval County Public Schools (DCPS) is in Jacksonville, Florida, which covers 840 square miles—the largest city in the contiguous United States in geographic size. DCPS is the 6th largest school district in Florida and the 15th largest in the United States, serving approximately 125,000 students in 177 schools. More than 20,000 students attend the magnet programs in 62 district schools.</p>
     <p>The 2010-13 Magnet Schools Assistance Program (MSAP) grant is the third consecutive MSAP grant received by DCPS, and it supports five elementary schools and three middle schools. Although the schools are implementing five different themes, all use standards-based instruction with an interdisciplinary curriculum that features engaging, active learning.</p>
     <p>Woodland Acres and Carter G. Woodson Elementary Schools provide the elementary component to the district’s successful School of the Medical Arts at Darnell-Cookman Middle/High School. The medical sciences are integrated across the curriculum to spark student interest and create curiosity about the variety of opportunities available in the medical field. The schools are creating strong vertical articulation to prepare elementary students for the rigorous program at the secondary level.</p>
     <p>Spring Park Elementary and Southside Middle Schools are candidates for the International Baccalaureate (IB) Primary Years and Middle Years programs, joining the four other Primary Years programs, three other Middle Years programs, and six IB Diploma programs currently in the district. 
Given the coastal Florida location, the new theme of Coastal Sciences is a natural fit for DCPS magnet schools. Mayport Elementary and Mayport Middle Schools, located on adjacent campuses along the Atlantic Ocean, provide opportunities for in-depth study about the coastal environment through classroom instruction as well as hands-on inquiry and problem solving.</p>
	<p>The Early High School magnet theme at Jean Ribault Middle School motivates academic underachievers to excel and builds a college-going culture. Middle school students can earn up to three high school credits and prepare for one of the district’s high school acceleration programs such as an Early College magnet program.</p>
    <p>At Ortega Elementary School, the Museum Studies program is a new theme for DCPS. The integrated curriculum enables students to be “hands-on, minds-on” in cooperative learning groups. Each quarter students create museum gallery exhibitions to showcase their knowledge of the current unit of study.</p>
    <p>Dr. Sally Hague, Executive Director of the School Choice Department and MSAP project director, has been with DCPS for her entire 44-year career and has been involved with magnet programs for 17 years. She is assisted by Ann Edgecombe, Supervisor of the MSAP grant, who works with the schools to design and implement the magnet themes.  
</p>
      <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div></asp:Content>
