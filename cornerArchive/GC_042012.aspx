﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
   <div class="mainContent">
<h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;City School District of Albany</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <br />
            <img src="../images/gc_042012/GC_4_2012_img1.png" alt="Students participate in an E Beam Deposition Lab in a cleanroom at the College of Nanoscale Science and Engineering." width="200" border="0"  /><br />
        <small>Innovation Academy students participate in an E Beam Deposition Lab in a cleanroom at the College of Nanoscale Science and Engineering.</small>
          <br />
            <br />
        <img src="../images/gc_042012/GC_4_2012_img2.jpg" alt="Students pose with Mayor Jerry Jennings in the United Nations room of the Discovery Academy." width="200" /><br />
            <small>Students attend lunch with Mayor Jerry Jennings in the United Nations room of the <br/>Discovery Academy.</small><br />
            <br />
            
         <img src="../images/gc_042012/GC_4_2012_img3.jpg" alt="Leadership Academy students and staff pose during a holiday fundraiser." width="200" /><br />
            <small>Leadership Academy students and staff pose during a holiday fundraiser.</small><br />
            <br />
         <img src="../images/gc_042012/Stan_Harper.JPG" alt="Dr. Kelly King image" width="200" /><br />
          <small>Stan Harper, City School District of Albany Project Director</small><br />
            <br />
      </div>
    <p>The City School District of Albany serves about 8,700 pre-K through 12th grade students in 15 schools. As a result of the Magnet Schools Assistance
      Program (MSAP) grant, Albany High School now boasts four physically smaller learning environments designed to help students feel connected, involved, and
       engaged.</p>
       
       <p>Each academy has its own space and offers more demanding coursework and extra supports for all students. The academies offer the same core classes 
       while academy-specific electives make each academy unique. </p>
       
       <p>With its focus on critical thinking and preparation for a global economy, the Citizenship Academy offers advanced math, business, law, and family and
        consumer sciences classes. Its partners include the U.S. Department of Justice, the Albany County District Attorney’s Office, the Albany Mayor’s 
        Office, and Albany City Court. It features such initiatives as youth court, monthly service projects on behalf of various human services organizations,
         lunch speakers, and Citizen of the Week appointments. It employs a mock courtroom complete with a judge’s bench, witness stand, jury box, counsel 
         table, and audience seating.</p>
         
         <p>The Discovery Academy’s theme is communications and performing arts. It offers advanced English, music, drama, and arts classes. Partners include 
         Proctor’s Theatre, Walden Chamber Orchestra, Albany Pro Musica, and Music Mobile, all of which contribute to artist-in-residence projects that include 
         filmmaking, set construction, playwriting, musical composition, and performance. A state-of-the-art multimedia production facility has equipment for 
         music and video recording.</p>
         
        <p>The Innovation Academy’s themes are science, technology, engineering, and math. Partnerships with the University at Albany Center for Nanoscale 
        Science and Engineering, Hudson Valley Community College, and Siena College help the academy provide bimonthly lectures and demonstrations. The 
        academy’s mock cleanroom simulates a sterile laboratory like those used in biotechnology, semiconductor manufacturing, and other fields that are 
        sensitive to environmental contamination.</p>
        
        <p>The Leadership Academy offers advanced social studies and foreign language electives that focus on the academy’s civic and social responsibility 
        theme. The Albany Mayor’s Office and various community organizations support such initiatives as the “Lunch with a Leader” series, a bedding/fund 
        drive for victims of Hurricane Irene, and the “Soles4Souls” drive to collect shoes. Students in this academy have the use of a large-group instruction 
        room modeled after the General Assembly Room in the United Nations.</p>
        
        <p>Project Director Stan Harper is the magnet director and house principal at Albany High School. He has been a teacher, curriculum leader for 
        elementary math and science, and an administrator. A doctoral degree candidate, Harper has tremendous knowledge and experience in curriculum 
        development, implementation practices, assessment, instruction, differentiated instruction, and financial budgeting.</p>

        <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
