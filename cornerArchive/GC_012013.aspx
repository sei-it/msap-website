﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
 <div class="mainContent">
                <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Community School District 14</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
            
         <img src="../images/gc012013/PS_257_ballerinas_perform_Waltz_of_the_Flowers.jpg" alt="Three PS 257 students perform as ballerinas in Waltz of the Flowers." width="200" /><br />
            <small>PS 257 students perform as ballerinas in Waltz of the Flowers.</small><br />
            <br />
         
               <img src="../images/gc012013/PS257_marches_in_Brooklyns_3_Kings_Parade.JPG" alt="PS 257 students perform in the marching band in Brooklyn’s Three Kings Parade." width="200" /><br />
            <small>PS 257 students perform in the marching band in Brooklyn’s Three Kings Parade.</small><br />
            <br />
            
            <img src="../images/gc012013/PS380_4th_graders_research_for_writing_projects.JPG" alt="PS 380 4th grade students research their writing projects on a computer." width="200" /><br />
            <small>PS 380 4th grade students research their writing projects on a computer.</small><br />
            <br />
            
            
            <img src="../images/gc012013/PS414_kindergarteners_participate_in_TheBigAppleCrunch.JPG" alt="PS 414 kindergarten students hold up apples while participating in the Big Apple Crunch." width="200" /><br />
            <small>PS 414 kindergarten students participate in the Big Apple Crunch.</small><br />
            <br />
            
            <img src="../images/gc012013/Joseph_Gallagher.jpg" alt="Joseph Gallagher, Project Director" width="200" /><br />
            <small>Joseph Gallagher,<br/> Project Director</small><br />
            <br />
            
      </div>
     <p>New York City Community School District 14 (District 14) is a large urban district that serves the diverse Brooklyn communities of Williamsburg and Greenpoint. Its 45 elementary, middle, and high schools enroll 20,360 students in grades pre-kindergarten through 12.</p>
	<p>District 14 has enjoyed success with past Magnet Schools Assistance Program (MSAP) projects and is building on that success with a 2010-13 MSAP grant that includes six magnet schools. The program goal is to reduce minority group isolation among Hispanics, and the schools have all created rigorous theme-based instructional programs aimed at increasing diversity while promoting student achievement. A recent <i>New York Times</i> article on school integration in New York City described the District 14 magnet schools as “rich in programming.”</p>
	<p>The PS 19 magnet theme of global and ethical studies uses the Heartwood Ethics curriculum, which integrates ethical topics into various curricular areas and activities. The school applies some of these values through service-learning projects. Students also receive focused instruction in world culture, history, government, and geography, and they present their work in an annual, culminating Magnet Exposition for parents and members of the community.</p>
	<p>PS 414 is a new school that shares a building and magnet theme with PS 19. Also known as the Brooklyn Arbor School, PS 414 uses ethical studies as a lens for examining the environment. Students engage in hands-on activities around recycling and energy conservation. In a recent celebration of Food Day, the school used donations from Whole Foods supermarket to explore issues related to nutrition and food sustainability. In global studies, the magnet specialist works with early grade students to help them learn different customs from around the world, and she frequently incorporates artwork into the lessons.</p>
	<p>PS 250’s magnet theme is communication and multimedia arts. Drama and technology specialists concentrate on improving students’ skills in performance, public speaking, and technology and software applications. As part of the school’s involvement with the University of Connecticut’s Schoolwide Enrichment Model, instructional staff implement the Independent Investigation Model, which teaches students how to conduct authentic research and develop their ability to communicate an understanding of the topics they are researching.</p>
	<p>At PS 257, the staff is integrating the magnet theme of performing arts with Common Core standards and the Blueprint for the Arts. The school’s marching band has performed throughout the city and state. The magnet specialists and partners such as Creative Music, City at Work, and American Ballroom Theatre provide all students with direct music instruction in piano, flute, and guitar, along with dance training and theatre arts instruction. </p>
	<p>At PS 380 the magnet theme of literary arts and integrated technology has infused the classrooms with new technology that supports the school’s focus on improving teacher effectiveness and student performance. Strategies include full integration of technology with literary arts across all curriculum areas, theme-related training for teachers, and celebration of students’ writing and speaking skills in monthly magnet events for students and their parents. PS 380 has also leveraged a partnership with Teaching Matters to develop differentiated instructional tasks in the magnet theme.</p>
	<p>Environmental engineering is the theme at District 14’s magnet middle school, MS 126. Here, magnet specialists support classroom teachers in applying the theme across content areas, and several partners are also engaged in magnet activities. These include robotics with Vision Education and Media, renewable energy work with Solar One, environmental science with Trout in the Classroom, and neighborhood environmental awareness with the Newtown Creek Alliance.</p>
	<p>The District 14 MSAP project director is Joseph Gallagher, of the New York City Department of Education Office of School Support. Mr. Gallagher has extensive experience as a junior high school teacher and district-level administrator. He has worked with several magnet programs, all in Brooklyn, since 1998.
</p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
