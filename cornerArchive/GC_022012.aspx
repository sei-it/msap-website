﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
   <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Corpus Christi Independent School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <!--<br />  <img src="images/gc120711/choir.JPG" alt="Richland Northeast High School's Pop, Broadway, and Jazz Ensemble singing" width="200" /><br />
            <small>Richland Northeast High School's Pop, Broadway, and Jazz Ensemble performs during the International Assembly</small><br />-->
            <br />
    
            <img src="../images/gc020112/Math_science_class.jpg" alt="Students in front of a white board" width="200" border="0"  /><br />
        <small>Metro Preparatory students learn about kinetic and potential energy by designing and <br/>building rollercoasters in the integrated math/science class.</small>
          <br />
            <br />
        <img src="../images/gc020112/Science_Tutorials.jpg" alt="Elementary students create and record science tutorials" width="200" /><br />
            <small>Metro Elementary students create and record science tutorials to share with students across the district.</small><br />
            <br />
            
         <img src="../images/gc020112/student_assignment.jpg" alt="Elementary students in front of a laptop" width="200" /><br />
            <small>Metro Elementary students create a “Night at the Museum” which showcases historical figures and their impact on society.</small><br />
            <br />
         <img src="../images/gc020112/Orlando_Salazar.jpg" alt="Orlando Salazar image" width="200" /><br />
            <small>Orlando Salazar, Corpus Christi Independent School District Project Director</small><br />
            <br />
  
      </div>
      
        <p>The Corpus Christi Independent School District (CCISD) serves more than 38,000 students at 60 campuses within 68 square miles. The district’s mission is to develop the 
        hearts and minds of all students, preparing them to be lifelong learners who will continue with their education, enter the world of work, and be productive citizens.</p>
        
        <p>In August 2011, CCISD opened two new magnet schools: Wynn Seale Metropolitan School of Design (Metro E) and Roy Miller High School Metropolitan School of Design (Metro 
        Prep). Metro E is a whole-school magnet with 630 students in grades K-6, and Metro Prep is a  partial-school magnet with  430 students in grades 7-12. The vision for the Metro 
        Schools is to nurture global learners who lead with the passion to create, the persistence to innovate, and the confidence to design new possibilities for the future. </p>
        
        <p>This exciting, new K-12 continuum capitalizes on artistic learning in a unique instructional setting. Students use project-based learning to analyze real problems. They 
        employ emerging technologies, peer collaboration, and community contacts to work toward viable solutions. Students’ interests and special talents work in concert with an 
        engaging and vibrant curriculum to build key 21st century skills.</p>
        
        <p>Students at both campuses are involved in a variety of community projects such as helping to coordinate the city’s recycling program at the school and working with key 
        officials to bring a water park to the city. In just the first few months, the Metro schools have made a difference in the lives of their students, staff, and surrounding 
        community.</p>
        
        <p>Orlando Salazar, the CCISD Magnet Schools Director, oversees the Metropolitan Schools of Design. He has 24 years of experience in education working as a classroom teacher 
        and as a campus and district-level administrator. He has extensive experience with at-risk populations and curriculum management.</p>


        <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
