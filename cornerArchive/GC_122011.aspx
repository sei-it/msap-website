﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
   <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Richland School District Two</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px;  width: 200px;text-align:center;">
          <br />
            <!--<br />  <img src="../images/gc120711/choir.JPG" alt="Richland Northeast High School's Pop, Broadway, and Jazz Ensemble singing" width="200" /><br />
            <small>Richland Northeast High School's Pop, Broadway, and Jazz Ensemble performs during the International Assembly</small><br />-->
            <br />
    
            <img src="../images/gc120711/paper_cranes.JPG" alt="High School students boxing paper cranes for Cranes for Japan Projects" width="200" border="0"  /><br />
        <small>Richland Northeast High School students count and box more than 3,000 paper cranes for the Cranes for Japan project</small>
          <br />
            <br />
        <img src="../images/gc120711/chick.JPG" alt="Elementary students observe chicks" width="200" /><br />
            <small>Windsor Elementary School students study life cycles by hatching chicks</small><br />
            <br />
        <img src="../images/gc120711/presents.jpg" alt="Students sitting with gifts for the needy" width="200" /><br />
            <small>E.L. Wright students offer supplies to needy families within the school during the <br/>holiday season</small><br />
            <br />
        <img src="../images/gc120711/sara_wheeler.jpg" alt="Sara Wheeler, Richland School District Two Project Director" width="200" /><br />
            <small>Sara Wheeler, Richland School District Two Project Director</small>
      </div>
      
       <p>Richland School District Two (Richland Two) is the largest district in the Midlands of South Carolina and one of the fastest growing school districts in the state. Richland Two serves more than 24,000 Pre-K through 12th grade students. Eighty-three of the 242 miles of the district are located on the property of Fort Jackson, the nation’s largest and most active military training installation for the U.S. Army. </p>
       <p>Richland Two’s International Baccalaureate Continuum is in the process of pursuing authorization to become International Baccalaureate (IB) World Schools at three magnet schools that are close to each other in an area of need in the district. All three schools have been accepted as candidate schools, and form a continuum of learning at the elementary, middle, and high school levels. IB programs promote the education of the whole person, emphasizing intellectual, personal, emotional, and social growth through all domains of knowledge. IB assessment is rigorous, criterion-referenced, and differentiating of student ability. The program provides services for students to live, learn, and work in a rapidly globalizing world.  Because a major portion of the community is military, offering the IB program to students of military families may help these students bridge the gap between their former and current schools.</p>
       <p>The various community stakeholders enthusiastically support the International Baccalaureate Continuum. State and county governments, colleges and universities, and other community partners understand the importance of this project. Some community partners include the Columbia Museum of Art, Cheerwine Soft Drinks, and State Farm Insurance. </p>
       <p>The International Baccalaureate Continuum is administered by Project Director Sara Wheeler. Ms. Wheeler has 37 years of experience in education, with 24 of them in Richland Two. She is certified in K to 8 education, secondary social studies, and gifted education, and has worked as both a teacher and administrator. She currently sits on the Board of Directors for the Magnet Schools of America.</p>


        <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
