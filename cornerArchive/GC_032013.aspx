﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
 <div class="mainContent">
              <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Napa Valley Unified School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
           
         <img src="../images/gc032013/PuebloVistaStudentsMagnetStudents_studyplants.jpg" alt="Two students work with plants during an environmental sciences class." width="200" /><br />
            <small>Pueblo Vista Magnet School students work with plants during an environmental sciences class.</small><br />
            <br />
            
             <img src="../images/gc032013/AltaHeightsMASTAcademy_playmusic.jpg" alt="Four students play keyboards in music class." width="200" /><br />
            <small>Salvador Elementary students play keyboards in an arts class.</small><br />
            <br />
         
               <img src="../images/gc032013/AltaHeightsMASTAcademy_worksiPad.jpg" alt="A female student works with an iPad." width="200" /><br />
            <small>Alta Heights MAST Academy student works with an iPad.</small><br />
            <br />
           
            
            <img src="../images/gc032013/Chuck_Neidhoefer.jpg" alt="Chuck Neidhoefer, Project Director" width="200" /><br />
            <small>Chuck Neidhoefer, <br/>Project Director</small><br />
            <br />
            
      </div>
      <p>Napa Valley Unified School District (NVUSD) is located in the heart of world-famous wine country, an hour north of San Francisco, where the local economy centers on agriculture and tourism. The district serves 18,100 K-12 students: 51 percent Latino, 32 percent White, and 8 percent Asian or Filipino. Of the 8,500 elementary students, 25 percent attend four established charter programs, and 15 percent attend four new magnet schools, which range in size from 190 students to 480 students. This is NVUSD’s first Magnet Schools Assistance Program (MSAP) grant. Through this grant, NVUSD has made progress towards its desegregation and academic achievement goals.</p>
<p>NVUSD’s mission is to transform lives by instilling 21st century skills and inspiring lifelong learning in every student. Coordinated systems of intervention provide teacher teams with the data necessary to ensure the success of struggling students and to close the achievement gap. The district’s commitment to positive behavior intervention enables NVUSD to support the academic and emotional development of each child.
</p>
     <p>In support of 21st century skills, NVUSD’s magnet schools were among the first to train their entire faculties in project-based learning. Lead teachers now contribute to a national effort to develop kid-friendly rubrics for using and measuring “the 4 Cs” (communication, collaboration, creativity, and critical thinking).
The four elementary-level magnet schools support college and career readiness by being among the first to use the California Common Core State Standards to create compelling, integrated curricular units. The MSAP grant supports three new academic programs and the expansion of a fourth program.</p>
	<ul class="list_space">
        <li>Alta Heights MAST (math, science, art, and technology) Academy integrates art into English language arts and utilizes technology as a teaching and learning tool throughout the day.</li>
        <li>Vista 360 at Pueblo Vista is an environmental science school.</li>
        <li>Salvador Elementary, an arts integration magnet school, uses Artful Learning, a program of the Leonard Bernstein Center.</li>
        <li>The International Baccalaureate Primary Years Program was expanded to a second district site at Bel Aire Park Magnet School.</li>
    </ul>
<p>Project Director Chuck Neidhoefer has more than 20 years of experience as a California educator. He has worked at the elementary, secondary, and district levels as a teacher and an administrator in start-up, charter, traditional, magnet, and private school settings. A member of the original MSAP grant-writing team, he has led the implementation of the current grant from its inception. He is motivated by shaping educational systems that ensure service to parents, support for teachers, and engagement of students.
</p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
