﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Independent School District 197</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
          <br />
            <br />
            <img src="../images/gc102011/snowshoe.jpg" alt="Heritage E-STEM Middle School students conduct a study of prairie ecosystems" width="200" height="256"
                                border="0"  /><br />
        <small>Heritage E-STEM Middle School students conduct a study of prairie ecosystems </small><br />
            <br />
      <img src="../images/gc102011/kids.jpg" alt="three children in white coats" /><br />
            <small>Pilot Knob STEM School students conduct a science experiment</small>
          <br />
            <br />
        <img src="../images/gc102011/marilynn-smith.jpg" alt="Marilynn Smith" /><br />
            <small>Marilynn Smith, Independent School District 197 Project Director</small>
            <br />
            
      </div>
        <p>Independent School District 197 (District 197) is located on the urban fringe of St. Paul, Minnesota. District 197 serves nearly 4,500 students within one high school, two middle schools, and five elementary schools. </p>
        <p>The Magnet Schools Assistance Program (MSAP) project has been implemented within one middle school and two elementary schools in District 197. All three schools emphasize rigorous, standards-based instruction; use an inquiry-based instructional model and interdisciplinary curriculum; and feature engaging, hands-on projects and active learning environments.&nbsp; </p>
        <p>In Heritage E-STEM Middle School, with 725 students, the environment serves as the integrating context for a Science, Technology, Engineering, and Mathematics (STEM) magnet theme. Students are actively engaged in project-based learning using Project Lead the Way in engineering. Students are also conducting a yearlong water study at Dodge Nature Center. Their findings will be presented to local government agencies. Heritage E-STEM students participate in a one-to-one iPad deployment initiative, which will have significant impact on the level of student engagement as well as on personalized learning.&nbsp; The school’s partners are Dodge Nature Center and the City of West St. Paul.</p>
        <p>Pilot Knob STEM School, with 300 students, also has a STEM magnet theme. Pilot Knob students are engaged in STEM Sparks classes one day per week. These classes, which relate to the magnet theme and students’ unique personal interests, offer students choices in learning opportunities. On STEM Sparks day, students transition to their class selections and learn about a wide variety of subjects. They explore topics such as space science, bicycles, architecture, animal care, video animation, cooking, and rocketry. Pilot Knob staff integrate interactive technology, including iPads, white boards, and digital media tools.&nbsp; Pilot Knob’s major partners are Lockheed Martin and the Science Museum of Minnesota.</p>
      <p>Moreland Academy of Arts and Health Sciences, with 400 students, has an arts and health sciences magnet theme. Moreland students are actively engaged in learning about how healthy life habits affect their learning. Moreland has an academic climbing wall and an indoor track for movement breaks. In addition, the arts are expanding in each classroom, with artist-in-residence experiences in both dance and visual arts. Moreland students are also involved with technology; kindergarteners, for example, use digital cameras to make alphabet books for the weekly show Moreland Today that is produced by third and fourth graders. This spring Moreland launched a drive to provide a bike to every student who wanted one for the summer, resulting in the donation of more than 60 bikes to families.&nbsp; Partners for Moreland are the Ordway Center for the Performing Arts, the Minnesota Orchestra, Stage Theatre, Dakota County Public Health, and the YMCA.&nbsp; </p>
      <p>Project Director Marilynn Smith, who has classroom teaching experience in a magnet school, administers the MSAP project. Her leadership experience includes facilitating teacher development for educators working with English language learners, overseeing literacy across the content areas, and using Understanding by Design to build standards-based units that reflect an integrated content approach. There are also magnet leaders at each school who work together to support all staff in transforming their instructional practices and curricula. District 197’s approach is to collaborate across school buildings to build the capacity of all staff to implement the magnet theme.      </p>
      <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
