﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Lafayette Parish School System</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
           
             <img src="../images/gc062013/ThreeStudents.png" alt="Three students work with trays of papyrus strips." width="200" /><br />
            <small>David Thibodaux 6th grade World History students process papyrus, a reed plant used by Egyptians as paper.</small><br />
            <br />
          	
            <img src="../images/gc062013/StudentWithGoggles.png" alt="A student wearing goggles stands in front of a beaker being heated." width="200" /><br />
            <small>David Thibodaux 11th grade student conducts a chemistry lab experiment on formation of a salt."</small><br />
            <br />
            
               <img src="../images/gc062013/TwoStudents.png" alt="Two students sitting at a desk discuss schoolwork." width="200" /><br />
            <small>Paul Breaux World Language Academy students in a Spanish Immersion Science class working on the law of conservation of matter. </small><br />
            <br />
           
            
            <img src="../images/gc062013/ProjectDirector.png" alt="Alicia Caesar, MSAP Project Director" width="200" /><br />
            <small>Alicia Caesar, <br/>MSAP Project Director</small><br />
            <br />
            
      </div>
      <p>Located in “Cajun Country” in south central Louisiana, the Lafayette Parish School System (LPSS) has 43 elementary, middle, and high schools that serve 30,451 culturally diverse students. Twenty-three schools are open to all students as schools of choice. These schools offer specialized programs or themes that allow students to focus on their areas of interest while receiving instruction in the core curriculum.</p> 

<p>LPSS received Magnet Schools Assistance Program (MSAP) funding for four schools. The schools include four themes: biomedical; International Baccalaureate (IB)/gifted; science, technology, engineering and math (STEM); and world languages.</p>

<p>JW Faulk Elementary, an IB Candidate, is the only elementary school in the grant. To improve student achievement and increase diversity, JW Faulk has embraced the fundamental guidelines of IB, encouraging students to make connections and to become global thinkers. Because IB instruction incorporates gifted strategies, teachers have partnered with the University of Louisiana at Lafayette (ULL) to receive professional development on best practices for teaching gifted students.</p>
<p>The Biomedical Academy at Carencro Middle School offers an exciting, innovative learning environment designed to prepare students who are passionate about science and medicine for success in biological sciences and related fields. Through partnerships with the Louisiana State University AgCenter School Garden Initiative, the ULL College of Nursing, the Louisiana Organ Procurement Agency, and the Lafayette General Medical Center Epidemiology Department, students learn about lifestyle choices, childhood obesity, and the prevention and treatment of diseases. Students also campaign against obesity and diseases in school and in their community. Partnerships with the Vermilionville Living History Museum and Folklife Park and Rök Haus Climbing Gym extend classroom learning into real-world activities that help students apply medical concepts. The Academy’s mission is to guide students to think critically and creatively, to care about community, and to develop a sense that their opinions matter and that each of them can make a difference.</p>
<p>Paul Breaux World Languages Academy offers French and Spanish language immersion classes, including in the core subjects, to 6-8th graders. Partnerships, with Vermilionville Center for Cultural Heritage Studies, the Asociación Cultural Latino-Acadiana, and Les Amis de L'Immersion, provide professional development in instructional strategies and support teacher efforts in language studies. Paul Breaux aims to prepare students to succeed in a world that extends beyond parish, state, and national boundaries; to understand diversity; and to become successful contributing citizens.</p>
<p>David Thibodaux STEM Magnet Academy serves students in grades 6-12. The engaging and innovative STEM curriculum teaches students to investigate, explore, experiment, problem solve, and invent. Community partnerships, with 100 Black Men of America, the ULL Engineering Department, and Louisiana Immersive Technologies Enterprise, further enhance real-world experiences by offering student support and teacher professional development through on-site demonstrations and hands-on delivery. STEM at David Thibodaux means more than science, technology, engineering, and math; it also means Strong Teachers Empowering Minds.</p>
<p>Alicia Caesar, MSAP Project Director, has served LPSS for 18 years. Her prior positions include classroom instructor, administrator, Project Director for the Smaller Learning Communities Grant, and Career Coordinator. Her professional career has been dedicated to working with students as they transition from high school to adult life, helping them prepare for jobs and/or postsecondary education. As she continues her work in education, Ms. Caesar will continue to help students make informed choices as they prepare for the future.</p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
</asp:Content>
