﻿<%@ page title="" language="C#" masterpagefile="~/magnetpub.master"%>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Grantee Corner&nbsp; |&nbsp; <span style="color: #666;">Springfield Public Schools</span></h1>
        <div style="margin-top: -20px; float: right; margin-left: 20px; width: 200px;">
            
	    <img src="../images/SPSmountain.jpg" alt="Children posing with teacher on a mountain hike." /><br />
            <small>Springfield Renaissance School students on mountain expedition in Maine</small><br />
            <br />
            <img src="../images/SPSband.jpg" alt="Students in Springfield Central High School band pose for the camera." /><br />
            <small>Springfield Central High School students at band practice</small><br />
            <br />
            <img src="../images/SPSrunning.jpg" alt="Young children chasing butterflies with nets." /><br />
            <small>Brookings Expeditionary Learning and Museum Magnet School students study butterflies</small><br />
            <br />
            <img src="../images/SPSJoshBogin.jpg" alt="Project Director, Joshua Bogin" /><br />
            <small>Joshua Bogin, Springfield Public Schools Project Director</small></div>
	    
        <p>
	Springfield Public Schools (SPS) in Massachusetts consists of 52 elementary, middle, and high schools with a total enrollment of 25,360 students within a 32 square mile radius. SPS aims to "raise the bar and close the gap" with its mission to provide the highest quality of education so all students are empowered to realize their full potential and lead fulfilling lives as lifelong learners and responsible citizens and leaders. 
	</p>

	<p>
	SPS is using its grant funds to implement <em>Scaffolding and Accelerating in Diverse Learning Environments</em> in two high schools and two middle schools. This program combines the strongest elements of magnet school innovation- Science, Technology, Engineering, and Mathematics (STEM), International Baccalaureate (IB), Arts, Expeditionary Learning, Advanced Studies, and 21st Century Global Communication modalities.  It also addresses two imperatives: 1) making school relevant, engaging, challenging, and meaningful and 2) ensuring that <strong>all</strong> students, particularly second language learners and low-income students, are equipped with the tools and sense of equal standing that will enable them to succeed at the highest academic levels in secondary school and beyond.  These imperatives will be supported through active mentoring from middle school through college and into the workforce. 
	</p>
	
	<p>
	Listening to the voices of parents, students, educators, and business partners from across Springfield, the magnet programs are carefully crafted to meet their expressed academic interests and needs.  STEM Middle Academy will partner with Expeditionary Learning to deepen students' connections to specific STEM fields through attention to improved, active instructional practice and development of a personalized schoolwide culture. Van Sickle Middle School will partner its IB Middle Years Programme with attention to 21st  Century Global Communications, a theme also embraced by the High School of Science and Technology. The 21st  Century Global Communications theme is the umbrella for these two schools and will keep the focus at all times on the importance of making the core curriculum interesting and relevant to all groups of students, while providing students with the tools to be successful in the 21st  Century global marketplace. At Springfield Central High School, Arts and Advanced Studies are themes intended to attract and engage all students with high level academic work. The program will use the Arts as a centerpiece for culture-building across all student groups and will provide appropriate scaffolding to move more students into Honors, Pre-Advanced Placement, and Advanced Placement classes.
	</p>
	
	<p>
	The four schools are currently supported by a variety of business and community partners such as: the National Academy Foundation, College Board, Latino STEM Alliance, New England Equity Assistance Center, colleges and universities throughout Western Massachusetts and northern Connecticut, International Baccalaureate Organization, Baystate Medical Center, MassMutual Financial Group, Bay State Health, Project Lead the Way, Pioneer Valley Home Visit Project, and local arts groups. 
	</p>
	
	<p>
	<em>Scaffolding and Accelerating in Diverse Learning Environments</em> in Springfield Public Schools is administered by Joshua P. Bogin, the Project Director. He has 14 years of experience administering federal and other grant programs at the school district level, and broad additional experience in promoting equity (first as a civil rights lawyer and later as a technical assistance provider to school districts) in the delivery of educational programs. Mr. Bogin provides the district a breadth of understanding to bring forward the complementary goals of equity and excellence through the innovations proposed in <em>Scaffolding and Accelerating in Diverse Learning Environments.</em>
	</p>
<p><a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>

    </div>
</asp:Content>
