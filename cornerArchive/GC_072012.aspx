﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
      <div class="mainContent">
        <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Capitol Region Education Council</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 206px;text-align:center;">
          <br />
            <br />
         <img src="../images/gc072012/Hartford_magnet_TrinityVisitCollege.jpg" alt="Hartford Magnet Trinity College Academy students look at a map of the Trinity College campus during a visit." width="200" /><br />
            <small>Hartford Magnet Trinity College Academy students visit Trinity College.</small><br />
            <br />
         <img src="../images/gc072012/Museum.jpg" alt="Two young CREC Museum Academy students work on puzzles." width="200" /><br />
            <small>CREC Museum Academy students work on puzzles.</small><br />
            <br />
            <img src="../images/gc072012/AA_N_E.jpg" alt="A student and teacher discuss a science experience in a lab at Academy of Aerospace and Engineering." width="200" /><br />
            <small>Academy of Aerospace and Engineering student and teacher discuss a science experiment.</small><br />
            <br />
            <img src="../images/gc072012/MPTPA.jpg" alt="CREC Medical Professions and Teacher Preparation Academy sixth graders dissect a pig's heart with MSAP Project Director Christine Ruman looking on." width="200" /><br />
            <small>CREC Medical Professions and Teacher Preparation Academy sixth graders dissect a pig's heart.</small><br />
            <br />
         <img src="../images/gc072012/ChristineRuman.jpg" alt="Christine Ruman, Capitol Region Education Council Project Director" width="200" /><br />
            <small>Christine Ruman,<br/> Capitol Region Education Council<br/> Project Director</small><br />
            <br />
      </div>
            <p>The Capitol Region Education Council (CREC) and Hartford Public Schools (HPS) make up a Magnet Schools Assistance Program (MSAP) consortium between two school districts. This consortium’s eight MSAP schools have been developed to assist Connecticut in meeting required school desegregation goals in the Hartford region, where 92 percent of students are minorities and 91 percent qualify for the free or reduced-price meal program. MSAP funds give the consortium the opportunity to move forward with a shared mission of quality, desegregated education.</p>
<p>The CREC-HPS magnet school themes range from museum studies to science, technology, engineering, and math (STEM), to early college. Combined, these schools serve K to 12th graders.  The eight MSAP schools in the consortium have experienced a number of successes.</p>
<p>While providing students with unique theme-based experiences in grades 6-12, the Medical Professions and Teacher Preparation Academy (MPTPA) is setting high academic standards for its students. MPTPA is one of 21 high schools in four states that are participating in the national “Excellence for All” pilot program. The schools are implementing the Cambridge International General Certificate of Secondary Education (IGCSE) curriculum, which focuses on learners’ skills in creative thinking, inquiry, and problem solving.  MPTPA students will join students from more than 100 countries to take the Cambridge IGCSE assessments, which are recognized by many higher education institutions and employers as evidence of academic ability benchmarked against international standards. </p>
<p>At the Museum Academy, student work is the focus. At its first Exhibition Night, an event that showcased student work completed during the first months of the school year, the Academy displayed hundreds of projects to over 400 friends and family members. The theme of the exhibition was “A Day in the Life,” and three more exhibitions will follow: “World in My Backyard,” “Walk on the Wild Side,” and “Life’s Way.”</p>
<p>A group of students from Pathways to Technology High School, a project-based learning and technology magnet school, received top honors in the Network for Teaching Entrepreneurship World Series of Innovation competition. The team won the innovative beverage category for a caffeine-free, tropical fruit juice-infused line of ginger ale soft drinks. Microsoft hosts the competition to groups of five or more students worldwide. The purpose of the competition is to create innovative products and services that address everyday opportunities. </p>
<p>The MSAP grant is managed collaboratively by CREC and Hartford Public Schools, and is administered by project director Christine Ruman. Her 15 years of experience in education include program management and evaluation, and policy development and implementation. She worked on the Connecticut State Department of Education team charged with carrying out the state’s responsibilities under the Sheff v. O’Neill desegregation case. Her experience working with school and district improvement, diverse groups of students and families, and grant administration enables her to support the eight MSAP schools as they develop magnet programs that promote high levels of achievement for all students.
</p>
      <p>
      <a href="../cornerarchives.aspx">See Grantee Corner Archive</a></p>
    </div>
</asp:Content>
