﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Grantee Corner
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
 <div class="mainContent">
               <h1>
            Grantee Corner &nbsp;| <span style="color: #666; line-height: 120%;">&nbsp;Desert Sands Unified School District</span></h1>
        <div style="margin-top: -60px; float: right; margin-left: 20px; width: 200px;text-align:center;">
            <br />
            <br/>
           
         <img src="../images/gc022013/Singapore_math_lesson.jpg" alt="Four Math Science Magnet School students review a Singapore Math lesson at the front of the classroom." width="200" /><br />
            <small>John F. Kennedy Math Science Magnet School students review a Singapore Math lesson.</small><br />
            <br />
            
             <img src="../images/gc022013/student_performance.png" alt="4th grade students perform 'Westward Ho', an integrated curricular performance." width="200" /><br />
            <small>Carrillo Ranch Magnet Academy for the Arts 4th grade students perform <i>Westward Ho</i>, an integrated curricular performance.</small><br />
            <br />
         
               <img src="../images/gc022013/Student_and_Vernier Probe.JPG" alt="Students monitor water temperature for a science experiment using state-of-the-art equipment." width="200" /><br />
            <small>Students monitor water temperature for a science experiment using state-of-the-art equipment.</small><br />
            <br />
            
     
            <img src="../images/gc022013/student_performance3.jpg" alt="Tap students performing dressed up as reindeer at the Mall" width="200" /><br />
            <small>Carrillo Ranch Magnet Academy for the Arts tap students give back to the community by performing at the Westfield Palm Desert Mall.</small><br />
            <br />
            
             <img src="../images/gc022013/tile_wall.JPG" alt="A wall of colorful tiles made by 5th grade students to enhancing the permanent outdoor exhibit." width="200" /><br />
            <small>Carrillo Ranch Magnet Academy for the Arts 5th grade students created this tile wall for the arts as their contribution to enhancing the permanent outdoor exhibit.</small><br />
            <br />
            
            <img src="../images/gc022013/Valerie_Celaya.jpg" alt="Valerie Celaya, Project Director" width="200" /><br />
            <small>Valerie Celaya, <br/>Project Director</small><br />
            <br />
            
      </div>
     <p>The Desert Sands Unified School District (DSUSD) serves seven communities approximately 120 miles east of Los Angeles in the Southern California desert. The district serves 29,224 students in 20 elementary schools, 7 middle schools, 4 comprehensive high schools, 2 continuation schools, and 1 independent study school.</p>
     <p>DSUD has ten magnet schools, two of which are currently funded  by the Magnet Schools Assistance Program. DSUSD offers rigorous academic programs, community partnerships to foster real-world learning, and cutting-edge technology. DSUSD is a district of choice and ensures that every student develops the knowledge, skills, and motivation to succeed as a productive, ethical, and global citizen. The district assures equal access to student-centered learning provided by caring, committed staff who work in partnership with families and diverse communities. Magnet schools fit perfectly with this mission by offering thematic curriculum desired by parents, students, and staff.</p>
     <p>John F. Kennedy Elementary Math and Science Magnet School (JFK) was recently modernized to create an attractive environment that includes outdoor math and science learning experiences. JFK boasts a variety of theme-based programs: Singapore Math, Lego Robotics, science labs, and a garden lab. The integrated math and science curriculum encourages student learning through inquiry and investigations with hands-on, highly visual instruction. </p>
     <p>JFK partners with community organizations in the fields of engineering, agriculture, and environmental sciences to provide opportunities for real-life math and science applications with analysis and investigation. Community partnerships have led to projects such as a schoolwide recycling program, designing and building a replica of the Command Module from Apollo 11, and developing a FEMA-inspired student earthquake safety team called the Tremor Troop Team.</p>
<p>Visual and performing arts dominate the Carrillo Ranch Elementary Magnet Academy for the Arts campus. Beautiful murals, a student-created tile wall, and other student art adorn the space inside and out. Children participate in dance classes, drama and theatre productions, and painting and ceramics activities. A professional dance studio with portable barres and shatterproof mirrors, a state-of-the-art keyboard lab, dedicated classrooms for art and ceramics, two kilns, and professional-grade sound and lighting systems in the multipurpose room create environments that help students delve into the arts and expand their learning exponentially. Students can also participate in choir, band, and keyboard classes.</p>
<p>All students at Carrillo Ranch receive instruction in and exposure to music, dance, drama, and art. After being exposed to all genres in grades K-3, students in grades 4 and 5 choose “majors” such as band, choir, drama, dance, ceramics/mural painting, art, or keyboard. They are enrolled in their majors all year and rotate through “minors” each trimester. Partnerships with the McCallum Theatre, Breath of Life Arts, and Music Educational Services bring a variety of professional artists to the students as teachers and mentors.</p>
<p>Highly qualified teaching staffs at both schools use research-based teaching strategies to enhance the curriculum and meet the diverse needs of their 21st century learners. Project Director Valerie Celaya applies her 16 years of education experience to work collaboratively with school personnel, parents, students, and the community for successful, student-centered magnet schools. Both magnet schools have drawn the attention of dedicated local community organizations that contribute to sustainability. </p>
      <p>
      	<a href="../cornerarchives.aspx">See Grantee Corner Archive</a>
      </p>
    </div>
  </asp:Content>
