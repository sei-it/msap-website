﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="grantees.aspx.cs" Inherits="grantees" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - MSAP Grantees
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function Fetchdata(strField) {
            if ($("#<%= rbtnlstCohort.ClientID %> input:checked").val() == "2010") {
                $('#<%= SortFieldName.ClientID %>').val(strField);
                __doPostBack('ctl00$ContentPlaceHolder1$btnSearch', '');
            }
            else {
                $('#<%= SortFieldName2013.ClientID %>').val(strField);
                __doPostBack('ctl00$ContentPlaceHolder1$btnSearch2013', '');
            }
                
        }


        function SubmitForm() {
            if ($("#<%= rbtnlstCohort.ClientID %> input:checked").val() == "2010") {
                $('#<%=btnSearch.ClientID %>').click();
            }
            else {
                $('#<%=btnSearch2013.ClientID %>').click();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a> 
    <asp:RadioButtonList ID="rbtnlstCohort" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
    <asp:ListItem Value="2010" >MSAP 2010 Cohort</asp:ListItem>
    <asp:ListItem Value="2013" Selected="True">MSAP 2013 Cohort</asp:ListItem>
    </asp:RadioButtonList>
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:Panel ID="pnlCohort2010" runat="server">
    <div class="mainContent">
        <h1>
            2010 MSAP Grantee Cohort</h1>
        <p>
            Click a state on the map below or select a state from the dropdown menu to the right, then click a grantee's name to read the abstract.  To learn more about the 2010 MSAP cohort, visit the <a href="cornerarchives.aspx">Grantee Corner Archive</a>.
        </p>
        <p style="text-align: right">
            <asp:DropDownList ID="ddlStates" runat="server" OnSelectedIndexChanged="OnSateChanged"
                AutoPostBack="true">
                <asp:ListItem>Please select</asp:ListItem>
                <asp:ListItem Value="Arizona">Arizona</asp:ListItem>
                <asp:ListItem Value="California">California</asp:ListItem>
                <asp:ListItem Value="Colorado">Colorado</asp:ListItem>
                <asp:ListItem Value="Connecticut">Connecticut</asp:ListItem>
                <asp:ListItem Value="Florida">Florida</asp:ListItem>
                <asp:ListItem Value="Indiana">Indiana</asp:ListItem>
                <asp:ListItem Value="Illinois">Illinois</asp:ListItem>
                <asp:ListItem Value="Kansas">Kansas</asp:ListItem>
                <asp:ListItem Value="Louisiana">Louisiana</asp:ListItem>
                <asp:ListItem Value="Massachusetts">Massachusetts</asp:ListItem>
                <asp:ListItem Value="Minnesota">Minnesota</asp:ListItem>
                <asp:ListItem Value="Mississippi">Mississippi</asp:ListItem>
                <asp:ListItem Value="New York">New York</asp:ListItem>
                <asp:ListItem Value="South Carolina">South Carolina</asp:ListItem>
                <asp:ListItem Value="Tennessee">Tennessee</asp:ListItem>
                <asp:ListItem Value="Texas">Texas</asp:ListItem>
            </asp:DropDownList>
        </p>
        <div style="text-align: center; margin: auto;">
            <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="550"
                height="310">
                <param name="movie" value="fla/msap_map.swf" />
                <param name="quality" value="high" />
                <param name="wmode" value="opaque" />
                <param name="swfversion" value="6.0.65.0" />
                <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                <param name="expressinstall" value="Scripts/expressInstall.swf" />
                <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="fla/msap_map.swf" width="550"
                    height="310">
                    <!--<![endif]-->
                    <param name="quality" value="high" />
                    <param name="wmode" value="opaque" />
                    <param name="swfversion" value="6.0.65.0" />
                    <param name="expressinstall" value="Scripts/expressInstall.swf" />
                    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                    <div>
                        <h4>
                            Content on this page requires a newer version of Adobe Flash Player.</h4>
                        <p>
                            <a href="http://www.adobe.com/go/getflashplayer">
                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
                                    alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                    </div>
                    <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        </div>
        <asp:Button ID="btnSearch" runat="server" Text="Sort" OnClick="Buttons_Click" Style="visibility: hidden" />
        <asp:HiddenField runat="server" ID="SortFieldName" />
        <p style="text-align: center">
            <asp:Label ID="lblState" runat="server" CssClass="tblHeader"></asp:Label>
        </p>
        <div class='grnDiv'>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
                AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" GridLines="None"
                Width="400">
                <RowStyle BorderStyle="None" BorderColor="White" />
                <HeaderStyle BorderStyle="None" BorderColor="White" />
                <Columns>
                    <asp:HyperLinkField Target="_blank" DataTextField="Grantee" DataNavigateUrlFields="ProgramTitleLink"
                        HeaderText="Grantee" ItemStyle-Width="70%" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="City" SortExpression="" HeaderText="City" ItemStyle-Width="30%"
                        ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="ProgramTitle" SortExpression="" HeaderText="Project Title"
                        ItemStyle-Width="60%" Visible="false" ItemStyle-HorizontalAlign="Left" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
    </asp:Panel>
    <asp:Panel ID="pnlCohort2013" runat="server">
     <div class="mainContent">
        <h1>
            2013 MSAP Grantee Cohort</h1>
        <p>
            Click a state on the map below or select a state from the dropdown menu to the 
            right, then click a grantee&#39;s name to read the abstract. To learn more about the 
            2013 MSAP cohort, visit the <a href="cornerarchives.aspx">Grantee Corner Archive</a>.
        </p>
        <p style="text-align: right">
            <asp:DropDownList ID="ddlState2013" runat="server" OnSelectedIndexChanged="OnSateChanged2013"
                AutoPostBack="true">
                <asp:ListItem>Please select</asp:ListItem>
                <asp:ListItem Value="Arkansas">Arkansas</asp:ListItem>
                <asp:ListItem Value="California">California</asp:ListItem>
                <asp:ListItem Value="Colorado">Colorado</asp:ListItem>
                <asp:ListItem Value="Connecticut">Connecticut</asp:ListItem>
                <asp:ListItem Value="Florida">Florida</asp:ListItem>
                <asp:ListItem Value="Kansas">Kansas</asp:ListItem>
                <asp:ListItem Value="Massachusetts">Massachusetts</asp:ListItem>
                <asp:ListItem Value="Michigan">Michigan</asp:ListItem>
                <asp:ListItem Value="Mississippi">Mississippi</asp:ListItem>
                <asp:ListItem Value="New York">New York</asp:ListItem>
                <asp:ListItem Value="North Carolina">North Carolina</asp:ListItem>
                <asp:ListItem Value="South Carolina">South Carolina</asp:ListItem>
                <asp:ListItem Value="Texas">Texas</asp:ListItem>
            </asp:DropDownList>
        </p>
        <div style="text-align: center; margin: auto;">
            <object id="Object1" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="550"
                height="310">
                <param name="movie" value="fla/msap_map10-2014.swf" />
                <param name="quality" value="high" />
                <param name="wmode" value="opaque" />
                <param name="swfversion" value="6.0.65.0" />
                <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                <param name="expressinstall" value="Scripts/expressInstall.swf" />
                <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="fla/msap_map10-2014.swf" width="550"
                    height="310">
                    <!--<![endif]-->
                    <param name="quality" value="high" />
                    <param name="wmode" value="opaque" />
                    <param name="swfversion" value="6.0.65.0" />
                    <param name="expressinstall" value="Scripts/expressInstall.swf" />
                    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                    <div>
                        <h4>
                            Content on this page requires a newer version of Adobe Flash Player.</h4>
                        <p>
                            <a href="http://www.adobe.com/go/getflashplayer">
                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
                                    alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                    </div>
                    <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        </div>
        <asp:Button ID="btnSearch2013" runat="server" Text="Sort" OnClick="Search2013_Click" Style="visibility: hidden" />
        <asp:HiddenField runat="server" ID="SortFieldName2013" />
        <p style="text-align: center">
            <asp:Label ID="lblState2013" runat="server" CssClass="tblHeader"></asp:Label>
        </p>
        <div class='grnDiv'>
            <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AllowSorting="false"
                AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl" GridLines="None"
                Width="400">
                <RowStyle BorderStyle="None" BorderColor="White" />
                <HeaderStyle BorderStyle="None" BorderColor="White" />
                <Columns>
                    <asp:HyperLinkField Target="_blank" DataTextField="Grantee" DataNavigateUrlFields="ProgramTitleLink"
                        HeaderText="Grantee" ItemStyle-Width="70%" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="City" SortExpression="" HeaderText="City" ItemStyle-Width="30%"
                        ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="ProgramTitle" SortExpression="" HeaderText="Project Title"
                        ItemStyle-Width="60%" Visible="false" ItemStyle-HorizontalAlign="Left" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
     </asp:Panel>
</asp:Content>
