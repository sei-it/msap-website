﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="newsletters.aspx.cs" Inherits="newsletters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - The Magnet Compass
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="brnImg">
        <img src="images/newsHeader2.jpg" width="842" alt="Text" style="border-bottom: 1px solid #CCC;" />
    </div>
    <div class="mainContent" style="margin-top: -5px;">
        <div>
            <div style="margin-top: 10px; float: right; margin-left: 15px; padding-left: 15px;
                width: 200px; border-left: 1px solid #CCC;">
                <p class="expHdr" style="color: #F60;">
                    Recent Issues</p>
               <p>
                    <strong>May 2011</strong><br />
                    School Improvement<br />
                    <a href="doc/MSAP_Magnet_Compass_051311_508.pdf" target="_blank">
                        <img src="images/msapMay2011.jpg" alt="Magnet Compass button featuring an image from the May 2011 issue" width="129" height="75"
                            style="border: 1px solid #CCC;" /></a>
              </p>
               
          <p>
                    <strong>April 2011</strong><br />
                    Fidelity of Implementation<br />
                    <a href="doc/MSAP_Magnet_Compass_Vol1_Is3_0411.pdf" target="_blank">
                        <img src="images/msapApril2011.jpg" alt="Magnet Compass button featuring an image from the April 2011 issue" width="129" height="75"
                            style="border: 1px solid #CCC;" /></a>
                </p>
                <p>
                    <strong>March 2011</strong><br />
                    Family Engagement<br />
                    <a href="doc/MSAP_Magnet_Compass_Vol1_Is2_0311.pdf" target="_blank">
                        <img src="images/msapMarch2011.jpg" alt="Magnet Compass button featuring an image from the March 2011 issue" width="129" height="75"
                            style="border: 1px solid #CCC;" /></a>
                </p>
                <p>
                    <strong>February 2011</strong><br />
                    Program Sustainability<br />
                    <a href="doc/MSAP_Magnet_Compass_Vol1_Is1_0211.pdf" target="_blank">
                        <img src="images/msapFeb2011.jpg" width="129" alt="Magnet Compass button featuring an image from the February 2011 issue"
                            style="border: 1px solid #CCC;" /></a>
                </p>
            </div>
            <div style="width: 550px; float: left;">
                <h1 style="border:none;"><span class="archiveTxt1">The Magnet Compass</span> <span class="archiveTxt2">|</span> <span class="archiveTxt3">Archive</span></h1>
                <h2 style="font-size:1.2em;">
                    This newsletter will serve as a compass on your pathway to improve your magnet program.</h2>
                <p>
                    Superintendents, administrators, program directors, principals, teachers, parents,
                    and other stakeholders with a vested interest in magnet programs will find exemplary
                    resources, effective practices, research-based content, and innovative ideas for
                    implementing, managing and sustaining magnet programs. In these ways, the MSAP Center
                    will meet the ongoing needs of the MSAP grantees and the larger magnet schools community.
                </p>
                <p>
                    We thank all MSAP grantees and those in the greater magnet schools community for
                    your dedication and commitment to our students, and we commend your pursuit of diversity,
                    academic excellence, and equity through magnet schools.
                </p>
                <p>
                    <strong>"Remember to always keep The Magnet Compass by your side!"</strong>
                </p>
                <p style="margin-bottom: -1em;">
                    <span style="font-weight: bold; color: #f60;">Subscribe</span></p>
                <p>
                    Sign up for our monthly newsletter.<br />
                    <asp:TextBox ID="txtSubscription" runat="server" CssClass="msapTxt newsRegister"></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" Text="Sign Up!" CssClass="msapBtn" OnClick="OnSignup" /><br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSubscription"
                        ErrorMessage="Please type in a valid Email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </p>
            </div>
        </div>
        <script type="text/javascript">            $('#ctl00_ContentPlaceHolder1_txtSubscription').watermark('E-mail address', { className: 'lightWaterClass' });
    </script>
</asp:Content>
