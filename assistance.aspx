﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="assistance.aspx.cs" Inherits="assistance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register TagPrefix="cc1" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Technical Assistance Request Form
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Technical Assistance Request Form</h1>
        <p>
            To request technical assistance (TA), simply fill in the fields below. Required
            fields are marked with an asterisk (<span class="redFont">*</span>).</p>
        <p>
            Include your preferred contact method and the best times you can be reached in case
            someone from our staff needs to speak with you directly.</p>
        <p>
            The TA Request Form was designed to be as simple as possible so we can help you
            as quickly as possible. All TA requests are electronically routed directly to MSAP
            Center staff. All TA requests will be responded to within 2 business days.
        </p>
        <table>
            <tr>
                <td>
                    <span class="redFont">*</span>First name:
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="msapTxt" MaxLength="500"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtFirstName"
                        Text="This field is required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="redFont">*</span>Last name:
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="msapTxt" MaxLength="500"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLastName"
                        Text="This field is required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="redFont">*</span>Title:
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="msapTxt" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtTitle"
                        Text="This field is required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="redFont">*</span>District name:
                </td>
                <td>
                    <asp:TextBox ID="txtDistrict" runat="server" CssClass="msapTxt" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDistrict"
                        Text="This field is required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Address line 1:
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="msapTxt" MaxLength="250"></asp:TextBox>

                </td>
            </tr>
            <tr>
                <td>
                    Address line 2:
                </td>
                <td>
                    <asp:TextBox ID="txtAddressCont" runat="server" CssClass="msapTxt" MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    City:
                </td>
                <td>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="msapTxt" MaxLength="250"></asp:TextBox>
                    
                </td>
            </tr>
            <tr>
                <td>
                    State(Choose a State/Province):
                </td>
                <td>
                    <asp:DropDownList ID="ddlState" runat="server">
                        <asp:ListItem Value="">Select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    ZIP code:
                </td>
                <td>
                    <asp:TextBox ID="txtZipcode" runat="server" CssClass="msapTxt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="redFont">*</span>Telephone number:
                </td>
                <td>
                    <asp:TextBox ID="txtTelephone" runat="server" CssClass="msapTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtTelephone"
                        Mask="(999)999-9999">
                    </ajax:MaskedEditExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtTelephone"
                        Text="This field is required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Alternate phone number:
                </td>
                <td>
                    <asp:TextBox ID="txtAlternative" runat="server" CssClass="msapTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtAlternative"
                        Mask="(999)999-9999">
                    </ajax:MaskedEditExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Fax number:
                </td>
                <td>
                    <asp:TextBox ID="txtFax" runat="server" CssClass="msapTxt"></asp:TextBox>
                    <ajax:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txtFax"
                        Mask="(999)999-9999">
                    </ajax:MaskedEditExtender>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="redFont">*</span>Email address:
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="msapTxt"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtEmail"
                        Text="This field is required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="Please type in a valid Email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Preferred contact method:
                </td>
                <td>
                    <asp:RadioButtonList ID="rblPreferredMethod" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem>Phone</asp:ListItem>
                        <asp:ListItem>Email</asp:ListItem>
                    </asp:RadioButtonList>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <span class="redFont">*</span>Best time to be reached:
                </td>
                <td>
                    <asp:TextBox ID="txtHour" runat="server" Width="40" CssClass="msapTxt"></asp:TextBox>
                    <asp:DropDownList ID="cblBestTime" runat="server">
                        <asp:ListItem Value="">Select AM/PM</asp:ListItem>
                        <asp:ListItem Value="AM ET">AM ET</asp:ListItem>
                        <asp:ListItem Value="PM ET">PM ET</asp:ListItem>
                    </asp:DropDownList>
                        <asp:Label ID="lblBestTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
            <td colspan="2">
            <cc1:captchacontrol id="CaptchaControl1" runat="server" CaptchaMaxTimeout="300" ></cc1:captchacontrol>
            &nbsp;
           
            </td>
            </tr>
            <tr>
                <td>
                    TA request description:
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <cc2:Editor ID="txtTARequest" runat="server">
                    </cc2:Editor>
                   
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                    
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="Button1" runat="server" CssClass="msapBtn" Text="Submit Request"
                        OnClick="OnSubmit" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="PopupPanel" runat="server">
            <div class="mpeDivHeader">
                Alert!</div>
            <div class="mpeDiv">
                <table>
                    <tr>
                        <td>
                            Your request has been submitted successfully!
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="OK" OnClick="OnOK" />
                           
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="LinkButton7" runat="server"></asp:LinkButton>
        <ajax:ModalPopupExtender ID="mpeWindow" runat="server" TargetControlID="LinkButton7"
            PopupControlID="PopupPanel" DropShadow="true" 
        BackgroundCssClass="magnetMPE" Y="20" />
    </div>
</asp:Content>
