﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="contact.aspx.cs" Inherits="contact" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Contact Us
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
       <h1>Contact Us</h1>
        <div class="expBoxR" style="background: #A9DEF2; margin-bottom: 30px;">
            <div class="topCall">
                &nbsp;</div>
            <div class="midCall2">
                <div class="expTxt" style="padding: 10px 17px;">
                    <span class="expHdr" style="color: #F60;">MSAP Center</span>
                    <p>
                        8757 Georgia Avenue<br />
                        Suite 460<br />
                        Silver Spring, MD 20910<br />
                        1-866-997-MSAP (6727)<br />
                        <a href="mailto:msapcenter@leedmci.com">msapcenter@leedmci.com</a>
                    </p>
                </div>
            </div>
            <div class="botCall">
                &nbsp;</div>
        </div>
        <div style="background: url(images/contact.jpg) bottom right no-repeat;
            width: 600px; height: 275px;">
            <h2>MSAP Center Staff</h2><br />
            <div id="ContentDiv" runat="server"></div>
        </div>
    </div>
</asp:Content>
