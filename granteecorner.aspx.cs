﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class admin_granteecorner : System.Web.UI.Page
{
    MagnetDBDB db = new MagnetDBDB();
    protected void Page_Load(object sender, EventArgs e)
    {
        int gcid = Convert.ToInt32(Request.QueryString["gcid"]);
        if (!Page.IsPostBack)
        {
            var gc = gcid==0 ? db.MagnetGCs.SingleOrDefault(x=>x.isActive) : db.MagnetGCs.SingleOrDefault(x=>x.id == gcid);
            var gcimgs = from rd in db.MagnetGCImages
                         where rd.gc_id == gcid && (rd.isArchiveImage == false || rd.isShowOnHomePage == true)
                         orderby rd.displayOrder
                         select new { id = rd.id, alt_text = rd.alt_text, caption = rd.caption};

            ltlTitle.Text = gc.Title;
            ltlContentHolder.Text = gc.Contents;
            int total = gcimgs.Count();
            int indx = 0;
            foreach (var itm in gcimgs)
            {
                //if (indx == total - 1)
                //{
                //    ltlImagesHolder.Text += " <div id='photolayer' style='z-index: 1;  width: 200px; height: 150px; background-color:#A9DEF2;  padding-top:5px'>";
                //    ltlImagesHolder.Text += "<img src='img/Handler.ashx?GCID=" + itm.id + "' alt='" + itm.alt_text + "' height='145' /><br /><small><div style='padding-top:5px'>" + itm.caption + "</div></small></div><br /><br />";
                //}
                //else
                {
                    ltlImagesHolder.Text += "<img src='img/Handler.ashx?GCID=" + itm.id + "' alt='" + itm.alt_text + "' width='200' /><br /><small>" + itm.caption + "</small><br /><br />";
                }
                    indx++;
            }
        }
    }
}