﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class cornorarchives : System.Web.UI.Page
{
    MagnetDBDB db = new MagnetDBDB();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var gcimgs = from rd in db.MagnetGCImages
                         join gc in db.MagnetGCs on rd.gc_id equals gc.id 

                         where rd.isArchiveImage == true && gc.Cohort == "2010"
                         orderby rd.displayOrder descending
                         select new { gc_id = rd.gc_id, id = rd.id, rd.alt_text} ;

            int i = 1;
            if (gcimgs.Count() > 0)
            {
                ltlNavigationLinks.Text = "<div class='slide'><br /><h2 class='Featured_header'>&nbsp;</h2><p class='Featured_header'>";
            }
            foreach (var item in gcimgs)
            {
                if (i % 4 == 0)
                {
                    ltlNavigationLinks.Text += "</p></div>";
                    i = 1;
                    ltlNavigationLinks.Text += "<div class='slide'><br /><h2 class='Featured_header'>&nbsp;</h2><p class='Featured_header'>";
                }
                ltlNavigationLinks.Text += "<a href='granteecorner.aspx?gcid=" + item.gc_id + "'>";
                ltlNavigationLinks.Text += "<img src='img/Handler.ashx?GCID= " + item.id +"' alt=' " + item.alt_text +"' width='180' height='138' border='none'/></a>";
                i++;
            }
            if (!string.IsNullOrEmpty(ltlNavigationLinks.Text))
                ltlNavigationLinks.Text += "</p></div>";

            var gcimgs2013 = from rd in db.MagnetGCImages
                             join gc in db.MagnetGCs on rd.gc_id equals gc.id

                             where rd.isArchiveImage == true && gc.Cohort == "2013"
                             orderby rd.displayOrder descending
                             select new { gc_id = rd.gc_id, id = rd.id, rd.alt_text };

            i = 1;
            if (gcimgs2013.Count() > 0)
            {
                ltlNavigationLinks2013.Text = "<div class='slide13'><br /><h2 class='Featured_header'>&nbsp;</h2><p class='Featured_header'>";
            }
            foreach (var item in gcimgs2013)
            {
                if (i % 4 == 0)
                {
                    ltlNavigationLinks2013.Text += "</p></div>";
                    i = 1;
                    ltlNavigationLinks2013.Text += "<div class='slide13'><br /><h2 class='Featured_header'>&nbsp;</h2><p class='Featured_header'>";
                }
                ltlNavigationLinks2013.Text += "<a href='granteecorner.aspx?gcid=" + item.gc_id + "'>";
                ltlNavigationLinks2013.Text += "<img src='img/Handler.ashx?GCID= " + item.id + "' alt=' " + item.alt_text + "' width='180' height='138' border='none'/></a>";
                i++;
            }
            if(!string.IsNullOrEmpty(ltlNavigationLinks2013.Text))
                ltlNavigationLinks2013.Text += "</p></div>";
        }
    }
}