﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class conference : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
            LoadEvents(0);
    }
    private static int CompareEventByDate(MagnetPublicEvent x, MagnetPublicEvent y)
    {
        DateTime dt1 = new DateTime();
        DateTime dt2 = new DateTime();
        if (DateTime.TryParse(x.StartDate, out dt1) && DateTime.TryParse(y.StartDate, out dt2))
        {
            return dt1.CompareTo(dt2);
        }
        else
            return 0;
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadEvents(e.NewPageIndex);
    }
    private void LoadEvents(int PageNumber)
    {
        int count = 0;
        var conferences = MagnetPublicEvent.Find(x => x.EventType == true && x.Display == true).ToList();
        conferences.Sort(CompareEventByDate);
        List<ManageUtility.PublicationData> data = new List<ManageUtility.PublicationData>();
        foreach (MagnetPublicEvent conference in conferences)
        {
            count++;
            StringBuilder sb = new StringBuilder();            
            sb.AppendFormat("<a name='event{3}'></a><h2>{0}</h2> <p><strong>Organization</strong>: {1}<br /> <strong>Date</strong>: {2}<br />", conference.EventTitle, conference.Organization, conference.Date, count);
            sb.AppendFormat("<strong>Location</strong>: {0}<br /><strong>Description</strong>: {1}</p>", conference.Location, conference.Description);
            ManageUtility.PublicationData display = new ManageUtility.PublicationData();
            display.DisplayData = sb.ToString();
            data.Add(display);
        }
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
}