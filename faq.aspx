﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="faq.aspx.cs" Inherits="faq" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - FAQs
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            MSAP Center Frequently Asked Questions</h1>
        <h2>
            What is a magnet school?</h2>
        <p>
            A magnet school is a public, elementary or secondary school that offers a special
            curriculum capable of attracting substantial numbers of students from different
            backgrounds. “Magnet” refers to how schools attract students across normal boundaries
            with innovative curricula to desegregate racially isolated schools. Magnet schools
            first developed in the 1970s as a voluntary desegregation tool and an alternative
            to forced busing. After the Federal Court officially endorsed magnet schools as
            a viable desegregation method in 1975-76, the number of magnet schools nearly doubled
            in the 1980s, and they remain popular today.
        </p>
        <p>
            There are three defining characteristics of a magnet program: (1) they offer a distinctive
            curriculum or instructional approach; (2) they attract students from outside assigned
            neighborhood attendance zones; and (3) they include diversity as a stated purpose.
            Magnet schools offer innovative programs through a specialized theme or focus. They
            may emphasize subjects like the arts, science, or math, or they may adopt distinct
            instructional models, such as the Montessori or International Baccalaureate programs.
        </p>
        <p>
            While public schools draw students from predetermined attendance zones, magnet schools
            are not subject to these boundaries. The theory behind magnet schools is that by
            drawing from different neighborhoods, magnet schools will attract students with
            varied backgrounds, thereby creating diverse and engaging learning communities.</p>
        <h2>
            What is the difference between a magnet and a charter school?</h2>
        <p>
            Charter schools have a charter that grants them autonomy, while magnet schools operate
            under the same administration as other public schools. A charter school may, however,
            operate a magnet school program.</p>
        <h2>
            What is the Magnet Schools Assistance Program?
        </h2>
        <p>
            Federal support for magnet schools began in 1972 with the Emergency School Aid Act
            (ESAA), which authorized grants for school districts that were desegregating schools.
            ESAA funding ended in 1983, but support for magnet schools resumed in 1984 with
            the authorization of the Magnet Schools Assistance Program (MSAP).
        </p>
        <p>
            MSAP provides financial support to K-12 public schools to assist in desegregation
            by supporting the elimination, reduction, and prevention of racial group isolation.
            Racial group isolation is determined by each school district or local education
            agency (LEA), so the racially isolated group will vary by school.</p>
        <p>
            MSAP requires grantees to apply special magnet themes and curricula, hire quality
            teachers, implement professional development, and encourage greater parental and
            community involvement. The goal of MSAP’s requirements is to provide students with
            challenging academic content that meets academic achievement requirements and improves
            students’ grasp of tangible and marketable vocational skills. Additionally, MSAP
            grantees must provide equitable processes for program placement and sustain the
            program after MSAP funding is no longer in place.
        </p>
        <h2>
            What is the Magnet Schools Assistance Program Technical Assistance Center?</h2>
        <p>
            Funded by the U.S. Department of Education, Office of Innovation and Improvement (OII), the Magnet Schools Assistance Program Technical Assistance Center (MSAP Center) is dedicated to building MSAP grantees’ and magnet schools’ capacity in project implementation and program management. This includes providing grantees and schools with technical support in research-based content and innovative practices in project management, performance measurement and evaluation, magnet school programming, and sustainability. This dynamic and comprehensive technical assistance center will meet the continuous needs of MSAP grantees and schools by building a community of practice that creates, shares, and expands magnet schools’ knowledge and best practices. The ultimate goal of the MSAP Center is to help magnet schools provide communities with educational opportunities that promote diversity, academic excellence, and equity.
        </p>
        <h2>
            What is technical assistance?</h2>
        <p>
            The MSAP Center defines technical assistance as ongoing mentoring, training, and
            guidance that address the management, organizational, and project implementation
            needs of MSAP grantees.
        </p>
        <h2>
            What resources are available at the MSAP Center?</h2>
        <p>
            MSAP grantees can access research and expert advice on best practices in curriculum,
            instruction, professional development, program marketing, and student recruitment
            in order to improve magnet schools project implementation, management, and sustainability.
            The multimedia resources ensure interaction and the direct exchange of ideas and
            communication among MSAP grantees.</p>
        <p>
            For additional information on MSAP grant eligibility, requirements and parameters,
            visit the <a href="http://www.federalgrantswire.com/magnet-schools-assistance.html"
                target="_blank">Federal Grants Wire MSAP</a> page.
        </p>
    </div>
</asp:Content>
