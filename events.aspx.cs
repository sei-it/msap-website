﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class useresource : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadEvents();
    }
    private static int CompareEventByDate(MagnetPublicEvent x, MagnetPublicEvent y)
    {
        DateTime dt1 = new DateTime();
        DateTime dt2 = new DateTime();
        if (DateTime.TryParse(x.Date, out dt1) && DateTime.TryParse(y.Date, out dt2))
        {
            return -1* dt1.CompareTo(dt2);
        }
        else
            return 0;
    }

    private void LoadEvents()
    {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        var webniars = MagnetPublicEvent.Find(x => x.EventType == false && x.Display == true).ToList();
        webniars.Sort(CompareEventByDate);
        foreach (MagnetPublicEvent webniar in webniars)
        {
            count++;
            sb.AppendFormat("<a name='event{3}'></a><h2>{0}</h2> <p><strong>Organization</strong>: {1}<br /> <strong>Date</strong>: {2}<br />", webniar.EventTitle, webniar.Organization, webniar.Date, count);
            sb.AppendFormat("<strong>Time</strong>: {0}<br /><strong>Registration</strong>: {1}<br /><strong>Description</strong>: {2}</p>", webniar.Time, webniar.Registration, webniar.Description);
        }
        if (sb.Length > 0)
            divWebinars.InnerHtml = sb.ToString();

        sb = new StringBuilder();
        var conferences = MagnetPublicEvent.Find(x => x.EventType == true && x.Display == true).ToList();
        conferences.Sort(CompareEventByDate);
        foreach (MagnetPublicEvent conference in  conferences)
        {
            count++;
            sb.AppendFormat("<a name='event{3}'></a><h2>{0}</h2> <p><strong>Organization</strong>: {1}<br /> <strong>Date</strong>: {2}<br />", conference.EventTitle, conference.Organization, conference.Date, count);
            sb.AppendFormat("<strong>Location</strong>: {0}<br /><strong>Description</strong>: {1}</p>", conference.Location, conference.Description);
        }
        if (sb.Length > 0)
            divConferences.InnerHtml = sb.ToString();
    }
}