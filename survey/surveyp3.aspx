﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp3.aspx.cs"
    Inherits="survey_surveyp3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div>
                <img src="../images/surveytop.jpg" alt="Principal Survey" /></div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <p>
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                <p>
                    <strong>4. How would you rate your performance on the following activities and what
                        do you believe your performance should be? ( 1= Needs improvement; 2= Adequate;
                        3= Good; 4= Excellent; N/A= Not applicable).</strong>
                </p>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <strong>What is</strong>
                        </td>
                        <td align="center">
                            <strong>What should be</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #bfbfbf">
                            <strong>Capacity-building:</strong>
                        </td>
                        <td width="40%" style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            n. Providing professional development linked to state standards
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblProfessinoalDevelopment" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblProfessinoalDevelopment"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblProfessinoalDevelopmentShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblProfessinoalDevelopmentShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            o. Providing professional development linked to your magnet theme and curriculum
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblProfessinoalDeveloopmentTheme" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblProfessinoalDeveloopmentTheme"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblProfessinoalDeveloopmentThemeShouldBe" CssClass="fullTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblProfessinoalDeveloopmentThemeShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            p. Improving the knowledge and skills of teachers
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImprovingKnowledge" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImprovingKnowledge"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImprovingKnowledgeShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImprovingKnowledgeShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            q. Creating opportunities for professional collaboration among magnet teachers
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCreatingOpportunities" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCreatingOpportunities"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCreatingOpportunitiesShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCreatingOpportunitiesShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            r. Engaging magnet families in their student’s learning
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEngaging" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEngaging"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEngagingShouldBe" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEngagingShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            s. Establishing partnerships with the magnet community
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEstablishing" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEstablishing"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEstablishingShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEstablishingShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <strong>What is</strong>
                        </td>
                        <td align="center">
                            <strong>What should be</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #bfbfbf">
                            <strong>Marketing:</strong>
                        </td>
                        <td width="40%" style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            t. Improving the image of your school
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblSchoolImage" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblSchoolImage"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblSchoolImageShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblSchoolImageShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            u. Promoting the magnet program
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPromotingMagnet" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPromotingMagnet"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPromotingMagnetShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPromotingMagnetShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            v. Developing a marketing plan
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblMarketingPlan" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblMarketingPlan"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblMarketingPlanShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblMarketingPlanShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            w. Assessing the effectiveness of your marketing plan
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPlanEffectiveness" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPlanEffectiveness"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPlanEffectivenessShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPlanEffectivenessShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Button ID="Button1" runat="server" CssClass="msapBtn" OnClick="OnPrevious" Text="Previous" />
                        </td>
                        <td align="right">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <img src="../images/surveybot.jpg" alt="Principal Survey" /></div>
        </div>
    </div>
    </form>
</body>
</html>
