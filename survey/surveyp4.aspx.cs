﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4x1 != null) rblRecruitmentPlan.SelectedIndex = (int)survey.Question4x1;
            if (survey.Question4x2 != null) rblRecruitmentPlanShouldBe.SelectedIndex = (int)survey.Question4x2;
            if (survey.Question4y1 != null) rblRecruitmentPlanEffectiveness.SelectedIndex = (int)survey.Question4y1;
            if (survey.Question4y2 != null) rblRecruitmentPlanEffectivenessShouldBe.SelectedIndex = (int)survey.Question4y2;
            if (survey.Question4z1 != null) rblDiverseStudents.SelectedIndex = (int)survey.Question4z1;
            if (survey.Question4z2 != null) rblDiverseStudentsShouldBe.SelectedIndex = (int)survey.Question4z2;
            if (survey.Question4aa1 != null) rblBuildingEnrollment.SelectedIndex = (int)survey.Question4aa1;
            if (survey.Question4aa2 != null) rblBuildingEnrollmentShouldBe.SelectedIndex = (int)survey.Question4aa2;
            if (survey.Question4bb1 != null) rblRetainingStudents.SelectedIndex = (int)survey.Question4bb1;
            if (survey.Question4bb2 != null) rblRetainingStudentsShouldBe.SelectedIndex = (int)survey.Question4bb2;
            if (survey.Question4cc1 != null) rblDistrictAdministrators.SelectedIndex = (int)survey.Question4cc1;
            if (survey.Question4cc2 != null) rblDistrictAdministratorsShouldBe.SelectedIndex = (int)survey.Question4cc2;
            if (survey.Question4dd1 != null) rblCommunicatingParents.SelectedIndex = (int)survey.Question4dd1;
            if (survey.Question4dd2 != null) rblCommunicatingParentsShouldBe.SelectedIndex = (int)survey.Question4dd2;
            if (survey.Question4ee1 != null) rblCommunicatingTeachers.SelectedIndex = (int)survey.Question4ee1;
            if (survey.Question4ee2 != null) rblCommunicatingTeachersShouldBe.SelectedIndex = (int)survey.Question4ee2;
            if (survey.Question4ff1 != null) rblCommunicatingStaff.SelectedIndex = (int)survey.Question4ff1;
            if (survey.Question4ff2 != null) rblCommunicatingStaffShouldBe.SelectedIndex = (int)survey.Question4ff2;
            if (survey.Question4gg1 != null) rblCommunicatingStudents.SelectedIndex = (int)survey.Question4gg1;
            if (survey.Question4gg2 != null) rblCommunicatingStudentsShouldBe.SelectedIndex = (int)survey.Question4gg2;
            if (survey.Question4hh1 != null) rblPromtingDiversity.SelectedIndex = (int)survey.Question4hh1;
            if (survey.Question4hh2 != null) rblPromtingDiversityShouldBe.SelectedIndex = (int)survey.Question4hh2;
            if (survey.Question4ii1 != null) rblMeetingDesegregationPlan.SelectedIndex = (int)survey.Question4ii1;
            if (survey.Question4ii2 != null) rblMeetingDesegregationPlanShouldBe.SelectedIndex = (int)survey.Question4ii2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp3.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp5.aspx", true);
    }
    private void SaveData()
    {
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4x1 = rblRecruitmentPlan.SelectedIndex;
        survey.Question4x2 = rblRecruitmentPlanShouldBe.SelectedIndex;
        survey.Question4y1 = rblRecruitmentPlanEffectiveness.SelectedIndex;
        survey.Question4y2 = rblRecruitmentPlanEffectivenessShouldBe.SelectedIndex;
        survey.Question4z1 = rblDiverseStudents.SelectedIndex;
        survey.Question4z2 = rblDiverseStudentsShouldBe.SelectedIndex;
        survey.Question4aa1 = rblBuildingEnrollment.SelectedIndex;
        survey.Question4aa2 = rblBuildingEnrollmentShouldBe.SelectedIndex;
        survey.Question4bb1 = rblRetainingStudents.SelectedIndex;
        survey.Question4bb2 = rblRetainingStudentsShouldBe.SelectedIndex;
        survey.Question4cc1 = rblDistrictAdministrators.SelectedIndex;
        survey.Question4cc2 = rblDistrictAdministratorsShouldBe.SelectedIndex;
        survey.Question4dd1 = rblCommunicatingParents.SelectedIndex;
        survey.Question4dd2 = rblCommunicatingParentsShouldBe.SelectedIndex;
        survey.Question4ee1 = rblCommunicatingTeachers.SelectedIndex;
        survey.Question4ee2 = rblCommunicatingTeachersShouldBe.SelectedIndex;
        survey.Question4ff1 = rblCommunicatingStaff.SelectedIndex;
        survey.Question4ff2 = rblCommunicatingStaffShouldBe.SelectedIndex;
        survey.Question4gg1 = rblCommunicatingStudents.SelectedIndex;
        survey.Question4gg2 = rblCommunicatingStudentsShouldBe.SelectedIndex;
        survey.Question4hh1 = rblPromtingDiversity.SelectedIndex;
        survey.Question4hh2 = rblPromtingDiversityShouldBe.SelectedIndex;
        survey.Question4ii1 = rblMeetingDesegregationPlan.SelectedIndex;
        survey.Question4ii2 = rblMeetingDesegregationPlanShouldBe.SelectedIndex;
        survey.Save();
    }
}
