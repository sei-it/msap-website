﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4n1 != null) rblProfessinoalDevelopment.SelectedIndex = (int)survey.Question4n1;
            if (survey.Question4n2 != null) rblProfessinoalDevelopmentShouldBe.SelectedIndex = (int)survey.Question4n2;
            if (survey.Question4o1 != null) rblProfessinoalDeveloopmentTheme.SelectedIndex = (int)survey.Question4o1;
            if (survey.Question4o2 != null) rblProfessinoalDeveloopmentThemeShouldBe.SelectedIndex = (int)survey.Question4o2;
            if (survey.Question4p1 != null) rblImprovingKnowledge.SelectedIndex= (int)survey.Question4p1 ;
            if (survey.Question4p2 != null) rblImprovingKnowledgeShouldBe.SelectedIndex= (int)survey.Question4p2 ;
            if (survey.Question4q1 != null) rblCreatingOpportunities.SelectedIndex= (int)survey.Question4q1 ;
            if (survey.Question4q2 != null) rblCreatingOpportunitiesShouldBe.SelectedIndex= (int)survey.Question4q2 ;
            if (survey.Question4r1 != null) rblEngaging.SelectedIndex= (int)survey.Question4r1 ;
            if (survey.Question4r2 != null) rblEngagingShouldBe.SelectedIndex= (int)survey.Question4r2 ;
            if (survey.Question4s1 != null) rblEstablishing.SelectedIndex= (int)survey.Question4s1 ;
            if (survey.Question4s2 != null) rblEstablishingShouldBe.SelectedIndex= (int)survey.Question4s2 ;
            if (survey.Question4t1 != null) rblSchoolImage.SelectedIndex= (int)survey.Question4t1 ;
            if (survey.Question4t2 != null) rblSchoolImageShouldBe.SelectedIndex= (int)survey.Question4t2 ;
            if (survey.Question4u1 != null) rblPromotingMagnet.SelectedIndex= (int)survey.Question4u1 ;
            if (survey.Question4u2 != null) rblPromotingMagnetShouldBe.SelectedIndex= (int)survey.Question4u2 ;
            if (survey.Question4v1 != null) rblMarketingPlan.SelectedIndex= (int)survey.Question4v1 ;
            if (survey.Question4v2 != null) rblMarketingPlanShouldBe.SelectedIndex= (int)survey.Question4v2 ;
            if (survey.Question4w1 != null) rblPlanEffectiveness.SelectedIndex= (int)survey.Question4w1 ;
            if (survey.Question4w2 != null) rblPlanEffectivenessShouldBe.SelectedIndex= (int)survey.Question4w2 ;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp2.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp4.aspx", true);
    }
    private void SaveData()
    {
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4n1 = rblProfessinoalDevelopment.SelectedIndex;
        survey.Question4n2 = rblProfessinoalDevelopmentShouldBe.SelectedIndex;
        survey.Question4o1 = rblProfessinoalDeveloopmentTheme.SelectedIndex;
        survey.Question4o2 = rblProfessinoalDeveloopmentThemeShouldBe.SelectedIndex;
        survey.Question4p1 = rblImprovingKnowledge.SelectedIndex;
        survey.Question4p2 = rblImprovingKnowledgeShouldBe.SelectedIndex;
        survey.Question4q1 = rblCreatingOpportunities.SelectedIndex;
        survey.Question4q2 = rblCreatingOpportunitiesShouldBe.SelectedIndex;
        survey.Question4r1 = rblEngaging.SelectedIndex;
        survey.Question4r2 = rblEngagingShouldBe.SelectedIndex;
        survey.Question4s1 = rblEstablishing.SelectedIndex;
        survey.Question4s2 = rblEstablishingShouldBe.SelectedIndex;
        survey.Question4t1 = rblSchoolImage.SelectedIndex;
        survey.Question4t2 = rblSchoolImageShouldBe.SelectedIndex;
        survey.Question4u1 = rblPromotingMagnet.SelectedIndex;
        survey.Question4u2 = rblPromotingMagnetShouldBe.SelectedIndex;
        survey.Question4v1 = rblMarketingPlan.SelectedIndex;
        survey.Question4v2 = rblMarketingPlanShouldBe.SelectedIndex;
        survey.Question4w1 = rblPlanEffectiveness.SelectedIndex;
        survey.Question4w2 = rblPlanEffectivenessShouldBe.SelectedIndex;
        survey.Save();
    }
}
