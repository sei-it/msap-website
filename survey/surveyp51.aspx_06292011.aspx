﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp51.aspx.cs"
    Inherits="survey_surveyp5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                 <h2 style="color:#F90">MSAP Principal Survey</h2>
                 <hr color="#A6CAF0" />
                 <p style="font-size:12px">
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                 <p style="font-size:12px">
                    <strong>5. In your magnet program, how would you rate the following? <br />(1= needs improvement,
                        2= adequate, 3= good, 4= excellent, or N/A= not applicable).</strong>
                </p>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <p style="font-size:14px"> <strong>What is</strong></p>
                        </td>
                        <td align="center">
                            <p style="font-size:14px"> <strong>What should be</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #A6CAF0">
                            <p style="font-size:12px"> <strong>Family engagement:</strong></p>
                        </td>
                        <td width="40%" style="background-color: #A6CAF0">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                     <p style="font-size:12px">    <strong>1</strong></p>
                                    </td>
                                    <td>
                                     <p style="font-size:12px">    <strong>2</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"> <strong>3</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"> <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #A6CAF0">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                       <p style="font-size:12px">  <strong>1</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"> <strong>2</strong></p>
                                    </td>
                                    <td>
                                         <p style="font-size:12px"><strong>3</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"> <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <p style="font-size:12px">  a. Families’ engagement in their magnet &nbsp;&nbsp;&nbsp;&nbsp;students’ education</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFamilyEngagement" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator64" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFamilyEngagement"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFamilyEngagementShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator65" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFamilyEngagementShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-size:12px"> b. Family understanding of the magnet &nbsp;&nbsp;&nbsp;&nbsp;school curriculum</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFAmilyUnderstanding" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator76" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFAmilyUnderstanding"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFAmilyUnderstandingShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator77" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFAmilyUnderstandingShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-size:12px"> c. Family support for the magnet school</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFamilySupport" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator78" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFamilySupport"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFamilySupportShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFamilySupportShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <p style="font-size:12px">   d. Family support of your work</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFamilySupportWork" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator80" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFamilySupportWork"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblFamilySupportWorkShouldbe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblFamilySupportWorkShouldbe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" width="10%">
                            &nbsp;<asp:Button ID="Button1" runat="server" CssClass="msapBtn" OnClick="OnPrevious"
                                Text="Previous" />
                        </td>
                        <td align="center">
                            72% completed
                        </td>
                        <td align="right" width="10%">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>
            <div class="addressFt">
                <div style="float: right; width: 300px;">
                    <strong>MSAP Technical Assistance Center</strong><br />
                    8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                    1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a>
                </div>
                <a href="http://www.ed.gov" target="_blank">
                    <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                        vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                            Funded by the<br />
                            U.S. Department of Education<br />
                            Contract Number: ED-OII-10-C-0079</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
