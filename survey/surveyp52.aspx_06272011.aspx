﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp52.aspx.cs"
    Inherits="survey_surveyp5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <p>
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                <p>
                    <strong>6. Overall, how prepared are magnet teachers in your school and how prepared
                        do you think they should be? (1= not at all prepared, 2= not very prepared, 3= prepared,
                        4= very prepared, or N/A= not applicable).</strong>
                </p>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <strong>What is</strong>
                        </td>
                        <td align="center">
                            <strong>What should be</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #bfbfbf">
                            <strong>Magnet teachers:</strong>
                        </td>
                        <td width="40%" style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            a. To develop magnet curriculum
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDevelopCurriculum" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator92" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblDevelopCurriculum"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDevelopCurriculumShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator93" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblDevelopCurriculumShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            b. To integrate the magnet theme into classroom instruction
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblIntegrateTheme" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator94" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblIntegrateTheme"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblIntegrateThemeShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator95" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblIntegrateThemeShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            c. To teach the magnet school content
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblTeachContent" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator96" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblTeachContent"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblTeachContentShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator97" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblTeachContentShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            d. To access supplemental classroom materials
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAccessMaterials" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblAccessMaterials"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAccessMaterialsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator99" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblAccessMaterialsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            e. To manage the magnet classroom
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblManageClassroom" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblManageClassroom"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblManageClassroomShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblManageClassroomShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            f. To work with children of diverse backgrounds
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblWorkWithDiverseStudents" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator84" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblWorkWithDiverseStudents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblWorkWithDiverseStudentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator85" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblWorkWithDiverseStudentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            g. To promote the magnet program
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPromoteMagnet" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator86" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblPromoteMagnet"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPromoteMagnetShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator87" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblPromoteMagnetShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            h. To engage families in supporting their children’s magnet school education
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEngageFamily" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblEngageFamily"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEngageFamilyShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator89" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblEngageFamilyShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" width="10%">
                            &nbsp;<asp:Button ID="Button1" runat="server" CssClass="msapBtn" OnClick="OnPrevious"
                                Text="Previous" />
                        </td>
                        <td align="center">
                            81% completed
                        </td>
                        <td align="right" width="10%">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>
            <div class="addressFt">
                <div style="float: right; width: 300px;">
                    <strong>MSAP Technical Assistance Center</strong><br />
                    8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                    1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a>
                </div>
                <a href="http://www.ed.gov" target="_blank">
                    <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                        vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                            Funded by the<br />
                            U.S. Department of Education<br />
                            Contract Number: ED-OII-10-C-0079</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
