﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp22 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4j1 != null) rblCommunicating.SelectedIndex = (int)survey.Question4j1;
            if (survey.Question4j2 != null) rblCommunicatingShouldBe.SelectedIndex = (int)survey.Question4j2;
            if (survey.Question4k1 != null) rblImprovementVision.SelectedIndex = (int)survey.Question4k1;
            if (survey.Question4k2 != null) rblImprovementVisionShouldBe.SelectedIndex = (int)survey.Question4k2;
            if (survey.Question4l1 != null) rblEstablishingStandards.SelectedIndex = (int)survey.Question4l1;
            if (survey.Question4l2 != null) rblEstablishingStandardsShouldBe.SelectedIndex = (int)survey.Question4l2;
            if (survey.Question4m1 != null) rblAligning.SelectedIndex = (int)survey.Question4m1;
            if (survey.Question4m2 != null) rblAligningShouldBe.SelectedIndex = (int)survey.Question4m2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        //SaveData();
        Response.Redirect("surveyp21.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp31.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4j1 = rblCommunicating.SelectedIndex;
        survey.Question4j2 = rblCommunicatingShouldBe.SelectedIndex;
        survey.Question4k1 = rblImprovementVision.SelectedIndex;
        survey.Question4k2 = rblImprovementVisionShouldBe.SelectedIndex;
        survey.Question4l1 = rblEstablishingStandards.SelectedIndex;
        survey.Question4l2 = rblEstablishingStandardsShouldBe.SelectedIndex;
        survey.Question4m1 = rblAligning.SelectedIndex;
        survey.Question4m2 = rblAligningShouldBe.SelectedIndex;
        survey.Save();
    }
}
