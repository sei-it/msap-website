﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp22.aspx.cs"
    Inherits="survey_surveyp22" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <h1 style="color: #F90">
                    MSAP Principal Survey</h1>
                <hr color="#A6CAF0" />
                <p style="font-size: 12px">
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                <p style="color: #063; font-size: 16px">
                    Activities</p>
                <p style="font-size: 12px">
                    <strong>4. How would you rate your performance on the following activities and what
                        do you believe your performance should be?<br />
                        (1= Needs improvement; 2= Adequate; 3= Good; 4= Excellent; N/A= Not applicable).</strong>
                </p>
                <table class="surveyTbl" width="100%">
                    <tr>
                        <td style="border: 0;">
                        </td>
                        <td style="border: 0;" align="center">
                            <p style="font-size: 14px">
                                <strong>What is</strong></p>
                        </td>
                        <td align="center" style="border: 0;">
                            <p style="font-size: 14px">
                                <strong>What should be</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" class="surveyTh">
                            <p style="font-size: 13px">
                                <strong>School improvement and reform</strong></p>
                        </td>
                        <td class="surveyTh">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>1</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>2</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>3</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="surveyTh">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>1</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>2</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>3</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size: 12px">
                                            <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-size: 12px">
                                j. Communicating the district’s improvement vision</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicating" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicating"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven">
                        <td>
                            <p style="font-size: 12px">
                                k. Communicating your school’s improvement vision</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImprovementVision" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImprovementVision"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImprovementVisionShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImprovementVisionShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-size: 12px">
                                l. Establishing high content and performance standards for all students</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEstablishingStandards" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEstablishingStandards"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEstablishingStandardsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEstablishingStandardsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven SurveyLastTr">
                        <td>
                            <p style="font-size: 12px">
                                m. Aligning magnet curricula and instructional materials with content and performance
                                standards</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAligning" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAligning"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAligningShouldBe" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAligningShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table><br />
                <table width="100%">
                    <tr>
                        <td align="left" width="40%">
                        </td>
                        <td align="center">
                            18% completed
                        </td>
                        <td align="right" width="40%">
                            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" OnClick="OnPrevious"
                                Text="Previous" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Button2" runat="server" CssClass="surveyBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>
                <div class="surveyFt">
                    <div style="float: right; width: 300px;">
                        <strong>MSAP Technical Assistance Center</strong><br />
                        8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                        1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a>
                    </div>
                    <a href="http://www.ed.gov" target="_blank">
                        <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                            vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                                Funded by the<br />
                                U.S. Department of Education<br />
                                Contract Number: ED-OII-10-C-0079</div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
