﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4x1 != null) rblRecruitmentPlan.SelectedIndex = (int)survey.Question4x1;
            if (survey.Question4x2 != null) rblRecruitmentPlanShouldBe.SelectedIndex = (int)survey.Question4x2;
            if (survey.Question4y1 != null) rblRecruitmentPlanEffectiveness.SelectedIndex = (int)survey.Question4y1;
            if (survey.Question4y2 != null) rblRecruitmentPlanEffectivenessShouldBe.SelectedIndex = (int)survey.Question4y2;
            if (survey.Question4z1 != null) rblDiverseStudents.SelectedIndex = (int)survey.Question4z1;
            if (survey.Question4z2 != null) rblDiverseStudentsShouldBe.SelectedIndex = (int)survey.Question4z2;
            if (survey.Question4aa1 != null) rblBuildingEnrollment.SelectedIndex = (int)survey.Question4aa1;
            if (survey.Question4aa2 != null) rblBuildingEnrollmentShouldBe.SelectedIndex = (int)survey.Question4aa2;
            if (survey.Question4bb1 != null) rblRetainingStudents.SelectedIndex = (int)survey.Question4bb1;
            if (survey.Question4bb2 != null) rblRetainingStudentsShouldBe.SelectedIndex = (int)survey.Question4bb2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        //SaveData();
        Response.Redirect("surveyp32.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp42.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4x1 = rblRecruitmentPlan.SelectedIndex;
        survey.Question4x2 = rblRecruitmentPlanShouldBe.SelectedIndex;
        survey.Question4y1 = rblRecruitmentPlanEffectiveness.SelectedIndex;
        survey.Question4y2 = rblRecruitmentPlanEffectivenessShouldBe.SelectedIndex;
        survey.Question4z1 = rblDiverseStudents.SelectedIndex;
        survey.Question4z2 = rblDiverseStudentsShouldBe.SelectedIndex;
        survey.Question4aa1 = rblBuildingEnrollment.SelectedIndex;
        survey.Question4aa2 = rblBuildingEnrollmentShouldBe.SelectedIndex;
        survey.Question4bb1 = rblRetainingStudents.SelectedIndex;
        survey.Question4bb2 = rblRetainingStudentsShouldBe.SelectedIndex;
        survey.Save();
    }
}
