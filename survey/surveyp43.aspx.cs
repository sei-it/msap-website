﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4hh1 != null) rblPromtingDiversity.SelectedIndex = (int)survey.Question4hh1;
            if (survey.Question4hh2 != null) rblPromtingDiversityShouldBe.SelectedIndex = (int)survey.Question4hh2;
            if (survey.Question4ii1 != null) rblMeetingDesegregationPlan.SelectedIndex = (int)survey.Question4ii1;
            if (survey.Question4ii2 != null) rblMeetingDesegregationPlanShouldBe.SelectedIndex = (int)survey.Question4ii2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        //SaveData();
        Response.Redirect("surveyp42.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp51.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4hh1 = rblPromtingDiversity.SelectedIndex;
        survey.Question4hh2 = rblPromtingDiversityShouldBe.SelectedIndex;
        survey.Question4ii1 = rblMeetingDesegregationPlan.SelectedIndex;
        survey.Question4ii2 = rblMeetingDesegregationPlanShouldBe.SelectedIndex;
        survey.Save();
    }
}
