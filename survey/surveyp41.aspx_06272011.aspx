﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp41.aspx.cs"
    Inherits="survey_surveyp4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <p>
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                <p>
                    <strong>4. How would you rate your performance on the following activities and what
                        do you believe your performance should be? ( 1= Needs improvement; 2= Adequate;
                        3= Good; 4= Excellent; N/A= Not applicable).</strong>
                </p>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <strong>What is</strong>
                        </td>
                        <td align="center">
                            <strong>What should be</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #bfbfbf">
                            <strong>Student recruitment:</strong>
                        </td>
                        <td width="40%" style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            x. Developing a recruitment plan
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlan" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblRecruitmentPlan"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlanShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblRecruitmentPlanShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            y. Assessing the effectiveness of your recruitment plan
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlanEffectiveness" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator68" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblRecruitmentPlanEffectiveness"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlanEffectivenessShouldBe" CssClass="fullTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator69" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblRecruitmentPlanEffectivenessShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            z. Recruiting diverse students and families from outside traditional school boundaries
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDiverseStudents" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator70" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblDiverseStudents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDiverseStudentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator71" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblDiverseStudentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            aa. Building magnet student enrollment
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblBuildingEnrollment" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator74" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblBuildingEnrollment">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblBuildingEnrollmentShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblBuildingEnrollmentShouldBe">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            bb. Retaining magnet students
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRetainingStudents" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator72" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblRetainingStudents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRetainingStudentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator73" runat="server" ErrorMessage="This fields is required!"
                                ControlToValidate="rblRetainingStudentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                               <table width="100%">
                    <tr>
                        <td align="left" width="10%">
                            &nbsp;<asp:Button ID="Button1" runat="server" CssClass="msapBtn" OnClick="OnPrevious" Text="Previous" />
                        </td>
                        <td align="center">
                            45% completed
                        </td>
                        <td align="right" width="10%">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>

            <div class="addressFt">
                <div style="float: right; width: 300px;">
                    <strong>MSAP Technical Assistance Center</strong><br />
                    8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                    1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a>
                </div>
                <a href="http://www.ed.gov" target="_blank">
                    <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                        vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                            Funded by the<br />
                            U.S. Department of Education<br />
                            Contract Number: ED-OII-10-C-0079</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
