﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp2.aspx.cs"
    Inherits="survey_surveyp2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div>
                <img src="../images/surveytop.jpg" alt="Principal Survey" /></div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <p>
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                <p>
                    <strong>4. How would you rate your performance on the following activities and what
                        do you believe your performance should be? ( 1= Needs improvement; 2= Adequate;
                        3= Good; 4= Excellent; N/A= Not applicable).</strong>
                </p>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <strong>What is</strong>
                        </td>
                        <td align="center">
                            <strong>What should be</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #bfbfbf">
                            <strong>Project implementation and management:</strong>
                        </td>
                        <td width="40%" style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            a. Providing leadership for the MSAP project
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblLeaderShip" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblLeaderShip"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblLeaderShipShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblLeaderShipShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            b. Hiring necessary staff for the MSAP project
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblHiring" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblHiring"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblHiringShouldBe" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblHiringShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            c. Keeping teachers motivated to implement the MSAP project
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblMotivateTeacher" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblMotivateTeacher"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblMotivateTeacherShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblMotivateTeacherShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            d. Obtaining magnet school materials and equipment
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblObtaningMaterial" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblObtaningMaterial"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblObtaningMaterialShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblObtaningMaterialShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            e. Implementing the MSAP project on schedule
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImplementingProject" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImplementingProject"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImplementingProjectShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImplementingProjectShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            f. Implementing the critical components of the MSAP project
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImplementingCriticalComponents" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImplementingCriticalComponents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImplementingCriticalComponentsShouldBe" CssClass="fullTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImplementingCriticalComponentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            g. Assessing the implementation status of your magnet program
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAssessing" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAssessing"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAssessingShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAssessingShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            h. Assessing the progress of students in the magnet program
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAssessingProgress" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAssessingProgress"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAssessingProgressShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAssessingProgressShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            i. Interpreting assessment results
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblInterpreting" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblInterpreting"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblInterpretingShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblInterpretingShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <strong>What is</strong>
                        </td>
                        <td align="center">
                            <strong>What should be</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #bfbfbf">
                            <strong>School improvement and reform:</strong>
                        </td>
                        <td width="40%" style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #bfbfbf">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <strong>1</strong>
                                    </td>
                                    <td>
                                        <strong>2</strong>
                                    </td>
                                    <td>
                                        <strong>3</strong>
                                    </td>
                                    <td>
                                        <strong>4</strong>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            j. Communicating the district’s improvement vision
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicating" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicating"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            k. Communicating your school’s improvement vision
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImprovementVision" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImprovementVision"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblImprovementVisionShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblImprovementVisionShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            l. Establishing high content and performance standards for all students
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEstablishingStandards" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEstablishingStandards"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEstablishingStandardsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEstablishingStandardsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            m. Aligning magnet curricula and instructional materials with content and performance
                            standards
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAligning" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAligning"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAligningShouldBe" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAligningShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Button ID="Button1" runat="server" CssClass="msapBtn" OnClick="OnPrevious" Text="Previous" />
                        </td>
                        <td align="right">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <img src="../images/surveybot.jpg" alt="Principal Survey" /></div>
        </div>
    </div>
    </form>
</body>
</html>
