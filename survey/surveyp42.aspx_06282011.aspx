﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp42.aspx.cs"
    Inherits="survey_surveyp4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                 <h2 style="color:#F90">MSAP Principal Survey</h2>
                <hr color="#A6CAF0" />
                <p style="font-size:12px">
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                <p style="font-size:12px">
                    <strong>4. How would you rate your performance on the following activities and what
                        do you believe your performance should be? <br />( 1= Needs improvement; 2= Adequate;
                        3= Good; 4= Excellent; N/A= Not applicable).</strong>
                </p>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                           <p style="font-size:14px"> <strong>What is</strong></p>
                        </td>
                        <td width="46%" align="center">
                            <p style="font-size:14px"><strong>What should be</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="background-color: #A6CAF0">
                            <p style="font-size:12px"><strong>Communication:</strong></p>
                        </td>
                        <td width="34%" style="background-color: #A6CAF0">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                      <p style="font-size:12px">  <strong>1</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>2</strong></p>
                                    </td>
                                    <td>
                                      <p style="font-size:12px">  <strong>3</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #A6CAF0">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                       <p style="font-size:12px"> <strong>1</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"><strong>2</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>3</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                      <p style="font-size:12px">  <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <p style="font-size:12px">  cc. Communicating with district administrators</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDistrictAdministrators" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblDistrictAdministrators"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDistrictAdministratorsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblDistrictAdministratorsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-size:12px">dd. Communicating with parents</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingParents" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingParents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingParentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingParentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <p style="font-size:12px"> ee. Communicating with magnet teachers</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingTeachers" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingTeachers"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingTeachersShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingTeachersShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-size:12px">ff. Communicating with magnet school staff</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingStaff" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator48" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingStaff">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingStaffShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator49" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingStaffShouldBe">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <p style="font-size:12px">gg. Communicating with students</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingStudents" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingStudents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblCommunicatingStudentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblCommunicatingStudentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                            <table width="100%">
                    <tr>
                        <td align="left" width="10%">
                            &nbsp;<asp:Button ID="Button1" runat="server" CssClass="msapBtn" OnClick="OnPrevious" Text="Previous" />
                        </td>
                        <td align="center">
                            54% completed
                        </td>
                        <td align="right" width="10%">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>

            <div class="addressFt">
                <div style="float: right; width: 300px;">
                    <strong>MSAP Technical Assistance Center</strong><br />
                    8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                    1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a>
                </div>
                <a href="http://www.ed.gov" target="_blank">
                    <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                        vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                            Funded by the<br />
                            U.S. Department of Education<br />
                            Contract Number: ED-OII-10-C-0079</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
