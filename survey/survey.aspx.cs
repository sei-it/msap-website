﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_survey : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                byte[] decodedBytes = Convert.FromBase64String(Request.QueryString["id"]);
                string decodedText = System.Text.Encoding.UTF8.GetString(decodedBytes);

                hfUserID.Value = decodedText;
                Session["UserID"] = hfUserID.Value;
            }
            else if (Session["UserID"] != null)
            {
                hfUserID.Value = (string)Session["UserID"];
            }
            else
            {
                Response.Redirect("~/survey/login.aspx", true);
            }

            MagnetSurveyUser user = MagnetSurveyUser.SingleOrDefault(x => x.ID == Convert.ToInt32(hfUserID.Value));

            lblGranteeName.Text = MagnetGrantee.SingleOrDefault(x => x.ID == user.GranteeID).GranteeName;
            lblSchoolName.Text = MagnetSchool.SingleOrDefault(x => x.ID == user.SchoolID).SchoolName;

            MagnetGranteeContact Principal = MagnetGranteeContact.SingleOrDefault(x => x.ID == user.PrincipalID);
            if (Principal != null)
            {
                lblPrinpal.Text = Principal.ContactFirstName + " " + Principal.ContactLastName;
                lblTelephone.Text = ManageUtility.FormatPhoneNumber(Principal.Phone);

                var surveys = MagnetSurvey.Find(x => x.SurveyUserID == Convert.ToInt32(hfUserID.Value));
                if (surveys.Count > 0)
                {
                    Session["SurveyID"] = surveys[0].ID;
                    MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
                    if (survey.Submitted != null && survey.Submitted == true)
                    {
                        Response.Redirect("survey8.aspx", true);
                    }
                    if (survey.Question1 != null) ddlYearsInK12.SelectedValue = Convert.ToString(survey.Question1);
                    if (survey.Question2 != null) ddlProgress.SelectedValue = Convert.ToString(survey.Question2);
                    if (survey.Question3 != null) ddlSatisfication.SelectedValue = Convert.ToString(survey.Question3);
                }
                else
                {
                    MagnetSurvey survey = new MagnetSurvey();
                    survey.SurveyUserID = Convert.ToInt32(hfUserID.Value);
                    survey.Save();
                    Session["SurveyID"] = survey.ID;
                }
            }
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        SaveData();
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp21.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null)
            Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        //survey.SurveyUserID = Convert.ToInt32(hfUserID.Value);
        survey.Question1 = Convert.ToInt32(ddlYearsInK12.SelectedValue);
        survey.Question2 = Convert.ToInt32(ddlProgress.SelectedValue);
        survey.Question3 = Convert.ToInt32(ddlSatisfication.SelectedValue);
        survey.Save();
        //if (Session["SurveyID"] == null) Session["SurveyID"] = survey.ID;
    }
}
