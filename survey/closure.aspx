﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="closure.aspx.cs"
    Inherits="survey_closure" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <p style="font-size: 12px">
                    The needs assessment principal survey administered by the U.S. Department of Education
                    and MSAP Center is now closed. The survey results will help us to deliver the most
                    relevant and focused technical assistance to MSAP grantees. We appreciate your participation.
                </p>
                <p style="font-size: 12px">
                    If you have questions, please contact the MSAP Center at (866)-997-6727 or at msapcenter@seiservices.com
                </p>
                <p style="font-size: 12px">
                    Thank you for your participation.
                </p>
                <p style="font-size: 12px">
                    Click <a href="../default.aspx">here</a> to go back to the MSAP Center homepage.
                </p>
            </div>
            <div class="surveyFt">
                <div style="float: right; width: 300px;">
                    <strong>MSAP Technical Assistance Center</strong><br />
                    8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                    1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com" style="color: #FFF">
                        msapcenter@seiservices.com</a>
                </div>
                <a href="http://www.ed.gov" target="_blank">
                    <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                        vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                            Funded by the<br />
                            U.S. Department of Education<br />
                            Contract Number: ED-OII-10-C-0079</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
