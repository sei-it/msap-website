﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp41.aspx.cs"
    Inherits="survey_surveyp4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <h1 style="color: #F58220">
                    MSAP Principal Survey</h1>
                <hr color="#A6CAF0" />
                <p style="font-size: 12px">
                  Each statement has two answers that will be recorded separately on this survey. The first answer for each statement is “What is” and relates to the current conditions at your magnet school; the second answer for each statement is “What should be” and relates to what you believe ought to be the situation for you or your magnet program by May 2012. It is possible that some of your responses to “What is” and “What should be” could be the same.
                </p>
                <p style="color: #063; font-size: 16px">
                    Activities</p>
                <p style="font-size: 12px">
                    <strong>4. How would you rate your performance on the following activities, and what
                    do you believe your performance should be?<br /> 
                        (1= Needs improvement; 2= Adequate;
                        3= Good; 4= Excellent; N/A= Not applicable)</strong>
                </p>
                               <table class="surveyTbl" width="100%">
                    <tr>
                        <td style="border: 0;">
                        </td>
                        <td style="border: 0;" align="center">
                            <p style="font-size: 14px">
                                <strong>What is</strong></p>
                        </td>
                        <td align="center" style="border: 0;">
                            <p style="font-size: 14px">
                                <strong>What should be</strong></p>
                        </td>
                    </tr>

                    <tr>
                        <td width="30%" class="surveyTh">
                            <p style="font-size: 13px"><strong>Student recruitment</strong></p>
                        </td>
                        <td class="surveyTh">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <p style="font-size:12px"><strong>1</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"><strong>2</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"><strong>3</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"><strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="surveyTh">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                       <p style="font-size:12px"> <strong>1</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>2</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>3</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <strong>N/A</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        x.
                                    </td>
                                    <td>
                                         Developing a recruitment plan
                                    </td>
                                </tr>
                            </table> 
                        
                        
                       
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlan" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblRecruitmentPlan"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlanShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblRecruitmentPlanShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven">
                       <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        y.
                                    </td>
                                    <td>
                                        Assessing the effectiveness of your recruitment plan
                                    </td>
                                </tr>
                            </table> 
                            
                            
                            
                            
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlanEffectiveness" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator68" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblRecruitmentPlanEffectiveness"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRecruitmentPlanEffectivenessShouldBe" CssClass="fullTbl"
                                runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator69" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblRecruitmentPlanEffectivenessShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                          <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        z.
                                    </td>
                                    <td>
                                        Recruiting diverse students and families from outside traditional school boundaries
                                    </td>
                                </tr>
                            </table> 
                        
                        
                        <td>
                            <asp:RadioButtonList ID="rblDiverseStudents" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator70" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblDiverseStudents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDiverseStudentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator71" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblDiverseStudentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven">
                        
                        <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        aa.
                                    </td>
                                    <td >
                                        Building magnet student enrollment
                                    </td>
                                </tr>
                            </table> 
                        
                        
                        <td>
                            <asp:RadioButtonList ID="rblBuildingEnrollment" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator74" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblBuildingEnrollment">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblBuildingEnrollmentShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblBuildingEnrollmentShouldBe">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="SurveyLastTr">
                        
                            <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        bb.
                                    </td>
                                    <td>
                                        Retaining magnet students
                                    </td>
                                </tr>
                            </table> 
                            
                            
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRetainingStudents" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator72" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblRetainingStudents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblRetainingStudentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator73" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblRetainingStudentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table><br />
                <table width="100%">
                    <tr>
                        <td align="left" width="40%">
                        </td>
                        <td align="center">
                            45% completed
                        </td>
                        <td align="right" width="40%">
                            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" OnClick="OnPrevious"
                                Text="Previous"  OnClientClick="return confirm('Please note that all information on this page may not be saved if you go back to the previous page. Do you wish to proceed?');" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Button2" runat="server" CssClass="surveyBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>
                <div class="surveyFt">
                    <div style="float: right; width: 300px;">
                        <strong>MSAP Technical Assistance Center</strong><br />
                        8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                        1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com" style="color:#FFF">msapcenter@seiservices.com</a>
                    </div>
                    <a href="http://www.ed.gov" target="_blank">
                        <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                            vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                                Funded by the<br />
                                U.S. Department of Education<br />
                                Contract Number: ED-OII-10-C-0079</div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
