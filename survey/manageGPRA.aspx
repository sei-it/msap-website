﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"
    CodeFile="manageGPRA.aspx.cs" Inherits="admin_data_managegpra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Manage GPRA Table
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2 style="color:#F58220" >
        <a href="quarterreports.aspx">Home</a><img src="images/button_arrow.jpg" width="29" height="36" /> Manage GPRA Table</h2>
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="false" DataKeyNames="ID"  CssClass="msapGridView">
        <Columns>
            <asp:BoundField DataField="SchoolName" SortExpression="" HeaderText="Magnet School" />
            <asp:BoundField DataField="PartI" SortExpression="" HtmlEncode="false" HeaderText="Part I School Demographic" />
            <asp:BoundField DataField="PartII" SortExpression="" HtmlEncode="false" HeaderText="Part II GPRA Performance Measure 1" />
            <asp:BoundField DataField="PartIII" SortExpression="" HtmlEncode="false" HeaderText="Part III GPRA Performance Measure 2 and 3" />
            <asp:BoundField DataField="PartIV" SortExpression="" HtmlEncode="false" HeaderText="Part IV GPRA Performance Measure 6" />
        </Columns>
    </asp:GridView>
    <p style="text-align: right;">
        <asp:Button ID="Button2" runat="server" CssClass="msapBtn" Text="Print" OnClick="OnPrint" />
    </p>
</asp:Content>
