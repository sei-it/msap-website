﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp5 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question5a1 != null) rblFamilyEngagement.SelectedIndex = (int)survey.Question5a1;
            if (survey.Question5a2 != null) rblFamilyEngagementShouldBe.SelectedIndex = (int)survey.Question5a2;
            if (survey.Question5b1 != null) rblFAmilyUnderstanding.SelectedIndex = (int)survey.Question5b1;
            if (survey.Question5b2 != null) rblFAmilyUnderstandingShouldBe.SelectedIndex = (int)survey.Question5b2;
            if (survey.Question5c1 != null) rblFamilySupport.SelectedIndex = (int)survey.Question5c1;
            if (survey.Question5c2 != null) rblFamilySupportShouldBe.SelectedIndex = (int)survey.Question5c2;
            if (survey.Question5d1 != null) rblFamilySupportWork.SelectedIndex = (int)survey.Question5d1;
            if (survey.Question5d2 != null) rblFamilySupportWorkShouldbe.SelectedIndex = (int)survey.Question5d2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        //SaveData();
        Response.Redirect("surveyp43.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp52.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question5a1 = rblFamilyEngagement.SelectedIndex;
        survey.Question5a2 = rblFamilyEngagementShouldBe.SelectedIndex;
        survey.Question5b1 = rblFAmilyUnderstanding.SelectedIndex;
        survey.Question5b2 = rblFAmilyUnderstandingShouldBe.SelectedIndex;
        survey.Question5c1 = rblFamilySupport.SelectedIndex;
        survey.Question5c2 = rblFamilySupportShouldBe.SelectedIndex;
        survey.Question5d1 = rblFamilySupportWork.SelectedIndex;
        survey.Question5d2 = rblFamilySupportWorkShouldbe.SelectedIndex;
        survey.Save();
    }
}
