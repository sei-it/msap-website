﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4cc1 != null) rblDistrictAdministrators.SelectedIndex = (int)survey.Question4cc1;
            if (survey.Question4cc2 != null) rblDistrictAdministratorsShouldBe.SelectedIndex = (int)survey.Question4cc2;
            if (survey.Question4dd1 != null) rblCommunicatingParents.SelectedIndex = (int)survey.Question4dd1;
            if (survey.Question4dd2 != null) rblCommunicatingParentsShouldBe.SelectedIndex = (int)survey.Question4dd2;
            if (survey.Question4ee1 != null) rblCommunicatingTeachers.SelectedIndex = (int)survey.Question4ee1;
            if (survey.Question4ee2 != null) rblCommunicatingTeachersShouldBe.SelectedIndex = (int)survey.Question4ee2;
            if (survey.Question4ff1 != null) rblCommunicatingStaff.SelectedIndex = (int)survey.Question4ff1;
            if (survey.Question4ff2 != null) rblCommunicatingStaffShouldBe.SelectedIndex = (int)survey.Question4ff2;
            if (survey.Question4gg1 != null) rblCommunicatingStudents.SelectedIndex = (int)survey.Question4gg1;
            if (survey.Question4gg2 != null) rblCommunicatingStudentsShouldBe.SelectedIndex = (int)survey.Question4gg2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        //SaveData();
        Response.Redirect("surveyp41.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp43.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4cc1 = rblDistrictAdministrators.SelectedIndex;
        survey.Question4cc2 = rblDistrictAdministratorsShouldBe.SelectedIndex;
        survey.Question4dd1 = rblCommunicatingParents.SelectedIndex;
        survey.Question4dd2 = rblCommunicatingParentsShouldBe.SelectedIndex;
        survey.Question4ee1 = rblCommunicatingTeachers.SelectedIndex;
        survey.Question4ee2 = rblCommunicatingTeachersShouldBe.SelectedIndex;
        survey.Question4ff1 = rblCommunicatingStaff.SelectedIndex;
        survey.Question4ff2 = rblCommunicatingStaffShouldBe.SelectedIndex;
        survey.Question4gg1 = rblCommunicatingStudents.SelectedIndex;
        survey.Question4gg2 = rblCommunicatingStudentsShouldBe.SelectedIndex;
        survey.Save();
    }
}
