﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4t1 != null) rblSchoolImage.SelectedIndex= (int)survey.Question4t1 ;
            if (survey.Question4t2 != null) rblSchoolImageShouldBe.SelectedIndex= (int)survey.Question4t2 ;
            if (survey.Question4u1 != null) rblPromotingMagnet.SelectedIndex= (int)survey.Question4u1 ;
            if (survey.Question4u2 != null) rblPromotingMagnetShouldBe.SelectedIndex= (int)survey.Question4u2 ;
            if (survey.Question4v1 != null) rblMarketingPlan.SelectedIndex= (int)survey.Question4v1 ;
            if (survey.Question4v2 != null) rblMarketingPlanShouldBe.SelectedIndex= (int)survey.Question4v2 ;
            if (survey.Question4w1 != null) rblPlanEffectiveness.SelectedIndex= (int)survey.Question4w1 ;
            if (survey.Question4w2 != null) rblPlanEffectivenessShouldBe.SelectedIndex= (int)survey.Question4w2 ;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        //SaveData();
        Response.Redirect("surveyp31.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp41.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4t1 = rblSchoolImage.SelectedIndex;
        survey.Question4t2 = rblSchoolImageShouldBe.SelectedIndex;
        survey.Question4u1 = rblPromotingMagnet.SelectedIndex;
        survey.Question4u2 = rblPromotingMagnetShouldBe.SelectedIndex;
        survey.Question4v1 = rblMarketingPlan.SelectedIndex;
        survey.Question4v2 = rblMarketingPlanShouldBe.SelectedIndex;
        survey.Question4w1 = rblPlanEffectiveness.SelectedIndex;
        survey.Question4w2 = rblPlanEffectivenessShouldBe.SelectedIndex;
        survey.Save();
    }
}
