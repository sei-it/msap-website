﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_closure : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            MagnetSurveyUser user = MagnetSurveyUser.SingleOrDefault(y=>y.ID == survey.SurveyUserID);
            MagnetGranteeContact contact = MagnetGranteeContact.SingleOrDefault(x=>x.ID ==user.PrincipalID);
            //txtPrincipal.Text = contact.ContactLastName;
            Session["SurveyID"] = null;
            Session["UserID"] = null;
        }
    }
}
