﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp6 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            txtQuestion71.Text = survey.Question7a;
            txtQuestion72.Text = survey.Question7b;
            txtQuestion73.Text = survey.Question7c;
            if (!string.IsNullOrEmpty(survey.Question8))
            {
                foreach (string str in survey.Question8.Split(';'))
                {
                    foreach (ListItem item in cblPreferredMethods.Items)
                    {
                        if (item.Value.Equals(str))
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            txtOther1.Text = survey.Question8Other1;
            txtOther2.Text = survey.Question8Other2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {

        //if (CheckPreferredMethods())
        //{
            //SaveData();
        Response.Redirect("surveyp52.aspx", true);
        //}
    }
    protected void OnNext(object sender, EventArgs e)
    {
        if (CheckPreferredMethods())
        {
            SaveData();
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            survey.Submitted = true;
            survey.TimeStamp = DateTime.Now;
            survey.Save();
            Response.Redirect("survey7.aspx", true);
        }
    }
    private bool CheckPreferredMethods()
    {
        int count = 0;
        foreach (ListItem item in cblPreferredMethods.Items)
        {
            if (item.Selected)
            {
                count++;
            }
        }
        if (cbOther1.Checked) count++;
        if (CheckBox1.Checked) count++;

        if (count != 3)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "Unselect", "<script>alert('Please choose 3 options.');</script>", false);
            return false;
        }

        if (cbOther1.Checked && string.IsNullOrEmpty(txtOther1.Text))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "Unselect", "<script>alert('Please specify information.');</script>", false);
            return false;
        }

        if (CheckBox1.Checked && string.IsNullOrEmpty(txtOther2.Text))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "Unselect", "<script>alert('Please specify information.');</script>", false);
            return false;
        }

        return true;
    }
    protected void OnPreferredMethods(object sender, EventArgs e)
    {
        int count = 0;
        foreach (ListItem item in cblPreferredMethods.Items)
        {
            if (item.Selected)
            {
                count++;
            }
        }
        if (cbOther1.Checked) count++;
        if (CheckBox1.Checked) count++;

        if (count > 3)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "Unselect", "<script>alert('Please check no more than 3 selections!');</script>", false);
        }
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question7a = txtQuestion71.Text;
        survey.Question7b = txtQuestion72.Text;
        survey.Question7c = txtQuestion73.Text;
        string str = "";
        foreach (ListItem item in cblPreferredMethods.Items)
        {
            if (item.Selected)
            {
                if (string.IsNullOrEmpty(str))
                    str = item.Value;
                else
                    str += ";" + item.Value;
            }
        }
        survey.Question8 = str;
        if (cbOther1.Checked) survey.Question8Other1 = txtOther1.Text;
        if (CheckBox1.Checked) survey.Question8Other2 = txtOther2.Text;
        survey.Save();
    }
}
