﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp32.aspx.cs"
    Inherits="survey_surveyp3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <h2 style="color:#F90">MSAP Principal Survey</h2>
                <hr color="#A6CAF0" />
                <p style="font-size:12px">
                    
                    Each statement has two answers which will be recorded separately on this survey.
                    The first answer for each statement is “What Is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What Should Be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
          <p style="font-size:12px">
                    <strong>4. How would you rate your performance on the following activities and what
                        do you believe your performance should be? <br/>( 1= Needs improvement; 2= Adequate;
                        3= Good; 4= Excellent; N/A= Not applicable).</strong>
                </p>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                         <p style="font-size:14px">   <strong>What is</strong></p>
                        </td>
                        <td align="center">
                           <p style="font-size:14px"> <strong>What should be</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color: #A6CAF0">
                           <p style="font-size:12px"> <strong>Marketing:</strong></p>
                        </td>
                        <td width="40%" style="background-color: #A6CAF0">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                        <p style="font-size:12px"><strong>1</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>2</strong></p>
                                    </td>
                                    <td>
                                      <p style="font-size:12px">  <strong>3</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #A6CAF0">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                     <p style="font-size:12px">   <strong>1</strong></p>
                                    </td>
                                    <td>
                                      <p style="font-size:12px">  <strong>2</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>3</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"><strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                         <p style="font-size:12px">   t. Improving the image of your school</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblSchoolImage" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblSchoolImage"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblSchoolImageShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblSchoolImageShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <p style="font-size:12px"> u. Promoting the magnet program</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPromotingMagnet" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPromotingMagnet"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPromotingMagnetShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPromotingMagnetShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <p style="font-size:12px"> v. Developing a marketing plan</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblMarketingPlan" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblMarketingPlan"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblMarketingPlanShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblMarketingPlanShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-size:12px">w. Assessing the effectiveness of your marketing plan</p>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPlanEffectiveness" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPlanEffectiveness"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPlanEffectivenessShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPlanEffectivenessShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                         <table width="100%">
                    <tr>
                        <td align="left" width="10%">
                            &nbsp;<asp:Button ID="Button1" runat="server" CssClass="msapBtn" OnClick="OnPrevious" Text="Previous" />
                        </td>
                        <td align="center">
                            36% completed
                        </td>
                        <td align="right" width="10%">
                            <asp:Button ID="Button2" runat="server" CssClass="msapBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>

            <div class="addressFt">
                <div style="float: right; width: 300px;">
                    <strong>MSAP Technical Assistance Center</strong><br />
                    8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                    1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a>
                </div>
                <a href="http://www.ed.gov" target="_blank">
                    <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                        vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                            Funded by the<br />
                            U.S. Department of Education<br />
                            Contract Number: ED-OII-10-C-0079</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
