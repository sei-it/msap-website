﻿<%@ Page Title="MSAP Principal Survey" Language="C#" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Redirect("~/survey/closure.aspx", true);
    }
    
    protected void OnLogin(object sender, EventArgs e)
    {
        Response.Redirect("~/survey/closure.aspx", true);
        /*if (!string.IsNullOrEmpty(txtID.Text))
        {
            var users = Synergy.Magnet.MagnetSurveyUser.Find(x => x.Passcode.Equals(txtID.Text));
            if (users.Count > 0)
            {
                string id = Convert.ToString(users[0].ID);
                byte[] bytesToEncode = Encoding.UTF8.GetBytes(id);
                string encodedText = Convert.ToBase64String(bytesToEncode);
                Response.Redirect("~/survey/survey.aspx?id=" + encodedText, true);
            }
        }*/
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) - Log in</title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
                <p style="font-size: 12px">
                    The needs assessment principal survey administered by the U.S. Department of Education
                    and MSAP Center is now closed. The survey results will help us to deliver the most
                    relevant and focused technical assistance to MSAP grantees. We appreciate your participation.
                </p>
                <p style="font-size: 12px">
                    If you have questions, please contact the MSAP Center at (866)-997-6727 or at msapcenter@seiservices.com
                </p>
                <p style="font-size: 12px">
                    Thank you for your participation.
                </p>
                <p style="font-size: 12px">
                    Click <a href="../default.aspx">here</a> to go back to the MSAP Center homepage.
                </p>
            <div style="margin-bottom: 40px;display:none;">
                <p style="font-size:12px">
                    <strong>Public Burden Statement:</strong>
                    <br />
                    <br />
                    <strong>According to the Paperwork Reduction Act of 1995, no persons are required to
                        respond to a collection of information unless such collection displays a valid OMB
                        control number. Public reporting burden for this collection of information is estimated
                        to average 15 minutes per response, including time for reviewing instructions, searching
                        existing data sources, gathering and maintaining the data needed, and completing
                        and reviewing the collection of information. The obligation to respond to this collection
                        is voluntary. Send comments regarding the burden estimate or any other aspect of
                        this collection of information, including suggestions for reducing this burden,
                        to: U.S. Department of Education, 400 Maryland Ave., SW, Washington, DC 20210-4537
                        or email <a href="mailto:MMartucci@seiservices.com" title="ICDocketMgr@ed.gov">ICDocketMgr@ed.gov</a>
                        and reference the OMB Control Number 1800-0011. Note: Please do not return the completed
                        MSAP Principal Survey to this address.</strong>
                </p>
            </div>
            <table style="display:none;">
                <tr>
                    <td>
                        ID:
                    </td>
                    <td>
                        <asp:TextBox ID="txtID" runat="server" CssClass="msapTxt"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="Button1" CssClass="surveyBtn" runat="server" Text="Login" OnClick="OnLogin" />
                    </td>
                </tr>
            </table>
            <div class="surveyFt">
                <div style="float: right; width: 300px;">
                    <strong>MSAP Technical Assistance Center</strong><br />
                    8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                    1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com" style="color:#FFF">msapcenter@seiservices.com</a>
                </div>
                <a href="http://www.ed.gov" target="_blank">
                    <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                        vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                            Funded by the<br />
                            U.S. Department of Education<br />
                            Contract Number: ED-OII-10-C-0079</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
