﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="survey.aspx.cs"
    Inherits="survey_survey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../js/mbTooltip.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/mbTooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript">
        $(function () {
            $(".screenshot").mbTooltip({
                opacity: .97,
                wait: 400,
                cssClass: "default",
                timePerWord: 70,
                hasArrow: true,
                hasShadow: true,
                imgPath: "../images/",
                ancor: "mouse",
                shadowColor: "black",
                mb_fade: 200
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <h1 style="color: #F58220">
                MSAP Principal Survey</h1>
            <table class="surveyContact">
                <tr>
                    <td>
                        <table class="noneTbl">
                            <tr>
                                <td>
                                    <p style="font-size: 12px">
                                    Grantee Name:
                                    </p>
                                </td>
                                <td>
                                    <p style="font-size: 12px">
                                        <strong>
                                            <asp:Label ID="lblGranteeName" runat="server"></asp:Label></strong></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="noneTbl">
                            <tr>
                                <td>
                                    <p style="font-size: 12px">
                                    School Name:
                                    </p>
                                </td>
                                <td>
                                    <p style="font-size: 12px">
                                        <strong>
                                            <asp:Label ID="lblSchoolName" runat="server"></asp:Label></strong></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="noneTbl">
                            <tr>
                                <td>
                                    <p style="font-size: 12px">
                                    Principal:
                                    </p>
                                </td>
                                <td>
                                    <p style="font-size: 12px">
                                        <strong>
                                            <asp:Label ID="lblPrinpal" runat="server"></asp:Label></strong></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="noneTbl">
                            <tr>
                                <td>
                                    <p style="font-size: 12px">
                                    Telephone:
                                    </p>
                                </td>
                                <td>
                                    <p style="font-size: 12px">
                                        <strong>
                                            <asp:Label ID="lblTelephone" runat="server"></asp:Label></strong></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p style="font-size: 12px">
                Thank you for taking the time to participate in this survey. It will take 15 minutes.
                We are assessing the 2010 Magnet Schools Assistance Program (MSAP) grantees’ current
                technical assistance needs, and these surveys are part of our initial data collection
                effort. There are no right or wrong answers. We are interested in identifying your
                needs in program implementation and management in order to provide you with the
                most relevant and focused technical assistance.
            </p>
            <hr color="#A6CAF0" />
            <p style="color:#063; font-size:16px; style=margin-bottom:-7px;">
                Background
            </p>
            <table width="100%" style="margin-top:-8px;">
                <tr>
                    <td style="margin-top:2px;">
                        <p style="font-size: 12px;padding-top:0;margin-top:0;">
                            <strong>1. How many years have you worked in K-12 education?</strong></p>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlYearsInK12" runat="server"
                            CssClass="msapTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="1" Text="0-2 years"></asp:ListItem>
                            <asp:ListItem Value="2" Text="3-5 years"></asp:ListItem>
                            <asp:ListItem Value="3" Text="6-8 years"></asp:ListItem>
                            <asp:ListItem Value="4" Text="9 or more years"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required"
                            ControlToValidate="ddlYearsInK12"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-size: 12px">
                            <strong>2. Please indicate your program’s anticipated progress towards full implementation
                                in Fall 2011.</strong></p>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlProgress" runat="server" CssClass="msapTxt">
                            <asp:ListItem Value="">Please select</asp:ListItem>
                            <asp:ListItem Value="1" Text="Program installation"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Initial implementation"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Full operation"></asp:ListItem>
                        </asp:DropDownList>
                        <img src="../images/question_mark-thumb.png" class="screenshot" alt="Glossary of terms"
                            title="<strong><u>Glossary of terms</u></strong><br /><strong><u>Full operation</u></strong>- the program becomes integrated into practitioner, organizational, and community practices, policies, and procedures. At this point, the implemented program becomes fully operational with full staffing complements and all of the realities of “doing business” impinging on the newly implemented magnet program. Once fully operational, practitioners carry out the magnet program with proficiency and skill, administrators support and facilitate new practices, and the community has adapted to the presence of the magnet program.<br /><strong><u>Initial implementation</u></strong>- requires attempts to implement new practices effectively and make changes in the overall magnet environment. Changes in skill levels require education, practice, and time to mature. The program is struggling to begin and confidence in the decision to adopt the program is being tested during this stage of implementation.<br /> <strong><u>Program installation</u></strong>- after a decision is made to begin implementing MSAP, there are tasks that need to be accomplished; these activities define the installation stage of implementation. Resources are being consumed in active preparation for actually doing things differently in keeping with the tenets of MSAP and structural supports necessary to initiate the program are put in place.<br />(Definitions adapted from The National Implementation Research Network)" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required"
                            ControlToValidate="ddlProgress"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td />
                    <p style="font-size: 12px">
                        <strong>3. How satisfied are you with your job as a principal in this magnet school?</strong></p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlSatisfication" runat="server"
                        CssClass="msapTxt">
                        <asp:ListItem Value="">Please select</asp:ListItem>
                        <asp:ListItem Value="1" Text="Very dissatisfied"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Somewhat dissatisfied"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Somewhat satisfied"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Very satisfied"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required"
                        ControlToValidate="ddlSatisfication"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <table width="100%">
                    <tr>
                        <td align="left" width="40%">
                        </td>
                      <td align="center">
                            0% completed
                        </td>
                        <td align="right" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <asp:Button ID="Button2" runat="server" CssClass="surveyBtn" OnClick="OnNext" Text="Next" />
                      </td>
                    </tr>
                </table>
            <div class="surveyFt">
                   <div> <div style="float: right; width: 300px; font-size:11px" >
                        <strong>MSAP Technical Assistance Center</strong><br />
                        8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                        1-866-997-MSAP (6727) &#9679; <p style=" color:#FFF"><a href="mailto:msapcenter@seiservices.com">msapcenter@seiservices.com</a></p>
                    </div>
                    <a href="http://www.ed.gov" target="_blank">
                        <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;> 
                            vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server"/></a><div>
                                Funded by the<br />
                                U.S. Department of Education<br />
                                Contract Number: ED-OII-10-C-0079</div>
                </div>
                <div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
