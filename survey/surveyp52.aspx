﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="surveyp52.aspx.cs"
    Inherits="survey_surveyp5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSAP Principal Survey (Web-based) </title>
    <link href="~/css/msapMain.css" rel="stylesheet" type="text/css" />
    <link href="~/css/extra.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserID" runat="server" />
    <div class="msapWrap">
        <div class="msapHold">
            <div class="msapHdr">
                <a href="http://www.msapcenter.com">
                    <h1>
                    </h1>
                </a>
            </div>
            <div style="margin-top: 10px; margin-bottom: 10px;">
                <div style="display: none;">
                    <iframe id="KeepAliveFrame" src="KeepSessionAlive.aspx" frameborder="0" width="0"
                        height="0" runat="server"></iframe>
                </div>
                <h1 style="color: #F58220">
                    MSAP Principal Survey</h1>
                <hr color="#A6CAF0" />
                <p style="font-size: 12px">
                    Each statement has two answers that will be recorded separately on this survey.
                    The first answer for each statement is “What is” and relates to the current conditions
                    at your magnet school; the second answer for each statement is “What should be”
                    and relates to what you believe ought to be the situation for you or your magnet
                    program by May 2012. It is possible that many of your responses to “What Is” and
                    “What Should Be” will be the same.
                </p>
                <p style="color: #063; font-size: 16px">
                    Magnet Program</p>
                <p style="font-size: 12px">
                    <strong>6. Overall, how prepared are magnet teachers in your school, and how prepared
                        do you think they should be? <br />
                        (1= not at all prepared, 2= not very prepared, 3= prepared,
                        4= very prepared, or N/A= not applicable)</strong>
                </p>
                               <table class="surveyTbl" width="100%">
                    <tr>
                        <td style="border: 0;">
                        </td>
                        <td style="border: 0;" align="center">
                            <p style="font-size: 14px">
                                <strong>What is</strong></p>
                        </td>
                        <td align="center" style="border: 0;">
                            <p style="font-size: 14px">
                                <strong>What should be</strong></p>
                        </td>
                    </tr>

                    <tr>
                        <td width="30%" class="surveyTh2">
                            <p style="font-size: 13px"><strong>Magnet teachers</strong></p>
                        </td>
                        <td class="surveyTh2">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                    <p style="font-size:12px">    <strong>1</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>2</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>3</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>4</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="surveyTh2">
                            <table class="fullTbl">
                                <tr>
                                    <td>
                                       <p style="font-size:12px"> <strong>1</strong></p>
                                    </td>
                                    <td>
                                     <p style="font-size:12px">   <strong>2</strong></p>
                                    </td>
                                    <td>
                                       <p style="font-size:12px"> <strong>3</strong></p>
                                    </td>
                                    <td>
                                      <p style="font-size:12px">  <strong>4</strong></p>
                                    </td>
                                    <td>
                                        <p style="font-size:12px"><strong>N/A</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                   
                   
                   
                    <tr>
                    
                    <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        a.
                                    </td>
                                    <td>
                                        To develop magnet curriculum
                                    </td>
                                </tr>
                            </table> 
                      
                            
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDevelopCurriculum" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator92" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblDevelopCurriculum"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblDevelopCurriculumShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator93" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblDevelopCurriculumShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven">
                        <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        b.
                                    </td>
                                    <td>
                                        To integrate the magnet theme into classroom instruction
                                    </td>
                                </tr>
                            </table> 
                        
                        
                        
                        <td>
                            <asp:RadioButtonList ID="rblIntegrateTheme" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator94" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblIntegrateTheme"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblIntegrateThemeShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator95" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblIntegrateThemeShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        
                        <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        c.
                                    </td>
                                    <td>
                                        To teach the magnet school content
                                    </td>
                                </tr>
                            </table> 
                        
                        <td>
                            <asp:RadioButtonList ID="rblTeachContent" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator96" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblTeachContent"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblTeachContentShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator97" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblTeachContentShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven">
                        
                          <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        d.
                                    </td>
                                    <td>
                                        To access supplemental classroom materials
                                    </td>
                                </tr>
                            </table> 
                      
                        <td>
                            <asp:RadioButtonList ID="rblAccessMaterials" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAccessMaterials"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblAccessMaterialsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator99" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblAccessMaterialsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    
                      <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        e.
                                    </td>
                                    <td>
                                        To manage the magnet classroom
                                    </td>
                                </tr>
                            </table> 
                    
                    
                        <td>
                            <asp:RadioButtonList ID="rblManageClassroom" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblManageClassroom"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblManageClassroomShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblManageClassroomShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven">
                        <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        f.
                                    </td>
                                    <td>
                                        To work with children of diverse backgrounds
                                    </td>
                                </tr>
                            </table> 
                        
                        
                        <td>
                            <asp:RadioButtonList ID="rblWorkWithDiverseStudents" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator84" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblWorkWithDiverseStudents"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblWorkWithDiverseStudentsShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator85" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblWorkWithDiverseStudentsShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                   <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        g.
                                    </td>
                                    <td>
                                        To promote the magnet program
                                    </td>
                                </tr>
                            </table> 
                       
                       
                        
                        <td>
                            <asp:RadioButtonList ID="rblPromoteMagnet" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator86" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPromoteMagnet"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPromoteMagnetShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator87" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblPromoteMagnetShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="surveyEven SurveyLastTr">
                        
                        <td>
                          <table>
                                <tr style="font-size: 12px">
                                    <td valign="top">
                                        h.
                                    </td>
                                    <td>
                                        To engage families in supporting their children’s magnet school education
                                    </td>
                                </tr>
                            </table> 
                        
                       
                        <td>
                            <asp:RadioButtonList ID="rblEngageFamily" CssClass="fullTbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEngageFamily"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEngageFamilyShouldBe" CssClass="fullTbl" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="" Value="1"></asp:ListItem>
                                <asp:ListItem Text="" Value="2"></asp:ListItem>
                                <asp:ListItem Text="" Value="3"></asp:ListItem>
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="" Value="5"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator89" runat="server" ErrorMessage="Required"
                                ControlToValidate="rblEngageFamilyShouldBe"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table><br />
                <table width="100%">
                    <tr>
                        <td align="left" width="40%">
                        </td>
                        <td align="center">
                            81% completed
                        </td>
                        <td align="right" width="40%">
                            <asp:Button ID="Button1" runat="server" CssClass="surveyBtn" OnClick="OnPrevious"
                                Text="Previous"  OnClientClick="return confirm('Please note that all information on this page may not be saved if you go back to the previous page. Do you wish to proceed?');" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Button2" runat="server" CssClass="surveyBtn" OnClick="OnNext" Text="Next" />
                        </td>
                    </tr>
                </table>
                <div class="surveyFt">
                    <div style="float: right; width: 300px;">
                        <strong>MSAP Technical Assistance Center</strong><br />
                        8757 Georgia Ave., Suite 1440 &#9679; Silver Spring, MD 20910<br />
                        1-866-997-MSAP (6727) &#9679; <a href="mailto:msapcenter@seiservices.com" style="color:#FFF">msapcenter@seiservices.com</a>
                    </div>
                    <a href="http://www.ed.gov" target="_blank">
                        <img id="Img1" src="~/images/Edgov_ft.gif" border="0" alt="ed.gov" style="float: left;
                            vertical-align: middle; margin-right: 10px; margin-left: 16px;" runat="server" /></a><div>
                                Funded by the<br />
                                U.S. Department of Education<br />
                                Contract Number: ED-OII-10-C-0079</div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
