﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question4a1 != null) rblLeaderShip.SelectedIndex = (int)survey.Question4a1;
            if (survey.Question4a2 != null) rblLeaderShipShouldBe.SelectedIndex = (int)survey.Question4a2;
            if (survey.Question4b1 != null) rblHiring.SelectedIndex = (int)survey.Question4b1;
            if (survey.Question4b2 != null) rblHiringShouldBe.SelectedIndex = (int)survey.Question4b2;
            if (survey.Question4c1 != null) rblMotivateTeacher.SelectedIndex = (int)survey.Question4c1;
            if (survey.Question4c2 != null) rblMotivateTeacherShouldBe.SelectedIndex = (int)survey.Question4c2;
            if (survey.Question4d1 != null) rblObtaningMaterial.SelectedIndex = (int)survey.Question4d1;
            if (survey.Question4d2 != null) rblObtaningMaterialShouldBe.SelectedIndex = (int)survey.Question4d2;
            if (survey.Question4e1 != null) rblImplementingProject.SelectedIndex = (int)survey.Question4e1;
            if (survey.Question4e2 != null) rblImplementingProjectShouldBe.SelectedIndex = (int)survey.Question4e2;
            if (survey.Question4f1 != null) rblImplementingCriticalComponents.SelectedIndex = (int)survey.Question4f1;
            if (survey.Question4f2 != null) rblImplementingCriticalComponentsShouldBe.SelectedIndex = (int)survey.Question4f2;
            if (survey.Question4g1 != null) rblAssessing.SelectedIndex = (int)survey.Question4g1;
            if (survey.Question4g2 != null) rblAssessingShouldBe.SelectedIndex = (int)survey.Question4g2;
            if (survey.Question4h1 != null) rblAssessingProgress.SelectedIndex = (int)survey.Question4h1;
            if (survey.Question4h2 != null) rblAssessingProgressShouldBe.SelectedIndex = (int)survey.Question4h2;
            if (survey.Question4i1 != null) rblInterpreting.SelectedIndex = (int)survey.Question4i1;
            if (survey.Question4i2 != null) rblInterpretingShouldBe.SelectedIndex = (int)survey.Question4i2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        //SaveData();
        Response.Redirect("survey.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp22.aspx", true);
    }
    private void SaveData()
    {
        if (Session["SurveyID"] == null) Response.Redirect("~/survey/login.aspx");
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question4a1 = rblLeaderShip.SelectedIndex;
        survey.Question4a2 = rblLeaderShipShouldBe.SelectedIndex;
        survey.Question4b1 = rblHiring.SelectedIndex;
        survey.Question4b2 = rblHiringShouldBe.SelectedIndex;
        survey.Question4c1 = rblMotivateTeacher.SelectedIndex;
        survey.Question4c2 = rblMotivateTeacherShouldBe.SelectedIndex;
        survey.Question4d1 = rblObtaningMaterial.SelectedIndex;
        survey.Question4d2 = rblObtaningMaterialShouldBe.SelectedIndex;
        survey.Question4e1 = rblImplementingProject.SelectedIndex;
        survey.Question4e2 = rblImplementingProjectShouldBe.SelectedIndex;
        survey.Question4f1 = rblImplementingCriticalComponents.SelectedIndex;
        survey.Question4f2 = rblImplementingCriticalComponentsShouldBe.SelectedIndex;
        survey.Question4g1 = rblAssessing.SelectedIndex;
        survey.Question4g2 = rblAssessingShouldBe.SelectedIndex;
        survey.Question4h1 = rblAssessingProgress.SelectedIndex;
        survey.Question4h2 = rblAssessingProgressShouldBe.SelectedIndex;
        survey.Question4i1 = rblInterpreting.SelectedIndex;
        survey.Question4i2 = rblInterpretingShouldBe.SelectedIndex;
        survey.Save();
    }
}
