﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class survey_surveyp5 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
            if (survey.Question5a1 != null) rblFamilyEngagement.SelectedIndex = (int)survey.Question5a1;
            if (survey.Question5a2 != null) rblFamilyEngagementShouldBe.SelectedIndex = (int)survey.Question5a2;
            if (survey.Question5b1 != null) rblFAmilyUnderstanding.SelectedIndex = (int)survey.Question5b1;
            if (survey.Question5b2 != null) rblFAmilyUnderstandingShouldBe.SelectedIndex = (int)survey.Question5b2;
            if (survey.Question5c1 != null) rblFamilySupport.SelectedIndex = (int)survey.Question5c1;
            if (survey.Question5c2 != null) rblFamilySupportShouldBe.SelectedIndex = (int)survey.Question5c2;
            if (survey.Question5d1 != null) rblFamilySupportWork.SelectedIndex = (int)survey.Question5d1;
            if (survey.Question5d2 != null) rblFamilySupportWorkShouldbe.SelectedIndex = (int)survey.Question5d2;
            if (survey.Question6a1 != null) rblDevelopCurriculum.SelectedIndex = (int)survey.Question6a1;
            if (survey.Question6a2 != null) rblDevelopCurriculumShouldBe.SelectedIndex = (int)survey.Question6a2;
            if (survey.Question6b1 != null) rblIntegrateTheme.SelectedIndex = (int)survey.Question6b1;
            if (survey.Question6b2 != null) rblIntegrateThemeShouldBe.SelectedIndex = (int)survey.Question6b2;
            if (survey.Question6c1 != null) rblTeachContent.SelectedIndex = (int)survey.Question6c1;
            if (survey.Question6c2 != null) rblTeachContentShouldBe.SelectedIndex = (int)survey.Question6c2;
            if (survey.Question6d1 != null) rblAccessMaterials.SelectedIndex = (int)survey.Question6d1;
            if (survey.Question6d2 != null) rblAccessMaterialsShouldBe.SelectedIndex = (int)survey.Question6d2;
            if (survey.Question6e1 != null) rblManageClassroom.SelectedIndex = (int)survey.Question6e1;
            if (survey.Question6e2 != null) rblManageClassroomShouldBe.SelectedIndex = (int)survey.Question6e2;
            if (survey.Question6f1 != null) rblWorkWithDiverseStudents.SelectedIndex = (int)survey.Question6f1;
            if (survey.Question6f2 != null) rblWorkWithDiverseStudentsShouldBe.SelectedIndex = (int)survey.Question6f2;
            if (survey.Question6g1 != null) rblPromoteMagnet.SelectedIndex = (int)survey.Question6g1;
            if (survey.Question6g2 != null) rblPromoteMagnetShouldBe.SelectedIndex = (int)survey.Question6g2;
            if (survey.Question6h1 != null) rblEngageFamily.SelectedIndex = (int)survey.Question6h1;
            if (survey.Question6h2 != null) rblEngageFamilyShouldBe.SelectedIndex = (int)survey.Question6h2;
        }
    }
    protected void OnPrevious(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp4.aspx", true);
    }
    protected void OnNext(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("surveyp6.aspx", true);
    }
    private void SaveData()
    {
        MagnetSurvey survey = MagnetSurvey.SingleOrDefault(x => x.ID == (int)Session["SurveyID"]);
        survey.Question5a1 = rblFamilyEngagement.SelectedIndex;
        survey.Question5a2 = rblFamilyEngagementShouldBe.SelectedIndex;
        survey.Question5b1 = rblFAmilyUnderstanding.SelectedIndex;
        survey.Question5b2 = rblFAmilyUnderstandingShouldBe.SelectedIndex;
        survey.Question5c1 = rblFamilySupport.SelectedIndex;
        survey.Question5c2 = rblFamilySupportShouldBe.SelectedIndex;
        survey.Question5d1 = rblFamilySupportWork.SelectedIndex;
        survey.Question5d2 = rblFamilySupportWorkShouldbe.SelectedIndex;
        survey.Question6a1 = rblDevelopCurriculum.SelectedIndex;
        survey.Question6a2 = rblDevelopCurriculumShouldBe.SelectedIndex;
        survey.Question6b1 = rblIntegrateTheme.SelectedIndex;
        survey.Question6b2 = rblIntegrateThemeShouldBe.SelectedIndex;
        survey.Question6c1 = rblTeachContent.SelectedIndex;
        survey.Question6c2 = rblTeachContentShouldBe.SelectedIndex;
        survey.Question6d1 = rblAccessMaterials.SelectedIndex;
        survey.Question6d2 = rblAccessMaterialsShouldBe.SelectedIndex;
        survey.Question6e1 = rblManageClassroom.SelectedIndex;
        survey.Question6e2 = rblManageClassroomShouldBe.SelectedIndex;
        survey.Question6f1 = rblWorkWithDiverseStudents.SelectedIndex;
        survey.Question6f2 = rblWorkWithDiverseStudentsShouldBe.SelectedIndex;
        survey.Question6g1 = rblPromoteMagnet.SelectedIndex;
        survey.Question6g2 = rblPromoteMagnetShouldBe.SelectedIndex;
        survey.Question6h1 = rblEngageFamily.SelectedIndex;
        survey.Question6h2 = rblEngageFamilyShouldBe.SelectedIndex;
        survey.Save();
    }
}
