﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Technical Assistance Webinar</title>
<style type="text/css">
<!--
body {
	font: 12px Verdana, Arial, Helvetica, sans-serif;
	background: #fff;
	margin: 0; 
	padding: 0;
	text-align: center; 
	color: #000000;
}

.oneColElsCtrHdr #container {
	width: 960px;  
	background: #FFFFFF;
	margin: 0 auto;
	border: 1px solid #000000;
	text-align: left; 
}
.oneColElsCtrHdr #container h1{
    color: #F58220;
    font-size: 22px;
    font-weight: 100;
    margin-top: 15px;
    padding-top: 10px;
    width: 842px;
}
.oneColElsCtrHdr #subtitle {
    color: #2274A0;
    font-size: 14px;
    font-weight: bold;
	line-height:20px;
}


.oneColElsCtrHdr #header { 
	background: #DDDDDD; 
	padding: 0px;  
} 
.oneColElsCtrHdr #header h1 {
	margin: 0; 
	padding: 10px 0; 
}
.oneColElsCtrHdr #mainContent {
	padding: 0 20px; 
	background: #FFFFFF;
}
.oneColElsCtrHdr #footer { 
	padding: 0px; 
	background:#fff;
} 
.oneColElsCtrHdr #footer p {
	margin: 0; 
	padding: 10px 0;
}
a, a:visited {
	color: #F58220;
	text-decoration:none;
}
		
a:hover {
	text-decoration: none;
}

#download_list li{
	padding-bottom:10px;
}
-->
</style></head>
<body class="oneColElsCtrHdr">

<div id="container">
  <div id="header">
    <img src="Webinar_docs/TA_webinar_LandingPage_header_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #header --></div>
  <div id="mainContent">
    <h1>Program Sustainability Resources</h1>
   
   <table width="90%" border="0">
   		<tr>
 			<td style="text-align:right;vertical-align:top;">
            	<img src="Webinar_docs/Program_sustainability_webinar_2_22_12/PS_webinar_img.jpg" style="border:2px #2274A0 solid;" />
            </td>
   			<td style="vertical-align:top;padding-left:15px;">
             <div><span id="subtitle">Planning for Sustainability: Building the Framework</span><br/>
  Thank you for registering for the MSAP Center's Planning for Sustainability: Building the Framework webinar on February 22, 2012. Please download the following documents in preparation for the webinar. 
   
   <p>Sustaining a magnet school program depends on developing a clear, sensible, and convincing plan for organizing the key resources you need to continue—and to expand—your program. This four-part webinar series will help you to develop a sustainability plan for your Magnet Schools Assistance Program (MSAP) project.</p>
   
   <p>The first webinar, Planning for Sustainability: Building the Foundation, and the related tools will help you gather the necessary components. As you learn how to launch sustainability planning and who to include in this effort, you will also learn about adapting the structure to fit the process at your site.</p> 
   Here is the first set of tools:
   </div>
            <ol id="download_list">
                <li><strong>Getting Started: A Sustainability Self-Assessment</strong>: This tool is intended to help you and other magnet school leaders, such as school principals, superintendents, and school board members,  identify areas that need extra attention, resources, or technical assistance. <a href="Webinar_docs/Program_sustainability_webinar_2_22_12/5_Assessment_tool_with_instructions.doc" target="_blank">Download</a>
                </li>
                <li><strong>Worksheet: Clarifying MSAP Planning Parameters</strong>: This worksheet can help you set clear expectations for the sustainability planning process by addressing a set of key planning questions. 
                <a href="Webinar_docs/Program_sustainability_webinar_2_22_12/6_Worksheet_Clarifying_Planning_Parameters.doc" target="_blank">Download</a>
                </li>
                <li><strong>Sample Timeline for MSAP Sustainability Planning</strong>: This timeline offers an example of an 8-month planning process. Although your planning timeline may be shorter or longer, this tool can help you establish a structure and set a schedule that fit your project. <a href="Webinar_docs/Program_sustainability_webinar_2_22_12/7_Sample_Timeline_for_Sustainability_Planning.doc" target="_blank">Download</a>
                </li>
            </ol>
            </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
     </table>
	<!-- end #mainContent --></div>
  <div id="footer">
    <img src="Webinar_docs/TA_webinar_LandingPage_footer_960px.png" alt="Technical Assistance Webinar"/>
  <!-- end #footer --></div>
<!-- end #container --></div>
</body>
</html>
