﻿        function percentage_vald1(sender, args) {
            var num = document.getElementById('<%=txtAllcohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtAllgraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald2(sender, args) {
            var num = document.getElementById('<%=txtAmericanIndiancohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtAmericanIndiangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald3(sender, args) {
            var num = document.getElementById('<%=txtAsiancohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtAsiangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald4(sender, args) {
            var num = document.getElementById('<%=txtAfricanAmericancohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtAfricanAmericangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald5(sender, args) {
            var num = document.getElementById('<%=txtHispanicLatinocohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtHispanicLatinograduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald6(sender, args) {
            var num = document.getElementById('<%=txtNativeHawaiiancohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtNativeHawaiiangraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald7(sender, args) {
            var num = document.getElementById('<%=txtWhitecohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtWhitegraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald8(sender, args) {
            var num = document.getElementById('<%=txtMoreRacescohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtMoreRacesgraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald9(sender, args) {
            var num = document.getElementById('<%=txtEconomicallycohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtEconomicallygraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

        function percentage_vald10(sender, args) {
            var num = document.getElementById('<%=txtEnglishlearnerscohort.ClientID%>').value;
            var deno = document.getElementById('<%=txtEnglishlearnersgraduated.ClientID%>').value;
            if ((num == '' && deno != '') || (num != '' && deno == '')) {
                args.IsValid = false;
            }
        }

       
        function txtpercentage1() {
            ch1 = $("#<%=txtAllcohort.ClientID%>").val();
            ch2 = $("#<%=txtAllgraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtallpercentage.ClientID%>").val(ch3.toFixed(1)+'%');
            }
            else {
                $("#<%=txtallpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage2() {
            ch1 = $("#<%=txtAmericanIndiancohort.ClientID%>").val();
            ch2 = $("#<%=txtAmericanIndiangraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtAmericanIndianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtAmericanIndianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage3() {
            ch1 = $("#<%=txtAsiancohort.ClientID%>").val();
            ch2 = $("#<%=txtAsiangraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtAsianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtAsianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage4() {
            ch1 = $("#<%=txtAfricanAmericancohort.ClientID%>").val();
            ch2 = $("#<%=txtAfricanAmericangraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtAfricanAmericanpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtAfricanAmericanpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage5() {
            ch1 = $("#<%=txtHispanicLatinocohort.ClientID%>").val();
            ch2 = $("#<%=txtHispanicLatinograduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtHispanicLatinopercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtHispanicLatinopercentage.ClientID%>").val('');
            }
        }

        function txtpercentage6() {
            ch1 = $("#<%=txtNativeHawaiiancohort.ClientID%>").val();
            ch2 = $("#<%=txtNativeHawaiiangraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtNativeHawaiianpercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtNativeHawaiianpercentage.ClientID%>").val('');
            }
        }

        function txtpercentage7() {
            ch1 = $("#<%=txtWhitecohort.ClientID%>").val();
            ch2 = $("#<%=txtWhitegraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtWhitepercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtWhitepercentage.ClientID%>").val('');
            }
        }

        function txtpercentage8() {
            ch1 = $("#<%=txtMoreRacescohort.ClientID%>").val();
            ch2 = $("#<%=txtMoreRacesgraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtMoreRacespercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtMoreRacespercentage.ClientID%>").val('');
            }
        }

        function txtpercentage9() {
            ch1 = $("#<%=txtEconomicallycohort.ClientID%>").val();
            ch2 = $("#<%=txtEconomicallygraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtEconomicallypercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtEconomicallypercentage.ClientID%>").val('');
            }
        }

        function txtpercentage10() {
            ch1 = $("#<%=txtEnglishlearnerscohort.ClientID%>").val();
            ch2 = $("#<%=txtEnglishlearnersgraduated.ClientID%>").val();
            ch3 = '';

            if (ch1 != '' && ch2 != '') {
                ch3 = (ch1 / ch2) * 100;
                $("#<%=txtEnglishlearnerspercentage.ClientID%>").val(ch3.toFixed(1) + '%');
            }
            else {
                $("#<%=txtEnglishlearnerspercentage.ClientID%>").val('');
            }
        }