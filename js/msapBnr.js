// JavaScript Document

bnrArray = new Array(
new Array(
	'img1.jpg',
	'',
	'Kids jumping in the air. The banner reads Diversity, Academic Excellence, Equity.'
	),
new Array(
	'img2.jpg',
	'',
	'Diverse group of children reading. The banner reads Diversity, Academic Excellence, Equity.'
	),
	new Array(
	'img3.jpg',
	'',
	'Graduates in cap and gown. The banner reads Diversity, Academic Excellence, Equity.'
	),
	new Array(
	'img4.jpg',
	'',
	'Diverse group of children. The banner reads Diversity, Academic Excellence, Equity.'
	)
);

var prd=5000;
var tmr;
var sts=0;
var idx=0;
var ida='homeRBa';
var idi='homeRBimg';

function bnrInit()
{
	idx=mod(new Date().getSeconds(),bnrArray.length);
	document.write('<a href="'+bnrArray[idx][1]+'" id="'+ida+'">');
	document.write('<img src="images/'+bnrArray[idx][0]+'" alt="'+bnrArray[idx][2]+'" id="'+idi+'" width="842" height="140" border="0">');
	document.write('</a>');
	bnrStart();
}

function bnr(step,e)
{
	if(step=='0')
	{
		if(sts==1){bnrStop();}else{bnrStart();}
		e.src=(sts==1?'images/stop.png':'images/play.png');
		e.title=(sts==1?'Stop Banners':'Start Banners');
	}
	else if(sts==1){bnrStep(step);}
	else if(sts==0){bnrStep(step);bnrStop();}
}

function bnrStart()
{
	idx++;
	if(idx>=bnrArray.length){idx=0;}
	document.getElementById(ida).href=bnrArray[idx][1];
	document.getElementById(idi).src='images/'+bnrArray[idx][0];
	document.getElementById(idi).alt=bnrArray[idx][2];
	tmr=setTimeout('bnrStart()',prd);
	sts=1;
}

function bnrStop()
{
	clearTimeout(tmr);
	sts=0;
}

function bnrStep(step)
{
	bnrStop();
	if(step=='-1')
	{
		if(idx==1){idx=bnrArray.length-1;}
		else if(idx==0){idx=bnrArray.length-2;}
		else{idx-=2;}
	}
	bnrStart();
}

function mod(X,Y)
{
	return(X-Math.floor(X/Y)*Y);
}