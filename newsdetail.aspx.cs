﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class newsdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            MagnetNews news = MagnetNews.SingleOrDefault(x => x.ID == Convert.ToInt32(Request.QueryString["id"]));
            mainContent.InnerHtml = string.Format("<b>{0}</b><br /><i>{2} {3} {4}.</i><br />{1}<br>Visit the <a href='news.aspx'>News Archive</a> for more magnet school news.", 
                news.Title, news.NewsContent,
                !string.IsNullOrEmpty(news.Source)?news.Source + ".":news.Source,
                !string.IsNullOrEmpty(news.Author)?news.Author + ".":news.Author,
                news.Date);
        }
    }
}