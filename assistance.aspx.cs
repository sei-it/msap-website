﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Net;
using System.Net.Mail;
using System.IO;

public partial class assistance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (MagnetState state in MagnetState.All())
            {
                ddlState.Items.Add(new ListItem(state.StateFullName, state.StateAbb));
            }
        }
    }
    protected void OnSubmit(object sender, EventArgs e)
    {
        if (CaptchaControl1.UserValidated && !string.IsNullOrEmpty(txtFirstName.Text)
            && !string.IsNullOrEmpty(txtLastName.Text) && !string.IsNullOrEmpty(txtEmail.Text))
        {
            MagnetTARequest request = new MagnetTARequest();
            request.FirstName = txtFirstName.Text;
            request.LastName = txtLastName.Text;
            request.Title = txtTitle.Text;
            request.District = txtDistrict.Text;
            request.Address = txtAddress.Text;
            request.Addresscon = txtAddressCont.Text;
            request.City = txtCity.Text;
            request.State = ddlState.SelectedValue;
            request.Telephone = txtTelephone.Text;
            request.AlternatePhone = txtAlternative.Text;
            request.Fax = txtFax.Text;
            request.Email = txtEmail.Text;
            request.PreferredMethod = rblPreferredMethod.SelectedIndex == 0 ? true : false;
            request.BestTime = txtHour.Text + " " + cblBestTime.SelectedValue;
            request.TARequest = Server.HtmlEncode(txtTARequest.Content);
            request.CreatedDate = DateTime.Now;
            request.Save();

            //Create maildefination
            System.Web.UI.WebControls.MailDefinition mail = new System.Web.UI.WebControls.MailDefinition();
            mail.IsBodyHtml = true;

            mail.From = "msapcenter@leedmci.com";
            mail.Subject = "New TA request";
            string htmlBody = "<div>Dear manager,</div><div>A new TA request has been submitted online. Please check it out by loggin in private site.</div>";
            //Create email message
            MailMessage mess = mail.CreateMailMessage("msapcenter@leedmci.com", new ListDictionary(), htmlBody, new System.Web.UI.Control());
            mess.CC.Add(new MailAddress("Bbrown@leedmci.com"));
            mess.CC.Add(new MailAddress("EFord@leedmci.com"));

            SmtpClient objSMTPClient = new SmtpClient();
            objSMTPClient.Host = "mail2.seiservices.com";
            NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
            objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;

            mess.IsBodyHtml = true;

            try
            {
                objSMTPClient.Send(mess);

                mess.Dispose();
            }
            catch (Exception ex)
            {
            }

            mpeWindow.Show();
        }
    }
    protected void OnOK(object sender, EventArgs e)
    {
        Response.Redirect("default.aspx");
    }
}