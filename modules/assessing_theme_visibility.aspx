﻿<%@ Page Title="MSAP Center: Assessing Theme Visibility" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Assessing Theme Visibility
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li class="on"><a href="/modules/assessing_theme_visibility.aspx" class="on">Assessing Theme Visibility</a>
	<ul>
	<li><a href="/modules/assess_understanding_theme.aspx">Assess Understanding of Magnet Theme</a></li>
	<li><a href="/modules/survey.aspx">Survey Staff, Students, and Community</a></li>
	<li><a href="/modules/clarity_theme.aspx">Gain Clarity on Theme Visibility and Understanding</a></li>
	<li><a href="/modules/action_plan.aspx">Develop an Action Plan</a></li>
	<li><a href="/modules/reinforcing_theme.aspx">Ideas for Reinforcing the Magnet Theme</a></li>
	</ul>
</li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Assessing Theme Visibility</h2>

<img src="/modules/imgs/photo_assessing.jpg" alt="student pointing to pinboard" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>Part of building a shared understanding of the mission and goals of a magnet school is building the visibility of the magnet theme within the school and surrounding community. </p>

<p>This lesson offers tools and ideas for supporting the Magnet Identity Team as it assesses knowledge of the school theme and helps to brand and publicize the theme for greater recognition and pride. Ultimately, a clear identity of the theme may contribute to sustained enrollment, continued funding, and dynamic partnerships.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>When forming the Magnet Identity Team, remember that parents/guardians or other community members may have experience in marketing. Invite them to join or serve as consultants for the team.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />


</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="obj">OBJECTIVES</h3>

<ul>
<li>Assess understanding of magnet theme</li>
<li>Gain clarity on theme visibility and understanding</li>
<li>Develop an action plan</li>
<li>Propose ideas for reinforcing the magnet theme</li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="doc"><a href="/modules/downloads/school_walkthrough.doc">A School Walkthrough: Gathering Information Schoolwide (Activity)</a></li>
<li class="doc"><a href="/modules/downloads/sample_survey_questions.doc">Sample Survey Questions</a></li>
<li class="doc"><a href="/modules/downloads/summarizing_team_findings.doc">Summarizing Team Findings</a></li>
<li class="doc"><a href="/modules/downloads/team_meeting_notetaker.doc">Team Meeting Notetaker</a></li>
<li class="doc"><a href="/modules/downloads/theme_integration_action_plan.doc">Theme Integration Action Plan</a></li>
<li class="pdf"><a href="/modules/downloads/magnet_compass_vol2_issue2.pdf" target="_blank">The Magnet Compass, May 2011</a></li>
<!--<li><a href="/modules/downloads/.docx">Putting Ideas Into Action</a></li>-->
</ul>

</div>
</div>
<!-- mod end -->


</div>
<!-- Right Column end -->






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
