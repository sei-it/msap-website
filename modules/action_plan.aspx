﻿<%@ Page Title="MSAP Center: Develop an Action Plan" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Develop an Action Plan
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li class="on"><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a>
	<ul>
	<li><a href="/modules/assess_understanding_theme.aspx">Assess Understanding of Magnet Theme</a></li>
	<li><a href="/modules/survey.aspx">Survey Staff, Students, and Community</a></li>
	<li><a href="/modules/clarity_theme.aspx">Gain Clarity on Theme Visibility and Understanding</a></li>
	<li><a href="/modules/action_plan.aspx" class="on">Develop an Action Plan</a></li>
	<li><a href="/modules/reinforcing_theme.aspx">Ideas for Reinforcing the Magnet Theme</a></li>
	</ul>
</li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Assessing Theme Visibility</em> Develop an Action Plan</h2>

<p>When it’s time for the team to develop the action plan, refer to the downloadable <a href="/modules/downloads/theme_integration_action_plan.doc" class="doc">Theme Integration Action Plan (.doc)</a>.  This tool can help the team identify tasks, assign people to do them, and set a timeframe for completing the work. The suggested steps toward developing the plan are these:</p>

<!-- two column -->
<div class="twoCol">
<h4>STEP 1</h4>
<p>Prioritize the team’s focus and efforts.</p>

<h4>STEP 2</h4>
<p>Start with the end goal in mind.</p>

<h4>STEP 3</h4>
<p>Determine what teachers, students, parents, and community need to know to make this a reality.</p>

<h4>STEP 4</h4>

<p>Map backwards to the present.</p>

<h4>STEP 5</h4>

<p>Identify what is needed from other groups or organizations.</p>

</div>
<!-- two column end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Do these steps sound familiar? They resemble the steps that are laid out in the MSAP Center’s webinar series on Sustainability Planning. Members of the theme integration teams might find that members of the school sustainability team have good information and planning advice.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />







</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
