﻿<%@ Page Title="MSAP Center: Assess Understanding of Magnet Theme" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Assess Understanding of Magnet Theme
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li class="on"><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a>
	<ul>
	<li><a href="/modules/assess_understanding_theme.aspx" class="on">Assess Understanding of Magnet Theme</a></li>
	<li><a href="/modules/survey.aspx">Survey Staff, Students, and Community</a></li>
	<li><a href="/modules/clarity_theme.aspx">Gain Clarity on Theme Visibility and Understanding</a></li>
	<li><a href="/modules/action_plan.aspx">Develop an Action Plan</a></li>
	<li><a href="/modules/reinforcing_theme.aspx">Ideas for Reinforcing the Magnet Theme</a></li>
	</ul>
</li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Assessing Theme Visibility</em> Assess Understanding of Magnet Theme</h2>

<p>The team will collect data to take stock of the magnet theme’s on-campus visibility and the awareness of the theme among staff, students, and community. This multipart process will be worth the time it takes because it will help to focus the team’s efforts and strategies for next steps. </p>

<h3>Walkthroughs: Guided Investigation</h3>

<p>Examine school materials (for example, the student handbook) and conduct a school walkthrough to quickly gather information and to look at school surroundings with a different perspective. </p>

<p>Present the process as a “magnet identity scavenger hunt” that allows team members to gather information about the visible and tangible manifestations of the magnet theme. Download the activity, <a href="/modules/downloads/school_walkthrough.doc" class="doc">A School Walkthrough: Gathering Information Schoolwide (.doc)</a>, to help organize the team’s information collection efforts.</p>

<blockquote>
<p><strong>School example</strong> Conducting walkthroughs as a “school theme scavenger hunt” provided one science magnet middle school with an opportunity to view classrooms in a non-evaluative way. Equipped with lists of scavenger items to identify, faculty and staff teams walked the school grounds and visited classrooms to gather evidence, taking pictures of examples and collecting artifacts. The team collecting the most items on their list was rewarded with a lunch prepared by parents. While some teachers initially opted out of the process, the majority eventually joined in.</p>
</blockquote>

<p>The observer’s subjective discretion and input is particularly important in analyzing this information. Walkthrough notes should be informative to help the team consider known issues more deeply and identify new issues for review. </p>

<h4>Try these or similar questions to guide the investigation</h4>

<ul>
<li>Is the magnet theme prominently displayed in classrooms, campus walls, school correspondence, school merchandise, or on the school website? </li>
<li>Is the theme more visible in some grades or subjects than others?</li>
<li>Is the magnet theme part of the school facility (such as an engineering laboratory in a STEM school, or a theater in a performing arts school)?</li>
<li>Which, if any, schoolwide activities or events exemplify the theme (perhaps an art exhibit, a health fair, or a culminating field trip)?</li>
</ul>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<!-- two column -->
<div class="twoCol">
<h4>TIP 1</h4>

<p>Prior to beginning data collection, the team should meet to set some general guidelines and parameters for collecting and recording data to ensure a certain degree of reliability. This will make the interpretation and analysis much more straightforward.</p>

<hr />

<h4>TIP 2</h4>

<p>MSAP grantees may also want to examine their MSAP applications for reference to the intended theme or specific pedagogical approach initially proposed. Look for details on how the theme and strategies for implementation are described in the application.</p>
</div>
<!-- two column end -->
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Invite parents and students to be a part of the information-gathering activity. They will bring a fresh set of eyes to the process and provide an opportunity to spread the word on the magnet’s theme.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />








</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
