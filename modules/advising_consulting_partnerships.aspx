﻿<%@ Page Title="MSAP Center: Advising and Consulting Partnerships" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Advising and Consulting Partnerships
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li class="on"><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a>
	<ul>
	<li><a href="/modules/sustainable_partnerships.aspx">Build Sustainable Partnerships</a></li>
	<li><a href="/modules/partnership_opportunities.aspx">Explore Partnership Opportunities</a></li>
	<li><a href="/modules/learning_opportunities.aspx">Explore Learning Opportunities in the Community</a></li>
	<li><a href="/modules/volunteer_opportunities.aspx">Volunteer Opportunities</a></li>
	<li><a href="/modules/professional_development.aspx">Professional Development and Support for Teachers</a></li>
	<li><a href="/modules/advising_consulting_partnerships.aspx" class="on">Advising and Consulting Partnerships</a></li>
	<li><a href="/modules/encourage_family_support.aspx">Encourage Family Support</a></li>
	<li><a href="/modules/common_understanding.aspx">Create a Common Understanding and Vision</a></li>
	</ul>
</li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Connecting Theme, Students, and Community</em> Advising and Consulting Partnerships</h2>

<p>An advisory board can help a school leverage the knowledge that resides in the community, including parents, small business owners, church leaders, and community-based organization leaders. Advisory board members offer their professional experiences and knowledge, plus their professional contacts for potential new partnerships.</p>

<blockquote>
<p><strong>School Example</strong> A magnet school with an industrial design theme has regular meetings with an advisory board made up of community members from small businesses, manufacturing and engineering companies, and a marketing firm. These relationships have provided support for the school leaders, content knowledge for teachers, and internships for students.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>As a general practice, invite community members to offer suggestions and feedback on the magnet program. Ask people in the community to share what they value in a quality public school and have them suggest ways that they can contribute to this goal.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<blockquote>
<p><strong>School Example</strong> After a natural disaster destroyed an observation tower in one community, students from a design theme magnet high school partnered with a local architecture firm to design a replacement tower. In this way, the school was able to give back to the community that supported it.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>When forming partnerships, consider how both parties—magnet school and organization/individual—can benefit from the relationship. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
