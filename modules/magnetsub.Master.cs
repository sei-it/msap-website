﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using Synergy.Magnet;

namespace magnetmodule
{
    public partial class magnetsub : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getMasterTitle();

            if (HttpContext.Current.User.IsInRole("Subcontractor"))
                ltlVbar.Text = "";

            //modules
            hdmodules.RoleGroups[0].Roles = new string[1] { "Subcontractor"};
            ftmodules.RoleGroups[0].Roles = new string[17] { "Subcontractor", "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };

            //Home
            hdhome.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            fthome.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //TWG
            hdtwg.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            fttwg.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //Events
            hdevents.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //Resources
            hdresources.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            ftresources.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            //Magnet Connector
            hdmagnet.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            ftmagnet.RoleGroups[0].Roles = new string[16] { "School Staff", "Principal", "moderator", "twg", "Magnet Coordinator", "Consultant", "District Staff", "Teacher", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };

            //Prof. Reporting
            hdPR.RoleGroups[0].Roles = new string[9] { "moderator", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };
            ftPR.RoleGroups[0].Roles = new string[9] { "moderator", "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "Data", "GrantStaff" };

            if (MagnetReportPeriod.Find(x => x.isReportPeriod == true).Count() > 0)
            {
                //MAPS tag
                hdmaps.RoleGroups[0].Roles = new string[8] { "Administrators", "ED", "ProjectDirector", "Evaluator", "Project Director TWG", "EvaluatorTWG", "GrantStaff", "Data" };


                //Data
                hddata.RoleGroups[0].Roles = new string[2] { "Administrators", "Data" };
                //Admin
                hdadmin.RoleGroups[0].Roles = new string[1] { "Administrators" };
            }
            else
            {

                //MAPS
                hdmaps.RoleGroups[0].Roles = new string[4] { "Administrators", "ED", "GrantStaff", "Data" };


                //Data
                hddata.RoleGroups[0].Roles = new string[2] { "Administrators", "Data" };
                //Admin
                hdadmin.RoleGroups[0].Roles = new string[1] { "Administrators" };
            }
            if (!Page.IsPostBack)
            {
            }
        }
        protected void OnLogout(object sender, EventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx", true);
            //FormsAuthentication.RedirectToLoginPage();
        }
        protected void On1STQuarter(object sender, EventArgs e)
        {
            Session["ReportPeriodID"] = 1;
            Response.Redirect("~/admin/data/quarterreports.aspx", true);
        }

        private void getMasterTitle()
        {
            string[] dbcnnParams = ConfigurationManager.ConnectionStrings["MagnetServer"].ConnectionString.Split(';');
            string dbname = "";

            foreach (string dbtmp in dbcnnParams)
            {
                if (dbtmp.Contains("Initial Catalog"))
                {
                    dbname = dbtmp.Substring(dbtmp.IndexOf('=') + 1);
                    break;
                }

            }

            switch (dbname)
            {
                case "magnet_dev":
                    ltlTitle.Text = "DEVELOPMENT SITE";
                    break;
                case "magnet_staging":
                    ltlTitle.Text = "STAGING SITE";
                    break;
                case "magnet_staging1":
                    ltlTitle.Text = "Magnet Mirror Site";
                    break;
                case "magnet":
                    ltlTitle.Text = "";
                    break;
			case "magnet01":
                ltlTitle.Text = "Demo site";
                break;
                default:
                    ltlTitle.Text = "My DB site";
                    break;
            }
        }
    }
}