﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Explore Pedagogical Approaches to Theme Integration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li class="on"><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a>
	<ul>
	<li><a href="/modules/module2_leverage_resources.aspx">Leverage Resources to Deepen Knowledge</a></li>
	<li><a href="/modules/module2_expand_magnet_partners.aspx">Expand the Role of Magnet Partners</a></li>
	<li><a href="/modules/module2_pedagogical_approaches.aspx" class="on">Explore Pedagogical Approaches to Theme Integration</a></li>
	</ul>
</li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Explore Pedagogical Approaches to Theme Integration</h2>

<p>Theme integration will look different at each site, or even within a single site, depending on the pedagogical approaches teachers employ. While teams are engaged in curriculum mapping, they may want to discuss using various pedagogical approaches, such as</p>

<ul>
<li>Cross-disciplinary teaching of theme;</li>
<li>Team teaching;</li>
<li>Project-based learning;</li>
<li>Externships for staff or internships for students; and</li>
<li>College coursework.</li>
</ul>

<p>The <a href="/modules/downloads/module2_pedagogical_approaches.docx" class="doc" target="_blank">Pedagogical Approaches and Theme Integration Ideas (.doc)</a> tool provides reviews of these different pedagogical approaches and classroom examples that illustrate each approach. Share this tool with teachers to provide clear examples of how each pedagogical approach might apply to theme integration. Team discussions about these approaches may spark interest and spur teachers to modify their current approaches. </p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Look at theme-related connections through different lenses. If your district has schools with different themes—such as art and STEM—bring together teachers from those schools to offer operational or instructional ideas that are based on different themes but could apply to both settings.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />
















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
