﻿<%@ Page Title="MSAP Center: Increase Team Knowledge on Integration Design" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Increase Team Knowledge on Integration Design
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li class="on"><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a>
	<ul>
	<li><a href="/modules/guage_school_readiness.aspx">Gauge School Readiness for Professional Collaboration</a></li>
	<li><a href="/modules/frame_work_track_progress.aspx">Frame the Work of Teams and Track Progress</a></li>
	<li><a href="/modules/making_action_plan.aspx">Commit to Making the Action Plan a Living Document</a></li>
	<li><a href="/modules/increase_team_knowledge.aspx" class="on">Increase Team Knowledge on Integration Design</a></li>
	</ul>
</li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leading Theme Integration</em> Increase Team Knowledge on Integration Design</h2>

<p>Facilitation Team members, especially the project director, benefit from being well informed on key topics, such as integration design. This knowledge can guide other teams as they develop action plans to move the work forward in very deliberate ways. The Facilitation Team will need to do some advance work to better understand the topic of integration design, introduce terminology to faculty, and help develop the framework for theme integration. </p>

<p>To supplement existing knowledge on integration design, reference the downloadable <a href="/modules/downloads/primer_integrated_curr_design.doc" class="doc">Primer on Integrated Curriculum Design (doc)</a>.</p>

<blockquote>
<p><strong>School example</strong> For one recently created arts magnet school, clearly articulating how to integrate theme and curriculum was the first step in developing a cohesive plan. Some staff members weren’t sure how to build authentic connections between the arts and their instruction, and leaders were concerned about the time required to develop and implement a schoolwide, theme-based curriculum. After observing curricular strategies of other magnets and examining their own existing practices, the leadership team decided the staff was ready to take on shared planning and implementation. Relatively new to collaboration, the staff scaled up slowly, starting with two teachers from different content areas and developing an integrated curriculum that would eventually connect to other content areas.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Within the Facilitation Team, discuss what is being learned from the resources on curriculum integration and arrive at a shared understanding of the various types of integration design. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />





</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
