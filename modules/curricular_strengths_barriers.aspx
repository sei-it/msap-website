﻿<%@ Page Title="MSAP Center: Curricular Strengths and Barriers Protocol" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Curricular Strengths and Barriers Protocol
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li class="on"><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a>
	<ul>
	<li><a href="/modules/identify_strengths.aspx">Identify Existing Strengths</a></li>
	<li><a href="/modules/curricular_strengths_barriers.aspx" class="on">Curricular Strengths and Barriers Protocol</a></li>
	<li><a href="/modules/increase_knowledge_theme_integration.aspx">Increase Knowledge of Theme Integration</a></li>
	<li><a href="/modules/build_strengths.aspx">Build From Strengths</a></li>
	</ul>
</li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leveraging Theme-Related Practices</em> Curricular Strengths and Barriers Protocol</h2>

<p>Using the Curricular Strengths and Barriers Protocol, school faculty members (with the support of the Curriculum Connections Team) identify content areas, grade levels, and courses where the magnet theme is well integrated into the curriculum and discuss perceived barriers that get in the way of theme integration. Generating this information is critical for establishing a common understanding among the faculty.</p>

<p>Download and share with the Curriculum Connections Team the <a href="/modules/downloads/curr_strengths_barriers.doc" class="doc">Curricular Strengths and Barriers Protocol (.doc)</a> to guide faculty through 70 minutes of small-group activities. Some groups will focus on identifying grade levels or content areas in which theme-based instruction currently exists, describing what instruction looks like. Other groups will explore the grade levels or content areas where integration is challenging, uncovering the barriers that seem to prevent it. The outcome of these inquiry-based conversations will inform the Curriculum Connections Team as it focuses on developing an action plan to leverage promising practices and address any perceived barriers.</p>

<blockquote class="question">
<p><strong>Why use a protocol</strong> A protocol can provide a safe environment and guide group conversations by helping participants focus on the objective. The timed structure of a protocol may feel artificial at first, but the parameters are intended to ensure that all voices are heard and that attention remains on topic without diverging as a natural conversation would.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Use the Curricular Strengths and Barriers Protocol as a kick-off to summer professional development, reflecting on the previous year and planning ahead.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />













</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
