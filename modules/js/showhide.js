$(document).ready(function() {
 $(".parent > a").bind("click", function(e){
   e.preventDefault();
   var parent = $(this).parent();
   parent.toggleClass("open").find(".child").toggleClass("open");
   parent.parent().hasClass("accordion") ? parent.siblings(".open").removeClass("open").find('.child').removeClass("open") : '';
 });
});