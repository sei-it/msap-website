$(document).ready(function() {

   $('#showA').click(function(){
     $('div.itemA').show();
     $('div.itemB').hide();
     $('div.itemC').hide();
     $('div.itemD').hide();

    $(this).addClass("on");
	$('#showB').removeClass();
	$('#showC').removeClass();
	$('#showD').removeClass();

   });

   $('#showB').click(function(){
     $('div.itemB').show();
     $('div.itemA').hide();
     $('div.itemC').hide();
     $('div.itemD').hide();

    $(this).addClass("on");
	$('#showA').removeClass();
	$('#showC').removeClass();
	$('#showD').removeClass();

   });

   $('#showC').click(function(){
     $('div.itemC').show();
     $('div.itemA').hide();
     $('div.itemB').hide();
     $('div.itemD').hide();

    $(this).addClass("on");
	$('#showA').removeClass();
	$('#showB').removeClass();
	$('#showD').removeClass();

   });

   $('#showD').click(function(){
     $('div.itemD').show();
     $('div.itemA').hide();
     $('div.itemB').hide();
     $('div.itemC').hide();

    $(this).addClass("on");
	$('#showA').removeClass();
	$('#showB').removeClass();
	$('#showC').removeClass();

   });

 });
