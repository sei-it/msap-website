﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Reflect and Refine to Improve Practice
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li class="on"><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a>
	<ul>
	<li><a href="/modules/module2_team_progress.aspx">Check on Team Progress</a></li>
	<li><a href="/modules/module2_evolve_curriculum_mapping.aspx">Evolve Curriculum Mapping into an Ongoing Practice</a></li>
	<li><a href="/modules/module2_reflect_refine.aspx" class="on">Reflect and Refine to Improve Practice</a></li>
	<li><a href="/modules/module2_data_driven_decision.aspx">Support Data-Driven Decision Making Among Teams</a></li>
	</ul>
</li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Reflect and Refine to Improve Practice</h2>

<p>As teacher teams deepen their knowledge of the theme and hone their abilities to teach through a theme, they will begin to see their own growth. It’s a good idea to help teachers put a cycle of improvement into practice. This ongoing process leads teams to self-monitor their program’s effectiveness and progress, while building accountability to one another. </p>

<p>Teams can use the downloadable tool, <a href="/modules/downloads/module2_reflect_refine_renew.docx" class="doc" target="_blank">Reflect, Refine, and Renew: Mapping Supports New Learning (.doc)</a>, to help guide the collaborative process of examination, reflection, and refinement so that the process becomes automatic and natural. This tool helps teams discuss topics such as ensuring that the theme continues to be reflected in teaching and learning, increasing knowledge of the theme among teachers and students, and using data to examine student performance. 
</p>






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
