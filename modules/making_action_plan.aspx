﻿<%@ Page Title="MSAP Center: Commit to Making the Action Plan a Living Document" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Commit to Making the Action Plan a Living Document
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li class="on"><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a>
	<ul>
	<li><a href="/modules/guage_school_readiness.aspx">Gauge School Readiness for Professional Collaboration</a></li>
	<li><a href="/modules/frame_work_track_progress.aspx">Frame the Work of Teams and Track Progress</a></li>
	<li><a href="/modules/making_action_plan.aspx" class="on">Commit to Making the Action Plan a Living Document</a></li>
	<li><a href="/modules/increase_team_knowledge.aspx">Increase Team Knowledge on Integration Design</a></li>
	</ul>
</li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leading Theme Integration</em> Commit to Making the Action Plan a Living Document</h2>

<p>Consider specific ways to support each team’s work. As the other teams gather information and bring all of the components together for analysis, they are preparing to develop a detailed action plan for improving theme visibility, curricular theme integration, and community partnerships. Encourage teams to set both short-term (1 year) and long-term (3 years) benchmarks, reminding them that change takes time. </p>

<p>The collective action plan that emerges from the teams’ contributions will guide the magnet school toward future goals. The action plan is intended to be a living document that can be modified and changed as the program matures. The school community should review it each year, and define areas for revision and improvement for the coming year. </p>

<br clear="all" />


</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
