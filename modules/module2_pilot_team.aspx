﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Identify a Pilot Team for Mapping
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li class="on"><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a>
	<ul>
	<li><a href="/modules/module2_fit_approach.aspx">Fit the Implementation Approach to the Site</a></li>
	<li><a href="/modules/module2_pilot_team.aspx" class="on">Identify a Pilot Team for Mapping</a></li>
	<li><a href="/modules/module2_implementation_timeline.aspx">Develop an Implementation Timeline</a></li>
	</ul>
</li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Identify a Pilot Team for Mapping</h2>

<p>Start where effective activities have already opened the door to integrating the theme into instructional practice. Recruit grade-level or content-area team members whose subject areas have the strongest connections to the theme; some of them may already be using lessons and approaches that can help you jumpstart theme integration. </p>

<p>Relying on the Pilot Team’s support to develop and pilot theme integration instruments and materials, you can gradually expand your efforts and create strong knowledge and a good foundation for later work. This incremental approach accommodates both early implementers and reluctant teachers. </p>

<p>It is critical that Pilot Team members have experience in planning and working collaboratively. For pointers on how a community of practice can support and capture collaborative work, download the <a href="../doc/MSAP_Magnet_Compass_Vol3_Is1_113.pdf" class="pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', '../doc/MSAP_Magnet_Compass_Vol3_Is1_113_module2.pdf']);">January 2013 issue of The Magnet Compass (.pdf)</a>. </p>

<p>You should also engage the Facilitation Team in supporting the Pilot Team’s theme integration work, using additional staff in various advisory and coordinating capacities. Resources such as the downloadable <a href="/modules/downloads/module2_curr_strengths_barrier_protocol.docx" class="doc" target="_blank">Curricular Strengths and Barriers Protocol (.doc)</a> might be useful to the Facilitation Team for this phase of work. </p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Encourage some teachers who are enthusiastic and/or knowledgeable about the theme to join the Pilot Team even if their subject or grade level won’t be captured in the pilot maps. Capitalize on this motivation as a way to gain theme-related expertise for the team and to build buy-in for theme integration among the larger staff.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Before the Pilot Team begins work on the initial curriculum maps, ask one or more experts from the school’s partners (e.g., higher education, business, or community-based organizations) to consult with teachers to help build in rigor, relevance, and local connections from the start.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
