﻿<%@ Page Title="MSAP Center: Frame the Work of Teams and Track Progress" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Frame the Work of Teams and Track Progress
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li class="on"><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a>
	<ul>
	<li><a href="/modules/guage_school_readiness.aspx">Gauge School Readiness for Professional Collaboration</a></li>
	<li><a href="/modules/frame_work_track_progress.aspx" class="on">Frame the Work of Teams and Track Progress</a></li>
	<li><a href="/modules/making_action_plan.aspx">Commit to Making the Action Plan a Living Document</a></li>
	<li><a href="/modules/increase_team_knowledge.aspx">Increase Team Knowledge on Integration Design</a></li>
	</ul>
</li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leading Theme Integration</em> Frame the Work of Teams and Track Progress</h2>

<p>Once the teams are identified, a member of the Facilitation Team might attend the first meeting of each team to help facilitate conversations related to</p>

<ul>
<li>Group norms</li>
<li>Objectives and responsibilities</li>
<li>Team leader and secretary</li>
<li>Logistics (i.e., frequency and location of meetings, communication)</li>
<li>Progress updates to magnet project director, Facilitation Team, or faculty</li>
</ul>

<p>Share and review with teams the downloadable <a href="/modules/downloads/team_purpose_protocol.doc" class="doc">Team Purpose Protocol (.doc)</a> to help them develop a clear focus and common agreement.</p>

<p>Use the first meeting of each team as an opportunity to describe the team’s role, responsibilities, and expectations, and to talk about how it fits into the larger theme integration picture. For instance, let teams know they will need to complete and submit a one-page summary of weekly or biweekly meetings to keep the Facilitation Team informed about team activities and progress. (Download <a href="/modules/downloads/team_meeting_notetaker.doc" class="doc">Team Meeting Notetaker (.doc)</a> and <a href="/modules/downloads/team_progress_checklist.doc" class="doc">Team Progress Checklist (.doc)</a> as templates and modify, as needed.)</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>As an exercise, ask each team to identify one practice that can be replicated or easily improved. The Magnet Identity Team, for example, can develop two additional ways to publicize an upcoming theme-related event. The Curriculum Connections Team can identify a model cross-curricular project used between two teachers and have them share it with another pair of teachers who have the potential to teach the same project.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Teams and professional learning communities need support to be effective. As facilitators, the project director and school administrators will need to protect teachers’ time through either common planning time or compensation. In addition, teams should receive helpful tools or protocols that give purpose and structure to their work.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />








</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
