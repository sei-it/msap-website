﻿<%@ Page Title="MSAP Center: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Introduction
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1>
	<em>COURSE 1</em> Facilitating Magnet Theme Integration
    <a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li class="on"><a href="/modules/intro.aspx" class="on">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Introduction</h2>

<img src="/modules/imgs/photo_intro.jpg" alt="children gathered around work" class="photo" />

<!-- Left Column -->
<div class="colLeft intro">

<p>A successful magnet school requires a rigorous and coherent program built around a theme or instructional approach that is thoughtfully integrated into teaching and learning. This can happen only when teachers, school leaders, students, parents, and the community have a common understanding of the theme and vision, as well as a clear action plan for implementation.</p>

<p>In this course, five lessons offer strategies to help you facilitate four school-based theme integration teams. The framework and customizable tools can support and guide your work. Place your cursor on each button below to read more about the lessons, and click on the one you want to read first.</p>

<!-- Start -->
<div class="wherestart">

<h3>Where would you like to start? <span class="inst">HOVER TO LEARN MORE AND CLICK TO GO!</span></h3>

<ul>
<li class="lead hbox">
<a href="/modules/leading_theme_integration.aspx">Leading<br />Theme Integration</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>The Facilitation Team–a leadership team–supports the work of all the theme integration teams.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="assess hbox">
<a href="/modules/assessing_theme_visibility.aspx">Assessing Theme<br />Visibility</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>The Magnet School Identity Team assesses magnet theme visibility and branding within the school and the surrounding community.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="integ hbox">
<a href="/modules/theme_integration_teams.aspx">Creating Theme<br />Integration Teams</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>Think strategically when recruiting staff for theme integration teams–each focuses on a specific aspect of theme integration.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="lev hbox">
<a href="/modules/leveraging_theme_practices.aspx">Leveraging<br />Theme-Related Practices</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>The Curriculum Connections Team can identify existing theme-based practices to be scaled up, and start to address perceived barriers to implementation.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="conn hbox">
<a href="/modules/connecting.aspx">Connecting Theme,<br />Students, and Community</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>The Community Connections Team investigates how to strengthen current partnerships and find new opportunities to deepen and enrich teaching and learning.</p>
</div>
<!-- hoverbox end -->
</li>
</ul>

</div>
<!-- Start end -->

<br clear="all" />









</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight intro">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="modobj">COURSE <br/>OBJECTIVES</h3>

<ul>
<li>Improve magnet visibility in the community</li>
<li>Assess level of theme integration in the curriculum</li>
<li>Leverage partnerships within the community</li>
<li>Create an action plan for ongoing theme integration</li>
</ul>

</div>
</div>
<!-- mod end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose impcon">
<h3 class="showhide"><span class="ast"></span> CONSIDERATIONS</h3>
<!-- child -->
<div class="showhide-child">
<ul>
<li>Your program may have a designated magnet project director, district-level director, or other magnet program leader. In these lessons, magnet project director refers to all possible roles concerned with leading a magnet program.</li>
<li>The strategies and ideas offered here provide support for many aspects of theme integration. Lessons are presented in a suggested sequence (see the navigation panel in the left column); every school can choose its entry point according to program development and readiness. </li>
<li>The lessons are designed to build magnet staff capacity by distributing leadership and leveraging existing skills, interests, and knowledge. </li>
</ul>
</div>
<!-- child end -->
</div>
<!-- open/close end -->



</div>
<!-- Right Column end -->

















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
