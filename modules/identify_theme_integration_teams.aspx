﻿<%@ Page Title="MSAP Center: Identify Theme Integration Teams" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Identify Theme Integration Teams
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration<a href="../admin/TA_courses.aspx">
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li class="on"><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a>
	<ul>
	<li><a href="/modules/recruit_teams_strategically.aspx">Recruit Teams Strategically and Thoughtfully</a></li>
	<li><a href="/modules/identify_theme_integration_teams.aspx" class="on">Identify Theme Integration Teams</a></li>
	</ul>
</li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Creating Theme Integration Teams</em> Identify Theme Integration Teams</h2>

<p>Four separate school-level teams and one professional learning community (PLC) are suggested for supporting magnet theme integration. The following descriptions include each team’s role and suggestions for staffing. Keep in mind that membership will vary, and some teams may benefit from a specific make-up, as discussed in more detail under Staffing.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>New and revised magnet schools may find it challenging to focus on branding, curriculum, and partnerships all at once.  Gauge the readiness of the faculty at each site to decide the order in which you introduce the teams.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<p>Click to learn more about the role of and staffing for each team.</p>

<!-- Tabs Vertical -->
<div class="tabsvert nb" style="height: 340px;">

<ul class="accordion">
<li class="parent open"><a href="" class="trigger">Facilitation Team</a>
    <ul class="child open">
	<li>
	<h4>ROLE</h4>
	<p>The Facilitation Team is the first implemented. It recruits the other three teams, and serves as the hub of communication among them. Initially, this team reviews the school culture and systems to assess readiness for establishing work teams. It becomes the vision keeper of the magnet program’s Theme Integration Action Plan.</p>
	<h4>STAFFING</h4>
	<p>The magnet project director, school-level instructional leaders, and representatives of various content areas.</p>
	<p>See the lesson on <a href="/modules/leading_theme_integration.aspx">Leading Theme Integration Teams</a> for complete guidelines and tools for the Facilitation Team.</p>
	</li>
    </ul>
</li>
<li class="parent"><a href="" class="trigger">Magnet Identity Team</a>
    <ul class="child">
	<li>
	<h4>ROLE</h4>
	<p>The Magnet Identity Team strengthens the visibility of the magnet theme within the school and in the surrounding community. This team assesses program- or schoolwide understanding of the theme and uses these findings to develop an action plan for improving visibility.</p>
	<h4>STAFFING</h4>
	<p>Teachers, certificated staff, and parents, especially those who represent the wider community from which the program attracts students. Teachers from content areas not directly connected to the theme might find ways to contribute to this group.</p>
	<p>See the lesson on <a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a> for complete guidelines and tools for the Magnet Identity Team.</p>
	</li>
    </ul>
</li>
<li class="parent"><a href="" class="trigger">Curriculum Connections Team</a>
    <ul class="child">
	<li>
	<h4>ROLE</h4>
	<p>The role of the Curriculum Connections Team is to help staff recognize existing strengths in theme integration and perceived barriers to implementation before guiding the faculty through a scaling-up process. The team supports staff in learning about promising practices enacted onsite and in other programs, and develops an action plan for leveraging these practices and creating new ones.</p>
	<h4>STAFFING</h4>
	<p>One representative from each content area/department or grade that directly supports the theme. School size, grade levels, and magnet program design may influence the membership of this team. In addition to ensuring that appropriate content area teachers are included (i.e., a STEM magnet program would include math and science teachers), invite other teachers who are interested in expanding the theme.</p>
	<p>See the lesson on <a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a> for complete guidelines and tools for the Curriculum Connections Team.</p>
	</li>
    </ul>
</li>
<li class="parent"><a href="" class="trigger">Community Connections Team</a>
    <ul class="child">
	<li>
	<h4>ROLE</h4>
	<p>Based on the program’s learning goals, the Community Connections Team examines current partnerships for potential expansion and supports staff in considering new ways to leverage partnerships and create learning opportunities.</p>
	<h4>STAFFING</h4>
	<p>For the broadest perspective, include teachers from content areas directly connected to the theme, teachers from other content areas, and a school site administrator who understands district and magnet program policies.</p>
	<p>See the lesson on <a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a> for complete guidelines and tools for the Community Connections Team.</p>
	</li>
    </ul>
</li>
<li class="parent"><a href="" class="trigger">Magnet Program Professional Learning Community (PLC)</a>
    <ul class="child">
	<li>
	<h4>ROLE</h4>
	<p>A cross-school professional learning community enables ongoing communication among grantee magnet schools. The Magnet Program PLC meets regularly to track progress of other district magnet programs. Members bring questions and concerns from their staff to the PLC and return with new ideas and suggestions. </p>
	<h4>STAFFING</h4>
	<p>Representatives from various content areas. </p>
	</li>
    </ul>
</li>
</ul>

</div>
<!-- Tabs Vertical end -->

<br clear="all" />




</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
