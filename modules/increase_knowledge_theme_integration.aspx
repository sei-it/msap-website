﻿<%@ Page Title="MSAP Center: Increase Knowledge of Theme Integration" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Increase Knowledge of Theme Integration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li class="on"><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a>
	<ul>
	<li><a href="/modules/identify_strengths.aspx">Identify Existing Strengths</a></li>
	<li><a href="/modules/curricular_strengths_barriers.aspx">Curricular Strengths and Barriers Protocol</a></li>
	<li><a href="/modules/increase_knowledge_theme_integration.aspx" class="on">Increase Knowledge of Theme Integration</a></li>
	<li><a href="/modules/build_strengths.aspx">Build From Strengths</a></li>
	</ul>
</li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leveraging Theme-Related Practices</em> Increase Knowledge of Theme Integration</h2>

<p>With any new endeavor, it is important to build the knowledge of those doing the work—in this case, the Curriculum Connections Team, other teachers, administrators, and even parents. Clear examples of what is possible within a magnet program can inspire your program staff. To learn more, download and read the first four pages of this introduction to the concept of <a href="/modules/downloads/Integrated Curriculum.pdf" class="pdf" target="_blank">Integrated Curriculum (.pdf)</a>.</p>

<p>Seek out established magnet programs for ideas on innovative pedagogical approaches, cross-disciplinary projects, or enrichment programs that incorporate the magnet theme. Consult with theme-related experts to learn more about workplace expectations and skills. Here are some suggestions for getting started.</p>

<h3>School Visits</h3>

<p>As already noted, observing colleagues in the classroom—in your school or other schools—can be powerful and effective professional learning. Depending on funding, visit magnet schools in nearby districts or make trips to other states. Contact potential sites about scheduling a tour for a small cadre of staff to learn about the structure of their magnet program and the ways they integrate theme-related content.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Consider holding a “theme fair.” Along with teachers, invite nonteaching staff (bus drivers, cafeteria workers, and others) and parents. Give everyone an opportunity to see what the theme looks like when incorporated into teaching and learning. The audience will then be able to talk about the magnet school from experience.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<h4>Magnet Teachers as Guest Speakers</h4>

<p>Invite a team of magnet teachers who have implemented theme-based practices to speak with the Curriculum Connections Team, a department, or the whole faculty. If travel is a challenge, arrange a teleconference using Skype or another low-cost technology. Members from the Curriculum Connections Team might communicate with the visiting team in advance to describe the school context and to inform the visiting team’s planning and presentation.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Connecting with staff at other magnet schools can be challenging. Consider networking through a professional organization such as the National Council of Teachers of Mathematics to identify specific types of magnet programs.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<h3>Conversations With Theme-Related Professionals</h3>

<p>Use personal and professional networks to find people whose jobs connect to the school’s theme (e.g., engineer, museum curator, physician, environmentalist, parks and recreation director, musician, writer, chef) and to gather real-world, practical knowledge that may inform curriculum development. It is helpful to design a protocol to guide conversations so all team members are collecting similar information from different perspectives. </p>

<p>Do not forget to probe about “hidden” theme-related knowledge and skills that play a role in their professional practice, such as why math is important to a performing artist or healthcare worker. Consider scheduling regular talks with such experts during the theme integration process. As the magnet program matures, staff will be seeking new and different types of information.</p>

<blockquote>
<p><strong>School Example</strong> Teachers from a science magnet middle school collaborate with professors of a local state university and energy specialists at a local utilities company to develop a curriculum of authentic learning opportunities on various environmental issues. With support from the university, the middle school students have been conducting water quality tests at nearby waterways and uploading data to a national database. Through a partnership with the electric company, seventh-grade students learn to conduct energy audits of their homes, uncovering prime energy wasters and identifying ways to save energy. </p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Meet with the different teams for regular weekly updates. Coordinate opportunities for the teams to share information with one another and/or with the faculty. Work with the district’s Information Technology department to set up virtual meeting sites or electronic mailing lists to share information and track new ideas.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
