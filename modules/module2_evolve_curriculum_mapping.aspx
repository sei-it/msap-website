﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Evolve Curriculum Mapping into an Ongoing Practice
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>


<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li class="on"><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a>
	<ul>
	<li><a href="/modules/module2_team_progress.aspx">Check on Team Progress</a></li>
	<li><a href="/modules/module2_evolve_curriculum_mapping.aspx" class="on">Evolve Curriculum Mapping into an Ongoing Practice</a></li>
	<li><a href="/modules/module2_reflect_refine.aspx">Reflect and Refine to Improve Practice</a></li>
	<li><a href="/modules/module2_data_driven_decision.aspx">Support Data-Driven Decision Making Among Teams</a></li>
	</ul>
</li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Evolve Curriculum Mapping into an Ongoing Practice</h2>

<p>Teachers will feel a sense of accomplishment about their new curriculum maps, and they should. However, they also need to think of curriculum maps as flexible, living documents that require ongoing adjustments. </p>

<p>For example, curriculum maps will need to be revisited whenever there is a revision to the curriculum or site goals, or when new assessment data suggest a need for greater emphasis in a skill or content area. New developments in the district or community, or a new magnet partnership, may also need to be reflected in the maps.</p>

<p>Work with the school administrators and leaders who develop the site’s master schedule or calendar to ensure that teams have regular, ongoing common planning time. At the very least, set aside time at the beginning or end of each semester or academic year for teachers to revisit their curriculum maps.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>As staff begin to internalize the integration of theme and instruction, it’s time to think about how to describe integration to people outside the school. Coach teachers on how to talk to parents and community members about the theme so they can speak confidently about it. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />









</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
