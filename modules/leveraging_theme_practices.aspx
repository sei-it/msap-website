﻿<%@ Page Title="MSAP Center: Leveraging Theme-Related Practices" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Leveraging Theme-Related Practices
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li class="on"><a href="/modules/leveraging_theme_practices.aspx" class="on">Leveraging Theme-Related Practices</a>
	<ul>
	<li><a href="/modules/identify_strengths.aspx">Identify Existing Strengths</a></li>
	<li><a href="/modules/curricular_strengths_barriers.aspx">Curricular Strengths and Barriers Protocol</a></li>
	<li><a href="/modules/increase_knowledge_theme_integration.aspx">Increase Knowledge of Theme Integration</a></li>
	<li><a href="/modules/build_strengths.aspx">Build From Strengths</a></li>
	</ul>
</li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Leveraging Theme-Related Practices</h2>

<img src="/modules/imgs/photo_leveraging.jpg" alt="students traditional dancing with flags" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>Perhaps your programs have begun assessing familiarity with the magnet program theme in the community and working on ways to increase theme visibility in the immediate and larger school community. Branding and marketing the theme to the community is critical to growing and sustaining program enrollment, but such promotion only scratches the surface. The real challenges and rewards come when you focus on the core of the magnet program—curriculum, instruction, and learning. </p>

<p>This lesson explores how to form a new team, the Curriculum Connections Team, to guide the faculty in identifying and leveraging existing theme-based practices. The Curriculum Connections Team will engage the whole faculty in examining theme integration practices already in use, and then develop a work plan for supplementing existing practices or scaling them up for schoolwide implementation.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>When forming a Curriculum Connections Team, ensure there are teacher representatives for all grade levels and most content areas, including those disciplines that don’t seem to have an obvious connection with the magnet theme. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />




</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="obj">OBJECTIVES</h3>

<ul>
<li>Identify and build from existing strengths </li>
<li>Increase knowledge of theme integration</li>
<li>Guide staff toward a standards-based curriculum</li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="doc"><a href="/modules/downloads/team_meeting_notetaker.doc">Team Meeting Notetaker</a></li>
<li class="doc"><a href="/modules/downloads/curr_strengths_barriers.doc">Curricular Strengths and Barriers Protocol</a></li>
<li class="pdf"><a href="/modules/downloads/Integrated Curriculum.pdf" target="_blank">Integrated Curriculum</a></li>
<li class="doc"><a href="/modules/downloads/theme_integration_action_plan.doc">Theme Integration Action Plan</a></li>
</ul>

</div>
</div>
<!-- mod end -->


</div>
<!-- Right Column end -->






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
