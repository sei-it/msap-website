﻿<%@ Page Title="MSAP Center: Explore Learning Opportunities in the Community" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Explore Learning Opportunities in the Community
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li class="on"><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a>
	<ul>
	<li><a href="/modules/sustainable_partnerships.aspx">Build Sustainable Partnerships</a></li>
	<li><a href="/modules/partnership_opportunities.aspx">Explore Partnership Opportunities</a></li>
	<li><a href="/modules/learning_opportunities.aspx" class="on">Explore Learning Opportunities in the Community</a></li>
	<li><a href="/modules/volunteer_opportunities.aspx">Volunteer Opportunities</a></li>
	<li><a href="/modules/professional_development.aspx">Professional Development and Support for Teachers</a></li>
	<li><a href="/modules/advising_consulting_partnerships.aspx">Advising and Consulting Partnerships</a></li>
	<li><a href="/modules/encourage_family_support.aspx">Encourage Family Support</a></li>
	<li><a href="/modules/common_understanding.aspx">Create a Common Understanding and Vision</a></li>
	</ul>
</li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Connecting Theme, Students, and Community</em> Explore Learning Opportunities in the Community</h2>

<p>Collaborate with higher education, businesses, government agencies, and nonprofit organizations to provide student-centered learning opportunities that build connections between education and the real world.</p>

<p>To bring the local community to students, invite representatives from industry or community programs to provide new expertise and learning opportunities. Arrange a guest speaker series (e.g., women scientists or local historians) or an in-class, theme-related program (e.g., artist-in-residence) that spans the school year. To bring distant visitors to the school, use technology such as Skype, webinars, or video streaming to facilitate learning.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Don't get overwhelmed trying to implement all the learning opportunities presented here. Magnet programs need to decide which opportunities best align with their learning goals and the resources available through partner organizations.</p>

<p>Learning opportunities outside of school can provide hands-on, real-world experiences for students and staff. Internships for students, as well as summer internships for teachers, provide real-world knowledge and first-hand, authentic experience with professional careers in a particular field.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<blockquote>
<p><strong>School Example</strong> As part of a larger project, one class of high school students at an international studies magnet school participated in two 90-minute videoconferences with high school students in Iraq. Supported by a nonprofit whose mission is to deepen understandings of different cultures, this activity enabled students to exchange perspectives about the war’s impacts on their lives.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Consider having the Community Connections Team, or another appropriate group, establish a policy for determining student preparedness for out-of-school opportunities. If a student isn’t academically or socially ready for the learning tasks, the scope of an internship might have to be scaled back.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<blockquote>
<p><strong>School Example</strong> Through partnerships, one medical magnet high school offers student internships with nearby hospitals, medical schools, and research centers. Some students intern as clinical nursing assistants, while a few highly motivated students earn 6-week paid summer internships to assist researchers or physician scientists with research projects.</p>
</blockquote>

<p>Dual enrollment programs enable high school students to earn college credits, increasing the rigor of academic programs. Program designs may bring the college professor or instructor to the high school or allow students to attend courses at the college campus.</p>















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
