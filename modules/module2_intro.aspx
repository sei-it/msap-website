﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Introduction
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">
<img src="/modules/imgs/logo_print.gif" class="print" />

<h1>
	<em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
	<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li class="on"><a href="/modules/module2_intro.aspx" class="on">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Introduction</h2>

<img src="/modules/imgs/module2-photo_crit.jpg" alt="high school students conversing" class="photo" />

<!-- Left Column -->
<div class="colLeft intro">

<p>Your magnet teachers bring a wealth of content and pedagogical knowledge to the classroom, but they may need leadership and guidance to re-envision instruction through the lens of a magnet theme. As a magnet project director, you can work with support teams, such as a <span class="hbox"><a>Facilitation Team</a><span class="hoverbox">The Facilitation Team may include the project director, the school principal, school-level instructional leaders, and representatives from various content areas; it serves as the site-based hub for communication and coordination of the theme integration work.</span>
</span>, to help teachers enrich the existing, standards-based curriculum with theme-related learning opportunities. </p>

<p>Curriculum mapping will be the first step toward building an ongoing, collective teacher practice that focuses on integrating the magnet theme into a rigorous and relevant curriculum. The work you and your teachers do will help you meet the needs of your students and realize the potential of your magnet program. </p>

<p>The five lessons in this course provide activities and customizable tools to guide and support the use of curriculum mapping to integrate a magnet school’s theme. The strategies discussed here represent one possible approach to theme-based curriculum integration. Modify these procedures, tools, and instruments as needed to better serve your site’s specific needs. </p>


<!-- Start -->
<div class="wherestart">

<h3>Where would you like to start? <span class="inst">HOVER TO LEARN MORE AND CLICK TO GO!</span></h3>

<ul>
<li class="l1 hbox">
<a href="/modules/module2_groundwork_integration.aspx"><strong>Lesson 1</strong><br />Laying the Groundwork<br />for Theme Integration</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>Determine the integrated curriculum design, identify a Pilot Team, and develop an implementation timeline.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="l2 hbox">
<a href="/modules/module2_mapping_curriculum.aspx"><strong>Lesson 2</strong><br />Mapping Curriculum for<br />a Coherent Program</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>Introduce the theme integration mapping process, or expand existing curriculum maps to include magnet theme connections.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="l3 hbox">
<a href="/modules/module2_scaling_strategy.aspx"><strong>Lesson 3</strong><br />Scaling Up the Strategy</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>Scale up theme integration to other grade levels and subject areas through vertical alignment, starting with the work of the Pilot Team.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="l4 hbox">
<a href="/modules/module2_leveraging_strategies.aspx"><strong>Lesson 4</strong><br />Leveraging<br />Innovative Strategies</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>Leverage available resources, especially magnet partnerships and innovative pedagogical approaches.</p>
</div>
<!-- hoverbox end -->
</li>
<li class="l5 hbox">
<a href="/modules/module2_cycle_improvement.aspx"><strong>Lesson 5</strong><br />Instituting a Cycle of<br />Improvement</a>
<!-- hoverbox -->
<div class="hoverbox">
<p>Introduce teams to an ongoing practice of examining, reflecting on, and refining theme integration efforts.</p>
</div>
<!-- hoverbox end -->
</li>
</ul>

</div>
<!-- Start end -->

<br clear="all" />


</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight intro">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="modobj">COURSE<br />OBJECTIVES</h3>

<ul>
<li>Identify the curriculum integration design, Pilot Team, and implementation timeline</li>
<li>Guide the Pilot Team as it integrates the theme into curriculum maps</li>
<li>Scale the theme integration through vertical alignment</li>
<li>Help teams leverage partnerships and envision theme integration in the classroom</li>
<li>Institute a cyclical process of revisiting, reflecting on, and refining the work</li>
</ul>

</div>
</div>
<!-- mod end -->

<br clear="all" />




</div>
<!-- Right Column end -->

















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />


</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>. The video in Lesson 2 may be viewed online within the course or downloaded, which will require the <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a> player.</em></p>
<div class="clear">&nbsp;</div>
</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
