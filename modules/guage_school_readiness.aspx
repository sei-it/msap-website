﻿<%@ Page Title="MSAP Center: Gauge School Readiness for Professional Collaboration" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Gauge School Readiness for Professional Collaboration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li class="on"><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a>
	<ul>
	<li><a href="/modules/guage_school_readiness.aspx" class="on">Gauge School Readiness for Professional Collaboration</a></li>
	<li><a href="/modules/frame_work_track_progress.aspx">Frame the Work of Teams and Track Progress</a></li>
	<li><a href="/modules/making_action_plan.aspx">Commit to Making the Action Plan a Living Document</a></li>
	<li><a href="/modules/increase_team_knowledge.aspx">Increase Team Knowledge on Integration Design</a></li>
	</ul>
</li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leading Theme Integration</em> Gauge School Readiness for Professional Collaboration</h2>

<p>Magnet school staff members will have varying levels of understanding, experience, and enthusiasm for change, so it’s important to establish a culture of shared learning that engages everyone in the work. To support collaboration on theme integration, Facilitation Team members will work closely with staff to form and guide the collaborative teams.</p>

<p>Download the tool, <a href="/modules/downloads/gauging_school_readiness.doc" class="doc">Gauging School Readiness for Professional Collaboration (.doc)</a>, to guide discussion about the school’s current culture and practices for collaborative work. For instance, is there an existing practice of professional learning and sharing? Does the administration support such meetings among staff? Have theme-related standards and indicators been mapped to help all teachers find places where they can “hook in” to the theme?</p> 

<p>Use the information that comes from this exploration to inform how the Facilitation Team can best recruit and support the other teams.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>School site administrators and lead teachers can bring different perspectives to understanding existing school culture and practices of professional collaboration. Involve them in the process of gauging school readiness for collaboration. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />








</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
