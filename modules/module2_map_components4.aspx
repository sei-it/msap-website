﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Step 2: Identify Primary Map Components
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li class="on"><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a>
	<ul>
	<li><a href="/modules/module2_mapping_process.aspx">Step 1: Introduce the Mapping Process</a>
		<ul>
		<li><a href="/modules/module2_mapping_process_started.aspx">Get Started</a></li>
		</ul>
	</li>
	<li><a href="/modules/module2_map_components.aspx">Step 2: Identify Primary Map Components</a>
		<ul>
		<li><a href="/modules/module2_map_components2.aspx">Identify Standards and Assessments</a></li>
		<li><a href="/modules/module2_map_components3.aspx">Connect Theme to Curriculum</a></li>
		<li><a href="/modules/module2_map_components4.aspx" class="on">Develop Key Questions</a></li>
		</ul>
	</li>
	</ul>
</li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Step 2: Identify Primary Map Components</h2>

<h3>Develop Key Questions</h3>

<p>Developing key questions for a lesson, unit, course, or content area can provide a framework for understanding how the theme can connect to other subject areas. Key questions can also serve as “hooks,” or entry points, for other teams to create authentic opportunities for theme integration.</p>

<ul>
<li>Guide the team in developing key questions for each unit, subject, or grade area. Questions should be broad enough for other content areas to see opportunities for connection—for example, <em>What factors influence where we live</em>?</li>
<li>Identify connections to skills and/or content knowledge. For example, the key question <em>What factors influence where we live</em>? may offer content-area teachers connections such as these:
	<ul>
	<li>Math: Measure and graph natural resources and features, and examine their impact on the concentration of population.</li>
	<li>English Language Arts: Write a persuasive piece for a promotional campaign on the benefits of relocating to a state recently admitted to the union.</li>
	<li>Social Studies: Examine the impact of the Lewis and Clark expedition on westward expansion.</li>
	<li>Science: Explore how various forms of energy are converted to electricity, and examine how energy resources impact population concentration.</li>
	</ul>
</li>
<li>Remind the team that it is best to develop key questions after mapping the curriculum.</li>
</ul>
 

<!--<p><a href="/modules/module2_map_components3.aspx">&lt; Back: Connect Theme to Curriculum</a></p>-->






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
