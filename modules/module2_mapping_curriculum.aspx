﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Mapping Curriculum for a Coherent Program
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li class="on"><a href="/modules/module2_mapping_curriculum.aspx" class="on">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a>
	<ul>
	<li><a href="/modules/module2_mapping_process.aspx">Step 1: Introduce the Mapping Process</a>
		<ul>
		<li><a href="/modules/module2_mapping_process_started.aspx">Get Started</a></li>
		</ul>
	</li>
	<li><a href="/modules/module2_map_components.aspx">Step 2: Identify Primary Map Components</a>
		<ul>
		<li><a href="/modules/module2_map_components2.aspx">Identify Standards and Assessments</a></li>
		<li><a href="/modules/module2_map_components3.aspx">Connect Theme to Curriculum</a></li>
		<li><a href="/modules/module2_map_components4.aspx">Develop Key Questions</a></li>
		</ul>
	</li>
	</ul>
</li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Mapping Curriculum for a Coherent Program</h2>

<img src="/modules/imgs/module2-photo_adultsmeeting.jpg" alt="teachers" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>Curriculum mapping is effective for building a coherent, standards-based program. If done with an eye toward the magnet theme, curriculum mapping can be a powerful professional development activity to improve theme integration and program rigor. </p>

<p>How a magnet site begins curriculum mapping within the larger theme integration process will depend on the site’s past use of curriculum maps. Some sites may be developing curriculum maps for the first time. Other sites will have existing standards-based curriculum maps they can expand on (e.g., by integrating theme-related learning into a new project or unit, trying a different pedagogical approach, arranging externships for teachers or students, or creating a new course). </p>

<p>This lesson provides step-by-step guidance in how to help teams create or expand curriculum maps to include magnet theme connections. The lesson also suggests discussion topics for bringing structure and transparency to the process. The more clarity you can provide to teachers, the better they will be able to anticipate challenges and expectations during the process of theme integration.</p>



</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight intro">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="modobj">COURSE<br />OBJECTIVES</h3>

<ul>
<li>Introduce the mapping process</li>
<li>Identify primary map components</li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="doc"><a href="/modules/downloads/module2_introduction_mapping_process_protocol.docx" target="_blank">Introduction to the Mapping Process Protocol</a></li>
<li class="vid"><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Magnet Theme Mapping in Action</a></li>
<li class="doc"><a href="/modules/downloads/module2_sample_integrated_curriculum_maps.docx" target="_blank">Sample Integrated Curriculum Maps</a></li>
<li class="doc"><a href="/modules/downloads/module2_curriculum_map_template.docx" target="_blank">Curriculum Map Template</a></li>
</ul>

</div>
</div>
<!-- mod end -->

<br clear="all" />



</div>
<!-- Right Column end -->



</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
