﻿<%@ Page Title="MSAP Center: Build from Strengths" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Build from Strengths
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li class="on"><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a>
	<ul>
	<li><a href="/modules/identify_strengths.aspx">Identify Existing Strengths</a></li>
	<li><a href="/modules/curricular_strengths_barriers.aspx">Curricular Strengths and Barriers Protocol</a></li>
	<li><a href="/modules/increase_knowledge_theme_integration.aspx">Increase Knowledge of Theme-Integration</a></li>
	<li><a href="/modules/build_strengths.aspx" class="on">Build From Strengths</a></li>
	</ul>
</li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leveraging Theme-Related Practices</em> Build From Strengths</h2>

<p>Once the Curriculum Connections Team has identified promising theme integration practices, it is time to consider how to leverage those practices for broader implementation. Some teachers may still have difficulty seeing connections, and their perspectives may be especially valuable to the conversation as everyone considers:</p>

<ul>
<li>How could a promising practice be replicated in a different content area or grade level? </li>
<li>How could a theme-based project be expanded into other content areas for cross-disciplinary learning?</li>
</ul>

<p>At this point in the process, try to keep the conversation grounded in the possibilities of practices already in place. Later it will be time to delve into the details of curriculum and aligning new activities to standards. </p>

<p>Using the notes from the Curricular Strengths and Barriers Protocol, the team develops an action plan to build on the magnet program’s theme integration strengths and to surface any perceived barriers. Download and share with the team <a href="/modules/downloads/theme_integration_action_plan.doc" class="doc">The Theme Integration Action Plan (.doc)</a> to help them develop an action plan that includes both short- and long-term goals.</p>

<blockquote>
<p><strong>School Example</strong> Core and elective teachers at an elementary magnet school with an environmental theme collaborated to plan lessons that connected with the garden teacher’s unit on compost. The core teacher took students on a field trip to the local waste management recycling station to learn about waste diversion and the impact of waste on the environment. For a week, students calculated the percentages of recyclables, compostable food scraps, and trash produced from students’ lunches. They presented their findings to the faculty and student body. The art teacher supplemented the unit with lessons on effective poster designs, leading to a poster contest for waste diversion at the school.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>The Magnet Program Professional Learning Community (as described in the lesson on Creating Theme Integration Teams) can create routine opportunities for cross-program interactions (e.g., classroom observations, brainstorming sessions) among staff members from different programs. Encourage the team to solicit areas of need from the staff on a regular basis through staff e-mail blasts or by establishing a process for submitting requests.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<h3>Guide Staff Toward a Standards-Based Curriculum</h3>

<p>A magnet program that is working to integrate a variety of themes into a standards-based curriculum can benefit from the support of the school Facilitation Teams. These teams offer guidance and support within each school to ensure that theme integration progresses over time.</p>

<p>Each school’s Facilitation Team can oversee an important first step for expanding the theme beyond core content areas: recruit a core team of teachers with expertise in the theme’s content area (e.g., the core team of a STEM program would include teachers of science, math and technology; the core team of an arts program would include teachers of art, music, drama, humanities, technology, and possibly English). </p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Highlight the efforts of early implementers within and outside the theme content area so they serve as role models for other faculty members.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<p>This theme-based team of teachers collaborates to select theme-related topics for integration into each grade level. Teachers then map out theme-related, grade-level standards and indicators. These teachers share the map with other teachers, who review the indicators and find places to “hook in” from their content area, thus taking their first steps into theme-based instruction and learning. Over time, teachers work together toward developing a scope and sequence to ensure vertical alignment of skills. </p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>A program might choose to begin theme integration across all or some content areas, and to involve cross-disciplinary or single disciplinary instruction. In all cases, teachers need to decide on how best to connect their content area to the theme without sacrificing the standards they need to teach. This can be especially difficult when the theme’s connection to a subject area is not immediately obvious.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
