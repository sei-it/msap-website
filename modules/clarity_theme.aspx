﻿<%@ Page Title="MSAP Center: Gain Clarity on Theme Visibility and Understanding" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Gain Clarity on Theme Visibility and Understanding
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li class="on"><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a>
	<ul>
	<li><a href="/modules/assess_understanding_theme.aspx">Assess Understanding of Magnet Theme</a></li>
	<li><a href="/modules/survey.aspx">Survey Staff, Students, and Community</a></li>
	<li><a href="/modules/clarity_theme.aspx" class="on">Gain Clarity on Theme Visibility and Understanding</a></li>
	<li><a href="/modules/action_plan.aspx">Develop an Action Plan</a></li>
	<li><a href="/modules/reinforcing_theme.aspx">Ideas for Reinforcing the Magnet Theme</a></li>
	</ul>
</li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Assessing Theme Visibility</em> Gain Clarity on Theme Visibility and Understanding</h2>

<p>With the data collected, it’s time for the team to compile its information and make sense of the findings. Analyzing the information should reveal areas where the theme is strong and visible, and other areas where work needs to be done. Remember that trying to address every area and every group at once can be overwhelming. </p>

<p>An initial sorting and examination of the information may help the team prioritize its efforts and develop an action plan. Share with the Magnet Identity Team the downloadable <a href="/modules/downloads/summarizing_team_findings.doc" class="doc">Summarizing Team Findings (.doc)</a> to help them consolidate and make sense of information, and the <a href="/modules/downloads/team_meeting_notetaker.doc" class="doc">Team Meeting Notetaker (.doc)</a> to help focus team efforts.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Support the team’s effort to work toward a comprehensive action plan by helping it focus on designing components that are targeted to creating a common vision and understanding of the magnet’s theme.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />









</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
