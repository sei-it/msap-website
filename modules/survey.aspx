﻿<%@ Page Title="MSAP Center: Survey Staff, Students, and Community" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Survey Staff, Students, and Community
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li class="on"><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a>
	<ul>
	<li><a href="/modules/assess_understanding_theme.aspx">Assess Understanding of Magnet Theme</a></li>
	<li><a href="/modules/survey.aspx" class="on">Survey Staff, Students, and Community</a></li>
	<li><a href="/modules/clarity_theme.aspx">Gain Clarity on Theme Visibility and Understanding</a></li>
	<li><a href="/modules/action_plan.aspx">Develop an Action Plan</a></li>
	<li><a href="/modules/reinforcing_theme.aspx">Ideas for Reinforcing the Magnet Theme</a></li>
	</ul>
</li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Assessing Theme Visibility</em> Survey Staff, Students, and Community</h2>

<p>Surveys are valuable tools for starting to understand the different perspectives of a large and diverse group of people. When questions are carefully written, they can target both a particular audience and particular kinds of information. Surveys can take many forms, and it’s good to think about what the school is willing to invest so the results provide as much information as possible.</p>

<p>As the Magnet Identity Team designs surveys for various audiences to assess knowledge and visibility, download and share the <a href="/modules/downloads/sample_survey_questions.doc" class="doc">Sample Survey Questions (.doc)</a> for suggested examples.</p>

<h4>Get Great Response Rates</h4>

<p>The following tips may encourage people to respond to the school surveys. </p>

<ul>
<li><strong>Thank survey participants.</strong> Everyone likes to know that their opinions have value.</li>
<li><strong>Make surveys short.</strong> Do not include questions on anything but the main topic. </li>
<li><strong>Ensure anonymity.</strong> People will be more willing to respond honestly if they know their names are not attached to their answers.</li>
<li><strong>Use primarily closed-ended questions.</strong> When responses are “yes” and “no” or multiple choice, the survey is quick to take. This makes it likely to be completed (and makes it easy to analyze). </li>
<li><strong>Offer optional open-ended questions.</strong> Valuable and often unexpected information can be collected when respondents have the opportunity to volunteer comments in their own words. </li>
<li><strong>Make surveys easy to take and return.</strong> Attach the parent survey to something that is already being sent home (principal’s newsletter or homework packet) or use a free web-based survey service (such as Survey Monkey) and include the link on the school’s website or in e-mails to parents and partners.</li>
</ul>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Translate materials so that parents and guardians who have limited English skills can participate. Enlist the support of staff, parents, or a parent liaison to explore the best ways to reach all parents.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<h4>Build Fun Survey Activities </h4>

<p>Consider planning something fun to go with the surveys to encourage participation. Click on each role group to see an idea that might make taking the survey a treat, rather than a chore. </p>

<!-- Tabs -->
<div class="tabs group">
<ul>
<li><a id="showA" class="on">Staff</a></li>
<li><a id="showB">Parents</a></li>
<li><a id="showC">Students</a></li>
<li><a id="showD">Community</a></li>
</ul>
</div>
<!-- Tabs end -->

<!-- Tabs: Staff -->
<div class="itemA">
<h4>Staff</h4>
<img src="/modules/imgs/icon_brushes.png" alt="paint brushes" />
<p>Arrange donations of food or provide classroom supplies in the faculty room in exchange for completing a survey.</p>
</div>
<!-- Tabs: Staff end -->

<!-- Tabs: Parents -->
<div class="itemB">
<h4>Parents</h4>
<img src="/modules/imgs/icon_coffee.png" alt="steaming cup of coffee" />
<p>Set up a morning coffee station at the school’s entrance and offer a cup of coffee in exchange for filling out a brief survey or answering a few questions.</p>
</div>
<!-- Tabs: Parents end -->

<!-- Tabs: Student -->
<div class="itemC">
<h4>Students</h4>
<img src="/modules/imgs/icon_happyface.png" alt="happy face" />
<p>Set up a table in a central location where students can get a school pencil, sticker, or treat for filling out the survey. (Read surveys to younger students.)</p>
</div>
<!-- Tabs: Student end -->

<!-- Tabs: Community -->
<div class="itemD">
<h4>Community</h4>
<img src="/modules/imgs/icon_family.png" alt="family" />
<p>Community Teams of adults and students might collect survey responses outside of a grocery store, public library, church, or café. Teams can wear attention-getting school clothing or carry signs; consider using cheerleading pompoms in the school colors.</p>
</div>
<!-- Tabs: Community end -->

<br clear="all" />





</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
