﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Develop Overarching Questions
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li class="on"><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a>
	<ul>
	<li><a href="/modules/module2_sharing_environment.aspx">Create a Sharing Environment</a></li>
	<li><a href="/modules/module2_lead_teams_curriculum_mapping.aspx">Lead New Teams Through Curriculum Mapping</a></li>
	<li><a href="/modules/module2_support_vertical_alignment.aspx">Support Vertical Alignment</a></li>
	<li><a href="/modules/module2_develop_overarching_questions.aspx" class="on">Develop Overarching Questions</a></li>
	</ul>
</li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Develop Overarching Questions</h2>

<p>When your site has “finished” curriculum maps to work from, consider developing overarching questions to unify the curriculum at each grade level or schoolwide. Overarching questions differ from the key questions described in the lesson on Mapping Curriculum for a Coherent Program: Key questions provide focus within a course, grade level, or content area, and overarching questions are broad enough to engage all content areas and grade levels and may apply to a few weeks or a whole marking period.</p>

<p>Schools interested in developing overarching questions might base them on “habits of mind” practices that cross all disciplines and exemplify ideal student behavior—for example, “How does challenging yourself contribute to personal growth as a student?” or “How does listening to multiple perspectives affect your own ideas?” </p>

<p>A school that offers cross-curricular instruction might find theme-based overarching questions useful in connecting the different content areas under a single focus. If you decide that developing overarching questions is part of your goal, this step should come after all teams are comfortable with their curriculum maps and how the maps align with one another.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>If a school chooses to develop overarching questions, teams should be prepared for a process that requires time and discussion.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Direct teams to district or school frameworks that guide the larger vision as another resource for developing overarching questions.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />










</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
