﻿<%@ Page Title="MSAP Center: Identify Existing Strengths" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Identify Existing Strengths
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li class="on"><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a>
	<ul>
	<li><a href="/modules/identify_strengths.aspx" class="on">Identify Existing Strengths</a></li>
	<li><a href="/modules/curricular_strengths_barriers.aspx">Curricular Strengths and Barriers Protocol</a></li>
	<li><a href="/modules/increase_knowledge_theme_integration.aspx">Increase Knowledge of Theme Integration</a></li>
	<li><a href="/modules/build_strengths.aspx">Build From Strengths</a></li>
	</ul>
</li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Leveraging Theme-Related Practices</em> Identify Existing Strengths</h2>

<p>Chances are, there are already promising theme-integration practices in use within your magnet program. It is time to bring them to light and recognize where they are already folded into particular grade-level and content-area curricula and instruction. At the same time, teachers who have struggled with incorporating the theme into instruction can share valuable information about perceived barriers to integration. </p>

<p>Here are two approaches for the Curriculum Connections Team to use in taking stock of existing promising practices and surfacing barriers to integration. The <a href="/modules/downloads/team_meeting_notetaker.doc" class="doc">Team Meeting Notetaker (.doc)</a> can help the team focus its efforts.</p>

<blockquote>
<p><strong>School Example</strong> School staff and leaders of a STEM high school considered their curriculum innovative and rigorous; however, students continued to struggle with expository literacy tasks, specifically reading comprehension and writing. After conducting surveys and interviews with staff regarding current practices and perceived barriers, it became evident that the English department was working separately from the STEM team and struggling with theme integration. As one English teacher pointed out, “The E in STEM does not stand for English.” In a series of conversations and meetings, the school explored the benefits of cross-content collaborations, such as having English language arts teachers address expository reading and writing strategies using STEM-based content, while STEM teachers reinforced these skills in their classes.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Encourage the School Identity Team to provide faculty with an update on the progress made toward branding the magnet program theme and the work that still remains.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<h3>Classroom Observation</h3>

<p>Observing a promising theme integration practice is a powerful and effective activity for professional learning. However, in a school climate that does not value teacher collaboration and transparency, opening one’s classroom to an observer&#8211;even a colleague&#8211;may feel uncomfortable. Because the outcomes are so beneficial, it’s worth considering the following strategies to make this activity possible:</p>

<ul>
<li>Ask for volunteers willing to invite observers into their classroom. The Curriculum Connections Team might clarify that they are looking for promising practices, as teachers proud of their practice are often more willing to share.</li>
<li>Alternatively, ask for permission to videotape promising practices so other staff can observe.</li>
<li>Have members of the Curriculum Connections Team offer to let staff members observe them.</li>
<li>If specific grade levels or content areas show strong theme integration, approach these teachers to ask if they would be open to sharing their practice with their colleagues through classroom observations.</li>
<li>Make it clear that the observation is not intended to be evaluative. The objectives are to share good practices with and learn from colleagues. Remind faculty that classroom observation is a valuable learning experience for considering how to replicate or support the practices in other content areas or grade levels.</li>
</ul>

<br clear="all" />
<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>While classroom observations are valuable for learning about promising instructional practices, some faculty may find them uncomfortable. Speaking to staff can help determine if this approach might be acceptable within the school culture.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />







</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
