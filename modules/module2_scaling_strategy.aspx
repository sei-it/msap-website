﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Scaling the Strategy
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li class="on"><a href="/modules/module2_scaling_strategy.aspx" class="on">Lesson 3:<br />Scaling the Strategy</a>
	<ul>
	<li><a href="/modules/module2_sharing_environment.aspx">Create a Sharing Environment</a></li>
	<li><a href="/modules/module2_lead_teams_curriculum_mapping.aspx">Lead New Teams Through Curriculum Mapping</a></li>
	<li><a href="/modules/module2_support_vertical_alignment.aspx">Support Vertical Alignment</a></li>
	<li><a href="/modules/module2_develop_overarching_questions.aspx">Develop Overarching Questions</a></li>
	</ul>
</li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Scaling the Strategy</h2>

<img src="/modules/imgs/module2-photo_kidslooking.jpg" alt="high school students looking" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>Once the Pilot Team has prepared a curriculum map, it’s time to support other teams through the process. Scaling up and aligning efforts across other grades and/or content areas creates a learning progression and strengthens teacher communication and collaboration, all of which can improve instruction and student achievement.</p>

<p>This lesson focuses on designing, strengthening, and extending magnet theme connections across content areas and grade levels. The activities in this lesson create opportunities for the Pilot Team to share its curriculum maps with colleagues, thereby initiating a new cycle of curriculum mapping among staff. Expanding the number of teachers who are engaged in the curriculum mapping process will move your program forward in its effort to establish a coherent, theme-focused, integrated curriculum.</p>



</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight intro">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="modobj">COURSE<br />OBJECTIVES</h3>

<ul>
<li>Create a sharing environment for curriculum mapping</li>
<li>Lead new teams through curriculum mapping</li>
<li>Support vertical alignment through cross-grade collaboration</li>
<li>Develop overarching questions</li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="doc"><a href="/modules/downloads/module2_sharing_mapping_process_protocol.docx" target="_blank">Sharing the Mapping Process Protocol</a></li>
<li class="doc"><a href="/modules/downloads/module2_sample_integrated_curriculum_maps.docx" target="_blank">Sample Integrated Curriculum Maps </a></li>
<li class="doc"><a href="/modules/downloads/module2_curriculum_map_template.docx" target="_blank">Curriculum Map Template </a></li>
</ul>

</div>
</div>
<!-- mod end -->

<br clear="all" />



</div>
<!-- Right Column end -->



</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
