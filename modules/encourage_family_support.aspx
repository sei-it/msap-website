﻿<%@ Page Title="MSAP Center: Engage Parent and Family Support" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Engage Parent and Family Support
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li class="on"><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a>
	<ul>
	<li><a href="/modules/sustainable_partnerships.aspx">Build Sustainable Partnerships</a></li>
	<li><a href="/modules/partnership_opportunities.aspx">Explore Partnership Opportunities</a></li>
	<li><a href="/modules/learning_opportunities.aspx">Explore Learning Opportunities in the Community</a></li>
	<li><a href="/modules/volunteer_opportunities.aspx">Volunteer Opportunities</a></li>
	<li><a href="/modules/professional_development.aspx">Professional Development and Support for Teachers</a></li>
	<li><a href="/modules/advising_consulting_partnerships.aspx">Advising and Consulting Partnerships</a></li>
	<li><a href="/modules/encourage_family_support.aspx" class="on">Encourage Family Support</a></li>
	<li><a href="/modules/common_understanding.aspx">Create a Common Understanding and Vision</a></li>
	</ul>
</li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Connecting Theme, Students, and Community</em> Encourage Family Support</h2>

<p>Magnet programs attract students from diverse neighborhoods within a school district or consortium, so establishing a community within the school is very important. Create events and roles for parent involvement, and solicit members’ perspectives on how their skills and knowledge can support students and enrich the program.</p>

<p>When involving parents in teaching and learning at the school, consider these approaches for engagement:</p>

<ul>
<li>Consider language and culture when communicating with parents. A parent liaison can shed light on the best ways to connect. </li>
<li>Ask parents how they would like to contribute to teaching and learning. Parents can supplement classroom learning by offering expertise in a subject area or teaching a particular skill.</li>
<li>Offer parent-focused courses and workshops to support student learning at home. Topics may include math refresher classes to help parents support students with homework, and training for parents on how to access online school and student resources.</li>
</ul>

<blockquote>
<p><strong>School Example</strong> One school offers direct services to parents and has seen parent volunteer activity increase among these same parents. Every month, the school conducts at least two family nights that include a variety of activities: training on using computers that the school provides to students, providing information about afterschool activities, or offering engineering lessons delivered by students. Parents can also learn English in adult classes offered through the afterschool program or district services.</p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>When parents (and community members) come to the school to instruct or volunteer, they appreciate knowing about school expectations and policies beforehand. The Community Connections Team might consider developing a standard e-mail that provides this information and includes guidelines for appropriate topics to discuss with students.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />


















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
