﻿<%@ Page Title="MSAP Center: Ideas for Reinforcing the Magnet Theme" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Ideas for Reinforcing the Magnet Theme
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li class="on"><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a>
	<ul>
	<li><a href="/modules/assess_understanding_theme.aspx">Assess Understanding of Magnet Theme</a></li>
	<li><a href="/modules/survey.aspx">Survey Staff, Students, and Community</a></li>
	<li><a href="/modules/clarity_theme.aspx">Gain Clarity on Theme Visibility and Understanding</a></li>
	<li><a href="/modules/action_plan.aspx">Develop an Action Plan</a></li>
	<li><a href="/modules/reinforcing_theme.aspx" class="on">Ideas for Reinforcing the Magnet Theme</a></li>
	</ul>
</li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Assessing Theme Visibility</em> Ideas for Reinforcing the Magnet Theme</h2>

<p>As the Magnet Identity Team develops strategies for how to improve theme visibility, it should be inclusive of the different neighborhoods in which students live and be prepared to modify strategies, if needed.</p>

<p>As discussed in the May 2011 issue of <a href="http://www.msapcenter.com/doc/MSAP_Magnet_Compass_Vol1_Is4_0511.pdf" target="_blank" class="pdf" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is4_0511_reinforcing_theme.pdf']);"><em>The Magnet Compass</em> (.pdf)</a> newsletter, ideas for branding magnet programs include: </p>

<ul>
<li>Make a video for TV and movie theaters.</li>
<li>Make a brochure.</li>
<li>Create a “welcome to the neighborhood” box with a magnet, flyer, and invitation to visit the magnet school’s website.</li>
<li>Mail postcards.</li>
<li>Post door hangers. </li>
</ul>

<blockquote>
<p><strong>School example</strong> Teachers at a middle school with a citizen-centered focus wanted to publicize the innovative ways students demonstrated knowledge and skills. They videotaped a mock courtroom session and sent copies to several local news agencies. In a setting complete with a judge’s bench, witness stand, jury box, counsel table, and seating for a courtroom audience, student representatives debated a contentious local issue: should local land be developed for a professional sports arena or a family housing development? The procedure showcased students’ strengths in tackling complex issues related to the school’s focus on public debate, business, and law. </p>
</blockquote>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>The Magnet Program team might collaborate to launch a campaign promoting all district magnet programs, emphasizing innovation and choice.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Host a schoolwide contest for student teams to submit a phrase and/or image that best represents their school’s theme.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />







</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
