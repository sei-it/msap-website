﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Support Vertical Alignment
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li class="on"><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a>
	<ul>
	<li><a href="/modules/module2_sharing_environment.aspx">Create a Sharing Environment</a></li>
	<li><a href="/modules/module2_lead_teams_curriculum_mapping.aspx">Lead New Teams Through Curriculum Mapping</a></li>
	<li><a href="/modules/module2_support_vertical_alignment.aspx" class="on">Support Vertical Alignment</a></li>
	<li><a href="/modules/module2_develop_overarching_questions.aspx">Develop Overarching Questions</a></li>
	</ul>
</li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Support Vertical Alignment</h2>

<p>When teams are ready for cross-grade collaboration, bring together a few representative teachers from each grade, or, depending on department size, the whole team. Cross-grade discussions can take several paths, from focusing on scaffolding early-grade skills to extending or developing activities or magnet theme connections, and then creating overarching questions that encompass the focus of an entire grade level, content area, or magnet site. </p>

<p>Through these cross-grade conversations, teams will recognize gaps as well as good examples of curriculum alignment and scaffolding between successive grade levels. Participating teams will collaborate to identify ways to improve theme integration and generate new ideas for one another. As teachers work through vertical alignment, it’s possible that they will identify a need to develop an entirely new course or curriculum.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Analyzing the curriculum across grade levels or content areas can quickly become overwhelming. Suggest that teams start with looking closely at one category of the curriculum map, such as Skills or Theme Connections.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />









</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
