﻿<%@ Page Title="MSAP Center: Explore Partnership Opportunities" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Explore Partnership Opportunities
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li class="on"><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a>
	<ul>
	<li><a href="/modules/sustainable_partnerships.aspx">Build Sustainable Partnerships</a></li>
	<li><a href="/modules/partnership_opportunities.aspx" class="on">Explore Partnership Opportunities</a></li>
	<li><a href="/modules/learning_opportunities.aspx">Explore Learning Opportunities in the Community</a></li>
	<li><a href="/modules/volunteer_opportunities.aspx">Volunteer Opportunities</a></li>
	<li><a href="/modules/professional_development.aspx">Professional Development and Support for Teachers</a></li>
	<li><a href="/modules/advising_consulting_partnerships.aspx">Advising and Consulting Partnerships</a></li>
	<li><a href="/modules/encourage_family_support.aspx">Encourage Family Support</a></li>
	<li><a href="/modules/common_understanding.aspx">Create a Common Understanding and Vision</a></li>
	</ul>
</li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Connecting Theme, Students, and Community</em> Explore Partnership Opportunities</h2>

<blockquote>
<p><strong>SCHOOL EXAMPLE</strong> At one international foreign languages academy, about 500 K-6 magnet students become bi-literate, bilingual, and culturally aware in French, German, Italian, or Spanish and English in a research-based, 90-10 dual immersion program. Students who begin in kindergarten will achieve academic and social proficiency in two or more languages by the end of sixth grade. The magnet school’s collaborations with the French, German, and Italian governments help to make these opportunities possible. </p>
</blockquote>

<p>Partnership opportunities vary depending on the type of organization. Explore some possibilities by clicking on the types of organizations below.</p>

<br clear="all" />

<!-- Tabs Vertical -->
<div class="tabsvert">

<h3 id="org">ORGANIZATION</h3>
<h3 id="part">PARTNERSHIP OPPORTUNITY</h3>

<ul class="accordion">
<li class="parent open org"><a href="" class="trigger">Private Organization or Business</a>
    <ul class="child open part">
	<li>Professional Staff Development</li>
	<li>Theme Consultants and Advisors</li>
	<li>Guest Speakers and Programs</li>
	<li>Internships</li>
	<li>Sponsorships via Funds and Resources</li>
    </ul>
</li>
<li class="parent org"><a href="" class="trigger">Colleges and Universities</a>
    <ul class="child part">
	<li>Professional Staff Development</li>
	<li>Dual Enrollment Courses</li>
	<li>Student Teachers</li>
	<li>Afterschool Programs</li>
    </ul>
</li>
<li class="parent org"><a href="" class="trigger">Public Organizations</a>
    <ul class="child part">
	<li>Internships</li>
	<li>Theme Consultants</li>
	<li>Guest Speakers and Programs</li>
	<li>Theme-based Instruction </li>
	<li>Sponsorships via Funds and Resources</li>
    </ul>
</li>
<li class="parent org"><a href="" class="trigger">Community-based Organizations</a>
    <ul class="child part">
	<li>Afterschool Programs</li>
	<li>Tutoring and Mentoring</li>
	<li>Theme Consultants</li>
    </ul>
</li>
<li class="parent org"><a href="" class="trigger">Parents</a>
    <ul class="child part">
	<li>Guest Speakers </li>
	<li>Fundraising and Outreach Support</li>
	<li>Tutoring and Mentoring</li>
	<li>Classroom Assistance</li>
    </ul>
</li>
</ul>

</div>
<!-- Tabs Vertical end -->

<br clear="all" />

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>A community partnership can offer a magnet program exciting possibilities, but offerings that appear attractive might not support the program’s student learning goals or curriculum standards. Be sure to remind the Community Connections Team to maintain their focus on supporting a rigorous academic program as its members explore the possibilities for student learning opportunities. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
