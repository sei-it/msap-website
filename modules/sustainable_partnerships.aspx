﻿<%@ Page Title="MSAP Center: Build Sustainable Partnerships" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
   MSAP Center: Build Sustainable Partnerships </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
  <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li class="on"><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a>
	<ul>
	<li><a href="/modules/sustainable_partnerships.aspx" class="on">Build Sustainable Partnerships</a></li>
	<li><a href="/modules/partnership_opportunities.aspx">Explore Partnership Opportunities</a></li>
	<li><a href="/modules/learning_opportunities.aspx">Explore Learning Opportunities in the Community</a></li>
	<li><a href="/modules/volunteer_opportunities.aspx">Volunteer Opportunities</a></li>
	<li><a href="/modules/professional_development.aspx">Professional Development and Support for Teachers</a></li>
	<li><a href="/modules/advising_consulting_partnerships.aspx">Advising and Consulting Partnerships</a></li>
	<li><a href="/modules/encourage_family_support.aspx">Encourage Family Support</a></li>
	<li><a href="/modules/common_understanding.aspx">Create a Common Understanding and Vision</a></li>
	</ul>
</li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Connecting Theme, Students, and Community</em> Build Sustainable Partnerships</h2>

<p>As with any relationship, partnerships between magnet programs and community organizations or individuals will go through a process of establishing needs, common goals, and mutually beneficial ways of working together. A detailed description of this three-stage partnership process is discussed in the April 2012 issue of <a href="/modules/downloads/magnet_compass_vol2_issue2.pdf" target="_blank" class="pdf">The Magnet Compass (.pdf)</a>.</p>

<p>Community partnerships can play crucial roles in sustaining a magnet program—providing high-quality experiences for students, offering content expertise and resources, or connecting a program to an industry—so it is important to approach them thoughtfully. As the Community Connections Team begins its work with partnerships, share the downloadable <a href="/modules/downloads/team_meeting_notetaker.doc" class="doc">Team Meeting Notetaker (.doc)</a> to help team members organize their work.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Encourage members of the Community Connections Team to read and discuss the April 2012 issue of The Magnet Compass for specific guidelines on establishing and expanding partnerships to sustain a magnet program. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<h3>Use Community to Increase Capacity</h3>

<p>The Community Connections Team should identify and assess the school’s current partnerships and consider ways to capitalize on them to increase learning opportunities:</p>

<ol>
<li>Build a list of the school’s theme-based partners and indicate the resources they bring to the program.</li>
<li>Review student learning goals and identify areas where a partnership could provide new resources to strengthen the curriculum. </li>
<li>Consider potential ways to broaden current partnership or create multiple partnerships to meet complex needs. </li>
</ol>

<p>Leverage the strengths that organizations or individuals can offer, but do not expect that they can meet all program needs.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Determine a set schedule for meeting with community partners to ensure that both parties’ expectations are being met and to make adjustments if necessary. At the beginning of a partnership, monthly meetings might be appropriate; later, quarterly or annual meetings might suffice.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<br clear="all" />

<!-- open/close -->
<!--<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>-->
<!-- child -->
<!--<div class="showhide-child">
<p>See <a href="/modules/resources.aspx">Resources</a> for samples of agreements to use between parents, students, school, and community partners.</p>
</div>-->
<!-- child end -->
<!--</div>-->
<!-- open/close end -->

<!--<br clear="all" />
-->















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
