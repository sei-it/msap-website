﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Support Data-Driven Decision Making Among Teams
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a></h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li class="on"><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a>
	<ul>
	<li><a href="/modules/module2_team_progress.aspx">Check on Team Progress</a></li>
	<li><a href="/modules/module2_evolve_curriculum_mapping.aspx">Evolve Curriculum Mapping into an Ongoing Practice</a></li>
	<li><a href="/modules/module2_reflect_refine.aspx">Reflect and Refine to Improve Practice</a></li>
	<li><a href="/modules/module2_data_driven_decision.aspx" class="on">Support Data-Driven Decision Making Among Teams</a></li>
	</ul>
</li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Support Data-Driven Decision Making Among Teams</h2>

<p>Most likely, data-driven decision making is already part of magnet teachers’ practice. However, it helps to remind teams that a need for change doesn’t necessarily mean that something is wrong. Changes in the data may reflect shifts in circumstances. </p>

<p>Help your teachers access and/or collect both qualitative and quantitative data so they can apply an evidence-based approach to understanding progress and identifying areas for improvement. The <a href="/modules/downloads/module2_reflect_refine_renew.docx" class="doc" target="_blank">Reflect, Refine, and Renew: Mapping Supports New Learning (.doc)</a> tool reinforces best practice, as teams use data from their classrooms, school, and district to identify changes they want to make.</p>

<!-- promo -->
<div class="promo">

<span class="thumb"><a href="../doc/MSAP_Magnet_Compass_Vol2_Is4_1012.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', '../doc/MSAP_Magnet_Compass_Vol2_Is4_1012_module2.pdf']);"><img src="/modules/imgs/module2-promo_magnet_conn.png" alt="Magnet Connector, October 2012" /></a></span>

<p>For an introduction to effective use of data, download the October 2012 issue of The Magnet Compass.</p>

<p class="dl"><a href="../doc/MSAP_Magnet_Compass_Vol2_Is4_1012.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', '../doc/MSAP_Magnet_Compass_Vol2_Is4_1012_module2.pdf']);">The Magnet Compass, October 2012</a></p>

</div>
<!-- video end -->





</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
