﻿<%@ Page Title="MSAP Center: Creating Theme Integration Teams" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Creating Theme Integration Teams
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li class="on"><a href="/modules/theme_integration_teams.aspx" class="on">Creating Theme Integration Teams</a>
	<ul>
	<li><a href="/modules/recruit_teams_strategically.aspx">Recruit Teams Strategically and Thoughtfully</a></li>
	<li><a href="/modules/identify_theme_integration_teams.aspx">Identify Theme Integration Teams</a></li>
	</ul>
</li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Creating Theme Integration Teams</h2>

<img src="/modules/imgs/photo_creating.jpg" alt="students collaborating" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>A magnet program’s success and sustainability relies on the collective efforts of staff, who should be involved in every step of the decision-making process. When teachers and administrators collaborate on key magnet theme integration issues such as branding, curriculum development, and partnerships, they share the learning load and become invested and empowered in developing a thriving, dynamic magnet program.</p>

<p>Teams offer magnet program staff opportunities to build shared visions and common understandings of their specific themes as they engage in job-embedded learning. This lesson offers facilitation guidelines as you prepare staff for effective team collaboration and introduces four school-level teams that can work toward theme integration at each magnet site. </p>

<p>Each team focuses on a specific aspect of theme integration and, for its component, is expected to assess program strengths and needs; develop an action plan; and share learnings with the other teams, the school community, and members of the magnet program learning community. </p>


</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="obj">OBJECTIVES</h3>

<ul>
<li>Recruit teams strategically and thoughtfully</li>
<li>Identify theme integration teams</li>
</ul>

<!--

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<p>Aliquam mollis, neque ut ullamcorper tempor, dolor tortor varius nisi, id euismod est risus ac neque. Fusce cursus mattis leo. </p>

<ul>
<li class="fileDoc"><a href="">Document Name Which May Wrap by Golly</a></li>
<li class="fileDoc"><a href="">Document Name</a></li>
</ul>
-->

</div>
</div>
<!-- mod end -->


</div>
<!-- Right Column end -->






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
