﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Step 1: Introduce the Mapping Process
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li class="on"><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a>
	<ul>
	<li><a href="/modules/module2_mapping_process.aspx">Step 1: Introduce the Mapping Process</a>
		<ul>
		<li><a href="/modules/module2_mapping_process_started.aspx" class="on">Get Started</a></li>
		</ul>
	</li>
	<li><a href="/modules/module2_map_components.aspx">Step 2: Identify Primary Map Components</a>
		<ul>
		<li><a href="/modules/module2_map_components2.aspx">Identify Standards and Assessments</a></li>
		<li><a href="/modules/module2_map_components3.aspx">Connect Theme to Curriculum</a></li>
		<li><a href="/modules/module2_map_components4.aspx">Develop Key Questions</a></li>
		</ul>
	</li>
	</ul>
</li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Step 1: Introduce the Mapping Process</h2>

<h3>Get Started</h3>

<ul>
<li>Review the <a href="/modules/downloads/module2_sample_integrated_curriculum_maps.docx" class="doc" target="_blank">Sample Integrated Curriculum Map (.doc)</a>, focusing on one content area initially. Encourage discussion of connections that team members see to the theme at their site.</li>
<li>Suggest that team members determine how they will work together before they begin. Teachers may choose to work collaboratively on mapping across subjects. For example, elementary school grade-level teams, especially, may find that a jigsaw approach can be effective for distributing the work. Using this approach, each teacher maps the curriculum of one subject, and then all of the teachers merge their maps to create the grade-level map.</li>
<li>Give the team permission to begin building its curriculum map, beginning anywhere on the modifiable <a href="/modules/downloads/module2_curriculum_map_template.docx" class="doc" target="_blank">Curriculum Map Template (.doc)</a>. While a completed map has a linear flow, the process of building the map is a cyclical one. </li>
</ul>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>Curriculum maps are excellent professional development tools to share with teachers who are new to the school or veteran teachers who are moving to a new grade level.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />







</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
