﻿<%@ Page Title="MSAP: Mod2 Sample Subpage" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Mod2 Sample Subpage
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="">Introduction</a></li>
<li><a href="">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li class="on"><a href="">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a>
	<ul>
	<li><a href="/modules/mod2_sub.aspx" class="on">Sample Subpage</a></li>
	</ul>
</li>
<li><a href="">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="">Lesson 4:<br />Strategizing for Innovative Theme Integration</a></li>
<li><a href="">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Assessing Theme Visibility</em> Sample Subpage (the works!)</h2>

<p>Vestibulum ante ipsum primis <a href="" class="dl" target="_blank">faucibus (.doc)</a> orci luctus et ultrices posuere cubilia Curae; Duis nec ante. Duis convallis rutrum mauris. Pellentesque et risus. Proin feugiat.Nullam ullamcorper urna egestas lorem. Donec accumsan ligula vitae magna. Sed quis <span class="hbox"><a>Facilitation Team</a><span class="hoverbox">The Facilitation Team may include the project director, the school principal, school-level instructional leaders, and representatives from various content areas; it serves as the site-based hub for communication and coordination of the theme integration work.</span></span> vitae lectus vehicula tincidunt. Quisque ac mauris eget mauris egestas viverra. Integer felis arcu, condimentum at, pharetra a, sagittis non, ligula. Nam ac lacus. Sed at velit. Integer vel dui ac nisi scelerisque malesuada. </p>

<h3>Subhead Three</h3>

<p>Mauris felis nibh, placerat nec, feugiat vel, vestibulum interdum, augue. Aenean sollicitudin, eros quis cursus feugiat, lacus diam tempor tortor, vel posuere odio nulla vel nulla.Pellentesque ipsum risus, auctor vel, volutpat vitae, ultricies sed, tellus. </p>

<p class="download"><a href="" target="_blank">Title of Document to Download</a> <span class="type">(.pdf)</span></p>

<br clear="all" />
<br clear="all" />

<h4>SUBHEAD FOUR</h4>

<p>Mauris felis nibh, placerat nec, feugiat vel, vestibulum interdum, augue. Aenean sollicitudin, eros quis cursus feugiat, lacus diam tempor tor. </p>

<p><strong>Develop an Action Plan</strong></p>

<!-- two column -->
<div class="twoCol">
<h4>STEP 1</h4>
<p>Prioritize the team's focus and efforts.</p>

<h4>STEP 2</h4>
<p>Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at, euismod et, lorem. Ut auctor. Curabitur commodo.</p>

<h4>STEP 3</h4>
<p>This is my favorite step.</p>

</div>
<!-- two column end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at, euismod et, lorem. Ut auctor. Curabitur commodo. Pellentesque purus libero, dictum in.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<ul>
<li>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas ligula est, auctor morbi tristique.</li>
<li>Mauris ligula est, auctor vitae, pretium eget, hendrerit eu, mauris.</li>
<li>Praesent tempor molestie metus.</li>
</ul>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="ast"></span> IMPORTANT CONSIDERATIONS</h3>
<!-- child -->
<div class="showhide-child">
<!-- two column -->
<div class="twoCol">
<h4>TIP 1</h4>

<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliqua.</p>

<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>

<hr />

<h4>TIP 2</h4>

<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
</div>
<!-- two column end -->
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<blockquote>
<p><strong>SCHOOL EXAMPLE</strong> Utinterdum, risus id luctus consectetuer, velit neque ornare quam, at ornare nisi velit nec turpis. Phasellus neque libero, tincidunt accumsan, commodo eget, tempor quis, sem. Phasellus a ipsum Mauris et justo ac mauris congue convallis. </p>
</blockquote>


<table class="data">

<thead>
<tr>
<th width="200">Donect A Diam Ut Nunc</th>
<th>Ut Nunc Phaselius pickle</th>
</tr>
</thead>

<tbody>
<tr>
<td>Vivamus Pharetra Ornare Eros</td>
<td>Proin dui. Mauris facilisis dolor semper massa. Duis felis nunc, lobortis ut, mollis eu, commodo eu, dui. Vivamus pharetra ornare eros. Mauris vestibulum nulla et sapien. Donec a diam ut nunc tempor ornare. </td>
</tr>

<tr>
<td>Pharetra Ornare Eros</td>
<td>Donec a diam ut nunc tempor ornare. Proin dui. Mauris facilisis dolor semper massa. Duis felis nunc, lobortis ut, mollis eu, commodo eu, dui. Vivamus pharetra ornare eros. Mauris vestibulum nulla et sapien. </td>
</tr>
</tbody>

</table>


<!-- Tabs -->
<div class="tabs group">
<ul>
<li><a id="showA" class="on">Staff</a></li>
<li><a id="showB">Parents</a></li>
<li><a id="showC">Student</a></li>
<li><a id="showD">Community</a></li>
</ul>
</div>
<!-- Tabs end -->

<!-- Tabs: Staff -->
<div class="itemA">
<p>Staff stuff...  Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at, euismod et, lorem. Ut auctor. Curabitur commodo. Pellentesque purus libero, dictum in.  In hac habitasse platea dictumst. Proin sagittis mauris ac odio.Morbi </p>
</div>
<!-- Tabs: Staff end -->

<!-- Tabs: Parents -->
<div class="itemB">
<p>Parents stuff...  Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at, euismod et, lorem. Ut auctor. Curabitur commodo. Pellentesque purus libero, dictum in.  In hac habitasse platea dictumst. Proin sagittis mauris ac odio.Morbi </p>
</div>
<!-- Tabs: Parents end -->

<!-- Tabs: Student -->
<div class="itemC">
<p>Student stuff...  Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at, euismod et, lorem. Ut auctor. Curabitur commodo. Pellentesque purus libero, dictum in.  In hac habitasse platea dictumst. Proin sagittis mauris ac odio.Morbi </p>
</div>
<!-- Tabs: Student end -->

<!-- Tabs: Community -->
<div class="itemD">
<p>Community stuff...  Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at, euismod et, lorem. Ut auctor. Curabitur commodo. Pellentesque purus libero, dictum in.  In hac habitasse platea dictumst. Proin sagittis mauris ac odio. </p>
</div>
<!-- Tabs: Community end -->


<br clear="all" />
<br clear="all" />

<p><strong>Lesson 2 Interactive Table</strong></p>

<!-- Tabs Vertical -->
<div class="tabsvert nb" style="height: 300px;">

<ul class="accordion">
<li class="parent open"><a href="" class="trigger" target="_blank">Facilitation Team</a>
    <ul class="child open">
	<li>
	<h4>ROLE</h4>
	<p>Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at, euismod et, lorem. Ut auctor. Curabitur commodo. Pellentesque purus libero, dictum in.  In hac habitasse platea dictumst. Proin sagittis mauris ac odio.</p>
	<h4>STAFFING</h4>
	<p>Donec condimentum fermentum dolor. Duis vulputate. Curabitur in ipsum nec justo elementum facilisis. Pellentesque ipsum pede, malesuada non, vulputate at.</p>
	<p>Curabitur commodo. <a href="" target="_blank">Pellentesque purus libero</a>, dictum in.  In hac habitasse platea dictumst. Proin sagittis mauris ac odio.</p>
	</li>
    </ul>
</li>
<li class="parent"><a href="" class="trigger" target="_blank">Magnet Identity Team</a>
    <ul class="child">
    </ul>
</li>
<li class="parent"><a href="" class="trigger" target="_blank">Curriculum Connections Team</a>
    <ul class="child">
    </ul>
</li>
</ul>

</div>
<!-- Tabs Vertical end -->

<br clear="all" />
<br clear="all" />

<p><strong>Lesson 5 Interactive Table</strong></p>

<!-- Tabs Vertical -->
<div class="tabsvert">

<h3 id="org">ORGANIZATION</h3>
<h3 id="part">PARTNERSHIP OPPORTUNITY</h3>

<ul class="accordion">
<li class="parent open org"><a href="" class="trigger" target="_blank">Private Organization or Business</a>
    <ul class="child open part">
    <li>Professional Staff Development</li>
	<li>Theme Consultants</li>
	<li>Guest Speakers and Programs</li>
    </ul>
</li>
<li class="parent org"><a href="" class="trigger" target="_blank">Colleges and Universities</a>
    <ul class="child part">
	<li>Colleges Item one</li>
	<li>Colleges Item two</li>
	<li>Colleges Item three</li>
    </ul>
</li>
<li class="parent org"><a href="" class="trigger" target="_blank">Public Organizations</a>
    <ul class="child part">
	<li>Public Item one</li>
	<li>Public Item two</li>
	<li>Public Item three</li>
    </ul>
</li>
</ul>

</div>
<!-- Tabs Vertical end -->


















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
