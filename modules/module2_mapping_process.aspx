﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Step 1: Introduce the Mapping Process
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li class="on"><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a>
	<ul>
	<li><a href="/modules/module2_mapping_process.aspx" class="on">Step 1: Introduce the Mapping Process</a>
		<ul>
		<li><a href="/modules/module2_mapping_process_started.aspx">Get Started</a></li>
		</ul>
	</li>
	<li><a href="/modules/module2_map_components.aspx">Step 2: Identify Primary Map Components</a>
		<ul>
		<li><a href="/modules/module2_map_components2.aspx">Identify Standards and Assessments</a></li>
		<li><a href="/modules/module2_map_components3.aspx">Connect Theme to Curriculum</a></li>
		<li><a href="/modules/module2_map_components4.aspx">Develop Key Questions</a></li>
		</ul>
	</li>
	</ul>
</li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Step 1: Introduce the Mapping Process</h2>

<p>During your first meeting with either the Pilot Team or any subsequent teams, explain how mapping the theme into the curriculum will help to strengthen curricular coherence and integrate the theme in a meaningful way. If the grade-level or content-area teams have existing curriculum maps, they should review and revise the maps to ensure that they include important components. </p>

<h3>Set the Stage</h3>

<ul>
<li>Download the <a href="/modules/downloads/module2_introduction_mapping_process_protocol.docx" class="doc" target="_blank">Introduction to the Mapping Process Protocol (.doc)</a> to structure the curriculum mapping conversation and process for the team.</li>
<li>For the initial mapping session, have the team bring their lesson plans, scope and sequence guides, existing curriculum maps, and any theme-related materials they may find helpful. </li>
<li>Watch the video <a href="/modules/downloads/module2_mappinginaction.mp4" class="vid" target="_blank">Magnet Theme Mapping in Action</a> (6-minutes) with the team and discuss the aspects that are most beneficial and relevant to your site’s theme integration work. </li>
<li>Share the <a href="/modules/downloads/module2_sample_integrated_curriculum_maps.docx" class="doc" target="_blank">Sample Integrated Curriculum Maps (.doc)</a> with the team to review examples of original and updated curriculum maps and to see the evolution of one set of maps.</li>
<li>Provide the <a href="/modules/downloads/module2_curriculum_map_template.docx" class="doc" target="_blank">Curriculum Map Template (.doc)</a> and an overview of the primary components to be addressed. </li>
<li>Direct the team to focus initially on mapping the curriculum of just one existing unit of 4–5 weeks in duration.</li>
</ul>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>As teacher teams map curriculum, you may begin to see a need for additional professional development or teacher resources. Contact your district or regional office to learn about relevant professional development opportunities. Explore whether your district provides curriculum mapping software that aligns to standards and simplifies this process for teachers.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="thumbsup"></span> QUICK WIN</h3>
<!-- child -->
<div class="showhide-child">
<p>If a site has bought an integrated curriculum product, look into whether its developers offer free or fee-for-service professional development on curriculum mapping as part of the purchase price. </p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<h3>Magnet Theme Mapping in Action</h3>

<!-- video -->
<div class="video">
<p>This 6-minute video demonstrates how one 4th-grade team mapped its standards-based curriculum across content areas. Teachers considered how to modify instruction to integrate the site’s magnet theme in meaningful ways.
</p>

<div class="vid"><a href="/modules/module2_video_mappinginaction.htm" target="_blank"><span></span><img src="/modules/imgs/module2-video_still.jpg" /></a></div>

<p class="transcript"><a href="/modules/downloads/module2_mappinginaction_transcript.pdf" target="_blank">Download a transcript of this video (.pdf)</a></p>

</div>
<!-- video end -->






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>
</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
