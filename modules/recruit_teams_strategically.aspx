﻿<%@ Page Title="MSAP Center: Recruit Teams Strategically and Thoughtfully" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Recruit Teams Strategically and Thoughtfully
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li class="on"><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a>
	<ul>
	<li><a href="/modules/recruit_teams_strategically.aspx" class="on">Recruit Teams Strategically and Thoughtfully</a></li>
	<li><a href="/modules/identify_theme_integration_teams.aspx">Identify Theme Integration Teams</a></li>
	</ul>
</li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Creating Theme Integration Teams</em> Recruit Teams Strategically and Thoughtfully</h2>

<p>There are general guidelines to consider when establishing and recruiting staff for work teams:</p>

<ul>
<li>Define clear goals and responsibilities in advance, and share them with potential team members. This will help you and the staff (or parent or community volunteers) make informed decisions about ideal representation on the team. </li>
<li>Determine whether schools in the magnet program have existing collaboration structures that you can work within and that can absorb theme integration tasks. </li>
<li>Remind staff who don’t have a strong or obvious connection to the program theme that everyone’s experiences and perspectives are valuable in theme integration efforts.</li>
<li>Be sure to include staff who express interest and enthusiasm about a particular work team focus or magnet theme. They may become role models/cheerleaders for colleagues, encouraging buy-in among staff.</li>
</ul>

<p>It is important to note that many of the teams can be concurrently active and should have ongoing opportunities to share learnings and inform each other. To provide flexible support, all the tools provided in this module can be adapted to fit your situations and preferences. </p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>If funding permits, consider ways to recognize the efforts of each committee.  For example, provide sticky notes, poster paper, and markers for taking notes.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />







</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
