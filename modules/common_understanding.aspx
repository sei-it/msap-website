﻿<%@ Page Title="MSAP Center: Create a Common Understanding and Vision" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Create a Common Understanding and Vision
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li class="on"><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a>
	<ul>
	<li><a href="/modules/sustainable_partnerships.aspx">Build Sustainable Partnerships</a></li>
	<li><a href="/modules/partnership_opportunities.aspx">Explore Partnership Opportunities</a></li>
	<li><a href="/modules/learning_opportunities.aspx">Explore Learning Opportunities in the Community</a></li>
	<li><a href="/modules/volunteer_opportunities.aspx">Volunteer Opportunities</a></li>
	<li><a href="/modules/professional_development.aspx">Professional Development and Support for Teachers</a></li>
	<li><a href="/modules/advising_consulting_partnerships.aspx">Advising and Consulting Partnerships</a></li>
	<li><a href="/modules/encourage_family_support.aspx">Encourage Family Support</a></li>
	<li><a href="/modules/common_understanding.aspx" class="on">Create a Common Understanding and Vision</a></li>
	</ul>
</li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2><em>Connecting Theme, Students, and Community</em> Create a Common Understanding and Vision</h2>

<p>Publicizing the magnet school’s good work and efforts contributes to its sustainability. Ideally, parents and students will be among a school’s best ambassadors, able to communicate the program’s offerings to anyone interested in learning about them. Add a strategic marketing plan to further educate the whole community about unique magnet programs and their specific successes. Seek advice from parents or advisory group members who might be knowledgeable about marketing. Consider these practices for showcasing the magnet program:</p>

<ul>
<li>Highlight student achievements and theme-based projects or events in the principal’s newsletter or other regularly disseminated school literature.</li>
<li>Invite the local newspaper or radio and TV stations to cover a theme-based exhibition, performance, or event. </li>
<li>Participate in community festivals.</li>
<li>Design posters publicizing magnet program events and distribute to local businesses. Remember that students come from near and far neighborhoods.</li>
<li>Invite city council members, school board members, partners, and community members, including parents and families, to school events.</li>
</ul>

<p>For ideas on showcasing your magnet program, review the downloadable <a href="/modules/downloads/host_community_event.doc" class="doc">Host a Community Event (.doc)</a>.</p>

<blockquote>
<p><strong>School Example</strong> World Travel Night has become a signature event for one global awareness magnet school. Each year, the school hosts the event, inviting parents and members of the school community through flyers and student-made posters that appear in local merchants’ windows. For weeks leading up to the event, students work on various projects and older students prepare for their role as tour guides for the event that demonstrates the school’s focus on global literacy and cultural exploration.</p>

<p>Guests travel from classroom to classroom to view examples of student work. Students, wearing traditional costumes, explain the objective of the projects. In one class, first-grade students teach guests how to write their names in Chinese characters. The school’s all-purpose room presents samples of global cuisine prepared by parents and local restaurants, and the entertainment features a multicultural band comprised of fifth- and sixth-grade students.</p>
</blockquote>


















</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
