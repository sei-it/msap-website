﻿<%@ Page Title="MSAP Center: Connecting Theme, Students, and Community" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Connecting Theme, Students, and Community
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/showhide.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
	<script type="text/javascript" src="/modules/js/showhide3.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li class="on"><a href="/modules/connecting.aspx" class="on">Connecting Theme, Students, and Community</a>
	<ul>
	<li><a href="/modules/sustainable_partnerships.aspx">Build Sustainable Partnerships</a></li>
	<li><a href="/modules/partnership_opportunities.aspx">Explore Partnership Opportunities</a></li>
	<li><a href="/modules/learning_opportunities.aspx">Explore Learning Opportunities in the Community</a></li>
	<li><a href="/modules/volunteer_opportunities.aspx">Volunteer Opportunities</a></li>
	<li><a href="/modules/professional_development.aspx">Professional Development and Support for Teachers</a></li>
	<li><a href="/modules/advising_consulting_partnerships.aspx">Advising and Consulting Partnerships</a></li>
	<li><a href="/modules/encourage_family_support.aspx">Encourage Family Support</a></li>
	<li><a href="/modules/common_understanding.aspx">Create a Common Understanding and Vision</a></li>
	</ul>
</li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Connecting Theme, Students, and Community</h2>

<img src="/modules/imgs/photo_connecting.jpg" alt="students hugging and smiling" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>When schools, families, and the community work together to support student learning, students do better in school. Community partnerships bolster a school’s presence in the neighborhood and district, strengthen the student and teacher learning experience, and stabilize a magnet program for long-term sustainability. In short, partnerships are key to a magnet program’s success. </p>

<p>This lesson centers on the Community Connections Team, which advances a rigorous and innovative curriculum through exploring ways to connect the magnet theme to students and to the community. The work is multifaceted and will evolve over time, focusing on exploring new learning opportunities through community partnerships, broadening outreach to increase community capacity, and capitalizing on existing opportunities in the community.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>A successful Community Connections Team should include staff and parents with diverse expertise to spark ideas that the team can share with the leadership. Also, involve a school administrator who has knowledge of relevant magnet school and district policies and practices.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />



</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="obj">OBJECTIVES</h3>

<ul>
<li>Build sustainable partnerships</li>
<li>Explore partnership opportunities </li>
<li>Engage parent and family support</li>
<li>Create a common understanding and vision </li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="pdf"><a href="/modules/downloads/magnet_compass_vol2_issue2.pdf" target="_blank">The Magnet Compass, April 2012</a></li>
<li class="doc"><a href="/modules/downloads/team_meeting_notetaker.doc">Team Meeting Notetaker</a></li>
<li class="doc"><a href="/modules/downloads/host_community_event.doc">Host a Community Event (Activity)</a></li>
</ul>

</div>
</div>
<!-- mod end -->


</div>
<!-- Right Column end -->






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
