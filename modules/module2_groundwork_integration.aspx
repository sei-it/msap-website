﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Laying the Groundwork for Theme Integration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li class="on"><a href="/modules/module2_groundwork_integration.aspx" class="on">Lesson 1:<br />Laying the Groundwork for Theme Integration</a>
	<ul>
	<li><a href="/modules/module2_fit_approach.aspx">Fit the Implementation Approach to the Site</a></li>
	<li><a href="/modules/module2_pilot_team.aspx">Identify a Pilot Team for Mapping</a></li>
	<li><a href="/modules/module2_implementation_timeline.aspx">Develop an Implementation Timeline</a></li>
	</ul>
</li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Laying the Groundwork for Theme Integration</h2>

<img src="/modules/imgs/module2-photo_science.jpg" alt="students in science class" class="photo" />

<!-- Left Column -->
<div class="colLeft">


<p>Advance preparation helps to lay the groundwork that magnet leaders require when guiding teacher teams through curricular theme integration. This lesson begins by suggesting that you work with the Facilitation Team to clarify the <span class="hbox"><a>integrated curriculum design</a><span class="hoverbox">At each site, the integrated curriculum design describes the content areas, courses, and grade levels where the theme will be integrated.</span></span>. Knowing the intended design will inform your selection of a strong team to pilot the theme integration work. Then, establish an implementation timeline for completing each step, monitoring progress, and setting and communicating expectations for everyone involved. </p>

<p>Before you begin working with any teacher teams, it is important to do some advance preparation. Download the <a href="/modules/downloads/module2_curr_theme_integration_action_plan.docx" class="doc" target="_blank">Curriculum Theme Integration Action Plan (.doc)</a> tool to help you get started.</p>

<h3>Articulate the Integrated Curriculum Design</h3>

<p>When your MSAP grant application was written, someone thought about how fully the magnet theme would be integrated into each school site. Perhaps the design is intended to be schoolwide, perhaps selected content areas address the magnet theme, or maybe a separate pathway of courses comprises an academy. </p>

<p>As a magnet school gains momentum, its staff and community might decide to revise or expand on the initial design. Knowing the integrated curriculum design for each site—the specific content areas, grade levels, or courses in which theme integration should occur—will help you select the staff to pilot the work and plan the right implementation strategy. Download the <a href="/modules/downloads/module2_primer_integrated_curr_design.docx" class="doc" target="_blank">Primer on Integrated Curriculum Design (.doc)</a> tool for an overview of designs that can help to frame the theme integration work.</p>



</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight intro">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="modobj">COURSE<br />OBJECTIVES</h3>

<ul>
<li>Articulate the integrated curriculum design</li>
<li>Fit the implementation approach to the site</li>
<li>Identify a pilot team for mapping </li>
<li>Develop an implementation timeline</li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="doc"><a href="/modules/downloads/module2_curr_theme_integration_action_plan.docx" target="_blank">Curriculum Theme Integration Action Plan</a></li>
<li class="doc"><a href="/modules/downloads/module2_primer_integrated_curr_design.docx" target="_blank">Primer on Integrated Curriculum Design</a></li>
<li class="doc"><a href="/modules/downloads/module2_curr_strengths_barrier_protocol.docx" target="_blank">Curricular Strengths and Barriers Protocol</a></li>
<li class="pdf"><a href="../doc/MSAP_Magnet_Compass_Vol3_Is1_113.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', '../doc/MSAP_Magnet_Compass_Vol3_Is1_113_module2.pdf']);">January 2013 issue of The Magnet Compass</a></li>
</ul>

</div>
</div>
<!-- mod end -->

<br clear="all" />



</div>
<!-- Right Column end -->



</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->
<p id="footer_note"><em>Note: Some of the downloadable files on this website may require <a href="http://get.adobe.com/reader/" target="_blank">Adobe Reader</a>.</p>
<div class="clear">&nbsp;</div>

</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
