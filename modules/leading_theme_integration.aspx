﻿<%@ Page Title="MSAP Center: Leading Theme Integration" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Leading Theme Integration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li class="on"><a href="/modules/leading_theme_integration.aspx" class="on">Leading Theme Integration</a>
	<ul>
	<li><a href="/modules/guage_school_readiness.aspx">Gauge School Readiness for Professional Collaboration</a></li>
	<li><a href="/modules/frame_work_track_progress.aspx">Frame the Work of Teams and Track Progress</a></li>
	<li><a href="/modules/making_action_plan.aspx">Commit to Making the Action Plan a Living Document</a></li>
	<li><a href="/modules/increase_team_knowledge.aspx">Increase Team Knowledge on Integration Design</a></li>
	</ul>
</li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li><a href="/modules/resources.aspx">Resource Library</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Leading Theme Integration</h2>

<img src="/modules/imgs/photo_leading.jpg" alt="students looking at work pinned to board" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>Theme integration will look different at every magnet program site, depending on the theme and whether implementation is at whole school or partial school level. Whichever integration design a school chooses, staff and teams need to know that support is available.</p>

<h3>Facilitation Team</h3>

<p>The Facilitation Team&#8211;the project director, a school site administrator, and school-level instructional leaders&#8211;is established first and performs the following functions for the other theme integration teams:</p>

<ul>
<li>acts as information hub and liaison</li>
<li>recruits membership </li>
<li>monitors and supports work</li>
<li>provides guidance and oversees action plans for coherence and consistency with the magnet program vision outlined in the MSAP application.</li>
</ul>



</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="obj">OBJECTIVES</h3>

<ul>
<li>Gauge school readiness for professional collaboration</li>
<li>Frame the work of teams and track progress</li>
<li>Commit to making the action plan a living document</li>
<li>Increase team knowledge on integration design</li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="doc"><a href="/modules/downloads/gauging_school_readiness.doc">Gauging School Readiness for Professional Collaboration</a></li>
<li class="doc"><a href="/modules/downloads/team_purpose_protocol.doc">Team Purpose <br/>Protocol</a></li>
<li class="doc"><a href="/modules/downloads/team_meeting_notetaker.doc">Team Meeting Notetaker</a></li>
<li class="doc"><a href="/modules/downloads/team_progress_checklist.doc">Team Progress Checklist</a></li>
<li class="doc"><a href="/modules/downloads/primer_integrated_curr_design.doc">Primer on Integrated Curriculum Design</a></li>
</ul>

</div>
</div>
<!-- mod end -->


</div>
<!-- Right Column end -->






</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
