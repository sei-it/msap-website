﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Step 2: Identify Primary Map Components
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li class="on"><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a>
	<ul>
	<li><a href="/modules/module2_mapping_process.aspx">Step 1: Introduce the Mapping Process</a>
		<ul>
		<li><a href="/modules/module2_mapping_process_started.aspx">Get Started</a></li>
		</ul>
	</li>
	<li><a href="/modules/module2_map_components.aspx">Step 2: Identify Primary Map Components</a>
		<ul>
		<li><a href="/modules/module2_map_components2.aspx">Identify Standards and Assessments</a></li>
		<li><a href="/modules/module2_map_components3.aspx" class="on">Connect Theme to Curriculum</a></li>
		<li><a href="/modules/module2_map_components4.aspx">Develop Key Questions</a></li>
		</ul>
	</li>
	</ul>
</li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Step 2: Identify Primary Map Components</h2>

<h3>Connect Theme to Curriculum</h3>

<p>By mapping thematic connections in the curriculum, teachers can better see how or where theme integration already exists. Where integration is missing, teams can draw ideas from a wealth of resources, as described more fully in the lesson on Leveraging Innovative Strategies. And remember, teachers may identify a few lessons or units that won’t intersect with the theme (e.g., Beowulf and aerospace). Don’t worry about forcing connections in those cases, just remind them to do their best to ensure that students have some exposure to the theme in every subject area and grade that are included in your integration design.</p>

<p>Here are some ways to connect the theme to the curriculum:</p>

<ul>
<li>Ask the team to examine the <em>Skills</em> and <em>Activities</em> across the subjects and, keeping the site’s magnet theme in mind, brainstorm thematic connections.</li>
<li>Encourage the team to expand on a theme-related project, skill, or concept from another grade level or content area.</li>
<li>Arrange for discussions and meetings between team members and industry experts to generate new ideas.</li>
<li>Let the team know that theme integration may not work for every unit or lesson. However, do not entirely dismiss a content area with less obvious connections to the theme; teachers of that content area may have a unique perspective on possible links.</li>
</ul>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>Invite teachers of theme-based courses, such as engineering or bioscience in a STEM school, to share lesson plans and content to inform core-subject teachers’ integration and curriculum maps.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->

<br clear="all" />

<!--<p><a href="/modules/module2_map_components2.aspx">&lt; Back: Identify Standards and Assessments</a> | <a href="/modules/module2_map_components4.aspx">Next: Develop Key Questions &gt;</a></p>-->





</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
