﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Develop an Implementation Timeline
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li class="on"><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a>
	<ul>
	<li><a href="/modules/module2_fit_approach.aspx">Fit the Implementation Approach to the Site</a></li>
	<li><a href="/modules/module2_pilot_team.aspx">Identify a Pilot Team for Mapping</a></li>
	<li><a href="/modules/module2_implementation_timeline.aspx" class="on">Develop an Implementation Timeline</a></li>
	</ul>
</li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li><a href="/modules/module2_leveraging_strategies.aspx">Lesson 4:<br />Leveraging Innovative Strategies</a></li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Develop an Implementation Timeline</h2>

<p>Sites will need to begin small and gradually scale up theme integration. A comprehensive implementation timeline will address setting up the teams, creating pilot maps and testing them in the classroom, and sharing pilot maps with the subsequent teams. These steps will help you organize a gradual rollout. </p>

<p>Help teachers understand that, although theme integration starts with the Pilot Team, the subsequent content-area and/or grade-level teams will be expected to learn from and use the pilot efforts as a basis for their own theme integration work.</p>

<p>The time it takes to completely integrate the theme will vary depending on a site’s readiness when you begin to facilitate the process. Make sure teachers have sufficient time to do the work, and help them see how their incremental steps move the larger process forward. Be sure to confirm with school principals and administrators that teams have ongoing planning time to meet, and consider holding a summer institute for intensive, extended planning time.</p>

<br clear="all" />

<!-- open/close -->
<div class="openclose">
<h3 class="showhide"><span class="flag"></span> LEADERSHIP TIP</h3>
<!-- child -->
<div class="showhide-child">
<p>To bring the staff up to speed, provide a big-picture overview at a staff meeting, discussing how the Pilot Team is conducting the initial curriculum mapping work, and how subsequent teams will continue the work. Introduce the proposed timeline for implementation, and be flexible about modifying the timeline.</p>
</div>
<!-- child end -->
</div>
<!-- open/close end -->









</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
