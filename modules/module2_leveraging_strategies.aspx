﻿<%@ Page Title="MSAP: Introduction" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP: Leveraging Innovative Strategies
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
	<script type="text/javascript" src="/modules/js/hoverbox_init.js"></script>
	<script type="text/javascript" src="/modules/js/showhide2.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod2">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 2</em> Mapping the Magnet Theme Into the Curriculum
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon2.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/module2_intro.aspx">Introduction</a></li>
<li><a href="/modules/module2_groundwork_integration.aspx">Lesson 1:<br />Laying the Groundwork for Theme Integration</a></li>
<li><a href="/modules/module2_mapping_curriculum.aspx">Lesson 2:<br />Mapping Curriculum for a Coherent Program</a></li>
<li><a href="/modules/module2_scaling_strategy.aspx">Lesson 3:<br />Scaling the Strategy</a></li>
<li class="on"><a href="/modules/module2_leveraging_strategies.aspx" class="on">Lesson 4:<br />Leveraging Innovative Strategies</a>
	<ul>
	<li><a href="/modules/module2_leverage_resources.aspx">Leverage Resources to Deepen Knowledge</a></li>
	<li><a href="/modules/module2_expand_magnet_partners.aspx">Expand the Role of Magnet Partners</a></li>
	<li><a href="/modules/module2_pedagogical_approaches.aspx">Explore Pedagogical Approaches to Theme Integration</a></li>
	</ul>
</li>
<li><a href="/modules/module2_cycle_improvement.aspx">Lesson 5:<br />Instituting a Cycle of Improvement</a></li>
</ul>

<div class="vidpromo">
<span class="thumb"><img src="/modules/imgs/module2-promo_video.jpg" alt="Magnet Theme Mapping in Action Video" /></span>
<p><a href="/modules/downloads/module2_mappinginaction.mp4" target="_blank">Download our Magnet Theme Mapping in Action Video</a> (.mp4)</p>
<p class="note"><em>Requires <a href="http://www.apple.com/quicktime/download/" target="_blank">QuickTime</a></em></p>
</div>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Leveraging Innovative Strategies</h2>

<img src="/modules/imgs/module2-photo_youngkids.jpg" alt="young children laughing" class="photo" />

<!-- Left Column -->
<div class="colLeft">

<p>Theme can influence curriculum in many ways. It can alter content, pedagogy, and projects within a course. It may even lead to cross-disciplinary instruction, team teaching, or development of a new course. </p>

<p>At some points in the theme integration process, teachers may feel “stuck.” They may be interested in integrating the theme, but confounded as to how to do so. This lesson addresses specific strategies for using magnet resources that are already present to generate new ideas for theme integration.</p>





</div>
<!-- Left Column end -->

<!-- Right Column -->
<div class="colRight intro">

<!-- mod -->
<div class="mod">
<div class="mod-inner">

<h3 class="modobj">COURSE<br />OBJECTIVES</h3>

<ul>
<li>Leverage resources to deepen knowledge</li>
<li>Expand the role of magnet partners</li>
<li>Explore pedagogical approaches to theme integration</li>
</ul>

<hr />

<h3 class="toolsinst">TOOLS &amp; INSTRUMENTS</h3>

<ul>
<li class="doc"><a href="/modules/downloads/module2_pedagogical_approaches.docx" target="_blank">Pedagogical Approaches and Theme Integration Ideas</a></li>
</ul>

</div>
</div>
<!-- mod end -->

<br clear="all" />



</div>
<!-- Right Column end -->



</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
