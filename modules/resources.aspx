﻿<%@ Page Title="MSAP Center: Bibliography" Language="C#" MasterPageFile="~/modules/magnetsub.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center: Bibliography
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <!-- Style and Javascript here -->
    <script type="text/javascript">
        function PopWindows(divID, status, imageID, imageSrc) {
            if (status == 0)
                document.getElementById(divID).style.display = 'none'
            else
                document.getElementById(divID).style.display = 'block'
            document.getElementById(imageID).src = imageSrc;
        }
    </script>
	<link rel="stylesheet" type="text/css" media="screen" href="/modules/css/modules.css" />
	<link rel="stylesheet" type="text/css" media="print" href="/modules/css/print.css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- MAIN CONTENT -->


<!-- Module Container -->
<div id="mod-container" class="mod1">

<img src="/modules/imgs/logo_print.gif" class="print" />

<h1><em>COURSE 1</em> Facilitating Magnet Theme Integration
<a href="../admin/TA_courses.aspx"><img src="/modules/imgs/home_icon1.png"/></a>
</h1>

<!-- Inner -->
<div class="inner group">


<!-- Subnav -->
<div class="subnav">

<ul>
<li><a href="/modules/intro.aspx">Introduction</a></li>
<li><a href="/modules/theme_integration_teams.aspx">Creating Theme Integration Teams</a></li>
<li><a href="/modules/leading_theme_integration.aspx">Leading Theme Integration</a></li>
<li><a href="/modules/assessing_theme_visibility.aspx">Assessing Theme Visibility</a></li>
<li><a href="/modules/leveraging_theme_practices.aspx">Leveraging Theme-Related Practices</a></li>
<li><a href="/modules/connecting.aspx">Connecting Theme, Students, and Community</a></li>
<!--<li class="on"><a href="/modules/resources.aspx" class="on">Bibliography</a></li>-->
</ul>

</div>
<!-- Subnav end -->


<!-- Content -->
<div class="content">

<h2>Bibliography</h2>

<h4>INTEGRATED CURRICULUM</h4>

<ul class="res">
<li class="pdf"><a href="http://www.edu.gov.on.ca/eng/literacynumeracy/inspire/research/WW_Integrated_Curriculum.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'http://www.edu.gov.on.ca/eng/literacynumeracy/inspire/research/WW_Integrated_Curriculum.pdf']);">Integrated curriculum: Increasing relevance while maintaining accountability.</a>  What Works? Research into Practice (Research Monograph #28). Drake, S.M. & Reid, R. (2010, September).</li>

<li class="pdf"><a href="http://www.curriculumassociates.com/professional-development/topics/Integrated-Curriculum/extras/lesson1/Reading-Lesson1.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'http://www.curriculumassociates.com/professional-development/topics/Integrated-Curriculum/extras/lesson1/Reading-Lesson1.pdf']);">Integrated curriculum (Close-Up #16).</a> School Improvement Research Series (SIRS): Northwest Regional Educational Laboratory. Lake, K. (1994).</li>

<li class="pdf"><a href="http://www.eric.ed.gov/PDFS/EJ787749.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'http://www.eric.ed.gov/PDFS/EJ787749.pdf']);">A guide to curricular integration.</a> Kappa Delta Pi Record, 39(4), 164–167. Morris, R.C. (2003, Summer).</li>

<li class="pdf"><a href="http://www.pacificedgepublishing.com/pdf/PlanThem.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'http://www.pacificedgepublishing.com/pdf/PlanThem.pdf']);">Planning a theme-based unit.</a> Canada: Pacific Edge Publishing, Ltd. Mumford, D. (2000).</li>

<li class="pdf"><a href="http://www2.ed.gov/admins/comm/choice/magnet-k8/magnetk-8.pdf" target="_blank"  Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'http://www2.ed.gov/admins/comm/choice/magnet-k8/magnetk-8.pdf']);">Creating and sustaining successful K-8 magnet schools.</a> Washington DC. U.S. Department of Education, Office of Innovation and Improvement. (2008).</li>
</ul>

<h4>PROFESSIONAL LEARNING COMMUNITIES</h4>

<ul class="res">
<li class="pdf"><a href="http://www.msapcenter.com/doc/MSAP_Magnet_Compass_Vol1_Is6_1011.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is6_1011__resourcespg.pdf']);">The Magnet Compass: Professional Development</a> (Vol.1, Issue 6, October 2011).</li>
</ul>

<h4>MEETING FACILITATION</h4>

<ul class="res">
<li class="open"><a href="http://www.nsrfharmony.org/protocol/a_z.html" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Links', 'Website', 'http://www.nsrfharmony.org/protocol/a_z.html']);">National School Reform Faculty</a></li>
</ul>

<h4>MARKETING, RECRUITING, AND PARTNERSHIPS</h4>

<ul class="res">
<li class="pdf"><a href="../doc/MSAP_Magnet_Compass_Vol2_Is2_412.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol2_Is2_412_resourcespg.pdf']);"><em>The Magnet Compass:</em> Community Partnerships</a> (Vol. 2, Issue 2, April 2012).</li>

<li class="pdf"><a href="http://www.msapcenter.com/doc/MSAP_Magnet_Compass_Vol1_Is2_0311.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is2_0311__resourcespg.pdf']);"><em>The Magnet Compass:</em> Family Engagement</a> (Vol. 1, Issue 2, March 2011).</li>

<li class="pdf"><a href="http://www.msapcenter.com/doc/MSAP_Magnet_Compass_Vol1_Is4_0511.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is4_0511__resourcespg.pdf']);"><em>The Magnet Compass:</em> Marketing and Recruitment</a> (Vol. 1, Issue 4, May 2011).</li>
</ul>










</div>
<!-- Content end -->

<br clear="all" />
<br clear="all" />

</div>
<!-- inner end -->


</div>
<!-- Module Container end -->



<!-- MAIN CONTENT end -->
</asp:Content>
