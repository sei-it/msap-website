﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class grantees : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (rbtnlstCohort.SelectedValue == "2010")
        {
            pnlCohort2010.Visible = true;
            pnlCohort2013.Visible = false;
        }
        else
        {
            pnlCohort2010.Visible = false;
            pnlCohort2013.Visible = true;
        }
    }
    protected void Buttons_Click(object sender, EventArgs e)
    {
        String clickedOn = String.Empty;
        clickedOn = SortFieldName.Value;
        DrawMapData(clickedOn, 0);
    }
    private void DrawMapData(String clickedState, int sortIndex)
    {
        lblState.Text = clickedState;
        MagnetDBDB db = new MagnetDBDB();
        var grantees = from m in db.MagnetmapData
                       where m.State.ToLower().Equals(clickedState.ToLower()) && m.Cohort=="2010"
                       select new {m.ID, m.Grantee, m.City, m.ProgramTitle, ProgramTitleLink = string.Format("doc/{0}", m.AbstractFile) };
            GridView1.DataSource = grantees;
            GridView1.DataBind();
        
    }
    protected void OnSateChanged(object sender, EventArgs e)
    {
        if (ddlStates.SelectedIndex > 0)
            DrawMapData(ddlStates.SelectedValue, 0);
    }

    protected void OnSateChanged2013(object sender, EventArgs e)
    {
        if (ddlState2013.SelectedIndex > 0)
            DrawMapData2013(ddlState2013.SelectedValue, 0);
    }

    private void DrawMapData2013(String clickedState, int sortIndex)
    {
	if (clickedState.ToLower() == "nc")
        {
            clickedState = "North Carolina";
            
        }
		
        lblState2013.Text = clickedState;
        MagnetDBDB db = new MagnetDBDB();
        var grantees = from m in db.MagnetmapData
                       where m.State.ToLower().Equals(clickedState.ToLower()) && m.Cohort == "2013"
                       select new { m.ID, m.Grantee, m.City, m.ProgramTitle, ProgramTitleLink = string.Format("doc/cohort2013/{0}", m.AbstractFile) };
            GridView2.DataSource = grantees;
            GridView2.DataBind();

    }
    protected void Search2013_Click(object sender, EventArgs e)
    {
        String clickedOn = String.Empty;
        clickedOn = SortFieldName2013.Value;
        DrawMapData2013(clickedOn, 0);
    }
}