﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class learnresource : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int page)
    {
        GridView1.DataSource = ManageUtility.SearchPublicationType("Video");
        GridView1.PageIndex = page;
        GridView1.DataBind();
    }

}