﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;

public partial class newsletters : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int index = 1;
        string strSlide="";
        var data = magnetCompass.Find(x => x.id > 0).OrderByDescending(o => o.IssueDate);

        foreach (var itm in data)
        {
          
            if (index == 4)
            {
                strSlide += "</p></div>";
                index = 1;
            }
            if (index == 1)
                strSlide += "<div class='slide'><h2 class='Newsletter_header'>&nbsp; </h2><br /><p class='Newsletter_header'>";

            strSlide += "<a href='" + itm.DocURL + "' target='_blank' onclick='_gaq.push(['_trackEvent', 'Downloads', 'PDF', '" + itm.DocURL + "']);'>";
            strSlide += "<img src='img/Handler.ashx?CompassID=" + itm.id + "' alt='" + itm.imgAlt + "' width='180' style='border: 0px solid #CCC;' /></a>";
            index++;
        }
        strSlide += "</p></div>";
        ltlSilides.Text = strSlide;
                   
    }
    protected void OnSignup(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSubscription.Text))
        {
            MagnetMailList mail = new MagnetMailList();
            mail.Email = txtSubscription.Text;
            mail.UnSubscribed = false;
            mail.TimeStamp = DateTime.Now;
            mail.Save();
            txtSubscription.Text = "";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "confirm", "<script>alert('Your subscription has been received!');</script>", false);
        }
    }
}