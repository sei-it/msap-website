﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="disclaimer.aspx.cs" Inherits="disclaimer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
MSAP Center - Disclaimer
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Disclaimer</h1>
        <p>
            This website was produced in whole or in part with funds from the U.S. Department
            of Education under contract number: ED-OII-13-C-0073.</p>
            
        <p>This web site contains some copyrighted material whose use has not been authorized by the copyright owners. We believe that this not-for-profit, educational use on the Web constitutes a fair use of the copyrighted material (as provided for in section 107 of the US Copyright Law). If you wish to use this copyrighted material for purposes that go beyond fair use, you must obtain permission from the copyright owner.</p>
    </div>
</asp:Content>
