﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="importantlinks.aspx.cs" Inherits="importantlinks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Important links
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <h1>
            Important Links</h1>
        <p>
            <h2>
                <a href="http://www.centerii.org" target="_blank" title="Center on Innovation and Improvement ">
                    Center on Innovation and Improvement </a>
            </h2>
            The Center on Innovation & Improvement supports regional centers in their work with
            states to provide districts, schools, and families with the opportunity, information,
            and skills to make wise decisions on behalf of students.
        </p>
       <%-- <p>
            <h2>
                <a href="http://dww.ed.gov/" target="_blank" title="Doing What Works.">Doing What Works.</a></h2>
            Doing What Works’ mission is to translate research-based practices into practical
            tools to improve classroom instruction. The Doing What Works website features resources
            on data-driven improvement; quality teaching; literacy; math and science; comprehensive
            support; and early childhood.
        </p>--%>
        <p>
            <h2>
                <a href="links.aspx" title="Equity Assistance Centers">Equity Assistance Centers</a></h2>
            The Equity Assistance Centers provide assistance in the areas of race, gender, and
            national origin equity to public schools to promote equal educational opportunities.
            Equity Assistance Centers provide training and technical assistance for state or
            local education agencies, but requests may originate from teachers, principals,
            parents, community leaders, and state and district administrators.
        </p>
        <p>
            <h2>
                <a href="http://www.hfrp.org" target="_blank" title="Harvard Family Research Project">
                    Harvard Family Research Project</a>
            </h2>
            The Harvard Family Research Project’s (HFRP) work strengthens family, school, and
            community partnerships; early childhood care and education; promotes evaluation
            and accountability; and offers professional development to those who work directly
            with children, youth, and families. The audiences for HFRP's work include policymakers,
            practitioners, researchers, evaluators, philanthropists, teachers, school administrators,
            and concerned individuals.
        </p>
        <p>
            <h2>
                <a href="http://www.magnet.edu" target="_blank" title="Magnet Schools of America">
                    Magnet Schools of America</a>
            </h2>
            Magnet Schools of America is a not-for profit, professional educational association
            that sponsors programs, events, technical assistance, student scholarships, professional
            development, and leadership through the National Institute for Magnet School Leadership.
        </p>
        <p>
            <h2>
                <a href="http://www.vanderbilt.edu/schoolchoice/" target="_blank" title="National Center on School Choice">
                    National Center on School Choice</a>
            </h2>
            The National Center on School Choice (NCSC) conducts research on how school choice
            affects individuals, communities, and systems. NCSC’s work takes place across multiple
            disciplines and methodologies, and their aim is to provide national intellectual
            leadership on the study of school choice in all its forms.
        </p>
        <p>
            <h2>
                <a href="http://www.nationaltitleiassociation.org/" target="_blank" title="National Title I Association">
                    National Title I Association</a>
            </h2>
            The National Title I Association builds the capacity of state and local educators
            for leadership, support, and advocacy in the design and effective implementation
            of programs under Title I of the Elementary and Secondary Education Act to enable
            disadvantaged children and youth to meet or exceed high academic state standards.
        </p>
        <p>
            <h2>
                <a href="http://www2.ed.gov/about/offices/list/ocr/index.html" target="_blank" title="U.S. Department of Education, Office of Civil Rights ">
                    U.S. Department of Education, Office for Civil Rights</a>
            </h2>
            Office of Civil Rights (OCR) serves student populations facing discrimination and
            the advocates and institutions promoting systemic solutions to civil rights problems.
            OCR also provides technical assistance to help institutions achieve voluntary compliance
            with the civil rights laws that OCR enforces.
        </p>
        <p>
            <h2>
                <a href="http://ies.ed.gov/ncee/wwc/" target="_blank" title="What Works Clearinghouse">
                    What Works Clearinghouse</a>
            </h2>
            An initiative of the U.S. Department of Education's Institute of Education Sciences,
            the What Works Clearinghouse: 1.) Produces user-friendly practice guides for educators;
            2.) Assesses the rigor of research evidence on the effectiveness of interventions;
            3.) Develops and implements standards for reviewing and synthesizing education research;
            and 4.) Provides a public and easily accessible registry of education evaluation
            researchers.
        </p>
    </div>
</asp:Content>
