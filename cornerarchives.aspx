﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="cornerarchives.aspx.cs" Inherits="cornorarchives" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - The Grantee Corner | Archive
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
<!--
/** 
 * Slideshow style rules.
 */
#slideshow {
	margin:15 auto;
	width:840px;
	height:200px;
	background:transparent url(img/bg_slideshowgrantee.jpg) no-repeat  0 0;
	position:relative;
}
#slideshow #slidesContainer {
  margin:0 auto;
  width:760px;
  height:200px;
  overflow:auto; /* allow scrollbar */
  position:relative;
}
#slideshow #slidesContainer .slide {
  margin:0 auto;
  width:740px; /* reduce by 20 pixels of #slidesContainer to avoid horizontal scroll */
  height:200px;
}


/** 
 * Slideshow controls style rules.
 */
.control {
  display:block;
  width:39px;
  height:200px;
  text-indent:-10000px;
  position:absolute;
  cursor: pointer;
}
#leftControl {
  top:0;
  left:0;
  background:transparent url(img/control_left.png) no-repeat 0 0;
}
#rightControl {
  top:0;
  right:0;
  background:transparent url(img/control_right.png) no-repeat 0 0;
}

#slideshow13 {
	margin:15 auto;
	width:840px;
	height:200px;
	background:transparent url(img/bg_slideshowgrantee.jpg) no-repeat  0 0;
	position:relative;
}
#slideshow13 #slidesContainer13 {
  margin:0 auto;
  width:760px;
  height:200px;
  overflow:auto; /* allow scrollbar */
  position:relative;
}
#slideshow13 #slidesContainer13 .slide {
  margin:0 auto;
  width:740px; /* reduce by 20 pixels of #slidesContainer to avoid horizontal scroll */
  height:200px;
}

/** 
 * Slideshow controls style rules.
 */
.control13 {
  display:block;
  width:39px;
  height:200px;
  text-indent:-10000px;
  position:absolute;
  cursor: pointer;
}
#leftControl13 {
  top:0;
  left:0;
  background:transparent url(img/control_left.png) no-repeat 0 0;
}
#rightControl13 {
  top:0;
  right:0;
  background:transparent url(img/control_right.png) no-repeat 0 0;
}


#pageContainer {
  margin:0 auto;
  width:960px;
}
#pageContainer h1 {
  display:block;
  width:960px;
  height:114px;
  /*background:transparent url(img/bg_pagecontainer_h1.jpg) no-repeat top left;*/
  text-indent: -10000px;
}
.slide h2, .slide p {
  margin:10px 0px;
}

.Featured_header{
	margin:10px;
}

.slide img {
  float:left;
  /*margin:15 55px;*/
   margin:5px 10px;
}

.slide13 h2, .slide13 p {
  margin:10px 0px;
}

.Featured_header13{
	margin:10px;
}

.slide13 img {
  float:left;
  /*margin:15 55px;*/
   margin:5px 10px;
}

.mainContent {
	width: 100%;
	height: auto;
	display: table;
	clear: both;
}

.mainContent2 {
	width: 100%;
	height: auto;
	display: table;
	clear: both;
	border-bottom:none;
	border:none;
	border-color:#999;
}


#msap_Footer {
					width:  840px;
					margin-top: 15px;
					height: auto;
					display: table;
					font-family: Helvetica, Arial, sans-serif;
					font-size: 11px;
					clear: both;
				}
				
			  #archiveTxt1 {
				   color: #016d61;
				   font-size: 1em;
				   font-weight: bold;
				   font-family:Arial, Helvetica, sans-serif;
				  
				   
			   }
			   
			   #archiveTxt1 {
				   color: #016d61;
				   font-size: 1em;
				   font-weight: bold;
				   
			   }
			   			   
			   #archiveTxt2 {
				   color: #999;
				   font-size: 1em;
				   font-weight: lighter;
				  
			   }
			   
			   #archiveTxt3 {
				   color: #f60;
				   font-size: 1em;
				   font-weight: lighter;
				   font-family:Arial, Helvetica, sans-serif;
				
			   }


			  
-->
</style>
    <script type="text/javascript">
        $(document).ready(function () {
            var currentPosition = 0;
            var slideWidth = 600;
            var slides = $('.slide');
            var numberOfSlides = slides.length;

            var currentPosition13 = 0;
            var slideWidth13 = 600;
            var slides13 = $('.slide13');
            var numberOfSlides13 = slides13.length;

            // Remove scrollbar in JS
            $('#slidesContainer').css('overflow', 'hidden');
            $('#slidesContainer13').css('overflow', 'hidden');

            // Wrap all .slides with #slideInner div
            slides
    .wrapAll('<div id="slideInner"></div>')
            // Float left to display horizontally, readjust .slides width
	.css({
	    'float': 'left',
	    'width': slideWidth
	});

	slides13
    .wrapAll('<div id="slideInner13"></div>')
    .css({
        'float': 'left',
        'width': slideWidth13
    });
            // Set #slideInner width equal to total width of all slides
	$('#slideInner').css('width', slideWidth * numberOfSlides);
	$('#slideInner13').css('width', slideWidth13 * numberOfSlides13);

            // Insert controls in the DOM
            $('#slideshow')
    .prepend('<span class="control" id="leftControl">Clicking moves left</span>')
    .append('<span class="control" id="rightControl">Clicking moves right</span>');

            $('#slideshow13')
    .prepend('<span class="control13" id="leftControl13">Clicking moves left</span>')
    .append('<span class="control13" id="rightControl13">Clicking moves right</span>');

            // Hide left arrow control on first load
            manageControls(currentPosition);
            manageControls13(currentPosition13);

            // Create event listeners for .controls clicks
            $('.control')
    .bind('click', function () {
        // Determine new position
        currentPosition = ($(this).attr('id') == 'rightControl') ? currentPosition + 1 : currentPosition - 1;

        // Hide / show controls
        manageControls(currentPosition);
        // Move slideInner using margin-left
        $('#slideInner').animate({
            'marginLeft': slideWidth * (-currentPosition)
        });
    });

    // Create event listeners for .controls clicks
    $('.control13')
    .bind('click', function () {
        // Determine new position
        currentPosition13 = ($(this).attr('id') == 'rightControl13') ? currentPosition13 + 1 : currentPosition13 - 1;

        // Hide / show controls
        manageControls13(currentPosition13);
        // Move slideInner using margin-left
        $('#slideInner13').animate({
            'marginLeft': slideWidth13 * (-currentPosition13)
        });
    });

            // manageControls: Hides and Shows controls depending on currentPosition
            function manageControls(position) {
                // Hide left arrow if position is first slide
                if (position == 0) { $('#leftControl').hide() } else { $('#leftControl').show() }
                // Hide right arrow if position is last slide
                if (position == numberOfSlides - 1) { $('#rightControl').hide() } else { $('#rightControl').show() }
            }

        // manageControls: Hides and Shows controls depending on currentPosition
        function manageControls13(position) {
            // Hide left arrow if position is first slide
            if (position == 0) { $('#leftControl13').hide() } else { $('#leftControl13').show() }
            // Hide right arrow if position is last slide
            if (position == numberOfSlides13 - 1) { $('#rightControl13').hide() } else { $('#rightControl13').show() }
        }
    });
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
    </div>
    <a name="skip"></a>
    <div class="mainContent">
        <img src="img/3kids.jpg" width="842" alt="banner" />
        <br />
        <br />
        <br />
        <h2 style="border: thick; font-size: 2em;">
            <span class="archiveTxt1">The Grantee Corner</span> <span class="archiveTxt2">|</span>
            <span class="archiveTxt3">Archive</span></h2>
        <div id="pageContainer">
            <table width="842">
                <tr>
                    <td colspan="1">
                    </td>
                </tr>
                <tr>
                    <td width="840">
                        The U.S. Department of Education Magnet Schools Assistance Program (MSAP) projects
                        support the development of both innovative educational methods and practices that
                        promote diversity, while increasing options in public educational programs. MSAP
                        supports capacity development and the ability of a school to help all students meet
                        challenging standards through professional development and other activities that
                        will ultimately enable the continued operation of the magnet schools at a high performance
                        level after federal funding ends. The program provides support for magnet schools
                        to implement courses of instruction that strengthen students’ content knowledge
                        and their grasp of tangible and marketable vocational skills.
                        <br />
                        <br />
                        The Grantee Corner features MSAP grant projects, providing information on its magnet themes, partnerships, and innovative strategies.
                    </td>
                </tr>
            </table>
            <br />
                        <asp:Literal ID="Literal1" runat="server"><h3>MSAP 2013 Cohort</h3></asp:Literal>
            <!-- Slideshow HTML -->
            <div id="slideshow13">
            <div id="slidesContainer13">

             <asp:Literal ID="ltlNavigationLinks2013" runat="server" />                                              
                    <p style="font: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold;
                        color: #000">
                    </p>
            </div>
            </div>
            <br />
             <asp:Literal ID="ltlCohort2010" runat="server"><h3>MSAP 2010 Cohort</h3></asp:Literal>

            <div id="slideshow">
                <div id="slidesContainer">
                    <asp:Literal ID="ltlNavigationLinks" runat="server" />                                              
                    <p style="font: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold;
                        color: #000">
                    </p>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
                     
  
</asp:Content>
