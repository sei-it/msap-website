﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="webinar.aspx.cs" Inherits="webinar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - Webinars
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            Webinars</h1>
        <p>
            View upcoming webinars.</p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false"
            ShowHeader="false" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl"
            GridLines="None" OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
            <RowStyle BorderStyle="None" BorderColor="White" />
            <HeaderStyle BorderStyle="None" BorderColor="White" />
            <Columns>
                <asp:BoundField DataField="DisplayData" HtmlEncode="false" SortExpression="" HeaderText="" />
            </Columns>
        </asp:GridView>

    </div>
</asp:Content>
