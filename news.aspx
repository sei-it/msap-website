﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="news.aspx.cs" Inherits="news" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"
         TagPrefix="ajaxToolkit" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - News Archive
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <a name="skip"></a>
          <img src="images/NewsArchive_img.jpg" width="842px" style="margin:0px;"/>
    <div class="mainContent">
    <h1 style="border:none;"><span class="archiveTxt1">Magnet School News</span><span class="archiveTxt2">|</span> <span class="archiveTxt3">Archive</span></h1>
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" >
  
            <div class="news_search_topbox">
                &nbsp;</div>
            <div class="news_search_content">
                <div class="gcTxt">
                    <table width="842px">
                        <tr>
                            <td width="10%"><b>Search</b></td>
                            <td width="50%"><asp:TextBox ID="txtSearch"  Width="680px" runat="server" /></td>
                            <td width="40%">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="3" align="center" valign="middle">
                                <asp:Literal ID="Literal1" runat="server"><b>Start Date</b></asp:Literal>
                           
                                <telerik:RadDatePicker ID="dpStartDate" MinDate="2009/1/1" Runat="server">
                                <ClientEvents OnDateSelected="BeginDateSelected" />
                                </telerik:RadDatePicker>
                                <asp:Literal ID="Literal2" runat="server"><b>End Date</b></asp:Literal>
                                <telerik:RadDatePicker ID="dpEndDate" MinDate="2009/1/1" Runat="server">
                                </telerik:RadDatePicker>
                                <asp:LinkButton ID="lbtnClearDates" runat="server" 
                                    onclick="lbtnClearDates_Click">Clear Dates</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                 </div>
            </div>
            <div class="news_search_bottombox">
                &nbsp;</div>    
    <table width="842px" style="padding: 5px 0px;">
        <tr>
            <td colspan="3" align="right" >
                <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlSort_SelectedIndexChanged" >
                    <asp:ListItem Selected="True" Value="0">Newest to Oldest</asp:ListItem>
                    <asp:ListItem Value="1">Oldest to Newest</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
	</table>

        </telerik:RadAjaxPanel>
          <telerik:RadCodeBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                function BeginDateSelected(sender, e) {
                    var date = e.get_newDate();
                    var endDatePickerID = sender.get_id().replace("dpStartDate", "dpEndDate");
                    var endDatePicker = $find(endDatePickerID);

                    endDatePicker.set_minDate(date);
                }
            </script>
        </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ListViewPanel1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ListViewPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" MinDisplayTime="0" />
      <asp:Panel ID="ListViewPanel1" runat="server">
             <telerik:RadListView ID="RadListView1" Width="97%" AllowPaging="True" runat="server"
                AllowSorting="False" AllowMultiFieldSorting="false" AllowNaturalSort="false"
                ItemPlaceholderID="ProductsHolder" 
                DataKeyNames="ListID" onneeddatasource="RadListView1_NeedDataSource" 
                onpageindexchanged="RadListView1_PageIndexChanged" 
                onpagesizechanged="RadListView1_PageSizeChanged" >
                <LayoutTemplate>
                    <fieldset style="width: 822px;border:0px;" id="FieldSet11">
                        <asp:Panel ID="ProductsHolder" runat="server" />
                        <table cellpadding="0" cellspacing="0" width="100%;" style="clear: both;">
                            <tr>
                                <td>
                                    <telerik:RadDataPager ID="RadDataPager1" runat="server" Skin="Office2007" PagedControlID="RadListView1">
                                        <Fields>
                                            <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                                            <telerik:RadDataPagerButtonField FieldType="Numeric" />
                                            <telerik:RadDataPagerButtonField FieldType="NextLast" />
                                            <telerik:RadDataPagerPageSizeField PageSizeText="Page size: " />
                                            <telerik:RadDataPagerGoToPageField CurrentPageText="Page: " TotalPageText="of" SubmitButtonText="Go"
                                                TextBoxWidth="15" />
                                           <%-- <telerik:RadDataPagerTemplatePageField>
                                                <PagerTemplate>
                                                    <div style="float: right">
                                                        <b>Items
                                                            <asp:Label runat="server" ID="CurrentPageLabel" Text="<%#Container.Owner.StartRowIndex+1%>" />
                                                            to
                                                            <asp:Label runat="server" ID="TotalPagesLabel" Text="<%#Container.Owner.TotalRowCount > (Container.Owner.StartRowIndex+Container.Owner.PageSize) ? Container.Owner.StartRowIndex+Container.Owner.PageSize : Container.Owner.TotalRowCount %>" />
                                                            of
                                                            <asp:Label runat="server" ID="TotalItemsLabel" Text="<%#Container.Owner.TotalRowCount%>" />
                                                            <br />
                                                        </b>
                                                    </div>
                                                </PagerTemplate>
                                            </telerik:RadDataPagerTemplatePageField>--%>
                                        </Fields>
                                    </telerik:RadDataPager>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </LayoutTemplate>
                <ItemTemplate>
                    <div style="float: left;">
                        <table cellpadding="0" cellspacing="0" style="width: 822px; height: 100px;">
                            <tr>
                                <td >
                                    <%# Eval("Data") %>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <fieldset style="width: 822px; height: 280px; border: 0;">
                       <h2>No records for news archive available.</h2> 
                    </fieldset>
                </EmptyDataTemplate>
            </telerik:RadListView>
        </asp:Panel>
        <br />
    </div>
</asp:Content>

