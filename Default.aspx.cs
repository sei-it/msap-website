﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder sbContents = new StringBuilder();
        if (!Page.IsPostBack)
        {
            MagnetDBDB db = new MagnetDBDB();

            var gc = db.MagnetGCs.SingleOrDefault(x => x.isActive);

            if (gc != null)
            {
                var gcimg = db.MagnetGCImages.SingleOrDefault(x=>x.isShowOnHomePage && x.gc_id == gc.id);
                            //select cimg;  new { id = cimg.id, alt_text = cimg.alt_text, caption = cimg.caption };
                ltlAbsText.Text = gc.abs_contents;
                ltlTitle.Text = gc.Title;

                if (gcimg != null)
                {
                    ltlCaption.Text = gcimg.caption;
                    ltlImageHolder.Text = "<a href='granteecorner.aspx?gcid=" + gc.id  + "'>";
                    ltlImageHolder.Text += "<img src='img/Handler.ashx?GCID=" + gcimg.id + "' alt='" + gcimg.alt_text + "' border='0' width='160'/></a>";
                    
                }
            }

            var news = db.spp_getnews(1).ExecuteDataSet();
            newsRepeater.DataSource = news;
            newsRepeater.DataBind();

            StringBuilder sb = new StringBuilder();

            foreach (MagnetHomepageEvent data in MagnetHomepageEvent.Find(x => x.Display == true && x.EventType=="w").OrderBy(x => x.SortField))
            {
                         sb.AppendFormat("<span class='intro_txt'>{0}</span>", data.Title);
                sb.AppendFormat("<br/><br/><strong>Organization: </strong>{0}", data.Organization);
      
                sb.AppendFormat("<br/><strong>Date: </strong>{0}<br/><br/>{1}", data.Date, data.Description);
            }
            webinarDiv.InnerHtml = sb.ToString();

            sb.Clear();

            foreach (MagnetHomepageEvent data in MagnetHomepageEvent.Find(x => x.Display == true && x.EventType=="c").OrderBy(x => x.SortField))
            {
                sb.AppendFormat("<span class='intro_txt'>{0}</span>", data.Title);
                sb.AppendFormat("<br/><br/><strong>Organization: </strong>{0}", data.Organization);
                sb.AppendFormat("<br/><strong>Location: </strong>{0}", data.Location);
                sb.AppendFormat("<br/><strong>Date: </strong>{0}<br/><br/>{1}", data.Date, data.Description);
            }
            webConferenceDiv.InnerHtml = sb.ToString();
            
             
            //magnet compass
            //string content = magnetCompass.SingleOrDefault(x => x.isCurrent == true).newscontents;
            string filePathName = "";
            var rcd= magnetCompass.Find(x => x.isCurrent == true).OrderByDescending(y=>y.IssueDate);

            if (rcd.Count() > 0)
                filePathName = rcd.First().DocURL;

            ltlCompass.Text += "<a href='" + filePathName + "' target='_blank' " +
                " onclick='_gaq.push(['_trackEvent', 'Downloads', 'PDF', '" + filePathName + "']);'> " +
                " <img src='images/magnet_compass.jpg' style='float: left; margin-right: 10px; margin-bottom: 8px;' " +
                " alt='Magnet Compass Newsletter' border='0' /> </a> " +
                rcd.First().newscontents +
                " <a href='" + filePathName + "' target='_blank'" +
                " onclick='_gaq.push(['_trackEvent', 'Downloads', 'PDF', '" + filePathName + "']);'>Learn more</a> ";


        }
    }
}