﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="privacypolicy.aspx.cs" Inherits="privacypolicy" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP - Privacy Policy
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>Privacy Policy</h1>
        <p>
            Some of our pages feature forms that let you voluntarily submit personal information,
            such as your name or e-mail address. For example, this occurs when you submit a
            Request for Technical Assistance. In all cases, submitted information is used only
            for the purposes described on the form and is not made available to any third party.
        </p>
        <p>
            This website contains links to other sites. Please be aware that the Magnet Schools
            Assistance Program Technical Assistance Center (MSAP Center) is not responsible
            for the privacy practices of these sites. Also, when users leave our site, we encourage
            them to read the privacy statements of every website that potentially collects personally
            identifiable information.
        </p>
    </div>
</asp:Content>
