﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="about.aspx.cs" Inherits="about" %>

<asp:Content ID="Content3" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - About Us
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a name="skip"></a>
    <div class="mainContent">
        <h1>
            About Us</h1>
        <div class="AboutUs_expBoxR" style="background: #A9DEF2;margin-bottom: 10px;">
            <div class="topCall">
                &nbsp;</div>
            <div class="midCall">
                    <div class="expTxt">
                        <a href="faq.aspx"><img src="images/faq_icon.gif" border=0 alt="chalkboard, written FAQs" id="FAQimg"/></a>
                        <br/>
                        <span class="expHdr" style="color: #F60;">MSAP Center FAQs</span>
                        <p id="AboutUsRbox">
                            Visit the MSAP Center FAQs to learn about the MSAP Center and technical assistance. <a href="faq.aspx">Learn More</a></p>
                    </div>
                </div>
               <div class="botCall">&nbsp;</div>
        </div>
        <p class="aboutustxt">
            The
            <strong>Magnet Schools Assistance Program Technical Assistance Center (MSAP Center)</strong>
            is dedicated to building capacity around project implementation and program management. This includes providing MSAP grantees and magnet schools with technical support in research-based content and innovative practices in program implementation, project management, performance measurement, evaluation, and sustainability.</p>
        <p class="aboutustxt">
        The MSAP Center serves MSAP grantees and schools by working to build a community of practice that creates, shares, and expands magnet school knowledge and best practices. The ultimate goal is to help magnet schools provide communities with educational opportunities that promote diversity, academic excellence, and equity.
        </p>
        <p class="aboutustxt">
             The MSAP Center is funded by the U.S. Department of Education (ED) and the Office of Innovation and Improvement. For additional information, please contact the <a href="contact.aspx">MSAP Center</a> or <a href="http://www2.ed.gov/programs/magnet/contacts.html" target="_blank">ED's MSAP team</a>.</p>
                <div id="aboutUsimgs">
                    <img src="images/AboutUs/children_at_computers.jpg" alt="Children working on computers"/> 
                    <img src="images/AboutUs/children_writing.jpg" alt="Children writting"/> 
                    <img src="images/AboutUs/StudentsinClassroom.jpg" alt="Children in a classroom"/>
                </div>
    </div>
</asp:Content>
