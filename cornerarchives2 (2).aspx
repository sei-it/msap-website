﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true" CodeFile="cornerarchives.aspx.cs" Inherits="cornorarchives" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    MSAP Center - The Grantee Corner | Archive
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="brnImg">
        <img src="images/cornerArchive.jpg" width="842" alt="Text" style="border-bottom: 1px solid #CCC;" />
    </div>
    <div class="mainContent" style="margin-top: -5px;">
        <div>
            <div style="margin-top: -25px; float: right; margin-left: 15px; padding-left: 15px;
                width: 220px; border-left: 1px solid #CCC; margin-top:10px">
                <p class="expHdr" style="color: #F60; margin-top:5px;">
                  <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="225"
                height="650">
                    <param name="movie" value="fla/banner.swf" />
                    <param name="quality" value="high" />
                    <param name="wmode" value="opaque" />
                    <param name="swfversion" value="6.0.65.0" />
                    <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don't want users to see the prompt. -->
                    <param name="expressinstall" value="Scripts/expressInstall.swf" />
                    <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                    <!--[if !IE]>-->
                    <object type="application/x-shockwave-flash" data="fla/banner.swf" width="225"
                    height="650">
                      <!--<![endif]-->
                      <param name="quality" value="high" />
                      <param name="wmode" value="opaque" />
                      <param name="swfversion" value="6.0.65.0" />
                      <param name="expressinstall" value="Scripts/expressInstall.swf" />
                      <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                      <div>
                        <h4>
                          Content on this page requires a newer version of Adobe Flash Player.</h4>
                        <p>
                          <a href="http://www.adobe.com/go/getflashplayer">
                            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
                                    alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                      </div>
                      <!--[if !IE]>-->
                    </object>
                    <!--<![endif]-->
                  </object>
                </p>
  </div>
            <div style="width: 550px; float: left;">
            <h1 style="border:none;"><span class="archiveTxt1">The Grantee Corner</span> <span class="archiveTxt2">|</span> <span class="archiveTxt3">Archive</span></h1>
				<p>
				The U.S. Department of Education Magnet Schools Assistance Program (MSAP) awards grants to improve student academic achievement and assist in the desegregation of public schools by supporting the elimination, reduction, and prevention of racial group isolation in elementary and secondary schools with substantial numbers of racially isolated students.  In order to meet the statutory purposes of the program, MSAP grantees must: <!--1.) Support the development and implementation of magnet schools that assist in the achievement of systemic reforms and 2.) Provide all students with the opportunity to meet challenging academic content and achievement standards.-->
				<ol>
					<li>Support the development and implementation of magnet schools that assist in the achievement of systemic reforms and
					</li>
					<li>Provide all students with the opportunity to meet challenging academic content and achievement standards.
					</li>
				</ol>
				</p>
				
				<p>
				MSAP projects support the development of both innovative educational methods and practices that promote diversity, while increasing options in public educational programs. MSAP supports capacity development and the ability of a school to help all students meet challenging standards through professional development and other activities that will ultimately enable the continued operation of the magnet schools at a high performance level after federal funding ends. Finally, the program provides support for magnet schools to implement courses of instruction that strengthen students’ content knowledge and their grasp of tangible and marketable vocational skills.
				</p>
				<p>
				Each month, The Grantee Corner features a 2010 MSAP grantee, which may include their schools, themes, partnerships, and innovative ways to engage students and families.
				</p>
			</div><!-- /style -->

</asp:Content>

