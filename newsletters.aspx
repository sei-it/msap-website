﻿<%@ Page Title="" Language="C#" MasterPageFile="~/magnetpub.master" AutoEventWireup="true"
    CodeFile="newsletters.aspx.cs" Inherits="newsletters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    MSAP Center - The Magnet Compass
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    
    <style type="text/css">

/** 
 * Slideshow style rules.
 */

#slideshow {
	margin:15 auto;
	width:840px;
	height:200px;
	background:transparent url(img/bg_slideshow.jpg) no-repeat  0 0;
	position:relative;
}
#slideshow #slidesContainer {
  margin:0 auto;
  width:760px;
  height:200px;
  overflow:auto; /* allow scrollbar */
  position:relative;
}
#slideshow #slidesContainer .slide {
  margin:0 auto;
  width:740px; /* reduce by 20 pixels of #slidesContainer to avoid horizontal scroll */
  height:200px;
}

/** 
 * Slideshow controls style rules.
 */
.control {
  display:block;
  width:39px;
  height:200px;
  text-indent:-10000px;
  position:absolute;
  cursor: pointer;
}
#leftControl {
  top:0;
  left:0;
  background:transparent url(img/control_left.png) no-repeat 0 0;
}
#rightControl {
  top:0;
  right:0;
  background:transparent url(img/control_right.png) no-repeat 0 0;
}


#pageContainer {
  margin:0 auto;
  width:960px;
}
#pageContainer h1 {
  display:block;
  width:960px;
  height:114px;
  /*background:transparent url(img/bg_pagecontainer_h1.jpg) no-repeat top left;*/
  text-indent: -10000px;
}
.slide h2, .slide p {
  margin:10px 0px;
}

.Newsletter_header{
	margin:10px;
}

.slide img {
  float:left;
  /*margin:15 55px;*/
   margin:5px 10px;
}


.mainContent {
	width: 100%;
	height: auto;
	display: table;
	clear: both;
}

.mainContent2 {
	width: 100%;
	height: auto;
	display: table;
	clear: both;
	border-bottom:none;
	border:none;
	border-color:#999;
}


#msap_Footer {
					width:  840px;
					margin-top: 15px;
					height: auto;
					display: table;
					font-family: Helvetica, Arial, sans-serif;
					font-size: 11px;
					clear: both;
				}
				
			  #archiveTxt1 {
				   color: #016d61;
				   font-size: 1em;
				   font-weight: bold;
				   font-family:Arial, Helvetica, sans-serif;
				  
				   
			   }
			   
			   #archiveTxt1 {
				   color: #016d61;
				   font-size: 1em;
				   font-weight: bold;
				   
			   }
			   			   
			   #archiveTxt2 {
				   color: #999;
				   font-size: 1em;
				   font-weight: lighter;
				  
			   }
			   
			   #archiveTxt3 {
				   color: #f60;
				   font-size: 1em;
				   font-weight: lighter;
				   font-family:Arial, Helvetica, sans-serif;
				
			   }


</style>

    <script type="text/javascript">
        $(document).ready(function () {
            var currentPosition = 0;
            var slideWidth = 600;
            var slides = $('.slide');
            var numberOfSlides = slides.length;

            // Remove scrollbar in JS
            $('#slidesContainer').css('overflow', 'hidden');

            // Wrap all .slides with #slideInner div
            slides
    .wrapAll('<div id="slideInner"></div>')
            // Float left to display horizontally, readjust .slides width
	.css({
	    'float': 'left',
	    'width': slideWidth
	});

            // Set #slideInner width equal to total width of all slides
            $('#slideInner').css('width', slideWidth * numberOfSlides);

            // Insert controls in the DOM
            $('#slideshow')
    .prepend('<span class="control" id="leftControl">Clicking moves left</span>')
    .append('<span class="control" id="rightControl">Clicking moves right</span>');

            // Hide left arrow control on first load
            manageControls(currentPosition);

            // Create event listeners for .controls clicks
            $('.control')
    .bind('click', function () {
        // Determine new position
        currentPosition = ($(this).attr('id') == 'rightControl') ? currentPosition + 1 : currentPosition - 1;

        // Hide / show controls
        manageControls(currentPosition);
        // Move slideInner using margin-left
        $('#slideInner').animate({
            'marginLeft': slideWidth * (-currentPosition)
        });
    });

            // manageControls: Hides and Shows controls depending on currentPosition
            function manageControls(position) {
                // Hide left arrow if position is first slide
                if (position == 0) { $('#leftControl').hide() } else { $('#leftControl').show() }
                // Hide right arrow if position is last slide
                if (position == numberOfSlides - 1) { $('#rightControl').hide() } else { $('#rightControl').show() }
            }
        });
		
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainContent">
        <img src="img/4kids.jpg" width="842" alt="banner" />
        <br />
        <br />
        <h2 style="border: thick; font-size: 2em;">
            <span class="archiveTxt1">The Magnet Compass</span> <span class="archiveTxt2">|</span>
            <span class="archiveTxt3 ">Archive</span></h2>
        <p style="font-size: 1.2em; font-style: normal; color: #016d61;">
            This quarterly newsletter will serve as a compass on your pathway to improve your magnet program.</p>
        <p>
            Superintendents, administrators, program directors, principals, teachers, parents,
            and other stakeholders with a vested interest in magnet programs will find exemplary
            resources, effective practices, research-based content, and innovative ideas for
            implementing, managing and sustaining magnet programs. In these ways, the MSAP Center
            will meet the ongoing needs of the MSAP grantees and the larger magnet schools community.
        </p>
        <p>
            <strong>"Remember to always keep The Magnet Compass by your side!"</strong>
        </p>
        <!-- Slideshow HTML -->
        <div id="slideshow">
            <div id="slidesContainer">
              <%--  <div class="slide">
                    <h2 class="Newsletter_header">&nbsp;
                    
                    </h2>
                    <br />
                    <p class="Newsletter_header">
                          <a href="doc/MSAP_Magnet_Compass_Vol3_Is2_413.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol3_Is2_413.pdf']);">
                            <img src="img/Apr2013_magnet_compass.jpg" alt="January 2013, Communities of Practice Magnet Compass Issue" width="180" style="border: 0px solid #CCC;" />
                        </a>
                        <a href="doc/MSAP_Magnet_Compass_Vol3_Is1_113.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol3_Is1_113.pdf']);">
                            <img src="img/Jan2013_magnet_compass.jpg" alt="January 2013, Communities of Practice Magnet Compass Issue" width="180" style="border: 0px solid #CCC;" />
                        </a>
                        <a href="doc/MSAP_Magnet_Compass_Vol2_Is4_1012.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol2_Is4_1012.pdf']);">
                            <img src="img/Oct2012_magnet_compass.jpg" alt="October 2012, Data-Driven Management Magnet Compass Issue" width="180" style="border: 0px solid #CCC;" />
                        </a>
                       
                    </p>
                    </div>
                <div class="slide">
                    <h2 class="Newsletter_header">&nbsp;
                        
                    </h2>
                    <br />
                    <p class="Newsletter_header">
                     	<a href="doc/MSAP_Magnet_Compass_Vol2_Is3_712.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol2_Is3_712.pdf']);">
                            <img src="img/Jul2012_magnet_compass.jpg" alt="July 2012, English Language Learners Magnet Compass Issue" width="180" style="border: 0px solid #CCC;" />
                        </a>
                        <a href="doc/MSAP_Magnet_Compass_Vol2_Is2_412.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol2_Is2_412.pdf']);">
                            <img src="img/Apr2012_magnet_compass.jpg" alt="April 2012, Community Partnerships Magnet Compass Issue" width="180" style="border: 0px solid #CCC;" />
                        </a>
                      
                        <a href="doc/MSAP_Magnet_Compass_Vol2_Is1_0112.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol2_Is1_0112.pdf']);">
                            <img src="img/Jan2012_magnet_compass.jpg" alt="January 2012, Program Management Magnet Compass Issue" width="180" style="border: 0px solid #CCC;" />
                        </a>
                   
                        	
                    </p>
                    </div>
                <div class="slide">
                        <h2>&nbsp;
                            
                        </h2>
                        <br />
                        <p>
                             <a href="doc/MSAP_Magnet_Compass_Vol1_Is6_1011.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is6_1011.pdf']);">
                            <img src="img/october_magnet_compass.jpg" alt="October 2011, Professional Development Magnet Compass Issue"
                                width="180" style="border: 0px solid #CCC;" />
                        </a>
                        <a href="doc/MSAP_Magnet_Compass_Vol1_Is5_0711.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is5_0711.pdf']);">
                            <img src="img/july_magnet_compass.jpg" alt="July 2011, School Improvement Magnet Compass Issue"
                                width="180" style="border: 0px solid #CCC;" />
                        	</a>
                            <a href="doc/MSAP_Magnet_Compass_Vol1_Is4_0511.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is4_0511.pdf']);">
                                <img src="img/may_magnet_compass.jpg" alt="May 2011, Marketing and Recruitment Magnet Compass Issue"
                                    width="180" style="border: 0px solid #CCC;" />
                            </a>
                           
                        
                        </p>
                    </div>
                <div class="slide">
                        <h2>&nbsp;
                            
                        </h2>
                        <br />
                        <p>
                           <a href="doc/MSAP_Magnet_Compass_Vol1_Is3_0411.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is3_0411.pdf']);">
                                <img src="img/april_magnet_compass.jpg" alt="April 2011, Fidelity of Implementation Magnet Compass Issue"
                                    width="180" style="border: 0px solid #CCC;" />
                            </a>
                      	    <a href="doc/MSAP_Magnet_Compass_Vol1_Is2_0311.pdf" target="_blank" Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is2_0311.pdf']);">
                                <img src="img/march_magnet_compass.jpg" alt="March 2011, Family Engagement Magnet Compass Issue"
                                    width="180" style="border: 0px solid #CCC;" />
                            </a>
                            <a href="doc/MSAP_Magnet_Compass_Vol1_Is1_0211.pdf" target="_blank"  Onclick="_gaq.push(['_trackEvent', 'Downloads', 'PDF', 'doc/MSAP_Magnet_Compass_Vol1_Is1_0211.pdf']);">
                                <img src="img/february_magnet_compass.jpg" width="180" alt="February 2011 Program Sustainability Magnet Compass Issue"
                                    style="border: 0px solid #CCC;" /></a></p>
                    </div>       --%> 
            <asp:Literal ID="ltlSilides" runat="server" />
                </div>
            </div>
            <p style="margin-bottom: -2em;">
                <span style="font-weight: bold; color: #f60;">Subscribe</span></p>
            <br />
            <p>
                Sign up for our quarterly newsletter.<br />
                <asp:TextBox ID="txtSubscription" runat="server" CssClass="msapTxt newsRegister"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="Sign Up!" CssClass="msapBtn" OnClick="OnSignup" /><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSubscription"
                    ErrorMessage="Please type in a valid Email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </p>
            <script type="text/javascript">                
                $('#ctl00_ContentPlaceHolder1_txtSubscription').watermark('E-mail address', { className: 'lightWaterClass' });
            </script>
            <p>
                <a href="cornerarchives.aspx"></a>
            </p>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />

</asp:Content>
