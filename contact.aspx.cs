﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Magnet;
using System.Text;

public partial class contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StringBuilder sb = new StringBuilder();
            foreach (MagnetGroupMember member in MagnetGroupMember.All().OrderBy(x => x.Priority))
            {
                sb.AppendFormat("<strong>{0}</strong><br />", member.MemberName);
                sb.AppendFormat("{0}<br />", member.Title);
                sb.AppendFormat("<a href='mailto:{0}'>{0}</a></p>", member.Email);
            }
            ContentDiv.InnerHtml = sb.ToString();
        }
    }
}