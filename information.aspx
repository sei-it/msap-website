﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/magnetadmin.master" AutoEventWireup="true"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    User Guide
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../css/mbtooltip.css" title="style1"
        media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.timers.js"></script>
    <script type="text/javascript" src="../../js/jquery.dropshadow.js"></script>
    <script type="text/javascript" src="../../js/mbtooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[title]").mbTooltip({
                opacity: .97,       //opacity
                wait: 800,           //before show
                cssClass: "default",  // default = default
                timePerWord: 70,      //time to show in milliseconds per word
                hasArrow: true, 		// if you whant a little arrow on the corner
                hasShadow: true,
                imgPath: "../../css/images/",
                anchor: "mouse", //"parent"  you can anchor the tooltip to the mouse position or at the bottom of the element
                shadowColor: "black", //the color of the shadow
                mb_fade: 200 //the time to fade-in
            });
        });
        $(function () {
            $("input:submit").click(function () {
                $("#hfControlID").val($(this).attr("id"));
            });
        });

    </script>
       <style>
        .msapDataTbl tr td:first-child
        {
            color: #000 !important;
        }
        .msapDataTbl tr td:last-child
        {
            border-right: 0px !important;
            text-align: center !important;
        }
        .msapDataTbl th
        {
            color: #000;
            text-align: left;
        }
		.msapDataTbl TDWithBottomNoRightBorder{
			text-align: center;
		}
        .TDCenterClass
        {
            text-align: center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4 class="text_nav">
        <a href="quarterreports.aspx">Main Menu</a> <span class="greater_than">&gt;</span>
        User Guide</h4>
    <ajax:ToolkitScriptManager ID="Manager1" runat="server">
    </ajax:ToolkitScriptManager>
    <p>
        Please read all the documents and the MAPS Guide below. These documents are intended to help users fill out all forms found on the MAPS.
    </p>
    <span style="color: #f58220;">
        <img src="../../images/head_button.jpg" align="ABSMIDDLE" />&nbsp;&nbsp; <b>User Guide</b></span>
        <ContentTemplate>
        <div id="user_guide_pg">
	   
            <table class="msapDataTbl">
                    <tr>
                        <th>
                            Document Name
                        </th>
                        <th class="TDCenterClass">
                            File Type
                        </th>
                    </tr>
                    <tr>
                        <td>Dear Colleague Letter</td>
                        
                        <td class="TDWithBottomNoRightBorder">
                           <a href="guides/MSAP_Dear_Colleague.doc">DOC</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          ED 524B Instructions
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                           <a href="guides/Ed524B_Instructions.pdf">PDF</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           GPRA Guide
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                            <a href="guides/MSAP_GPRA_Guidance.pdf">PDF</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          MAPS Guide
                        </td>
                        <td class="TDWithBottomNoRightBorder">
                           <a href="MAPS_guide.aspx">HTML Guide</a>
                        </td>
                    </tr>
                    
                </table>
        </div>
        
            
        </ContentTemplate>

   
</asp:Content>
